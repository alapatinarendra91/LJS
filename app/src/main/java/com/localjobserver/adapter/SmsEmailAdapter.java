package com.localjobserver.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.localjobserver.models.FresharsCoursesModel;
import com.localjobserver.models.LoadCandidatesModel;

import java.util.ArrayList;

import co.talentzing.R;

/**
 * Created by latitude on 03-08-2017.
 */

public class SmsEmailAdapter extends BaseAdapter {
    private OnCartChangedListener onCartChangedListener;
    private Activity context;
    private ArrayList<LoadCandidatesModel> LoadCandidatesList;
    public SmsEmailAdapter(Activity context,  ArrayList<LoadCandidatesModel> LoadCandidatesList) {
        super();
        this.context=context;
        this.LoadCandidatesList=LoadCandidatesList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.sms_email_adapter, null);
        }
        TextView repliedDate = (TextView)convertView.findViewById(R.id.repliedDate);
        repliedDate.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getRepliedDate());

        TextView contactNo = (TextView)convertView.findViewById(R.id.contactNo);
        contactNo.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getContactNo());

        TextView email = (TextView)convertView.findViewById(R.id.email);
        email.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getEmail());

        TextView name = (TextView)convertView.findViewById(R.id.name);
        name.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getFirstName());

        TextView repliedMessage = (TextView)convertView.findViewById(R.id.repliedMessage);
        repliedMessage.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getMessage());

        TextView comments = (TextView)convertView.findViewById(R.id.comments);
        if (((LoadCandidatesModel)LoadCandidatesList.get(position)).getComments().equalsIgnoreCase("")){
            comments.setText("NA");
        }else
        comments.setText(""+((LoadCandidatesModel)LoadCandidatesList.get(position)).getComments());

        contactNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCartChangedListener.setCartClickListener("call",position);
            }
        });

        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCartChangedListener.setCartClickListener("comments",position);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return LoadCandidatesList.size();
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return LoadCandidatesList.get(position);
    }

    public void setOnCartChangedListener(OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status,int position);
    }

    }

