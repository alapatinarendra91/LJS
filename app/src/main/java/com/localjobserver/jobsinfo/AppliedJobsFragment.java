package com.localjobserver.jobsinfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.CommonJobsListAdapter;
import com.localjobserver.ui.JobDescriptionActivity;

import java.util.ArrayList;
import java.util.List;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AppliedJobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AppliedJobsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView jobsListView;
    private ProgressDialog progressDialog;
    public ArrayList<JobsData> jobList;
    private CommonJobsListAdapter commonArrayAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppliedJobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AppliedJobsFragment newInstance(String param1, String param2) {
        AppliedJobsFragment fragment = new AppliedJobsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public AppliedJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_applied_jobs, container, false);
        jobsListView = (ListView)rootView.findViewById(R.id.appliedjobsList);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("jobItems", (ArrayList<JobsData>) jobList).putExtra("_id", ((JobsData) jobList.get(position)).get_id()).putExtra("selectedPos", position).setAction("fromApplied"));
                startActivity(new Intent(getActivity(), JobDescriptionActivity.class)   .putExtra("_id", ((JobsData) jobList.get(position)).get_id()).putExtra("selectedPos", position).setAction("fromApplied"));
            }
        });

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getJobsData();
                                    }
                                }
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJobsData();

            }
            });
        return rootView;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    public void onRefresh() {
        getJobsData();
        CommonUtils.showToast("Refresh", getActivity());
    }


    public void getJobsData(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    jobList = (ArrayList) result;
                    CommonUtils.appliedjobList = (ArrayList) result;
                    commonArrayAdapter = new CommonJobsListAdapter(getActivity(), jobList);
                    jobsListView.setAdapter(commonArrayAdapter);
                    progressDialog.dismiss();
                    Intent uiUpdateIntent = new Intent("update");
                    uiUpdateIntent.putExtra("type", 0);
                    uiUpdateIntent.putExtra("appliedCount", jobList.size());
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getAppliedJobs, CommonUtils.getUserEmail(getActivity())));
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
