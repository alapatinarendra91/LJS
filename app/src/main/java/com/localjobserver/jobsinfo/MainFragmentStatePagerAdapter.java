package com.localjobserver.jobsinfo;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

public class MainFragmentStatePagerAdapter extends FragmentStatePagerAdapter {
    private static final String LOG_TAG = MainFragmentStatePagerAdapter.class.getName();

    public static final String FRAGMENT_CLAZZ_KEY = "fragment";
    public static final String FRAGMENT_TITLE_KEY = "title";

    private final SparseArray<WeakReference<Fragment>> cache = new SparseArray<WeakReference<Fragment>>();
    private final java.util.List<Bundle> payloads = new java.util.ArrayList<Bundle>();
    private final FragmentManager manager;
    private final Context context;

    private String[] checkTtile = new String[10];
    public MainFragmentStatePagerAdapter(FragmentManager manager, final Context context, final java.util.List<Bundle> payloads, final String[] checkTtile) {
        super(manager);

        this.manager = manager;
        this.context = context;
        this.checkTtile = checkTtile;
        if (null != payloads) {
            this.payloads.addAll(payloads);
        }
    }

    public int getCount() { return payloads.size(); }
    public int getItemId(final int position) { return position; }
//  public int getItemPosition(final Object object) { return POSITION_NONE; }

    public Bundle getPayload(final int position) {
        if (position >= payloads.size()) return null;
        return payloads.get(position);
    }

    public void setPayloads(final java.util.List<Bundle> payloads) {
        this.payloads.clear();
        this.payloads.addAll(payloads);
        notifyDataSetChanged();
    }

    public Fragment getItemFromCache(final int position) {
        final WeakReference<Fragment> wf = cache.get(position);
        return null==wf?null:wf.get();
    }

    public Fragment getItem(final int position) {
        final Bundle payload = payloads.get(position);
//        Fragment fragment = getItemFromCache(position);
//        if (null == fragment) {
            Fragment fragment = (Fragment) Fragment.instantiate(context, payload.getString(FRAGMENT_CLAZZ_KEY), payload);
           // cache.put(position, new WeakReference<Fragment>(fragment));
//        }
        return fragment;
    }

    

    // for the indicator
    public CharSequence getPageTitle(final int position) {
        return checkTtile[position];
    }

    public String[] getTitles() {
        if (0 == payloads.size()) return new String[] { "" };

        final String[] titles = new String[payloads.size()];
        for (int i = 0; i < titles.length; i++) {
            titles[i] = payloads.get(i).getString(FRAGMENT_TITLE_KEY);
        }
        return titles;
    }

    /*
     * pulled from FragmentStatePagerAdapter.java
     * - capitalize on the fact that FSPA saves state as "f"+index
     */
    public void restoreState(Parcelable state, ClassLoader loader) {
        super.restoreState(state, loader);

        cache.clear();

        final Bundle bundle = (Bundle)state;
        bundle.setClassLoader(loader);
        final Parcelable[] fss = bundle.getParcelableArray("states");

        final Iterable<String> keys = bundle.keySet();
        for (final String key : keys) {
            if (!key.startsWith("f")) continue;

            final int position = Integer.parseInt(key.substring(1));
            final Fragment f = manager.getFragment(bundle, key);
            if (f != null) {
                // update our cache of fragments the same way
                cache.put(position, new WeakReference<Fragment>((Fragment) f));
            }
        }
    }



    public void updateTitle(String title, int position){
        checkTtile[position] = title;
        this.notifyDataSetChanged();
    }
}
