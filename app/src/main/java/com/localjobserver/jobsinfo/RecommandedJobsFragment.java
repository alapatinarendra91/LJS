package com.localjobserver.jobsinfo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.JobDescriptionActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link RecommandedJobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecommandedJobsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView jobsListView;
    private TextView noJobsText;
    private ProgressDialog progressDialog;
    public ArrayList<JobsData> jobList;
    private RecommendedjobsListAdpter commonArrayAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View rootView;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReecommandedJobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecommandedJobsFragment newInstance(String param1, String param2) {
        RecommandedJobsFragment fragment = new RecommandedJobsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public RecommandedJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_reecommanded_jobs, container, false);
        jobsListView = (ListView)rootView.findViewById(R.id.recommandedjobsList);
        noJobsText = (TextView)rootView.findViewById(R.id.noJobsText);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
//        getJobsData();
        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("jobItems", (ArrayList<JobsData>) jobList).putExtra("selectedPos", position).putExtra("_id", ((JobsData) jobList.get(position)).get_id()).setAction("fromRecommanded"));
                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("selectedPos", position).putExtra("_id", ((JobsData) jobList.get(position)).get_id()).setAction("fromRecommanded"));
            }
        });

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getJobsData();
                                    }
                                }
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getJobsData();
            }
        });

        return rootView;
    }

    public void getJobsData(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null && result.size() != 0) {
                    jobList = (ArrayList) result;
                    CommonUtils.jobList = (ArrayList) result;
                    commonArrayAdapter = new RecommendedjobsListAdpter(getActivity());
                    jobsListView.setAdapter(commonArrayAdapter);
                    progressDialog.dismiss();

                    Intent uiUpdateIntent = new Intent("update");
                    uiUpdateIntent.putExtra("type", 1);
                    uiUpdateIntent.putExtra("appliedCount", jobList.size());
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                }else {
                    progressDialog.dismiss();
                    noJobsText.setVisibility(View.VISIBLE);
                    jobsListView.setVisibility(View.GONE);
//                        Toast.makeText(getActivity(), "No Recommended results found", Toast.LENGTH_SHORT).show();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getRecommendedJobs, CommonUtils.getUserEmail(getActivity())));
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    class RecommendedjobsListAdpter extends BaseAdapter {

        Context context;
        public RecommendedjobsListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.jobs_list_row, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.positionNAME);
            jobTitle.setText(((JobsData) jobList.get(position)).getJobTitle() + "(" + ((JobsData) jobList.get(position)).getJobReference()+")");
            TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
            jobLoc.setText(Html.fromHtml("<b>Location: </b>" + (((JobsData) jobList.get(position)).getLocation())));
            TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
            jobComp.setText(Html.fromHtml("<b>Company Name: </b>"+ (((JobsData)jobList.get(position)).getCompanyName())
                    +"<br/>"+"<b>No.of Positions:</b>"+ ((JobsData) jobList.get(position)).getJobPositions()));
            TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
            String expStr = "( "+((JobsData)jobList.get(position)).getExpMin()+ "- "+((JobsData)jobList.get(position)).getExpMax() +"year(s))";
            jobExp.setText(""+expStr);
            TextView jobRole = (TextView)convertView.findViewById(R.id.roleJob);
            jobRole.setText(Html.fromHtml("<b>Role: </b> " + (((JobsData) jobList.get(position)).getRole()) + "<br/>" + "<b>CTC: </b> " + ((JobsData) jobList.get(position)).getMinSal() + "-" + ((JobsData) jobList.get(position)).getMaxSal() +"Lac(s)P.A"));
            TextView jobKeySkills = (TextView)convertView.findViewById(R.id.keySkills);
            jobKeySkills.setText(Html.fromHtml("<b>Keyskills: </b>"+(((JobsData)jobList.get(position)).getKeySkills())));
            TextView jobNotice = (TextView)convertView.findViewById(R.id.noticePeriod);
                // Seeker
                jobNotice.setText(Html.fromHtml("<b>Notice Period: </b>"+(((JobsData)jobList.get(position)).getNoticePeriod()+" days")+"<br/>"+"<b>Qualification:</b> "+((JobsData) jobList.get(position)).getQualification()
//                        +"<br/>"+"<b>Job Description:</b> "+((JobsData) jobList.get(position)).getJobDescription()
                        +"<br/>"+"<b><font color='#285e8e'>Applied:</font></b>"+((JobsData) jobList.get(position)).getAppliedCount()
                        +"<br/>"+"<b><font color='#285e8e'>Relevance:</font></b>"+((JobsData) jobList.get(position)).getMatchedScore()
                        +"<br/>"+"<b><font color='#285e8e'>Viewed:</font></b>"+ ((JobsData) jobList.get(position)).getViewsCount()));

            TextView postedDate = (TextView)convertView.findViewById(R.id.postedDate);
            postedDate.setText(Html.fromHtml("<b>Posted Date:</b>"+ CommonUtils.getCleanDate(((JobsData) jobList.get(position)).getPostDate())));

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return jobList.size();
        }


        @Override
        public long getItemId(int arg0) {

            return 0;// TODO Auto-generated method stub
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return jobList.get(position);
        }

    }


}
