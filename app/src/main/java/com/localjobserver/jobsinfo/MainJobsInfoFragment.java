package com.localjobserver.jobsinfo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.talentzing.R;
import com.localjobserver.ui.MainInfoActivity;
import com.localjobserver.viewpagerindicator.TabPageIndicator;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MainJobsInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainJobsInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ViewPager pager;
    private TabPageIndicator indicator;
    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;
    private View rootView;
    private MainFragmentStatePagerAdapter mStateAdapter;
    public static MainJobsInfoFragment newInstance(int param1,String type) {
        MainJobsInfoFragment fragment = new MainJobsInfoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    public MainJobsInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main_jobs_info2, container, false);


        pager = (ViewPager)rootView.findViewById(R.id.pager);
        indicator = (TabPageIndicator)rootView.findViewById(R.id.indicator);

        final List<Bundle> payloads = new java.util.ArrayList<Bundle>();
        String[] titles = new String[2];
        final Bundle topPayload = new Bundle();
        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, RecommandedJobsFragment.class.getName());
        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Recommended");
        payloads.add(topPayload);
        titles[0] = "Recommended";

        final Bundle tvShowsPayload = new Bundle();
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, AppliedJobsFragment.class.getName());
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Applied");
        payloads.add(tvShowsPayload);
        titles[1] = "Applied";
//
//        final Bundle tvShowsPayload1 = new Bundle();
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, SavedJobsFragment.class.getName());
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Saved");
//        payloads.add(tvShowsPayload1);

        mStateAdapter = new MainFragmentStatePagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), payloads, titles);


        pager.setAdapter(mStateAdapter);

        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(opcl);
        if(mParam2.length() > 0){
            pager.setCurrentItem(1);
            indicator.setCurrentItem(1);
        }else{
            pager.setCurrentItem(0);
            indicator.setCurrentItem(0);
        }

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("update"));

        return rootView;
    }


    private final ViewPager.OnPageChangeListener opcl = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {

        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            if (activity instanceof  MainInfoActivity){
                ((MainInfoActivity) activity).onSectionAttached("Jobs View");
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("update")) {
                    if (intent.getIntExtra("type", 0) != 0) {
                        int appliedCount = intent.getIntExtra("appliedCount", 0);
                        mStateAdapter.updateTitle("Recommended"+" ("+appliedCount+")" , 0);
                        indicator.notifyDataSetChanged();
                    } else {
                        int appliedCount = intent.getIntExtra("appliedCount", 0);
                        mStateAdapter.updateTitle("Applied"+" ("+appliedCount+")" , 1);
                        indicator.notifyDataSetChanged();
                    }
                }
            }
        }
    };

}
