package com.localjobserver.jobsinfo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import co.talentzing.R;
import com.localjobserver.viewpagerindicator.TabPageIndicator;

import java.util.List;

public class MainJobsInfoActivity extends FragmentActivity {
    private ViewPager pager;
    private TabPageIndicator indicator;
    private MainFragmentStatePagerAdapter mStateAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_jobs_info);

        pager = (ViewPager) findViewById(R.id.pager);
        indicator = (TabPageIndicator) findViewById(R.id.indicator);

        final List<Bundle> payloads = new java.util.ArrayList<Bundle>();
        String[] titles = new String[2];
        final Bundle topPayload = new Bundle();
        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, AppliedJobsFragment.class.getName());
//        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Applied");
        payloads.add(topPayload);
        titles[0] = "Applied";
        final Bundle tvShowsPayload = new Bundle();
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, RecommandedJobsFragment.class.getName());
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Recommended");
        payloads.add(tvShowsPayload);
        titles[1] = "Recommand";
//        final Bundle tvShowsPayload1 = new Bundle();
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, SavedJobsFragment.class.getName());
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Saved");
//        payloads.add(tvShowsPayload1);

        mStateAdapter = new MainFragmentStatePagerAdapter(this.getSupportFragmentManager(), getApplicationContext(), payloads, titles);
        pager.setAdapter(mStateAdapter);

        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(opcl);
        indicator.setCurrentItem(0);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("update"));
    }


    private final ViewPager.OnPageChangeListener opcl = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_jobs_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {


            }
        }
    };

}
