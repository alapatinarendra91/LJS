package com.localjobserver.onlineTrainers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.TrainingInstitute.InstitutionNavigationActivity;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.TrainerRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class RegisterOnlineTrainer extends ActionBarActivity {
    private ActionBar actionBar = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_institute_registratiojn);


        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (getIntent().getStringExtra("POST")== null)
            actionBar.setTitle("UpdateProfile");
        else
            actionBar.setTitle("Trainer Registration Form");

        if (savedInstanceState == null) {
            Fragment itemFrag = new OnlineTrainingInstituteRegistratiojnFragment();
            itemFrag.setArguments(getIntent().getBundleExtra("flag"));
            getSupportFragmentManager().beginTransaction().add(R.id.container, itemFrag).commit();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class OnlineTrainingInstituteRegistratiojnFragment extends Fragment {

        private View flipper;
        private Button choose_image,submitbtn,cancilbtnbtn;
        private EditText business_NameEdt,contactnameEdt,emailEdt,passwordEdt,confirmpasswordEdt,contactnumberEdt,landlineEdt,addressEdt;
        private MultiAutoCompleteTextView keySkillsEdt;
        private AutoCompleteTextView localityEdit;
        private Spinner locationSpin;
        private ImageView institutelogoimgv,eye_image;
        private CheckBox emailChk, SMSview,Chatview, noneView,chk_agree;
        private LinearLayout dailyStatus, selectTime, timeStatus,profileAlerts_lay,agreeLay,pwd_txt_lyt,c_pwd_lyt;
        private RelativeLayout image_lay;
        private TextView ex_pwd_txt;
        private RadioButton radioDaily, radioWeekly, radioInstant;
        private RadioGroup radioTimeStatus,radioStatus;
        private Spinner daysSpin;
        private boolean take_photo = false;
        private static final int SELECT_PICTURE =1,FROMCAM = 2;
        private byte[] seekerImageData;
        private ProgressDialog progressDialog;
        private String selectedDay = "", compressedStr = "";
        private SharedPreferences preferences;
        private TrainerRegistrationModel mDataModel;
        private int flag;
        public List<TrainerRegistrationModel> trainerProfileList;
        private String selectedStr;
        private Bundle bundlePosition;
        private String ConfigUrl;
        private ArrayList<HashMap> morderHashMapList;
        private LinkedHashMap<String, String> locationsMap;
        private ArrayList<String> keySkillsList;
        private boolean eye_var = false;

        public OnlineTrainingInstituteRegistratiojnFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            flipper = inflater.inflate(R.layout.activity_register_online_trainer, container, false);

            initViews();
            setViews();
            getLocations();
            getKeyWords();
            bundlePosition = getArguments();
            flag = Integer.parseInt(bundlePosition.getString("flag"));
            //Flag ==1 Update Profile
            if (flag == 1)
                getTrainerData();

            return flipper;

}

        private void initViews() {
            progressDialog = new ProgressDialog(getActivity());
            mDataModel = new TrainerRegistrationModel();
            preferences = getActivity().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);

            business_NameEdt  = (EditText)flipper.findViewById(R.id.business_NameEdt);
            contactnameEdt  = (EditText)flipper.findViewById(R.id.contactnameEdt);
            emailEdt  = (EditText)flipper.findViewById(R.id.emailEdt);
            passwordEdt  = (EditText)flipper.findViewById(R.id.passwordEdt);
            confirmpasswordEdt  = (EditText)flipper.findViewById(R.id.confirmpasswordEdt);
            contactnumberEdt  = (EditText)flipper.findViewById(R.id.contactnumberEdt);
            landlineEdt  = (EditText)flipper.findViewById(R.id.landlineEdt);
            addressEdt  = (EditText)flipper.findViewById(R.id.addressEdt);
            keySkillsEdt  = (MultiAutoCompleteTextView)flipper.findViewById(R.id.keySkillsEdt);
            keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            locationSpin  = (Spinner)flipper.findViewById(R.id.locationSpin);
            localityEdit  = (AutoCompleteTextView)flipper.findViewById(R.id.localityEdit);
            ex_pwd_txt  = (TextView)flipper.findViewById(R.id.ex_pwd_txt);

            choose_image  = (Button)flipper.findViewById(R.id.choose_image);
            submitbtn  = (Button)flipper.findViewById(R.id.submitbtn);
            cancilbtnbtn  = (Button)flipper.findViewById(R.id.cancilbtnbtn);

            institutelogoimgv  = (ImageView)flipper.findViewById(R.id.institutelogoimgv);
            eye_image = (ImageView) flipper.findViewById(R.id.eye_image);

            emailChk = (CheckBox)flipper.findViewById(R.id.ac_email);
            SMSview = (CheckBox)flipper.findViewById(R.id.ac_sms);
            Chatview = (CheckBox)flipper.findViewById(R.id.ac_chat);
            noneView = (CheckBox)flipper.findViewById(R.id.ac_none);
            chk_agree = (CheckBox)flipper.findViewById(R.id.chk_agree);

            dailyStatus = (LinearLayout) flipper.findViewById(R.id.dailyStatus);
            selectTime = (LinearLayout) flipper.findViewById(R.id.selectTime);
            timeStatus = (LinearLayout) flipper.findViewById(R.id.timeStatus);
            profileAlerts_lay = (LinearLayout) flipper.findViewById(R.id.profileAlerts_lay);
            agreeLay = (LinearLayout) flipper.findViewById(R.id.agreeLay);
            pwd_txt_lyt = (LinearLayout) flipper.findViewById(R.id.pwd_txt_lyt);
            c_pwd_lyt = (LinearLayout) flipper.findViewById(R.id.c_pwd_lyt);
            image_lay = (RelativeLayout) flipper.findViewById(R.id.image_lay);

            radioDaily = (RadioButton) flipper.findViewById(R.id.radioDaily);
            radioWeekly = (RadioButton) flipper.findViewById(R.id.radioWeekly);
            radioInstant = (RadioButton) flipper.findViewById(R.id.radioInstant);
            radioTimeStatus  = (RadioGroup)flipper.findViewById(R.id.radioTimeStatus);
            radioStatus  = (RadioGroup)flipper.findViewById(R.id.radioStatus);
            daysSpin = (Spinner) flipper.findViewById(R.id.daytimeSpin);
            radioDaily.setOnClickListener(operationListener);
            radioWeekly.setOnClickListener(operationListener);
            radioInstant.setOnClickListener(operationListener);


        }

        private void setViews() {

            daysSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));

            daysSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDay = parent.getSelectedItem().toString();
                    if (parent.getSelectedItemId() != 0)
                        timeStatus.setVisibility(View.VISIBLE);
                    else
                        timeStatus.setVisibility(View.GONE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            eye_image.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (eye_var == false) {
                        eye_var = true;
                        eye_image.setImageResource(R.drawable.eye_hidden);
                        passwordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        confirmpasswordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        passwordEdt.setSelection(passwordEdt.getText().length());
                        confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                    } else {
                        eye_var = false;
                        eye_image.setImageResource(R.drawable.eye_show);
                        passwordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        confirmpasswordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        passwordEdt.setSelection(passwordEdt.getText().length());
                        confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                    }
                }
            });

            emailChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (emailChk.isChecked()) {
                        dailyStatus.setVisibility(View.VISIBLE);
                        timeStatus.setVisibility(View.GONE);
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                    } else {
                        dailyStatus.setVisibility(View.GONE);
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                    }
                }
            });

            noneView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    emailChk.setChecked(false);
                    SMSview.setChecked(false);
                    Chatview.setChecked(false);
                    timeStatus.setVisibility(View.GONE);
                    dailyStatus.setVisibility(View.GONE);
                    selectTime.setVisibility(View.GONE);
                    if (noneView.isChecked())
                        noneView.setChecked(false);
                    else
                        noneView.setChecked(true);
                }
            });

            SMSview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    noneView.setChecked(false);
                }
            });
            Chatview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    noneView.setChecked(false);
                }
            });

            choose_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profilePic();
                }
            });

            submitbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (flag == 1) {
                        if (validateTrainerRegister() == true && validateMobileNo() == true ) {
                            setTrainingData();
                            uploadUsersData(toUpload());

                        }

                    } else {


                        if (validateTrainerRegister() == true && validateMobileNo() == true) {
                            if (validatePassword().equals("")) {
                                if (take_photo) {
                                    if (chk_agree.isChecked()) {

                                        setTrainingData();
                                        uploadUsersData(toUpload());

                                    } else {
                                        Toast.makeText(getActivity(), "Please agree terms & Conditions", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Select Institute Logo", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getActivity(), "Check password Confirm password", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                }
            });

            cancilbtnbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();

                }
            });

        }

        public void getKeyWords(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }

                    if (result != null) {
                        if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()) {
//                            keySkillsEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                            keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(), keySkillsList));
                        }
                        progressDialog.dismiss();
                    }
                }
            });
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {

                            }
                        }
                    });
                }
            });

        }

        public void getLocations() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        locationsMap = (LinkedHashMap<String, String>) result;
                        if (null != locationsMap && null != locationSpin) {
                            locationSpin.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                            localityEdit.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                            if (flag == 1) {
                                String[] jobType_array = CommonUtils.fromMap(locationsMap, "Location");
                                locationSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), jobType_array));
                                for (int i = 0; i < jobType_array.length; i++) {
                                    if (jobType_array[i].contains(trainerProfileList.get(0).getLocation())) {
                                        locationSpin.setSelection(i);
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Not able to get locations", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }
        private void getTrainerData() {
            trainerProfileList = DatabaseAccessObject.getTrainerProfile(Queries.getInstance().getDetails("TrainersRegistration"), getActivity());
            business_NameEdt.setEnabled(false);
            emailEdt.setEnabled(false);

            business_NameEdt.setText("" + trainerProfileList.get(0).getInstituteName());
            localityEdit.setText("" + trainerProfileList.get(0).getLocality());
            contactnameEdt.setText(""+trainerProfileList.get(0).getFirstName());
            emailEdt.setText(""+trainerProfileList.get(0).getEmail());
            passwordEdt.setText("" + trainerProfileList.get(0).getPassword());
            confirmpasswordEdt.setText("" + trainerProfileList.get(0).getConfirmPassword());
            contactnumberEdt.setText("" + trainerProfileList.get(0).getContactNo());
            landlineEdt.setText("" + trainerProfileList.get(0).getContactNoLandline());
            addressEdt.setText("" + trainerProfileList.get(0).getAddress());
            keySkillsEdt.setText("" + trainerProfileList.get(0).getCoursesOffer());

            agreeLay.setVisibility(View.GONE);
            c_pwd_lyt.setVisibility(View.GONE);
            pwd_txt_lyt.setVisibility(View.GONE);
            ex_pwd_txt.setVisibility(View.GONE);
            profileAlerts_lay.setVisibility(View.GONE);
            image_lay.setVisibility(View.GONE);
            eye_image.setVisibility(View.GONE);
            submitbtn.setText("Update");

//            byte[]  byteArray=trainerProfileList.get(0).getPhoto();
//            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//            if(bmp!=null){
//                institutelogoimgv.setImageBitmap(bmp);
//            }

        }

        public boolean validateTrainerRegister(){
            Form mForm = new Form(getActivity());
            mForm.addField(Field.using(business_NameEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(contactnameEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(confirmpasswordEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(contactnumberEdt).validate(NotEmpty.build(getActivity())));
//            mForm.addField(Field.using(landlineEdt).validate(NotEmpty.build(getActivity())));
//            mForm.addField(Field.using(addressEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(keySkillsEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(localityEdit).validate(NotEmpty.build(getActivity())));
            if (mForm.isValid())
            if (CommonUtils.spinnerSelect("Location", locationSpin.getSelectedItemPosition(), getActivity()))
                return (mForm.isValid()) ? true : false;

            return (mForm.isValid()) ? false : false;
        }

        public boolean validateMobileNo(){

            if (CommonUtils.isValidMobile(contactnumberEdt.getText().toString(), contactnumberEdt))
                return true;
            else{
                CommonUtils.showToast("Please enter valid Mobile No",getActivity());
                return false;

            }


        }

        public String validatePassword(){

            if (!(passwordEdt.getText().toString().toLowerCase().equalsIgnoreCase(confirmpasswordEdt.getText().toString().toLowerCase()))){
                return "Password and confirm password is need to be same";
            }else {
                return "";
            }

        }

        View.OnClickListener operationListener = new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                switch (v.getId()) {


                    case R.id.radioDaily:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioWeekly:
                        selectTime.setVisibility(View.VISIBLE);
                        timeStatus.setVisibility(View.VISIBLE);

                        break;
                    case R.id.radioInstant:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                        break;

                }
            }
        };

        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
                        //selectImage();
                        // Calls an Intent that opens camera to take a picture
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, FROMCAM);
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        //Call an intent that opens gallery to select photo
                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            take_photo = true;
            if ((requestCode == 0) && (resultCode == -1)) {

            }else if (requestCode == FROMCAM) {
                if (null == data)
                    return;
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                Matrix matrix = new Matrix();
                matrix.postRotate(CommonUtils.getImageOrientation(f.getAbsolutePath()));
                Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(),yourSelectedImage.getHeight(), matrix, true);
                Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                institutelogoimgv.setImageDrawable(dr);
                institutelogoimgv.invalidate();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60,outputStream);
                seekerImageData = outputStream.toByteArray();
//                mDataModel.setPhoto(seekerImageData);

            } else if (requestCode == SELECT_PICTURE){
                if (null == data)
                    return;
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(filePath, 80, 80);
                Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90,outputStream);
                seekerImageData = outputStream.toByteArray();
                institutelogoimgv.setImageDrawable(dr);
                institutelogoimgv.invalidate();
//                mDataModel.setPhoto(seekerImageData);
            }// for if
        }

        Bitmap ShrinkBitmap(String file, int width, int height){

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

            int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
            int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

            if (heightRatio > 1 || widthRatio > 1)
            {
                if (heightRatio > widthRatio)
                {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
            return bitmap;
        }

        public void setTrainingData() {
            mDataModel.setFirstName(contactnameEdt.getText().toString());
            mDataModel.setLastName("");
            mDataModel.setPassword(passwordEdt.getText().toString());
            mDataModel.setConfirmPassword(passwordEdt.getText().toString());
            mDataModel.setEmail(emailEdt.getText().toString());
            mDataModel.setContactNo(contactnumberEdt.getText().toString());
            mDataModel.setInstituteName(business_NameEdt.getText().toString());
            mDataModel.setLocality(localityEdit.getText().toString());
//            mDataModel.setContactNoLandline(landlineEdt.getText().toString());
            mDataModel.setUpdateOn(CommonUtils.getDateTime());

//            if (take_photo)
//                mDataModel.setPhotoString(Base64.encodeToString(seekerImageData, Base64.DEFAULT));
//            else{
//                if (flag == 1)
//                    mDataModel.setPhotoString(trainerProfileList.get(0).getPhotoString());
//                else
//                    mDataModel.setPhotoString("");
//            }
            mDataModel.setPhotoString("");
            mDataModel.setLocation(locationSpin.getSelectedItem().toString());
            mDataModel.setAddress(addressEdt.getText().toString());
            mDataModel.setUpdateOn(CommonUtils.getDateTime());
            mDataModel.setCoursesOffer(keySkillsEdt.getText().toString());
            mDataModel.setStdCourseName(keySkillsEdt.getText().toString());
            mDataModel.setDTId(0);
            mDataModel.setDTType("Trainer");

            if (flag == 1){
                mDataModel.setTrainerStatus(true);
                mDataModel.setEmailVerified(true);
                mDataModel.setIsOnline(true);
            }else {
                mDataModel.setTrainerStatus(false);
                mDataModel.setEmailVerified(false);
                mDataModel.setIsOnline(false);
            }
        }

        public HashMap toUpload(){
            HashMap recruiterRegDataMap = new HashMap();
            morderHashMapList = new ArrayList<HashMap>();
            recruiterRegDataMap.put("FirstName",mDataModel.getFirstName());
            recruiterRegDataMap.put("LastName",mDataModel.getLastName());
            recruiterRegDataMap.put("Password",mDataModel.getPassword());
            recruiterRegDataMap.put("ConfirmPassword", mDataModel.getConfirmPassword());
            recruiterRegDataMap.put("BusinessName", mDataModel.getInstituteName());
            recruiterRegDataMap.put("Email", mDataModel.getEmail());
            recruiterRegDataMap.put("ContactNo", mDataModel.getContactNo());
//            recruiterRegDataMap.put("ContactNo_Landline", mDataModel.getContactNoLandline());
            recruiterRegDataMap.put("Location", mDataModel.getLocation());
            recruiterRegDataMap.put("Locality", mDataModel.getLocality());
            recruiterRegDataMap.put("Address", mDataModel.getAddress());
            recruiterRegDataMap.put("photoString", mDataModel.getPhotoString());
            recruiterRegDataMap.put("UpdateOn", mDataModel.getUpdateOn());
            recruiterRegDataMap.put("CoursesOffer", mDataModel.getCoursesOffer());
            recruiterRegDataMap.put("EmailVerified", mDataModel.getEmailVerified());
            recruiterRegDataMap.put("StdCourseName", mDataModel.getStdCourseName());
            recruiterRegDataMap.put("IsOnline", mDataModel.getIsOnline());
            recruiterRegDataMap.put("DTId", mDataModel.getDTId());
            recruiterRegDataMap.put("DTType", mDataModel.getDTType());
            recruiterRegDataMap.put("TrainerStatus", mDataModel.getTrainerStatus());
            recruiterRegDataMap.put("Photo", mDataModel.getPhoto());
            morderHashMapList.add(recruiterRegDataMap);
            return recruiterRegDataMap;
        }

        public void uploadUsersData(HashMap<String,String> input){
            if (flag == 1)
                progressDialog.setMessage("Update user data..");
            else
                progressDialog.setMessage("Register user data..");
            progressDialog.setCancelable(false);
            progressDialog.show();

            JSONObject json = new JSONObject(input);
            String requestString = json.toString();
            requestString = CommonUtils.encodeURL(requestString);
            if (flag == 1)
                ConfigUrl = String.format(Config.LJS_BASE_URL + Config.modifyTrainerDetails,CommonUtils.getUserEmail(getActivity()), "" + requestString);
            else
                ConfigUrl = String.format(Config.LJS_BASE_URL + Config.registerOnlineTrainer, "" + requestString);

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), ""+result.toString(), Toast.LENGTH_SHORT).show();
                        if (result != null) {
//                            Toast.makeText(getActivity(), "Fail to Register Trainer", Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    if (result != null) {
                        if (result.toString().equalsIgnoreCase("true")) {
                            if (flag == 1) {
                                String whereCondition = " where Email='" + emailEdt.getText().toString().trim() + "'";
                                DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
                                dbHelper.updateData("TrainersRegistration", morderHashMapList, whereCondition, getActivity());

                                CommonUtils.showToast("Profile Updated Successfully",getActivity());
                                startActivity(new Intent(getActivity(),InstitutionNavigationActivity.class));
                                progressDialog.dismiss();
                            }
                            else{
                                uploadPrivacySettings();
                            }

                        }
                    }
                }
            }, ConfigUrl);
        }


        public void uploadPrivacySettings() {

            getTrainerconditions();

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null && result.toString().equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        uploadUserImage();

                    }else {
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.trainerPrivacySettings, emailEdt.getText().toString().trim(), selectedDay, selectedStr, emailChk.isChecked(), SMSview.isChecked()));

        }

        private void getTrainerconditions(){
            if (emailChk.isChecked()){
                int selectedId = radioTimeStatus.getCheckedRadioButtonId();
                int selectedId_status = radioStatus.getCheckedRadioButtonId();
                selectedStr = "";
                RadioButton timebutton = (RadioButton)flipper.findViewById(selectedId);
                RadioButton timebutton_status = (RadioButton)flipper.findViewById(selectedId_status);

                if (null != timebutton){
                    selectedStr = timebutton.getText().toString();
                    com.localjobserver.networkutils.Log.i("", "time is : " + selectedStr);
                }else {
                    selectedStr = "";
                }


                if (selectTime.getVisibility() == View.VISIBLE){

                }else{
                    selectedDay = timebutton_status.getText().toString();
                }


            }else {

            }



        }




      /*  public void uploadPrivacySettings() {

//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();

            JSONObject json = new JSONObject(privacyData());
            String requestString = json.toString();
            requestString = CommonUtils.encodeURL(requestString);

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
//                    progressDialog.dismiss();
                        uploadUserImage();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.alertsJobSeeker, "" + requestString));

        }*/

        public LinkedHashMap privacyData() {

            int selectedId = radioTimeStatus.getCheckedRadioButtonId();
            String selectedStr = "";
            RadioButton timebutton = (RadioButton)flipper.findViewById(selectedId);
            if (null != timebutton){
                selectedStr = timebutton.getText().toString();
            }else {
                selectedStr = "";
            }
            LinkedHashMap settingsDataMap = new LinkedHashMap();
            settingsDataMap.put("Email", "" + emailEdt.getText().toString());
            if (null != selectedDay && selectedDay.length() > 0){
                settingsDataMap.put("WeekDay", selectedDay);
            }else{
                settingsDataMap.put("WeekDay", "Daily");
            }

            settingsDataMap.put("OnDemandTime", selectedStr);
            settingsDataMap.put("EmailAlert", "" + emailChk.isChecked());
            settingsDataMap.put("SmsAlert", "" + SMSview.isChecked());
            settingsDataMap.put("ChatAlert", "" + Chatview.isChecked());
            settingsDataMap.put("PremiumJobSeekerService", "false");
            settingsDataMap.put("LjsCommunication", "false");
            return settingsDataMap;
        }

        public void uploadUserImage() {

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
//                progressDialog.show();
            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    Log.i("", "Result is : " + result.toString());
                    if (result != null && result.toString().contains("true")) {
                        progressDialog.dismiss();

                        if (preferences.getString(CommonUtils.getUserEmail(getActivity()), null) == null) {
                            designSuccessDialog();
                        }
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);

                    }
                }
            }, "" + emailEdt.getText().toString() + ".png", emailEdt.getText().toString(), compressedStr, "uploadTrainerImage", getActivity(), 0);

        }

       /* public void  uploadUserImage(){

//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Uploading user data..");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    Log.i("", "Result is : " + result.toString());
                    if (result != null && result.toString().contains("true")) {
                        progressDialog.dismiss();

                        if (preferences.getString(CommonUtils.getUserEmail(getActivity()), null) == null) {
                            designSuccessDialog();
                        }
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                        editor.commit();

                    }
                }
            }, "" + emailEdt.getText().toString() + ".png", emailEdt.getText().toString(), compressedStr, getActivity(), 0);

        }*/

        Dialog dialog;
        public void designSuccessDialog(){
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.common_dialog);
            dialog.setCanceledOnTouchOutside(true);
            TextView msgView = (TextView) dialog.findViewById(R.id.msgTxt);
            TextView titleHeader = (TextView) dialog.findViewById(R.id.titleHeader);
            titleHeader.setText("Confirmation");
            msgView.setText("Thank you for registering to Local Job Server ");
            dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    startActivity(new Intent(getActivity(), LoginScreen.class));
                    getActivity().finish();
                }
            });

            dialog.show();
        }

    }

}
