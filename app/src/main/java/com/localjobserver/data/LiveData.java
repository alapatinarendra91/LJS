package com.localjobserver.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.freshers.ElearningsModel;
import com.localjobserver.freshers.TopInstitutes;
import com.localjobserver.helper.SortBasedOnId;
import com.localjobserver.models.FresharLocalCourses;
import com.localjobserver.models.FresharsCoursesModel;
import com.localjobserver.models.JobsData;
import com.localjobserver.models.LoadCandidatesModel;
import com.localjobserver.models.LocalJobsModel;
import com.localjobserver.models.LocalitiesWithLatLngModel;
import com.localjobserver.models.PotentialModel;
import com.localjobserver.models.PrivacySettingsModel;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.models.SearchResultsServiceModel;
import com.localjobserver.models.TrainerPostCourseModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by admin on 25-04-2015.
 */
public class LiveData {

    public static ArrayList<JobsData> jobList;
    public static ArrayList<TrainerPostCourseModel> TrainerPostCourseModel;
    public static ArrayList<FresharsCoursesModel> CoursesFresharsList;
    public static ArrayList<SearchResultsServiceModel> resumesServiceModelArrayList;
    public static ArrayList<RecommendedCourses> courcesServiceModelArrayList;
    public static ArrayList<PotentialModel> PotentialModelArrayList;
    public static ArrayList<FresharLocalCourses> fresharcourcesServiceModelArrayList;

    public static void getJobDetails(final ApplicationThread.OnComplete<List<JobsData>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getJobs...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (result != null && ((String)result).toString().length() > 0) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                            mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            try {
                                jobList = mapper.readValue(result.toString(), new TypeReference<List<JobsData>>() {
                                });
                                oncomplete.execute(true, jobList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        } else {
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

 public static void getAppliedCandidatesDetails(final ApplicationThread.OnComplete<List<LoadCandidatesModel>> oncomplete, final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getJobs...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (result != null && ((String)result).toString().length() > 0) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                            mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            try {
//                               ArrayList<LoadCandidatesModel> LoadCandidatesList = mapper.readValue(result.toString(), new TypeReference<List<LoadCandidatesModel>>() {
//                                });
//                                oncomplete.execute(true, LoadCandidatesList, null);

                                Type collectionType = new TypeToken<Collection<LoadCandidatesModel>>(){}.getType();
                                Gson googleJson = new Gson();
                                List<LoadCandidatesModel> LoadCandidatesList = googleJson.fromJson((String) result,collectionType);
                                oncomplete.execute(true, LoadCandidatesList, null);


                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        } else {
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getCoursePostDetails(final ApplicationThread.OnComplete<List<RecommendedCourses>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getJobs...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (result != null && ((String)result).toString().length() > 0) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                            mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            try {
//                                TrainerPostCourseModel = mapper.readValue(result.toString(), new TypeReference<List<TrainerPostCourseModel>>() {
//                                });
                                Type collectionType = new TypeToken<Collection<RecommendedCourses>>(){}.getType();
                                Gson googleJson = new Gson();
                                List<RecommendedCourses> TrainerPostCourseModel = googleJson.fromJson((String) result,collectionType);
                                oncomplete.execute(true, TrainerPostCourseModel, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        } else {
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getCourses_fresharDetails(final ApplicationThread.OnComplete<List<FresharsCoursesModel>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getCourses...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (result != null && ((String)result).toString().length() > 0) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                            mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            try {
                                CoursesFresharsList = mapper.readValue(result.toString(), new TypeReference<List<FresharsCoursesModel>>() {

                                });
                                oncomplete.execute(true, CoursesFresharsList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        } else {
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }


    public static void sendMessage(final String userid, final HashMap<String,String> values, final ApplicationThread.OnComplete<String> onComplete)
    {
        ApplicationThread.bgndPost(LiveData.class.getName(),"seller registration",new Runnable() {
            @Override
            public void run() {
                HttpClient.download(String.format(Config.LJS_BASE_URL + "", userid),false, new ApplicationThread.OnComplete<String>() {
                    @Override
                    public void execute(boolean success, String result, String msg) {
                        if (success) {
                            try {
                                onComplete.execute(success, result.toString(), msg);
                            } catch (Exception e) {
                                e.printStackTrace();
                                onComplete.execute(success, null, msg);
                            }
                        } else
                            onComplete.execute(success, null, msg);
                    }
                });
            }
        });

    }

    public static void getResumesFromServer(final ApplicationThread.OnComplete<List<SearchResultsServiceModel>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getResumes...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        if (result != null && ((String)result).toString().length() > 0) {
                            try {
                                resumesServiceModelArrayList = mapper.readValue(result.toString(), new TypeReference<List<SearchResultsServiceModel>>() {
                                });
                                Log.v(LiveData.class.getSimpleName(),"Resumes size.."+resumesServiceModelArrayList.size());
                                oncomplete.execute(true, resumesServiceModelArrayList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        }else {
                            resumesServiceModelArrayList = new ArrayList<SearchResultsServiceModel>();
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(true, resumesServiceModelArrayList, "No results found");
                        }
                    }
                });
            }
        });
    }

    public static void getfresherLocalCourses(final ApplicationThread.OnComplete<List<FresharLocalCourses>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getRecommendedCourses...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        if (result != null && ((String)result).toString().length() > 0) {
                            try {
                                Type collectionType = new TypeToken<Collection<FresharLocalCourses>>(){}.getType();
                                Gson googleJson = new Gson();
                                List<FresharLocalCourses> courcesServiceModelArrayList = googleJson.fromJson((String) result,collectionType);

//                                courcesServiceModelArrayList = mapper.readValue(result.toString(), new TypeReference<List<RecommendedCourses>>() {
//                                });
                                Log.v(LiveData.class.getSimpleName(),"getRecommendedCourses size.."+courcesServiceModelArrayList.size());
                                oncomplete.execute(true, courcesServiceModelArrayList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        }else {
                            fresharcourcesServiceModelArrayList = new ArrayList<FresharLocalCourses>();
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(true, fresharcourcesServiceModelArrayList, "No results found");
                        }
                    }
                });
            }
        });
    }

    public static void getRecommendedCourses(final ApplicationThread.OnComplete<List<RecommendedCourses>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getRecommendedCourses...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        if (result != null && ((String)result).toString().length() > 0) {
                            try {
                                Type collectionType = new TypeToken<Collection<RecommendedCourses>>(){}.getType();
                                Gson googleJson = new Gson();
                                List<RecommendedCourses> courcesServiceModelArrayList = googleJson.fromJson((String) result,collectionType);

//                                courcesServiceModelArrayList = mapper.readValue(result.toString(), new TypeReference<List<RecommendedCourses>>() {
//                                });
                                Log.v(LiveData.class.getSimpleName(),"getRecommendedCourses size.."+courcesServiceModelArrayList.size());
                                oncomplete.execute(true, courcesServiceModelArrayList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        }else {
                            courcesServiceModelArrayList = new ArrayList<RecommendedCourses>();
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(true, courcesServiceModelArrayList, "No results found");
                        }
                    }
                });
            }
        });
    }

    public static void getPotentials(final ApplicationThread.OnComplete<List<PotentialModel>> oncomplete,final String url) {
        ApplicationThread.bgndPost(LiveData.class.getName(), "getPotentialModel...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        if (result != null && ((String)result).toString().length() > 0) {
                            try {
                                Type collectionType = new TypeToken<Collection<PotentialModel>>(){}.getType();
                                Gson googleJson = new Gson();
                                List<PotentialModel> courcesServiceModelArrayList = googleJson.fromJson((String) result,collectionType);

//                                courcesServiceModelArrayList = mapper.readValue(result.toString(), new TypeReference<List<PotentialModel>>() {
//                                });
                                Log.v(LiveData.class.getSimpleName(),"getPotentialModel size.."+courcesServiceModelArrayList.size());
                                oncomplete.execute(true, courcesServiceModelArrayList, null);
                            } catch (Exception ex) {

                                ex.printStackTrace();
                                oncomplete.execute(false, null, null);
                            }
                        }else {
                            PotentialModelArrayList = new ArrayList<PotentialModel>();
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(true, PotentialModelArrayList, "No results found");
                        }
                    }
                });
            }
        });
    }

    public static void getGenericPairValues(final ApplicationThread.OnComplete oncomplete,final String url,final String[] arrKeys){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        LinkedHashMap<String,String> genericDetails = new LinkedHashMap<String, String>();
                        ArrayList<String> industry_list=new ArrayList<String>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
//                                genericDetails.put("0", "Select");
                                for (int i=0;i<len;i++){
                                    jArray.getJSONObject(i);

                                    genericDetails.put(jArray.getJSONObject(i).getString(arrKeys[0]), jArray.getJSONObject(i).getString(arrKeys[1]));
                                }
                            }
                            oncomplete.execute(true, genericDetails, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getLocalityGenericPairValues(final ApplicationThread.OnComplete oncomplete,final String url,final String[] arrKeys){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        LinkedHashMap<String,String> genericDetails = new LinkedHashMap<String, String>();
                        ArrayList<String> industry_list=new ArrayList<String>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
//                                genericDetails.put("0", "Select");
                                for (int i=0;i<len;i++){
                                    jArray.getJSONObject(i);

                                    genericDetails.put(""+i, jArray.getJSONObject(i).getString(arrKeys[1]));
                                }
                            }
                            oncomplete.execute(true, genericDetails, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }


    public static void getIndusPairValues(final ApplicationThread.OnComplete oncomplete,final String url,final String[] arrKeys){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        LinkedHashMap<String,String> genericDetails = new LinkedHashMap<String, String>();
                        ArrayList<String> industry_list=new ArrayList<String>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
//                            jArray = getSortedList(jArray);
                            if (jArray != null) {
                                int len = jArray.length();
//                                genericDetails.put("0", "Select");
                                for (int i=0;i<len;i++){
                                    jArray.getJSONObject(i);

                                    genericDetails.put(jArray.getJSONObject(i).getString(arrKeys[0]), jArray.getJSONObject(i).getString(arrKeys[1]));
                                }
                            }
                            oncomplete.execute(true, genericDetails, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }


    public static JSONArray getSortedList(JSONArray array) throws JSONException {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            list.add(array.getJSONObject(i));
        }
        Collections.sort(list, new SortBasedOnId());

        JSONArray resultArray = new JSONArray(list);

        return resultArray;
    }

    public static void getFolders(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        ArrayList<String> foldersDetails = new ArrayList<String>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
                                for (int i=0;i<len;i++){
                                    foldersDetails.add(jArray.getString(i));
                                }
                            }
                            oncomplete.execute(true, foldersDetails, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getLocalJobsData(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        List<LocalJobsModel> arrLocalJobs = new ArrayList<LocalJobsModel>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }

                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
                        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
                        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, true);
                        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        try {
                            Gson googleJson = new Gson();
                            LocalJobsModel LocalJobsModel_= googleJson.fromJson(result.toString(), LocalJobsModel.class);
                            oncomplete.execute(true, LocalJobsModel_, null);
                        } catch (Exception ex) {

                            ex.printStackTrace();
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getElearnings(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "getElearnings...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        List<ElearningsModel> arrElearnings = new ArrayList<ElearningsModel>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
                                ElearningsModel mElearningsModel;
                                for (int i=0;i<len;i++){
                                    mElearningsModel = new ElearningsModel();
                                    JSONObject dataObj = jArray.getJSONObject(i);
                                    mElearningsModel.setCategory(dataObj.getString("Category"));
                                    mElearningsModel.setSubcategory(dataObj.getString("Subcategory"));
                                    mElearningsModel.setDescription(dataObj.getString("Description"));
                                    mElearningsModel.setUrl(dataObj.getString("Url"));
                                    mElearningsModel.setPostedBy(dataObj.getString("PostedBy"));
                                    mElearningsModel.setPostedDate(dataObj.getString("PostDate"));
                                    arrElearnings.add(mElearningsModel);
                                    mElearningsModel = null;
                                }
                            }
                            oncomplete.execute(true, arrElearnings, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }


    public static void getTopInstitutes(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "getTopInstitutes...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        List<TopInstitutes> arrTopInstitutions = new ArrayList<TopInstitutes>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
                                TopInstitutes mTopInstitutesModel;
                                for (int i=0;i<len;i++){
                                    mTopInstitutesModel = new TopInstitutes();
                                    JSONObject dataObj = jArray.getJSONObject(i);
                                    mTopInstitutesModel.setInstituteName(dataObj.getString("InstituteName"));
                                    mTopInstitutesModel.setCourseName(dataObj.getString("CourseName"));
                                    mTopInstitutesModel.setDate(dataObj.getString("Date"));
                                    mTopInstitutesModel.setRating(dataObj.getString("Rating"));
                                    mTopInstitutesModel.setTraineeEmail(dataObj.getString("TraineeEmail"));
                                    mTopInstitutesModel.setInstituteEmail(dataObj.getString("InstituteEmail"));
                                    mTopInstitutesModel.setLocation(dataObj.getString("Location"));
                                    mTopInstitutesModel.setCity(dataObj.getString("City"));
                                    arrTopInstitutions.add(mTopInstitutesModel);
                                    mTopInstitutesModel = null;
                                }
                            }
                            oncomplete.execute(true, arrTopInstitutions, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void uploadVenueDetails(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        try {
                            if (result.toString().replace("\"","").equalsIgnoreCase("true") || result.toString().contains("successfully")){
                                oncomplete.execute(true, true, null);
                            }else
                                oncomplete.execute(false, false, null);

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                        }
                    }
                });
            }
        });
    }

    public static void uploadRecruiterRegistrationDetails(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null || result.toString().equalsIgnoreCase("\"Already registered\"")) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {

                            if (result.toString().equalsIgnoreCase("true") || result.toString().equalsIgnoreCase("200")) {
                                oncomplete.execute(true, result, null);
                            } else
                                oncomplete.execute(false, result, null);

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                        }
                    }
                });
            }
        });
    }

    public static void uploadRegistrationDetails(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {

                            if (result.toString().equalsIgnoreCase("true") || result.toString().equalsIgnoreCase("200") ||
                                    result.toString().equalsIgnoreCase("\"Your password has updated successfully..\"")) {
                                oncomplete.execute(true, result, null);
                            } else
                                oncomplete.execute(false, result, null);

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                        }
                    }
                });
            }
        });
    }

    public static void uploadPostJobDetails(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {

                            if (result.toString().contains("jobid") || result.toString().equalsIgnoreCase("true") || result.toString().equalsIgnoreCase("200")) {
                                oncomplete.execute(true, result, null);
                            } else
                                oncomplete.execute(false, result, null);

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                        }
                    }
                });
            }
        });
    }

    public static void checkEmilExist(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {

                            if (result.toString().length() > 5) {
                                oncomplete.execute(false, result, null);
                            } else
                                oncomplete.execute(true, false, null);

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                        }
                    }
                });
            }
        });
    }

    public static void getLocalitiesWithLatLngModelDetails(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        List<LocalitiesWithLatLngModel> arrLocalitiesWithLatLngJobs = new ArrayList<LocalitiesWithLatLngModel>();
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
                                LocalitiesWithLatLngModel mLocalitiesWithLatLngModel;
                                for (int i=0;i<len;i++){
                                    mLocalitiesWithLatLngModel = new LocalitiesWithLatLngModel();
                                    JSONObject dataObj = jArray.getJSONObject(i);
                                    mLocalitiesWithLatLngModel.setCountry(dataObj.getString("Country"));
                                    mLocalitiesWithLatLngModel.setCity(dataObj.getString("City"));
                                    mLocalitiesWithLatLngModel.setLocation(dataObj.getString("Location"));
                                    mLocalitiesWithLatLngModel.setLongitude(dataObj.getString("Longitude"));
                                    mLocalitiesWithLatLngModel.setLatitude(dataObj.getString("Latitude"));
                                    arrLocalitiesWithLatLngJobs.add(mLocalitiesWithLatLngModel);
                                    mLocalitiesWithLatLngModel = null;
                                }
                            }
                            oncomplete.execute(true, arrLocalitiesWithLatLngJobs, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

//    public static void getLocalitiesWithLatLngModelDetails(final ApplicationThread.OnComplete oncomplete,final String url){
//        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
//            @Override
//            public void run() {
//                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
//                    public void run() {
//                        if (success == false || result == null) {
//                            oncomplete.execute(false, null, null);
//                            return;
//                        }
//                        try {
//
//                            oncomplete.execute(true, result, null);
//
//                        } catch (Exception e) {
//                            Message messageToParent = new Message();
//                            messageToParent.what = 0;
//                            Bundle bundleData = new Bundle();
//                            bundleData.putString("Time Over", "Lost Internet Connection");
//                            messageToParent.setData(bundleData);
//                        }
//                    }
//                });
//            }
//        });
//    }


    public static void uploadRegistrationDetailsWithHeader(final ApplicationThread.OnComplete oncomplete,final String url, final String data){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.sendGet(url, data, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null ||  !result.toString().equalsIgnoreCase("true")) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {
                            oncomplete.execute(true, true, null);
                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);

                        }
                    }
                });
            }
        });
    }

    public static void editJobDetailsWithHeader(final ApplicationThread.OnComplete oncomplete,final String url, final String data){
        ApplicationThread.bgndPost(LiveData.class.getName(), "pairvalues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.sendGet(url, data, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null ||  !result.toString().equalsIgnoreCase("true")) {
                            oncomplete.execute(false, result, null);
                            return;
                        }
                        try {
                            oncomplete.execute(true, true, null);
                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);

                        }
                    }
                });
            }
        });
    }



    public static void uploadPics(final ApplicationThread.OnComplete oncomplete,final String url,final String base64String){
        ApplicationThread.bgndPost(LiveData.class.getName(), "uploadPics...", new Runnable() {
            @Override
            public void run() {
                HttpClient.sendGet(url, base64String, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        try {
                            oncomplete.execute(true, result, null);
                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);

                        }
                    }
                });
            }
        });
    }



    public static void uploadSoapDataInXml(final ApplicationThread.OnComplete oncomplete, final String fileName,final String email, final String encodeString,  final Context context, final int type){
        ApplicationThread.bgndPost(LiveData.class.getName(), "uploadPics...", new Runnable() {
            @Override
            public void run() {

               if (type == 0) {

                   HttpClient.uploadImage(fileName, encodeString,email,context, new ApplicationThread.OnComplete() {
                       public void run() {
                           if (success == false || result == null) {
                               oncomplete.execute(false, null, null);
                               return;
                           }
                           try {
                               oncomplete.execute(true, result, null);
                           } catch (Exception e) {
                               Message messageToParent = new Message();
                               messageToParent.what = 0;
                               Bundle bundleData = new Bundle();
                               bundleData.putString("Time Over", "Lost Internet Connection");
                               messageToParent.setData(bundleData);
                               oncomplete.execute(false, null, null);

                           }
                       }
                   });

               } else  if (type == 1){

                   HttpClient.uploadResume(fileName, encodeString,email,context, new ApplicationThread.OnComplete() {
                       public void run() {
                           if (success == false || result == null) {
                               oncomplete.execute(false, null, null);
                               return;
                           }
                           try {
                               oncomplete.execute(true, result, null);
                           } catch (Exception e) {
                               Message messageToParent = new Message();
                               messageToParent.what = 0;
                               Bundle bundleData = new Bundle();
                               bundleData.putString("Time Over", "Lost Internet Connection");
                               messageToParent.setData(bundleData);
                               oncomplete.execute(false, null, null);

                           }
                       }
                   });
               }else  if (type == 2){

                   HttpClient.uploadBrochure(encodeString,fileName , email, context, new ApplicationThread.OnComplete() {
                       public void run() {
                           if (success == false || result == null) {
                               oncomplete.execute(false, null, null);
                               return;
                           }
                           try {
                               oncomplete.execute(true, result, null);
                           } catch (Exception e) {
                               Message messageToParent = new Message();
                               messageToParent.what = 0;
                               Bundle bundleData = new Bundle();
                               bundleData.putString("Time Over", "Lost Internet Connection");
                               messageToParent.setData(bundleData);
                               oncomplete.execute(false, null, null);

                           }
                       }
                   });
               }else  if (type == 3){

                   HttpClient.getResumeDetails(fileName, encodeString,context, new ApplicationThread.OnComplete() {
                       public void run() {
                           if (success == false || result == null) {
                               oncomplete.execute(false, null, null);
                               return;
                           }
                           try {
                               oncomplete.execute(true, result, null);
                           } catch (Exception e) {
                               Message messageToParent = new Message();
                               messageToParent.what = 0;
                               Bundle bundleData = new Bundle();
                               bundleData.putString("Time Over", "Lost Internet Connection");
                               messageToParent.setData(bundleData);
                               oncomplete.execute(false, null, null);

                           }
                       }
                   });
               }

            }
        });
    }



    public static void uploadSoapDataInXml(final ApplicationThread.OnComplete oncomplete, final String fileName,final String email, final String encodeString, final String method, final Context context, final int type){
        ApplicationThread.bgndPost(LiveData.class.getName(), "uploadPics...", new Runnable() {
            @Override
            public void run() {

                if (type == 0) {

                    HttpClient.uploadImage(fileName, encodeString,email, method, context, new ApplicationThread.OnComplete() {
                        public void run() {
                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            try {
                                oncomplete.execute(true, result, null);
                            } catch (Exception e) {
                                Message messageToParent = new Message();
                                messageToParent.what = 0;
                                Bundle bundleData = new Bundle();
                                bundleData.putString("Time Over", "Lost Internet Connection");
                                messageToParent.setData(bundleData);
                                oncomplete.execute(false, null, null);

                            }
                        }
                    });

                } else {

                    HttpClient.uploadResume(fileName, encodeString,email, method, context, new ApplicationThread.OnComplete() {
                        public void run() {
                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            try {
                                oncomplete.execute(true, result, null);
                            } catch (Exception e) {
                                Message messageToParent = new Message();
                                messageToParent.what = 0;
                                Bundle bundleData = new Bundle();
                                bundleData.putString("Time Over", "Lost Internet Connection");
                                messageToParent.setData(bundleData);
                                oncomplete.execute(false, null, null);

                            }
                        }
                    });
                }

            }
        });
    }

     public static void sendJobValues(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "sendJobValues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        if (result.toString().equalsIgnoreCase("true")) {
                            oncomplete.execute(true, result, null);
                        } else {
                            oncomplete.execute(false, result, null);
                        }
                    }
                });
            }
        });
    }

    public static void getPrivacySettingsValues(final ApplicationThread.OnComplete oncomplete,final String url, final int userty){
        ApplicationThread.bgndPost(LiveData.class.getName(), "getPrivacySettingsValues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        PrivacySettingsModel privacySettingsModel = new PrivacySettingsModel();
                        ArrayList<PrivacySettingsModel> privacySettingsModelList = new ArrayList<PrivacySettingsModel>();
                        try {
                            jArray = new JSONArray(result.toString());
                            if (jArray != null) {
                                int len = jArray.length();
                                for (int i=0;i<len;i++){
                                    JSONObject dataObj = jArray.getJSONObject(i);

                                    privacySettingsModel.setEmail(dataObj.getString(CommonKeys.LJS_Email));
                                    privacySettingsModel.setWeekDay(dataObj.getString(CommonKeys.LJS_WeekDay));
                                    privacySettingsModel.setOnDemandTime(dataObj.getString(CommonKeys.LJS_OnDemandTime));
                                    privacySettingsModel.setEmailAlert(dataObj.getBoolean(CommonKeys.LJS_EmailAlert));
                                    privacySettingsModel.setSmsAlert(dataObj.getBoolean(CommonKeys.LJS_SmsAlert));
                                    privacySettingsModel.setChatAlert(dataObj.getBoolean(CommonKeys.LJS_ChatAlert));
                                    if (userty == 0){
                                        privacySettingsModel.setLjsCommunication(dataObj.getBoolean(CommonKeys.LJS_LjsCommunication));
                                        privacySettingsModel.setPremiumJobSeekerService(dataObj.getBoolean(CommonKeys.LJS_PremiumJobSeekerService));
                                    }

                                    privacySettingsModelList.add(privacySettingsModel);

                                }
                            }
                            oncomplete.execute(true, privacySettingsModelList, null);
                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            oncomplete.execute(false, null, null);
                        }
                    }
                });
            }
        });
    }

    public static void getResultsValues(final ApplicationThread.OnComplete oncomplete,final String url){
        ApplicationThread.bgndPost(LiveData.class.getName(), "getResultsValues...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(url, false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        oncomplete.execute(true, result, null);
                    }
                });
            }
        });
    }



}
