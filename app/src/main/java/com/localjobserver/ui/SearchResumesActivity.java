package com.localjobserver.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;


public class SearchResumesActivity extends ActionBarActivity {
    private ActionBar actionBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_resumes);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (getIntent() != null)
        actionBar.setTitle(getIntent().getStringExtra("Search"));

        Bundle bundle = new Bundle();
        SearchResumesFragment fragInfo = new SearchResumesFragment();
        fragInfo.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, fragInfo)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        this.finish();


    }

}
