package com.localjobserver.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.LocalJobsModel;
import com.localjobserver.models.LocalitiesWithLatLngModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 23-07-2016.
 */
public class MapSeekerFragment extends Fragment {
    private LatLng ljsPoint = new LatLng(21 , 57);
    private LatLng ljsPoint_second = new LatLng(0,0);
    private TextView jobsCont_txt;
    private GoogleMap googleMap;
    private MapView mapView;
    private LocationService locationService;
    private String cityNameStr = "",localityNameStr = "";
    private ProgressDialog progressDialog;
    private ListView localJobsList,recommandedJobsListView;
    private ScrollView scrollview;
    public List<LocalJobsModel> arrLocalJobs = new ArrayList<LocalJobsModel>();
    public List<LocalitiesWithLatLngModel> arrLocalitiesWithLatLngModel = new ArrayList<LocalitiesWithLatLngModel>();
    private LocalJobsModel LocalJobsModel_model = new LocalJobsModel();
    private View rootView;
    private static final String ARG_PARAM1 = "param1";
    private Dialog dialo_changeLocation;
    private Button popup_subsubmitBtn;
//    private Spinner popupLocationSpin;
    private AutoCompleteTextView popupLocationSpin,popup_localityEdt;
    private LinkedHashMap<String,String> locationsMap = null,localityMap = null;
    private MenuItem changeLocMenuItem;


    public static MapSeekerFragment newInstance(int param1) {
        MapSeekerFragment fragment = new MapSeekerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public MapSeekerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.map, container, false);
        try {

            if (googleMap == null) {
                googleMap = ((SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.mapView)).getMap();
                // Check if we were successful in obtaining the map.
                if (googleMap != null) {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            }
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        dialo_changeLocation = new Dialog(getActivity());
        dialo_changeLocation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialo_changeLocation.setContentView(R.layout.popup_changelocation);
        dialo_changeLocation.setCanceledOnTouchOutside(false);
        popup_subsubmitBtn = (Button) dialo_changeLocation.findViewById(R.id.popup_subsubmitBtn);
        popupLocationSpin = (AutoCompleteTextView) dialo_changeLocation.findViewById(R.id.popupLocationSpin);
        popup_localityEdt = (AutoCompleteTextView) dialo_changeLocation.findViewById(R.id.popup_localityEdt);
        popupLocationSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupLocationSpin.showDropDown();
            }
        });

        localJobsList = (ListView)rootView.findViewById(R.id.localJobsListView);
        recommandedJobsListView = (ListView)rootView.findViewById(R.id.recommandedJobsListView);
        jobsCont_txt = (TextView)rootView.findViewById(R.id.jobsCont_txt);
        scrollview = (ScrollView) rootView.findViewById(R.id.scrollview);

            if (isGPSEnabled()){
                Intent intent = new Intent(getActivity(), LocationService.class);
                getActivity().bindService(intent, srvConnection, Context.BIND_AUTO_CREATE);
            }else {
                showSettingsAlert();
            }


        localJobsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String rid = String.valueOf(LocalJobsModel_model.getRoleData().get(position).getRId());
                LinkedHashMap<String,String> input = toUpload(rid);
                JSONObject json = new JSONObject(input);
                String requestString = json.toString();
                requestString = CommonUtils.encodeURL(requestString);

                Bundle searchBundle = new Bundle();
                searchBundle.putString("rid", rid);
                searchBundle.putString("location", cityNameStr);
                searchBundle.putString("mapStr", requestString);
                Intent jobListIntent = new Intent(getActivity(), JobListActivity.class);
                jobListIntent.putExtras(searchBundle);
                startActivity(jobListIntent);
            }
        });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker arg0) {
                localityNameStr = arg0.getSnippet();
                arg0.showInfoWindow();
                getLocalJobs(arg0.getTitle(), arg0.getSnippet());
                return true;
            }

        });

        recommandedJobsListView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scrollview.requestDisallowInterceptTouchEvent(true);

                int action = event.getActionMasked();

                switch (action) {
                    case MotionEvent.ACTION_UP:
                        scrollview.requestDisallowInterceptTouchEvent(false);
                        break;
                }

                return false;
            }
        });

        popup_subsubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (CommonUtils.spinnerSelect("Location",popupLocationSpin.getSelectedItemPosition(),getActivity())){
                if (!popup_localityEdt.getText().toString().equalsIgnoreCase("")) {
                    dialo_changeLocation.dismiss();
                    ljsPoint = null;
                    googleMap.clear();
                    cityNameStr = popupLocationSpin.getText().toString();
                    localityNameStr = popup_localityEdt.getText().toString();
//                    changeLocMenuItem.setTitle("Not your location ?\n"+cityNameStr);
                    changeLocMenuItem.setTitle("Not your location ?\n" + Html.fromHtml("<font color='#008ACE'>"+cityNameStr+"</font>"));
                    getLocalJobs(popupLocationSpin.getText().toString(), popup_localityEdt.getText().toString());
                    getLocalitiesWithLatLngModel(popupLocationSpin.getText().toString());
                } else
                    CommonUtils.showToast("Please select Locality", getActivity());
//                }
            }
        });

        popupLocationSpin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (Map.Entry<String, String> e : locationsMap.entrySet()) {
                    if (e.getValue().equalsIgnoreCase(popupLocationSpin.getText().toString()))
                        getLocalities(""+e.getKey());
                }
                popup_localityEdt.setText("");
            }
        });

       /* popupLocationSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                for (Map.Entry<String, String> e : locationsMap.entrySet()) {
                    if (e.getValue().equalsIgnoreCase(popupLocationSpin.getText().toString()))
                        getLocalities(""+e.getKey());
                }
                popup_localityEdt.setText("");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.map_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
         changeLocMenuItem = menu.findItem(R.id.changeLocation);
        changeLocMenuItem.setTitle("Not your location ?\n" + Html.fromHtml("<font color='#008ACE'>" + cityNameStr + "</font>"));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeLocation:
                if (localityMap != null && localityMap.size()>0){
                    popupLocationSpin.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                    dialo_changeLocation.show();
                }
                else
                getLocations();
                break;
        }
        return true;

    }

    private  LinkedHashMap  toUpload(String rid){
        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put("KeyWords","");
        seekerDataMap.put("Location",cityNameStr);
        if (LocalJobsModel_model.getSearchResultsBy() == 2)
        seekerDataMap.put("SubLocation",localityNameStr);
        else
            seekerDataMap.put("SubLocation","");

        seekerDataMap.put("MinExp","");
        seekerDataMap.put("MaxExp","");
        seekerDataMap.put("MaxSal","0");
        seekerDataMap.put("MinSal","0");
        seekerDataMap.put("JobType","");
        seekerDataMap.put("Industry",  "");
        seekerDataMap.put("Role", rid);
        seekerDataMap.put("Education", "");
        seekerDataMap.put("PostedBy", "");
        seekerDataMap.put("JType","");
        return seekerDataMap;
    }
    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String,String>) result;
                    popupLocationSpin.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                    progressDialog.dismiss();
                    dialo_changeLocation.show();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void getLocalities(String locationCode){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        LiveData.getLocalityGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    localityMap = (LinkedHashMap<String,String>) result;
                    popup_localityEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(localityMap, "Locality")));
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocalities,locationCode), CommonKeys.arrLocalities);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        if (googleMap != null)
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.mapView)).getMap();
            // Check if we were successful in obtaining the map.
            if (googleMap != null)
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }
    private final ServiceConnection srvConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            locationService = ((LocationService.ServiceBinder)service).getService();
            locationService.getLocation(new ApplicationThread.OnComplete() {
                @Override
                public void execute(boolean success, Object result, String msg) {
                    Log.d("LOG_TAG", "Get location:" + String.valueOf(success) + (result == null ? "/null" : ""));
                    if (success && result != null) {
                      final Location location = (Location) result;
                        CommonUtils.getAddressFromLocation(getActivity(), location.getLatitude(), location.getLongitude(), new ApplicationThread.OnComplete() {
                            @Override
                            public void execute(boolean success, Object cityObj, String locality) {
                                if (success) {
                                    if(!TextUtils.isEmpty(cityObj.toString())){
//                                        cityNameTx.setText(""+cityObj.toString());
                                        cityNameStr=cityObj.toString();
                                        localityNameStr=locality.toString();
                                        if (changeLocMenuItem != null)
//                                        changeLocMenuItem.setTitle("Not your location ?\n"+cityNameStr);
                                            changeLocMenuItem.setTitle("Not your location ?\n" + Html.fromHtml("<font color='#008ACE'>"+cityNameStr+"</font>"));
                                        ljsPoint = new LatLng(location.getLatitude(), location.getLongitude());
                                        Marker TP = googleMap.addMarker(new MarkerOptions().
                                                position(ljsPoint).title(cityNameStr).snippet(locality));
                                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ljsPoint,12));
                                        if (!CommonUtils.isNetworkAvailable(getActivity())){
                                            Toast.makeText(getActivity(),"Please check your internet connection and try again.",Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        getLocalJobs(cityNameStr,localityNameStr);
                                        getLocalitiesWithLatLngModel(cityNameStr);
                                    }
                                }else{
                                    Toast.makeText(getActivity(),"Not able to find the location.  please check your GPS settings",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            locationService = null;
        }

    };

    public void getLocalJobs(final String location, final String locality){
        arrLocalJobs  = new ArrayList<LocalJobsModel>();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getLocalJobsData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
//                    arrLocalJobs = (ArrayList<LocalJobsModel>) result;
                    LocalJobsModel_model = (LocalJobsModel)result;
                    LocalListAdpter lJobsAdaper = new LocalListAdpter(getActivity());
                    localJobsList.setAdapter(lJobsAdaper);
                    recommandedJobsListView.setAdapter(lJobsAdaper);
                    progressDialog.dismiss();

                    if (LocalJobsModel_model.getSearchResultsBy() == 0){
                        jobsCont_txt.setText("Local Jobs - Metro Cities : "+LocalJobsModel_model.getJobCount());
                        CommonUtils.showToast("No jobs found in "+locality,getActivity());
                    }
                    else if (LocalJobsModel_model.getSearchResultsBy() == 1){
                        CommonUtils.showToast("No jobs found in "+locality,getActivity());
                        jobsCont_txt.setText("Local Jobs - "+location+": "+LocalJobsModel_model.getJobCount());
                    }
                    else
                        jobsCont_txt.setText("Local Jobs - "+locality+": "+LocalJobsModel_model.getJobCount());

                }else
                    CommonUtils.showToast("No jobs in this locality",getActivity());
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocalJobsCount, location,locality));
    }

    public void getLocalitiesWithLatLngModel( String location){
        arrLocalitiesWithLatLngModel  = new ArrayList<LocalitiesWithLatLngModel>();
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getLocalitiesWithLatLngModelDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    arrLocalitiesWithLatLngModel = (ArrayList<LocalitiesWithLatLngModel>) result;
                    for(int i=0;i<arrLocalitiesWithLatLngModel.size();i++){
                        if (ljsPoint == null && arrLocalitiesWithLatLngModel.get(i).getLocation().trim().equalsIgnoreCase(localityNameStr.toString().trim())){
                            ljsPoint = new LatLng( Double.parseDouble(arrLocalitiesWithLatLngModel.get(i).getLatitude()),  Double.parseDouble(arrLocalitiesWithLatLngModel.get(i).getLongitude()));
                            Marker TP = googleMap.addMarker(new MarkerOptions().
                                    position(ljsPoint).title(cityNameStr).snippet(arrLocalitiesWithLatLngModel.get(i).getLocation()));
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ljsPoint,12));
                        }else {
                            ljsPoint_second = new LatLng( Double.parseDouble(arrLocalitiesWithLatLngModel.get(i).getLatitude()),  Double.parseDouble(arrLocalitiesWithLatLngModel.get(i).getLongitude()));
                            Marker  myMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(ljsPoint_second)
                                    .title(arrLocalitiesWithLatLngModel.get(i).getCity())
                                    .snippet(arrLocalitiesWithLatLngModel.get(i).getLocation())
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                            if (ljsPoint == null)
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ljsPoint_second,12));
                        }
                    }

                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocalitiesWithLatLng, location.trim()));
    }

    class LocalListAdpter extends BaseAdapter {

        Context context;
        public LocalListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.localjobs_list_row, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.title);
            jobTitle.setText(LocalJobsModel_model.getRoleData().get(position).getRoleName());
            TextView jobLoc = (TextView)convertView.findViewById(R.id.count);
            jobLoc.setText(""+LocalJobsModel_model.getRoleData().get(position).getRoleCount());

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return LocalJobsModel_model.getRoleData().size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return LocalJobsModel_model.getRoleData().get(position);
        }

    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * lauch Settings Options
     * */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
//        alertDialog.setTitle("Location is notenab");

        // Setting Dialog Message
        alertDialog
                .setMessage("Please enable Location from GPS Settings");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    public void onResume(){
        super.onResume();
//        if (CommonUtils.isLoggedIn(getActivity()) && CommonUtils.getUserLoggedInType(getActivity()) != 1){
//            if (isGPSEnabled()){
//                Intent intent = new Intent(getActivity(), LocationService.class);
//                getActivity().bindService(intent, srvConnection, Context.BIND_AUTO_CREATE);
//            }else {
//                showSettingsAlert();
//            }
//        }
    }
    public boolean isGPSEnabled(){
        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            if (null != (MainInfoActivity) activity) {
                ((MainInfoActivity) activity).onSectionAttached("Home");
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

}
