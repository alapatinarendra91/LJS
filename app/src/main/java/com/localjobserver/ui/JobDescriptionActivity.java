package com.localjobserver.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Base64;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.helper.ActionItem;
import com.localjobserver.helper.QuickAction;
import com.localjobserver.helper.SocialHelper;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.social.LinkedInSocialDialog;

import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import twitter4j.auth.AccessToken;

public class JobDescriptionActivity extends ActionBarActivity {

    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private Intent receiverIntent = null;
    private int selectPos = 0;
    private JobDescriptionFragment selecedFragment = null;
    private ImageView rightClick,leftClick;
    private ActionBar actionBar;
    public ArrayList<JobsData> jobList;
    public ArrayList<Integer> jobList_applied = new ArrayList<>();
    private RelativeLayout bottomApplyRel,  applyRel;
    private ProgressDialog progressDialog;
    private Button applyBtn;
    private static final int ID_FACEBOOK   = 1;
    private static final int ID_TWITTER   = 2;
    private static final int ID_LINKEDIN = 3;
    private static final int ID_WHATSAPP = 4;
    private QuickAction quickAction = null;
    private SocialHelper mSocialHelper = null;
    public static final int WEBVIEW_REQUEST_CODE = 100;
    private SharedPreferences appPrefs;
    private int userType;
    private Dialog dialog_singleShare;
    private EditText dialog_ContactEdt;
    private TextView dialog_titleTxt;
    private Button dialog_saveBtn;

    private String jobId = "";
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_description);

        appPrefs = getApplicationContext().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        bottomApplyRel = (RelativeLayout) findViewById(R.id.bottomApplyRel);
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (userType == 0)
        actionBar.setTitle("Job Description");
        else
            actionBar.setTitle("Post Jobs");
        actionBar.setCustomView(R.layout.action_title);
        receiverIntent = getIntent();
        applyRel = (RelativeLayout) findViewById(R.id.confirmRel);

        applyBtn = (Button) findViewById(R.id.applyBtn);

        ActionItem fbItem = new ActionItem(ID_FACEBOOK, "", getResources().getDrawable(R.drawable.fb_icon_converted));
//        ActionItem twitterItem = new ActionItem(ID_TWITTER, "", getResources().getDrawable(R.drawable.tw_icon_converted));
        ActionItem linkedInItem = new ActionItem(ID_LINKEDIN, "", getResources().getDrawable(R.drawable.ln_icon_converted));
        ActionItem whatsAppItem = new ActionItem(ID_WHATSAPP, "", getResources().getDrawable(R.drawable.whata_app));
        fbItem.setSticky(true);
//        twitterItem.setSticky(true);

        quickAction = new QuickAction(this, QuickAction.HORIZONTAL);

        //add action items into QuickAction
        quickAction.addActionItem(fbItem);
//        quickAction.addActionItem(twitterItem);
        quickAction.addActionItem(linkedInItem);
        quickAction.addActionItem(whatsAppItem);

        if (CommonUtils.getUserLoggedInType(this) == 0 && !receiverIntent.getAction().equalsIgnoreCase("fromApplied")) {
            bottomApplyRel.setVisibility(View.VISIBLE);
        } else {
            bottomApplyRel.setVisibility(View.GONE);
        }

        if (receiverIntent.getAction().equalsIgnoreCase("fromSearch")) {
            jobList = CommonUtils.jobList;
        } else if (receiverIntent.getAction().equalsIgnoreCase("fromApplied")){
            jobList = CommonUtils.appliedjobList;
        }else
            jobList = CommonUtils.jobList;

        selectPos = receiverIntent.getExtras().getInt("selectedPos");
        jobId  = jobList.get(selectPos).get_id();

        System.out.println("Check..id.." + selectPos+"..."+jobId);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(selectPos);
//        mPager.setOffscreenPageLimit(0);
        selecedFragment = mPagerAdapter.getFragment(selectPos);
        rightClick = (ImageView) findViewById(R.id.rightArrwo);
        rightClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
            }
        });
        leftClick = (ImageView) findViewById(R.id.leftArrwo);
        leftClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isLoggedIn(JobDescriptionActivity.this)){
                    getjobapply(jobId);
                } else{
//                    startActivity(new Intent(JobDescriptionActivity.this, LoginScreen.class));
                    actionBar.hide();
                    CommonUtils.selectedPos_withotLogin = selectPos;
                    CommonUtils.id_withotLogin = jobId;
                    CommonUtils.fromAppliedJobs = true;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(1, "jr")).commit();
                }

            }
        });

        mSocialHelper = new SocialHelper(JobDescriptionActivity.this, this,jobList,null);
        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                //here we can filter which action item was clicked with pos or actionId parameter
                if (actionId == ID_LINKEDIN) {
//                    StringBuffer lnStr = new StringBuffer();
//                    lnStr.append("https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+jobList.get(selectPos).get_id());
//                    startActivity(new Intent(JobDescriptionActivity.this, LinkedInSocialDialog.class).putExtra("lnData", lnStr.toString()));
                    onShareClick();
                } else if (actionId == ID_FACEBOOK) {
                    Session.openActiveSession(JobDescriptionActivity.this, true, new Session.StatusCallback() {
                        // callback when session changes state
                        public void call(Session session, SessionState state,Exception exception) {
                            if (session.isOpened()) {
                                mSocialHelper.publishFeedDialog(selectPos,"seeker");
                            }
                        }
                    });
                } else if (actionId == ID_TWITTER) {
                    mSocialHelper.loginToTwitter(selectPos);
                }else if (actionId == ID_WHATSAPP) {
                    whatsAppShareAlert();

                }
            }
        });

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                selectPos = position;
                jobId  = jobList.get(position).get_id();
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        generateHashkey();

    }

    public void generateHashkey(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "co.talentzing",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                android.util.Log.e("TAG", "Hash is : " + Base64.encodeToString(md.digest(), Base64.NO_WRAP) );
                android.util.Log.e("TAG", "Package is : " +info.packageName );
            }
        } catch (PackageManager.NameNotFoundException e) {
            android.util.Log.e("TAG", e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            android.util.Log.e("TAG", e.getMessage(), e);
        }
    }

    public void onShareClick() {

        Intent linkedinIntent = new Intent(Intent.ACTION_SEND);
        linkedinIntent.setType("text/plain");
        linkedinIntent.putExtra(Intent.EXTRA_TEXT, "https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+jobList.get(selectPos).get_id());

        boolean linkedinAppFound = false;
        List<ResolveInfo> matches2 = getPackageManager()
                .queryIntentActivities(linkedinIntent, 0);

        for (ResolveInfo info : matches2) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(
                    "com.linkedin")) {
                linkedinIntent.setPackage(info.activityInfo.packageName);
                linkedinAppFound = true;
                break;
            }
        }

        if (linkedinAppFound) {
            startActivity(linkedinIntent);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"LinkedIn app not Insatlled in your mobile", Toast.LENGTH_SHORT).show();
        }
    }

    private void openWhatsApp(String PhoneNo) {

        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+PhoneNo)+"@s.whatsapp.net");
        startActivity(sendIntent);
    }

    private void whatsAppShareAlert(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(JobDescriptionActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(JobDescriptionActivity.this);
        }
        builder.setTitle("How you Share Job")
                .setMessage("Please slect one")
                .setPositiveButton("Multiple Share", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        StringBuffer lnStr = new StringBuffer();
                        lnStr.append("\nJob Title:\n");
                        lnStr.append(jobList.get(selectPos).getJobTitle()+"\n");
                        lnStr.append("More Info At:\n");
                        lnStr.append("https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+jobList.get(selectPos).get_id());

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, ""+lnStr.toString());
                        sendIntent.setType("text/plain");
                        sendIntent.setPackage("com.whatsapp");
                        startActivity(sendIntent);
                    }
                })
                .setNegativeButton("Single Share", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        singleShareWhatsapp();

                    }
                })
                .setIcon(R.drawable.icon)
                .show();
    }


    private void singleShareWhatsapp(){
        dialog_singleShare = CommonUtils.dialogIntialize(JobDescriptionActivity.this,R.layout.dialog_other);

        dialog_ContactEdt = (EditText) dialog_singleShare.findViewById(R.id.dialog_otherEdt);
        dialog_titleTxt = (TextView) dialog_singleShare.findViewById(R.id.dialog_titleTxt);
        dialog_saveBtn = (Button) dialog_singleShare.findViewById(R.id.dialog_saveBtn);
        dialog_titleTxt.setText("Contact No");
//        dialog_ContactEdt.setLines(4);
        dialog_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog_ContactEdt.getText().toString().length() == 10){
 /*Worked but not taking Message but need to call */
                    openWhatsApp(jobList.get(selectPos).getPhone());

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                           /* 2nd option but not worked well but after above method it's worked fine*/

                            String toNumber = "+91"+dialog_ContactEdt.getText().toString().trim(); // contains spaces.
                            toNumber = toNumber.replace("+", "").replace(" ", "");

                            Intent sendIntent = new Intent("android.intent.action.MAIN");
                            sendIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
                            sendIntent.putExtra(Intent.EXTRA_TEXT, "https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+jobList.get(selectPos).get_id());
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.setPackage("com.whatsapp");
                            sendIntent.setType("text/plain");
                            startActivity(sendIntent);

                        }
                    }, 1000);

                    dialog_singleShare.dismiss();
                }else
                    CommonUtils.showToast("Please specify Contact",JobDescriptionActivity.this);

            }
        });

        dialog_singleShare.show();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == WEBVIEW_REQUEST_CODE) {
                final String verifier = data.getExtras().getString(mSocialHelper.oAuthVerifier);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            AccessToken accessToken = SocialHelper.twitter.getOAuthAccessToken(SocialHelper.requestToken, verifier);
                            mSocialHelper.saveTwitterInfo(accessToken, data.getIntExtra("position", 0));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            } else {
                Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private final Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            final int what = msg.what;
            switch(what) {
                case 0:
                    if (selectPos ==jobList.size()-1){
                        startActivity(new Intent(JobDescriptionActivity.this, MainInfoActivity.class));
                    }else {
                        jobList_applied.add(selectPos);
                        mPager.setCurrentItem(selectPos+1);
                        applyRel.setVisibility(View.GONE);
                        bottomApplyRel.setVisibility(View.VISIBLE);
                    }

                 break;
            }
        }
    };
    public void getjobapply(final String jobId){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(JobDescriptionActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        selecedFragment = mPagerAdapter.getFragment(selectPos);
        String jobUpId = selecedFragment.jobdataObj.get_id();
        Log.v("", "Check..."+jobUpId);
        jobApply(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (success) {
                    progressDialog.dismiss();
                    if (!result.toString().contains("applied")){
                    bottomApplyRel.setVisibility(View.GONE);
                    applyRel.setVisibility(View.VISIBLE);
                    new Thread(){
                        @Override
                           public  void run(){
                            try {
                                sleep(2000);
                            }catch (Exception e){
                                e.printStackTrace();
                            }finally {
                                updateHandler.sendEmptyMessageAtTime(0, 100);
                            }
                        }
                    }.start();
                }else
                        CommonUtils.showToast(""+result,getApplicationContext());
                }
            }
        }, jobUpId);
    }

    public void deleteJob(final String jobId){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(JobDescriptionActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        selecedFragment = mPagerAdapter.getFragment(selectPos);
        String jobUpId = selecedFragment.jobdataObj.get_id();
        Log.v("", "Check..."+jobUpId);
        deleteJob(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (success) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Job deleted Successfully", getApplicationContext());
                    startActivity(new Intent(JobDescriptionActivity.this, MyJobPostingsActivity.class));
                    finish();

                } else {
                    CommonUtils.showToast("Job delete failed", getApplicationContext());
                }
            }
        }, jobUpId);
    }


    public  void jobApply(final ApplicationThread.OnComplete oncomplete, final  String jobId){
        Log.i("", "Mail id is : " + CommonUtils.getUserEmail(JobDescriptionActivity.this));
        ApplicationThread.bgndPost(JobDescriptionActivity.class.getName(), "applyJob...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.applyJob, "" + jobId, CommonUtils.getUserEmail(JobDescriptionActivity.this)), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        oncomplete.execute(true, result, null);
                    }
                });
            }
        });
    }

    public  void deleteJob(final ApplicationThread.OnComplete oncomplete, final  String jobId){
        Log.i("","Mail id is : "+CommonUtils.getUserEmail(JobDescriptionActivity.this));
        ApplicationThread.bgndPost(JobDescriptionActivity.class.getName(), "applyJob...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.deleteJob, "" + jobId), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        oncomplete.execute(true, result, null);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_description, menu);

        if (userType == 1 || userType == 4){
            menu.getItem(0).setVisible(true);
//            menu.getItem(1).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            View v = findViewById(R.id.action_share);
            quickAction.show(v);
        }
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            if (CommonUtils.fromAppliedJobs){
                CommonUtils.fromAppliedJobs = false;
                startActivity(new Intent(getApplicationContext(),MainInfoActivity.class));
            }else
            finish();
        }

        if (id == R.id.action_delete){
            if (CommonUtils.isLoggedIn(JobDescriptionActivity.this)){
//                deleteJob(jobId);
                deleteJobDialog();
            } else{
                startActivity(new Intent(JobDescriptionActivity.this, LoginScreen.class));
            }
        }
        if (id == R.id.action_edit){
            if (CommonUtils.isLoggedIn(JobDescriptionActivity.this)){
                if (userType == 1 )
//                startActivity(new Intent(JobDescriptionActivity.this, PostWalkinActivity.class).putExtra("editRecruiter", "editRecruiter"));
                startActivity(new Intent(JobDescriptionActivity.this, PostWalkinActivity.class).putExtra("editRecruiter", "editRecruiter").putExtra("selectedPos", selectPos).putExtra("_id", ((JobsData) jobList.get(selectPos)).get_id()).setAction("fromPosting"));

                else if (userType == 4 )
//                    startActivity(new Intent(JobDescriptionActivity.this, PostWalkinActivity.class).putExtra("editReferral", "editReferral"));
                startActivity(new Intent(JobDescriptionActivity.this, PostWalkinActivity.class).putExtra("editReferral", "editReferral").putExtra("selectedPos", selectPos).putExtra("_id", ((JobsData) jobList.get(selectPos)).get_id()).setAction("fromPosting"));

            } else{
                startActivity(new Intent(JobDescriptionActivity.this, LoginScreen.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void deleteJobDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(JobDescriptionActivity.this);
        alertDialogBuilder.setMessage("Are you sure want to delete? ");

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                deleteJob(jobId);
            }
        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    /**
     * A simple pager adapter that represents 5 {@link com.localjobserver.ui.JobDescriptionFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<WeakReference<JobDescriptionFragment>> mPageReferenceMap = new SparseArray<WeakReference<JobDescriptionFragment>>();
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return getFragment(position);
        }

        @Override
        public int getItemPosition(Object object){
            return ScreenSlidePagerAdapter.POSITION_NONE;
        }
        @Override
        public int getCount() {
            return jobList.size();
        }
        @Override
        public Parcelable saveState()
        {
            return null;
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            if (CommonUtils.getUserLoggedInType(getApplicationContext()) == 0 && !receiverIntent.getAction().equalsIgnoreCase("fromApplied")){
                if (selectPos == 0 || selectPos == jobList.size()-1 ){
                    if (jobList_applied.size() > 0 && jobList_applied.contains(selectPos)){
                        bottomApplyRel.setVisibility(View.GONE);
                        applyRel.setVisibility(View.VISIBLE);
                    }else {
                        bottomApplyRel.setVisibility(View.VISIBLE);
                        applyRel.setVisibility(View.GONE);
                    }
                }
            }

            if(object != null){
                return ((Fragment)object).getView() == view;
            }else{
                return false;
            }

        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mPageReferenceMap.remove(Integer.valueOf(position));
            super.destroyItem(container, position, object);
        }


        public Object instantiateItem(ViewGroup container, int position) {
            JobDescriptionFragment fragment = new JobDescriptionFragment();

            // Attach some data to it that we'll
            // use to populate our fragment layouts
            Bundle args = new Bundle();
            args.putSerializable("data", jobList.get(position));
            System.out.println("Check..id..nnn." + (position) + "..." + jobId);
            // Set the arguments on the fragment
            // that will be fetched in DemoFragment@onCreateView

            if (CommonUtils.getUserLoggedInType(getApplicationContext()) == 0 && !receiverIntent.getAction().equalsIgnoreCase("fromApplied")){
                if (jobList_applied.size() > 0 && jobList_applied.contains(selectPos)){
                    bottomApplyRel.setVisibility(View.GONE);
                    applyRel.setVisibility(View.VISIBLE);
                }else {
                    bottomApplyRel.setVisibility(View.VISIBLE);
                    applyRel.setVisibility(View.GONE);
                }
            }

            fragment.setArguments(args);
            mPageReferenceMap.put(Integer.valueOf(position), new WeakReference<JobDescriptionFragment>(fragment));
            return super.instantiateItem(container, position);
        }

        public JobDescriptionFragment getFragment(int key) {
            WeakReference<JobDescriptionFragment> weakReference = mPageReferenceMap.get(key);
            if (null != weakReference) {
                return (JobDescriptionFragment) weakReference.get();
            }
            else {
                return null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        CommonUtils.deleteContact(JobDescriptionActivity.this);
        Log.e("Tag","OnResume");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (CommonUtils.fromAppliedJobs){
            CommonUtils.fromAppliedJobs = false;
            startActivity(new Intent(getApplicationContext(),MainInfoActivity.class));
        }else
            finish();
    }
}
