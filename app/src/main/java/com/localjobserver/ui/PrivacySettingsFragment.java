package com.localjobserver.ui;

/**
 * Created by admin on 02-04-2015.
 */

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.PrivacySettingsModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;
import co.talentzing.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public class PrivacySettingsFragment extends Fragment {
    private View rootView;
    private LinearLayout dailyStatus, selectTime, timeStatus,training_services_lay,dailyStatus_extra, selectTime_extra, timeStatus_extra;
    private CheckedTextView emailChk, SMSview, Chatview, preview, ljscommunication, tr_email, TrSMSview, TrChatview;
    private View emailChLine,SMSviewLine,ChatviewLine,previewLine,ljscommunicationLine;
    private RadioButton radioDaily, radioWeekly, radioInstant,radioDaily_extra, radioWeekly_extra, radioInstant_extra;
    private ProgressDialog progressDialog;
    private SharedPreferences appPrefs;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_SETTINGS = 2;
    private static final int RESULT_SEND_RECRUITERSETTINGS = 2;
    private Spinner daysSpin,daysSpin_extra;
    private Button submitBtn, cancelBtn;
    private String selectedDay = "",selectedDay_extra = "";
    private RadioGroup radioTimeStatus,radioStatus,radioTimeStatus_extra,radioStatus_extra;
//    private PrivacySettingsModel mPrivacySettingsModel;
    public static ArrayList<PrivacySettingsModel> mPrivacySettingsModelList = new ArrayList<PrivacySettingsModel>();
    private ArrayList<String> seekarChecklist ;
    private int userType;
    private String getconfigUrl,sendconfigUrl;
    private String emailcheck,dailicheck,dayTimecheck,timecheck,smscheck,chatcheck;
    private String selectedStr;
    private RadioButton radioButton;
    private String selectedStr_extra;

    public PrivacySettingsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_privacy_settings, container, false);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        emailChk = (CheckedTextView) rootView.findViewById(R.id.emailCh);
        SMSview = (CheckedTextView) rootView.findViewById(R.id.SMSview);
        Chatview = (CheckedTextView) rootView.findViewById(R.id.Chatview);
        preview = (CheckedTextView) rootView.findViewById(R.id.preview);
        ljscommunication = (CheckedTextView) rootView.findViewById(R.id.ljscommunication);
        tr_email = (CheckedTextView) rootView.findViewById(R.id.tr_email);
        TrSMSview = (CheckedTextView) rootView.findViewById(R.id.TrSMSview);
        TrChatview = (CheckedTextView) rootView.findViewById(R.id.TrChatview);
        submitBtn = (Button) rootView.findViewById(R.id.submitBtn);
        cancelBtn = (Button) rootView.findViewById(R.id.cancelBtn);
        emailChLine = (View) rootView.findViewById(R.id.emailChLine);
        SMSviewLine = (View) rootView.findViewById(R.id.SMSviewLine);
        ChatviewLine = (View) rootView.findViewById(R.id.ChatviewLine);
        previewLine = (View) rootView.findViewById(R.id.previewLine);
        ljscommunicationLine = (View) rootView.findViewById(R.id.ljscommunicationLine);

        radioTimeStatus  = (RadioGroup)rootView.findViewById(R.id.radioTimeStatus);
        radioStatus  = (RadioGroup)rootView.findViewById(R.id.radioStatus);
        radioStatus_extra  = (RadioGroup)rootView.findViewById(R.id.radioStatus_extra);
        radioTimeStatus_extra  = (RadioGroup)rootView.findViewById(R.id.radioTimeStatus_extra);

        dailyStatus = (LinearLayout) rootView.findViewById(R.id.dailyStatus);
        selectTime = (LinearLayout) rootView.findViewById(R.id.selectTime);
        timeStatus = (LinearLayout) rootView.findViewById(R.id.timeStatus);
        dailyStatus_extra = (LinearLayout) rootView.findViewById(R.id.dailyStatus_extra);
        selectTime_extra = (LinearLayout) rootView.findViewById(R.id.selectTime_extra);
        timeStatus_extra = (LinearLayout) rootView.findViewById(R.id.timeStatus_extra);
        training_services_lay = (LinearLayout) rootView.findViewById(R.id.training_services_lay);


        radioDaily = (RadioButton) rootView.findViewById(R.id.radioDaily);
        radioWeekly = (RadioButton) rootView.findViewById(R.id.radioWeekly);
        radioInstant = (RadioButton) rootView.findViewById(R.id.radioInstant);
        radioDaily_extra = (RadioButton) rootView.findViewById(R.id.radioDaily_extra);
        radioWeekly_extra = (RadioButton) rootView.findViewById(R.id.radioWeekly_extra);
        radioInstant_extra = (RadioButton) rootView.findViewById(R.id.radioInstant_extra);

        daysSpin = (Spinner) rootView.findViewById(R.id.daytimeSpin);
        daysSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));


        daysSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDay = parent.getSelectedItem().toString();
                if (parent.getSelectedItemId() != 0)
                    timeStatus.setVisibility(View.VISIBLE);
                else
                    timeStatus.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        daysSpin_extra = (Spinner) rootView.findViewById(R.id.daytimeSpin_extra);
        daysSpin_extra.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));

        LinearLayout parentId = (LinearLayout)rootView.findViewById(R.id.parentId);

        parentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        daysSpin_extra.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDay_extra = parent.getSelectedItem().toString();
                if (parent.getSelectedItemId() != 0)
                    timeStatus_extra.setVisibility(View.VISIBLE);
                else
                    timeStatus_extra.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        mPrivacySettingsModel = new PrivacySettingsModel();

        SMSview.setOnClickListener(operationListener);
        Chatview.setOnClickListener(operationListener);
        preview.setOnClickListener(operationListener);
        ljscommunication.setOnClickListener(operationListener);
        tr_email.setOnClickListener(operationListener);
        TrSMSview.setOnClickListener(operationListener);
        TrChatview.setOnClickListener(operationListener);
        submitBtn.setOnClickListener(operationListener);
        cancelBtn.setOnClickListener(operationListener);
        radioDaily.setOnClickListener(operationListener);
        radioWeekly.setOnClickListener(operationListener);
        radioInstant.setOnClickListener(operationListener);
        radioDaily_extra.setOnClickListener(operationListener);
        radioWeekly_extra.setOnClickListener(operationListener);
        radioInstant_extra.setOnClickListener(operationListener);

        emailChk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (emailChk.isChecked()) {
                    emailChk.setChecked(false);
                    dailyStatus.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.GONE);
                    selectTime.setVisibility(View.GONE);

                } else {
                    dailyStatus.setVisibility(View.VISIBLE);
                    emailChk.setChecked(true);
                    radioInstant.setChecked(true);
                }
            }
        });

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


        //Recruiter type = 1
        if (userType == 1){
            ljscommunication.setVisibility(View.GONE);
            ljscommunicationLine.setVisibility(View.GONE);
            preview.setVisibility(View.GONE);
            previewLine.setVisibility(View.GONE);
            training_services_lay.setVisibility(View.GONE);
            getconfigUrl = Config.getRecruiterPrivacySettings;
            sendconfigUrl = Config.getRecruiterPrivacySettings;

        }else if (userType == 0){
            getconfigUrl = Config.getSeekerPrivacySettings;
            sendconfigUrl = Config.alertsJobSeeker;
            training_services_lay.setVisibility(View.VISIBLE);
            preview.setVisibility(View.GONE);
            previewLine.setVisibility(View.GONE);
        }else if (userType == 2){
            getconfigUrl = Config.getFresherPrivacySettings;
            ljscommunication.setVisibility(View.GONE);
            preview.setVisibility(View.GONE);
            previewLine.setVisibility(View.GONE);
            SMSview.setVisibility(View.GONE);
            SMSviewLine.setVisibility(View.GONE);
            training_services_lay.setVisibility(View.GONE);
        }else if (userType == 3){
            getconfigUrl = Config.getTrainerPrivacySettings;
            ljscommunication.setVisibility(View.GONE);
            preview.setVisibility(View.GONE);
            previewLine.setVisibility(View.GONE);
            SMSview.setVisibility(View.GONE);
            SMSviewLine.setVisibility(View.GONE);
            training_services_lay.setVisibility(View.GONE);
        }

        seekarChecklist = new ArrayList<String>();
        getPrivacySettings();

        return rootView;
    }


    public void uploadPrivacySettings() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        JSONObject json = new JSONObject(privacyData());
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    userType = 3;
                    uploadRecruterPrivacySettings();
//                    Message messageToParent = new Message();
//                    messageToParent.what = RESULT_USERDATA;
//                    Bundle bundleData = new Bundle();
//                    messageToParent.setData(bundleData);
//                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + sendconfigUrl, "" + requestString));

    }

    public void uploadRecruterPrivacySettings() {

        if (seekarChecklist.size() == 1)
        getrecruiterconditions();
        else
            privacyData_extra();

        if (userType == 1)
        sendconfigUrl = String.format(Config.LJS_BASE_URL +  Config.recruiterPrivacySettings, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "").trim(),selectedDay , selectedStr,emailChk.isChecked(),SMSview.isChecked(),Chatview.isChecked());
        else if (userType == 2)
            sendconfigUrl = String.format(Config.LJS_BASE_URL +  Config.fresherPrivacySettings, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "").trim(),selectedDay , selectedStr,emailChk.isChecked(),Chatview.isChecked());
        else if (userType == 3)
            sendconfigUrl = String.format(Config.LJS_BASE_URL +  Config.trainerPrivacySettings, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "").trim(),selectedDay , selectedStr,emailChk.isChecked(),Chatview.isChecked());

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null && result.toString().equalsIgnoreCase("true")) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }else {
                    Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                }
            }
        }, sendconfigUrl);

    }

    public LinkedHashMap privacyData() {

        int selectedId = radioTimeStatus.getCheckedRadioButtonId();
        int selectedId_status = radioStatus.getCheckedRadioButtonId();
        String selectedStr = "";
        RadioButton timebutton = (RadioButton)rootView.findViewById(selectedId);
        RadioButton timebutton_status = (RadioButton)rootView.findViewById(selectedId_status);
        if (null != timebutton){
            selectedStr = timebutton.getText().toString();
        }else {
            selectedStr = "";
        }
        LinkedHashMap settingsDataMap = new LinkedHashMap();
        settingsDataMap.put("Email", "" + appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));
        if (timebutton_status.getText().toString().equalsIgnoreCase("Weekly")){
            settingsDataMap.put("WeekDay", selectedDay);
        }else if (timebutton_status.getText().toString().equalsIgnoreCase("Daily")){
            settingsDataMap.put("WeekDay", "Daily");
        }else
            settingsDataMap.put("WeekDay", "Instant");

        settingsDataMap.put("OnDemandTime", selectedStr);
        settingsDataMap.put("EmailAlert", "" + emailChk.isChecked());
        settingsDataMap.put("SmsAlert", "" + SMSview.isChecked());
        settingsDataMap.put("ChatAlert", "" + Chatview.isChecked());
        settingsDataMap.put("PremiumJobSeekerService", "" + preview.isChecked());
        settingsDataMap.put("LjsCommunication", "" + ljscommunication.isChecked());
        return settingsDataMap;
    }

    public void privacyData_extra() {

        int selectedId_extra = radioTimeStatus_extra.getCheckedRadioButtonId();
        int selectedId_status_extra = radioStatus_extra.getCheckedRadioButtonId();
        selectedStr_extra = "";
        RadioButton timebutton_extra = (RadioButton)rootView.findViewById(selectedId_extra);
        RadioButton timebutton_status_extra = (RadioButton)rootView.findViewById(selectedId_status_extra);

        if (null != timebutton_extra){
            selectedStr_extra = timebutton_extra.getText().toString();
            Log.i("","time is : "+selectedStr_extra);
        }else {
            selectedStr_extra = "";
        }


        if (selectTime_extra.getVisibility() == View.VISIBLE){

        }else{
            selectedDay_extra = timebutton_status_extra.getText().toString();
        }

        selectedDay = selectedDay_extra;
        selectedStr = selectedStr_extra;
        emailChk.setChecked(tr_email.isChecked());
        Chatview.setChecked(TrChatview.isChecked());

    }

    private void getrecruiterconditions(){
//        if (emailChk.isChecked()){
            int selectedId = radioTimeStatus.getCheckedRadioButtonId();
            int selectedId_status = radioStatus.getCheckedRadioButtonId();
             selectedStr = "";
            RadioButton timebutton = (RadioButton)rootView.findViewById(selectedId);
            RadioButton timebutton_status = (RadioButton)rootView.findViewById(selectedId_status);

            if (null != timebutton){
                selectedStr = timebutton.getText().toString();
                Log.i("","time is : "+selectedStr);
            }else {
                selectedStr = "";
            }


            if (selectTime.getVisibility() == View.VISIBLE){

            }else{
                selectedDay = timebutton_status.getText().toString();
            }


//        }else {
//
//        }



    }


    View.OnClickListener operationListener = new View.OnClickListener() {

        public void onClick(View v) {
            // TODO Auto-generated method stub

            switch (v.getId()) {

                case R.id.SMSview:
                    if (SMSview.isChecked()) {
                        SMSview.setChecked(false);
                    } else {
                        SMSview.setChecked(true);
                    }
                    break;
                case R.id.Chatview:
                    if (Chatview.isChecked()) {
                        Chatview.setChecked(false);
                    } else {
                        Chatview.setChecked(true);
                    }
                    break;
                case R.id.preview:
                    if (preview.isChecked()) {
                        preview.setChecked(false);
                    } else {
                        preview.setChecked(true);
                    }
                    break;
                case R.id.ljscommunication:
                    if (ljscommunication.isChecked()) {
                        ljscommunication.setChecked(false);
                    } else {
                        ljscommunication.setChecked(true);
                    }
                    break;
                case R.id.tr_email:
                    if (tr_email.isChecked()) {
                        tr_email.setChecked(false);
                        dailyStatus_extra.setVisibility(View.GONE);
                        timeStatus_extra.setVisibility(View.GONE);
                        selectTime_extra.setVisibility(View.GONE);

                    } else {
                        dailyStatus_extra.setVisibility(View.VISIBLE);
                        tr_email.setChecked(true);
                        radioInstant_extra.setChecked(true);
                    }
                    break;
                case R.id.TrSMSview:
                    if (TrSMSview.isChecked()) {
                        TrSMSview.setChecked(false);

                    } else {
                        TrSMSview.setChecked(true);
                    }
                    break;
                case R.id.radioDaily:
                    selectTime.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.VISIBLE);
                    break;
                case R.id.radioDaily_extra:
                    selectTime_extra.setVisibility(View.GONE);
                    timeStatus_extra.setVisibility(View.VISIBLE);
                    break;
                case R.id.TrChatview:
                    if (TrChatview.isChecked()) {
                        TrChatview.setChecked(false);

                    } else {
                        TrChatview.setChecked(true);
                    }
                    break;
                case R.id.radioWeekly:
                    selectTime.setVisibility(View.VISIBLE);
                    if (selectedDay.toString().equalsIgnoreCase("Select"))
                    timeStatus.setVisibility(View.GONE);
                    else
                        timeStatus.setVisibility(View.VISIBLE);

                    break;
                case R.id.radioWeekly_extra:
                    selectTime_extra.setVisibility(View.VISIBLE);
                    break;
                case R.id.radioInstant:
                    selectTime.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.GONE);
                    break;
                case R.id.radioInstant_extra:
                    selectTime_extra.setVisibility(View.GONE);
                    timeStatus_extra.setVisibility(View.GONE);
                    break;
                case R.id.submitBtn:
//                    if (selectTime_extra.getVisibility() == View.VISIBLE){
//                        if (CommonUtils.spinnerSelect("Training Day and time",daysSpin_extra.getSelectedItemPosition(),getActivity())== false);{
//                            return;
//                        }
//
//                    }

                    if (selectTime.getVisibility() == View.VISIBLE ){
                        if (CommonUtils.spinnerSelect("Day and time",daysSpin.getSelectedItemPosition(),getActivity())){
                            if (userType == 0)
                            uploadPrivacySettings();
                            else if (userType == 1 || userType == 2 || userType == 3)
                                uploadRecruterPrivacySettings();
                        }

                    }else{
                        if (userType == 0)
                            uploadPrivacySettings();
                        else if (userType == 1 || userType == 2 || userType == 3)
                            uploadRecruterPrivacySettings();
                    }

                    break;
                case R.id.cancelBtn:

                    break;
            }
        }
    };


    public void getPrivacySettings(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getPrivacySettingsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    mPrivacySettingsModelList = (ArrayList) result;
                    seekarChecklist.add("");
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_SETTINGS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        }, String.format(Config.LJS_BASE_URL + getconfigUrl,CommonUtils.getUserEmail(getActivity()).trim()),userType);
    }

    public void getSeekerTrainerPrivacySettings(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getPrivacySettingsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    mPrivacySettingsModelList = (ArrayList) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_SETTINGS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        }, String.format(Config.LJS_BASE_URL + getconfigUrl,CommonUtils.getUserEmail(getActivity()).trim()),userType);
    }


    private class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while getting data from  server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                        Toast.makeText(getActivity(), "Your privacy settings has updated successfully.", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().popBackStack();

                    break;

                case RESULT_SETTINGS:
                    if (seekarChecklist.size() == 1){
                    emailChk.setChecked(mPrivacySettingsModelList.get(0).getEmailAlert());
                    SMSview.setChecked(mPrivacySettingsModelList.get(0).getSmsAlert());
                    Chatview.setChecked(mPrivacySettingsModelList.get(0).getChatAlert());
                    ljscommunication.setChecked(mPrivacySettingsModelList.get(0).getLjsCommunication());
                    preview.setChecked(mPrivacySettingsModelList.get(0).getPremiumJobSeekerService());
                    if (mPrivacySettingsModelList.get(0).getEmailAlert()){
                        dailyStatus.setVisibility(View.VISIBLE);
                        if (mPrivacySettingsModelList.get(0).getWeekDay().equalsIgnoreCase("Daily")){
                            radioDaily.setChecked(true);
                            timeStatus.setVisibility(View.VISIBLE);

                        }else if (mPrivacySettingsModelList.get(0).getWeekDay().equalsIgnoreCase("Instant")){
                            radioInstant.setChecked(true);
                        }else {
                            radioWeekly.setChecked(true);
                            selectTime.setVisibility(View.VISIBLE);
                            timeStatus.setVisibility(View.VISIBLE);

                            String[] jobType_array = getActivity().getResources().getStringArray(R.array.days);
                            daysSpin.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, jobType_array));

                            for (int i = 0; i < jobType_array.length; i++) {
                                if (jobType_array[i].toString().equals(mPrivacySettingsModelList.get(0).getWeekDay())) {
                                    daysSpin.setSelection(i);
                                }
                            }

                        }

                        if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("8:00 AM"))
                            radioTimeStatus.check(R.id.radio1);
                        else if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("12:00 PM"))
                            radioTimeStatus.check(R.id.radio2);
                        if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("4:00 PM"))
                            radioTimeStatus.check(R.id.radio3);
                        if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("8:00 PM"))
                            radioTimeStatus.check(R.id.radio4);
                    }
                        if (userType == 0){
                            getconfigUrl = Config.getTrainerPrivacySettings;
                            userType = 3;
                            getPrivacySettings();
                        }

                    }else {
                        tr_email.setChecked(mPrivacySettingsModelList.get(0).getEmailAlert());
                        TrSMSview.setChecked(mPrivacySettingsModelList.get(0).getSmsAlert());
                        TrChatview.setChecked(mPrivacySettingsModelList.get(0).getChatAlert());
                        if (mPrivacySettingsModelList.get(0).getEmailAlert()){
                            dailyStatus_extra.setVisibility(View.VISIBLE);
                            if (mPrivacySettingsModelList.get(0).getWeekDay().equalsIgnoreCase("Daily")){
                                radioDaily_extra.setChecked(true);
                                timeStatus_extra.setVisibility(View.VISIBLE);

                            }else if (mPrivacySettingsModelList.get(0).getWeekDay().equalsIgnoreCase("Instant")){
                                radioInstant_extra.setChecked(true);
                            }else {
                                radioWeekly_extra.setChecked(true);
                                selectTime_extra.setVisibility(View.VISIBLE);
                                timeStatus_extra.setVisibility(View.VISIBLE);

                                String[] jobType_array = getActivity().getResources().getStringArray(R.array.days);
                                daysSpin_extra.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, jobType_array));

                                for (int i = 0; i < jobType_array.length; i++) {
                                    if (jobType_array[i].toString().equals(mPrivacySettingsModelList.get(0).getWeekDay())) {
                                        daysSpin_extra.setSelection(i);
                                    }
                                }

                            }

                            if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("8:00 AM"))
                                radioTimeStatus_extra.check(R.id.radio1_extra);
                            else if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("12:00 PM"))
                                radioTimeStatus_extra.check(R.id.radio2_extra);
                            if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("4:00 PM"))
                                radioTimeStatus_extra.check(R.id.radio3_extra);
                            if (mPrivacySettingsModelList.get(0).getOnDemandTime().equalsIgnoreCase("8:00 PM"))
                                radioTimeStatus_extra.check(R.id.radio4_extra);
                        }

                            userType = 0;
                    }
                    break;
            }
        }
    }

}

