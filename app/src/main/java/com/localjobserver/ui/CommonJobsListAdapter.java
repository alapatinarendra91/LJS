package com.localjobserver.ui;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.models.JobsData;

import java.util.ArrayList;

/**
 * Created by admin on 25-04-2015.
 */
public class CommonJobsListAdapter extends BaseAdapter {

    private  Activity context;
    private ArrayList<JobsData> jobList;
    public CommonJobsListAdapter(Activity context,  ArrayList<JobsData> jobList) {
        super();
        this.context=context;
        this.jobList=jobList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.jobs_list_row, null);
        }
        TextView jobTitle = (TextView)convertView.findViewById(R.id.positionNAME);
        jobTitle.setText(((JobsData) jobList.get(position)).getJobTitle() + "(" + ((JobsData) jobList.get(position)).getJobReference() + ")");
        TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
        jobLoc.setText(Html.fromHtml("<b>Location: </b>"+ (((JobsData)jobList.get(position)).getLocation())));
        TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
        jobComp.setText(Html.fromHtml("<b>Company Name: </b>"+ (((JobsData)jobList.get(position)).getCompanyName())));
        if (context instanceof com.localjobserver.ui.JobListActivity)
            jobComp.setText(Html.fromHtml("<b>Company Name: </b>"+ (((JobsData)jobList.get(position)).getCompanyName())+
                    "<br/>"+"<b>No of Positions:</b>" +((JobsData) jobList.get(position)).getJobPositions()));
        TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
        String expStr = "( "+((JobsData)jobList.get(position)).getExpMin()+ "- "+((JobsData)jobList.get(position)).getExpMax() +"year(s))";
        jobExp.setText(""+expStr);
        TextView jobRole = (TextView)convertView.findViewById(R.id.roleJob);
        jobRole.setText(Html.fromHtml("<b>Role: </b> " + (((JobsData) jobList.get(position)).getRole()) + "<br/>" + "<b>CTC: </b> " + ((JobsData) jobList.get(position)).getMinSal() + "-" + ((JobsData) jobList.get(position)).getMaxSal() +"Lac(s)P.A"));
        TextView jobKeySkills = (TextView)convertView.findViewById(R.id.keySkills);
        jobKeySkills.setText(Html.fromHtml("<b>Keyskills: </b>"+(((JobsData)jobList.get(position)).getKeySkills())));
        TextView jobNotice = (TextView)convertView.findViewById(R.id.noticePeriod);
        if ( context instanceof com.localjobserver.ui.JobListActivity ) {
            //Recruiter
            TextView postedDate = (TextView)convertView.findViewById(R.id.postedDate);
            postedDate.setText(Html.fromHtml("<b>Posted Date:</b>"+ CommonUtils.getCleanDate(((JobsData) jobList.get(position)).getPostDate())));

            jobNotice.setText(Html.fromHtml("<b>Notice Period: </b>"+(((JobsData)jobList.get(position)).getNoticePeriod()+" days")+"<br/>"+"<b>Qualification:</b> "+((JobsData) jobList.get(position)).getQualification()
//                    +"<br/>"+"<b>No of Positions:</b>" +((JobsData) jobList.get(position)).getJobPositions()
//                    +"<br/>"+"<b>Job Description:</b> "+((JobsData) jobList.get(position)).getJobDescription()
//                    +"<br/>"+"<b>Posted Date:</b>"+ CommonUtils.getCleanDate(((JobsData) jobList.get(position)).getPostDate())
                    +"<br/>"+" <b><font color='#285e8e'>Viewed:</font></b>"+((JobsData) jobList.get(position)).getViewsCount()
                    +",         "+"<b><font color='#285e8e'>Applied:</font></b>"+((JobsData) jobList.get(position)).getAppliedCount()+"<br/>"+"<b><font color='#285e8e'>Relevance Score:</font></b>"+ ((JobsData) jobList.get(position)).getMatchedScore()));
        }else{
            // Seeker
            jobNotice.setText(Html.fromHtml("<b>Notice Period: </b>"+(((JobsData)jobList.get(position)).getNoticePeriod()+" days")+"<br/>"+"<b>Qualification:</b> "+((JobsData) jobList.get(position)).getQualification()
                    ));
            TextView postedDate = (TextView)convertView.findViewById(R.id.postedDate);
            postedDate.setText(Html.fromHtml("<b>Applied:</b>"+ CommonUtils.getCleanDate(((JobsData) jobList.get(position)).getPostDate())));
        }

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jobList.size();
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return jobList.get(position);
    }

}