package com.localjobserver.ui;

/**
 * Created by admin on 02-04-2015.
 */

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VisibilitySettingsFragment extends Fragment {
    private View rootView;
    private RadioGroup radioVisibilityStatus;
    private RadioButton active,inActive,notVisible;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private SharedPreferences appPrefs;
    private int resultvalue = 1;
    private Button profile_details_submit_btn;
    private int userType;

    public  VisibilitySettingsFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_visibility_settings, container, false);

        LinearLayout parentId = (LinearLayout)rootView.findViewById(R.id.parentId);

        parentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        radioVisibilityStatus = (RadioGroup)rootView.findViewById(R.id.visibilityRadio);
        active = (RadioButton)rootView.findViewById(R.id.active);
        inActive = (RadioButton)rootView.findViewById(R.id.inactive);
        notVisible = (RadioButton)rootView.findViewById(R.id.notvisible);

        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);

        if (userType == 2){

        }

        profile_details_submit_btn=(Button)rootView.findViewById(R.id.submitBtn);
        profile_details_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioVisibilityStatus.getCheckedRadioButtonId();
                RadioButton visiblebutton = (RadioButton)rootView.findViewById(selectedId);
                if (visiblebutton.getText().toString().equalsIgnoreCase("Visible as Active"))
                resultvalue = 1;
                else if (visiblebutton.getText().toString().equalsIgnoreCase("Visible InActive"))
                resultvalue = 2;
                else if (visiblebutton.getText().toString().equalsIgnoreCase("Not Visible"))
                resultvalue = 3;

                uploadVisibiltySettings();
            }
        });

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        getPrivacySettings();

        return rootView;
    }



    public void getPrivacySettings(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
//                    JSONArray jArray = null;
                    try {
//                        jArray = new JSONArray(result.toString());
//                        if (jArray != null) {
//                            int len = jArray.length();
//                            for (int i=0;i<len;i++){
//                                JSONObject dataObj = jArray.getJSONObject(i);
                                JSONObject dataObj = new JSONObject(result.toString());
                                if (dataObj.getString(CommonKeys.LJS_Visibility).equalsIgnoreCase("1"))
                                    active.setChecked(true);
                                if (dataObj.getString(CommonKeys.LJS_Visibility).equalsIgnoreCase("2"))
                                    inActive.setChecked(true);
                                if (dataObj.getString(CommonKeys.LJS_Visibility).equalsIgnoreCase("3"))
                                    notVisible.setChecked(true);

//                            }
//                        }
                    } catch (JSONException e) {

                    }

                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getJsVisibleSettings, CommonUtils.getUserEmail(getActivity()).trim()));
    }

    public void uploadVisibiltySettings() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.changeVisibleSettings, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""),resultvalue));

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    Toast.makeText(getActivity(), "Visibility settings updated successfully.", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                    break;
            }
        }
    }


}

