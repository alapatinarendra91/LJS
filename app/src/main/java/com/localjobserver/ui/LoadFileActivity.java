package com.localjobserver.ui;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.FileChooser;
import com.localjobserver.data.LiveData;
import com.localjobserver.example.MultipartUtility;
import com.localjobserver.models.SearchResultsServiceModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadFileActivity extends ActionBarActivity {

	private TextView fileTxt;
	private WebView fileView;
	private String sdcard_path,attachment_name;
    private ActionBar actionBar;
    private SearchResultsServiceModel selectedSeeker;
    private ProgressDialog progressDialog;
    private static final int RESULT_RESUME = 1;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_RESUME_SAVED = 2;
    private String resumeFromServer, resumeName,fileExtention_str;
    private RelativeLayout upload_lay;
    private Button downLoad,upload,delete,cancil;
    private Intent receiverIntent;
    private String emailId = "",configUrl ="",type = "";
    private static final int FILE_CHOOSER = 0;
    private File resume_file;
    private String resumeFile,uploadMethod;
    private int uploadNo;
    private String path;
    private File f;
    private String responseString;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_file);
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Resume Information");
        actionBar.setCustomView(R.layout.action_title);

        deleteFiles(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TZ/Resumes"+"/"));

        downLoad = (Button)findViewById(R.id.downLoadFile);
        upload = (Button)findViewById(R.id.upload);
        delete = (Button)findViewById(R.id.delete);
        cancil = (Button)findViewById(R.id.cancil);
        upload_lay = (RelativeLayout)findViewById(R.id.upload_lay);
		fileTxt = (TextView)findViewById(R.id.fileNameTxt);
        receiverIntent = getIntent();
        if (receiverIntent.getAction().equalsIgnoreCase("fromDesc")) {
            selectedSeeker = (SearchResultsServiceModel)getIntent().getSerializableExtra("selectedObj");
            emailId = selectedSeeker.getEmail();
            configUrl =   String.format(Config.LJS_BASE_URL + Config.getProfileResume, emailId);
            type = "recruiter";
        } else if (getIntent().getStringExtra("FRESHER") != null){
            emailId = CommonUtils.getUserEmail(this);
            configUrl = String.format(Config.LJS_BASE_URL + Config.getFreshersProfileResume, emailId);
            upload_lay.setVisibility(View.VISIBLE);
            uploadMethod = "uploadFresherResume";
            uploadNo = 1;
            type = "fresher";
        }else {
            //Seeker
            emailId = CommonUtils.getUserEmail(this);
            configUrl = String.format(Config.LJS_BASE_URL + Config.getProfileResume, emailId);
            upload_lay.setVisibility(View.VISIBLE);
            uploadMethod = "uploadResume";
            uploadNo = 1;
            type = "seeker";
        }

		fileTxt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openAttachment();
            }
        });
        upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getApplicationContext(), FileChooser.class);
                ArrayList<String> extensions = new ArrayList<String>();
                extensions.add(".pdf");
                extensions.add(".doc");
                extensions.add(".docx");
                intent.putStringArrayListExtra("filterFileExtension", extensions);
                startActivityForResult(intent, FILE_CHOOSER);

            }
        });
        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                fileExtention_str = "";
                resumeFile = "";
                path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/TZ/Resumes"+"/"+ emailId+type;
                deleteResume();

            }
        });
        cancil.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
		fileView = (WebView)findViewById(R.id.fileView);
		fileView.setContentDescription("application/doc");
		downLoad.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				new FileDownloader(LoadFileActivity.this, "http://www.egr.msu.edu/classes/ece480/capstone/spring12/group03/Documents/Chris_ApplicationNote.doc", touchHandle).execute();
                getResume();
            }
        });

         path = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/TZ/Resumes"+"/"+emailId+type;
         f = new File(path);

//        getResume();

        if (CommonUtils.isFileExisted(emailId+type) || f.exists()) {
            resumeName = emailId+type;
            fileTxt.setVisibility(View.VISIBLE);
            fileTxt.setText("" + resumeName);
            downLoad.setVisibility(View.GONE);
        }
        else {
            // do your stuff

        }
	}

    void deleteFiles(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteFiles(child);

        fileOrDirectory.delete();
    }


    private void openAttachment() {
		// TODO Auto-generated method stub
        try {


        Intent intent = new Intent();
     intent.setAction(Intent.ACTION_VIEW);
	 File   fileIn = new File(path);
	 Uri uri = Uri.fromFile(fileIn);

      if(resumeName.endsWith(".doc") || resumeName.endsWith(".docx"))
     {
         intent.setDataAndType(uri, "application/msword");
         startActivity(intent);
     }else if(resumeName.endsWith(".pdf"))
     {
         Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
         pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         pdfOpenintent.setDataAndType(uri, "application/pdf");
         try {
             startActivity(pdfOpenintent);
         }
         catch (ActivityNotFoundException e) {

         }
//         intent.setDataAndType(uri, "*/*");
//         startActivity(intent);
     }else if(resumeName.endsWith(".rtf"))
     {
         intent.setDataAndType(uri, "application/vnd.ms-excel");
         startActivity(intent);
     }
     else {
//			intent.setDataAndType(uri, "*/*");
//			startActivity(intent);

          Intent myIntent = new Intent(Intent.ACTION_VIEW);
          myIntent.setData(uri);
          Intent j = Intent.createChooser(myIntent, "Choose an application to open with:");
          startActivity(j);
      }
        }catch (Exception e){
            CommonUtils.showToast(""+e.getMessage(),getApplicationContext());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
        if (id == android.R.id.home) {
            finish();
        }
		return super.onOptionsItemSelected(item);
	}


    public void getResume(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LoadFileActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(getApplicationContext(),"No resume",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (result != null && !result.toString().equalsIgnoreCase("false")) {
                    try {
                        JSONObject jsonObj = new JSONObject(result.toString());
                        resumeFromServer = jsonObj.getString("ResumeFile");
                        fileExtention_str = jsonObj.getString("FileName");
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_RESUME;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);

                    }catch (Exception e){

                    }

                }else
                    Toast.makeText(getApplicationContext(),"No resume",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }, configUrl);
    }


    public void getSaveResume(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LoadFileActivity.this);
            progressDialog.setMessage("Saving...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        CommonUtils.saveResumeToSdcard(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    resumeName = (String) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_RESUME_SAVED;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        }, resumeFromServer, emailId,type+fileExtention_str, LoadFileActivity.this);
    }


    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(LoadFileActivity.this, "Error occurred while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_RESUME:
                    if (resumeFromServer.equalsIgnoreCase(""))
                        CommonUtils.showToast("No Resumes to display",getApplicationContext());
                        else
                    getSaveResume();
                    break;
                case RESULT_RESUME_SAVED:
                    if (resumeFromServer.equalsIgnoreCase("")){
                        CommonUtils.showToast("No Resumes to display",getApplicationContext());
                    }else {
                        downLoad.setVisibility(View.GONE);
                        fileTxt.setVisibility(View.VISIBLE);
                        fileTxt.setText("" + resumeName);
                        path =  Environment.getExternalStorageDirectory()+ "/TZ/Resumes"+"/"+resumeName;
                    }

                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FILE_CHOOSER) {
            if (null != data) {
                downLoad.setVisibility(View.GONE);

                resumeName = data.getStringExtra("fileSelected");
                File f = new File(resumeName);
                System.out.println(f.getName());

                if (f.getName().substring(f.getName().length() - 3).equalsIgnoreCase("pdf"))
                    fileExtention_str = ".pdf";
                else
                    fileExtention_str = ".doc";

                path = resumeName;
                fileTxt.setText(f.getName());
                fileTxt.setVisibility(View.VISIBLE);
//                mDataModel.setResume(fileSelected);
                File dir = Environment.getExternalStorageDirectory();
                resume_file = new File(resumeName);
                try {
                    progressDialog = new ProgressDialog(LoadFileActivity.this);
                    progressDialog.setMessage("Upload Resume..");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    resumeFile = CommonUtils.encodeFileToBase64Binary(resume_file);
                    Thread timerThread = new Thread() {
                        public void run() {
                            try {
                                sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                uploadDocumentstoServer();
                            }
                        }
                    };
                    timerThread.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void uploadDocumentstoServer() {

        String charset = "UTF-8";
        String requestURL = "";
            requestURL = Config.LJS_BASE_URL +Config.uploadResume;

        try {
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);
            multipart.addFilePart("", resume_file);

            List<String> response = multipart.finish();
            System.out.println("SERVER REPLIED:");
            for (String line : response) {
                Log.e("Resp", "Respo is : " + line.toString());
                responseString = line.toString();
            }
//                CommonUtils.showToast("" + response, getActivity());
//            progressDialog.cancel();

                saveResume();

        } catch (IOException ex) {
            Log.e("Exce", "Exce is : " + ex.getMessage());
            CommonUtils.showToast("" + ex.getMessage(), getApplicationContext());
            progressDialog.cancel();
        }
    }

    public void saveResume() {

        if (progressDialog == null) {
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Resume Update Successfully",getApplicationContext());
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.saveResume, resume_file.getName(),CommonUtils.getUserEmail(this)));

    }

    public void deleteResume() {
            progressDialog = new ProgressDialog(LoadFileActivity.this);
            progressDialog.setMessage("Deleting user Resume..");
            progressDialog.setCancelable(false);
            progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Failed to Update Resume", getApplicationContext());
                    return;
                }
                if (result != null) {
                    progressDialog.cancel();
                        File f = new File(path);
                        f.delete();
                        finish();
                        CommonUtils.showToast("Resume Deleted Successfully",getApplicationContext());
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.deleteResume, CommonUtils.getUserEmail(this)));

    }



    public void uploadUserResume() {
//        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LoadFileActivity.this);
        if (fileExtention_str.equalsIgnoreCase(""))
            progressDialog.setMessage("Deleting user Resume..");
        else
            progressDialog.setMessage("Uploading user Resume..");
            progressDialog.setCancelable(false);
//        }
        progressDialog.show();
//        File f = new File(resumeName);
//        System.out.println(f.getName());

        LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Failed to Update Resume", getApplicationContext());
                    return;
                }
                if (result != null) {
                    progressDialog.cancel();
                    if (fileExtention_str.equalsIgnoreCase("")){
                        File f = new File(path);
                        f.delete();
                        finish();
                        CommonUtils.showToast("Resume Deleted Successfully",getApplicationContext());

                    }
                    else
                    CommonUtils.showToast("Resume Update Successfully",getApplicationContext());

                }
            }
        }, fileExtention_str, CommonUtils.getUserEmail(this), resumeFile, uploadMethod, getApplicationContext(), uploadNo);

    }

}
