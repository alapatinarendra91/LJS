package com.localjobserver.ui;

/**
 * Created by admin on 01-04-2015.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.imagecrop.Constant;
import com.localjobserver.imagecrop.CropImage.CropImage;
import com.localjobserver.imagecrop.ImageCropActivity;
import com.localjobserver.jobsinfo.MainJobsInfoFragment;
import com.localjobserver.models.SetupData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;
import com.localjobserver.profile.UpdateProfileActivity;
import co.talentzing.R;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.List;

public class ProfileActivity extends ActionBarActivity {

    private TextView tvmailid,tvphonenum,tvlocation;
    public static TextView tvname,tvresumeheadline,tvkeyskills,tvexperience,tvlatupdateddate;
    private static ProgressDialog progressDialog;
    private  RelativeLayout profilepic;
    private Button editprofilebtn,downloadresumebtn;
    public List<SetupData> seekerdetails,jobSeekerProfileList;
    public SetupData mSetupData;
    private Context context;
    private ActionBar actionBar = null;
    private Bitmap bmp = null;
    private static Drawable dr = null;
    private ImageView edit_dis;
    private String profile_pic = "";
    private SharedPreferences appPrefs = null;
    private byte[] seekerImageData;
    private static String  compressedStr;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context=this;
        mSetupData = new SetupData();
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Profile Information");

        tvname=(TextView)findViewById(R.id.profilename_dis);
        tvresumeheadline=(TextView)findViewById(R.id.resumeheadline_dis);
        tvkeyskills=(TextView)findViewById(R.id.keyskills_dis);
        tvexperience=(TextView)findViewById(R.id.experience_dis);
        tvmailid=(TextView)findViewById(R.id.mailid);
        tvphonenum=(TextView)findViewById(R.id.phonenum);
        tvlocation=(TextView)findViewById(R.id.location);
        tvlatupdateddate=(TextView)findViewById(R.id.lastupdateddate);
        edit_dis = (ImageView)findViewById(R.id.edit_dis);
        profilepic=(RelativeLayout)findViewById(R.id.sec_rel);

        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        profile_pic = appPrefs.getString(CommonUtils.getUserEmail(this), null);

        fm = getSupportFragmentManager();



        if (profile_pic != null && profile_pic.toString().length() >= 10){
            try {
                String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                if (seekerImageData != null){
                    BitmapDrawable dr = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));
                    profilepic.setBackground(dr);
                    profilepic.invalidate();
                }else {
                    (profilepic).setBackgroundResource(R.drawable.avatar);
                }

            } catch (Exception e) {
                e.printStackTrace();
                (profilepic).setBackgroundResource(R.drawable.avatar);
            }
        }else {
            (profilepic).setBackgroundResource(R.drawable.avatar);
        }

//        profilepic.setBackgroundResource(R.drawable.sample);
        editprofilebtn=(Button)findViewById(R.id.profile_details_edit_btn);
        editprofilebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment newFragment = MainProfileFragment.newInstance();
//                getSupportFragmentManager().beginTransaction().replace(android.R.id.content, newFragment).addToBackStack(null).commit();

                Intent mIntent = new Intent(getApplicationContext(), UpdateProfileActivity.class);
                startActivity(mIntent);
            }
        });
        downloadresumebtn=(Button)findViewById(R.id.profile_details_download_btn);


        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, new PlaceholderFragment()).addToBackStack(null)
                        .commit();
            }
        });
        Button recJobsBtn = (Button)findViewById(R.id.recommandedjobsList_btn);
        recJobsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment newFragment = MainJobsInfoFragment.newInstance(2, "rec");
//                getSupportFragmentManager().beginTransaction().add(android.R.id.content, newFragment).commit();
                startActivity(new Intent(getApplicationContext(), LoadFileActivity.class).putExtra("selectedObj","").setAction("gh"));
            }
        });
        getProfileData();

    }


    public  void getProfileData() {
        getCompleteProfile();
        bindData();
        setDataToObject();
    }

    private void setDataToObject(){
        SetupData mJobListData = jobSeekerProfileList.get(0);
        mSetupData.setfName(mJobListData.getfName());
        mSetupData.setlName(mJobListData.getlName());
        mSetupData.setEmail(mJobListData.getEmail());
        mSetupData.setContactNum(mJobListData.getContactNum());
        mSetupData.setLandNum(mJobListData.getLandNum());
        mSetupData.setPassword(mJobListData.getPassword());
        mSetupData.setConfirmPass(mJobListData.getConfirmPass());
        mSetupData.setGender(mJobListData.getGender());
        mSetupData.setCurrentCompany(mJobListData.getCurrentCompany());
        mSetupData.setRole(mJobListData.getRole());
        mSetupData.setCurrentDisgnation(mJobListData.getCurrentDisgnation());
        mSetupData.setTotExp(mJobListData.getTotExp());
        mSetupData.setCurrentCtc(mJobListData.getCurrentCtc());
        mSetupData.setExpectedCtc(mJobListData.getExpectedCtc());
        mSetupData.setKeySkills(mJobListData.getKeySkills());
        mSetupData.setCurrentLocation(mJobListData.getCurrentLocation());
        mSetupData.setPreferedLocation(mJobListData.getPreferedLocation());
        mSetupData.setNoticePeriod(mJobListData.getNoticePeriod());
        mSetupData.setEduCourse(mJobListData.getEduCourse());
        mSetupData.setInstitution(mJobListData.getInstitution());
        mSetupData.setYearPassing(mJobListData.getYearPassing());
        mSetupData.setResumeHeadLine(mJobListData.getResumeHeadLine());
        mSetupData.setInId(mJobListData.getInId());
        mSetupData.setJobtype(mJobListData.getJobtype());
    }






    public void toUpload(){
        LinkedHashMap seekerDataMap = new LinkedHashMap();
        Log.i("","Experience is : "+mSetupData.getExperience());
        seekerDataMap.put("FirstName",""+mSetupData.getfName());
        seekerDataMap.put("LastName",""+mSetupData.getlName());
        seekerDataMap.put("Password",""+mSetupData.getPassword());
        seekerDataMap.put("ConfirmPassword",""+mSetupData.getConfirmPass());
        seekerDataMap.put("Email",""+mSetupData.getEmail());
        seekerDataMap.put("ContactNo",""+mSetupData.getContactNum());
        seekerDataMap.put("ContactNo_Landline",""+mSetupData.getLandNum());
        seekerDataMap.put("Industry",""+mSetupData.getCurrentIndestry());
        seekerDataMap.put("Experience",""+mSetupData.getExperience());
        seekerDataMap.put("CurrentCTC",""+mSetupData.getCurrentCtc());
        seekerDataMap.put("expectedCTC",""+mSetupData.getExpectedCtc());
        seekerDataMap.put("Education",""+mSetupData.getEduCourse());
        seekerDataMap.put("KeySkills",""+mSetupData.getKeySkills());
        seekerDataMap.put("StdKeySkills",""+mSetupData.getStdKeySkills());
        seekerDataMap.put("Location",""+mSetupData.getCurrentLocation());
        seekerDataMap.put("UpdateResume","");
        seekerDataMap.put("Gender",""+mSetupData.getGender());
        seekerDataMap.put("Institute",""+mSetupData.getInstitution());
        seekerDataMap.put("YearOfPass",""+mSetupData.getYearPassing());
        seekerDataMap.put("FunctionalArea",""+mSetupData.getFunArea());
        seekerDataMap.put("Role",""+mSetupData.getRole());
        seekerDataMap.put("ResumeHeadLine",""+mSetupData.getResumeHeadLine());
        seekerDataMap.put("ProfileUpdate","0");
        seekerDataMap.put("UpdateOn", CommonUtils.getDateTime());
        seekerDataMap.put("Resume","");
        seekerDataMap.put("TextResume","");
        seekerDataMap.put("UserName","");
        seekerDataMap.put("Photo","");
        seekerDataMap.put("CurrentDesignation",""+mSetupData.getCurrentDisgnation());
        seekerDataMap.put("CurrentCompany",""+mSetupData.getCurrentCompany());
        seekerDataMap.put("StdCurCompany",""+mSetupData.getCurrentCompany());
        seekerDataMap.put("PreviousDesignation",""+mSetupData.getPreviousDesignation());
        seekerDataMap.put("PreviousCompany",""+mSetupData.getPreviousCompany());
        seekerDataMap.put("StdPreCompany",""+mSetupData.getPreviousCompany());
        seekerDataMap.put("PreferredLocation",""+mSetupData.getPreferedLocation());
        seekerDataMap.put("NoticePeriod",""+mSetupData.getNoticePeriod());
        seekerDataMap.put("Jobtype",""+mSetupData.getJobtype());

        updateJobSeekerProfile(seekerDataMap);
    }
    private void updateJobSeekerProfile(LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(ProfileActivity.this, "Error while registering user", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(ProfileActivity.this,"Profile Details updated successfully", Toast.LENGTH_SHORT).show();
//                        DatabaseAccessObject.deleteRecord("JobSeekers", ProfileActivity.this);
//                        DatabaseAccessObject.insertJobSeekerData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
//                            public void run() {
//                                if (success == false) {
//                                    return;
//                                }
//                                if (result != null) {
//                                    getProfileDate();
//                                    progressDialog.dismiss();
//                                }
//                            }
//                        }, mSetupData, ProfileActivity.this);
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyJSDetails,mSetupData.getEmail(), "" + requestString));
    }

    private void getCompleteProfile(){
        jobSeekerProfileList = DatabaseAccessObject.getJobSeekerProfle(Queries.getInstance().getDetails("JobSeekers"),context);
    }

    private void bindData()
    {
        for(int i=0;i<jobSeekerProfileList.size();i++)
        {
            tvname.setText(jobSeekerProfileList.get(i).getfName()+" "+jobSeekerProfileList.get(i).getlName());
            tvresumeheadline.setText(jobSeekerProfileList.get(i).getExperience() + " year(s) of experience on "+jobSeekerProfileList.get(i).getKeySkills());
            tvkeyskills.setText(jobSeekerProfileList.get(i).getKeySkills());
            tvexperience.setText(jobSeekerProfileList.get(i).getExperience() + " year(s)");
            tvmailid.setText(jobSeekerProfileList.get(i).getEmail());
            tvphonenum.setText(jobSeekerProfileList.get(i).getContactNum());
            tvlocation.setText(jobSeekerProfileList.get(i).getCurrentLocation());
            if (jobSeekerProfileList.get(i).getUpdateOn().contains("Date"))
                tvlatupdateddate.setText("Last Update: "+CommonUtils.getCleanDate(jobSeekerProfileList.get(i).getUpdateOn()));
            else
                tvlatupdateddate.setText("Last Update: "+jobSeekerProfileList.get(i).getUpdateOn());

            byte[] imhvbytes=jobSeekerProfileList.get(i).getSeekerImageData();
        //    bmp= getBitmap(imhvbytes);
            if(bmp!=null){
                dr = new BitmapDrawable(context.getResources(), bmp);
                (profilepic).setBackground(dr);
            }

        }

    }
    public Bitmap getBitmap(byte[] bitmap) {
        return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);

    }

  private static   Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("update"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){

            case R.id.yourresume:
                Intent in2 =new Intent(getApplicationContext(), LoadFileActivity.class);
                in2.setAction("fromProfile");
                startActivity(in2);
                break;

            case R.id.changepassword:
                actionBar.setTitle("Change Password");
                getSupportFragmentManager().beginTransaction().add(android.R.id.content,  new ChangePasswordFragment()).addToBackStack(null).commit();
                break;

            case R.id.visibilitysettings:
                actionBar.setTitle("Visibility Settings");
                getSupportFragmentManager().beginTransaction().add(android.R.id.content,  new VisibilitySettingsFragment()).addToBackStack(null).commit();
                break;

            case R.id.appliedjobs:
                actionBar.setTitle("Jobs For You");
                Fragment newFragment = MainJobsInfoFragment.newInstance(1,"Applied");
                getSupportFragmentManager().beginTransaction().replace(android.R.id.content, newFragment).commit();
                break;

            case R.id.privacysettings:
                actionBar.setTitle("Privacy Settings");
                getSupportFragmentManager().beginTransaction().add(android.R.id.content,  new PrivacySettingsFragment()).addToBackStack(null).commit();
                break;

            case android.R.id.home:
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }else{
                    this.finish();
                }
                break;

        }
        return true;

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final int REQUEST_CODE_CROP_IMAGE = 4;
        private static  int CURRENT_DISPLAY_SCREEN = 0, FILE_CHOOSER = 1, SELECT_PICTURE =2, FROMCAM = 3;
        private ImageView profilePick;
        private byte[] seekerImageData;
        private View rootView = null;
        private Button choosePic = null,  cancelBtn = null;
        private String profile_pic = "";
        private SharedPreferences appPrefs = null;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_profile2, container, false);
            profilePick = (ImageView)rootView.findViewById(R.id.proFic);
            appPrefs =  getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
            profile_pic = appPrefs.getString(CommonUtils.getUserEmail(getActivity()), null);

//            if(null != dr){
//                profilePick.setImageDrawable(dr);
//                profilePick.invalidate();
//            }

            if (profile_pic != null){
                try {
                    String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                    seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                    BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));
                    profilePick.setImageDrawable(dr);
                    profilePick.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            choosePic = (Button)rootView.findViewById(R.id.edit);
            choosePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(choosePic.getText().toString().equalsIgnoreCase("edit")){
                        profilePic();
                    }else{

                        uploadUserImage();

                    }
                }
            });



            cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
//                        if (fm.getBackStackEntryCount() > 0) {
//                            fm.popBackStack();
//                        }else{
//                            getActivity().finish();
//                        }
                    if (seekerImageData == null || seekerImageData.length== 0)
                        CommonUtils.showToast("No image",getActivity());
                    else
                    DeleteImage();

                }
            });


            return rootView;
        }

        public void  uploadUserImage(){


//            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
                progressDialog.show();
//            }

            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Failed to update your Image", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null) {
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                        editor.commit();
                        Toast.makeText(getActivity(), "Your photo has been updated successfully", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        } else {
                            getActivity().finish();
                        }
                        Intent uiUpdateIntent = new Intent("update");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

                        Intent uiUpdateIntent_seeker = new Intent("updateProfilePick");
                        uiUpdateIntent_seeker.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent_seeker);
                    }
                }
            }, "" + CommonUtils.getUserEmail(getActivity()) + ".png", CommonUtils.getUserEmail(getActivity()), compressedStr,"seekerUploadImage", getActivity(), 0);

        }

        public void DeleteImage(){


//            if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
            progressDialog.show();
//            }

            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Failed to Delete Profile Pic",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null) {
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), "");
                        editor.commit();
                        Toast.makeText(getActivity(),"Delete Profile Pic Successfully",Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        dr = null;
                        Intent uiUpdateIntent = new Intent("updateProfilePick");
                        uiUpdateIntent.putExtra("imageString", "");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                        Intent uiUpdateIntentupdate = new Intent("update");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntentupdate);

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        }else{
                            getActivity().finish();
                        }



                    }
                }
            }, "default", CommonUtils.getUserEmail(getActivity()), "","seekerUploadImage", getActivity(), 0);

        }

        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                        startActivityForResult(intent, FROMCAM);
                        CommonUtils.setimageDestination(Constant.imageNames.SeekerImage);
                        Intent intent = new Intent(getActivity(), ImageCropActivity.class);
                        intent.putExtra("ACTION", "action-camera");
                        startActivityForResult(intent, FROMCAM);
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        //Call an intent that opens gallery to select photo
//                        Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(intent, SELECT_PICTURE);
                        CommonUtils.setimageDestination(Constant.imageNames.SeekerImage);
                        Intent intent = new Intent(getActivity(), ImageCropActivity.class);
                        intent.putExtra("ACTION", "action-gallery");
                        startActivityForResult(intent, FROMCAM);

                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == FROMCAM  ) {
                if (resultCode == RESULT_OK){


               /* File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(CommonUtils.Image_destination.getAbsolutePath(),80,80);
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(CommonUtils.Image_destination.getAbsolutePath(), bitmapOptions);
                Matrix matrix = new Matrix();
                matrix.postRotate(CommonUtils.getImageOrientation(CommonUtils.Image_destination.getAbsolutePath()));
//                Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(),yourSelectedImage.getHeight(), matrix, true);
                dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                seekerImageData = outputStream.toByteArray();

                Intent uiUpdateIntent = new Intent("updateProfilePick");
                uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                */

                startCropImage();

                }else if (resultCode == RESULT_CANCELED) {
                } else {
                    String errorMsg = data.getStringExtra(ImageCropActivity.ERROR_MSG);
                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == SELECT_PICTURE){
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inPurgeable = true;
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath,options);
                dr = new BitmapDrawable(yourSelectedImage);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 90,outputStream);
                seekerImageData = outputStream.toByteArray();
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");

                Intent uiUpdateIntent = new Intent("updateProfilePick");
                uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

            }else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
                if (resultCode == RESULT_CANCELED)
                    return;

                Bitmap photo = BitmapFactory.decodeFile(CommonUtils.Image_destination.getPath());
                try {
                    FileOutputStream out = new FileOutputStream(CommonUtils.Image_destination);
                    photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    Bitmap myBitmap = BitmapFactory.decodeFile(CommonUtils.Image_destination.getAbsolutePath());

                    dr = new BitmapDrawable(this.getResources(), myBitmap);
                    profilePick.setImageDrawable(dr);
                    profilePick.invalidate();
                    choosePic.setText("OK");

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                    seekerImageData = outputStream.toByteArray();


//                    Intent uiUpdateIntent = new Intent("updateProfilePick");
//                    uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
//                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }

        private void startCropImage() {
            Intent intent = new Intent(getActivity(), CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, CommonUtils.Image_destination.getPath());
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);

            //indicate output X and Y
            intent.putExtra("outputX", 256);
            intent.putExtra("outputY", 256);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        }
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("update")) {
                    // restore from shared prefs if there's value there
                    if(null != dr)
                        (profilepic).setBackground(dr);
                    else
                        (profilepic).setBackgroundResource(R.drawable.avatar);
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }else{
            this.finish();
        }
    }
}
