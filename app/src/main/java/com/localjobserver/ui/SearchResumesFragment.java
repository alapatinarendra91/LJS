package com.localjobserver.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.SearchModel;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedBackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchResumesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private View rootView;
    private LinearLayout keySkillsLay;
    private  MultiAutoCompleteTextView keySkillsEdt,LocEdt,preLocEdt;
    private AutoCompleteTextView compNameEdt;
    private MultiSpinnerSearch currentIndeSpin,roleSpin;
    private Spinner scopeSpin,expMinSp,totCtcSpmin,totThCtcSp,expSp,DegreehighestSpin,activeLastSpin;
    private ImageView speaker;
    private Button submitBtn,cancelBtn;
    private String selectedInd;
    public LinkedHashMap<String,String> industries = null, locationsMap = null,roleMap = new LinkedHashMap<>();
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_KEYSKILLS = 2;
    private static final int  RESULT_LOCATIONS = 3;
    private static final int RESULT_INDUSTRIES = 4;
    private static final int RESULT_COMPANIES = 5;
    private static final int RESULT_EDUCATION = 6;
    private ArrayList<String> keySkillsList;
    private LinkedHashMap<String, String> companiesMap;
    private LinkedHashMap<String, String> educationMap;
    private SearchModel searchModel;
    private  boolean isClassVisible = false;
    private String activityComingFrom = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_search_resumes, container, false);
        searchModel = new SearchModel();
        keySkillsLay = (LinearLayout) rootView.findViewById(R.id.keySkillsLay);
        keySkillsEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt);
        LocEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.LocEdt);
        preLocEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.preLocEdt);
        compNameEdt = (AutoCompleteTextView)rootView.findViewById(R.id.compNameEdt);
        keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        LocEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        preLocEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        speaker = (ImageView) rootView.findViewById(R.id.speaker);
        scopeSpin = (Spinner)rootView.findViewById(R.id.scopeSpin);
        expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        currentIndeSpin = (MultiSpinnerSearch)rootView.findViewById(R.id.currentIndeSpin);
        totCtcSpmin = (Spinner)rootView.findViewById(R.id.totCtcSp);
        totThCtcSp = (Spinner)rootView.findViewById(R.id.totThCtcSp);
        roleSpin = (MultiSpinnerSearch)rootView.findViewById(R.id.roleSpin);
        DegreehighestSpin = (Spinner)rootView.findViewById(R.id.DegreehighestSpin);
        activeLastSpin = (Spinner)rootView.findViewById(R.id.activeLastSpin);

        scopeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.scope)));
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totThCtcSp.setOnItemSelectedListener(spinListener);
        totCtcSpmin = (Spinner)rootView.findViewById(R.id.totCtcSpmin);
        totCtcSpmin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totCtcSpmin.setOnItemSelectedListener(spinListener);
        activeLastSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.activeInLast)));
//        roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));
        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,"Select Role");

        scopeSpin.setSelection(1);
        activeLastSpin.setSelection(5);

        currentIndeSpin.setOnItemSelectedListener(spinListener);
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);

        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.getString("Filter") != null)
        activityComingFrom = bundle.getString("Filter");

        if (activityComingFrom != null && activityComingFrom.equalsIgnoreCase("Filter")){
            keySkillsLay.setVisibility(View.GONE);
        }else
        getKeyWords();

        getIndustries();
        getLocations();
        getCompanies();
        getEducations();

        expMinSp.setOnItemSelectedListener(spinListener);
        expSp.setOnItemSelectedListener(spinListener);
        currentIndeSpin.setOnItemSelectedListener(spinListener);
        DegreehighestSpin.setOnItemSelectedListener(spinListener);
        roleSpin.setOnItemSelectedListener(spinListener);
        activeLastSpin.setOnItemSelectedListener(spinListener);
        expMinSp.setSelection(1);
        totCtcSpmin.setSelection(1);

        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptSpeechInput();
            }
        });

        keySkillsEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO && validateFields()) {

                        postSearchData(toPostSearch(searchModel));
                }
                return false;
            }
        });
        LocEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO && validateFields()) {

                        postSearchData(toPostSearch(searchModel));
                }
                return false;
            }
        });
        preLocEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO && validateFields()) {

                        postSearchData(toPostSearch(searchModel));
                }
                return false;
            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields() ) {
                    postSearchData(toPostSearch(searchModel));
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            Bundle bundle = this.getArguments();
            if (bundle == null)
            ((MainInfoActivity) activity).onSectionAttached("Search Resumes");
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say your skill");
        try {
            getActivity().startActivityForResult(intent, 100);
        } catch (ActivityNotFoundException a) {
            CommonUtils.showToast("Sorry! Your device doesn\\'t support speech input",getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // In fragment class callback
        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (result.get(0).equalsIgnoreCase("search"))
                        postSearchData(toPostSearch(searchModel));
                    else{
                    keySkillsEdt.append(result.get(0)+", ");
                    keySkillsEdt.setSelection(keySkillsEdt.getText().toString().length());
                }
                }
                break;
            }
        }
    }

    public void postSearchData(LinkedHashMap<String,String> input){
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        Log.e("ReqSt", "ReqSt Str is :" + requestString);
        requestString = CommonUtils.encodeURL(requestString);
        String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.ResumeResults, "" + requestString);
        Log.i("", "preparedUrltoGetResumes is  :" + preparedUrltoGetResumes);
        startActivity(new Intent(getActivity(), ResumesListActivity.class).putExtra("searchTxt", "").putExtra("resumesLink", preparedUrltoGetResumes));
    }

    public LinkedHashMap toPostSearch(SearchModel searchModel){
        LinkedHashMap postSearch = new LinkedHashMap();
        postSearch.put("ReturnUrl","");
        postSearch.put("KeySkills",keySkillsEdt.getText().toString());
        postSearch.put("Scope", scopeSpin.getSelectedItem().toString());
        postSearch.put("Location", LocEdt.getText().toString());
        if (!searchModel.getMinExp().equalsIgnoreCase("Select"))
        postSearch.put("MinExp",""+searchModel.getMinExp());
        else
            postSearch.put("MinExp","0");

        if (!searchModel.getMaxExp().equalsIgnoreCase("Select"))
        postSearch.put("MaxExp",""+searchModel.getMaxExp());
        else
            postSearch.put("MaxExp","0");

        if (!searchModel.getCurrentCTC().equalsIgnoreCase("Select"))
            postSearch.put("CurrentCTC",""+searchModel.getCurrentCTC());
        else
            postSearch.put("CurrentCTC","0.0");

        postSearch.put("Industry",searchModel.getIndustry());
        postSearch.put("FunctionalArea","");
        postSearch.put("CompanyName",compNameEdt.getText().toString());
        postSearch.put("Role",searchModel.getRole());
        postSearch.put("HighestDegree", searchModel.getHighestDegree());
        postSearch.put("CandidatesActiveinLast", searchModel.getCandidatesActiveinLast());
        postSearch.put("ResumeperPage", "");
        postSearch.put("ActiveDate", "");
        postSearch.put("PreferedLocation", preLocEdt.getText().toString());
        postSearch.put("SearchQuery", "");
        postSearch.put("Email",""+DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getActivity()));
        postSearch.put("SearchType","");
//        postSearch.put("Date", "" + CommonUtils.getDateTime());
        postSearch.put("SaveId", "0");
        postSearch.put("Rid", "0");
        postSearch.put("Fid", "0");
        postSearch.put("IndId", "" + searchModel.getIndId());
        postSearch.put("Education", searchModel.getHighestDegree());
        postSearch.put("NoticePeriod", "0");
        return postSearch;
    }

    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String,String>) result;

                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }
    public void getCompanies(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
//                        progressDialog.dismiss();
                    companiesMap = (LinkedHashMap<String,String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_COMPANIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_EDUCATION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }


    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
//                                keySkillsList.add(c.getString(CommonKeys.LJS_StdKeySkills));
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }
    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:

                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap &&  isClassVisible == true) {
                        LocEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                        preLocEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));

                    }else{
                        Toast.makeText(getActivity(),"Not able to get locations",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_KEYSKILLS:
                    keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                    break;
                case RESULT_COMPANIES:
                    if (null != companiesMap && null != compNameEdt &&  isClassVisible == true) {
                            compNameEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(companiesMap, "Company")));
                    }
                    break;
                case RESULT_EDUCATION:
                    if (null != educationMap && null != DegreehighestSpin &&  isClassVisible == true) {
                        DegreehighestSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Qualification")));
                    } else {
                        Toast.makeText(getActivity(),"Not able to get education details",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries &&  isClassVisible == true) {
//                        currentIndeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(industries, "IndustryType")));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndeSpin,industries,1,"Select Industry");
                    }else{
                        Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    public void getRoles(final String selectedInd){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    roleMap = (LinkedHashMap<String,String>)result;
                    if (null != roleMap && null != roleSpin) {
//                        roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,"Select Role");
                    }
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getRoles,selectedInd), CommonKeys.arrRoles);
    }

    public boolean validateFields(){
        Form mForm = new Form(getActivity());

        if (keySkillsEdt.getText().toString().equals("") &&
                 LocEdt.getText().toString().equals("")&&
                preLocEdt.getText().toString().equals("")&&
                compNameEdt.getText().toString().equals("")&&
                expMinSp.getSelectedItem().toString().equalsIgnoreCase("0")&&
                currentIndeSpin.getSelectedIds().size() == 0&&
                DegreehighestSpin.getSelectedItem().toString().contains("Select Qualification")){
            Toast.makeText(getActivity(),"Please Select any critaria",Toast.LENGTH_SHORT).show();
            return (mForm.isValid()) ? false : false;

        }else
            return (mForm.isValid()) ? true : false;

    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.scopeSpin:
                    searchModel.setScope(scopeSpin.getSelectedItem().toString());
                    break;
                case R.id.expMinSp:
                    searchModel.setMinExp(expMinSp.getSelectedItem().toString());
                    if (expMinSp.getSelectedItemId() == 31){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() == 1){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() != 0){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }
                    break;

                case R.id.totCtcSpmin:
                    searchModel.setCurrentCTC(totCtcSpmin.getSelectedItem().toString());

                    if (totCtcSpmin.getSelectedItemId() == 100){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totCtcSpmin.getSelectedItemId() == 1){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSpmin.getSelectedItem().toString()), 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totCtcSpmin.getSelectedItemId() != 0){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSpmin.getSelectedItem().toString())+1, 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.totThCtcSp:
                    searchModel.setCurrentCTC(totCtcSpmin.getSelectedItem().toString()+"."+totThCtcSp.getSelectedItem().toString());
                    break;

                case R.id.expSp:
                    searchModel.setMaxExp(expSp.getSelectedItem().toString());
                    break;
                case R.id.currentIndeSpin:

                    if (null != industries && currentIndeSpin.getSelectedIds().size() > 0){
//                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
                        selectedInd = String.valueOf(currentIndeSpin.getSelectedIds().get(0));
                        searchModel.setIndId(selectedInd);
                        searchModel.setIndustry(currentIndeSpin.getSelectedItem().toString());
                        getRoles(selectedInd);
                    }else{
                        searchModel.setIndId("0");
                        searchModel.setIndustry("");
                    }

                    break;
                case R.id.roleSpin:
                    if (null != roleMap &&  roleSpin.getSelectedIds().size() > 0)
                    searchModel.setRole(roleSpin.getSelectedItem().toString());
                    else
                        searchModel.setRole("");
                    break;
                case R.id.DegreehighestSpin:
                    if (DegreehighestSpin.getSelectedItemId() != 0)
                    searchModel.setHighestDegree(DegreehighestSpin.getSelectedItem().toString());
                    else
                        searchModel.setHighestDegree("");
                    break;

                case R.id.activeLastSpin:
                    searchModel.setCandidatesActiveinLast(activeLastSpin.getSelectedItem().toString().replace("Days",""));
                    break;

            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
