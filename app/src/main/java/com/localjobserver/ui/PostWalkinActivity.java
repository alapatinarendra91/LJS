package com.localjobserver.ui;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import co.talentzing.R;


public class PostWalkinActivity extends ActionBarActivity {
    private SharedPreferences preferences;

    private ActionBar actionBar = null;
    public static boolean back_var = true;
    FragmentManager fragmentManager = getSupportFragmentManager();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        preferences = getApplicationContext().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (getIntent().getStringExtra("walkin") != null){
            actionBar.setTitle("Post Walk-In");
            fragmentManager.beginTransaction().replace(R.id.container, new PostWalkinFragment("walkin", 0)).commit();
        }else if (getIntent().getStringExtra("newjob") != null){
            actionBar.setTitle("Post Job");
            fragmentManager.beginTransaction().replace(R.id.container, new PostWalkinFragment("newjob",0)).commit();
        }else if (getIntent().getStringExtra("referral") != null){
            actionBar.setTitle("Post A Refferal");
            fragmentManager.beginTransaction().replace(R.id.container, new PostWalkinFragment("referral",0)).commit();
        }else if (getIntent().getStringExtra("editRecruiter") != null){
            actionBar.setTitle("Edit post");
            fragmentManager.beginTransaction().replace(R.id.container, new PostWalkinFragment("editRecruiter", getIntent().getExtras().getInt("selectedPos"))).commit();
        }else if (getIntent().getStringExtra("editReferral") != null){
            actionBar.setTitle("Edit post");
            fragmentManager.beginTransaction().replace(R.id.container, new PostWalkinFragment("editReferral", getIntent().getExtras().getInt("selectedPos"))).commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        back_var = false;
        this.finish();


    }
}
