package com.localjobserver.ui;

/**
 * Created by admin on 02-04-2015.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.FileChooser;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.util.ArrayList;

public class UploadResume extends ActionBarActivity {

    private  Button chooseBtn,uploadBtn;
    private static  int  FILE_CHOOSER = 1;
    private TextView nofilechoosentv;
    private ProgressDialog progressDialog;
    private static final int RESULT_RESUME = 1;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_RESUME_SAVED = 2;
    private String resumeFromServer, resumeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_resume);

        chooseBtn=(Button)findViewById(R.id.choosebtn);
        uploadBtn=(Button)findViewById(R.id.profile_details_upload_btn);
        nofilechoosentv=(TextView)findViewById(R.id.nofilechoosentv);

        chooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getResume();
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), FileChooser.class);
                ArrayList<String> extensions = new ArrayList<String>();
                extensions.add(".pdf");
                extensions.add(".doc");
                extensions.add(".docx");
                intent.putStringArrayListExtra("filterFileExtension", extensions);
                startActivityForResult(intent, FILE_CHOOSER);

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == 0) && (resultCode == -1)) {
            String fileSelected = data.getStringExtra("fileSelected");
//                Toast.makeText(getActivity(), fileSelected, Toast.LENGTH_SHORT).show();
            nofilechoosentv.setText(fileSelected);

        }
    }

    public void getResume(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    resumeFromServer = (String) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_RESUME;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getProfileResume, "hello@gmail.com"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(UploadResume.this, "Error occurred while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_RESUME:
                    getSaveResume();
                    break;
                case RESULT_RESUME_SAVED:
//                    downLoad.setVisibility(View.GONE);
//                    fileTxt.setVisibility(View.VISIBLE);
//                    fileTxt.setText(""+resumeName);
                    break;
            }
        }
    }

    public void getSaveResume(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Saving...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        CommonUtils.saveResumeToSdcard(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    resumeName = (String) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_RESUME_SAVED;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        }, resumeFromServer, "hello@gmail","", this);
    }

}
