package com.localjobserver.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.chat.LJSChatActivity;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.helper.StickyScrollView;
import com.localjobserver.menu.MenuRootActivity;
import com.localjobserver.menu.ShareResumeActivity;
import com.localjobserver.models.SearchResultsServiceModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


public class ResumeDescriptionActivity extends ActionBarActivity {

    private ViewPager mPager;
    private ArrayList<SearchResultsServiceModel> resumeData;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private Intent receiverIntent = null;
    private int selectPos = 0;
    private ActionBar actionBar;
    private String _id, email_resume;
    private ProgressDialog progressDialog;
    private SharedPreferences appPrefs;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_description);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Seeker Info");
        actionBar.setCustomView(R.layout.action_title);
        receiverIntent = getIntent();
        selectPos = receiverIntent.getExtras().getInt("selectedPos");
        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);

        if (receiverIntent.getAction().equalsIgnoreCase("fromSearch")) {
            resumeData = CommonUtils.ResumeList;
            if (getIntent() != null) {
                _id = getIntent().getStringExtra("_id");
                email_resume = getIntent().getStringExtra("email_resume");
            }
        } else {
            resumeData = (ArrayList<SearchResultsServiceModel>) getIntent().getSerializableExtra("resumesItems");
        }
//        resumeData = (ArrayList<SearchResultsServiceModel>)getIntent().getSerializableExtra("resumesItems");
        resumeData.get(0).getResumeHeadLine();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(selectPos);
        mPager.setOffscreenPageLimit(1);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resume_description, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
        menu.getItem(3).setVisible(false);
        menu.getItem(4).setVisible(false);
        menu.getItem(5).setVisible(false);
        menu.getItem(6).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        if (id == R.id.action_chat) {
            if (CommonUtils.isLoggedIn(getApplicationContext()) && resumeData.get(selectPos - 1).getIsOnline().equalsIgnoreCase("True")) {
                Intent i = new Intent(getApplicationContext(), LJSChatActivity.class);
                Bundle msgBundle = new Bundle();
                msgBundle.putString("fromEmailId", "" + CommonUtils.getUserEmail(getApplicationContext()));
                msgBundle.putString("toEmailId", resumeData.get(selectPos - 1).getEmail());
                i.putExtras(msgBundle);
                startActivity(i);
            }
        }

        if (id == R.id.action_dial) {
            String number = "" + resumeData.get(selectPos - 1).getContactNo();
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        }

        if (id == R.id.action_sms) {
            startActivity(new Intent(ResumeDescriptionActivity.this, MenuRootActivity.class).putExtra("type", "Send SMS").putExtra("_id", _id).putExtra("email_resume", resumeData.get(selectPos - 1).getEmail()));
        }

        if (id == R.id.action_mail) {
            startActivity(new Intent(ResumeDescriptionActivity.this, MenuRootActivity.class).putExtra("type", "Send Email").putExtra("_id", _id).putExtra("email_resume", resumeData.get(selectPos - 1).getEmail()));
        }

        if (id == R.id.action_download) {
            startActivity(new Intent(ResumeDescriptionActivity.this, LoadFileActivity.class).putExtra("selectedObj", resumeData.get(selectPos - 1)).setAction("fromDesc"));
        }

        if (id == R.id.action_share) {
            startActivity(new Intent(ResumeDescriptionActivity.this, ShareResumeActivity.class).putExtra("type", "Send Email").putExtra("_id", _id).putExtra("email_resume", email_resume));
        }

        if (id == R.id.action_delete) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ResumeDescriptionActivity.this);
            alertDialogBuilder.setMessage("Do you want to delete?");

            alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {

                    CommonUtils.showToast("Waiting for Implement", ResumeDescriptionActivity.this);
//                    Deleteresume();
                }
            });

            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void Deleteresume() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ResumeDescriptionActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();


        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success != true) {
                    progressDialog.dismiss();
                    Toast.makeText(ResumeDescriptionActivity.this, "Failed  " + result.toString(), Toast.LENGTH_LONG).show();
                    if (result != null) {
                    }
                    return;
                }
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(ResumeDescriptionActivity.this, "Resume deleted Successfully", Toast.LENGTH_LONG).show();
                        progressDialog.cancel();
                    }
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.deleteSavedResume, email_resume, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), "FolderName"));
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<WeakReference<ResumeDescriptionFragment>> mPageReferenceMap = new SparseArray<WeakReference<ResumeDescriptionFragment>>();

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return getFragment(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return ScreenSlidePagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return resumeData.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            if (object != null) {
                return ((Fragment) object).getView() == view;
            } else {
                return false;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mPageReferenceMap.remove(Integer.valueOf(position));
            super.destroyItem(container, position, object);
        }


        public Object instantiateItem(ViewGroup container, int position) {
            ResumeDescriptionFragment fragment = new ResumeDescriptionFragment();

            // Attach some data to it that we'll
            // use to populate our fragment layouts
            pos = position;
            Bundle args = new Bundle();
            args.putSerializable("data", resumeData.get(position));

            // Set the arguments on the fragment
            // that will be fetched in DemoFragment@onCreateView
            fragment.setArguments(args);
            selectPos = position;
            mPageReferenceMap.put(Integer.valueOf(position), new WeakReference<ResumeDescriptionFragment>(fragment));
            return super.instantiateItem(container, position);
        }

        public ResumeDescriptionFragment getFragment(int key) {
            WeakReference<ResumeDescriptionFragment> weakReference = mPageReferenceMap.get(key);
            if (null != weakReference) {
                return (ResumeDescriptionFragment) weakReference.get();
            } else {
                return null;
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ResumeDescriptionFragment extends Fragment {

        private View rootView;
        private SearchResultsServiceModel jobdataObj;
        private TextView tvResumeTitle, tvFirstName, tvexperience,
                tvcompname, tvnoticeperiod, tvemail, tvcontactnumber,
                tvlocation, tvPreferedLoc, tvindustrytype, tvfunctionalarea, tvkeyskills, tvJobName, tvEducation, tvYearOfpassing, tvDesignaatgion, tvInstitution, tvGender, tvRole;
        private StickyScrollView scroll = null;
        private Button similarProfiles;
        private Bundle bundle1;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                jobdataObj = (SearchResultsServiceModel) getArguments().getSerializable("data");
            }
        }

        public ResumeDescriptionFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_resume_description, container, false);

            initializeUi();

            bindData();

            return rootView;
        }


        public void initializeUi() {
            scroll = (StickyScrollView) rootView.findViewById(R.id.topScroll);
            tvFirstName = (TextView) rootView.findViewById(R.id.fname);
            tvResumeTitle = (TextView) rootView.findViewById(R.id.resumeHead);
            tvGender = (TextView) rootView.findViewById(R.id.gender);
            tvexperience = (TextView) rootView.findViewById(R.id.experience);
            tvlocation = (TextView) rootView.findViewById(R.id.currentloc);
            tvcompname = (TextView) rootView.findViewById(R.id.currentComp);
            tvnoticeperiod = (TextView) rootView.findViewById(R.id.noticePeriod);
            tvemail = (TextView) rootView.findViewById(R.id.email);
            tvcontactnumber = (TextView) rootView.findViewById(R.id.contactNum);
            tvPreferedLoc = (TextView) rootView.findViewById(R.id.preffredloc);
            tvindustrytype = (TextView) rootView.findViewById(R.id.indType);
            tvfunctionalarea = (TextView) rootView.findViewById(R.id.funArea);
            tvkeyskills = (TextView) rootView.findViewById(R.id.keyskills);
            tvEducation = (TextView) rootView.findViewById(R.id.qualification);
            tvYearOfpassing = (TextView) rootView.findViewById(R.id.year);
            tvDesignaatgion = (TextView) rootView.findViewById(R.id.currentDesg);
            tvInstitution = (TextView) rootView.findViewById(R.id.institution);
            tvRole = (TextView) rootView.findViewById(R.id.role);
            similarProfiles = (Button) rootView.findViewById(R.id.similarProfiles);
        }


        public void bindData() {
//            similarProfiles.setText("   Similar Profiles : "+jobdataObj.getSimilarCount()+"  ");
            similarProfiles.setText("   Similar Profiles ");
            tvFirstName.setText("" + jobdataObj.getFirstName());
            tvResumeTitle.setText("" + jobdataObj.getResumeHeadLine());
             tvGender.setText("" + jobdataObj.getGender());
            tvexperience.setText("" + jobdataObj.getExperience());
            tvlocation.setText("" + jobdataObj.getLocation());
            tvcompname.setText("" + jobdataObj.getCurrentCompany());
            tvnoticeperiod.setText("" + jobdataObj.getNoticePeriod());
            tvemail.setText("" + jobdataObj.getEmail());
            tvcontactnumber.setText("" + jobdataObj.getContactNo());
            tvPreferedLoc.setText("" + jobdataObj.getPreferredLocation());
            tvindustrytype.setText("" + jobdataObj.getIndustry());
            tvfunctionalarea.setText("");
            tvkeyskills.setText("" + jobdataObj.getKeySkills());
            tvEducation.setText("" + jobdataObj.getEducation());
            tvYearOfpassing.setText(""+jobdataObj.getYearOfPass());
            tvDesignaatgion.setText("" + jobdataObj.getCurrentDesignation());
            tvInstitution.setText(""+jobdataObj.getInstitute());
            tvRole.setText("" + jobdataObj.getRole());

            ImageView onlineStatus = (ImageView)rootView.findViewById(R.id.onlineStatus);
            onlineStatus.setImageResource((((null != jobdataObj.getIsOnline()) && (jobdataObj.getIsOnline().equalsIgnoreCase("True"))) ? R.drawable.online : R.drawable.offline));

            onlineStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != jobdataObj.getIsOnline() && jobdataObj.getIsOnline().equalsIgnoreCase("True")) {
                        if (CommonUtils.isLoggedIn(getActivity())) {
                            Intent i = new Intent(getActivity(), LJSChatActivity.class);
                            Bundle msgBundle = new Bundle();
                            msgBundle.putString("fromEmailId", "" + CommonUtils.getUserEmail(getActivity()));
                            msgBundle.putString("toEmailId", jobdataObj.getEmail());
                            i.putExtras(msgBundle);
                            startActivity(i);
                        }
                    }
                }
            });

            ImageView sendEmail = (ImageView)rootView.findViewById(R.id.emailImg);
            sendEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getActivity(), MenuRootActivity.class).putExtra("type", "Send Email").putExtra("_id", jobdataObj.get_id()).putExtra("email_resume", jobdataObj.getEmail()));

                }
            });

            ImageView sms = (ImageView)rootView.findViewById(R.id.sms);
            sms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getActivity(), MenuRootActivity.class).putExtra("type", "Send SMS").putExtra("_id", jobdataObj.get_id()).putExtra("email_resume", jobdataObj.getEmail()));

                }
            });

            ImageView sendCALL = (ImageView)rootView.findViewById(R.id.callImg);
            sendCALL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String number=""+jobdataObj.getContactNo();
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+number));
                    startActivity(callIntent);
                }
            });

            ImageView download = (ImageView)rootView.findViewById(R.id.download);
            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getActivity(), LoadFileActivity.class).putExtra("selectedObj", jobdataObj).setAction("fromDesc"));

                }
            });

            ImageView share = (ImageView)rootView.findViewById(R.id.share);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(getActivity(), ShareResumeActivity.class).putExtra("type", "Send Email").putExtra("_id", jobdataObj.getEmail()).putExtra("email_resume", jobdataObj.getEmail()));

                }
            });

            similarProfiles.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    if (!jobdataObj.getSimilarCount().equalsIgnoreCase("0"))
                    startActivity(new Intent(getActivity(),ResumesListActivity.class).putExtra("SIMILARRESUMES_MAIL",jobdataObj.getEmail()));
                }
            });

        }
    }
}
