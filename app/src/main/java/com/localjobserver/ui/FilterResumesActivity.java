package com.localjobserver.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;
import com.localjobserver.filter.FilterRecruiterFragment;

public class FilterResumesActivity extends ActionBarActivity {

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_resumes);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle("Filter Profiles");

        Bundle bundle = new Bundle();
        bundle.putString("Filter","Filter");
        FilterRecruiterFragment fragInfo = new FilterRecruiterFragment();
        fragInfo.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, fragInfo)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
