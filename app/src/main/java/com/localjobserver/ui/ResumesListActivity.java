package com.localjobserver.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.menu.MenuRootActivity;
import com.localjobserver.models.SearchResultsServiceModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.List;

public class ResumesListActivity extends ActionBarActivity {

    private ResumesListAdpter resumesListAdpter = null;
    private FragmentManager fragmentManager;
    private ListView resumesListView;
    private ActionBar actionBar = null;
    private TextView searchTxt;
    private ImageView editBtn;
    private String searchStr;
    private ArrayList<SearchResultsServiceModel> resumesList;
    public String preparedUrltoGetResumes;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumes_list);

        fragmentManager = getSupportFragmentManager();
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        resumesListView = (ListView)findViewById(R.id.resumeListView);
        searchTxt = (TextView)findViewById(R.id.searchTxt);
        editBtn = (ImageView)findViewById(R.id.searchEdit);
        searchStr = getIntent().getStringExtra("searchTxt");
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResumesListActivity.this, MainInfoActivity.class).putExtra("searchTxt", searchStr));
            }
        });
        searchTxt.setText("" + searchStr);
        if (getIntent().getStringExtra("resumesLink")!= null){
            preparedUrltoGetResumes = getIntent().getStringExtra("resumesLink");
            getSupportActionBar().setTitle("Search Results");
        }else if (getIntent().getStringExtra("appliedResumes")!= null){
            preparedUrltoGetResumes = getIntent().getStringExtra("appliedResumes");
            getSupportActionBar().setTitle("Applied candidates");
        }else if (getIntent().getStringExtra("matchingProfiles")!= null){
            preparedUrltoGetResumes = getIntent().getStringExtra("matchingProfiles");
            getSupportActionBar().setTitle("Matching Profiles");
        }else if (getIntent().getStringExtra("SIMILARRESUMES_MAIL") != null){
            getSupportActionBar().setTitle("Similar Proifiles");
            preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getSimilarProfiles, getIntent().getStringExtra("SIMILARRESUMES_MAIL"));
        }

        Log.e("preparedUrltoGetResumes","preparedUrltoGetResumes is  :"+preparedUrltoGetResumes);
        getResumessData(preparedUrltoGetResumes);
//        resumesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                startActivity(new Intent(ResumesListActivity.this, ResumeDescriptionActivity.class).putExtra("resumesItems", resumesList).putExtra("selectedPos",position));
//
//                Intent jobIntent = new Intent(ResumesListActivity.this, ResumeDescriptionActivity.class);
//                jobIntent.putExtra("selectedPos", position);
//                jobIntent.putExtra("_id", ((SearchResultsServiceModel) resumesList.get(position)).get_id());
//                jobIntent.putExtra("email_resume", ((SearchResultsServiceModel) resumesList.get(position)).getEmail());
//                jobIntent.setAction("fromSearch");
//                startActivity(jobIntent);
//
//            }
//        });
    }


    public void getResumessData(final String url){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(ResumesListActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        resumesList = new ArrayList<>();
        LiveData.getResumesFromServer(new ApplicationThread.OnComplete<List<SearchResultsServiceModel>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(ResumesListActivity.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }else
                        Toast.makeText(ResumesListActivity.this, "No results found", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    resumesList = (ArrayList) result;
                    if (null != resumesList && resumesList.size() > 0){

                        resumesList = (ArrayList) result;
                        CommonUtils.ResumeList = (ArrayList) result;
                        resumesListAdpter = new ResumesListAdpter(ResumesListActivity.this);
                        resumesListView.setAdapter(resumesListAdpter);
                        if (getIntent().getStringExtra("resumesLink")!= null){
                            getSupportActionBar().setTitle("Search Results" + "(" + resumesList.size() + ")");
                        }else if (getIntent().getStringExtra("appliedResumes")!= null){
                            getSupportActionBar().setTitle("Applied candidates" + "(" + resumesList.size() + ")");
                        }else if (getIntent().getStringExtra("SIMILARRESUMES_MAIL") != null){
                            getSupportActionBar().setTitle("Similar Proifiles" + "(" + resumesList.size() + ")");
                        }
                    }else {
                        Toast.makeText(ResumesListActivity.this, "No results found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_resumes_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        if (id == R.id.action_filter) {
            startActivity(new Intent(ResumesListActivity.this, FilterResumesActivity.class).putExtra("Filter","Filter"));
        }

        if (id == R.id.action_mail) {
            String emails = "";
            for (int i = 0 ; i < resumesList.size() ; i++){
                if (resumesList.get(i).getIsChecked())
                    emails = emails + resumesList.get(i).getEmail() +",";
            }
            if (!emails.equalsIgnoreCase(""))
            startActivity(new Intent(ResumesListActivity.this, MenuRootActivity.class).putExtra("type", "Send Email").putExtra("_id", "").putExtra("email_resume", emails.substring(0, emails.length() - 1)));
        }

        if (id == R.id.action_sms) {
            String emails = "";
            for (int i = 0 ; i < resumesList.size() ; i++){
                if (resumesList.get(i).getIsChecked())
                    emails = emails + resumesList.get(i).getEmail() +",";
            }
            if (!emails.equalsIgnoreCase(""))
            startActivity(new Intent(ResumesListActivity.this, MenuRootActivity.class).putExtra("type", "Send SMS").putExtra("_id", "").putExtra("email_resume", emails.substring(0, emails.length() - 1)));

        }

        return super.onOptionsItemSelected(item);
    }

    class ResumesListAdpter extends BaseAdapter {

        Context context;
        public ResumesListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.resume_listitem, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.resumeNAME);
            jobTitle.setText(Html.fromHtml("<b>" + resumesList.get(position).getResumeHeadLine()+ "</b>"));
            TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
            jobLoc.setText(Html.fromHtml("<b>Current Location: </b>"+ resumesList.get(position).getLocation()));
            TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
            jobComp.setText(Html.fromHtml("<b>Current Company: </b>"+ resumesList.get(position).getCurrentCompany()));
            TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
            jobExp.setText(Html.fromHtml("<b>Current Exp: </b>"+ resumesList.get(position).getExperience()+" Year(s)"));
            TextView keySkills = (TextView)convertView.findViewById(R.id.keyskills);
            keySkills.setText(Html.fromHtml("<b>Key Skills: </b>"+ resumesList.get(position).getKeySkills()));
            final CheckBox check = (CheckBox)convertView.findViewById(R.id.check);

            check.setChecked(resumesList.get(position).getIsChecked());

            check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resumesList.get(position).setIsChecked(check.isChecked());
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent jobIntent = new Intent(ResumesListActivity.this, ResumeDescriptionActivity.class);
                    jobIntent.putExtra("selectedPos", position);
                    jobIntent.putExtra("_id", ((SearchResultsServiceModel) resumesList.get(position)).get_id());
                    jobIntent.putExtra("email_resume", ((SearchResultsServiceModel) resumesList.get(position)).getEmail());
                    jobIntent.setAction("fromSearch");
                    startActivity(jobIntent);

                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return resumesList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return resumesList.get(position);
        }

    }
}
