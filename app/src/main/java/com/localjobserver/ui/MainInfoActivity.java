package com.localjobserver.ui;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import com.localjobserver.TrainingInstitute.TrainingInstituteRegistratiojn;
import com.localjobserver.chat.GcmIntentService;
import com.localjobserver.chat.LJSChatActivity;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.employee.EmployeeRegisterActivity;
import com.localjobserver.employee.ShareReferralActivity;
import com.localjobserver.jobsinfo.MainJobsInfoFragment;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.Config;
import com.localjobserver.setup.FreshersRegistration;
import com.localjobserver.setup.RegisterFragmentActivity;
import co.talentzing.R;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MainInfoActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private SharedPreferences appPrefs = null;
    private SharedPreferences gcmPrefs = null;
    private int userType = 0;
    private Bundle bundle1;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor_gcmPrefs;
    Dialog typedialog;
    private FragmentManager fragmentManager;
    private int seekerMachedSkillJob_count = 0;
    private int recruiterAlert_count = 0;
    private int badgeCount_Chat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_info);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp( R.id.navigation_drawer,(DrawerLayout) findViewById(R.id.drawer_layout));

        appPrefs =  this.getSharedPreferences("ljs_prefs",MODE_PRIVATE);
        gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        editor = appPrefs.edit();
        editor_gcmPrefs = gcmPrefs.edit();
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        }


    private void addContact(String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(Contacts.People.NUMBER, phone);
        values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        values.put(Contacts.People.LABEL, phone);
        values.put(Contacts.People.NAME, phone);
        Uri dataUri = getContentResolver().insert(Contacts.People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
        values.put(Contacts.People.NUMBER, phone);
        updateUri = getContentResolver().insert(updateUri, values);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
         fragmentManager = getSupportFragmentManager();
        switch (position){
            case  0:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (CommonUtils.getUserLoggedInType(MainInfoActivity.this) == 0){
                        if (CommonUtils.fromAppliedJobs){
                                        Intent jobIntent = new Intent(MainInfoActivity.this, JobDescriptionActivity.class);
                                        jobIntent.putExtra("selectedPos", CommonUtils.selectedPos_withotLogin);
                                        jobIntent.putExtra("_id", CommonUtils.id_withotLogin);
                                        jobIntent.setAction("fromSearch");
                                        startActivity(jobIntent);
//                                        CommonUtils.fromAppliedJobs = false;
                                    }else {
                            GcmBadgeCondition();
                            CommonUtils.fromAppliedJobs = false;
                        }
                    }else{
                        fragmentManager.beginTransaction().replace(R.id.container, new SearchResumesFragment()).commit();
                        GcmBadgeConditionRecruiter();
//                        fragmentManager.beginTransaction().replace(R.id.container, new SearchResumesFragment()).commit();
                    }
                }else{
                    if (getIntent().getStringExtra("searchJobs") == null)
                    fragmentManager.beginTransaction().replace(R.id.container, MapSeekerFragment.newInstance(0 + 1)).commit();
                    else
                        fragmentManager.beginTransaction().replace(R.id.container, new SearchResumesFragment()).commit();
                }

//                addContact("TZing","9879246414");

                break;
            case  1:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
                        startActivity(new Intent(MainInfoActivity.this, SeekerAdvanceSearchActivity.class));

                    }else{
//                        fragmentManager.beginTransaction().replace(R.id.container, new MyJobPostingsFragment()).commit();
                        startActivity(new Intent(MainInfoActivity.this, MyJobPostingsActivity.class));
                    }
                }else{
                    startActivity(new Intent(MainInfoActivity.this, SeekerAdvanceSearchActivity.class));
                }
                break;

            case  2:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        startActivity(new Intent(MainInfoActivity.this, SettingsActivity.class));
                        fragmentManager.beginTransaction().replace(R.id.container, MainJobsInfoFragment.newInstance(position + 1,"")).commit();
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, PostWalkinActivity.class).putExtra("walkin","walkin"));
                    }
                }else{
                    fragmentManager.beginTransaction().replace(R.id.container, new SearchResumesFragment()).commit();
                }
                break;
            case  3:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
                        startActivity(new Intent(MainInfoActivity.this, SettingsActivity.class));
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, VenuDetailsActivity.class));
                    }
                }else{
                    mTitle = "Login";
                    restoreActionBar();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(position + 1, "jr")).commit();
                }

                break;
            case  4:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        fragmentManager.beginTransaction().replace(R.id.container, new FeedBackFragment()).commit();
                        startActivity(new Intent(MainInfoActivity.this, FeedbackActivity.class));
                    }else if (userType == 1){
                        startActivity(new Intent(MainInfoActivity.this, CommunicationSendSmsEmailActivity.class));
                    }else if (userType == 4){
                        startActivity(new Intent(MainInfoActivity.this, PostWalkinActivity.class).putExtra("referral", "referral"));
                    }
                }else{
                    designTypeDialog();
                }
                break;
            case  5:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        designAboutDialog();http://docs.google.com/gview?embedded=true&url=<url of a supported doc>
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName","AboutUs").putExtra("url","https://www.talentzing.com/AboutUs.aspx"));
                    }else if (userType == 1){
                        startActivity(new Intent(MainInfoActivity.this, RepliedCanditadesActivity.class));
                    }else{
                        fragmentManager.beginTransaction().replace(R.id.container, new SavedResumesFragment()).commit();
                    }

                }else{
                    startActivity(new Intent(MainInfoActivity.this, FeedbackActivity.class));
                }
                break;
            case 6:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        startActivity(new Intent(getApplicationContext(), LoadFileActivity.class).putExtra("selectedObj","").setAction("gh"));
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.faqs)).putExtra("url","https://www.talentzing.com/FrequentlyAskedQuestions.aspx"));
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, PostWalkinActivity.class).putExtra("newjob", "newjob"));
                    }
                }else{
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName","AboutUs").putExtra("url","https://www.talentzing.com/AboutUs.aspx"));
                }
                break;
            case 7:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)) {
                    if (userType == 0) {
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName", getString(R.string.privacy_policy)).putExtra("url", "https://www.talentzing.com/PrivacyPolacy.aspx"));
                    } else {
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName","AboutUs").putExtra("url","https://www.talentzing.com/AboutUs.aspx"));
                    }
                }else{
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.faqs)).putExtra("url","https://www.talentzing.com/FrequentlyAskedQuestions.aspx"));
                    }

                break;
            case 8:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                if (userType == 0){
//                    Intent jobListIntent = new Intent(getApplicationContext(),JobListActivity.class);
//                    jobListIntent.putExtra("APPLIEDBYSMS", "Applied By Sms");
//                    startActivity(jobListIntent);
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.terms_conditions)).putExtra("url","https://www.talentzing.com/TermsAndConditions.aspx"));

                }else{
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.faqs)).putExtra("url","https://www.talentzing.com/FrequentlyAskedQuestions.aspx"));
                }
                }else
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.privacy_policy)).putExtra("url","https://www.talentzing.com/PrivacyPolacy.aspx"));


                break;
            case  9:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        startActivity(new Intent(MainInfoActivity.this, SeekerAdvanceSearchActivity.class));
                        CommonUtils.logoutDialog(userType,MainInfoActivity.this);
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.terms_conditions)).putExtra("url","https://www.talentzing.com/TermsAndConditions.aspx"));
                    }
                }else
                    startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName", getString(R.string.privacy_policy)).putExtra("url", "https://www.talentzing.com/PrivacyPolacy.aspx"));

                break;
            case  10:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.terms_conditions)).putExtra("url","https://www.talentzing.com/TermsAndConditions.aspx"));
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.terms_conditions)).putExtra("url","https://www.talentzing.com/TermsAndConditions.aspx"));
                    }
                }
                break;
            case  11:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//                        designLogoutDialog();
                        CommonUtils.logoutDialog(userType,MainInfoActivity.this);
                    }else{
                        CommonUtils.logoutDialog(userType,MainInfoActivity.this);                    }
                }
                break;
            case  12:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.faqs)).putExtra("url","https://www.talentzing.com/FrequentlyAskedQuestions.aspx"));
                    }
                }
                break;

            case  13:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//
                    }else{
                        startActivity(new Intent(MainInfoActivity.this, AboutUsActivity.class).putExtra("actionBarName",getString(R.string.terms_conditions)).putExtra("url","https://www.talentzing.com/TermsAndConditions.aspx"));
                    }
                }
                break;

            case  14:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){

                    }else  if (userType == 1){
                        CommonUtils.logoutDialog(userType,MainInfoActivity.this);
                    }else  if (userType == 4){
                        startActivity(new Intent(MainInfoActivity.this, ShareReferralActivity.class));
                    }
                }
                break;

            case 15:
                if (CommonUtils.isUserExisted && CommonUtils.isLoggedIn(MainInfoActivity.this)){
                    if (userType == 0){
//
                    }else{
                        CommonUtils.logoutDialog(userType,MainInfoActivity.this);
                    }
                }
                break;

            default:

                break;
        }
    }

    private void GcmBadgeCondition(){
        SharedPreferences gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = gcmPrefs.edit();
            badgeCount_Chat = gcmPrefs.getInt("badgeCount_Chat", 0);
            seekerMachedSkillJob_count = gcmPrefs.getInt("badgeCount_seekerMachedSkillJob", 0);
            recruiterAlert_count = gcmPrefs.getInt("badgeCount_recruiterAlert", 0);
        if (badgeCount_Chat == 0){

            if (seekerMachedSkillJob_count == 0)
                fragmentManager.beginTransaction().replace(R.id.container, MapSeekerFragment.newInstance(0 + 1)).commit();
            else {
                int remainingBadges =  gcmPrefs.getInt("badgeCount_Total", 0) - seekerMachedSkillJob_count;
                editor.putInt("badgeCount_seekerMachedSkillJob", 0);
                editor.putInt("badgeCount_Total", remainingBadges);
                editor.commit();
                ShortcutBadger.applyCount(MainInfoActivity.this,remainingBadges);
                fragmentManager.beginTransaction().replace(R.id.container, MainJobsInfoFragment.newInstance(1 + 1,"")).commit();
            }

        }else {

        }
    }

    private void GcmBadgeConditionRecruiter(){
        SharedPreferences gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = gcmPrefs.edit();
        badgeCount_Chat = gcmPrefs.getInt("badgeCount_Chat", 0);
        recruiterAlert_count = gcmPrefs.getInt("badgeCount_recruiterAlert", 0);
        if (recruiterAlert_count > 0){
            int remainingBadges =  gcmPrefs.getInt("badgeCount_Total", 0) - recruiterAlert_count;
            editor.putInt("badgeCount_recruiterAlert", 0);
            editor.putInt("badgeCount_Total", remainingBadges);
            editor.commit();
            ShortcutBadger.applyCount(MainInfoActivity.this,remainingBadges);

//                String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getAppliedResumesByJobId, ((JobsData) jobList.get(position)).get_id());
            String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getAppliedResumesByJobId, gcmPrefs.getString("recruiterAlertJobId",""));
            startActivity(new Intent(getApplicationContext(), ResumesListActivity.class).putExtra("appliedResumes", "").putExtra("appliedResumes", preparedUrltoGetResumes));
        }else {
//            fragmentManager.beginTransaction().replace(R.id.container, new SearchResumesFragment()).commit();
        }
    }



    public void designTypeDialog(){
        typedialog = new Dialog(this);
        typedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        typedialog.setContentView(R.layout.registrationtype);
        typedialog.setCanceledOnTouchOutside(true);

        Button seekerBtn = (Button)typedialog.findViewById(R.id.radioSeeker);
        Button recruiterBtn = (Button)typedialog.findViewById(R.id.radioRecruiter);
        Button fresherBtn = (Button)typedialog.findViewById(R.id.fresher);
        Button trainerBtn = (Button) typedialog.findViewById(R.id.trainer);
        Button employeeBtn = (Button) typedialog.findViewById(R.id.employee);
        seekerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainInfoActivity.this, RegisterFragmentActivity.class));
                typedialog.dismiss();
            }
        });

        recruiterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainInfoActivity.this, RecruiterRegistrationActivity.class));
                typedialog.dismiss();
            }
        });

        fresherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainInfoActivity.this, FreshersRegistration.class));
                typedialog.dismiss();
            }
        });
        trainerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1 = new Bundle();
                bundle1.putString("flag", "0");
                startActivity(new Intent(MainInfoActivity.this, TrainingInstituteRegistratiojn.class).putExtra("flag", bundle1).putExtra("POST","POST"));
                typedialog.dismiss();
            }
        });

        employeeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isNetworkAvailable(MainInfoActivity.this)) {
                    Intent i = new Intent(MainInfoActivity.this, EmployeeRegisterActivity.class);
                    i.putExtra("flag", 0);
                    startActivity(i);
                } else {
                    Toast.makeText(MainInfoActivity.this, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        typedialog.show();
    }

    public void onSectionAttached(String title) {
        mTitle = title;
        restoreActionBar();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_info, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CommonUtils.CancelNotification(getApplicationContext(),0);
    }
}
