package com.localjobserver.ui;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VenuDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VenuDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    private View rootView;
    private EditText mobileEdt, jobTitletxt, addressEdit;
    private RadioButton radiosms,radiomail;
    private TextView email_mobile_txt;
    private ProgressDialog progressDialog;
    private String configUrl = "";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VenuDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VenuDetailsFragment newInstance(String param1, String param2) {
        VenuDetailsFragment fragment = new VenuDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public VenuDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView = inflater.inflate(R.layout.fragment_venu_details, container, false);
         mobileEdt = (EditText)rootView.findViewById(R.id.mobileEdt);
         jobTitletxt = (EditText)rootView.findViewById(R.id.jobTitletxt);
         addressEdit = (EditText)rootView.findViewById(R.id.addressEdit);
        radiosms = (RadioButton)rootView.findViewById(R.id.radiosms);
        radiomail = (RadioButton)rootView.findViewById(R.id.radiomail);
        email_mobile_txt = (TextView)rootView.findViewById(R.id.email_mobile_txt);

        mobileEdt.setKeyListener(DigitsKeyListener.getInstance("0123456789,"));
        email_mobile_txt.setText("Email(s):");
        email_mobile_txt.setText("Phone Number(s):");

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        Button submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateVenueDetails()) {
                    if (radiosms.isChecked())
                        configUrl = String.format(Config.LJS_BASE_URL + Config.sendVenue, mobileEdt.getText().toString(), jobTitletxt.getText().toString(), addressEdit.getText().toString());
                    else{
                        String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getActivity());
                        configUrl = String.format(Config.LJS_BASE_URL + Config.sendVenueDetailsAsEmail, mobileEdt.getText().toString(), jobTitletxt.getText().toString(), addressEdit.getText().toString(),CompanyMail);

                    }
                    uploadVenueDetails();
                }
            }
        });


        radiosms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEdt.setText("");
                email_mobile_txt.setText("Phone Number(s):");
                mobileEdt.setHint("Mobile numbers with comma(,) separator");
                mobileEdt.setKeyListener(DigitsKeyListener.getInstance("0123456789,"));
            }
        });

        radiomail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEdt.setText("");
                email_mobile_txt.setText("Email(s):");
                mobileEdt.setHint("Emails with comma(,) separator");
                mobileEdt.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );

            }
        });

        return rootView;
    }



    public boolean validateVenueDetails(){
        Form mForm = new Form(getActivity());
        if (radiosms.isChecked()){
            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.mobileEdt)).validate(NotEmpty.build(getActivity())));
            if (mobileEdt.getText().toString().length() < 10){
                mobileEdt.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>"));
                return  false;
            }
            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.jobTitletxt)).validate(NotEmpty.build(getActivity())));

        } else
//            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.mobileEdt)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
//        mForm.addField(Field.using((MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.jobTitletxt)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.addressEdit)).validate(NotEmpty.build(getActivity())));

        return (mForm.isValid()) ? true : false;
    }

    public boolean validateMobileNo(){
        EditText tv = (EditText)rootView.findViewById(R.id.mobileEdt);

        if (CommonUtils.isValidMobile(tv.getText().toString(), tv))
            return true;
        else
            return  false;
    }
    public void uploadVenueDetails() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadVenueDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (radiosms.isChecked())
                        Toast.makeText(getActivity(),"Sms has been sent failed.",Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(),"Email has been sent failed.",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null && result.toString().equalsIgnoreCase("true")) {
                    if (radiosms.isChecked())
                    Toast.makeText(getActivity(),"Sms has been sent successfully.",Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(),"Email has been sent successfully.",Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                    mobileEdt.setText("");
//                    jobTitletxt.setText("");
//                    addressEdit.setText("");
                }else {
                    if (radiosms.isChecked())
                    Toast.makeText(getActivity(),"Sms has been sent failed",Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(),"Email has been sent failed",Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                }
            }
        }, configUrl);

    }
}
