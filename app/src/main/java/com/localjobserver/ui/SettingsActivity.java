package com.localjobserver.ui;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.networkutils.ApplicationThread;
import co.talentzing.R;

public class SettingsActivity extends ActionBarActivity {

    private LinearLayout visibilitysettingsLL,privacysettingsLL,changepasswordLL;
    private ActionBar actionBar = null;
    private SharedPreferences appPrefs;
    private SharedPreferences gcmPrefs = null;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor_gcmPrefs;
    private int userType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Settings");
        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
         editor = appPrefs.edit();
        editor_gcmPrefs = gcmPrefs.edit();

        visibilitysettingsLL = (LinearLayout)findViewById(R.id.visibilitysettingsLL);
        if (userType == 1 || userType == 3)
            visibilitysettingsLL.setVisibility(View.GONE);

        visibilitysettingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBar.setTitle("Visibility Settings");
                FragmentManager fm = getSupportFragmentManager();
                VisibilitySettingsFragment newFragment = new VisibilitySettingsFragment();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(android.R.id.content, newFragment).addToBackStack(null).commit();
            }
        });
        privacysettingsLL = (LinearLayout)findViewById(R.id.privacysettingsLL);
        privacysettingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBar.setTitle("Privacy Settings");
                FragmentManager fm = getSupportFragmentManager();
                PrivacySettingsFragment newFragment = new PrivacySettingsFragment();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(android.R.id.content, newFragment).addToBackStack(null).commit();
            }
        });
        changepasswordLL = (LinearLayout)findViewById(R.id.changepasswordLL);
        changepasswordLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBar.setTitle("Change Password");
                FragmentManager fm = getSupportFragmentManager();
                ChangePasswordFragment newFragment = new ChangePasswordFragment();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(android.R.id.content, newFragment).addToBackStack(null).commit();
            }
        });

        Button logoutBtn = (Button)findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                designLogoutDialog();
                CommonUtils.logoutDialog(userType,SettingsActivity.this);
            }
        });
    }

    Dialog logoutdialog;
    public void designLogoutDialog(){
        logoutdialog = new Dialog(SettingsActivity.this);
        logoutdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutdialog.setContentView(R.layout.logout_layout);
        logoutdialog.setCanceledOnTouchOutside(true);
        logoutdialog.findViewById(R.id.yesBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                ApplicationThread.dbPost("Delete", "Delete Record", new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (CommonUtils.getUserLoggedInType(SettingsActivity.this) == 0) {
                            DatabaseAccessObject.deleteRecord("JobSeekers",SettingsActivity.this);
                        } else if (CommonUtils.getUserLoggedInType(SettingsActivity.this) == 1) {
                            DatabaseAccessObject.deleteRecord("RecuiterRegistration",SettingsActivity.this);
                        } else if (userType == 3){
                            DatabaseAccessObject.deleteRecord("TrainersRegistration",SettingsActivity.this);
                        } else {
                            DatabaseAccessObject.deleteRecord("FreshersRegistration",SettingsActivity.this);
                        }
                        editor.clear();
                        editor_gcmPrefs.clear();
                        editor.commit();
                        editor_gcmPrefs.commit();


                    }
                });

                SharedPreferences.Editor editor = appPrefs.edit();
                editor.putBoolean("isLogin", false);
                editor.putBoolean("isFresherLogin", false);
                editor.putBoolean("isTrainerLogin", false);
                editor.commit();
                startActivity(new Intent(SettingsActivity.this, LoginScreen.class).setAction("fromLogout").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();



            }
        });

        logoutdialog.findViewById(R.id.noBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                logoutdialog.dismiss();
            }
        });
        logoutdialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (actionBar.getTitle().toString().equalsIgnoreCase("Settings")) {
                    this.finish();

                }else{
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    actionBar.setTitle("Settings");
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }else{
            this.finish();
        }
    }

}
