package com.localjobserver.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyJobPostingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyJobPostingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyJobPostingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView list;
    private  ArrayList jobList;
    private MyJobsPostingAdapter myJobsPostingAdapter;
    private ProgressDialog progressDialog;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyJobPostingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyJobPostingsFragment newInstance(String param1, String param2) {
        MyJobPostingsFragment fragment = new MyJobPostingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MyJobPostingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_job_postings, container, false);
        list = (ListView)view.findViewById(R.id.mypostinglist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                LinearLayout appliedLay = (LinearLayout)view.findViewById(R.id.appliedLay);
//                appliedLay.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getAppliedResumesByJobId, ((JobsData) jobList.get(position)).get_id());
//                        startActivity(new Intent(getActivity(), ResumesListActivity.class).putExtra("appliedResumes", "").putExtra("appliedResumes", preparedUrltoGetResumes));
//                    }
//                });
                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("selectedPos", position).putExtra("_id", ((JobsData) jobList.get(position)).get_id()).setAction("fromPosting"));

            }
        });
        String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getActivity());
        String url = String.format(Config.LJS_BASE_URL + Config.getMyJobPostings,CompanyMail, CommonUtils.getUserEmail(getActivity()));

        getJobsData(url);
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
//            mListener = (OnFragmentInteractionListener) activity;
            ((MainInfoActivity) activity).onSectionAttached("Manage Postings");
        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    public void getJobsData(final String url){

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("No posted jobs", getActivity());
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null ) {
                    jobList = (ArrayList) result;
                    CommonUtils.jobList = (ArrayList) result;
                    myJobsPostingAdapter = new MyJobsPostingAdapter(getActivity(), jobList);
                    list.setAdapter(myJobsPostingAdapter);
                    progressDialog.dismiss();
                    MyJobPostingsActivity.actionBar.setTitle("Manage Postings" + "(" + jobList.size() + ")");
                }
            }
        }, url);
    }
}
