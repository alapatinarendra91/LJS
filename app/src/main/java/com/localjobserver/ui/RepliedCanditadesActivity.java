package com.localjobserver.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.localjobserver.adapter.SmsEmailAdapter;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.menu.SendSMSFragment;
import com.localjobserver.models.JobsData;
import com.localjobserver.models.JobsListToShareModel;
import com.localjobserver.models.LoadCandidatesModel;
import com.localjobserver.models.LoadRepliedJobTitleModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import co.talentzing.R;

public class RepliedCanditadesActivity extends ActionBarActivity implements View.OnClickListener, SmsEmailAdapter.OnCartChangedListener {
    private ActionBar actionBar = null;
    private SharedPreferences appPrefs;
    private Spinner email_smsRepliedSpin,job_smsTitleSpin;
    private RadioButton repliedRadio,appliedRadio;
    private RadioGroup repliedAppliedGroup;
    private LinearLayout jobSmsLay;
    private TextView job_smsTitleTxt;
    private ListView smsEmalList;
    private SmsEmailAdapter mSmsEmailAdapter;
    private ProgressDialog progressDialog;
    public ArrayList<LoadCandidatesModel> LoadCandidatesList = new ArrayList<>();
    private LinkedHashMap<String, String> LoadRepliedSmsJobMap,LoadAppliedSmsJobMap,LoadEmailJobTitlesMap;
    private Dialog dialog_comments;
    private EditText dialog_commentsEdt;
    private TextView dialog_titleTxt;
    private Button dialog_saveBtn;
    private int selectedPos = 0;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replied_canditades);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("SMS/EMAIL Replied Candidates");
        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);

        initViews();
        setViews();
    }

    private void initViews() {
        email_smsRepliedSpin = (Spinner) findViewById(R.id.email_smsRepliedSpin);
        job_smsTitleSpin = (Spinner) findViewById(R.id.job_smsTitleSpin);
        repliedRadio = (RadioButton) findViewById(R.id.repliedRadio);
        appliedRadio = (RadioButton) findViewById(R.id.appliedRadio);
        job_smsTitleTxt = (TextView) findViewById(R.id.job_smsTitleTxt);
        jobSmsLay = (LinearLayout) findViewById(R.id.jobSmsLay);
        repliedAppliedGroup = (RadioGroup) findViewById(R.id.repliedAppliedGroup);
        smsEmalList = (ListView) findViewById(R.id.smsEmalList);
    }

    private void setViews() {
        email_smsRepliedSpin.setOnItemSelectedListener(spinListener);
        job_smsTitleSpin.setOnItemSelectedListener(spinListener);
        repliedRadio.setOnClickListener(this);
        appliedRadio.setOnClickListener(this);

        dialog_comments = CommonUtils.dialogIntialize(RepliedCanditadesActivity.this,R.layout.dialog_other);

        dialog_commentsEdt = (EditText) dialog_comments.findViewById(R.id.dialog_otherEdt);
        dialog_titleTxt = (TextView) dialog_comments.findViewById(R.id.dialog_titleTxt);
        dialog_saveBtn = (Button) dialog_comments.findViewById(R.id.dialog_saveBtn);
        dialog_titleTxt.setText("Comments");
        dialog_commentsEdt.setLines(4);
        dialog_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!dialog_commentsEdt.getText().toString().equalsIgnoreCase("")){

                    UpdateSMS_EmailComments();
                    dialog_comments.dismiss();
                }else
                    CommonUtils.showToast("Please specify Comment",RepliedCanditadesActivity.this);

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        this.finish();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.repliedRadio:
                if (LoadRepliedSmsJobMap != null)
                    job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadRepliedSmsJobMap, "JOB Title")));
                else
                getLoadRepliedSMSJobTitles(Config.LoadRepliedSMSJobTitles);
                job_smsTitleTxt.setText("JOB Title");
                break;

            case R.id.appliedRadio:

                if (LoadAppliedSmsJobMap != null)
                    job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadAppliedSmsJobMap, "SMS Title")));
                else
                    getLoadRepliedSMSJobTitles(Config.LoadAppliedSMSJobTitles);
                job_smsTitleTxt.setText("SMS Title");

                break;
        }
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.email_smsRepliedSpin:
                    if (position == 0){
                        repliedAppliedGroup.setVisibility(View.GONE);
                        jobSmsLay.setVisibility(View.GONE);
                        job_smsTitleSpin.setSelection(0);
                    }else if (position == 1){
                        repliedAppliedGroup.setVisibility(View.GONE);
                        jobSmsLay.setVisibility(View.VISIBLE);
                        if (LoadEmailJobTitlesMap == null)
                            getLoadRepliedSMSJobTitles(Config.LoadEmailJobTitles);
                        else
                        job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadEmailJobTitlesMap, "JOB Title")));

                    }else if (position == 2){
                        repliedAppliedGroup.setVisibility(View.VISIBLE);
                        jobSmsLay.setVisibility(View.VISIBLE);
                    }

                    break;
                case R.id.job_smsTitleSpin:
                    if (position != 0 ){
                        if (repliedAppliedGroup.isShown() && repliedRadio.isChecked() && LoadRepliedSmsJobMap != null)
                        getCandidatesData(String.format(Config.LJS_BASE_URL + Config.LoadSMSRepliedGridDetails,LoadRepliedSmsJobMap.keySet().toArray(new String[LoadRepliedSmsJobMap.size()])[position-1]));
                    else if (repliedAppliedGroup.isShown() && appliedRadio.isChecked() && LoadAppliedSmsJobMap != null)
                            getCandidatesData(String.format(Config.LJS_BASE_URL + Config.LoadSMSRepliedGridDetails,LoadAppliedSmsJobMap.keySet().toArray(new String[LoadAppliedSmsJobMap.size()])[position-1]));
                        else if (LoadEmailJobTitlesMap != null)
                        getCandidatesData(String.format(Config.LJS_BASE_URL + Config.LoadEmailRepliedCandidatesData,LoadEmailJobTitlesMap.keySet().toArray(new String[LoadEmailJobTitlesMap.size()])[position-1]));

                    }else
                        smsEmalList.setVisibility(View.GONE);

                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public void getLoadRepliedSMSJobTitles(final String configUrl){
        String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getApplicationContext());
         url = String.format(Config.LJS_BASE_URL + configUrl, CompanyMail,CommonUtils.getUserEmail(RepliedCanditadesActivity.this));
//        String url = String.format(Config.LJS_BASE_URL + configUrl, "recruitment@vishist.com","sowji@vishist.com");

//        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RepliedCanditadesActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
//        }
            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                        progressDialog.dismiss();
                    LinkedHashMap<String, String> example  = new LinkedHashMap<String, String>();
                    job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(example, "Job Title")));
                    Toast.makeText(getApplicationContext(),"No Data found.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    if (configUrl.equalsIgnoreCase(Config.LoadRepliedSMSJobTitles)){
                        LoadRepliedSmsJobMap = (LinkedHashMap<String,String>)result;
                        job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadRepliedSmsJobMap, "Job Title")));

                    }else if (configUrl.equalsIgnoreCase(Config.LoadAppliedSMSJobTitles)){
                        LoadAppliedSmsJobMap = (LinkedHashMap<String,String>)result;
                        job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadAppliedSmsJobMap, "SMS Title")));

                    }else  if (configUrl.equalsIgnoreCase(Config.LoadEmailJobTitles)){
                        LoadEmailJobTitlesMap = (LinkedHashMap<String,String>)result;
                        job_smsTitleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(RepliedCanditadesActivity.this,CommonUtils.fromMap(LoadEmailJobTitlesMap, "JOB Title")));
                    }

                    progressDialog.dismiss();
                }
            }
        },url, CommonKeys.arrLoadRepliedSMSJobTitles);
    }

    public void getCandidatesData(final String url){
            progressDialog = new ProgressDialog(RepliedCanditadesActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        progressDialog.show();

        LoadCandidatesList = new ArrayList<>();
        LiveData.getAppliedCandidatesDetails(new ApplicationThread.OnComplete<List<LoadCandidatesModel>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("No job(s) are present",RepliedCanditadesActivity.this);
                    return;
                }
                if (result != null) {
                    LoadCandidatesList = (ArrayList) result;
                    if (LoadCandidatesList.size() == 0){
                        Toast.makeText(getApplicationContext(),"No results found",Toast.LENGTH_LONG).show();
                    }
                    mSmsEmailAdapter = new SmsEmailAdapter(RepliedCanditadesActivity.this, LoadCandidatesList);
                    smsEmalList.setAdapter(mSmsEmailAdapter);
                    mSmsEmailAdapter.setOnCartChangedListener(RepliedCanditadesActivity.this);
                    smsEmalList.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                }
            }
        }, url);
    }

    public void UpdateSMS_EmailComments(){
            progressDialog = new ProgressDialog(RepliedCanditadesActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        progressDialog.show();

        String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getApplicationContext());

        if (email_smsRepliedSpin.getSelectedItemPosition() == 1)
       url =  String.format(Config.LJS_BASE_URL + Config.UpdateEmailComments,
                LoadEmailJobTitlesMap.keySet().toArray(new String[LoadEmailJobTitlesMap.size()])[job_smsTitleSpin.getSelectedItemPosition()-1],
                CompanyMail,CommonUtils.getUserEmail(RepliedCanditadesActivity.this),LoadCandidatesList.get(selectedPos).getEmail(),dialog_commentsEdt.getText().toString());
        else if (email_smsRepliedSpin.getSelectedItemPosition() == 2){
            if (repliedRadio.isChecked())
                url =   String.format(Config.LJS_BASE_URL + Config.UpdateSMSComments,LoadRepliedSmsJobMap.keySet().toArray(new String[LoadRepliedSmsJobMap.size()])[job_smsTitleSpin.getSelectedItemPosition()-1],
                        dialog_commentsEdt.getText().toString(),LoadCandidatesList.get(selectedPos).getContactNo());
            else
                url =   String.format(Config.LJS_BASE_URL + Config.UpdateSMSComments,LoadAppliedSmsJobMap.keySet().toArray(new String[LoadAppliedSmsJobMap.size()])[job_smsTitleSpin.getSelectedItemPosition()-1],
                        dialog_commentsEdt.getText().toString(),LoadCandidatesList.get(selectedPos).getContactNo());
        }


        LiveData.uploadVenueDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Comments has been update failed.",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null && result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(getApplicationContext(),"Comments has been update successfully.",Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                }
                LoadCandidatesList.get(selectedPos).setComments(dialog_commentsEdt.getText().toString());
                mSmsEmailAdapter.notifyDataSetChanged();
                dialog_commentsEdt.setText("");
            }
        }, url);
    }

    @Override
    public void setCartClickListener(String clickItem,final int selectPos) {
        selectedPos = selectPos;
        if (clickItem.contains("call")){
            startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + LoadCandidatesList.get(selectPos).getContactNo())));
        }else if (clickItem.contains("comments")){
            dialog_comments.show();
        }
    }

}
