package com.localjobserver.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import co.talentzing.R;

public class CommunicationSendSmsEmailActivity extends ActionBarActivity {
    private ActionBar actionBar = null;
    private EditText mobileEdt, jobTitletxt, addressEdit;
    private RadioButton radiosms,radiomail;
    private TextView email_mobile_txt;
    private ProgressDialog progressDialog;
    private String configUrl = "";
    private Button submitBtn,cancelBtn;
    private SharedPreferences appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication_send_sms_email);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Communication to Candidates");
        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);

        initViews();
        setViews();
        
    }

    private void initViews() {
        mobileEdt = (EditText)findViewById(R.id.mobileEdt);
        jobTitletxt = (EditText)findViewById(R.id.jobTitletxt);
        addressEdit = (EditText)findViewById(R.id.addressEdit);
        radiosms = (RadioButton)findViewById(R.id.radiosms);
        radiomail = (RadioButton)findViewById(R.id.radiomail);
        email_mobile_txt = (TextView)findViewById(R.id.email_mobile_txt);

        mobileEdt.setKeyListener(DigitsKeyListener.getInstance("0123456789,"));
        email_mobile_txt.setText("Email(s):");
        email_mobile_txt.setText("Phone Number(s):");

         cancelBtn = (Button)findViewById(R.id.cancelBtn);
         submitBtn = (Button)findViewById(R.id.submitBtn);

    }

    private void setViews() {

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateVenueDetails()) {
                    String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getApplicationContext());
                    String RecruiterContactNo = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("ContactNo","SubUserRegistration"),getApplicationContext());
                    if (radiosms.isChecked())
                        configUrl = String.format(Config.LJS_BASE_URL + Config.sendSMSCommunicationToCandidates, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), CompanyMail, RecruiterContactNo,mobileEdt.getText().toString(),
                                jobTitletxt.getText().toString(),addressEdit.getText().toString());
                    else{
                        configUrl = String.format(Config.LJS_BASE_URL + Config.sendEmailCommunicationToCandidates,appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), CompanyMail, mobileEdt.getText().toString(), addressEdit.getText().toString());

                    }
                    uploadVenueDetails();
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        radiosms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEdt.setText("");
                mobileEdt.setMaxLines(10);
                email_mobile_txt.setText("Phone Number(s):");
                mobileEdt.setHint("Mobile numbers with comma(,) separator");
                mobileEdt.setKeyListener(DigitsKeyListener.getInstance("0123456789,"));
            }
        });

        radiomail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileEdt.setText("");
                email_mobile_txt.setText("Email(s):");
                mobileEdt.setHint("Emails with comma(,) separator");
                mobileEdt.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        this.finish();


    }
    public boolean validateVenueDetails(){
        Form mForm = new Form(CommunicationSendSmsEmailActivity.this);
        if (radiosms.isChecked()){
            mForm.addField(Field.using((EditText) findViewById(R.id.mobileEdt)).validate(NotEmpty.build(getApplicationContext())));
            if (mobileEdt.getText().toString().length() < 10){
                mobileEdt.setError(Html.fromHtml("<font color='red'>" + "Please specify a valid contact number" + "</font>"));
                return  false;
            }
            mForm.addField(Field.using((EditText) findViewById(R.id.jobTitletxt)).validate(NotEmpty.build(getApplicationContext())));

        } else
//            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.mobileEdt)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
//        mForm.addField(Field.using((MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using((EditText) findViewById(R.id.jobTitletxt)).validate(NotEmpty.build(getApplicationContext())));
        mForm.addField(Field.using((EditText) findViewById(R.id.addressEdit)).validate(NotEmpty.build(getApplicationContext())));

        return (mForm.isValid()) ? true : false;
    }

    public boolean validateMobileNo(){
        EditText tv = (EditText)findViewById(R.id.mobileEdt);

        if (CommonUtils.isValidMobile(tv.getText().toString(), tv))
            return true;
        else
            return  false;
    }
    public void uploadVenueDetails() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CommunicationSendSmsEmailActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadVenueDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(getApplicationContext(),radiosms.isChecked() ? " has been sent failed" : "Email has been sent failed",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (result != null && result.toString().equalsIgnoreCase("true")) {
                    Toast.makeText(getApplicationContext(),radiosms.isChecked() ? "Sms has been sent successfully" : "Email has been sent successfully",Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                    mobileEdt.setText("");
                }else {
                        Toast.makeText(getApplicationContext(),radiosms.isChecked() ? "Sms has been sent failed" : "Email has been sent failed",Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();
                }
            }
        }, configUrl);

    }
}

