package com.localjobserver.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.chat.LJSChatActivity;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.freshers.MailFresherActivity;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JobDescriptionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobDescriptionFragment#} factory method to
 * create an instance of this fragment.
 */
public class JobDescriptionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private View rootView;
    public ArrayList<JobsData> jobList;
    public JobsData jobdataObj;
    private TextView  tvcompanyname,noOfPosTxt,tvexperience,tvtvannualsal,tvpostedon,tvjobdesc,tvcomptype,
            tvcompname,tvendclient,tvnoticeperiod,tvemail,tvcontactnumber,tvlandline,
            tvaddress,tvlocation,tvcontactperson,tvindustrytype,tvroles,qualificationTxt,tvkeyskills,tvJobName;
    private Button similarJobs;
    private Bundle bundle1;

    public JobDescriptionFragment() {
        // Required empty public constructor
    }
    private ProgressDialog progressDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobdataObj = (JobsData)getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_job_description, container, false);
        similarJobs=(Button) rootView.findViewById(R.id.similarJobs);
        tvJobName=(TextView)rootView.findViewById(R.id.jobNAME);
        tvcompanyname=(TextView)rootView.findViewById(R.id.companyname);
        noOfPosTxt=(TextView)rootView.findViewById(R.id.noOfPosTxt);
        tvexperience=(TextView)rootView.findViewById(R.id.expJob);
        tvtvannualsal=(TextView)rootView.findViewById(R.id.anulSal);
        tvpostedon=(TextView)rootView.findViewById(R.id.jobPosted);
        tvjobdesc=(TextView)rootView.findViewById(R.id.jobDescription);
        tvcomptype=(TextView)rootView.findViewById(R.id.txtcompanytype);
        tvcompname=(TextView)rootView.findViewById(R.id.compName);
        tvendclient=(TextView)rootView.findViewById(R.id.endClient);
        tvnoticeperiod=(TextView)rootView.findViewById(R.id.noticePeriod);
        tvemail=(TextView)rootView.findViewById(R.id.emailTxt);
        tvcontactnumber=(TextView)rootView.findViewById(R.id.contactNumTxt);
        tvlandline=(TextView)rootView.findViewById(R.id.landNumTxt);
        tvaddress=(TextView)rootView.findViewById(R.id.addrTxt);
        tvlocation=(TextView)rootView.findViewById(R.id.locationTxt);
        tvcontactperson=(TextView)rootView.findViewById(R.id.contactTxt);
        tvindustrytype=(TextView)rootView.findViewById(R.id.indTypeTxt);
        tvroles=(TextView)rootView.findViewById(R.id.rolesTxt);
        qualificationTxt=(TextView)rootView.findViewById(R.id.qualificationTxt);
        tvkeyskills=(TextView)rootView.findViewById(R.id.keySkillsTxt);

        ImageView onlineStatus = (ImageView)rootView.findViewById(R.id.onlineStatus);
        onlineStatus.setImageResource((((null != jobdataObj.getIsRecruiterOnline()) && (jobdataObj.getIsRecruiterOnline().equalsIgnoreCase("True"))) ? R.drawable.online : R.drawable.offline));

        onlineStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != jobdataObj.getIsRecruiterOnline() && jobdataObj.getIsRecruiterOnline().equalsIgnoreCase("True")) {
                    if (CommonUtils.isLoggedIn(getActivity())) {
                        Intent i = new Intent(getActivity(), LJSChatActivity.class);
                        Bundle msgBundle = new Bundle();
                        msgBundle.putString("fromEmailId", "" + CommonUtils.getUserEmail(getActivity()));
                        msgBundle.putString("toEmailId", jobdataObj.getEmail());
                        i.putExtras(msgBundle);
                        startActivity(i);
                    }
                }
            }
        });

        ImageView sendEmail = (ImageView)rootView.findViewById(R.id.emailImg);
        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (CommonUtils.isLoggedIn(getActivity())){
//                    getSendData(0);
                    bundle1 = new Bundle();
                    bundle1.putString("ID", jobdataObj.get_id());
                    bundle1.putString("title", jobdataObj.getJobTitle());
                    bundle1.putString("TYPE", "Seekers Email");
                    startActivity(new Intent(getActivity(), MailFresherActivity.class).putExtra("type", "Seekers Email").putExtra("ID", bundle1));
//                }else
//                    CommonUtils.showToast("Please Login first",getActivity());
            }
        });

        ImageView sms = (ImageView)rootView.findViewById(R.id.sms);
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1 = new Bundle();
                bundle1.putString("ID", jobdataObj.get_id());
                bundle1.putString("title", jobdataObj.getJobTitle());
                bundle1.putString("TYPE", "Seekers Sms");
                startActivity(new Intent(getActivity(), MailFresherActivity.class).putExtra("type", "Seekers Sms").putExtra("ID", bundle1));
//                }
            }
        });

        ImageView sendCALL = (ImageView)rootView.findViewById(R.id.callImg);
        sendCALL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number="+91"+jobdataObj.getPhone();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+number));
                startActivity(callIntent);
            }
        });

//        onlineStatus.setVisibility(View.VISIBLE);
//        sendEmail.setVisibility(View.VISIBLE);
//        sendCALL.setVisibility(View.VISIBLE);
//        similarJobs.setText("   Similar Jobs : "+jobdataObj.getSimilarJobsCount()+"  ");
        similarJobs.setText("   Similar Jobs ");
        similarJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (!jobdataObj.getSimilarJobsCount().equalsIgnoreCase("0"))
                startActivity(new Intent(getActivity(),JobListActivity.class).putExtra("SIMILARJOBS_ID",jobdataObj.get_id()));
            }
        });

        if (CommonUtils.getUserLoggedInType(getActivity()) == 1 || CommonUtils.getUserLoggedInType(getActivity()) == 4){
            onlineStatus.setVisibility(View.GONE);
            sendEmail.setVisibility(View.GONE);
            sendCALL.setVisibility(View.GONE);
            sms.setVisibility(View.GONE);
            similarJobs.setVisibility(View.GONE);
        } else {
            onlineStatus.setVisibility(View.VISIBLE);
            sendEmail.setVisibility(View.VISIBLE);
            sendCALL.setVisibility(View.VISIBLE);
            sms.setVisibility(View.VISIBLE);
        }

        bindData();
        return rootView;
    }

    private void bindData()
    {
        Log.i("id is ", "JOb id : " + jobdataObj.get_id());
        tvJobName.setText(":" + jobdataObj.getJobTitle());
        tvcompanyname.setText(": "+jobdataObj.getCompanyName());
        noOfPosTxt.setText(": "+jobdataObj.getJobPositions());
//        tvcompanyname.setText(jobdataObj.get_id());
        tvcompname.setText(": "+jobdataObj.getCompanyName());
        tvexperience.setText(": "+ jobdataObj.getExpMin() + " Yrs To " + jobdataObj.getExpMax()+" Yrs");
        tvtvannualsal.setText(": "+jobdataObj.getMinSal()+" L To "+jobdataObj.getMaxSal()+" L");
        tvpostedon.setText(": "+CommonUtils.getCleanDate(jobdataObj.getPostDate()));
        if(jobdataObj.getJobDescription() == null || jobdataObj.getJobDescription().equalsIgnoreCase("") || jobdataObj.getJobDescription().equalsIgnoreCase("null") || jobdataObj.getJobDescription().length()==0)
        tvjobdesc.setText("N/A");
        else
//            tvjobdesc.setText(jobdataObj.getJobDescription());
        tvjobdesc.setText(Html.fromHtml(Html.fromHtml(jobdataObj.getJobDescription()).toString()));

        tvcomptype.setText("");
        tvendclient.setText(": "+jobdataObj.getEndClient());
        tvnoticeperiod.setText(": "+jobdataObj.getNoticePeriod()+" Days");
        tvemail.setText(": "+jobdataObj.getEmail());
        tvcontactnumber.setText(": "+jobdataObj.getPhone());
        tvlandline.setText(": "+jobdataObj.getContactNo_Landline());
        tvaddress.setText(": "+jobdataObj.getAddress());
        tvlocation.setText(": "+jobdataObj.getLocation());
        tvcontactperson.setText(": "+jobdataObj.getContactPerson());
        tvindustrytype.setText(": " + jobdataObj.getIndustry());
        tvroles.setText(": "+jobdataObj.getRole());
        qualificationTxt.setText(": "+jobdataObj.getQualification());
        tvkeyskills.setText(": "+jobdataObj.getKeySkills());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public void getSendData(final int sendType){
        String sendUrl = "";
        sendUrl = (sendType == 0) ? Config.sendEmail : Config.sendSMS;

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.sendJobValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(),""+((sendType == 0) ? "Email " : "SMS ")+"sent Failed  .",Toast.LENGTH_SHORT).show();
                    if (result != null) {
                    }
                    return;
                }


                Log.i("","Send emailUrl iss  :"+result.toString());

                if (result != null && result.toString().equalsIgnoreCase("true")) {
                    Toast.makeText(getActivity(),""+((sendType == 0) ? "Email " : "SMS ")+" sent successfully.",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }else {
                    Toast.makeText(getActivity(),""+((sendType == 0) ? "Email " : "SMS ")+"sent Failed  .",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }

        }, String.format(Config.LJS_BASE_URL + sendUrl, jobdataObj.get_id(), CommonUtils.getUserEmail(getActivity())));


    }

}
