package com.localjobserver.ui;

/**
 * Created by admin on 02-04-2015.
 */

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

public class ChangePasswordFragment extends Fragment {

    private EditText existingpwd,newpwd,confirmpwd;
    private Button profile_details_submit_btn;
    private ImageView eye_image;

    private View rootView;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private SharedPreferences appPrefs;
    private int userType = 0;
    private boolean eye_var = false;
    private String configUrl = "";

    public  ChangePasswordFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_change_password, container, false);
        existingpwd=(EditText)rootView.findViewById(R.id.existingpwd);
        newpwd=(EditText)rootView.findViewById(R.id.newpwd);
        confirmpwd=(EditText)rootView.findViewById(R.id.confirmpwd);
        eye_image=(ImageView)rootView.findViewById(R.id.eye_image);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        profile_details_submit_btn=(Button)rootView.findViewById(R.id.submitBtn);

//        if (userType ==0){
////            configUrl = Config.changePassword;
//            String.format(Config.LJS_BASE_URL + Config.changePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString())
//        }else if (userType ==1){
//            String companyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId", "SubUserRegistration"), getActivity());
////            configUrl = String.format(Config.LJS_BASE_URL + Config.changeSubUserPassword ,companyMail, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString());
//            String.format(Config.LJS_BASE_URL + Config.changeSubUserPassword ,companyMail , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString())
//
//        }if (userType ==2){
////            configUrl = Config.fresherChangePassword;
//            String.format(Config.LJS_BASE_URL + Config.fresherChangePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString())
//
//        }if (userType ==3){
////            configUrl = Config.trainerChangePassword;
//            String.format(Config.LJS_BASE_URL + Config.trainerChangePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString())
//
//        }

        LinearLayout parentId = (LinearLayout)rootView.findViewById(R.id.parentId);

        parentId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        eye_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eye_var == false) {
                    eye_var = true;
                    eye_image.setImageResource(R.drawable.eye_hidden);
                    existingpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    newpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    confirmpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    existingpwd.setSelection(existingpwd.getText().length());
                    newpwd.setSelection(newpwd.getText().length());
                    confirmpwd.setSelection(confirmpwd.getText().length());
                }else {
                    eye_var = false;
                    eye_image.setImageResource(R.drawable.eye_show);
                    existingpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    newpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    confirmpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    existingpwd.setSelection(existingpwd.getText().length());
                    newpwd.setSelection(newpwd.getText().length());
                    confirmpwd.setSelection(confirmpwd.getText().length());
                }
            }
        });

        profile_details_submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateChangepasswordDetails() && CommonUtils.passwordValidate(newpwd.getText().toString(),getActivity())) {
                    if (validatePassword().length() == 0 ){
                        if (userType ==0){
                            configUrl = String.format(Config.LJS_BASE_URL + Config.changePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString());
                        }else if (userType ==1){
                            String companyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId", "SubUserRegistration"), getActivity());
                            configUrl = String.format(Config.LJS_BASE_URL + Config.changeSubUserPassword ,companyMail , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString());
                        }if (userType ==2){
                            configUrl = String.format(Config.LJS_BASE_URL + Config.fresherChangePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString());
                        }if (userType ==3){
                            configUrl = String.format(Config.LJS_BASE_URL + Config.trainerChangePassword , appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), existingpwd.getText().toString(), newpwd.getText().toString());
                        }
                        PasswordUpdate();
                        uploadChangePassword();
                    }else {
                        Toast.makeText(getActivity(),""+validatePassword(),Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return rootView;
    }

    public boolean validateChangepasswordDetails(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.existingpwd)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.newpwd)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.confirmpwd)).validate(NotEmpty.build(getActivity())));

        return (mForm.isValid()) ? true : false;
    }

  public String validatePassword(){
      if (TextUtils.isEmpty(existingpwd.getText().toString())){
          return "Please check the existing password";
      }
      if (!(newpwd.getText().toString().toLowerCase().equalsIgnoreCase(confirmpwd.getText().toString().toLowerCase()))){
          return "Your password entries do not match";
      }else {
          return "";
      }

  }

    private void PasswordUpdate()
    {
        final DatabaseHelper seekerInsertDB = new DatabaseHelper(getActivity());
        ApplicationThread.dbPost(this.getClass().getName(), "update", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                seekerInsertDB.updatePassword(confirmpwd.getText().toString(),"1");
            }
        });
    }

    public void uploadChangePassword() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(),""+result.toString(),Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }else {
                    Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                }
            }
        }, configUrl);

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                        Toast.makeText(getActivity(), "Password updated successfully.", Toast.LENGTH_SHORT).show();
                        existingpwd.setText("");
                        newpwd.setText("");
                        confirmpwd.setText("");
                        getActivity().getSupportFragmentManager().popBackStack();
                    break;
            }
        }
    }
}

