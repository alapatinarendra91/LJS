package com.localjobserver.ui;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.JobsData;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostWalkinFragment extends Fragment {

    private View rootView;
    private EditText jobTitleEdit, walkinDate, walkinTime, jobDescriptiontxt, compProfileEdit, contactPersonEdit, contactNumEdt, landNumEdt, roles_responceEdt, addressEdit, numbderofPositions;
    private Spinner expSp, expMinSp, totCtcSp, totThCtcSp, totCtcSpmin, totThCtcSpmin, noticeSpin, jobTypeSpin;
    private AutoCompleteTextView  currentDisEdt, companynameEdt, endClientEdit;
    private MultiSpinnerSearch qulficationEdt,LocalityEdt,keySkillsEdt,LocSpin,currentIndeSpin,roleSp;
    private TextView referenceExampleTxt;
    private CheckBox sameAddresscheck,marketRatecheck;
    private Button submitBtn, cancilBtn;
    private ProgressDialog progressDialog;
    private String selectedInd = "", selectedRoleId;
    private String currentCTCStr = "", expectedCtcStr = "";
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_INDUSTRIES = 1;
    private static final int RESULT_ROLES = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_DISIGNATIONS = 4;
    private static final int RESULT_COMPANIES = 5;
    private static final int RESULT_KEYSKILLS = 6;
    private static final int RESULT_EDUCATION = 7;
    public ArrayList<String> keySkillsList = null;
    public ArrayList<String> educationList = null;
    public ArrayList<JobsData> jobList;
    private String from;
    private int selectPos = 0;
    public LinkedHashMap<String, String> industries = null, locationsMap = null, localityMap = null, roleMap = new LinkedHashMap<>(), disigntionMap = null, companiesMap,keyskillsMap;
    private LinearLayout referenceLL, walkindateLL, walkinTimeLL, positionsLL, endClient_lay;
    private JobsData postDataModel = null;
    private SharedPreferences appPrefs;
    private EditText jobReferencetxt;
    private Calendar myCalendar = Calendar.getInstance();
    private LinkedHashMap<String, String> educationMap;
    private boolean isClassVisible = false;
    private Handler handler;
    private boolean editRecruiter_var = false,roleVar = false;
    private boolean editReferral_var = false;
    private String configUrl = "";
    private String resultid = "";


    public PostWalkinFragment() {
        // Required empty public constructor
    }


    public boolean validateProfessionalDetails() {
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.jobTitletxt)).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using((MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
        if (from.equalsIgnoreCase("walkin")) {
            mForm.addField(Field.using(walkinDate).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(walkinTime).validate(NotEmpty.build(getActivity())));
//            mForm.addField(Field.using((AutoCompleteTextView) rootView.findViewById(R.id.endClientEdit)).validate(NotEmpty.build(getActivity())));
        } else if (from.equalsIgnoreCase("newjob")) {
            mForm.addField(Field.using(jobReferencetxt).validate(NotEmpty.build(getActivity())));
            if (mForm.isValid()){
                if ( !CommonUtils.referanceNoValidate(jobReferencetxt.getText().toString(),getContext()))
                    return false;
            }

//            mForm.addField(Field.using((AutoCompleteTextView) rootView.findViewById(R.id.endClientEdit)).validate(NotEmpty.build(getActivity())));
        } else if (from.equalsIgnoreCase("referral")) {
            mForm.addField(Field.using(jobReferencetxt).validate(NotEmpty.build(getActivity())));
        }

        if (mForm.isValid()){
        if (CommonUtils.spinnerSelect("Location", LocSpin.getSelectedIds().size(), getActivity()) &&
                CommonUtils.spinnerSelect("Industry Type", currentIndeSpin.getSelectedIds().size(), getActivity()) &&
                CommonUtils.spinnerSelect("Role", roleSp.getSelectedIds().size(), getActivity())){

        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.jobDescriptiontxt)).validate(NotEmpty.build(getActivity())));
            if (CommonUtils.spinnerSelect("Keyskills", keySkillsEdt.getSelectedIds().size(), getActivity())){
            if (mForm.isValid()){
            if (CommonUtils.spinnerSelect("Notice period", noticeSpin.getSelectedItemPosition(), getActivity()) &&
                    CommonUtils.spinnerSelect("Job type", jobTypeSpin.getSelectedItemPosition(), getActivity())){
                if (CommonUtils.spinnerSelect("Education", qulficationEdt.getSelectedIds().size(), getActivity())){
//        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.qualificationEdit)).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using((EditText) rootView.findViewById(R.id.companynameEdt)).validate(NotEmpty.build(getActivity())));

                if (from.equalsIgnoreCase("walkin") || from.equalsIgnoreCase("newjob"))
                    mForm.addField(Field.using((AutoCompleteTextView) rootView.findViewById(R.id.endClientEdit)).validate(NotEmpty.build(getActivity())));

        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.compProfileEdit)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.contactNumEdt)).validate(NotEmpty.build(getActivity())));

        return (mForm.isValid()) ? true : false;
            }
            }
            }
            }
        }
        }
        return false;
    }

    public boolean spinnervalidation() {

        if (!CommonUtils.spinnerSelect("Experience Minimum", expMinSp.getSelectedItemPosition(), getActivity()) ||
                !CommonUtils.spinnerSelect("Location", LocSpin.getSelectedIds().size(), getActivity()) ||
                !CommonUtils.spinnerSelect("Industry Type", currentIndeSpin.getSelectedIds().size(), getActivity()) ||
                !CommonUtils.spinnerSelect("Role", roleSp.getSelectedIds().size(), getActivity()) ||
                !CommonUtils.spinnerSelect("Notice period", noticeSpin.getSelectedItemPosition(), getActivity()) ||
                !CommonUtils.spinnerSelect("Job type", jobTypeSpin.getSelectedItemPosition(), getActivity()) ||
                !CommonUtils.spinnerSelect("CTC Lakhs Minimum", totCtcSpmin.getSelectedItemPosition(), getActivity())
//                !CommonUtils.spinnerSelect("CTC Thousands Minimum",totThCtcSp.getSelectedItemPosition(),getActivity())||
//                !CommonUtils.spinnerSelect("CTC Lakhs Maximim",totCtcSpmin.getSelectedItemPosition(),getActivity())||
//                !CommonUtils.spinnerSelect("CTC Thousands Maximim",totThCtcSpmin.getSelectedItemPosition(),getActivity())||


                ) {
            return false;
        }

        return true;
    }

    public boolean validateMobileNo() {

        if (CommonUtils.isValidMobile(contactNumEdt.getText().toString(), contactNumEdt))
            return true;
        else {
            CommonUtils.showToast("Please specify a valid contact number", getActivity());
            return false;
        }

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        walkinDate.setText(sdf.format(myCalendar.getTime()));
        walkinDate.setError(null);

    }

    public PostWalkinFragment(String type, int position) {
        this.from = type;
        this.selectPos = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_post_walkin, container, false);


        postDataModel = new JobsData();
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        jobTitleEdit = (EditText) rootView.findViewById(R.id.jobTitletxt);
        walkinDate = (EditText) rootView.findViewById(R.id.walkinDate);
        walkinTime = (EditText) rootView.findViewById(R.id.walkinTime);
        jobDescriptiontxt = (EditText) rootView.findViewById(R.id.jobDescriptiontxt);
        endClientEdit = (AutoCompleteTextView) rootView.findViewById(R.id.endClientEdit);
        compProfileEdit = (EditText) rootView.findViewById(R.id.compProfileEdit);
        contactPersonEdit = (EditText) rootView.findViewById(R.id.contactPersonEdit);
        contactNumEdt = (EditText) rootView.findViewById(R.id.contactNumEdt);
        landNumEdt = (EditText) rootView.findViewById(R.id.landNumEdt);
        roles_responceEdt = (EditText) rootView.findViewById(R.id.roles_responceEdt);
        addressEdit = (EditText) rootView.findViewById(R.id.addressEdit);

        expSp = (Spinner) rootView.findViewById(R.id.expSp);
//        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expSp.setOnItemSelectedListener(spinListener);
        expMinSp = (Spinner) rootView.findViewById(R.id.expMinSp);
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expMinSp.setSelection(1);
        expMinSp.setOnItemSelectedListener(spinListener);
        currentIndeSpin = (MultiSpinnerSearch) rootView.findViewById(R.id.currentIndeSpin);
        currentIndeSpin.setOnItemSelectedListener(spinListener);
        roleSp = (MultiSpinnerSearch) rootView.findViewById(R.id.roleSp);
        roleSp.setOnItemSelectedListener(spinListener);

        totCtcSp = (Spinner) rootView.findViewById(R.id.totCtcSp);
        totThCtcSp = (Spinner) rootView.findViewById(R.id.totThCtcSp);
        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totThCtcSp.setOnItemSelectedListener(spinListener);
        totCtcSpmin = (Spinner) rootView.findViewById(R.id.totCtcSpmin);
        totCtcSpmin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totCtcSpmin.setSelection(1);
        totCtcSpmin.setOnItemSelectedListener(spinListener);
        totThCtcSpmin = (Spinner) rootView.findViewById(R.id.totThCtcSpmin);
        totThCtcSpmin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totThCtcSpmin.setOnItemSelectedListener(spinListener);
        LocSpin = (MultiSpinnerSearch) rootView.findViewById(R.id.LocSpin);
        LocSpin.setOnItemSelectedListener(spinListener);
        currentDisEdt = (AutoCompleteTextView) rootView.findViewById(R.id.currentDisEdt);
        companynameEdt = (AutoCompleteTextView) rootView.findViewById(R.id.companynameEdt);

        referenceExampleTxt = (TextView) rootView.findViewById(R.id.referenceExampleTxt);
//        keySkillsEdt = (MultiAutoCompleteTextView) rootView.findViewById(R.id.keySkillsEdt);
//        keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//        qulficationEdt = (MultiAutoCompleteTextView) rootView.findViewById(R.id.qualificationEdit);
//        qulficationEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        qulficationEdt = (MultiSpinnerSearch)rootView.findViewById(R.id.qualificationEdit);
        LocalityEdt = (MultiSpinnerSearch) rootView.findViewById(R.id.LocalityEdt);
        keySkillsEdt = (MultiSpinnerSearch) rootView.findViewById(R.id.keySkillsEdt);
        numbderofPositions = (EditText) rootView.findViewById(R.id.jobPositionstxt);
        submitBtn = (Button) rootView.findViewById(R.id.submitBtn);
        cancilBtn = (Button) rootView.findViewById(R.id.cancilBtn);
        sameAddresscheck = (CheckBox) rootView.findViewById(R.id.sameAddresscheck);
        marketRatecheck = (CheckBox) rootView.findViewById(R.id.marketRatecheck);

        referenceLL = (LinearLayout) rootView.findViewById(R.id.referenceLL);
        walkindateLL = (LinearLayout) rootView.findViewById(R.id.walkinDateLL);
        walkinTimeLL = (LinearLayout) rootView.findViewById(R.id.walkinTimeLL);
        positionsLL = (LinearLayout) rootView.findViewById(R.id.positionsLL);
        endClient_lay = (LinearLayout) rootView.findViewById(R.id.endClient_lay);

        jobReferencetxt = (EditText) rootView.findViewById(R.id.jobReferencetxt);
        walkinDate = (EditText) rootView.findViewById(R.id.walkinDate);
        walkinTime = (EditText) rootView.findViewById(R.id.walkinTime);

        noticeSpin = (Spinner) rootView.findViewById(R.id.notiSp);
        noticeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.noticeperiod)));
        noticeSpin.setOnItemSelectedListener(spinListener);

        jobTypeSpin = (Spinner) rootView.findViewById(R.id.jobTypeSp);
        jobTypeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.positiontype)));
        jobTypeSpin.setOnItemSelectedListener(spinListener);

//        roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));
        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSp,roleMap,1,"Select Role");

        CommonUtils.edittextChangelistner(getActivity(), jobTitleEdit);
        CommonUtils.edittextChangelistner(getActivity(),jobDescriptiontxt);
        CommonUtils.edittextChangelistner(getActivity(),companynameEdt);
//        CommonUtils.edittextChangelistner(getActivity(),keySkillsEdt);
//        CommonUtils.edittextChangelistner(getActivity(),qulficationEdt);
        CommonUtils.edittextChangelistner(getActivity(),endClientEdit);
        CommonUtils.edittextChangelistner(getActivity(),compProfileEdit);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                designSuccessDialog("walkin");
                if (validateProfessionalDetails() && spinnervalidation() && validateMobileNo() == true) {
                    setDataToModel();
                    postJobData(toPostJob(postDataModel));
                }
            }
        });

        cancilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        sameAddresscheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    endClientEdit.setText(companynameEdt.getText().toString());
                else
                    endClientEdit.setText("");

            }
        });

        marketRatecheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    totCtcSpmin.setEnabled(false);
                    totCtcSp.setEnabled(false);
                }else{
                    totCtcSpmin.setEnabled(true);
                    totCtcSp.setEnabled(true);
                }

            }
        });

        walkinDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
                datePickerDialog.show();

            }
        });

        walkinTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                            walkinTime.setText("0" + selectedHour + ":0" + selectedMinute);
                        else if (String.valueOf(selectedHour).length() == 1)
                            walkinTime.setText("0" + selectedHour + ":" + selectedMinute);
                        else if (String.valueOf(selectedMinute).length() == 1)
                            walkinTime.setText(selectedHour + ":" + "0" + selectedMinute);
                        else
                            walkinTime.setText(selectedHour + ":" + selectedMinute);

                        walkinTime.setError(null);

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        setVisibility(from);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.getSerializable("jobdataobject") != null) {
            JobsData jobsData = (JobsData) bundle.getSerializable("jobdataobject");
            jobTitleEdit.setText(jobsData.getJobTitle());
            walkinDate.setText(jobsData.getWalkinDate());
            walkinTime.setText(jobsData.getFunctionalArea());
//            LocalityEdt.setText(jobsData.getLocality());
            jobDescriptiontxt.setText(Html.fromHtml(jobsData.getJobDescription()));
//            keySkillsEdt.setText(jobsData.getKeySkills());
            currentDisEdt.setText(jobsData.getDesignation());
//            qulficationEdt.setText(jobsData.getQualification());
            companynameEdt.setText(jobsData.getCompanyName());
            endClientEdit.setText(jobsData.getEndClient());
            compProfileEdit.setText(jobsData.getCompanyProfile());
            contactPersonEdit.setText(jobsData.getContactPerson());
            landNumEdt.setText(jobsData.getContactNo_Landline());
            addressEdit.setText(jobsData.getAddress());
        }

        if (from.equalsIgnoreCase("editRecruiter")) {
            editRecruiter_var = true;
            bindData();
        } else if (from.equalsIgnoreCase("editReferral")) {
            editReferral_var = true;
            bindData();
        }

        getIndustries();
        getLocations();
        getCompanies();
        return rootView;
    }

    private void bindData() {
        jobList = CommonUtils.jobList;

        if (jobList.get(selectPos).getPostedType().equalsIgnoreCase("walkin")) {
            referenceLL.setVisibility(View.GONE);
            referenceExampleTxt.setVisibility(View.GONE);
            walkindateLL.setVisibility(View.VISIBLE);
            walkinTimeLL.setVisibility(View.VISIBLE);
            positionsLL.setVisibility(View.GONE);
            jobReferencetxt.setText("text");
        } else if (jobList.get(selectPos).getPostedType().equalsIgnoreCase("normal")) {
            referenceLL.setVisibility(View.VISIBLE);
            referenceExampleTxt.setVisibility(View.VISIBLE);
            walkindateLL.setVisibility(View.GONE);
            walkinTimeLL.setVisibility(View.GONE);
            positionsLL.setVisibility(View.VISIBLE);
        } else if (jobList.get(selectPos).getPostedType().equalsIgnoreCase("referral")) {
            referenceLL.setVisibility(View.VISIBLE);
            referenceExampleTxt.setVisibility(View.VISIBLE);
            walkindateLL.setVisibility(View.GONE);
            walkinTimeLL.setVisibility(View.GONE);
            positionsLL.setVisibility(View.VISIBLE);
            endClient_lay.setVisibility(View.GONE);
        }

        submitBtn.setText("Update");
        jobTitleEdit.setText("" + jobList.get(selectPos).getJobTitle());
        jobReferencetxt.setText("" + jobList.get(selectPos).getJobReference());
        walkinDate.setText("" + CommonUtils.getCleanDate(jobList.get(selectPos).getWalkinDate()));
        walkinTime.setText("" + CommonUtils.getCleanTime(jobList.get(selectPos).getWalkinDate()));
//        LocalityEdt.setText(""+ jobList.get(selectPos).getLocality());
        numbderofPositions.setText("" + jobList.get(selectPos).getJobPositions());
        jobDescriptiontxt.setText(Html.fromHtml("" + jobList.get(selectPos).getJobDescription()));
//        keySkillsEdt.setText("" + jobList.get(selectPos).getKeySkills());
        currentDisEdt.setText("" + jobList.get(selectPos).getDesignation());
//        qulficationEdt.setText("" + jobList.get(selectPos).getQualification());
        companynameEdt.setText("" + jobList.get(selectPos).getCompanyName());
        endClientEdit.setText("" + jobList.get(selectPos).getEndClient());
        compProfileEdit.setText("" + jobList.get(selectPos).getCompanyProfile());
        contactPersonEdit.setText("" + jobList.get(selectPos).getContactPerson());
        contactNumEdt.setText("" + jobList.get(selectPos).getPhone());
        landNumEdt.setText("" + jobList.get(selectPos).getContactNo_Landline());
        roles_responceEdt.setText("" + jobList.get(selectPos).getRolesResponsibilties());
        addressEdit.setText("" + jobList.get(selectPos).getAddress());

        expMinSp.setSelection(Integer.parseInt(jobList.get(selectPos).getExpMin()) + 1);
        totCtcSpmin.setSelection(Integer.parseInt(jobList.get(selectPos).getMinSal()) + 1);
        jobTypeSpin.setSelection(jobList.get(selectPos).getJTId());
        handler = new Handler();
        Thread t = new Thread(new Runnable() {
            public void run() {
                handler.postDelayed(new Runnable() {
                    public void run() {

                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
                        totCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
                        expSp.setSelection(jobList.get(selectPos).getExpMax() + 1);
                        totCtcSp.setSelection(Integer.parseInt(jobList.get(selectPos).getMaxSal()) + 1);
                    }
                }, 1000);
            }
        });
        t.start();

        String[] jobType_notice = getResources().getStringArray(R.array.noticeperiod);
        for (int i = 0; i < jobType_notice.length; i++) {
            if (jobType_notice[i].contains(CommonUtils.convertToWeeks(jobList.get(selectPos).getNoticePeriod()))) {
                noticeSpin.setSelection(i);
            }
        }

    }


    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()) {
                case R.id.currentIndeSpin:

                    if (null != industries && currentIndeSpin.getSelectedIds().size() > 0) {
//                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position - 1];
                        selectedInd = String.valueOf(currentIndeSpin.getSelectedIds().get(0));
                        postDataModel.setIndustry(currentIndeSpin.getSelectedItem().toString());
                        postDataModel.setInId(Integer.parseInt(selectedInd));
                        getRoles(selectedInd);
                    }

                    break;
                case R.id.expSp:
//                    if (!(expSp.getSelectedItemPosition() == 0))
                    postDataModel.setExpMax(Integer.parseInt(expSp.getSelectedItem().toString()));

                    break;
                case R.id.expMinSp:
                    if (!(expMinSp.getSelectedItemPosition() == 0))
                        postDataModel.setExpMin(expMinSp.getSelectedItem().toString());

                    if (expMinSp.getSelectedItemId() == 31) {
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    } else if (expMinSp.getSelectedItemId() == 1) {
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    } else if (expMinSp.getSelectedItemId() != 0) {
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()) + 1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }

                    break;
                case R.id.roleSp:
                    if (null != roleMap && roleSp.getSelectedIds().size() > 0) {
//                        selectedRoleId = roleMap.keySet().toArray(new String[roleMap.size()])[position];
                        selectedRoleId = String.valueOf(roleSp.getSelectedIds().get(0));
                        postDataModel.setRole(roleSp.getSelectedItem().toString());
                        postDataModel.setRId(Integer.parseInt(selectedRoleId));
                    }
                    break;
                case R.id.totCtcSp:
                    currentCTCStr = totCtcSp.getSelectedItem().toString();
                    postDataModel.setMaxSal(currentCTCStr);
                    break;
                case R.id.totThCtcSp:
                    if (currentCTCStr != null && currentCTCStr.length() > 0) {
                        currentCTCStr = currentCTCStr + "." + totThCtcSp.getSelectedItem().toString();
                    } else {
                        currentCTCStr = "0" + "." + totThCtcSp.getSelectedItem().toString();
                    }
                    postDataModel.setMaxSal(currentCTCStr);
                    break;
                case R.id.totCtcSpmin:
                    expectedCtcStr = totCtcSpmin.getSelectedItem().toString();
                    postDataModel.setMinSal(expectedCtcStr);

                    if (totCtcSpmin.getSelectedItemId() == 100) {
                        totCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totCtcSp.setOnItemSelectedListener(spinListener);
                    } else if (totCtcSpmin.getSelectedItemId() != 0) {
                        totCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expectedCtcStr) + 1, 100)));
                        totCtcSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.totThCtcSpmin:
                    if (expectedCtcStr != null && expectedCtcStr.length() > 0) {
                        expectedCtcStr = expectedCtcStr + "." + totThCtcSpmin.getSelectedItem().toString();
                    } else {
                        expectedCtcStr = "0" + "." + totThCtcSpmin.getSelectedItem().toString();
                    }
                    postDataModel.setMinSal(expectedCtcStr);
                    break;

                case R.id.notiSp:
                    postDataModel.setNoticePeriod(CommonUtils.convertToDays(noticeSpin.getSelectedItem().toString()));
                    break;

                case R.id.LocSpin:
//                    for (Map.Entry<String, String> e : locationsMap.entrySet()) {
//                        if (e.getValue().equalsIgnoreCase(LocSpin.getSelectedItem().toString())){
//                            getLocalities("" + e.getKey());
//                            break;
//                        }
//                    }
                    if (LocSpin.getSelectedIds().size() > 0)
                    getLocalities("" + LocSpin.getSelectedIds().get(0));
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    public void setVisibility(String from) {
        if (from.equalsIgnoreCase("walkin")) {
            referenceLL.setVisibility(View.GONE);
            referenceExampleTxt.setVisibility(View.GONE);
            walkindateLL.setVisibility(View.VISIBLE);
            walkinTimeLL.setVisibility(View.VISIBLE);
            positionsLL.setVisibility(View.GONE);
            submitBtn.setText("Post Walk-In");
            jobReferencetxt.setText("text");
        } else if (from.equalsIgnoreCase("newjob")) {
            referenceLL.setVisibility(View.VISIBLE);
            referenceExampleTxt.setVisibility(View.VISIBLE);
            walkindateLL.setVisibility(View.GONE);
            walkinTimeLL.setVisibility(View.GONE);
            positionsLL.setVisibility(View.VISIBLE);
            submitBtn.setText("Post Job");
        } else if (from.equalsIgnoreCase("referral")) {
            referenceLL.setVisibility(View.VISIBLE);
            referenceExampleTxt.setVisibility(View.VISIBLE);
            walkindateLL.setVisibility(View.GONE);
            walkinTimeLL.setVisibility(View.GONE);
            positionsLL.setVisibility(View.VISIBLE);
            endClient_lay.setVisibility(View.GONE);
            submitBtn.setText("Post Referral");
        }
    }

    public void getIndustries() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
//                    getLocations();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
//                    getLocations();

                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public void getRoles(final String selectedInd) {
        if (roleVar == true && progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {

                    if (roleVar ==  true)
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    roleMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_ROLES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    if (roleVar ==  true)
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getRoles, selectedInd), CommonKeys.arrRoles);
    }

    public void getLocations() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
//                    getCompanies();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
//                    getCompanies();

                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void getLocalities(String locationCode) {

        if (roleVar ==  true){
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        LiveData.getLocalityGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    if (roleVar ==  true)
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    localityMap = (LinkedHashMap<String, String>) result;
//                    LocalityEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(localityMap, "Locality")));
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(), LocalityEdt, localityMap, 1, "");
                    if (jobList != null && jobList.size()>0 && jobList.get(selectPos).getLocality() != null) {
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocalityEdt,localityMap,1,jobList.get(selectPos).getLocality());
                    }
                    if (roleVar ==  true)
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocalities, locationCode), CommonKeys.arrLocalities);
    }

    public void getCompanies() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    getDisegnation();
                    return;
                }
                if (result != null) {
//                    progressDialog.dismiss();
                    companiesMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_COMPANIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    getDisegnation();

                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
    }

    public void getDisegnation() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    getEducations();
                    return;
                }
                if (result != null) {
                    disigntionMap = (LinkedHashMap<String, String>) result;
//                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISIGNATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                    getEducations();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getDesignations), CommonKeys.arrDesignations);
    }

    public void getEducations() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    getKeyWords();
                    Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    getKeyWords();
                    educationMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_EDUCATION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }

    public void getKeyWords() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Getting keyskills..");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        });
    }

    public void getKeySkills(final ApplicationThread.OnComplete oncomplete) {
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            keyskillsMap = new LinkedHashMap<String, String>();
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keyskillsMap.put(""+i,c.getString(CommonKeys.LJS_UserKeySkills));
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            Log.i("", "list issss : " + keySkillsList);
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while getting data from  server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries && isClassVisible == true) {

//                        currentIndeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(industries, "Industry")));
//                        String[] industryType_array = CommonUtils.fromMap(industries, "Industry");
//                        currentIndeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), industryType_array));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndeSpin,industries,1,"Select Industry");

                        if (from.equalsIgnoreCase("editRecruiter") || from.equalsIgnoreCase("editReferral")) {
//                            for (int i = 0; i < industryType_array.length; i++) {
//                                if (industryType_array[i].contains(jobList.get(selectPos).getIndustry())) {
//                                    currentIndeSpin.setSelection(i);
//                                }
//                            }
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndeSpin,industries,1,jobList.get(selectPos).getIndustry());
                        }


                    } else {
//                        Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_ROLES:
//                    roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
//                    String[] roleType_array = CommonUtils.fromMap(roleMap, "Role");
//                        Arrays.sort(jobType_array);
//                    roleSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, roleType_array));
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSp,roleMap,1,"Select Role");
                    if (from.equalsIgnoreCase("editRecruiter") || from.equalsIgnoreCase("editReferral")) {
//                        for (int i = 0; i < roleType_array.length; i++) {
//                            if (roleType_array[i].equals(jobList.get(selectPos).getRole())) {
//                                roleSp.setSelection(i);
//                                from = jobList.get(selectPos).getPostedType();
//                            }
//                        }
                        from = jobList.get(selectPos).getPostedType();
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSp,roleMap,1,jobList.get(selectPos).getRole());
                    }

                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap && null != LocSpin && isClassVisible == true) {
//                        LocSpin.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocSpin,locationsMap,1,"Select Location");

                        if (jobList != null && jobList.size() > 0)
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocSpin,locationsMap,1,jobList.get(selectPos).getLocation());
//                        for (int i = 0; i < CommonUtils.fromMap(locationsMap, "Location").length; i++) {
//                            if (CommonUtils.fromMap(locationsMap, "Location")[i].contains(jobList.get(selectPos).getLocation())) {
//                                LocSpin.setSelection(i);
//                            }
//                        }
                    }
                    break;
                case RESULT_DISIGNATIONS:
                    if (null != disigntionMap && null != currentDisEdt && isClassVisible == true) {
                        currentDisEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(disigntionMap, "Designation")));
                    }
                    break;
                case RESULT_COMPANIES:
                    if (null != companiesMap && null != companynameEdt && isClassVisible == true) {
                        companynameEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(companiesMap, "Company")));
                        endClientEdit.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(companiesMap, "Company")));
                    }
                    break;
                case RESULT_KEYSKILLS:
                    if (null != keySkillsList && null != keySkillsEdt && isClassVisible == true){
//                        keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(), keySkillsList));
                        roleVar = true;

                        if (jobList != null && jobList.size()>0 && jobList.get(selectPos).getKeySkills() != null) {
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),keySkillsEdt,keyskillsMap,9999,jobList.get(selectPos).getKeySkills());
                        }else
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),keySkillsEdt,keyskillsMap,9999,"");
                    }

                    break;

                case RESULT_EDUCATION:
                    if (null != educationMap && null != qulficationEdt && isClassVisible == true){
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),qulficationEdt,educationMap,9999,"");
                    if (jobList != null && jobList.size()>0 && jobList.get(selectPos).getQualification() != null) {
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),qulficationEdt,educationMap,9999,jobList.get(selectPos).getQualification());
                    }
                    }
                    break;
            }
        }
    }


    public void setDataToModel() {
        String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),getActivity());
        String CompanyName = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("RecruiterCompanyName","SubUserRegistration"),getActivity());

        if (marketRatecheck.isChecked()){
            postDataModel.setMinSal("0");
            postDataModel.setMaxSal("0");
        }

        postDataModel.setJobTitle("" + jobTitleEdit.getText().toString());
        postDataModel.setJobSummary("");
//        postDataModel.setJobDescription("" + jobDescriptiontxt.getText().toString());
        postDataModel.setJobDescription("");
        postDataModel.setKeySkills("" + keySkillsEdt.getSelectedItem());
        postDataModel.setStdKeySkills("" + keySkillsEdt.getSelectedItem());
//        postDataModel.setCompanyProfile("" + compProfileEdit.getText().toString());
        postDataModel.setFunctionalArea(""+walkinTime.getText().toString());
        postDataModel.setLocation("" + LocSpin.getSelectedItem().toString());
        postDataModel.setLocality("" + LocalityEdt.getSelectedItem());
        postDataModel.setQualification("" + qulficationEdt.getSelectedItem());
        postDataModel.setEmail("" + appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));
        postDataModel.setCompanyEmailId("" + CompanyMail);
        postDataModel.setCompanyName("" + CompanyName);
        postDataModel.setPhone("" + contactNumEdt.getText().toString());
        postDataModel.setContactNo_Landline("" + landNumEdt.getText().toString());
        postDataModel.setAddress("" + addressEdit.getText().toString());
        postDataModel.setCompanyName("" + companynameEdt.getText().toString());
        postDataModel.setStdCurCompany("" + companynameEdt.getText().toString());
        postDataModel.setContactPerson("" + contactPersonEdit.getText().toString());
        postDataModel.setSalary("");
        postDataModel.setLocId(0);
        postDataModel.setFId(0);
        postDataModel.setJTId(jobTypeSpin.getSelectedItemPosition());
        if (from.equalsIgnoreCase("walkin")) {
            postDataModel.setPostedType("Walkin");
            postDataModel.setEndClient("" + endClientEdit.getText().toString());
            postDataModel.setPbId(0);
            postDataModel.setPostDate("" + CommonUtils.getDateTime());
            postDataModel.setFunctionalArea("" + walkinTime.getText().toString());
            postDataModel.setWalkinDate("" + CommonUtils.getDateTime());

        } else if (from.equalsIgnoreCase("newjob") || from.equalsIgnoreCase("Normal")) {
            postDataModel.setPostedType("Normal");
            postDataModel.setJobReference("" + jobReferencetxt.getText().toString());
            postDataModel.setEndClient("" + endClientEdit.getText().toString());
            postDataModel.setPbId(0);
            postDataModel.setPostDate("" + CommonUtils.getDateTime());
            postDataModel.setWalkinDate("" + CommonUtils.getDateTime());

        } else if (from.equalsIgnoreCase("referral")) {
            postDataModel.setPostedType("Referral");
            postDataModel.setEndClient("" + companynameEdt.getText().toString());
            postDataModel.setPbId(2);
            postDataModel.setWalkinDate("" + CommonUtils.getDateTime());
        }

        postDataModel.setDesignation("" + currentDisEdt.getText().toString());
        postDataModel.setViewsCount(0);
        postDataModel.setAppliedCount(0);
        postDataModel.setMatchedScore("0");
        postDataModel.setSearchQuery("");
        postDataModel.setResume("");
        postDataModel.setJobPositions("" + numbderofPositions.getText().toString());
    }

    public LinkedHashMap toPostJob(JobsData postDataModel) {
        LinkedHashMap postJob = new LinkedHashMap();
//        postJob.put("_id", postDataModel.get_id());
        postJob.put("JobNo", "0");
        if (from.equalsIgnoreCase("newjob") || from.equalsIgnoreCase("Normal"))
        postJob.put("JobReference", postDataModel.getJobReference());

        postJob.put("JobTitle", postDataModel.getJobTitle());
        postJob.put("JobSummary", postDataModel.getJobSummary());
        postJob.put("JobDescription", postDataModel.getJobDescription());
        postJob.put("KeySkills", postDataModel.getKeySkills());
        postJob.put("StdKeySkills", postDataModel.getKeySkills());
        postJob.put("MinSal", postDataModel.getMinSal());
        postJob.put("MaxSal", postDataModel.getMaxSal());
        postJob.put("CompanyProfile", postDataModel.getCompanyProfile());
        postJob.put("FunctionalArea", postDataModel.getFunctionalArea());
        postJob.put("Industry", postDataModel.getIndustry());
        postJob.put("ExpMin", postDataModel.getExpMin());
        postJob.put("ExpMax", postDataModel.getExpMax());
        postJob.put("Location", postDataModel.getLocation());
        postJob.put("Locality", postDataModel.getLocality());
        postJob.put("Qualification", postDataModel.getQualification());
        postJob.put("Email", postDataModel.getEmail());
        postJob.put("CompanyEmailId", postDataModel.getCompanyEmailId());
        postJob.put("Phone", postDataModel.getPhone());
        postJob.put("ContactNo_Landline", postDataModel.getContactNo_Landline());
        postJob.put("PostDate", postDataModel.getPostDate());
        postJob.put("Role", postDataModel.getRole());
        postJob.put("Address", postDataModel.getAddress());
        postJob.put("CompanyName", postDataModel.getCompanyName());
        postJob.put("StdCurCompany", postDataModel.getCompanyName());
        postJob.put("ContactPerson", postDataModel.getContactPerson());
        postJob.put("Salary", postDataModel.getSalary());
        postJob.put("LocId", postDataModel.getLocId());
        postJob.put("InId", postDataModel.getInId());
        postJob.put("FId", postDataModel.getFId());
        postJob.put("RId", postDataModel.getRId());
        postJob.put("JTId", postDataModel.getJTId());
        postJob.put("PbId", postDataModel.getPbId());
        postJob.put("PostedType", postDataModel.getPostedType());
        postJob.put("Designation", postDataModel.getDesignation());
        postJob.put("ViewsCount", postDataModel.getViewsCount());
        postJob.put("AppliedCount", postDataModel.getAppliedCount());
        postJob.put("EndClient", postDataModel.getEndClient());
        postJob.put("MatchedScore", postDataModel.getMatchedScore());
        postJob.put("NoticePeriod", postDataModel.getNoticePeriod());
        postJob.put("WalkinDate", postDataModel.getWalkinDate());
        postJob.put("SearchQuery", postDataModel.getSearchQuery());
        postJob.put("Resume", postDataModel.getResume());

        if (postDataModel.getJobPositions().equalsIgnoreCase(""))
            postJob.put("jobPositions", Integer.parseInt("1"));
        else
            postJob.put("jobPositions", Integer.parseInt(postDataModel.getJobPositions()));

        return postJob;
    }

    public void postJobData(LinkedHashMap<String, String> input) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        Log.e("", "Result is String : " + requestString.toString());
        requestString = CommonUtils.encodeURL(requestString);
        if (editRecruiter_var) {
            configUrl = String.format(Config.LJS_BASE_URL + Config.RecruiterUpdatePostJob, "" + requestString, "" + jobList.get(selectPos).get_id());
        } else
            configUrl = String.format(Config.LJS_BASE_URL + Config.RecruiterPostJob, "" + requestString);
//        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
        LiveData.uploadPostJobDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null)
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), "Error while posting job", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.i("", "Result is : " + result.toString());
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true") || result.toString().contains("jobid")) {
                        if (result.toString().contains("jobid"))
                        resultid = result.toString().replace("\"", "").substring(6);
//                        if (editRecruiter_var) {
//                            designSuccessDialog("Edit Job");
                        postJobDescriptionData();
//                        } else if (from.equalsIgnoreCase("walkin")) {
//                            designSuccessDialog("Walk-in job posted ");
//                        } else if (from.equalsIgnoreCase("newjob")) {
//                            designSuccessDialog("job posted");
//                        } else if (from.equalsIgnoreCase("referral")) {
//                            designSuccessDialog("Posted Referral");
//                        }
                    }
//                    progressDialog.dismiss();
                }
            }
        }, configUrl);
    }

    public void postJobDescriptionData() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();

        if (jobList != null)
             resultid  = jobList.get(selectPos).get_id();

        configUrl = String.format(Config.LJS_BASE_URL + Config.updateJobDescription, "" + jobDescriptiontxt.getText().toString(), "" + resultid);

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result == null) {
                        Toast.makeText(getActivity(), "Error while  posting job", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                Log.i("", "Result is : " + result.toString());
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        postCompanyProfileData();
                    }
//                    progressDialog.dismiss();
                }
            }
        }, configUrl);
    }

    public void postCompanyProfileData() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();

        if (jobList != null)
            resultid  = jobList.get(selectPos).get_id();

        configUrl = String.format(Config.LJS_BASE_URL + Config.updateJobCompanyProfile, "" + compProfileEdit.getText().toString(), "" + resultid);

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result == null) {
                        Toast.makeText(getActivity(), "Error while  posting job", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                Log.i("", "Result is : " + result.toString());
                if (result != null) {
                    progressDialog.dismiss();
                    if (result.toString().equalsIgnoreCase("true")) {
                        if (editRecruiter_var) {
                            designSuccessDialog("Job updated");
                        } else if (from.equalsIgnoreCase("walkin")) {
                            designSuccessDialog("Walk-in job posted ");
                        } else if (from.equalsIgnoreCase("newjob")) {
                            designSuccessDialog("job posted");
                        } else if (from.equalsIgnoreCase("referral")) {
                            designSuccessDialog("Posted Referral");
                        }

                    }
                }
            }
        }, configUrl);
    }

    Dialog dialog;

    public void designSuccessDialog(String message) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_dialog);
        dialog.setCanceledOnTouchOutside(true);
        TextView msgView = (TextView) dialog.findViewById(R.id.msgTxt);
        TextView titleHeader = (TextView) dialog.findViewById(R.id.titleHeader);
        titleHeader.setText("Confirmation");
        msgView.setText("" + message + " successfully.");
        dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                startActivity(new Intent(getActivity(), MyJobPostingsActivity.class));
                getActivity().finish();
            }
        });

        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause() {
        super.onPause();
        isClassVisible = false;
    }
}
