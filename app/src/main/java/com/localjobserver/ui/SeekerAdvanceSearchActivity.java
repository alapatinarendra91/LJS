package com.localjobserver.ui;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.filter.FilterSeekerFragment;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;

public class SeekerAdvanceSearchActivity extends ActionBarActivity {
    private ActionBar actionBar = null;
    Fragment itemFrag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeker_advance_search);

        Bundle bundle = new Bundle();

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (getIntent().getStringExtra("Filter")!= null){
            actionBar.setTitle("Filter Jobs");
            bundle.putString("Filter", "Filter");
            bundle.putString("keySkills", getIntent().getStringExtra("keySkills"));
            itemFrag = new FilterSeekerFragment();
        } else{
            itemFrag = new AdvanceSearchFragment();
            actionBar.setTitle("Search Jobs");
        }


//            itemFrag = new AdvanceSearchFragment();
            itemFrag.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.container, itemFrag).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class AdvanceSearchFragment extends Fragment {

        private View rootView;
        private MultiAutoCompleteTextView autoKeys;
        private MultiSpinnerSearch LocEdt,IndustrySp,roleSp;
        private Spinner walkinSp,expMinSp,expSp,PostedbySp,jobTypeSp;
        private ImageView speaker;
//        MaterialSpinner expMinSp;
        private String jobType_str,Postedby_str,walkin_str;
        private AutoCompleteTextView EduEdt;
        public StringBuffer searchStr;
        private Button searchBtn,cancilBtn;
        private ProgressDialog progressDialog;private static  int SEARCH_RESULT = 0;
        private static final int RESULT_ERROR = 0;
        private static final int RESULT_KEYSKILLS = 1;
        private static final int RESULT_INDUSTRIES = 2;
        private static final int RESULT_LOCATIONS = 3;
        private static final int RESULT_LOCALRESULT = 4;
        private static final int RESULT_DISIGNATIONS = 5;
        private static final int RESULT_EDUCATION = 6;
        private static final int RESULT_ROLES = 7;
        private String selectedInd = "";
        public LinkedHashMap<String,String> industries = null, locationsMap = null,disigntionMap = null,educationMap,roleMap = new LinkedHashMap<>();
        public ArrayList<String> keySkillsList = null;
        private String activityComingFrom = "";

        public AdvanceSearchFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_seeker_advance_search, container, false);

            Bundle bundle = this.getArguments();
            activityComingFrom = bundle.getString("Filter");

            autoKeys = (MultiAutoCompleteTextView)rootView.findViewById(R.id.autoskillskeyword);
            autoKeys.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            autoKeys.setThreshold(1);
            LocEdt = (MultiSpinnerSearch)rootView.findViewById(R.id.LocEdt);
            EduEdt = (AutoCompleteTextView)rootView.findViewById(R.id.EduEdt);
            expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
            speaker = (ImageView) rootView.findViewById(R.id.speaker);
//            expMinSp = (MaterialSpinner) rootView.findViewById(R.id.expMinSp);
            expSp = (Spinner)rootView.findViewById(R.id.expSp);
            roleSp = (MultiSpinnerSearch)rootView.findViewById(R.id.roleSp);
            IndustrySp = (MultiSpinnerSearch)rootView.findViewById(R.id.IndustrySp);
            walkinSp = (Spinner)rootView.findViewById(R.id.walkinSp);
            PostedbySp = (Spinner)rootView.findViewById(R.id.PostedbySp);
            jobTypeSp = (Spinner)rootView.findViewById(R.id.jobTypeSp);
            searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
            cancilBtn = (Button)rootView.findViewById(R.id.cancelBtn);

            searchStr  = new StringBuffer();
            expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
            expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
            jobTypeSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.positiontype)));
            PostedbySp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.postedBy)));
            walkinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.referralJobs)));
//            roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));
            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSp,roleMap,1,"Select Role");

            walkinSp.setSelection(1);
//          expMinSp.setSelectedIndex(1);
            expMinSp.setSelection(1);
            expSp.setSelection(1);


            autoKeys.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        getJobsfromSearch();
                    }
                    return false;
                }
            });

            EduEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        getJobsfromSearch();
                    }
                    return false;
                }
            });

            expMinSp.setOnItemSelectedListener(spinListener);
            expSp.setOnItemSelectedListener(spinListener);
            IndustrySp.setOnItemSelectedListener(spinListener);
            jobTypeSp.setOnItemSelectedListener(spinListener);
            walkinSp.setOnItemSelectedListener(spinListener);
            PostedbySp.setOnItemSelectedListener(spinListener);
            speaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promptSpeechInput();
                }
            });
            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getJobsfromSearch();

                }
            });

            cancilBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });

            getKeyWords();

            if (activityComingFrom != null && activityComingFrom.equalsIgnoreCase("Filter"))
                autoKeys.setVisibility(View.GONE);

            return rootView;
        }

        /**
         * Showing google speech input dialog
         * */
        private void promptSpeechInput() {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say your skill");
            try {
                getActivity().startActivityForResult(intent, 100);
            } catch (ActivityNotFoundException a) {
                CommonUtils.showToast("Sorry! Your device doesn\\'t support speech input",getActivity());
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            // In fragment class callback
            switch (requestCode) {
                case 100: {
                    if (resultCode == RESULT_OK && null != data) {
                        ArrayList<String> result = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        if (result.get(0).equalsIgnoreCase("search"))
                            getJobsfromSearch();
                            else{
                        autoKeys.append(result.get(0)+", ");
                        autoKeys.setSelection(autoKeys.getText().toString().length());
                    }
                    }
                    break;
                }
            }
        }

        private void getJobsfromSearch(){

            searchStr.append(autoKeys.getText().toString());
            if (!CommonUtils.isNetworkAvailable(getActivity())) {
                Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!CommonUtils.checkEditTextEampty(autoKeys) &&
//                            !CommonUtils.checkEditTextEampty(LocEdt) &&
                    !CommonUtils.checkEditTextEampty(EduEdt) &&
                    LocEdt.getSelectedIds().size() == 0  &&
                    IndustrySp.getSelectedIds().size() == 0  &&
                    roleSp.getSelectedIds().size() == 0  &&
                    !CommonUtils.checkSpinnerposition(jobTypeSp) &&
                    !CommonUtils.checkSpinnerposition(PostedbySp) &&
                    !CommonUtils.checkSpinnerposition(walkinSp)&&
                    expSp.getSelectedItem().toString().equalsIgnoreCase("0")
                    ){
                CommonUtils.showToast("Please select any search criteria.",getActivity());
                return;
            }

            if (autoKeys.getText().toString() != null && autoKeys.getText().toString().length() > 0){
                CommonUtils.highlightingSkill = autoKeys.getText().toString().trim();
                CommonUtils.highlightingSkill = autoKeys.getText().toString().substring(0, autoKeys.getText().toString().length()-2);
            }


            LinkedHashMap<String,String> input = toUpload();
            JSONObject json = new JSONObject(input);
            String requestString = json.toString();
            Log.e("TAG", "Request Str is :" + requestString);
            requestString = CommonUtils.encodeURL(requestString);

            Bundle searchBundle = new Bundle();
            searchBundle.putString("keySkills", autoKeys.getText().toString());
            searchBundle.putString("searchTxt", requestString);
            searchBundle.putString("selectedJobType", selectedInd);
            Intent jobListIntent = new Intent(getActivity(), JobListActivity.class);
            jobListIntent.putExtras(searchBundle);
            startActivity(jobListIntent);

        }

//        @Override
//        public void onAttach(Activity activity) {
//            super.onAttach(activity);
//            try {
//                if (null != (MainInfoActivity) activity) {
//                    ((MainInfoActivity) activity).onSectionAttached("Search Jobs");
//                }
//            } catch (ClassCastException e) {
//                throw new ClassCastException(activity.toString()
//                        + " must implement OnFragmentInteractionListener");
//            }
//        }

        private  LinkedHashMap  toUpload(){
            LinkedHashMap seekerDataMap = new LinkedHashMap();
            seekerDataMap.put("KeyWords",autoKeys.getText().toString());
            if (LocEdt.getSelectedItem().equals("Location"))
            seekerDataMap.put("Location","");
            else
                seekerDataMap.put("Location",LocEdt.getSelectedItem());
            seekerDataMap.put("SubLocation","");
            seekerDataMap.put("MinExp",""+expMinSp.getSelectedItem().toString());
//            seekerDataMap.put("MinExp",""+expMinSp.getItems().toString());
            seekerDataMap.put("MaxExp",""+expSp.getSelectedItem().toString());
            seekerDataMap.put("MaxSal","0");
            seekerDataMap.put("MinSal","0");
            seekerDataMap.put("JobType",jobType_str);
            seekerDataMap.put("Industry",""+selectedInd);
            seekerDataMap.put("Role","");
            seekerDataMap.put("Education",EduEdt.getText().toString());
            seekerDataMap.put("PostedBy",Postedby_str);
            seekerDataMap.put("JType",walkin_str);
            return seekerDataMap;
        }

        public void getKeyWords(){
//            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait..");
                progressDialog.setCancelable(false);
//            }
            progressDialog.show();
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        getIndustries();
                        getLocations();
                        getEducations();
                        return;
                    }

                    if (result != null) {
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_KEYSKILLS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();

                        getIndustries();
                        getLocations();
                        getEducations();
                    }
                }
            });
        }


        public void getIndustries(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getIndusPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        industries = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_INDUSTRIES;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
        }

        public void getRoles(final String selectedInd){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        roleMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_ROLES;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getRoles, selectedInd), CommonKeys.arrRoles);
        }

        public void getDisignation(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        disigntionMap = (LinkedHashMap<String, String>) result;
                        progressDialog.dismiss();
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_DISIGNATIONS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getDesignations), CommonKeys.arrDesignations);
        }

        public void getEducations(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
//            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
//                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        educationMap = (LinkedHashMap<String,String>)result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_EDUCATION;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
        }


        public void getLocations(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        locationsMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_LOCATIONS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                    if (c.getString(CommonKeys.LJS_UserKeySkills).contains("java") || c.getString(CommonKeys.LJS_UserKeySkills).contains("JAVA"))
                                        Log.e("is ","is : "+c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {
                                Message messageToParent = new Message();
                                messageToParent.what = 0;
                                Bundle bundleData = new Bundle();
                                bundleData.putString("Time Over", "Lost Internet Connection");
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);
                            }
                        }
                    });
                }
            });
        }
        class StatusHandler extends Handler {

            public void handleMessage(android.os.Message msg)
            {
                switch (msg.what){
                    case RESULT_ERROR:
                        Toast.makeText(getActivity(), "Error occured while getting data from  server.", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_KEYSKILLS:
                        if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()){
//                            autoKeys.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                            autoKeys.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                        }
                        break;
                    case RESULT_INDUSTRIES:
                        if (null != industries && industries.size() > 0 && isVisible()) {
//                            IndustrySp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(industries, "Industry")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),IndustrySp,industries,1,"Industry");
                        }
                        break;
                    case RESULT_LOCATIONS:
                        if (null != locationsMap && locationsMap.size() > 0 && isVisible()){
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocEdt,locationsMap,9999,"");
                        }
                        break;
                    case RESULT_ROLES:
                        if (null != roleMap && null != roleSp) {
//                            roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSp,roleMap,1,"Select Role");
                        }
                        break;
                    case RESULT_EDUCATION:
                        if (null != educationMap ) {
                            EduEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Education")));
                        } else {
                            Toast.makeText(getActivity(),"Not able to get education details",Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        }

        AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()){
                    case R.id.IndustrySp:
                        if (null != industries && IndustrySp.getSelectedIds().size() > 0){
//                            selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
                            selectedInd = String.valueOf(IndustrySp.getSelectedIds().get(0));
                            getRoles(selectedInd);
                        }
                        break;
                    case R.id.expMinSp:
                        if (expMinSp.getSelectedItemId() == 31){
                            expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                            expSp.setOnItemSelectedListener(spinListener);
                        }else if (expMinSp.getSelectedItemId() == 1){
                            expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                            expSp.setOnItemSelectedListener(spinListener);
                        }else if (expMinSp.getSelectedItemId() != 0){
                            expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                            expSp.setOnItemSelectedListener(spinListener);
                        }
                        break;
                    case R.id.expSp:
                        break;

                    case R.id.walkinSp:
                        if (walkinSp.getSelectedItemId() != 0)
                            walkin_str = walkinSp.getSelectedItem().toString();
                        else
                            walkin_str = "";
                        break;

                    case R.id.jobTypeSp:
                        if (jobTypeSp.getSelectedItemId() != 0)
                            jobType_str = (jobTypeSp.getSelectedItemId()) +"";
                        else
                            jobType_str = "";
                        break;

                    case R.id.PostedbySp:
                        if (PostedbySp.getSelectedItemId() != 0)
                            Postedby_str = String.valueOf(PostedbySp.getSelectedItemPosition());
                        else
                            Postedby_str = "";
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }
    }
