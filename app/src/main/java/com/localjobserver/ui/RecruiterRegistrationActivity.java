package com.localjobserver.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;


public class RecruiterRegistrationActivity extends ActionBarActivity {
    private SharedPreferences preferences;

    private ActionBar actionBar = null;
    public static boolean back_var = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recruiter_registration);
        preferences = getApplicationContext().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        if (savedInstanceState == null) {
            Intent i=getIntent();
            int flagHere = i.getIntExtra("flag", 0);
            if (flagHere == 1){
                actionBar.setTitle("Update Profile");
            }else {
                actionBar.setTitle("Recruiter Registration");
            }
            Bundle bundle = new Bundle();
            bundle.putInt("flag",i.getIntExtra("flag",0));
            RecrutierRegistrationFragment fragInfo = new RecrutierRegistrationFragment();
            fragInfo.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragInfo)
                    .commit();
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recruiter_registration, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        back_var = false;
            this.finish();


    }
}
