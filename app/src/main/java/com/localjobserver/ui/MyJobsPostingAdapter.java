package com.localjobserver.ui;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.Config;

import co.talentzing.R;

import java.util.ArrayList;

/**
 * Created by admin on 25-04-2015.
 */
public class MyJobsPostingAdapter extends BaseAdapter {

    private  Activity context;
    private ArrayList<JobsData> jobList;
    public MyJobsPostingAdapter(Activity context, ArrayList<JobsData> jobList) {
        super();
        this.context=context;
        this.jobList=jobList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.my_jobs_posting_row, null);
        }
        TextView jobTitle = (TextView)convertView.findViewById(R.id.positionNAME);

        TextView walkinType = (TextView)convertView.findViewById(R.id.walkinType);
//        if (((JobsData)jobList.get(position)).getPostedType() !=null && !(((JobsData)jobList.get(position)).getPostedType().equalsIgnoreCase("Normal")))
//        walkinType.setText(((JobsData)jobList.get(position)).getPostedType());
//        else
//            walkinType.setVisibility(View.GONE);

        if (((JobsData)jobList.get(position)).getPostedType() !=null){
            walkinType.setText(((JobsData)jobList.get(position)).getPostedType());
            if (((JobsData)jobList.get(position)).getPostedType().equalsIgnoreCase("Normal"))
            walkinType.setVisibility(View.GONE);
            else{
                walkinType.setVisibility(View.VISIBLE);
                walkinType.setText(((JobsData)jobList.get(position)).getPostedType());
            }

        }


        jobTitle.setText(((JobsData)jobList.get(position)).getJobTitle());
        TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
        jobLoc.setText(Html.fromHtml("<b>Location: </b> "+(((JobsData)jobList.get(position)).getLocation())));
        TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
        jobComp.setText(Html.fromHtml("<b>Company Name: </b> "+(((JobsData)jobList.get(position)).getCompanyName())));
        TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
        String salStr = "("+((JobsData)jobList.get(position)).getJobNo()+")"+"( "+((JobsData)jobList.get(position)).getExpMin()+ "- "+((JobsData)jobList.get(position)).getExpMax() +"years)";
        jobExp.setText("\n"+salStr);
        TextView jobRole = (TextView)convertView.findViewById(R.id.roleJob);
        jobRole.setText(Html.fromHtml("<b>Posted Date: </b> "+(CommonUtils.getCleanDate(((JobsData) jobList.get(position)).getPostDate()))));
        TextView jobKeySkills = (TextView)convertView.findViewById(R.id.keySkills);
        jobKeySkills.setText(Html.fromHtml("<b>Role: </b> "+(((JobsData)jobList.get(position)).getRole())+"<br/>"+
                "<b>CTC:</b> "+(((JobsData)jobList.get(position)).getMinSal())+"-"+(((JobsData)jobList.get(position)).getMaxSal()+" Lac(s) P.A")));
        TextView jobNotice = (TextView)convertView.findViewById(R.id.noticePeriod);
        jobNotice.setText(Html.fromHtml("<b>Notice Period: </b> "+(((JobsData)jobList.get(position)).getNoticePeriod()+" days")));
//                +"<br/>"+"<b>Job Description:</b> "+(((JobsData)jobList.get(position)).getJobDescription())));
        TextView viewed_count = (TextView)convertView.findViewById(R.id.viewed_count);
        viewed_count.setText(""+(((JobsData)jobList.get(position)).getViewsCount()));
        final TextView applied_count = (TextView)convertView.findViewById(R.id.applied_count);
        applied_count.setText("   Applied:  "+(((JobsData)jobList.get(position)).getAppliedCount())+"  ");
        TextView matchingProfiles_count = (TextView)convertView.findViewById(R.id.matchingProfiles_count);
        matchingProfiles_count.setText(""+(((JobsData)jobList.get(position)).getMatchedProfilesCount()));

        TextView matchingProfiles_txt = (TextView)convertView.findViewById(R.id.matchingProfiles_txt);

        matchingProfiles_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(((JobsData)jobList.get(position)).getMatchedProfilesCount()).equalsIgnoreCase("0")){
                    String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getMatchedProfiles, ((JobsData) jobList.get(position)).get_id());
                    context.startActivity(new Intent(context, ResumesListActivity.class).putExtra("matchingProfiles", preparedUrltoGetResumes));
                }else
                CommonUtils.showToast("No Mached profiles found",context);
            }
        });

        applied_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((((JobsData)jobList.get(position)).getAppliedCount()) != 0){
                    String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getAppliedResumesByJobId, ((JobsData) jobList.get(position)).get_id());
                    context.startActivity(new Intent(context, ResumesListActivity.class).putExtra("appliedResumes", "").putExtra("appliedResumes", preparedUrltoGetResumes));
                }else
                CommonUtils.showToast("No Applied jobs found",context);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jobList.size();
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return jobList.get(position);
    }

}