package com.localjobserver.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.WebDialog;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.slider.BaseActivity;
import com.localjobserver.slider.SlidingMenu;
import com.localjobserver.social.LinkedInSampleActivity;
import com.localjobserver.social.LinkedInSocialDialog;
import co.talentzing.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class JobListActivity extends BaseActivity {
    private SlidingMenu sampleMenu;
    private ProgressDialog progressDialog;
    public JobListActivity() {
        super(R.string.title_activity_job_list);
    }
    private JobListAdapter jobLllistAd = null;
    private ListView jobsListView;
    public ArrayList<JobsData> jobList;
    private ActionBar actionBar = null;
//    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    private CommonJobsListAdapter commonArrayAdapter;
    /* Any number for uniquely distinguish your request */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    private ProgressDialog pDialog;

    private static Twitter twitter;
    private static RequestToken requestToken;

    private static SharedPreferences mSharedPreferences;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    private TextView searchTxt;
    private ImageView editBtn;
    private String searchStr,ridStr,locationStr,selectedJobType;
    private String preparedUrltoGetJobs = "";
    private Bundle searchBundle = null;

    public static final String OAUTH_CALLBACK_HOST = "litestcalback";

    final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
            .getInstance().createLinkedInOAuthService(
                    Config.LINKEDIN_CONSUMER_KEY, Config.LINKEDIN_CONSUMER_SECRET);
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory
            .newInstance(Config.LINKEDIN_CONSUMER_KEY,
                    Config.LINKEDIN_CONSUMER_SECRET);
    LinkedInRequestToken liToken;
    LinkedInApiClient client;
    public static LinkedInAccessToken linkedInAccessToken = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_job_list);
        getSupportActionBar().setTitle("Jobs List");

        sampleMenu = getSlidingMenu();
        sampleMenu.setSlidingEnabled(false);
//        getSlidingMenu().setMode(SlidingMenu.RIGHT);
//        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//        getSlidingMenu().setSecondaryMenu(R.layout.menu_frame_two);
//        getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.menu_frame_two, new SampleListFragment())
//                .commit();

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        showHashKey(JobListActivity.this);
        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);

        jobsListView = (ListView)findViewById(R.id.jobListView);
        searchTxt = (TextView)findViewById(R.id.searchTxt);
        editBtn = (ImageView)findViewById(R.id.searchEdit);

        searchBundle  = getIntent().getExtras();
        if (searchBundle.containsKey("location")) {
            locationStr = searchBundle.getString("location");
            ridStr = searchBundle.getString("rid");
//            searchStr = locationStr;
            searchStr =  searchBundle.getString("mapStr");
//            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getLocalJobsList,ridStr,  (null != searchStr) ? searchStr : "");
            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getJobSearchResults, searchStr);
        }else if (getIntent().getStringExtra("APPLIEDBYSMS") != null){
            getSupportActionBar().setTitle("Applied By SMS");
            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getAppliedJobsBySMS, CommonUtils.getUserEmail(getApplicationContext()));
        }else if (getIntent().getStringExtra("SIMILARJOBS_ID") != null){
            getSupportActionBar().setTitle("Similar Jobs");
            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getSimilarJobs, getIntent().getStringExtra("SIMILARJOBS_ID"));
        }else {
            searchStr = searchBundle.getString("searchTxt");
            selectedJobType = searchBundle.getString("selectedJobType");
//            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getJobs, (null != searchStr) ? searchStr : "", selectedJobType);
            preparedUrltoGetJobs = String.format(Config.LJS_BASE_URL + Config.getJobSearchResults, searchStr);
        }

        searchTxt.setText("" +searchStr);
        getJobsData(preparedUrltoGetJobs);
        initTwitterConfigs();
        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent jobIntent = new Intent(JobListActivity.this, JobDescriptionActivity.class);
//                jobIntent.putExtra("jobItems", (ArrayList<JobsData>) jobList);
                CommonUtils.jobList = jobList;
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((JobsData) jobList.get(position)).get_id());
                jobIntent.setAction("fromSearch");
            startActivity(jobIntent);


//                startActivity(new Intent(JobListActivity.this, JobDescriptionActivity.class)
//                        .putExtra("jobItems", (ArrayList<JobsData>) jobList).putExtra("selectedPos", position)
//                        .putExtra("_id", ((JobsData) jobList.get(position)).get_id()).setAction("fromSearch"));
           }
       });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        if (id == R.id.action_filter) {
//           sampleMenu.toggle();
            startActivity(new Intent(JobListActivity.this, SeekerAdvanceSearchActivity.class).putExtra("Filter","Filter").putExtra("keySkills",searchBundle.getString("keySkills") != null ?searchBundle.getString("keySkills") : ""));
        }

        return super.onOptionsItemSelected(item);
    }

    public void getJobsData(final String url){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(JobListActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
//                    if (getIntent().getStringExtra("APPLIEDBYSMS") != null) {
                            CommonUtils.showToast("No job(s) are present",JobListActivity.this);
                               finish();
//                    }
                    if (result != null) {
                        Toast.makeText(JobListActivity.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    jobList = (ArrayList) result;
                    if (jobList.size() == 0){
                        Toast.makeText(getApplicationContext(),"No results found",Toast.LENGTH_LONG).show();
                        finish();
                    }
                    CommonUtils.jobList = (ArrayList) result;
                    commonArrayAdapter = new CommonJobsListAdapter(JobListActivity.this, jobList);
                    jobsListView.setAdapter(commonArrayAdapter);
                    progressDialog.dismiss();

                    if (getIntent().getStringExtra("APPLIEDBYSMS") != null) {
                        getSupportActionBar().setTitle("Applied By SMS" + "(" + jobList.size() + ")");
                    }else if (getIntent().getStringExtra("SIMILARJOBS_ID") != null){
                        getSupportActionBar().setTitle("Similar Jobs" + "(" + jobList.size() + ")");
                    } else
                        getSupportActionBar().setTitle("Jobs List" +"("+jobList.size()+")");



                }
            }
        }, url);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == WEBVIEW_REQUEST_CODE) {
                final String verifier = data.getExtras().getString(oAuthVerifier);

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

                            long userID = accessToken.getUserId();
                            final User user = twitter.showUser(userID);
                            String username = user.getName();

                            saveTwitterInfo(accessToken, data.getIntExtra("position", 0));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();

            } else {
              //  super.onActivityResult(requestCode, resultCode, data);
                Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
            }
            super.onActivityResult(requestCode, resultCode, data);
            //Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }
    }


    class JobListAdapter extends BaseAdapter {

        Activity context;
        public JobListAdapter(Activity context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.job_item_layout, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.jobNAME);
            jobTitle.setText(((JobsData)jobList.get(position)).getJobTitle());
            TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
            jobLoc.setText("" + (((JobsData)jobList.get(position)).getLocation()));
            TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
            jobComp.setText(""+(((JobsData)jobList.get(position)).getCompanyName()));
            TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
            jobExp.setText(""+(((JobsData)jobList.get(position)).getMaxSal()));
            convertView.findViewById(R.id.imageView2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginToTwitter(position);
                }
            });
            convertView.findViewById(R.id.imageView3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(JobListActivity.this, LinkedInSampleActivity.class));
                    StringBuffer lnStr = new StringBuffer();
                    lnStr.append("\nJob Title:");
                    lnStr.append("\n");
                    lnStr.append(jobList.get(position).getJobTitle());
                    lnStr.append("\n");
                    lnStr.append("More Info At:");
                    lnStr.append("\n");
                    lnStr.append("https://localjobserver.com/home.aspx");
                    startActivity(new Intent(JobListActivity.this, LinkedInSocialDialog.class).putExtra("lnData", lnStr.toString()));
//                    linkedInLogin(((JobsData)jobList.get(position)));
                }
            });
            convertView.findViewById(R.id.imgFacebook).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  Session.openActiveSession(context, true, new Session.StatusCallback() {

                        // callback when session changes state
                        public void call(Session session, SessionState state,
                                         Exception exception) {
                            if (session.isOpened()) {
                                publishFeedDialog(position);
                         }
                        }
                    });
                }
            });
            return convertView;
        }

       @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return jobList.size();
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return jobList.get(position);
        }

    }

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }


    private void publishFeedDialog(int position) {
        Bundle params = new Bundle();
        params.putString("name", ((JobsData)jobList.get(position)).getJobTitle());
        params.putString("caption", ((JobsData)jobList.get(position)).getCompanyProfile());
        params.putString("description", ((JobsData)jobList.get(position)) + "\n" + ((JobsData)jobList.get(position)));
        params.putString("link", "https://developers.facebook.com/android");

        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(this,
                        Session.getActiveSession(),
                        params))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Toast.makeText(getApplicationContext(),
                                        "Posted story, id: "+postId,
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(getApplicationContext(),
                                    "Error posting story",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                })
                .build();
        feedDialog.show();
    }


    private void saveTwitterInfo(AccessToken accessToken , int position) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();

			/* Storing oAuth tokens to shared preferences */
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();

            StringBuilder builder = new StringBuilder();
            builder.append(((JobsData)jobList.get(position))).append("\n").append(((JobsData)jobList.get(position)).getCompanyProfile()).append("\n").append(((JobsData)jobList.get(position)).getLocation()).append("\n").append(((JobsData) jobList.get(position)).getMaxSal());
            new updateTwitterStatus().execute(builder.toString());

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.localjobserver", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:","Check....."+Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    private void loginToTwitter(final int position) {
        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (!isLoggedIn) {

            new Thread(new Runnable() {
                @Override
                public void run() {


            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);

                /**
                 *  Loading twitter login page on webview for authorization
                 *  Once authorized, results are received at onActivityResult
                 *  */
                final Intent intent = new Intent(JobListActivity.this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                intent.putExtra("position", position);
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
                }
            }).start();
        }
        else
        {
            StringBuilder builder = new StringBuilder();
                    builder.append(((JobsData)jobList.get(position)).getJobTitle()).append("\n").append(((JobsData)jobList.get(position)).getCompanyProfile()).append("\n").append(((JobsData)jobList.get(position)).getLocation()).append("\n").append(((JobsData)jobList.get(position)).getMaxSal());
            new updateTwitterStatus().execute(builder.toString());
        }
    }


    class updateTwitterStatus extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(JobListActivity.this);
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(consumerKey);
                builder.setOAuthConsumerSecret(consumerSecret);

                // Access Token
                String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
                // Access Token Secret
                String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);


                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

			/* Dismiss the progress dialog after sharing */
            pDialog.dismiss();

            Toast.makeText(JobListActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
        }

    }
}
