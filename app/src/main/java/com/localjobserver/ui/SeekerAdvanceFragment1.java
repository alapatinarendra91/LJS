package com.localjobserver.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by nl on 29-Jun-16.
 */
public class SeekerAdvanceFragment1 extends Fragment {
    private View rootView;
    private MultiAutoCompleteTextView autoKeys;
    private Spinner IndustrySp,roleSp, walkinSp,expMinSp,expSp,PostedbySp,jobTypeSp;
    private String jobType_str,Postedby_str,walkin_str;
    private AutoCompleteTextView LocEdt,EduEdt;
    public StringBuffer searchStr;
    private Button searchBtn,cancilBtn;
    private ProgressDialog progressDialog;private static  int SEARCH_RESULT = 0;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_KEYSKILLS = 1;
    private static final int RESULT_INDUSTRIES = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_LOCALRESULT = 4;
    private static final int RESULT_DISIGNATIONS = 5;
    private static final int RESULT_EDUCATION = 6;
    private static final int RESULT_ROLES = 7;
    private String selectedInd = "";
    public LinkedHashMap<String,String> industries = null, locationsMap = null,disigntionMap = null,educationMap,roleMap;
    public ArrayList<String> keySkillsList = null;
    private static final String ARG_PARAM1 = "param1";

    public static SeekerAdvanceFragment1 newInstance(int param1) {
        SeekerAdvanceFragment1 fragment = new SeekerAdvanceFragment1();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public SeekerAdvanceFragment1() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_seeker_advance_search, container, false);

        autoKeys = (MultiAutoCompleteTextView)rootView.findViewById(R.id.autoskillskeyword);
        autoKeys.setThreshold(1);
        autoKeys.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        LocEdt = (AutoCompleteTextView)rootView.findViewById(R.id.LocEdt);
        EduEdt = (AutoCompleteTextView)rootView.findViewById(R.id.EduEdt);
        expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        roleSp = (Spinner)rootView.findViewById(R.id.roleSp);
        IndustrySp = (Spinner)rootView.findViewById(R.id.IndustrySp);
        walkinSp = (Spinner)rootView.findViewById(R.id.walkinSp);
        PostedbySp = (Spinner)rootView.findViewById(R.id.PostedbySp);
        jobTypeSp = (Spinner)rootView.findViewById(R.id.jobTypeSp);
        searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
        cancilBtn = (Button)rootView.findViewById(R.id.cancelBtn);

        searchStr  = new StringBuffer();
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expMinSp.setSelection(1);
        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        jobTypeSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.positiontype)));
        PostedbySp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.postedBy)));
        walkinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.referralJobs)));
        roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));

        expMinSp.setOnItemSelectedListener(spinListener);
        expSp.setOnItemSelectedListener(spinListener);
        IndustrySp.setOnItemSelectedListener(spinListener);
        jobTypeSp.setOnItemSelectedListener(spinListener);
        walkinSp.setOnItemSelectedListener(spinListener);
        PostedbySp.setOnItemSelectedListener(spinListener);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStr.append(autoKeys.getText().toString());
                if (!CommonUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                    return;
                }
                LinkedHashMap<String,String> input = toUpload();
                JSONObject json = new JSONObject(input);
                String requestString = json.toString();
                requestString = CommonUtils.encodeURL(requestString);

                Bundle searchBundle = new Bundle();
                searchBundle.putString("searchTxt", requestString);
                searchBundle.putString("selectedJobType", selectedInd);
                Intent jobListIntent = new Intent(getActivity(), JobListActivity.class);
                jobListIntent.putExtras(searchBundle);
                startActivity(jobListIntent);
            }
        });

        cancilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        getKeyWords();
        getIndustries();
        getLocations();
        getEducations();


        return rootView;
    }

//        @Override
//        public void onAttach(Activity activity) {
//            super.onAttach(activity);
//            try {
//                if (null != (MainInfoActivity) activity) {
//                    ((MainInfoActivity) activity).onSectionAttached("Search Jobs");
//                }
//            } catch (ClassCastException e) {
//                throw new ClassCastException(activity.toString()
//                        + " must implement OnFragmentInteractionListener");
//            }
//        }

    private  LinkedHashMap  toUpload(){
        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put("KeyWords",autoKeys.getText().toString());
        seekerDataMap.put("Location",LocEdt.getText().toString());
        seekerDataMap.put("SubLocation","");
        seekerDataMap.put("MinExp",""+expMinSp.getSelectedItem().toString());
        seekerDataMap.put("MaxExp",""+expSp.getSelectedItem().toString());
        seekerDataMap.put("MaxSal","0");
        seekerDataMap.put("MinSal","0");
        seekerDataMap.put("JobType",jobType_str);
        seekerDataMap.put("Industry",""+selectedInd);
        seekerDataMap.put("Role","");
        seekerDataMap.put("Education",EduEdt.getText().toString());
        seekerDataMap.put("PostedBy",Postedby_str);
        seekerDataMap.put("JType",walkin_str);
        return seekerDataMap;
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        });
    }


    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getIndusPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public void getRoles(final String selectedInd){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    roleMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_ROLES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getRoles, selectedInd), CommonKeys.arrRoles);
    }

    public void getDisignation(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    disigntionMap = (LinkedHashMap<String, String>) result;
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISIGNATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getDesignations), CommonKeys.arrDesignations);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_EDUCATION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }


    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });
    }
    class StatusHandler extends Handler {

        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what){
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occured while getting data from  server.", Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_KEYSKILLS:
                    if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()){
//                            autoKeys.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                        autoKeys.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                    }
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries && industries.size() > 0 && isVisible()) {
                        IndustrySp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(industries, "Industry")));
                    }
                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap && locationsMap.size() > 0 && isVisible()){
//                        LocEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(locationsMap)));
                        LocEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                    }
                    break;
                case RESULT_ROLES:
                    if (null != roleMap && null != roleSp) {
                        roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                    }
                    break;
                case RESULT_EDUCATION:
                    if (null != educationMap ) {
                        EduEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Education")));
                    } else {
                        Toast.makeText(getActivity(),"Not able to get education details",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.IndustrySp:
                    if (null != industries && IndustrySp.getSelectedItemId() !=0){
                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
                        getRoles(selectedInd);
                    }
                    break;
                case R.id.expMinSp:
                    if (expMinSp.getSelectedItemId() == 31){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() == 1){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() != 0){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.expSp:
                    break;

                case R.id.walkinSp:
                    if (walkinSp.getSelectedItemId() != 0)
                        walkin_str = walkinSp.getSelectedItem().toString();
                    else
                        walkin_str = "";
                    break;

                case R.id.jobTypeSp:
                    if (jobTypeSp.getSelectedItemId() != 0)
                        jobType_str = (jobTypeSp.getSelectedItemId()) +"";
                    else
                        jobType_str = "";
                    break;

                case R.id.PostedbySp:
                    if (PostedbySp.getSelectedItemId() != 0)
                        Postedby_str = String.valueOf(PostedbySp.getSelectedItemPosition());
                    else
                        Postedby_str = "";
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
