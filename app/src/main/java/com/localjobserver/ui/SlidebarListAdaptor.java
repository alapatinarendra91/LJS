package com.localjobserver.ui;

import android.app.Activity;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.localjobserver.commonutils.CommonUtils;

import co.talentzing.R;

import java.util.List;

/**
 * Created by ANDROID on 02/01/2017.
 */
public class SlidebarListAdaptor extends BaseAdapter {

    Activity context;
    private List<String> listNamesArray;
    TypedArray imgs;
    public SlidebarListAdaptor(Activity context,TypedArray imgs,List<String> listNamesArray) {
        super();
        this.context=context;
        this.imgs=imgs;
        this.listNamesArray=listNamesArray;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.sliderlist, null);
        }
        ImageView listImage = (ImageView)convertView.findViewById(R.id.listImage);
        listImage.setImageResource(imgs.getResourceId(position, -1));
        final TextView listName = (TextView)convertView.findViewById(R.id.listName);
        listName.setText(listNamesArray.get(position).toString());
//        if (position == 3 && listNamesArray.get(position).toString().equalsIgnoreCase(context.getString(R.string.venueDetails))){
//            listImage.setImageResource(R.drawable.ic_launcher);
//            listName.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    CommonUtils.showToast(""+listName.getText().toString(),context);
//                }
//            });
//        }


        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listNamesArray.size();
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listNamesArray.get(position);
    }

}
