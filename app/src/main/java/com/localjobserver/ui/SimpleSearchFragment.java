package com.localjobserver.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.KeyskillsModel;
import com.localjobserver.models.SearchModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SimpleSearchFragment extends Fragment {

    private Button _submit_btn, clearBtn;

    private View rootView;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_KEYSKILLS = 2;
    private static final int  RESULT_LOCATIONS = 3;
    private static final int RESULT_INDUSTRIES = 4;
    public ArrayList<String> keySkillsList = null;
    private SharedPreferences appPrefs;
    private AutoCompleteTextView preLocEdt;
    private MultiAutoCompleteTextView keySkillsEdt;
    private Spinner expSp,expMinSp,currentIndeSpin;
    private SearchModel searchModel;
    private String selectedInd = "";
    public LinkedHashMap<String,String> industries = null, locationsMap = null;
    List<KeyskillsModel> keySkillsDataList;

    public SimpleSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView = inflater.inflate(R.layout.fragment_simple_search, container, false);
        preLocEdt = (AutoCompleteTextView)rootView.findViewById(R.id.preLocEdt);
        keySkillsEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt);
        keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        searchModel = new SearchModel();
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expMinSp.setOnItemSelectedListener(spinListener);
        currentIndeSpin = (Spinner)rootView.findViewById(R.id.currentIndeSpin);
        currentIndeSpin.setOnItemSelectedListener(spinListener);
        _submit_btn = (Button)rootView.findViewById(R.id.submitBtn);
        clearBtn = (Button)rootView.findViewById(R.id.cancelBtn);

        getKeyWords();
        getIndustries();
        getLocations();

        _submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (validateSearchFieldsDetails() || spinnerValidation()) {

                    postSearchData(toPostSearch(searchModel));
                } else
                    Toast.makeText(getActivity(), "Select any of one", Toast.LENGTH_SHORT).show();
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    clearFields();
            }
        });


//        keySkillsEdt.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                System.out.println("Text [" + s + "]");
//
//                mSearchableAdapter.getFilter().filter(s.toString());
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });


        return rootView;
    }


    public void clearFields(){
        preLocEdt.setText("");
        keySkillsEdt.setText("");
        expSp.setSelection(0);
        expMinSp.setSelection(0);
        currentIndeSpin.setSelection(0);
    }

    public void postSearchData(LinkedHashMap<String,String> input){
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.ResumeResults, "" + requestString);
        Log.i("", "preparedUrltoGetResumes is  :" + preparedUrltoGetResumes);
        startActivity(new Intent(getActivity(), ResumesListActivity.class).putExtra("searchTxt", "").putExtra("resumesLink", preparedUrltoGetResumes));
    }

    public boolean validateSearchFieldsDetails(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.preLocEdt)).validate(NotEmpty.build(getActivity())));

        return (mForm.isValid()) ? true : false;
    }


    public  boolean spinnerValidation(){
//        if (!CommonUtils.spinnerSelect("Experience Minimum",expSp.getSelectedItemPosition(),getActivity()) ||
//                !CommonUtils.spinnerSelect("Experience Maximum",expMinSp.getSelectedItemPosition(),getActivity())||
//                !CommonUtils.spinnerSelect("Industry Type",currentIndeSpin.getSelectedItemPosition(),getActivity())
//                ){
//            return false;
//        }

        if (expMinSp.getSelectedItemPosition() !=0)
            return true;
        else if (currentIndeSpin.getSelectedItemPosition() != 0)
            return true;

        return false;
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.currentIndeSpin:
                    if (null != industries && currentIndeSpin.getSelectedItemPosition() != 0){
                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
                        searchModel.setIndustry(currentIndeSpin.getSelectedItem().toString());
                        searchModel.setIndId(selectedInd);
                    }
                    break;
                case R.id.expSp:
                    searchModel.setMaxExp(expSp.getSelectedItem().toString());

                    break;
                case R.id.expMinSp:
                    searchModel.setMinExp(expMinSp.getSelectedItem().toString());
                    if (expMinSp.getSelectedItemId() == 31){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() != 0){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }
                    break;


            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    public LinkedHashMap toPostSearch(SearchModel searchModel){
        LinkedHashMap postSearch = new LinkedHashMap();
        postSearch.put("ReturnUrl","");
        postSearch.put("KeySkills",keySkillsEdt.getText().toString());
        postSearch.put("Scope","");
        postSearch.put("Location",preLocEdt.getText().toString());
        postSearch.put("MinExp",""+searchModel.getMinExp());
        postSearch.put("MaxExp",""+searchModel.getMaxExp());
        postSearch.put("Industry",searchModel.getIndustry());
        postSearch.put("FunctionalArea","");
        postSearch.put("CompanyName","");
        postSearch.put("Role","");
        postSearch.put("HighestDegree","");
        postSearch.put("CandidatesActiveinLast","");
        postSearch.put("ResumeperPage","");
        postSearch.put("ActiveDate","");
        postSearch.put("PreferedLocation","");
        postSearch.put("SearchQuery","");
        postSearch.put("Email",""+CommonUtils.getUserEmail(getActivity()));
        postSearch.put("SearchType","");
        postSearch.put("Date",""+CommonUtils.getDateTime());
        postSearch.put("SaveId","0");
        postSearch.put("Rid","0");
        postSearch.put("Fid","0");
        postSearch.put("IndId",""+searchModel.getIndId());
        postSearch.put("Education","");
        postSearch.put("NoticePeriod","0");
        return postSearch;
    }
    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String,String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);

                }
            }
        });
    }

    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }


    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {

                            Type collectionType = new TypeToken<Collection<KeyskillsModel>>() {
                            }.getType();
                            Gson googleJson = new Gson();
                            keySkillsDataList = googleJson.fromJson((String) result, collectionType);
                            Log.v(LiveData.class.getSimpleName(), "Resumes size.." + keySkillsDataList.size());

                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }
    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:

                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap) {
                        preLocEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                    }else{
                        Toast.makeText(getActivity(),"Not able to get locations",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_KEYSKILLS:
//                    keySkillsEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
//                    keySkillsEdt.setThreshold(1);

                    keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                    keySkillsEdt.setThreshold(1);

                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries) {
                        currentIndeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(industries, "Industry")));
                    }else{
                        Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((MainInfoActivity) activity).onSectionAttached("Simple Search");
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
