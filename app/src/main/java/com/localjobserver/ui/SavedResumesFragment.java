package com.localjobserver.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SavedResumesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavedResumesFragment extends Fragment {

    public View rootView;
    private ProgressDialog progressDialog;
    private ListView localJobsFoldersListView;
    private List<String> arrFolders;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static  int SEARCH_RESULT = 0;
    private SharedPreferences appPrefs;

    public SavedResumesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_saved_resumes, container, false);
        localJobsFoldersListView = (ListView)rootView.findViewById(R.id.localJobsFoldersListView);
        arrFolders = new ArrayList<String>();
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        localJobsFoldersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getSavedResumes, CommonUtils.getUserEmail(getActivity()), arrFolders.get(position));

                startActivity(new Intent(getActivity(), ResumesListActivity.class).putExtra("searchTxt", "").putExtra("resumesLink",preparedUrltoGetResumes));
            }
        });

        getFolders();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
//            mListener = (OnFragmentInteractionListener) activity;
            ((MainInfoActivity) activity).onSectionAttached("Saved Resumes");
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void getFolders(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getFolders(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("No folders at present",getActivity());
                    return;
                }
                if (result != null) {
                    arrFolders = (ArrayList<String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getSavedFolders,  appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "")));
    }

    class FoldersListAdpter extends BaseAdapter {

        Context context;
        public FoldersListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.localjobs_list_row, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.title);
            jobTitle.setText(arrFolders.get(position));
            TextView jobLoc = (TextView)convertView.findViewById(R.id.count);
            jobLoc.setVisibility(View.GONE);
            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return arrFolders.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return arrFolders.get(position);
        }

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while getting the data.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    FoldersListAdpter lFolderssAdaper = new FoldersListAdpter(getActivity());
                    localJobsFoldersListView.setAdapter(lFolderssAdaper);
                    break;
            }
        }
    }

}
