package com.localjobserver.ui;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.localjobserver.networkutils.ApplicationThread;


public class LocationService extends Service implements LocationListener {
    private static final String LOG_TAG = LocationService.class.getName();

    public class ServiceBinder extends Binder {
        public LocationService getService() {
            return LocationService.this;
        }
    }

    private final IBinder mBinder = new ServiceBinder();

    private static final int MIN_UPDATE_TIME = 250;
    private static final int MIN_UPDATE_DISTANCE = 0;

    private Location location;
    public LocationManager locationManager;

    public void startLocationService(ApplicationThread.OnComplete onComplete) {
        String providerType = null;
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            boolean gpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (gpsProviderEnabled || networkProviderEnabled) {
                if (networkProviderEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        providerType = "network";
                        Log.d(LOG_TAG, "network lbs provider:" + (location == null ? "null" : String.valueOf(location.getLatitude()) +"," + String.valueOf(location.getLongitude())));
                    }
                }

                if (gpsProviderEnabled && location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        providerType = "gps";
                        Log.d(LOG_TAG, "gps lbs provider:" + (location == null ? "null" : String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude())));
                    }
                }
            }

        } catch (Exception e) {
           Log.e(LOG_TAG, "Cannot get location", e);
        }

        if (onComplete != null) {
            onComplete.execute(location != null, location, providerType);
        }
    }

    public void getLocation (final ApplicationThread.OnComplete onComplete) {
        if (location != null) {
            onComplete.execute(true, location, null);
        } else {
            startLocationService(new ApplicationThread.OnComplete() {
                @Override
                public void execute(boolean success, Object result, String msg) {
                    if (!success) {
                        onComplete.execute(false, null, null);
                    } else {
                        onComplete.execute(true, result, null);
                    }
                }
            });
        }
    }

    public void stopLocationService(){
        if (locationManager != null) {
            locationManager.removeUpdates(LocationService.this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startLocationService(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
