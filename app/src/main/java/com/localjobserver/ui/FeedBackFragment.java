package com.localjobserver.ui;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedBackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedBackFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private EditText fName, email,message;
    private View rootView;
    private RadioGroup rateStatus;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private SharedPreferences appPrefs;
    private Button submitBtn;
    private String radiovalue;
    private RadioButton radioButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_feed_back, container, false);
        fName = (EditText)rootView.findViewById(R.id.fullNameEdit);
        email = (EditText)rootView.findViewById(R.id.emailEdit);
        message = (EditText)rootView.findViewById(R.id.msgEdit);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        rateStatus = (RadioGroup)rootView.findViewById(R.id.radioRate);
        email.setText(""+appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFeedBackDetails()){

                    if (rateStatus.getCheckedRadioButtonId() == -1){
                        CommonUtils.showToast("Please Rate us " , getActivity());
                        return;
                    }

                     radiovalue = ((RadioButton)rootView.findViewById(rateStatus.getCheckedRadioButtonId())).getText().toString();
                    int selectedId = rateStatus.getCheckedRadioButtonId();
                    radioButton = (RadioButton) rootView.findViewById(selectedId);
                    radiovalue = radioButton.getText().toString();

                    uploadFeedback();
                }
            }
        });

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().popBackStack();
                clearFields();
                getActivity().finish();
            }
        });
        return rootView;
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            ((MainInfoActivity) activity).onSectionAttached("Feed Back");
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }


    public void clearFields(){
        fName.setText("");
//        email.setText("");
        message.setText("");

    }


    public boolean validateFeedBackDetails(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.fullNameEdit)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.emailEdit)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.msgEdit)).validate(NotEmpty.build(getActivity())));
        return (mForm.isValid()) ? true : false;
    }

    public void uploadFeedback() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        int selectedId = rateStatus.getCheckedRadioButtonId();
        String selectedStr = "";
        RadioButton rateButton = (RadioButton)rootView.findViewById(selectedId);

//        Toast.makeText(getActivity(),"is : "+radiovalue,Toast.LENGTH_SHORT).show();
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Email Send failed",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.sendFeedcack,fName.getText().toString(), email.getText().toString(),message.getText().toString(), radiovalue));

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    Toast.makeText(getActivity(), "Email Send successfully.", Toast.LENGTH_SHORT).show();
                    clearFields();
                    getActivity().finish();
                    break;
            }
        }
    }

}
