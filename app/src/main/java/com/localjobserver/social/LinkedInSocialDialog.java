package com.localjobserver.social;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

public class LinkedInSocialDialog extends Activity {

    public static LinkedInApiClientFactory factory;
    public static LinkedInOAuthService oAuthService;
    public static LinkedInRequestToken liToken;
    LinkedInAccessToken accessToken = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_linked_in_social_dialog);

        if (Build.VERSION.SDK_INT >= 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        new NetworkTask().execute();
    }


    private class NetworkTask extends AsyncTask<String, Void, HttpResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected HttpResponse doInBackground(String... params) {

            try {
                setWebView();

            } finally {
            }
            return null;
        }

        @Override
        protected void onPostExecute(HttpResponse result) {
            loadWebView();

            setVerifierListener(new OnVerifyListener() {
                @SuppressLint("NewApi")
                @Override
                public void onVerify(String verifier) {
                    try {
                        Log.e("LinkedinSample", "verifier: " + verifier);

                        accessToken = oAuthService.getOAuthAccessToken(liToken,verifier);
                        factory
                                .createLinkedInApiClient(accessToken);

                        Log.e("LinkedinSample", "ln_access_token: "
                                + accessToken.getToken());
                        Log.e("LinkedinSample", "ln_access_token: "
                                + accessToken.getTokenSecret());


                        new PostRequest().execute();

                        finish();

                    } catch (Exception e) {
                        Log.e("LinkedinSample", "error to get verifier");
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private class PostRequest extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(String... params) {
            org.apache.http.HttpResponse response = null;

//            String share = getIntent().getStringExtra("lnData");

            String share = getIntent().getStringExtra("lnData");
            if (null != share && !share.equalsIgnoreCase("")) {
                OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
                        Config.LINKEDIN_CONSUMER_KEY,
                        Config.LINKEDIN_CONSUMER_SECRET);
                consumer.setTokenWithSecret(accessToken.getToken(),
                        accessToken.getTokenSecret());

                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost post = new HttpPost(
                        "https://api.linkedin.com/v1/people/~/shares");
                try {
                    consumer.sign(post);
                } catch (OAuthMessageSignerException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (OAuthExpectationFailedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (OAuthCommunicationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } // here need the consumer for sign in for post the share

                String myEntity = "<share><comment>"
                        + share
                        + "</comment><visibility><code>anyone</code></visibility></share>";

                post.setHeader("Content-Type",
                        "application/xml;charset=UTF-8");
                try {
                    StringEntity entity = new StringEntity(myEntity);
                    entity.setContentType("text/xml");
                    post.setEntity(entity);
                    response = httpclient
                            .execute(post);

                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                return false;
            }
            if ( response != null && response.getStatusLine().getStatusCode() == 200
                    || response.getStatusLine().getStatusCode() == 201) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(LinkedInSocialDialog.this,
                        "Posted Successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(LinkedInSocialDialog.this,
                        "Failed to post your update", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    /**
     * set webview.
     */
    public void setWebView() {
        try {
            oAuthService = LinkedInOAuthServiceFactory.getInstance()
                    .createLinkedInOAuthService(Config.LINKEDIN_CONSUMER_KEY,
                            Config.LINKEDIN_CONSUMER_SECRET);
            factory = LinkedInApiClientFactory.newInstance(
                    Config.LINKEDIN_CONSUMER_KEY, Config.LINKEDIN_CONSUMER_SECRET);

           liToken = oAuthService
                    .getOAuthRequestToken(Config.OAUTH_CALLBACK_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void loadWebView() {
        //	Log.e("i am here", LinkedinDialog.liToken.toString());

        WebView mWebView = (WebView) findViewById(R.id.webkitWebView1);

        mWebView.getSettings().setJavaScriptEnabled(true);
        Log.e("LinkedinSample", ""+liToken.getAuthorizationUrl());
        mWebView.loadUrl(liToken.getAuthorizationUrl());
        mWebView.setWebViewClient(new HelloWebViewClient());
    }

    /**
     * webview client for internal url loading
     * callback url: "https://www.linkedin.com/uas/oauth/mukeshyadav4u.blogspot.in"
     */
    class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains(Config.OAUTH_CALLBACK_URL)) {
                Uri uri = Uri.parse(url);
                String verifier = uri.getQueryParameter("oauth_verifier");

//                cancel();

                for (OnVerifyListener d : listeners) {
                    // call listener method
                    d.onVerify(verifier);
                }
            } else if (url
                    .contains("https://www.linkedin.com/uas/oauth/mukeshyadav4u.blogspot.in")) {
//                cancel();
            } else {
                Log.i("LinkedinSample", "url: " + url);
                view.loadUrl(url);
            }

            return true;
        }
    }

    /**
     * List of listener.
     */
    private List<OnVerifyListener> listeners = new ArrayList<OnVerifyListener>();

    /**
     * Register a callback to be invoked when authentication have finished.
     *
     * @param data The callback that will run
     */
    public void setVerifierListener(OnVerifyListener data) {
        listeners.add(data);
    }

    /**
     * Listener for oauth_verifier.
     */
    interface OnVerifyListener {
        /**
         * invoked when authentication have finished.
         *
         * @param verifier oauth_verifier code.
         */
        public void onVerify(String verifier);
    }

}
