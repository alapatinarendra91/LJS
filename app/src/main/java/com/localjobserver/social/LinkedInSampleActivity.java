package com.localjobserver.social;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.localjobserver.networkutils.Config;
import com.localjobserver.social.LinkedinDialog.OnVerifyListener;
import co.talentzing.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

/**
 * @author Mukesh Kumar Yadav
 */
public class LinkedInSampleActivity extends Activity {
	Button login;
	Button share;
	EditText et;
	TextView name, profile;
	ImageView photo;

	final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
			.getInstance().createLinkedInOAuthService(
					Config.LINKEDIN_CONSUMER_KEY,
					Config.LINKEDIN_CONSUMER_SECRET);
	final LinkedInApiClientFactory factory = LinkedInApiClientFactory
			.newInstance(Config.LINKEDIN_CONSUMER_KEY,
					Config.LINKEDIN_CONSUMER_SECRET);
	LinkedInRequestToken liToken;
	LinkedInApiClient client;
	LinkedInAccessToken accessToken = null;

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		if (Build.VERSION.SDK_INT >= 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		share = (Button) findViewById(R.id.share);
		name = (TextView) findViewById(R.id.name);
		profile = (TextView) findViewById(R.id.profile);
		et = (EditText) findViewById(R.id.et_share);
		login = (Button) findViewById(R.id.login);
		photo = (ImageView) findViewById(R.id.photo);

		new NetworkTask().execute();

		login.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				new NetworkTask().execute();
			}
		});

		// share on linkedin
		share.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				new PostRequest().execute();
			}
		});
	}

	private class NetworkTask extends AsyncTask<String, Void, HttpResponse> {
		LinkedinDialog d;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			d = new LinkedinDialog(LinkedInSampleActivity.this);
			d.show();
		}

		@Override
		protected HttpResponse doInBackground(String... params) {

			try {
				d.setWebView();

			} finally {
			}
			return null;
		}

		@Override
		protected void onPostExecute(HttpResponse result) {
			d.loadWebView();

			d.setVerifierListener(new OnVerifyListener() {
				@SuppressLint("NewApi")
				@Override
				public void onVerify(String verifier) {
					try {
						Log.i("LinkedinSample", "verifier: " + verifier);

						accessToken = LinkedinDialog.oAuthService
								.getOAuthAccessToken(LinkedinDialog.liToken,
										verifier);
						LinkedinDialog.factory
								.createLinkedInApiClient(accessToken);

						Log.i("LinkedinSample", "ln_access_token: "
								+ accessToken.getToken());
						Log.i("LinkedinSample", "ln_access_token: "
								+ accessToken.getTokenSecret());

						login.setVisibility(View.GONE);
						share.setVisibility(View.VISIBLE);
						et.setVisibility(View.VISIBLE);
					}

					catch (Exception e) {
						Log.i("LinkedinSample", "error to get verifier");
						e.printStackTrace();
					}
				}
			});
		}
	}

	private class PostRequest extends AsyncTask<String, Void, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Boolean doInBackground(String... params) {
			org.apache.http.HttpResponse response = null;

			String share = et.getText().toString();
			if (null != share && !share.equalsIgnoreCase("")) {
				OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
						Config.LINKEDIN_CONSUMER_KEY,
						Config.LINKEDIN_CONSUMER_SECRET);
				consumer.setTokenWithSecret(accessToken.getToken(),
						accessToken.getTokenSecret());

				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost post = new HttpPost(
						"https://api.linkedin.com/v1/people/~/shares");
				try {
					consumer.sign(post);
				} catch (OAuthMessageSignerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OAuthExpectationFailedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OAuthCommunicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // here need the consumer for sign in for post the share

				String myEntity = "<share><comment>"
						+ share
						+ "</comment><visibility><code>anyone</code></visibility></share>";

				post.setHeader("Content-Type",
						"application/xml;charset=UTF-8");
				try {
					StringEntity entity = new StringEntity(myEntity);
					entity.setContentType("text/xml");
					post.setEntity(entity);
					response = httpclient
							.execute(post);

				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				return false;
			}
			if ( response != null && response.getStatusLine().getStatusCode() == 200
					|| response.getStatusLine().getStatusCode() == 201) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				Toast.makeText(LinkedInSampleActivity.this,
						"Posted Successfully", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(LinkedInSampleActivity.this,
						"Please enter the text to share", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
}

