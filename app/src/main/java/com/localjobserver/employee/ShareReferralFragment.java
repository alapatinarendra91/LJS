package com.localjobserver.employee;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShareReferralFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShareReferralFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private EditText toMail,fromemailEdit,subjectEdit,message;
    private Spinner selectJobSpin;
    private CheckBox getSavedContactscheck;
    private View rootView;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_JobshareData = 2;
    private static final int RESULT_SavedContacts = 3;
    private SharedPreferences appPrefs;
    private Button submitBtn;
    private String configUrl = "";
    private LinkedHashMap<String, String> sharedjobmap;
    public String savedContacts = "",jobid_str= "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.activity_share_referral_fragment, container, false);
        toMail = (EditText)rootView.findViewById(R.id.toMail);
        fromemailEdit = (EditText)rootView.findViewById(R.id.fromemailEdit);
        subjectEdit = (EditText)rootView.findViewById(R.id.subjectEdit);
        message = (EditText)rootView.findViewById(R.id.msgEdit);
        selectJobSpin = (Spinner)rootView.findViewById(R.id.selectJobSpin);
        getSavedContactscheck = (CheckBox)rootView.findViewById(R.id.getSavedContactscheck);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        fromemailEdit.setText(""+appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);

        getgenericvalues();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateMailDetails()){
                    for (Map.Entry<String, String> entry : sharedjobmap.entrySet()) {
                        if (entry.getValue().equals(selectJobSpin.getSelectedItem().toString())) {
                            Log.e("","entry is : "+entry.getValue()+", "+selectJobSpin.getSelectedItem().toString());
                            jobid_str = entry.getKey();
                            uploadMail();
                        }
                    }

                }
            }
        });

        getSavedContactscheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSavedContactscheck.isChecked()){
                    if (savedContacts.equals(""))
                        getSavedContactsData();
                     else
                        toMail.setText(savedContacts);

                }else {
                    toMail.setText("");
                }
            }
        });

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().finish();
            }
        });
        return rootView;
    }


    public boolean validateMailDetails(){

        if (!CommonUtils.spinnerSelect("Job",selectJobSpin.getSelectedItemPosition(),getActivity())){
            return false;
        }
        Form mForm = new Form(getActivity());

        if (!toMail.getText().toString().contains(","))
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.toMail)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.fromemailEdit)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.subjectEdit)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.msgEdit)).validate(NotEmpty.build(getActivity())));
        return (mForm.isValid()) ? true : false;
    }

    public void uploadMail() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Mail Send failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.EmployeeShareReferral, jobid_str, fromemailEdit.getText().toString(), toMail.getText().toString(), subjectEdit.getText().toString().replace("|", ","), message.getText().toString()));

    }

    public void getgenericvalues(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }

        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    sharedjobmap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_JobshareData;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getJobsListToShare, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "")), CommonKeys.arrSharedJobsdata);
    }

    public void getSavedContactsData() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        ApplicationThread.bgndPost(getClass().getSimpleName(), "getTrainerDetailsHere...", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getSavedContacts, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "")), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null || result.toString().contains("Invalid")) {
                            return;
                        }
                            if (result != null && ((String) result).toString().length() > 0) {
                                progressDialog.dismiss();
                                 savedContacts = ((String) result).toString().replace("\"","");
                                Message messageToParent = new Message();
                                messageToParent.what = RESULT_SavedContacts;
                                Bundle bundleData = new Bundle();
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);

                            }


                    }
                });
            }
        });
    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    Toast.makeText(getActivity(), "Job shared successfully..", Toast.LENGTH_SHORT).show();
//                    getActivity().finish();
//                    clearFields();
                    break;

                case RESULT_JobshareData:
                    selectJobSpin.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(sharedjobmap, " Job")));
                    break;
                case RESULT_SavedContacts:
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toMail.setText("" + savedContacts);
                        }
                    });

                    break;
            }
        }
    }



}
