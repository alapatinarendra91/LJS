package com.localjobserver.employee;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonDialogFragment;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.models.RecuiterObject;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.MainInfoActivity;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.IsPositiveInteger;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link EmployeeRegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeRegisterFragment extends Fragment implements CommonDialogFragment.YesNoDialogListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ProgressDialog progressDialog;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String profile_pic = "";

    private Spinner industrytypeSpnr;
    private  TextView agree_txt,ex_pwd_text;
    LinearLayout c_pw_lyt,pwd_lyt;
    private EditText edtfirstname, edtlastname,  edtemail, edtcompanyurl, edtpassword, edtconfirmpwd, edtmobilenumber, edtlandline,
            edtaddress, edtcompanyprofile,dateOfBirth;
    private MultiAutoCompleteTextView languages;
    private AQuery aQuery = null;
    private AutoCompleteTextView edtcompanyname,edtdesignation;
    private Button btnSubmit,cancilbtnbtn, btnChoosefile,btncompanylogo;
    private ImageView companylogoimgv,eye_image;
    private View rootView;
    private static final int RESULT_ERROR = 0,SELECT_PICTURE =1,FROMCAM = 2,RESULT_COMPANIES = 3,RESULT_DISIGNATIONS=4,RESULT_INDUSTRIES = 5;
    private RecuiterObject recuiterObject=null;
    private byte[] seekerImageData;
    private CheckBox emailChk, SMSview,Chatview, noneView,chk_agree;
    public LinkedHashMap<String,String> industries = null, companiesMap = null, disigntionMap;
    private SharedPreferences preferences;
    private SharedPreferences gcmPrefs = null;
    private String gcmId = "";
    List<RecuiterObject> arrRecLisr = null;
    ArrayList<String> industry_list;
    private Calendar myCalendar = Calendar.getInstance();

    private LinearLayout dailyStatus, selectTime, timeStatus,profileAlerts_lay;
    private RadioButton radioDaily, radioWeekly, radioInstant;
    private Spinner daysSpin;
    private String selectedDay = "", compressedStr = "";
    private RadioGroup radioTimeStatus,radioStatus;

    private int flag;
    private static final int RESULT_USERDATA = 12;
    private boolean take_photo = false;
    private boolean eye_var = false;
    private String selectedStr;
    private ArrayList<HashMap> morderHashMapList;
    protected static boolean isClassVisible = false;

    /**boolean
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment RecrutierRegistrationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeeRegisterFragment newInstance(int param1) {
        EmployeeRegisterFragment fragment = new EmployeeRegisterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public EmployeeRegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.employee_register_fragment, container, false);
        recuiterObject=new RecuiterObject();

        preferences = getActivity().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);
        gcmPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        gcmId = gcmPrefs.getString("gcmRegId", "");

        aQuery = new AQuery(getActivity());
        progressDialog = new ProgressDialog(getActivity());

        industrytypeSpnr=(Spinner)rootView.findViewById(R.id.industrytypeEdt);

        edtfirstname=(EditText)rootView.findViewById(R.id.firstnameEdt);
        edtlastname=(EditText)rootView.findViewById(R.id.lastnameEdt);
        edtcompanyname=(AutoCompleteTextView)rootView.findViewById(R.id.companynameEdt);
        edtemail=(EditText)rootView.findViewById(R.id.emailEdt);
        edtcompanyurl=(EditText)rootView.findViewById(R.id.companyurlEdt);
        edtpassword=(EditText)rootView.findViewById(R.id.passwordEdt);
        edtconfirmpwd=(EditText)rootView.findViewById(R.id.confirmpasswordEdt);
        edtdesignation=(AutoCompleteTextView)rootView.findViewById(R.id.designationEdt);
        edtmobilenumber=(EditText)rootView.findViewById(R.id.contactnumberEdt);
        edtlandline=(EditText)rootView.findViewById(R.id.landlineEdt);
        edtaddress=(EditText)rootView.findViewById(R.id.addressEdt);
        edtcompanyprofile=(EditText)rootView.findViewById(R.id.companyprofileEdt);
        dateOfBirth=(EditText)rootView.findViewById(R.id.dateOfBirth);
        languages=(MultiAutoCompleteTextView)rootView.findViewById(R.id.languages);
        languages.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        companylogoimgv=(ImageView)rootView.findViewById(R.id.companylogoimgv);
        eye_image=(ImageView)rootView.findViewById(R.id.eye_image);
        btncompanylogo=(Button)rootView.findViewById(R.id.companylEdt);

        btnSubmit=(Button)rootView.findViewById(R.id.submitbtn);
        cancilbtnbtn=(Button)rootView.findViewById(R.id.cancilbtnbtn);
        emailChk = (CheckBox)rootView.findViewById(R.id.ac_email);
        SMSview = (CheckBox)rootView.findViewById(R.id.ac_sms);
        Chatview = (CheckBox)rootView.findViewById(R.id.ac_chat);
        noneView = (CheckBox)rootView.findViewById(R.id.ac_none);
        chk_agree = (CheckBox)rootView.findViewById(R.id.chk_agree);

        dailyStatus = (LinearLayout) rootView.findViewById(R.id.dailyStatus);
        selectTime = (LinearLayout) rootView.findViewById(R.id.selectTime);
        timeStatus = (LinearLayout) rootView.findViewById(R.id.timeStatus);
        profileAlerts_lay = (LinearLayout) rootView.findViewById(R.id.profileAlerts_lay);
        radioDaily = (RadioButton) rootView.findViewById(R.id.radioDaily);
        radioWeekly = (RadioButton) rootView.findViewById(R.id.radioWeekly);
        radioInstant = (RadioButton) rootView.findViewById(R.id.radioInstant);
        radioTimeStatus  = (RadioGroup)rootView.findViewById(R.id.radioTimeStatus);
        radioStatus  = (RadioGroup)rootView.findViewById(R.id.radioStatus);
        radioDaily.setOnClickListener(operationListener);
        radioWeekly.setOnClickListener(operationListener);
        radioInstant.setOnClickListener(operationListener);
        eye_image.setOnClickListener(operationListener);
        daysSpin = (Spinner) rootView.findViewById(R.id.daytimeSpin);
        ex_pwd_text=(TextView) rootView.findViewById(R.id.ex_pwd_txt);
        c_pw_lyt=(LinearLayout) rootView.findViewById(R.id.c_pwd_lyt);
        pwd_lyt=(LinearLayout) rootView.findViewById(R.id.pwd_txt_lyt);


        agree_txt = (TextView)rootView.findViewById(R.id.agree_txt);
        String styledText = "I have read, understood and agree to the<font color='#008ACE'><a href='http://localjobserver.com/TermsAndConditions.aspx'>Terms and Conditions</a></font>";

        agree_txt.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
        agree_txt.setMovementMethod(LinkMovementMethod.getInstance());

        if (preferences.getInt(CommonKeys.LJS_PREF_USERTYPE, 0) == 1){
//            btnSubmit.setText("Update");
//            chk_agree.setVisibility(View.GONE);
//            agree_txt.setVisibility(View.GONE);
//            profileAlerts_lay.setVisibility(View.GONE);
            {
                try {
                    char CR = '\n';
                    seekerImageData = Base64.decode(CommonUtils.replaceNewLines(preferences.getString(CommonUtils.getUserEmail(getActivity()), null)), Base64.DEFAULT);
                    BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));
                    companylogoimgv.setImageDrawable(dr);
                    companylogoimgv.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }else {
        }

        industry_list=new ArrayList<String>();
        String[] userType=  getResources().getStringArray(R.array.userType);
        String[] industrytype=  getResources().getStringArray(R.array.industrytype);
        Bundle bundle = this.getArguments();
        flag = bundle.getInt("flag");
        if(flag==1){
            btnSubmit.setText("Update");
            ex_pwd_text.setVisibility(View.GONE);
            c_pw_lyt.setVisibility(View.GONE);
            pwd_lyt.setVisibility(View.GONE);
            chk_agree.setVisibility(View.GONE);
            agree_txt.setVisibility(View.GONE);
            eye_image.setVisibility(View.GONE);
            profileAlerts_lay.setVisibility(View.GONE);
            edtemail.setEnabled(false);
            getRecruiterData();
            industrytypeSpnr.setAdapter(new CustomSpinnerAdapter(getActivity(), R.layout.spinner_row, industrytype, ""+arrRecLisr.get(0).getIndustryType()));
        }
        else{
            btnSubmit.setText("Submit ");
            industrytypeSpnr.setAdapter(new CustomSpinnerAdapter(getActivity(), R.layout.spinner_row, industrytype, industrytype[0]));
        }
        //getUserIndustryType();


        daysSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));

        daysSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDay = parent.getSelectedItem().toString();
                if (parent.getSelectedItemId() != 0)
                    timeStatus.setVisibility(View.VISIBLE);
                else
                    timeStatus.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dateOfBirth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        emailChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                noneView.setChecked(false);
                if (isChecked) {
                    dailyStatus.setVisibility(View.VISIBLE);
                    timeStatus.setVisibility(View.GONE);
                } else {
                    dailyStatus.setVisibility(View.GONE);
                    selectTime.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.GONE);
                }
            }
        });

        noneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailChk.setChecked(false);
                SMSview.setChecked(false);
                Chatview.setChecked(false);
                timeStatus.setVisibility(View.GONE);
                dailyStatus.setVisibility(View.GONE);
                selectTime.setVisibility(View.GONE);
                if (noneView.isChecked())
                    noneView.setChecked(false);
                else
                    noneView.setChecked(true);
            }
        });

        SMSview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                noneView.setChecked(false);
            }
        });
        Chatview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                noneView.setChecked(false);
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnSubmit.getText().toString().equalsIgnoreCase("Update")) {
//                        uploadUserImage();
                    setRecuiterDatatoModel("Update");

                } else {
                    if (validaterecruterRegister() == true && validateMobileNo() == true && spinnerValidations() == true && CommonUtils.passwordValidate(edtpassword.getText().toString(),getActivity())){
                        if (validatePassword().equals("")){
                            if (take_photo){
                                if (chk_agree.isChecked()) {
                                    setRecuiterDatatoModel("register");
                                    uploadUsersData(toUpload());

                                } else {

                                    Toast.makeText(getActivity(), "Please agree terms & Conditions", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(getActivity(), "Select company Logo", Toast.LENGTH_SHORT).show();
                            }

                        }else {
                            Toast.makeText(getActivity(), "Check password Confirm password", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });

        cancilbtnbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();

            }
        });
        btncompanylogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilePic();
            }
        });

        bindDataToUI();
        return rootView;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    public boolean validaterecruterRegister(){
        Form mForm = new Form(getActivity());
            mForm.addField(Field.using(edtfirstname).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtlastname).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtcompanyname).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtemail).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using(edtcompanyurl).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtpassword).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtconfirmpwd).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using(edtdesignation).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(edtmobilenumber).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using(edtlandline).validate(NotEmpty.build(getActivity())));
            if (CommonUtils.spinnerSelect("Industry Type",industrytypeSpnr.getSelectedItemPosition(),getActivity())){
                mForm.addField(Field.using(edtaddress).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(edtcompanyprofile).validate(NotEmpty.build(getActivity())));
            }

        return (mForm.isValid()) ? true : false;
    }

    public  boolean spinnerValidations(){
        if ( !CommonUtils.spinnerSelect("Industry Type",industrytypeSpnr.getSelectedItemPosition(),getActivity())
                ){
            return false;
        }

        return true;
    }

    public boolean validateMobileNo(){

        if (CommonUtils.isValidMobile(edtmobilenumber.getText().toString(),edtmobilenumber))
            return true;
        else{
            CommonUtils.showToast("Enter Valid Mobile No",getActivity());
            return  false;
        }


    }


    public String validatePassword(){

        if (!(edtpassword.getText().toString().toLowerCase().equalsIgnoreCase(edtconfirmpwd.getText().toString().toLowerCase()))){
            return "Password and confirm password is need to be same";
        }else {
            return "";
        }

    }


    View.OnClickListener operationListener = new View.OnClickListener() {

        public void onClick(View v) {
            // TODO Auto-generated method stub

            switch (v.getId()) {

                case R.id.eye_image:
                    if (eye_var == false) {
                        eye_var = true;
                        eye_image.setImageResource(R.drawable.eye_hidden);
                        edtpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        edtconfirmpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        edtpassword.setSelection(edtpassword.getText().length());
                        edtconfirmpwd.setSelection(edtconfirmpwd.getText().length());
                    }else {
                        eye_var = false;
                        eye_image.setImageResource(R.drawable.eye_show);
                        edtpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        edtconfirmpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        edtpassword.setSelection(edtpassword.getText().length());
                        edtconfirmpwd.setSelection(edtconfirmpwd.getText().length());
                    }

                    break;

                case R.id.SMSview:

                    break;
                case R.id.Chatview:

                    break;
                case R.id.preview:

                    break;
                case R.id.ljscommunication:

                    break;
                case R.id.tr_email:

                    break;
                case R.id.TrSMSview:

                    break;
                case R.id.radioDaily:
                    selectTime.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.VISIBLE);
                    break;
                case R.id.TrChatview:

                    break;
                case R.id.radioWeekly:
                    selectTime.setVisibility(View.VISIBLE);
                    timeStatus.setVisibility(View.VISIBLE);

                    break;
                case R.id.radioInstant:
                    selectTime.setVisibility(View.GONE);
                    timeStatus.setVisibility(View.GONE);
                    break;

            }
        }
    };


    public void uploadPrivacySettings() {

//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Please wait...");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
//                    progressDialog.dismiss();
                    uploadUserImage();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.recruiterPrivacySettings, edtemail.getText().toString().trim(), selectedDay, selectedStr, emailChk.isChecked(), SMSview.isChecked(), Chatview.isChecked()));



    }

    private void getrecruiterconditions(){
        if (emailChk.isChecked()){
            int selectedId = radioTimeStatus.getCheckedRadioButtonId();
            int selectedId_status = radioStatus.getCheckedRadioButtonId();
            selectedStr = "";
            RadioButton timebutton = (RadioButton)rootView.findViewById(selectedId);
            RadioButton timebutton_status = (RadioButton)rootView.findViewById(selectedId_status);

            if (null != timebutton){
                selectedStr = timebutton.getText().toString();
                com.localjobserver.networkutils.Log.i("", "time is : " + selectedStr);
            }else {
                selectedStr = "";
            }


            if (selectTime.getVisibility() == View.VISIBLE){

            }else{
                selectedDay = timebutton_status.getText().toString();
            }


        }else {

        }

        uploadPrivacySettings();


    }


    public void getUserIndustryType()
    {
        industrytypeSpnr.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.industrytype)));
        industrytypeSpnr.setOnItemSelectedListener(spinListener);
    }


    public void bindDataToUI() {
        getCompanies();
        getIndustries();
        getDisegnation();
    }

    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public void getDisegnation(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    disigntionMap = (LinkedHashMap<String,String>)result;
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISIGNATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getDesignations),CommonKeys.arrDesignations);
    }

    public boolean validateProfessionalDetails(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using(edtfirstname).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtlastname).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtcompanyname).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtemail).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using(edtcompanyurl).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtpassword).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtconfirmpwd).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using(edtdesignation).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtmobilenumber).validate(NotEmpty.build(getActivity())).validate(IsPositiveInteger.build(getActivity())));
        mForm.addField(Field.using(edtlandline).validate(NotEmpty.build(getActivity())).validate(IsPositiveInteger.build(getActivity())));
        mForm.addField(Field.using(edtaddress).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(edtcompanyprofile).validate(NotEmpty.build(getActivity())));
        return (mForm.isValid()) ? true : false;
    }



    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){

                case R.id.industrytypeEdt:
                    recuiterObject.setIndustryType(industrytypeSpnr.getSelectedItem().toString());
                    break;

            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public void setRecuiterDatatoModel(String type ){
        recuiterObject.setRecruterId("");
        recuiterObject.setFirstName(edtfirstname.getText().toString());
        recuiterObject.setLastName(edtlastname.getText().toString());
        recuiterObject.setPassword(edtpassword.getText().toString());
        recuiterObject.setConfirmPassword(edtconfirmpwd.getText().toString());
        recuiterObject.setCompanyName(edtcompanyname.getText().toString());
        recuiterObject.setStdCurCompany("");
        recuiterObject.setEmail(edtemail.getText().toString());
        recuiterObject.setCompanyUrl(edtcompanyurl.getText().toString());
        recuiterObject.setContactNo(edtmobilenumber.getText().toString());
        recuiterObject.setContactNoLandline(edtlandline.getText().toString());
        recuiterObject.setIndustryType(industrytypeSpnr.getSelectedItem().toString());
        recuiterObject.setLocation("");
        recuiterObject.setAddress(edtaddress.getText().toString());
        recuiterObject.setCompanyProfile(edtcompanyprofile.getText().toString());
        recuiterObject.setKeySkills("");
        recuiterObject.setActivation("");
        recuiterObject.setRegDate(CommonUtils.getDateTime());
        recuiterObject.setEmailVerified(false);
        recuiterObject.setISOnline(false);
        recuiterObject.setDesignation(edtdesignation.getText().toString());
        recuiterObject.setVisibility(false);
        recuiterObject.setUpdateDate(CommonUtils.getDateTime());
        recuiterObject.setDeviceID(gcmId);
        recuiterObject.setLogoString(Base64.encodeToString(seekerImageData, Base64.DEFAULT));
        recuiterObject.setIsMobileOnline(false);
        recuiterObject.setLastLogin(CommonUtils.getDateTime());
        recuiterObject.setLastActive(CommonUtils.getDateTime());

        if (type.equalsIgnoreCase("Update"))
            toRecrutorUpdateDerailsUpload();


    }
    public void toRecrutorUpdateDerailsUpload(){
        morderHashMapList = new ArrayList<HashMap>();

        LinkedHashMap RecuiterRegDataMap = new LinkedHashMap();
//        RecuiterRegDataMap.put("RecruterId",recuiterObject.getRecruterId());
        RecuiterRegDataMap.put("FirstName",recuiterObject.getFirstName());
        RecuiterRegDataMap.put("LastName",recuiterObject.getLastName());
        RecuiterRegDataMap.put("Password",recuiterObject.getPassword());
        RecuiterRegDataMap.put("ConfirmPassword",recuiterObject.getConfirmPassword());
        RecuiterRegDataMap.put("CompanyName",recuiterObject.getCompanyName());
//        RecuiterRegDataMap.put("StdCurCompany",recuiterObject.getStdCurCompany());
        RecuiterRegDataMap.put("Email",recuiterObject.getEmail());
        RecuiterRegDataMap.put("CompanyUrl",recuiterObject.getCompanyUrl());
        RecuiterRegDataMap.put("ContactNo",recuiterObject.getContactNo());
        RecuiterRegDataMap.put("ContactNo_Landline",recuiterObject.getContactNoLandline());
        RecuiterRegDataMap.put("EmployType","Employee(referral)");
        RecuiterRegDataMap.put("IndustryType",recuiterObject.getIndustryType());
        RecuiterRegDataMap.put("Location",recuiterObject.getLocation());
        RecuiterRegDataMap.put("Address", recuiterObject.getAddress());
        RecuiterRegDataMap.put("CompanyProfile", recuiterObject.getCompanyProfile());
        RecuiterRegDataMap.put("KeySkills", recuiterObject.getKeySkills());
        RecuiterRegDataMap.put("Activation", "true");
        RecuiterRegDataMap.put("RegDate", recuiterObject.getRegDate());
        RecuiterRegDataMap.put("EmailVerified", "true");
        RecuiterRegDataMap.put("IsOnline", "true");
        RecuiterRegDataMap.put("Designation", recuiterObject.getDesignation());
        RecuiterRegDataMap.put("CompanyLogo","");
        RecuiterRegDataMap.put("ProfileAlerts","");
//        RecuiterRegDataMap.put("Visibility", "true");
//        RecuiterRegDataMap.put("UpdateDate", recuiterObject.getUpdateDate());
//        RecuiterRegDataMap.put("DeviceID", recuiterObject.getDeviceID());
//        RecuiterRegDataMap.put("LogoString","");
//        RecuiterRegDataMap.put("IsMobileOnline", "true");
//        RecuiterRegDataMap.put("LastLogin", recuiterObject.getLastLogin());
//        RecuiterRegDataMap.put("LastActive", recuiterObject.getLastActive());

        morderHashMapList.add(RecuiterRegDataMap);

       /* String whereCondition=" where Email='"+CommonUtils.getUserEmail(getActivity())+"'";
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        dbHelper.updateData("RecuiterRegistration", morderHashMapList, whereCondition, getActivity());*/

        updateJobRecrutorProfile(RecuiterRegDataMap);
    }

    private void updateJobRecrutorProfile(final LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    // if (result != null) {
                    Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                    //  }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        String whereCondition = " where Email='" + CommonUtils.getUserEmail(getActivity()) + "'";
                        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
                        dbHelper.updateData("RecuiterRegistration", morderHashMapList, whereCondition, getActivity());

                        if (take_photo)
                            uploadUserImage();
                        else
                            designSuccessDialog();
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyRecDetails, CommonUtils.getUserEmail(getActivity()), "" + requestString));
    }

    public void profilePic(){
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")){
                    //selectImage();
                    // Calls an Intent that opens camera to take a picture
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, FROMCAM);
                }
                else if (options[item].equals("Choose from Gallery")){
                    //Call an intent that opens gallery to select photo
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_PICTURE);

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    public void insertRecuiterRegData(){
        if(seekerImageData != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(""+edtemail.getText().toString(), Base64.encodeToString(seekerImageData, Base64.DEFAULT));
            editor.commit();
        }
//        DatabaseAccessObject.insertRecuiterRegData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
//            public void run() {
//                if (success == false) {
//                    return;
//                }
//                if (result != null) {
//                    designSuccessDialog();
//                }
//            }
//        }, recuiterObject, getActivity());
    }



    public HashMap toUpload(){
        HashMap RecuiterRegDataMap = new HashMap();
        RecuiterRegDataMap.put("RecruterId",recuiterObject.getRecruterId());
        RecuiterRegDataMap.put("FirstName",recuiterObject.getFirstName());
        RecuiterRegDataMap.put("LastName",recuiterObject.getLastName());
        RecuiterRegDataMap.put("Password",recuiterObject.getPassword());
        RecuiterRegDataMap.put("ConfirmPassword",recuiterObject.getConfirmPassword());
        RecuiterRegDataMap.put("CompanyName",recuiterObject.getCompanyName());
        RecuiterRegDataMap.put("StdCurCompany",recuiterObject.getStdCurCompany());
        RecuiterRegDataMap.put("Email",recuiterObject.getEmail());
        RecuiterRegDataMap.put("CompanyUrl",recuiterObject.getCompanyUrl());
        RecuiterRegDataMap.put("ContactNo",recuiterObject.getContactNo());
        RecuiterRegDataMap.put("ContactNo_Landline",recuiterObject.getContactNoLandline());
        RecuiterRegDataMap.put("EmployType",recuiterObject.getEmployType());
        RecuiterRegDataMap.put("IndustryType",recuiterObject.getIndustryType());
        RecuiterRegDataMap.put("Location",recuiterObject.getLocation());
        RecuiterRegDataMap.put("Address",recuiterObject.getAddress());
        RecuiterRegDataMap.put("CompanyProfile",recuiterObject.getCompanyProfile());
        RecuiterRegDataMap.put("KeySkills",recuiterObject.getKeySkills());
        RecuiterRegDataMap.put("Activation","false");
        RecuiterRegDataMap.put("RegDate",recuiterObject.getRegDate());
        RecuiterRegDataMap.put("EmailVerified","false");
        RecuiterRegDataMap.put("IsOnline","false");
        RecuiterRegDataMap.put("Designation",recuiterObject.getDesignation());
        RecuiterRegDataMap.put("CompanyLogo","");
        RecuiterRegDataMap.put("Visibility","false");
        RecuiterRegDataMap.put("UpdateDate",recuiterObject.getUpdateDate());
        RecuiterRegDataMap.put("DeviceID",recuiterObject.getDeviceID());
        RecuiterRegDataMap.put("LogoString","");
        RecuiterRegDataMap.put("IsMobileOnline","false");
        RecuiterRegDataMap.put("LastLogin",recuiterObject.getLastLogin());
        RecuiterRegDataMap.put("LastActive",recuiterObject.getLastActive());
        return RecuiterRegDataMap;
    }

    public void getCompanies(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    companiesMap = (LinkedHashMap<String,String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_COMPANIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what){
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occured while getting data from  server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null){
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_COMPANIES:
                    if (null != companiesMap && isClassVisible == true) {
//                        if (RecruiterRegistrationActivity.back_var == true){
                        edtcompanyname.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(companiesMap, "Company")));
//                    }
                    }
                    break;
                case RESULT_DISIGNATIONS:
                    if (null != disigntionMap && isClassVisible == true) {
                        edtdesignation.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(disigntionMap, "Designation")));
                    }
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries && isClassVisible == true) {
                        industrytypeSpnr.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(industries, "Industry")));
                        if(flag==1){
                            ArrayList<String> arList = new ArrayList<String>();
                            for(Map.Entry<String,String> map : industries.entrySet()){
                                arList.add(map.getValue());
                            }
                            String [] Industries_Array = arList.toArray(new String[arList.size()]);
                            industrytypeSpnr.setAdapter(new CustomSpinnerAdapter(getActivity(), R.layout.spinner_row, Industries_Array, ""+arrRecLisr.get(0).getIndustryType()));
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        take_photo = true;
        if ((requestCode == 0) && (resultCode == -1)) {

        }else if (requestCode == FROMCAM) {
            if (null == data)
                return;
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inSampleSize = 2;
            bitmapOptions.inPurgeable = true;
            Bitmap yourSelectedImage = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(CommonUtils.getImageOrientation(f.getAbsolutePath()));
            Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(),yourSelectedImage.getHeight(), matrix, true);
            Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
            companylogoimgv.setImageDrawable(dr);
            companylogoimgv.invalidate();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60,outputStream);
            seekerImageData = outputStream.toByteArray();
            recuiterObject.setCompanyLogo(seekerImageData);

        } else if (requestCode == SELECT_PICTURE){
            if (null == data)
                return;
            Uri uri = data.getData();
            String[] projection = { MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            options.inPurgeable = true;
            Bitmap rotatedBitmap = ShrinkBitmap(filePath, 80, 80);
            Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90,outputStream);
            seekerImageData = outputStream.toByteArray();
            companylogoimgv.setImageDrawable(dr);
            companylogoimgv.invalidate();
            recuiterObject.setCompanyLogo(seekerImageData);
        }// for if
    }

    public void uploadUsersData(HashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    getrecruiterconditions();
                    if (result != null) {
                    }
                    return;
                }
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        getrecruiterconditions();

                    }
//                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.registerRecruiter, "" + requestString));
    }


    Dialog dialog;
    public void designSuccessDialog(){
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_dialog);
        dialog.setCanceledOnTouchOutside(true);
        TextView msgView = (TextView) dialog.findViewById(R.id.msgTxt);
        TextView titleHeader = (TextView) dialog.findViewById(R.id.titleHeader);
        titleHeader.setText("Confirmation");
        if (btnSubmit.getText().toString().equalsIgnoreCase("Update"))
            msgView.setText("Profile Update Successfully");
        else
            msgView.setText(getResources().getString(R.string.registrationSuccessDialog));
        dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                if (btnSubmit.getText().toString().equalsIgnoreCase("Update")) {
                    startActivity(new Intent(getActivity(), MainInfoActivity.class));
                }else {
                    startActivity(new Intent(getActivity(), LoginScreen.class));
                }

                getActivity().finish();
            }
        });

        dialog.show();
    }


    public void  uploadUserImage(){

//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Uploading user data..");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
        LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                Log.i("", "Result is : " + result.toString());
                if (result != null && result.toString().contains("true")) {
                    progressDialog.dismiss();

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                    editor.commit();
                    designSuccessDialog();
                }
            }
        }, "" + edtemail.getText().toString() + ".png", edtemail.getText().toString(), compressedStr,"recruiterUploadImage", getActivity(), 0);

    }

    @Override
    public void onFinishYesNoDialog(boolean state) {
        // -- Finish dialog box show msg
//        Toast.makeText(this, "Which Option Selected: " + state,Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), MainInfoActivity.class));
    }

    public  void  saveUserInfo(){
        SharedPreferences.Editor editor = preferences.edit();
        CommonUtils.isLogin = true;
        CommonUtils.isUserExisted = true;
        Cursor dataCur = DatabaseAccessObject.getuserInfo(Queries.getInstance().checkExistance("RecuiterRegistration"),getActivity());
        if (dataCur != null && dataCur.moveToFirst()) {
            String email = dataCur.getString(dataCur.getColumnIndex("Email"));
            CommonUtils.userEmail =email;
            editor.putString(CommonKeys.LJS_PREF_EMAILID, email);
            editor.putString(CommonKeys.LJS_PREF_PASSWORD, dataCur.getString(dataCur.getColumnIndex("Password")));
            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 1);
            editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
            editor.commit();
        }
    }

    private void getRecruiterData(){
        arrRecLisr= DatabaseAccessObject.getRecuiterRegistrationDetails(Queries.getInstance().getRecuiterRegDetails(), getActivity());
        edtfirstname.setText(""+arrRecLisr.get(0).getFirstName());
        edtlastname.setText(""+arrRecLisr.get(0).getLastName());
        edtcompanyname.setText(""+arrRecLisr.get(0).getCompanyName());
        edtemail.setText("" + arrRecLisr.get(0).getEmail());
        edtmobilenumber.setText("" + arrRecLisr.get(0).getContactNo());
        edtcompanyurl.setText(""+arrRecLisr.get(0).getCompanyUrl());
        edtlandline.setText("" + arrRecLisr.get(0).getContactNoLandline());
        edtaddress .setText("" + arrRecLisr.get(0).getAddress());
        edtpassword.setText("" + arrRecLisr.get(0).getPassword());
        edtconfirmpwd.setText("" + arrRecLisr.get(0).getPassword());
        edtdesignation.setText(""+arrRecLisr.get(0).getDesignation());
        edtcompanyprofile.setText("" + arrRecLisr.get(0).getCompanyProfile());

        byte[]  byteArray=arrRecLisr.get(0).getCompanyLogo();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        if(bmp!=null){
            companylogoimgv.setImageBitmap(bmp);
        }
    }
    class CustomSpinnerAdapter extends ArrayAdapter<String> {

        Context context;
        String[] objects;
        String firstElement;
        boolean isFirstTime;

        public CustomSpinnerAdapter(Context context, int textViewResourceId, String[] objects, String defaultText) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.objects = objects;
            this.isFirstTime = true;
            setDefaultText(defaultText);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            if(isFirstTime) {
                objects[0] = firstElement;
                isFirstTime = false;
            }
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            notifyDataSetChanged();
            return getCustomView(position, convertView, parent);
        }

        public void setDefaultText(String defaultText) {
            this.firstElement = objects[0];
            objects[0] = defaultText;
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label = (TextView) row.findViewById(R.id.textView);
            label.setText(objects[position]);
            return row;

        }

    }


    Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }

}
