package com.localjobserver.trainingsNfreshers;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.freshers.FresshersProfileViewActivity;
import com.localjobserver.helper.CircleImageView;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class FnTMenuFragment extends android.support.v4.app.Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned_first";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView, rootView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    private String[] itemsList = null;
    private CircleImageView profilePic;
    private TextView profileName;
    private ProgressDialog progressDialog;
    private static final int RESULT_IMAGE = 1;
    private static final int RESULT_ERROR = 0;
    private String imageFromServer;
    private String profile_pic = "";
    private byte[] seekerImageData = null;
    private SharedPreferences appPrefs = null;
    private int userType = 0;

    public FnTMenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            rootView = (View) inflater.inflate(R.layout.fragment_first_menu, container, false);
            mDrawerListView = (ListView)rootView.findViewById(R.id.navList);
            mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectItem(position);
                }
            });

        appPrefs =  getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        profile_pic = appPrefs.getString(CommonUtils.getUserEmail(getActivity()), null);


        RelativeLayout profileLayout = (RelativeLayout)rootView.findViewById(R.id.profile_parent_logout);
        profilePic = (CircleImageView)rootView.findViewById(R.id.iv_profile_no_pic);
        profileName = (TextView)rootView.findViewById(R.id.tv_desc);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("updateProfilePick_Fresher"));


        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), FresshersProfileViewActivity.class));
            }
        });

        if (CommonUtils.isFresherLoggedIn(getActivity())){
            profileLayout.setVisibility(View.VISIBLE);
            updateProfileData();
            if (userType == 2){
                itemsList =  getActivity().getResources().getStringArray(R.array.naviItems_freshers);
            }else{
                itemsList =  getActivity().getResources().getStringArray(R.array.naviItems_recruiter_login);
            }

        }else{
            itemsList =  getActivity().getResources().getStringArray(R.array.naviItems_freshers_login);
            profileLayout.setVisibility(View.GONE);
        }



            mDrawerListView.setAdapter(new ArrayAdapter<String>(
                    getActionBar().getThemedContext(),
                    android.R.layout.simple_list_item_activated_1,
                    android.R.id.text1,
                    itemsList
            ));
            mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);

        if (profile_pic == null){
            getProfilePick();
        } else {
            try {
                char CR = '\n';
                seekerImageData = Base64.decode(CommonUtils.replaceNewLines(profile_pic), Base64.DEFAULT);
                BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                        (seekerImageData, 0, seekerImageData.length));

                if ( seekerImageData.length != 0)
                    profilePic.setImageDrawable(dr);
                else
                    profilePic.setImageResource(R.drawable.app_icon);

                profilePic.invalidate();
            } catch (Exception e) {
                profilePic.setImageResource(R.drawable.app_icon);
                e.printStackTrace();
            }


        }


        if (userType == 2){
            profileName.setText("" + DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("FirstName", "FreshersRegistration"), getActivity()));
        }else{
            profileName.setText("" + DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyName", "FreshersRegistration"), getActivity()));
        }

        return rootView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }
//                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

    public void  updateProfileData(){

        try {
            if(profile_pic !=null) {
                seekerImageData = Base64.decode(profile_pic, Base64.DEFAULT);
                BitmapDrawable dr = new BitmapDrawable(this.getResources(), BitmapFactory.decodeByteArray
                        (seekerImageData, 0, seekerImageData.length));
                profilePic.setImageDrawable(dr);
                profilePic.invalidate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (userType == 0){
            profileName.setText(""+ DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("FirstName","FreshersRegistration"),getActivity()));
        }else{
            profileName.setText(""+ DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyName","RecuiterRegistration"),getActivity()));
        }
    }


    public void getProfilePick(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    profilePic.setImageResource(R.drawable.app_icon);
                    return;
                }




                if (result != null) {
                    imageFromServer = (String) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_IMAGE;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getFresherProfilePic, CommonUtils.getUserEmail(getActivity())));
    }


    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_IMAGE:
                    SharedPreferences.Editor editor = appPrefs.edit();
                    if (imageFromServer == null  || imageFromServer.toString().equalsIgnoreCase("default")) {
                        editor.putString(CommonUtils.getUserEmail(getActivity()), null);
                        editor.commit();
                        profilePic.setImageResource(R.drawable.app_icon);
                        return;

                    }
                    if(imageFromServer != null ) {
                        try {
                            editor.putString(CommonUtils.getUserEmail(getActivity()), CommonUtils.replaceNewLines(imageFromServer));
                            editor.commit();
                            seekerImageData = Base64.decode(CommonUtils.replaceNewLines(imageFromServer), Base64.DEFAULT);
                            BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                                    (seekerImageData, 0, seekerImageData.length));
                            profilePic.setImageDrawable(dr);
                            profilePic.invalidate();
                        } catch (Exception e) {

                        }

                    }
                    break;
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("updateProfilePick_Fresher")) {
                    if (intent.getStringExtra("imageString") != null) {
                        if (intent.getStringExtra("imageString").equalsIgnoreCase("delete")){
                            profilePic.setImageResource(R.drawable.app_icon);
                        }else {
                            seekerImageData = Base64.decode(intent.getStringExtra("imageString"), Base64.DEFAULT);
                            BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                                    (seekerImageData, 0, seekerImageData.length));
                            profilePic.setImageDrawable(dr);
                        }

                        profilePic.invalidate();
                    }else  if (intent.getStringExtra("ProfileName") != null) {
                        profileName.setText(""+intent.getStringExtra("ProfileName"));
                    }
                }
            }
        }
    };
}

