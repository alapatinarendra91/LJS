package com.localjobserver.trainingsNfreshers;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.freshers.CoursesList;
import com.localjobserver.freshers.ELearningsScreen;
import com.localjobserver.freshers.FresharsJobsInfoActivity;
import com.localjobserver.freshers.SimpleCourceSearchActivity;
import com.localjobserver.freshers.TopInstitutionsScreen;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.setup.FreshersRegistration;
import com.localjobserver.ui.AboutUsActivity;
import com.localjobserver.ui.FeedbackActivity;
import com.localjobserver.ui.SettingsActivity;
import co.talentzing.R;

public class FnTNavigationActivity extends ActionBarActivity implements FnTMenuFragment.NavigationDrawerCallbacks {

    private FnTMenuFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private SharedPreferences appPrefs = null;
    private SharedPreferences gcmPrefs = null;
    private int userType = 0;
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor_gcmPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_navigation);

        mNavigationDrawerFragment = (FnTMenuFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        appPrefs =  this.getSharedPreferences("ljs_prefs",MODE_PRIVATE);
        gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        editor = appPrefs.edit();
        editor_gcmPrefs = gcmPrefs.edit();
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE,0);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent in = null;
        switch (position) {
            case 0:
                fragmentManager.beginTransaction().replace(R.id.container, SearchCourseFragment.newInstance(position + 1)).commit();

                break;
            case 1:
                if (CommonUtils.isFresherLoggedIn(this)) {
//                    in = new Intent(this, CoursesList.class);
//                    String preparedUrl = String.format(Config.LJS_BASE_URL+Config.getRecommendedCourses, CommonUtils.getUserEmail(this));
//                    in.putExtra("coursesLink", preparedUrl);
//                    startActivity(in);
                    startActivity(new Intent(FnTNavigationActivity.this, FresharsJobsInfoActivity.class));
                } else {
                    mTitle = "Login";
                    restoreActionBar();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(position + 1, "freshers")).commit();
                }
                break;
            case 2:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    in = new Intent(this, CoursesList.class);
                    String preparedUrl = String.format(Config.LJS_BASE_URL+Config.getFresherEnrolledCourses, CommonUtils.getUserEmail(this));
                    in.putExtra("coursesLink", preparedUrl);
                    in.putExtra("actionBarName", "Enrolled Courses");
                    startActivity(in);
                } else  {
                    startActivity(new Intent(this, FreshersRegistration.class));
                }
                break;
            case 3:

                if (CommonUtils.isFresherLoggedIn(this)) {
                    fragmentManager.beginTransaction().replace(R.id.container, TopInstitutionsScreen.newInstance("", "")).commit();
                } else  {
                    Toast.makeText(this, "Please login or register to access this feature",Toast.LENGTH_SHORT).show();
                }

                break;
            case 4:
                if (CommonUtils.isFresherLoggedIn(this)) {
//                    fragmentManager.beginTransaction().replace(R.id.container, SimpleCourceSearch.newInstance("", "")).commit();
                    startActivity(new Intent(FnTNavigationActivity.this, SimpleCourceSearchActivity.class));
                } else  {
                    Toast.makeText(this, "Please login or register to access this feature",Toast.LENGTH_SHORT).show();
                }

                break;
            case 5:

                if (CommonUtils.isFresherLoggedIn(this)) {
                    fragmentManager.beginTransaction().replace(R.id.container, ELearningsScreen.newInstance("", "")).commit();
                } else  {
                    Toast.makeText(this, "Please login or register to access this feature",Toast.LENGTH_SHORT).show();
                }
                break;

            case 6:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    startActivity(new Intent(FnTNavigationActivity.this, SettingsActivity.class));
                } else  {
                    mTitle = "Login";
                    restoreActionBar();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(position + 1, "freshers")).commit();
                }

                break;

            case 7:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    startActivity(new Intent(FnTNavigationActivity.this, AboutUsActivity.class));
                } else  {
                    startActivity(new Intent(FnTNavigationActivity.this, FeedbackActivity.class));
                }

                break;
            case 8:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    startActivity(new Intent(FnTNavigationActivity.this, FeedbackActivity.class));
                } else  {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://localjobserver.com/PrivacyPolacy.aspx"));
                    startActivity(i);
                }

                break;
            case 9:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    Intent ii = new Intent(Intent.ACTION_VIEW);
                    ii.setData(Uri.parse("https://localjobserver.com/PrivacyPolacy.aspx"));
                    startActivity(ii);
                } else  {
                    Intent ii = new Intent(Intent.ACTION_VIEW);
                    ii.setData(Uri.parse("https://localjobserver.com/FrequentlyAskedQuestions.aspx"));
                    startActivity(ii);
                }
                break;

            case 10:
                if (CommonUtils.isFresherLoggedIn(this)) {
                    Intent ii = new Intent(Intent.ACTION_VIEW);
                    ii.setData(Uri.parse("https://localjobserver.com/FrequentlyAskedQuestions.aspx"));
                    startActivity(ii);
                } else  {
                }

                break;

            case 11:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://ljsjobs.com/TermsAndConditions.aspx"));
                startActivity(i);

                break;

            case 12:
                designLogoutDialog();

                break;

        }
    }


    Dialog logoutdialog;
    public void designLogoutDialog(){
        logoutdialog = new Dialog(FnTNavigationActivity.this);
        logoutdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutdialog.setContentView(R.layout.logout_layout);
        logoutdialog.setCanceledOnTouchOutside(true);
        logoutdialog.findViewById(R.id.yesBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                ApplicationThread.dbPost("Delete", "Delete Record", new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (CommonUtils.getUserLoggedInType(FnTNavigationActivity.this) == 0) {
                            DatabaseAccessObject.deleteRecord("JobSeekers", FnTNavigationActivity.this);
                        } else if (CommonUtils.getUserLoggedInType(FnTNavigationActivity.this) == 1) {
                            DatabaseAccessObject.deleteRecord("RecuiterRegistration", FnTNavigationActivity.this);
                        } else if (userType == 3) {
                            DatabaseAccessObject.deleteRecord("TrainersRegistration", FnTNavigationActivity.this);
                        } else {
                            DatabaseAccessObject.deleteRecord("FreshersRegistration", FnTNavigationActivity.this);
                        }
                        editor.clear();
                        editor_gcmPrefs.clear();
                        editor.commit();
                        editor_gcmPrefs.commit();


                    }
                });

                SharedPreferences.Editor editor = appPrefs.edit();
                editor.putBoolean("isLogin", false);
                editor.putBoolean("isFresherLogin", false);
                editor.putBoolean("isTrainerLogin", false);
                editor.commit();
                startActivity(new Intent(FnTNavigationActivity.this, LoginScreen.class).setAction("fromLogout").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();


            }
        });

        logoutdialog.findViewById(R.id.noBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                logoutdialog.dismiss();
            }
        });
        logoutdialog.show();
    }
    public void onSectionAttached(String title) {
        mTitle = title;
        restoreActionBar();

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_info, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

}
