package com.localjobserver.trainingsNfreshers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.freshers.CoursesList;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.FresharLocalCourses;
import com.localjobserver.models.LocalJobsModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.ui.LocationService;
import com.localjobserver.ui.ResumesListActivity;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchCourseFragment} interface
 * to handle interaction events.
 * Use the {@link SearchCourseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchCourseFragment extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private Spinner jobCateSpin, localtionSpin;
    private View rootView;
    public StringBuffer searchStr;
    private ProgressDialog progressDialog;
    private MultiAutoCompleteTextView autoKeys;
    private static  int SEARCH_RESULT = 0;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_KEYSKILLS = 1;
    private static final int RESULT_INSTITUTION = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_LOCALRESULT = 4;
    private static final int RESULT_DISIGNATIONS = 5;
    private static final int RESULT_IMAGE = 5;
    public ArrayList<String> keySkillsList = null;
    public LinkedHashMap<String,String> institutionMap = null, locationsMap = null,disigntionMap = null;
    private ListView localJobsList;
    public List<LocalJobsModel> arrLocalJobs = null;
    public  String cityNameStr = "", selectedJobType = "";
    private static final String LOG_TAG = SearchCourseFragment.class.getName();
    private LocationService locationService;
    private TextView cityNameTx;
    private LinearLayout localJobsLL;
    private ArrayList<FresharLocalCourses> courcesList;
    private AutoCompleteTextView locationAuto;

    private final ServiceConnection srvConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            locationService = ((LocationService.ServiceBinder)service).getService();
            locationService.getLocation(new ApplicationThread.OnComplete() {
                @Override
                public void execute(boolean success, Object result, String msg) {
                    Log.d(LOG_TAG, "Get location:" + String.valueOf(success) + (result == null ? "/null" : ""));
                    if (success && result != null) {
                        Location location = (Location) result;
                        CommonUtils.getAddressFromLocation(getActivity(), location.getLatitude(), location.getLongitude(), new ApplicationThread.OnComplete() {
                            @Override
                            public void execute(boolean success, Object cityObj, String msg) {
                                if (success) {
                                    if (!TextUtils.isEmpty(cityObj.toString())) {
                                        cityNameTx.setText("" + cityObj.toString());
                                        cityNameStr = cityObj.toString();
                                        if (!CommonUtils.isNetworkAvailable(getActivity())) {
                                            Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                                            return;
                                        }
//                                        getLocalJobs(cityObj.toString());
                                        getLocalCourses(String.format(Config.LJS_BASE_URL + Config.getFreshersLocalCoursesWithCount, cityObj.toString()));
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Not able to find the location.  please check your GPS settings", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            });
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            locationService = null;
        }
    };

    public void getLocalCourses(final String url){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        courcesList = new ArrayList<>();
        LiveData.getfresherLocalCourses(new ApplicationThread.OnComplete<List<FresharLocalCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    courcesList = (ArrayList) result;
                    if (null != courcesList && courcesList.size() > 0) {

                        courcesList = (ArrayList) result;
                        if (result != null) {
                            Log.i("", "Count is : " + result.toString());
                            Message messageToParent = new Message();
                            messageToParent.what = RESULT_LOCALRESULT;

                            Bundle bundleData = new Bundle();
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                            progressDialog.dismiss();
                        }

                    } else {
                        Toast.makeText(getActivity(), "No results found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, url);
    }

    public void getLocalJobs(final String location){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);

        }
        progressDialog.show();
        LiveData.getLocalJobsData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    arrLocalJobs = (ArrayList<LocalJobsModel>) result;
                    Log.i("","Count is : "+arrLocalJobs.size());
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCALRESULT;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocalCourceList, location));
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment SearchJobFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchCourseFragment newInstance(int param1) {
        SearchCourseFragment fragment = new SearchCourseFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchCourseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_search_course, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        jobCateSpin = (Spinner)rootView.findViewById(R.id.institutionTypeSp);

        jobCateSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedJobType = CommonUtils.getKeyFromValue(institutionMap,jobCateSpin.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        localtionSpin = (Spinner)rootView.findViewById(R.id.jobLocSp);
        locationAuto = (AutoCompleteTextView)rootView.findViewById(R.id.locationAuto);

        autoKeys = (MultiAutoCompleteTextView)rootView.findViewById(R.id.autoscoursekeyword);
        localJobsList = (ListView)rootView.findViewById(R.id.localJobsListView);

        localJobsLL = (LinearLayout)rootView.findViewById(R.id.localJobsLL);
        if (CommonUtils.isLoggedIn(getActivity()) && CommonUtils.getUserLoggedInType(getActivity()) != 1){
            localJobsLL.setVisibility(View.VISIBLE);
        }else {
//            localJobsLL.setVisibility(View.GONE);
            localJobsLL.setVisibility(View.VISIBLE);
        }

        localJobsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String rid = arrLocalJobs.get(position).getRId();
//                Bundle searchBundle = new Bundle();
//                searchBundle.putString("rid",rid);
//                searchBundle.putString("location",cityNameStr);
//                Intent jobListIntent = new Intent(getActivity(),CoursesList.class);
//                jobListIntent.putExtras(searchBundle);
//                startActivity(jobListIntent);
                String urlPrepared = String.format(Config.LJS_BASE_URL + Config.simpleCourseSearch, courcesList.get(position).getCourse(), "", "", "", "");
                Intent in = new Intent(getActivity(), CoursesList.class);
                in.putExtra("coursesLink", urlPrepared);
                startActivity(in);

            }
        });
        cityNameTx = (TextView)rootView.findViewById(R.id.cityName);
        autoKeys.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        getKeyWords();
        getInstitutions();
        getLocations();
//        getJobsInfo();
        searchStr  = new StringBuffer();
        CommonUtils.hideKeyPad(getActivity(),autoKeys);
        Button searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStr.append(autoKeys.getText().toString());

                if (!CommonUtils.isNetworkAvailable(getActivity())){
                    Toast.makeText(getActivity(),"Please check your internet connection and try again.",Toast.LENGTH_SHORT).show();
                    return;
                }

                if (autoKeys.getText().toString().length() == 0 && locationAuto.getText().toString().length() == 0) {
                    Toast.makeText(getActivity() , "Please select any search criteria to search." , Toast.LENGTH_SHORT).show();
                }  else {

//                    String preparedUrl = String.format(Config.LJS_BASE_URL+Config.SearchFresherJobsByCourseWithLocation, locationAuto.getText().toString(),CommonUtils.removeLastComma(autoKeys.getText().toString().replace(" ","")));
//                    System.out.print("link for data.."+preparedUrl);
//
//                    Intent in = new Intent(getActivity(), CoursesList.class);
//                    in.putExtra("coursesLink", preparedUrl);
//                    startActivity(in);

                    String urlPrepared = String.format(Config.LJS_BASE_URL + Config.simpleCourseSearch, CommonUtils.removeLastComma(autoKeys.getText().toString().replace(" ", "")), locationAuto.getText().toString(), jobCateSpin.getSelectedItem().toString(), "", "");
                    Intent in = new Intent(getActivity(), CoursesList.class);
                    in.putExtra("coursesLink", urlPrepared);
                    startActivity(in);

                }
            }
        });

        Button searchResumeBtn = (Button)rootView.findViewById(R.id.searchResumeBtn);
        searchResumeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CommonUtils.isNetworkAvailable(getActivity())){
                    Toast.makeText(getActivity(),"Please check your internet connection and try again.",Toast.LENGTH_SHORT).show();
                    return;
                }

                searchStr.append(autoKeys.getText().toString());

                if (null != searchStr && searchStr.toString().length() > 0) {
                    String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.getResumes, searchStr.toString(), selectedJobType);
                    startActivityForResult(new Intent(getActivity(), ResumesListActivity.class).putExtra("searchTxt",searchStr.toString()).putExtra("resumesLink",preparedUrltoGetResumes),SEARCH_RESULT);
                } else {
                    Toast.makeText(getActivity(),"Please enter keywords.",Toast.LENGTH_SHORT).show();
                }

            }
        });


        arrLocalJobs  = new ArrayList<LocalJobsModel>();

        return rootView;

    }


    @Override
    public void onResume(){
        super.onResume();
        Intent intent = new Intent(getActivity(), LocationService.class);
        getActivity().bindService(intent, srvConnection, Context.BIND_AUTO_CREATE);

//        if (CommonUtils.isLoggedIn(getActivity()) && CommonUtils.getUserLoggedInType(getActivity()) != 1){
//            if (isGPSEnabled()){
//                Intent intent = new Intent(getActivity(), LocationService.class);
//                getActivity().bindService(intent, srvConnection, Context.BIND_AUTO_CREATE);
//            }else {
//                showSettingsAlert();
//            }
//        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SEARCH_RESULT) {
        }
    }

    class StatusHandler extends Handler {

        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what){
                case RESULT_ERROR:
                    Toast.makeText(getActivity(),"Error occured while getting data from  server.",Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_KEYSKILLS:
                    if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()){
                        autoKeys.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                    }
                    break;
                case RESULT_INSTITUTION:
                    if (null != institutionMap && institutionMap.size() > 0 && isVisible()){
                        jobCateSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(institutionMap, "Institute/Online Trainer")));
                    }
                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap && locationsMap.size() > 0 && isVisible()){
                        locationAuto.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                    }
                    break;
                case RESULT_LOCALRESULT:
                    LocalListAdpter lJobsAdaper = new LocalListAdpter(getActivity());
                    localJobsList.setAdapter(lJobsAdaper);
                    break;
                case RESULT_DISIGNATIONS:

                    break;
            }
        }
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        });
    }


    public void getInstitutions(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    institutionMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INSTITUTION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getInstitutes), CommonKeys.arrInstitutes);
    }

    public void getDisignation(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    disigntionMap = (LinkedHashMap<String,String>)result;
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISIGNATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getDesignations),CommonKeys.arrDesignations);
    }

    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            if (null != (FnTNavigationActivity) activity) {
                ((FnTNavigationActivity) activity).onSectionAttached("Search Courses");
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    class LocalListAdpter extends BaseAdapter {

        Context context;
        public LocalListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.localjobs_list_row, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.title);
            jobTitle.setText(courcesList.get(position).getCourse());
            TextView jobLoc = (TextView)convertView.findViewById(R.id.count);
            jobLoc.setText(""+courcesList.get(position).getCount());

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return courcesList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return courcesList.get(position);
        }

    }

    public boolean isGPSEnabled(){
        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            return false;
        }else {
            return true;
        }
    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * lauch Settings Options
     * */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS Not Enabled");

        // Setting Dialog Message
        alertDialog
                .setMessage("Please enable the GPS settings to get Local Jobs.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getActivity().startActivity(intent);
                    }
                });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

//    @Override
//    public void onDetach() {
////        super.onDetach();
//    }



}
