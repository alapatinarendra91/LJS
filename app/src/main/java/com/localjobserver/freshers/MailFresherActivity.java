package com.localjobserver.freshers;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;

public class MailFresherActivity extends ActionBarActivity {
    private SharedPreferences preferences;

    private ActionBar actionBar = null;
    public static boolean back_var = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_fresher);
        preferences = getApplicationContext().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (getIntent().getStringExtra("type") != null)
        actionBar.setTitle(""+getIntent().getStringExtra("type"));

        if (getIntent().getStringExtra("type").equalsIgnoreCase("Seekers Email"))
            actionBar.setTitle("Job Email");

        if (getIntent().getStringExtra("type").equalsIgnoreCase("Seekers Sms"))
            actionBar.setTitle("Send Sms");

        Fragment itemFrag = new Fragment_mail_fresher();
        itemFrag.setArguments(getIntent().getBundleExtra("ID"));
        getSupportFragmentManager().beginTransaction().add(R.id.container, itemFrag).commit();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        back_var = false;
        this.finish();


    }
}
