package com.localjobserver.freshers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.TrainingInstitute.TrainerManagePostingActivity;
import com.localjobserver.TrainingInstitute.TrainerPostCourseActivity;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.helper.StickyScrollView;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.trainingsNfreshers.FnTNavigationActivity;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class CourceDescriptionActivity extends ActionBarActivity {

    private ArrayList<RecommendedCourses> courcesData;
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private Intent receiverIntent = null;
    private int selectPos = 0;
    private ActionBar actionBar;
    private RelativeLayout bottomApplyRel,applyRel;
    private Button applyBtn;
    private String jobId = "";
    public static ProgressDialog progressDialog;
    private CourceDescriptionFragment selecedFragment = null ;
    private ImageView rightClick,leftClick;
    private TextView applieSuccessdTxt;
    private Bundle bundle1;
    private int userType;
    private SharedPreferences appPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cource_description);

        initViews();
        setViews();

    }

    private void initViews() {
        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Cources Details");
        actionBar.setCustomView(R.layout.action_title);
        receiverIntent = getIntent();

        bottomApplyRel = (RelativeLayout) findViewById(R.id.bottomApplyRel);
        applyRel = (RelativeLayout) findViewById(R.id.confirmRel);
        applyBtn = (Button) findViewById(R.id.applyBtn);
        rightClick = (ImageView) findViewById(R.id.rightArrwo);
        applieSuccessdTxt = (TextView) findViewById(R.id.applieSuccessdTxt);
        leftClick = (ImageView) findViewById(R.id.leftArrwo);

        selectPos = receiverIntent.getExtras().getInt("selectedPos");

        if (receiverIntent.getAction().equalsIgnoreCase("applied") ) {
            courcesData = CommonUtils.CoursesList_applied;
            bottomApplyRel.setVisibility(View.GONE);
        } else {
            courcesData = CommonUtils.CoursesList;
            Log.i("", "Courses is  : " + courcesData.get(0).get_id());
            bottomApplyRel.setVisibility(View.VISIBLE);
        }



        Log.i("","Courses is  : "+courcesData.size());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(selectPos);
//        mPager.setOffscreenPageLimit(5);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                selectPos = position;
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        selecedFragment = mPagerAdapter.getFragment(selectPos);


    }

        private void setViews() {

            rightClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
            });

            leftClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
            });



            applyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (CommonUtils.isFresherLoggedIn(CourceDescriptionActivity.this)){
                        selecedFragment = mPagerAdapter.getFragment(selectPos);

                        jobId  = String.valueOf(selecedFragment.jobdataObj.get_id());

                        System.out.println("Check...id.." + jobId);
                        getjobapply(jobId);

                    } else{
                        startActivity(new Intent(CourceDescriptionActivity.this, LoginScreen.class));
                    }

                }
            });

    }

    public void deleteJob(final String jobId){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CourceDescriptionActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        selecedFragment = mPagerAdapter.getFragment(selectPos);
        String jobUpId = selecedFragment.jobdataObj.get_id();
        Log.v("", "Check..." + jobUpId);
        deleteCourse(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (success) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Course deleted Successfully", getApplicationContext());
                    startActivity(new Intent(CourceDescriptionActivity.this, TrainerManagePostingActivity.class));

                } else {
                    CommonUtils.showToast("Course delete failed", getApplicationContext());
                }
            }
        }, jobUpId);
    }

    public void getjobapply(final String jobId){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CourceDescriptionActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        enrollCourse(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (success) {
                    progressDialog.dismiss();
                    applieSuccessdTxt.setText("Your course is enrolled successfully");
                    bottomApplyRel.setVisibility(View.GONE);
                    applyRel.setVisibility(View.VISIBLE);
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(2000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                updateHandler.sendEmptyMessageAtTime(0, 100);
                            }
                        }
                    }.start();
                } else {

                }
            }
        }, jobId);
    }

    public  void enrollCourse(final ApplicationThread.OnComplete oncomplete, final  String jobId){
        Log.i("", "Mail id is : " + CommonUtils.getUserEmail(CourceDescriptionActivity.this));
        ApplicationThread.bgndPost(CourceDescriptionActivity.class.getName(), "applyJob...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.EnrollFresher, "" + CommonUtils.getUserEmail(CourceDescriptionActivity.this), jobId), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        oncomplete.execute(true, result, null);
                    }
                });
            }
        });
    }

    public  void deleteCourse(final ApplicationThread.OnComplete oncomplete, final  String courseId){
        Log.i("","Mail id is : "+CommonUtils.getUserEmail(CourceDescriptionActivity.this));
        ApplicationThread.bgndPost(CourceDescriptionActivity.class.getName(), "deletecourse...", new Runnable() {
            @Override
            public void run() {
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.deletePostedCourse, "" + courseId), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        oncomplete.execute(true, result, null);
                    }
                });
            }
        });
    }

    private final Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            final int what = msg.what;
            switch(what) {
                case 0:
                    if(selectPos == courcesData.size()-1){
                        Intent jobIntent = new Intent(CourceDescriptionActivity.this, FnTNavigationActivity.class);
                        startActivity(jobIntent);
                    }else {
                        mPager.setCurrentItem(selectPos+1);
                        applyRel.setVisibility(View.GONE);
                        bottomApplyRel.setVisibility(View.VISIBLE);
                    }

                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mail, menu);

        if (receiverIntent.getAction().equalsIgnoreCase("applied") )
        menu.getItem(0).setVisible(false);
        if (userType == 3){
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(true);
            menu.getItem(3).setVisible(true);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }


        if (id == R.id.action_mail) {
            selecedFragment = mPagerAdapter.getFragment(selectPos);
            jobId  = String.valueOf(selecedFragment.jobdataObj.get_id());
            bundle1 = new Bundle();
            bundle1.putString("ID", jobId);
            bundle1.putString("TYPE", "FRESHER");
            bundle1.putString("title", selecedFragment.jobdataObj.getCourseName());
            startActivity(new Intent(CourceDescriptionActivity.this, MailFresherActivity.class).putExtra("type", "Training Email").putExtra("ID", bundle1));
        }
        if (id == R.id.action_enroll) {

            if (CommonUtils.isFresherLoggedIn(CourceDescriptionActivity.this)){
                selecedFragment = mPagerAdapter.getFragment(selectPos);
                jobId  = String.valueOf(selecedFragment.jobdataObj.get_id());

                System.out.println("Check...id.." + jobId);
                getjobapply(jobId);

            } else{
                startActivity(new Intent(CourceDescriptionActivity.this, LoginScreen.class));
            };
        }

        if (id == R.id.action_edit){
            bundle1 = new Bundle();
            bundle1.putString("POSITION", String.valueOf(selectPos));
            startActivity(new Intent(CourceDescriptionActivity.this, TrainerPostCourseActivity.class).putExtra("POSITION",bundle1));
        }

        if (id == R.id.action_delete){
            deleteJob(jobId);
        }



        return super.onOptionsItemSelected(item);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<WeakReference<CourceDescriptionFragment>> mPageReferenceMap = new SparseArray<WeakReference<CourceDescriptionFragment>>();
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return getFragment(position);
        }

        @Override
        public int getItemPosition(Object object){
            return ScreenSlidePagerAdapter.POSITION_NONE;
        }
        @Override
        public int getCount() {
            return courcesData.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            if(object != null){
                return ((Fragment)object).getView() == view;
            }else{
                return false;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mPageReferenceMap.remove(Integer.valueOf(position));
            super.destroyItem(container, position, object);
        }


        public Object instantiateItem(ViewGroup container, int position) {
            CourceDescriptionFragment fragment = new CourceDescriptionFragment();

            // Attach some data to it that we'll
            // use to populate our fragment layouts
            Bundle args = new Bundle();
            args.putSerializable("data", courcesData.get(position));
            jobId  = String.valueOf(courcesData.get(position).get_id());

            // Set the arguments on the fragment
            // that will be fetched in DemoFragment@onCreateView
            fragment.setArguments(args);
            mPageReferenceMap.put(Integer.valueOf(position), new WeakReference<CourceDescriptionFragment>(fragment));
            return super.instantiateItem(container, position);
        }

        public CourceDescriptionFragment getFragment(int key) {
            WeakReference<CourceDescriptionFragment> weakReference = mPageReferenceMap.get(key);
            if (null != weakReference) {
                return (CourceDescriptionFragment) weakReference.get();
            }
            else {
                return null;
            }
        }
    }

    public static class CourceDescriptionFragment extends Fragment {

        private View rootView;
        public RecommendedCourses jobdataObj;
        private StickyScrollView scroll = null;
        private LinearLayout Course_idEdt_lay;
        private TextView Course_idEdt;
        private TextView Course_NameEdt,Institute_NameEdt,CityEdt,courseDescEdt,areaEdt,locationEdt,Eligibility_Edt,Instructor_NameEdt,emaileEdt,contactNumEdt,ContactNo_LandlineEdt,Start_DateEdt,End_DateEdt,classStartTimeEdt,classendTimeEdt,DurationEdt,Batch_SizeEdt,Course_FeeEdt,DiscountTypeEdt,DiscountValueEdt,GroupCountEdt,GrouptDiscountEdt,demoClass,placementSupport,labTimings,classTimings,enrollStatus,courseStatus,AddressEdt,Contact_PersonEdt,Company_URLEdt,Class_TimingsEdt,Venue_DetailsEdt,Course_Enrolled_StatusEdt,brochureName;
        private CheckBox check_EveryDay,check_Weekend,check_Online,check_Normal_Track,check_Fast_Track,check_Project_Work;
        private String resumeFromServer,sdcard_path,resumeName;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                jobdataObj = (RecommendedCourses)getArguments().getSerializable("data");
            }
        }

        public CourceDescriptionFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_cource_description, container, false);

            initializeUi();
            bindData();

            return rootView;
        }

        private void initializeUi() {

            Course_idEdt= (TextView)rootView.findViewById(R.id.Course_idEdt);
            Course_idEdt_lay= (LinearLayout)rootView.findViewById(R.id.Course_idEdt_lay);
            Course_NameEdt = (TextView)rootView.findViewById(R.id.Course_NameEdt);
            Institute_NameEdt = (TextView)rootView.findViewById(R.id.Institute_NameEdt);
            CityEdt = (TextView)rootView.findViewById(R.id.CityEdt);
            locationEdt = (TextView)rootView.findViewById(R.id.locationEdt);
            areaEdt = (TextView)rootView.findViewById(R.id.areaEdt);
            courseDescEdt = (TextView)rootView.findViewById(R.id.courseDescEdt);
            Eligibility_Edt = (TextView)rootView.findViewById(R.id.Eligibility_Edt);
            Instructor_NameEdt = (TextView)rootView.findViewById(R.id.Instructor_NameEdt);
            emaileEdt = (TextView)rootView.findViewById(R.id.emaileEdt);
            contactNumEdt = (TextView)rootView.findViewById(R.id.contactNumEdt);
            ContactNo_LandlineEdt = (TextView)rootView.findViewById(R.id.ContactNo_LandlineEdt);
            Start_DateEdt = (TextView)rootView.findViewById(R.id.Start_DateEdt);
            End_DateEdt = (TextView)rootView.findViewById(R.id.End_DateEdt);
            classStartTimeEdt = (TextView)rootView.findViewById(R.id.classStartTimeEdt);
            classendTimeEdt = (TextView)rootView.findViewById(R.id.classendTimeEdt);
            DurationEdt = (TextView)rootView.findViewById(R.id.DurationEdt);
            Batch_SizeEdt = (TextView)rootView.findViewById(R.id.Batch_SizeEdt);
            Course_FeeEdt = (TextView)rootView.findViewById(R.id.Course_FeeEdt);
            DiscountTypeEdt = (TextView)rootView.findViewById(R.id.DiscountTypeEdt);
            DiscountValueEdt = (TextView)rootView.findViewById(R.id.DiscountValueEdt);
            GroupCountEdt = (TextView)rootView.findViewById(R.id.GroupCountEdt);
            GrouptDiscountEdt = (TextView)rootView.findViewById(R.id.GrouptDiscountEdt);
            demoClass = (TextView)rootView.findViewById(R.id.demoClass);
            placementSupport = (TextView)rootView.findViewById(R.id.placementSupport);
            labTimings = (TextView)rootView.findViewById(R.id.labTimings);
            classTimings = (TextView)rootView.findViewById(R.id.classTimings);
            enrollStatus = (TextView)rootView.findViewById(R.id.enrollStatus);
            courseStatus = (TextView)rootView.findViewById(R.id.courseStatus);
            AddressEdt = (TextView)rootView.findViewById(R.id.AddressEdt);
            Contact_PersonEdt = (TextView)rootView.findViewById(R.id.Contact_PersonEdt);
            Company_URLEdt = (TextView)rootView.findViewById(R.id.Company_URLEdt);
            Class_TimingsEdt = (TextView)rootView.findViewById(R.id.Class_TimingsEdt);
            Venue_DetailsEdt = (TextView)rootView.findViewById(R.id.Venue_DetailsEdt);
            Course_Enrolled_StatusEdt = (TextView)rootView.findViewById(R.id.Course_Enrolled_StatusEdt);
            brochureName = (TextView)rootView.findViewById(R.id.brochureName);

            check_EveryDay = (CheckBox)rootView.findViewById(R.id.check_EveryDay);
            check_Weekend = (CheckBox)rootView.findViewById(R.id.check_Weekend);
            check_Online = (CheckBox)rootView.findViewById(R.id.check_Online);
            check_Normal_Track = (CheckBox)rootView.findViewById(R.id.check_Normal_Track);
            check_Fast_Track = (CheckBox)rootView.findViewById(R.id.check_Fast_Track);
            check_Project_Work = (CheckBox)rootView.findViewById(R.id.check_Project_Work);

            brochureName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String path = Environment.getExternalStorageDirectory() + "/LJS/Brochure" + "/" + jobdataObj.get_id() + ".pdf";
                    File f = new File(path);
                    if (CommonUtils.isBrochureExisted(jobdataObj.get_id())) {
                        resumeName = jobdataObj.get_id() + ".pdf";
                        brochureName.setText("" + resumeName);
                        openAttachment();
                    } else {
                        getBrochure();
                    }


                }
            });

            Course_idEdt_lay.setVisibility(View.VISIBLE);

        }

        private void bindData() {

            Course_idEdt.setText("" + jobdataObj.getPostId());
            Course_NameEdt.setText("" + jobdataObj.getCourseName());
            Institute_NameEdt.setText("" + jobdataObj.getInstituteName());
            CityEdt.setText("" + jobdataObj.getCity());
            locationEdt.setText("" + jobdataObj.getLocation());
            areaEdt.setText("" + jobdataObj.getCity());
            courseDescEdt.setText("" + jobdataObj.getCourseDescription());
            Eligibility_Edt.setText("" + jobdataObj.getEligibility());
            Instructor_NameEdt.setText("" + jobdataObj.getInstituteName());
            emaileEdt.setText("" + jobdataObj.getEmail());
            contactNumEdt.setText("" + jobdataObj.getContactNumber());
            ContactNo_LandlineEdt.setText("" + jobdataObj.getContactNoLandline());
            Start_DateEdt.setText("" + CommonUtils.getCleanDate(jobdataObj.getStartDate()));
            End_DateEdt.setText("" + CommonUtils.getCleanDate(jobdataObj.getEndDate()));
            classStartTimeEdt.setText(""+jobdataObj.getStartTime());
            classendTimeEdt.setText("   " +jobdataObj.getEndTime());
            DurationEdt.setText("" + jobdataObj.getDuration());
            Batch_SizeEdt.setText("" + jobdataObj.getBatchSize());
            Course_FeeEdt.setText("" + jobdataObj.getCourseFee());
            DiscountTypeEdt.setText("" + jobdataObj.getDiscountType());
            DiscountValueEdt.setText("" + jobdataObj.getDiscountValue());
            GroupCountEdt.setText("" + jobdataObj.getGroupCount());
            GrouptDiscountEdt.setText("" + jobdataObj.getDiscountType());
            demoClass.setText("" );
            placementSupport.setText("");
            labTimings.setText("" );
            classTimings.setText("" );
            enrollStatus.setText("" );
            courseStatus.setText("" +jobdataObj.getCourseStatus());
            AddressEdt.setText("" + jobdataObj.getAddress());
            Contact_PersonEdt.setText("" + jobdataObj.getContactPerson());
            Company_URLEdt.setText("" + jobdataObj.getCompanyURL());
//            Class_TimingsEdt.setText("" + jobdataObj.getti);
            Venue_DetailsEdt.setText("" + jobdataObj.getVenue());
//            Course_Enrolled_StatusEdt.setText("" + jobdataObj.gete);

            if (jobdataObj.getEveryDay())
                check_EveryDay.setChecked(true);
            if (jobdataObj.getWeekend())
                check_Weekend.setChecked(true);
            if (jobdataObj.getOnline())
                check_Online.setChecked(true);
            if (jobdataObj.getNormaltrack())
                check_Normal_Track.setChecked(true);
            if (jobdataObj.getNormaltrack())
                check_Fast_Track.setChecked(true);
            if (jobdataObj.getProjectWork())
                check_Project_Work.setChecked(true);

            check_EveryDay.setEnabled(false);
            check_Weekend.setEnabled(false);
            check_Online.setEnabled(false);
            check_Normal_Track.setEnabled(false);
            check_Fast_Track.setEnabled(false);
            check_Project_Work.setEnabled(false);

        }

        public void getBrochure(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null && result.toString().length() > 0) {
                        resumeFromServer = (String) result;
                        getSaveBrochure();
                        progressDialog.dismiss();
                    } else{
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry No Brochure Found", Toast.LENGTH_SHORT).show();
                    }

                }
            }, String.format(Config.LJS_BASE_URL + Config.getBrochure, jobdataObj.get_id()));
        }

        public void getSaveBrochure(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Saving...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            CommonUtils.saveBrochureToSdcard(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        progressDialog.dismiss();
                        resumeName = (String) result;
                        openAttachment();
                    }
                }
            }, resumeFromServer, jobdataObj.get_id(), "", getActivity());
        }

        private void openAttachment() {
            // TODO Auto-generated method stub
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            sdcard_path = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/LJS/Brochure");
            File fileIn = new File(sdcard_path+ "/"+resumeName);
            Uri uri = Uri.fromFile(fileIn);

            if(resumeName.endsWith(".doc"))
            {
                intent.setDataAndType(uri, "application/msword");
                startActivity(intent);
            }else if(resumeName.endsWith(".pdf"))
            {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }else if(resumeName.endsWith(".rtf"))
            {
                intent.setDataAndType(uri, "application/vnd.ms-excel");
                startActivity(intent);
            }
            else {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }
        }
    }

}
