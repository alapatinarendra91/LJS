package com.localjobserver.freshers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import co.talentzing.R;import com.localjobserver.commonutils.CommonCoursesListAdapter;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.FresharsCoursesModel;
import com.localjobserver.models.FresherRegistrationModel;
import com.localjobserver.models.FreshersData;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.LoadFileActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FresshersProfileViewActivity extends ActionBarActivity {

    private Context context;
    public FreshersData mSetupData;
    private ActionBar actionBar = null;
    private TextView tvname,tvresumeheadline,tvkeyskills,tvexperience,tvmailid,tvphonenum,tvlocation,tvlatupdateddate;
    private static ProgressDialog progressDialog;
    private RelativeLayout profilepic;
    private Button editprofilebtn,downloadresumebtn;
    public static ImageView edit_dis;
    private FresherRegistrationModel FresherProfileList;
    private SharedPreferences preferences;
    private byte[] seekerImageData;
    private ListView courseListView;
    private ScrollView scrollview;
    public ArrayList<FresharsCoursesModel> jobList;
    private CommonCoursesListAdapter commonArrayAdapter;
    private ArrayList<RecommendedCourses> courcesList;
    private CoursesListAdpter resumesListAdpter = null;
    private static Drawable dr = null;
    private static String  compressedStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fresshers_profile_view);

        context=this;
        mSetupData = new FreshersData();
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Profile Information");


        preferences = getApplicationContext().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);

        tvname=(TextView)findViewById(R.id.profilename_dis);
        tvresumeheadline=(TextView)findViewById(R.id.resumeheadline_dis);
        tvkeyskills=(TextView)findViewById(R.id.keyskills_dis);
        tvexperience=(TextView)findViewById(R.id.experience_dis);
        tvmailid=(TextView)findViewById(R.id.mailid);
        tvphonenum=(TextView)findViewById(R.id.phonenum);
        tvlocation=(TextView)findViewById(R.id.location);
        tvlatupdateddate=(TextView)findViewById(R.id.lastupdateddate);
        edit_dis = (ImageView)findViewById(R.id.edit_dis);
        profilepic=(RelativeLayout)findViewById(R.id.sec_rel);

        courseListView = (ListView)findViewById(R.id.offeredTrainingsList);
        scrollview = (ScrollView) findViewById(R.id.scrollview);

        editprofilebtn=(Button)findViewById(R.id.profile_details_edit_btn);
        downloadresumebtn=(Button)findViewById(R.id.profile_details_download_btn);
        editprofilebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment newFragment = MainProfileFragment.newInstance();
//                getSupportFragmentManager().beginTransaction().replace(android.R.id.content, newFragment).addToBackStack(null).commit();

                Intent mIntent = new Intent(getApplicationContext(), FreshersProfileActivity.class);
                startActivity(mIntent);
            }
        });
        downloadresumebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), LoadFileActivity.class).putExtra("FRESHER","FRESHER").setAction("gh"));
            }
        });

        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, new PlaceholderFragment()).addToBackStack(null)
                        .commit();
            }
        });

//        openWhatsappContact("9490339969");

        courseListView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scrollview.requestDisallowInterceptTouchEvent(true);

                int action = event.getActionMasked();

                switch (action) {
                    case MotionEvent.ACTION_UP:
                        scrollview.requestDisallowInterceptTouchEvent(false);
                        break;
                }

                return false;
            }
        });

        courseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(ResumesListActivity.this, ResumeDescriptionActivity.class).putExtra("resumesItems", resumesList).putExtra("selectedPos",position));

                Intent jobIntent = new Intent(getApplicationContext(), CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((RecommendedCourses) courcesList.get(position)).get_id());
                jobIntent.setAction("");

                startActivity(jobIntent);

            }
        });


        getprofileData();

        getRecommendedCourses();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public void getRecommendedCourses() {
//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Please wait..");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
        courcesList = new ArrayList<>();
        LiveData.getRecommendedCourses(new ApplicationThread.OnComplete<List<RecommendedCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(FresshersProfileViewActivity.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
//                    progressDialog.dismiss();
                    courcesList = (ArrayList) result;
                    if (null != courcesList && courcesList.size() > 0) {

                        courcesList = (ArrayList) result;
                        CommonUtils.CoursesList = (ArrayList) result;
                        resumesListAdpter = new CoursesListAdpter(FresshersProfileViewActivity.this, courcesList);
                        courseListView.setAdapter(resumesListAdpter);
                    } else {
                        Toast.makeText(FresshersProfileViewActivity.this, "No results found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getOfferedCourses));
    }

    private void getprofileData(){
        FresherProfileList = DatabaseAccessObject.getFresherRegistrationDetails(Queries.getInstance().getDetails("FreshersRegistration"), getApplicationContext());

        tvname.setText(FresherProfileList.getFirstName()+" "+FresherProfileList.getLastName());
        tvresumeheadline.setText(FresherProfileList.getResumeHeadLine());
        tvkeyskills.setText(FresherProfileList.getKeySkills());
        tvmailid.setText(FresherProfileList.getEmail());
        tvphonenum.setText(FresherProfileList.getContactNo());
        tvlocation.setText(FresherProfileList.getLocation());
        tvlatupdateddate.setText("Last Update: "+CommonUtils.getCleanDate(FresherProfileList.getUpdateOn()));

        if (preferences.getString(CommonUtils.getUserEmail(getApplicationContext()), null) != null ){
            {
                try {
                    char CR = '\n';
                    seekerImageData = Base64.decode(CommonUtils.replaceNewLines(preferences.getString(CommonUtils.getUserEmail(getApplicationContext()), null)), Base64.DEFAULT);
                    BitmapDrawable dr = new BitmapDrawable(getApplicationContext().getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));

                    if ( seekerImageData.length != 0)
                        edit_dis.setImageDrawable(dr);
                    else
                        edit_dis.setImageResource(R.drawable.app_icon);

                    edit_dis.invalidate();



                } catch (Exception e) {
                    edit_dis.setImageResource(R.drawable.app_icon);
                    e.printStackTrace();
                }


            }
        }else {
            edit_dis.setImageResource(R.drawable.app_icon);
        }
    }

    private static   Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static  int CURRENT_DISPLAY_SCREEN = 0, FILE_CHOOSER = 1, SELECT_PICTURE =2, FROMCAM = 3;
        private ImageView profilePick;
        private byte[] seekerImageData;
        private View rootView = null;
        private Button choosePic = null,  cancelBtn = null;
        private String profile_pic = "";
        private SharedPreferences appPrefs = null;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_profile2, container, false);
            profilePick = (ImageView)rootView.findViewById(R.id.proFic);
            appPrefs =  getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
            profile_pic = appPrefs.getString(CommonUtils.getUserEmail(getActivity()), null);

            if(null != dr){
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
            }

            if (profile_pic != null){
                try {
                    String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                    seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                    BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));
                    profilePick.setImageDrawable(dr);
                    profilePick.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            choosePic = (Button)rootView.findViewById(R.id.edit);
            choosePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (choosePic.getText().toString().equalsIgnoreCase("edit")) {
                        profilePic();
                    } else {

                        uploadUserImage();

                    }
                }
            });



            cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    if (fm.getBackStackEntryCount() > 0) {
//                        fm.popBackStack();
//                    } else {
//                        getActivity().finish();
//                    }
                    if (profile_pic == null || profile_pic.equalsIgnoreCase(""))
                        CommonUtils.showToast("No Image",getActivity());
                    else
                    DeleteImage();
                }
            });


            return rootView;
        }

       /* public void  uploadUserImage(){


//            if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
            progressDialog.show();
//            }

            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                        editor.commit();
                        Toast.makeText(getActivity(), "Update", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        } else {
                            getActivity().finish();
                        }
                        Intent uiUpdateIntent = new Intent("update");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                    }
                }
            }, "" + CommonUtils.getUserEmail(getActivity()) + ".png", CommonUtils.getUserEmail(getActivity()), compressedStr, getActivity(), 0);

        }*/

        public void uploadUserImage() {

//            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Updating Profile Pic..");
                progressDialog.setCancelable(false);
                progressDialog.show();
//            }

            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    Log.i("", "Result is : " + result.toString());
                    if (result != null) {
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                        editor.commit();
                        Toast.makeText(getActivity(), "Your photo has been updated successfully", Toast.LENGTH_SHORT).show();
                        edit_dis.setImageDrawable(dr);

                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        } else {
                            getActivity().finish();
                        }
                        Intent uiUpdateIntent = new Intent("updateProfilePick_Fresher");
                        uiUpdateIntent.putExtra("imageString", compressedStr);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

                    }
                }
            }, "" + CommonUtils.getUserEmail(getActivity()) + ".png", CommonUtils.getUserEmail(getActivity()), compressedStr, "uploadFresherImage", getActivity(), 0);

        }

        public void DeleteImage(){

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Deleting user Image..");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }

            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    Log.i("", "Result is : " + result.toString());
                    if (result != null) {
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), "");
                        editor.commit();
                        Toast.makeText(getActivity(), "Delete Profile Pic", Toast.LENGTH_SHORT).show();
                        edit_dis.setImageResource(R.drawable.app_icon);
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        } else {
                            getActivity().finish();
                        }
                        Intent uiUpdateIntent = new Intent("updateProfilePick_Fresher");
                        uiUpdateIntent.putExtra("imageString", "delete");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

                    }
                }
            }, "default", CommonUtils.getUserEmail(getActivity()), "", "uploadFresherImage", getActivity(), 0);

        }

        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
                        //selectImage();
                        // Calls an Intent that opens camera to take a picture
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, FROMCAM);
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        //Call an intent that opens gallery to select photo
                        Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == FROMCAM) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(filePath,80,80);
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                Matrix matrix = new Matrix();
                matrix.postRotate(CommonUtils.getImageOrientation(f.getAbsolutePath()));
//                Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(),yourSelectedImage.getHeight(), matrix, true);
                dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                seekerImageData = outputStream.toByteArray();


            } else if (requestCode == SELECT_PICTURE){
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inPurgeable = true;
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath,options);
                dr = new BitmapDrawable(yourSelectedImage);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 90,outputStream);
                seekerImageData = outputStream.toByteArray();
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");

            }// for if
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("update")) {
                    // restore from shared prefs if there's value there
                    if(null != dr)
                        (profilepic).setBackground(dr);
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }else{
            this.finish();
        }
    }

    void openWhatsappContact(String number) {
        Uri mUri = Uri.parse("smsto:+9490339969");
        Intent mIntent = new Intent(Intent.ACTION_SENDTO, mUri);
        mIntent.setPackage("com.whatsapp");
        mIntent.putExtra("sms_body", "The text goes here");
        mIntent.putExtra("chat",true);
        startActivity(mIntent);
    }
}

