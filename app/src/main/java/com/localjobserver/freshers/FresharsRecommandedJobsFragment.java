package com.localjobserver.freshers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonCoursesListAdapter;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.FresharsCoursesModel;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FresharsRecommandedJobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FresharsRecommandedJobsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView noCoursesText;
    private ListView jobsListView;
    private ProgressDialog progressDialog;
    public ArrayList<FresharsCoursesModel> jobList;
    private CommonCoursesListAdapter commonArrayAdapter;
    private View rootView;
    private ArrayList<RecommendedCourses> courcesList;
    private CoursesListAdpter resumesListAdpter = null;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReecommandedJobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FresharsRecommandedJobsFragment newInstance(String param1, String param2) {
        FresharsRecommandedJobsFragment fragment = new FresharsRecommandedJobsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FresharsRecommandedJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.freshars_recommanded_jobs_fragment, container, false);
        jobsListView = (ListView)rootView.findViewById(R.id.recommandedjobsList);
        noCoursesText = (TextView)rootView.findViewById(R.id.noCoursesText);
        getJobsData();
        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent jobIntent = new Intent(getActivity(), CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((RecommendedCourses) courcesList.get(position)).get_id());
                jobIntent.setAction("recomanded");
                startActivity(jobIntent);

            }
        });
        return rootView;
    }

    public void getJobsData(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        courcesList = new ArrayList<>();
        LiveData.getRecommendedCourses(new ApplicationThread.OnComplete<List<RecommendedCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    courcesList = (ArrayList) result;
                    if (null != courcesList && courcesList.size() > 0) {

                        courcesList = (ArrayList) result;
                        CommonUtils.CoursesList = (ArrayList) result;
                        resumesListAdpter = new CoursesListAdpter(getActivity());
                        jobsListView.setAdapter(resumesListAdpter);

                        Intent uiUpdateIntent = new Intent("update");
                        uiUpdateIntent.putExtra("type", 1);
                        uiUpdateIntent.putExtra("appliedCount", courcesList.size());
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                    } else {
                        noCoursesText.setVisibility(View.VISIBLE);
                        jobsListView.setVisibility(View.GONE);
//                        Toast.makeText(getActivity(), "No Recommended results found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getRecommendedJobs_freshars, CommonUtils.getUserEmail(getActivity())));
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    class CoursesListAdpter extends BaseAdapter {

        Context context;
        public CoursesListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.courses_list_item, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
            jobTitle.setText(courcesList.get(position).getCourseName());
            TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
            jobLoc.setText(""+courcesList.get(position).getInstituteName());
            TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
            jobComp.setText(""+courcesList.get(position).getCity());
            TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
            jobExp.setText(""+courcesList.get(position).getLocation());
            TextView keySkills = (TextView)convertView.findViewById(R.id.contactPerson);
            keySkills.setText(""+courcesList.get(position).getContactNumber());
            TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
            courseFee.setText(""+courcesList.get(position).getCourseFee());
            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return courcesList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return courcesList.get(position);
        }

    }


}
