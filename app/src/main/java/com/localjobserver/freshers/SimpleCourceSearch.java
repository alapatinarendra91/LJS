package com.localjobserver.freshers;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SimpleCourceSearch#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SimpleCourceSearch extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    private MultiAutoCompleteTextView autoKeys;
    private AutoCompleteTextView locationAuto,inNameAuto,localityAuto,disCountAuto;
    private Button searchBtn,cancilBtn;
    private ProgressDialog progressDialog;
    private ArrayList<String> keySkillsList;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_KEYSKILLS = 1;
    private static final int RESULT_INSTITUTION = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_DISCOUNT_TYPE = 4;
    public LinkedHashMap<String,String>  locationsMap = null,institutionMap,discountMap;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SimpleCourceSearch.
     */
    // TODO: Rename and change types and number of parameters
    public static SimpleCourceSearch newInstance(String param1, String param2) {
        SimpleCourceSearch fragment = new SimpleCourceSearch();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SimpleCourceSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_simple_cource_search, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        locationAuto = (AutoCompleteTextView)rootView.findViewById(R.id.locationAuto);
        inNameAuto = (AutoCompleteTextView)rootView.findViewById(R.id.inNameAuto);
        localityAuto = (AutoCompleteTextView)rootView.findViewById(R.id.localityAuto);
        disCountAuto = (AutoCompleteTextView)rootView.findViewById(R.id.disCountAuto);
        searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
        cancilBtn = (Button)rootView.findViewById(R.id.cancilBtn);
        autoKeys = (MultiAutoCompleteTextView)rootView.findViewById(R.id.autoscoursekeyword);
        autoKeys.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        getKeyWords();
        getLocations();
        getDiscountType();
        getInstitutions();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoKeys.getText().toString().equals("") && locationAuto.getText().toString().equals("") && localityAuto.getText().toString().equals("") && inNameAuto.getText().toString().equals("") && disCountAuto.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please select any Criteria", Toast.LENGTH_SHORT).show();
                } else {
                    String urlPrepared = String.format(Config.LJS_BASE_URL + Config.simpleCourseSearch, CommonUtils.removeLastComma(autoKeys.getText().toString().replace(" ", "")), locationAuto.getText().toString(), localityAuto.getText().toString(), inNameAuto.getText().toString(), disCountAuto.getText().toString());
                    Intent in = new Intent(getActivity(), CoursesList.class);
                    in.putExtra("coursesLink", urlPrepared);
                    startActivity(in);
                }

            }
        });
        cancilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();

            }
        });

        return rootView ;
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }

    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void getDiscountType(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    discountMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISCOUNT_TYPE;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getDiscountTypes), CommonKeys.arrDiscountType);
    }

    public void getInstitutions(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    institutionMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INSTITUTION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getInstitutes), CommonKeys.arrInstitutes);
    }

    class StatusHandler extends Handler {

        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what){
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occured while getting data from  server.", Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_KEYSKILLS:
                    if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()){
                        autoKeys.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                    }
                    break;

                case RESULT_LOCATIONS:
                    if (null != locationsMap && locationsMap.size() > 0 && isVisible()){
                        locationAuto.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                        localityAuto.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Locality")));
                    }
                    break;
                case RESULT_DISCOUNT_TYPE:
                    if (null != discountMap && discountMap.size() > 0 && isVisible()){
                        disCountAuto.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(discountMap, "DiscountType")));
                    }
                    break;

                case RESULT_INSTITUTION:
                    if (null != institutionMap ) {
                        inNameAuto.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(institutionMap, "Industry")));
                    } else {
                        Toast.makeText(getActivity(),"Not able to get institution details",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            ((FnTNavigationActivity) activity).onSectionAttached("Simple Search");
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
