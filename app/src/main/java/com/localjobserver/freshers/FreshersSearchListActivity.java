package com.localjobserver.freshers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.localjobserver.ui.MainInfoActivity;
import co.talentzing.R;

import java.util.ArrayList;

public class FreshersSearchListActivity extends ActionBarActivity {
    private ArrayList<com.localjobserver.models.FreshersData> freshersDataList;
    private FreshersData freshersDataAdapter = null;
    private ListView freshresListView;
    private ActionBar actionBar = null;
    private TextView searchTxt;
    private ImageView editBtn;
    private String searchStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freshers_search_list);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        freshresListView = (ListView)findViewById(R.id.freshersListView);
        searchTxt = (TextView)findViewById(R.id.searchTxt);
        editBtn = (ImageView)findViewById(R.id.searchEdit);
        searchStr = getIntent().getStringExtra("searchTxt");
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FreshersSearchListActivity.this, MainInfoActivity.class).putExtra("searchTxt", searchStr));
            }
        });
        searchTxt.setText(""+searchStr);

        com.localjobserver.models.FreshersData mFreshersData = new com.localjobserver.models.FreshersData();
        mFreshersData.setCourceName("JAVA J2EE");
        mFreshersData.setInstitutionName("IILT");
        mFreshersData.setCity("Bangalore");
        mFreshersData.setContactPname("iilt");
        mFreshersData.setDiscountType("Group Discount");
        mFreshersData.setDiscountValue("5%");
        mFreshersData.setCourceFee("5000");
        mFreshersData.setPostedDate("15/Mar/2015 16:24");
        mFreshersData.setEmail("info@iilt.co.in");
        mFreshersData.setFreConNum("9742793615");
        mFreshersData.setAddress("1st Floor, Carmel Complex, Gedlahalli, Hennur, Bangalore-560077");
        mFreshersData.setCompURL("www.iilt.co.in");


        com.localjobserver.models.FreshersData mFreshersData1 = new com.localjobserver.models.FreshersData();
        mFreshersData1.setCourceName("Java");
        mFreshersData1.setInstitutionName("V.S. Technologies Pvt. Ltd.,");
        mFreshersData1.setCity("Hyderabad");
        mFreshersData1.setContactPname("Naveen");
        mFreshersData1.setDiscountType("Group Discount");
        mFreshersData1.setDiscountValue("10%%");
        mFreshersData1.setCourceFee("4500");
        mFreshersData1.setPostedDate("20/Mar/2015 12:55");
        mFreshersData1.setEmail("naveenreddy.potti@gmail.com");
        mFreshersData1.setFreConNum("9885612057");
        mFreshersData1.setAddress("Miyapur , Allwyn");
        mFreshersData1.setCompURL("www.vstechnologies.com");
        mFreshersData1.setStartDate("03/20/2015");
        mFreshersData1.setEndDate("05/22/2015");
        mFreshersData1.setStartTime("08:55");
        mFreshersData1.setEndTime("15:55");

        freshersDataList = new ArrayList<com.localjobserver.models.FreshersData>();
        freshersDataList.add(mFreshersData);
        freshersDataList.add(mFreshersData1);

        freshersDataAdapter = new FreshersData(this);
        freshresListView.setAdapter(freshersDataAdapter);

        freshresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(FreshersSearchListActivity.this, FreshersDetailsActivity.class).putParcelableArrayListExtra("courceItems",freshersDataList).putExtra("selectedPos",position));

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_freshers_search_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    class FreshersData extends BaseAdapter {

        Context context;
        public FreshersData(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.freshers_search_listitem, null);
            }
            TextView courceName = (TextView)convertView.findViewById(R.id.courceNAME);
            courceName.setText(""+freshersDataList.get(position).getCourceName());

            TextView institutionTxt = (TextView)convertView.findViewById(R.id.institutionNAME);
            institutionTxt.setText(""+freshersDataList.get(position).getInstitutionName());

            TextView cityTxt = (TextView)convertView.findViewById(R.id.locName);
            cityTxt.setText(""+freshersDataList.get(position).getCity());

            TextView feeTxt = (TextView)convertView.findViewById(R.id.courceFee);
            feeTxt.setText(""+freshersDataList.get(position).getCourceFee());

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return freshersDataList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return freshersDataList.get(position);
        }

    }
}
