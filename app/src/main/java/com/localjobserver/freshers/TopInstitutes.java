package com.localjobserver.freshers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopInstitutes {

    @SerializedName("InstituteName")
    @Expose
    private String InstituteName;
    @SerializedName("CourseName")
    @Expose
    private String CourseName;
    @SerializedName("Date")
    @Expose
    private String Date;
    @SerializedName("Rating")
    @Expose
    private String Rating;
    @SerializedName("TraineeEmail")
    @Expose
    private String TraineeEmail;
    @SerializedName("InstituteEmail")
    @Expose
    private String InstituteEmail;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("City")
    @Expose
    private String City;

    /**
     *
     * @return
     * The InstituteName
     */
    public String getInstituteName() {
        return InstituteName;
    }

    /**
     *
     * @param InstituteName
     * The InstituteName
     */
    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    /**
     *
     * @return
     * The CourseName
     */
    public String getCourseName() {
        return CourseName;
    }

    /**
     *
     * @param CourseName
     * The CourseName
     */
    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }

    /**
     *
     * @return
     * The Date
     */
    public String getDate() {
        return Date;
    }

    /**
     *
     * @param Date
     * The Date
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     *
     * @return
     * The Rating
     */
    public String getRating() {
        return Rating;
    }

    /**
     *
     * @param Rating
     * The Rating
     */
    public void setRating(String Rating) {
        this.Rating = Rating;
    }

    /**
     *
     * @return
     * The TraineeEmail
     */
    public String getTraineeEmail() {
        return TraineeEmail;
    }

    /**
     *
     * @param TraineeEmail
     * The TraineeEmail
     */
    public void setTraineeEmail(String TraineeEmail) {
        this.TraineeEmail = TraineeEmail;
    }

    /**
     *
     * @return
     * The InstituteEmail
     */
    public String getInstituteEmail() {
        return InstituteEmail;
    }

    /**
     *
     * @param InstituteEmail
     * The InstituteEmail
     */
    public void setInstituteEmail(String InstituteEmail) {
        this.InstituteEmail = InstituteEmail;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The City
     */
    public String getCity() {
        return City;
    }

    /**
     *
     * @param City
     * The City
     */
    public void setCity(String City) {
        this.City = City;
    }

}