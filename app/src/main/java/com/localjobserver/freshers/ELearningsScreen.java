package com.localjobserver.freshers;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.trainingsNfreshers.FnTNavigationActivity;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ELearningsScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ELearningsScreen extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<ElearningsModel> elearningsDataList;
    private ListView eLearnignsListView;
    private View rootView;
    private ProgressDialog progressDialog;
    private static final int RESULT_ELEARNINGS = 1;
    private static final int RESULT_INDUSTRIES = 2;
    private TextView shareE_learnings;
    private RelativeLayout shareE_learningsLay;
    private Spinner categoryspin;
    private EditText subcategoryEdt,descriptionEdt,urlEdt,postedbyEdt;
    private Button submitBtn,cancilBtn;
    private LinkedHashMap<String, String> industries;
    private ElearningsModel mDataModel;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ELearningsScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static ELearningsScreen newInstance(String param1, String param2) {
        ELearningsScreen fragment = new ELearningsScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ELearningsScreen() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_elearnings_screen, container, false);

        mDataModel = new ElearningsModel();
        categoryspin = (Spinner)rootView.findViewById(R.id.categoryspin);
        subcategoryEdt = (EditText)rootView.findViewById(R.id.subcategoryEdt);
        descriptionEdt = (EditText)rootView.findViewById(R.id.descriptionEdt);
        urlEdt = (EditText)rootView.findViewById(R.id.urlEdt);
        postedbyEdt = (EditText)rootView.findViewById(R.id.postedbyEdt);
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        cancilBtn = (Button)rootView.findViewById(R.id.cancilBtn);
        shareE_learningsLay = (RelativeLayout)rootView.findViewById(R.id.shareE_learningsLay);
        shareE_learnings = (TextView)rootView.findViewById(R.id.shareE_learnings);
        String styledText = "Would you like to share any E-learnings? <font color='#008ACE'>Yes</a></font>";
        shareE_learnings.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
        shareE_learnings.setMovementMethod(LinkMovementMethod.getInstance());
        eLearnignsListView = (ListView) rootView.findViewById(R.id.eLearningList);
        getElearnings();
        shareE_learnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCategoryes();
                shareE_learningsLay.setVisibility(View.VISIBLE);
                shareE_learnings.setVisibility(View.GONE);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isNetworkAvailable(getActivity())){
//                            uploadUserResume();
                    if (validateShareELearnDetails()){

                                    setElearningsData();
                                    uploadElearningData(toUpload());
                    }

                } else {
                    Toast.makeText(getActivity(), "Please Check network Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        cancilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareE_learningsLay.setVisibility(View.GONE);
                shareE_learnings.setVisibility(View.VISIBLE );
            }
        });
        return rootView;
    }

    public void getCategoryes(){if (progressDialog == null) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
    }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    Log.i("", "Institutes is : " + result);
                    industries = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public boolean validateShareELearnDetails(){
        Form mForm = new Form(getActivity());
        if (CommonUtils.spinnerSelect("Category",categoryspin.getSelectedItemPosition(),getActivity())) {
            mForm.addField(Field.using(subcategoryEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(descriptionEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(urlEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(postedbyEdt).validate(NotEmpty.build(getActivity())));

            return (mForm.isValid()) ? true : false;
        }else
            return (mForm.isValid()) ? false : false;

    }


    public void getElearnings() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);

        }
        progressDialog.show();
        LiveData.getElearnings(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    elearningsDataList = (ArrayList<ElearningsModel>) result;
                    Log.i("", "Count is : " + elearningsDataList.size());
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_ELEARNINGS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getElearnings));
    }

    public void setElearningsData() {
        mDataModel.setCategory(categoryspin.getSelectedItem().toString());
        mDataModel.setSubcategory(subcategoryEdt.getText().toString());
        mDataModel.setDescription(descriptionEdt.getText().toString());
        mDataModel.setUrl(urlEdt.getText().toString());
        mDataModel.setPostedBy(postedbyEdt.getText().toString());
        mDataModel.setPostedDate(CommonUtils.getDateTime());
    }

    public HashMap toUpload() {
        HashMap freshersDataMap = new HashMap();
        freshersDataMap.put("Category", mDataModel.getCategory());
        freshersDataMap.put("Subcategory", mDataModel.getSubcategory());
        freshersDataMap.put("Description", mDataModel.getDescription());
        freshersDataMap.put("Verified", "false");
        freshersDataMap.put("Description", mDataModel.getDescription());
        freshersDataMap.put("Url", mDataModel.getUrl());
        freshersDataMap.put("PostedBy", mDataModel.getPostedBy());
        freshersDataMap.put("PostDate", mDataModel.getPostedDate());
        return freshersDataMap;
    }

    public void uploadElearningData(HashMap<String, String> input) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading Elearning data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    CommonUtils.showToast("Failed to uploda Elearnings", getActivity());
                    if (result != null) {
                    }
                    return;
                }
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        CommonUtils.showToast("Uploaded successfully \n Wait for admin approval",getActivity());
                        categoryspin.setSelection(0);
                        subcategoryEdt.setText("");
                        descriptionEdt.setText("");
                        urlEdt.setText("");
                        postedbyEdt.setText("");
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.postElearnings, "" + requestString));
    }

    class ElearningsListAdpter extends BaseAdapter {

        Context context;
        public ElearningsListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.elearnings_list_item, null);
            }
            TextView categoryTxt = (TextView)convertView.findViewById(R.id.categoryType);
            categoryTxt.setText(elearningsDataList.get(position).getCategory());
            TextView subcategoryTxt = (TextView)convertView.findViewById(R.id.subCategoryType);
            subcategoryTxt.setText(""+elearningsDataList.get(position).getSubcategory());
            TextView description = (TextView)convertView.findViewById(R.id.desc);
            description.setText(""+elearningsDataList.get(position).getDescription());
            TextView urlTxt = (TextView)convertView.findViewById(R.id.url);
            SpannableString content = new SpannableString(elearningsDataList.get(position).getUrl());
            content.setSpan(new UnderlineSpan(), 0, elearningsDataList.get(position).getUrl().length(), 0);
            urlTxt.setText(content);
            urlTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(elearningsDataList.get(position).getUrl()));
                        startActivity(browserIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            TextView postedBy = (TextView)convertView.findViewById(R.id.postedBy);
            postedBy.setText(""+elearningsDataList.get(position).getPostedBy());
            TextView postedDate = (TextView)convertView.findViewById(R.id.postedDate);
            postedDate.setText(""+ CommonUtils.getCleanDate(elearningsDataList.get(position).getPostedDate()));

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return elearningsDataList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return elearningsDataList.get(position);
        }

    }


    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {

                case RESULT_ELEARNINGS:
                    eLearnignsListView.setAdapter(new ElearningsListAdpter(getActivity()));
                    ((FnTNavigationActivity) getActivity()).onSectionAttached("E-Learnings" + "(" + elearningsDataList.size() + ")");
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries && null != categoryspin) {
                        categoryspin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(industries, "Category ")));
                    }else{
                        Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((FnTNavigationActivity) activity).onSectionAttached("E-Learnings");
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

}
