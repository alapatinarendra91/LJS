package com.localjobserver.freshers;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_mail_fresher#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_mail_fresher extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private EditText toMail,fromemailEdit,subjectEdit,message;
    private TextView to_email,subjectTxt;
    private LinearLayout fromMail,message_lay;
    private View rootView;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private SharedPreferences appPrefs;
    private Button submitBtn;
    private Bundle bundleSubCat;
    private String configUrl = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.mail_fresher, container, false);
        toMail = (EditText)rootView.findViewById(R.id.toMail);
        fromemailEdit = (EditText)rootView.findViewById(R.id.fromemailEdit);
        subjectEdit = (EditText)rootView.findViewById(R.id.subjectEdit);
        to_email = (TextView)rootView.findViewById(R.id.to_email);
        subjectTxt = (TextView)rootView.findViewById(R.id.subjectTxt);
        message = (EditText)rootView.findViewById(R.id.msgEdit);
        fromMail = (LinearLayout)rootView.findViewById(R.id.fromMail);
        message_lay = (LinearLayout)rootView.findViewById(R.id.message_lay);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        fromemailEdit.setText(""+appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);

        bundleSubCat = getArguments();

        if (CommonUtils.isLoggedIn(getActivity()))
            fromMail.setVisibility(View.GONE);

        if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Email")){
            subjectEdit.setText("Job opportunity | " + bundleSubCat.getString("title"));
            toMail.setHint("Enter Receiver Email");
        }else if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Sms")){
            if (CommonUtils.isLoggedIn(getActivity()))
                fromMail.setVisibility(View.GONE);
                message_lay.setVisibility(View.GONE);
                subjectTxt.setText("Subject :");
                to_email.setText("To :");
                toMail.setHint("Mobile numbers with comma(,) separator");
                toMail.setKeyListener(DigitsKeyListener.getInstance("0123456789,"));
                subjectEdit.setText("" + bundleSubCat.getString("title"));
//            }else {
//
//            }

        }else {
            subjectEdit.setText("Course Opportunity | " + bundleSubCat.getString("title"));
            toMail.setHint("Enter To Email");
        }

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateMailDetails()){
                    if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Email")){
//                        configUrl = Config.sendEmail;
                        configUrl = String.format(Config.LJS_BASE_URL + Config.sendEmail, fromemailEdit.getText().toString(), toMail.getText().toString(), bundleSubCat.getString("ID"), subjectEdit.getText().toString().replace("|",","),message.getText().toString());
                        uploadMail();
                    }else if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Sms")){
                         configUrl = String.format(Config.LJS_BASE_URL + Config.sendJobAsSms, fromemailEdit.getText().toString(), toMail.getText().toString(), subjectEdit.getText().toString().replace("|", ","), bundleSubCat.getString("ID"));
                        uploadMail();
                    }else {
//                        configUrl = Config.SendCourseAsEmail;
                        configUrl = String.format(Config.LJS_BASE_URL + Config.SendCourseAsEmail, fromemailEdit.getText().toString(), toMail.getText().toString(), bundleSubCat.getString("ID"), subjectEdit.getText().toString().replace("|",","),message.getText().toString());
                        uploadMail();
                    }

                }
            }
        });

        Button cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().popBackStack();
                clearFields();
            }
        });
        return rootView;
    }

    public void clearFields(){


        toMail.setText("");
        fromemailEdit.setText("");
        subjectEdit.setText("");

        message.setText("");
        getActivity().finish();

    }


    public boolean validateMailDetails(){
        Form mForm = new Form(getActivity());
        if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Sms")){
            mForm.addField(Field.using(toMail).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.fromemailEdit)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using(subjectEdit).validate(NotEmpty.build(getActivity())));
            if (validateMobileNo())
                return (mForm.isValid()) ? true : false;

        }else {
            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.toMail)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.fromemailEdit)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            return (mForm.isValid()) ? true : false;
        }


//        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.subjectEdit)).validate(NotEmpty.build(getActivity())));
//        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.msgEdit)).validate(NotEmpty.build(getActivity())));
        return false;
    }

    public boolean validateMobileNo(){

        if (CommonUtils.isValidMobile(toMail.getText().toString(),toMail))
            return true;
        else
            return  false;
    }
    public void uploadMail() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Sms"))
                        CommonUtils.showToast("Sms send failed",getActivity());
                    else
                        CommonUtils.showToast("Email send failed", getActivity());
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, configUrl);

    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    if (bundleSubCat.getString("TYPE").equalsIgnoreCase("Seekers Sms"))
                        CommonUtils.showToast("Your sms has been sent successfully",getActivity());
                    else
                    Toast.makeText(getActivity(), "Your mail has been sent successfully.", Toast.LENGTH_SHORT).show();
                    toMail.setText("");
                    getActivity().finish();
                    break;
            }
        }
    }

}
