package com.localjobserver.freshers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.helper.SocialHelper;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Log;
import com.localjobserver.social.LinkedInSocialDialog;
import com.localjobserver.ui.MainInfoActivity;

import java.util.ArrayList;
import java.util.List;

public class CoursesList extends ActionBarActivity implements CoursesListAdpter.OnCartChangedListener{

    private ListView resumesListView;
    private ActionBar actionBar = null;
    private TextView searchTxt;
    private ImageView editBtn;
    private String searchStr;
    private ArrayList<RecommendedCourses> courcesList;
    public String preparedUrltoGetResumes;
    private ProgressDialog progressDialog;
    private com.localjobserver.freshers.CoursesListAdpter resumesListAdpter = null;
    private SocialHelper mSocialHelper = null;
    private String courseIdId;
    private Bundle bundle1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses_list);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        resumesListView = (ListView)findViewById(R.id.cursesListView);
        searchTxt = (TextView)findViewById(R.id.searchTxt);
        editBtn = (ImageView)findViewById(R.id.searchEdit);
        searchStr = getIntent().getStringExtra("searchTxt");
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CoursesList.this, MainInfoActivity.class).putExtra("searchTxt", searchStr));
            }
        });
        searchTxt.setText("" + searchStr);
        preparedUrltoGetResumes = getIntent().getStringExtra("coursesLink");
        Log.i("", "Link is " + preparedUrltoGetResumes);
        getRecommendedCourses(preparedUrltoGetResumes);

        if (getIntent().getStringExtra("actionBarName") != null)
            getSupportActionBar().setTitle(""+getIntent().getStringExtra("actionBarName")+"("+courcesList.size()+")");
        else
            getSupportActionBar().setTitle("Courses List"+"("+courcesList.size()+")");

        resumesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(ResumesListActivity.this, ResumeDescriptionActivity.class).putExtra("resumesItems", resumesList).putExtra("selectedPos",position));

                Intent jobIntent = new Intent(CoursesList.this, CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((RecommendedCourses) courcesList.get(position)).get_id());
                if (getIntent().getStringExtra("actionBarName") != null)
                jobIntent.setAction("applied");
                else
                    jobIntent.setAction("");

                startActivity(jobIntent);

            }
        });

        mSocialHelper = new SocialHelper(CoursesList.this, this,null,courcesList);
    }


    public void getRecommendedCourses(final String url){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(CoursesList.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        courcesList = new ArrayList<>();
        LiveData.getRecommendedCourses(new ApplicationThread.OnComplete<List<RecommendedCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(CoursesList.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    courcesList = (ArrayList) result;
                    if (null != courcesList && courcesList.size() > 0) {

                        courcesList = (ArrayList) result;
                        CommonUtils.CoursesList = (ArrayList) result;
                        CommonUtils.CoursesList_applied = (ArrayList) result;
                        resumesListAdpter = new com.localjobserver.freshers.CoursesListAdpter(CoursesList.this, courcesList);
                        resumesListView.setAdapter(resumesListAdpter);
                        resumesListAdpter.setOnCartChangedListener(CoursesList.this);

                        if (getIntent().getStringExtra("actionBarName") != null)
                            getSupportActionBar().setTitle("" + getIntent().getStringExtra("actionBarName") + "(" + courcesList.size() + ")");
                        else
                            getSupportActionBar().setTitle("Courses List" + "(" + courcesList.size() + ")");


                    } else {
                        if (getIntent().getStringExtra("actionBarName") != null)
                            Toast.makeText(CoursesList.this, "No results found", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(CoursesList.this, "No courses found, please refine your search", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, url);
    }

//    @Override
//    public void getStatus(String status) {
//        CommonUtils.showToast(""+status,CoursesList.this);
//    }
    @Override
    public void setCartClickListener(String clickItem,final int selectPos) {
        if (clickItem.contains("facebook")){
            Session.openActiveSession(CoursesList.this, true, new Session.StatusCallback() {
                // callback when session changes state
                public void call(Session session, SessionState state, Exception exception) {
                    if (session.isOpened()) {
                        mSocialHelper.publishFeedDialog(selectPos, "fresher");
                    }
                }
            });
        }else if (clickItem.contains("linkedin")){
            StringBuffer lnStr = new StringBuffer();
            lnStr.append("\nCourse Title:");
            lnStr.append("\n");
            lnStr.append(courcesList.get(selectPos).getCourseName());
            lnStr.append("\n");
            lnStr.append("More Info At:");
            lnStr.append("\n");
            lnStr.append("https://localjobserver.com/home.aspx");
            startActivity(new Intent(CoursesList.this, LinkedInSocialDialog.class).putExtra("lnData", lnStr.toString()));
        }else if (clickItem.contains("email")){
            courseIdId  = String.valueOf(courcesList.get(selectPos).get_id());
            bundle1 = new Bundle();
            bundle1.putString("ID", courseIdId);
            bundle1.putString("TYPE", "FRESHER");
            bundle1.putString("title", courcesList.get(selectPos).getCourseName());
            startActivity(new Intent(CoursesList.this, MailFresherActivity.class).putExtra("type", "Training Email").putExtra("ID", bundle1));
        }else {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class CoursesListAdpter extends BaseAdapter {

        Context context;
        public CoursesListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.courses_list_item, null);
            }
            TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
            jobTitle.setText(""+courcesList.get(position).getCourseName());
            TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
            jobLoc.setText("Institute/Business Name: "+courcesList.get(position).getInstituteName());
            TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
            jobComp.setText("Location: "+courcesList.get(position).getLocation());
            TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
            jobExp.setText("Locality: "+courcesList.get(position).getCity());
            TextView contactPerson = (TextView)convertView.findViewById(R.id.contactPerson);
//            contactPerson.setText("Mobile No: "+courcesList.get(position).getContactNumber());
            TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
            courseFee.setText("Corse Fee: "+courcesList.get(position).getCourseFee()+"\nAfter Discount Fee: "+courcesList.get(position).getAfterDiscountedfee()+"\nStart Date: "+CommonUtils.getCleanDate(courcesList.get(position).getStartDate())
                    +"\nCourse Enrolled Status: "+courcesList.get(position).getCourseStatus()+"\nPosted Date: "+CommonUtils.getCleanDate(courcesList.get(position).getPostDate())
                    +"\nContact Person: "+courcesList.get(position).getContactPerson());
            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return courcesList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return courcesList.get(position);
        }

    }
}
