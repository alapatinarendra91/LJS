package com.localjobserver.freshers;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.trainingsNfreshers.FnTNavigationActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link TopInstitutionsScreen#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TopInstitutionsScreen extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView topInsitutionsListView;
    private static final int RESULT_TOPINSTITUTIONS = 1;
    private List<TopInstitutes> topInstitutesDataList;
    private ProgressDialog progressDialog;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TopInstitutionsScreen.
     */
    // TODO: Rename and change types and number of parameters
    public static TopInstitutionsScreen newInstance(String param1, String param2) {
        TopInstitutionsScreen fragment = new TopInstitutionsScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TopInstitutionsScreen() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_top_institutions_screen, container, false);
        topInsitutionsListView = (ListView) rootView.findViewById(R.id.topInstitutionsList);
        getTopInstitutes();
        return rootView;
    }

    public void getTopInstitutes() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);

        }
        progressDialog.show();
        LiveData.getTopInstitutes(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    topInstitutesDataList = (ArrayList<TopInstitutes>) result;
                    Log.i("", "Count is : " + topInstitutesDataList.size());
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_TOPINSTITUTIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getTopInstitutes));
    }

    class InstitutionsListAdpter extends BaseAdapter {

        Context context;
        public InstitutionsListAdpter(Context context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.topinstitutions_list_item, null);
            }
            TextView categoryTxt = (TextView)convertView.findViewById(R.id.inName);
            categoryTxt.setText(topInstitutesDataList.get(position).getInstituteName());
            TextView subcategoryTxt = (TextView)convertView.findViewById(R.id.courseNAME);
            subcategoryTxt.setText(topInstitutesDataList.get(position).getCourseName());
            TextView description = (TextView)convertView.findViewById(R.id.location);
            description.setText(topInstitutesDataList.get(position).getCity());
            TextView urlTxt = (TextView)convertView.findViewById(R.id.locality);
            urlTxt.setText(topInstitutesDataList.get(position).getLocation());
            TextView postedBy = (TextView)convertView.findViewById(R.id.rating);
            postedBy.setText(""+topInstitutesDataList.get(position).getRating());
            TextView postedDate = (TextView)convertView.findViewById(R.id.postedDate);
            postedDate.setText(CommonUtils.getCleanDate(topInstitutesDataList.get(position).getDate()));

            return convertView;
        }


        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return topInstitutesDataList.size();
        }


        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return topInstitutesDataList.get(position);
        }

    }


    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {

                case RESULT_TOPINSTITUTIONS:
                    topInsitutionsListView.setAdapter(new InstitutionsListAdpter(getActivity()));
                    break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ((FnTNavigationActivity) activity).onSectionAttached("Top 10 Institutes");
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

}
