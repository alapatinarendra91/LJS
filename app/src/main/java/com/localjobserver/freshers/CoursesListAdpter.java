package com.localjobserver.freshers;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.models.RecommendedCourses;

import java.util.ArrayList;

/**
 * Created by siva on 25/10/15.
 */
public class CoursesListAdpter extends BaseAdapter {
    private OnCartChangedListener onCartChangedListener;
    public  Context context;
    private ArrayList<RecommendedCourses> courcesList;
    public CoursesListAdpter(Context context, ArrayList<RecommendedCourses> courcesList) {
        super();
        this.context=context;
        this.courcesList=courcesList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.courses_list_item, null);
        }
        TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
        jobTitle.setText(courcesList.get(position).getCourseName());
        TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
        jobLoc.setText(Html.fromHtml("<b>Institute Name&#160;&#160;: </b>"+courcesList.get(position).getInstituteName()));
        TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
        jobComp.setText(Html.fromHtml("<b>Location&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </b>"+courcesList.get(position).getCity()));
        TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
        jobExp.setText(Html.fromHtml("<b>Locality&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </b>"+courcesList.get(position).getLocation()));
        TextView keySkills = (TextView)convertView.findViewById(R.id.contactPerson);
        keySkills.setText(Html.fromHtml("<b>ContactNumber: </b>"+courcesList.get(position).getContactNumber()));
        TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
        courseFee.setText(Html.fromHtml("<b>Course Fee&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </b>" + courcesList.get(position).getCourseFee()+
                "<br/>"+"<b>Posted Date&#160;&#160;&#160;&#160;&#160;&#160;: </b> "+CommonUtils.getCleanDate((courcesList.get(position)).getPostDate()) +
                "<br/>" + "<b>Start Date&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;: </b> " + CommonUtils.getCleanDate((courcesList.get(position)).getStartDate()) +
                "<br/>"+"<b>After Discounted Fee: </b> "+(courcesList.get(position)).getAfterDiscountedfee()+
                "<br/>" + "<b>Course Enrolled Status:</b> " + (courcesList.get(position)).getStatus()));
        ImageView chatImg = (ImageView)convertView.findViewById(R.id.chatImg);
        ImageView emailImg = (ImageView)convertView.findViewById(R.id.emailImg);
        ImageView linkedInImg = (ImageView)convertView.findViewById(R.id.linkedInImg);
        ImageView fbImg = (ImageView)convertView.findViewById(R.id.fbImg);
         chatImg.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 onCartChangedListener.setCartClickListener("chat",position);
             }
         });

        emailImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("email",position);
            }
        });

        linkedInImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("linkedin",position);
            }
        });

        fbImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("facebook",position);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return courcesList.size();
    }


    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return courcesList.get(position);
    }
    private class ViewHolder {
    }

    public void setOnCartChangedListener(OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status,int position);
    }


}
