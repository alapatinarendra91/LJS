package com.localjobserver.freshers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonCoursesListAdapter;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.FresharsCoursesModel;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;

import java.util.ArrayList;
import java.util.List;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FresharsAppliedjobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FresharsAppliedjobsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView jobsListView;
    private ProgressDialog progressDialog;
    public ArrayList<FresharsCoursesModel> jobList;
    private CommonCoursesListAdapter commonArrayAdapter;
    private ArrayList<RecommendedCourses> courcesList;
    private CoursesListAdpter resumesListAdpter = null;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppliedJobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FresharsAppliedjobsFragment newInstance(String param1, String param2) {
        FresharsAppliedjobsFragment fragment = new FresharsAppliedjobsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FresharsAppliedjobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_applied_jobs, container, false);
        jobsListView = (ListView)rootView.findViewById(R.id.appliedjobsList);
//        getJobsData();
        getAppliedCourses();
        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("jobItems", (ArrayList<FresharsCoursesModel>) jobList).putExtra("_id", ((FresharsCoursesModel) jobList.get(position)).getId()).putExtra("selectedPos", position).setAction("fromApplied"));

                Intent jobIntent = new Intent(getActivity(), CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((RecommendedCourses) courcesList.get(position)).get_id());
                jobIntent.setAction("applied");
                startActivity(jobIntent);

            }
        });
        return rootView;
    }

    public void getAppliedCourses(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        courcesList = new ArrayList<>();
        LiveData.getRecommendedCourses(new ApplicationThread.OnComplete<List<RecommendedCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    courcesList = (ArrayList) result;
                    if (null != courcesList && courcesList.size() > 0) {

                        courcesList = (ArrayList) result;
                        CommonUtils.CoursesList_applied = (ArrayList) result;
                        resumesListAdpter = new CoursesListAdpter(getActivity(), courcesList);
                        jobsListView.setAdapter(resumesListAdpter);

                        Intent uiUpdateIntent = new Intent("update");
                        uiUpdateIntent.putExtra("type", 0);
                        uiUpdateIntent.putExtra("appliedCount", courcesList.size());
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                    } else {
                        Toast.makeText(getActivity(), "No Applied results found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getFresherEnrolledCourses, CommonUtils.getUserEmail(getActivity())));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


//    class CoursesListAdpter extends BaseAdapter {
//
//        Context context;
//        public CoursesListAdpter(Context context) {
//            super();
//            this.context=context;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                LayoutInflater mInflater= LayoutInflater.from(context);
//                convertView = mInflater.inflate(R.layout.courses_list_item, null);
//            }
//            TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
//            jobTitle.setText(courcesList.get(position).getCourseName());
//            TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
//            jobLoc.setText(""+courcesList.get(position).getInstituteName());
//            TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
//            jobComp.setText(""+courcesList.get(position).getCity());
//            TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
//            jobExp.setText(""+courcesList.get(position).getLocation());
//            TextView keySkills = (TextView)convertView.findViewById(R.id.contactPerson);
//            keySkills.setText(""+courcesList.get(position).getContactNumber());
//            TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
//            courseFee.setText(""+courcesList.get(position).getCourseFee());
//            return convertView;
//        }
//
//
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return courcesList.size();
//        }
//
//
//        @Override
//        public long getItemId(int arg0) {
//            // TODO Auto-generated method stub
//            return 0;
//        }
//
//        @Override
//        public Object getItem(int position) {
//            // TODO Auto-generated method stub
//            return courcesList.get(position);
//        }
//
//    }

}
