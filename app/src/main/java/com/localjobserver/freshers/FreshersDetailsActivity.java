package com.localjobserver.freshers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localjobserver.models.FreshersData;
import co.talentzing.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class FreshersDetailsActivity extends ActionBarActivity {

    private ViewPager mPager;
    private ArrayList<FreshersData> freshersDatas;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private Intent receiverIntent = null;
    private int selectPos = 0;
    private FreshersDetailsFragment selecedFragment = null;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freshers_details);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Course Information");
        actionBar.setCustomView(R.layout.action_title);
        receiverIntent = getIntent();
        selectPos = receiverIntent.getExtras().getInt("selectedPos");

        freshersDatas = getIntent().getParcelableArrayListExtra("courceItems");
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(selectPos);
        mPager.setOffscreenPageLimit(1);
        selecedFragment = mPagerAdapter.getFragment(selectPos);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_freshers_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id ==android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<WeakReference<FreshersDetailsFragment>> mPageReferenceMap = new SparseArray<WeakReference<FreshersDetailsFragment>>();
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return getFragment(position);
        }

        @Override
        public int getItemPosition(Object object){
            return ScreenSlidePagerAdapter.POSITION_NONE;
        }
        @Override
        public int getCount() {
            return freshersDatas.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            if(object != null){
                return ((Fragment)object).getView() == view;
            }else{
                return false;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mPageReferenceMap.remove(Integer.valueOf(position));
            super.destroyItem(container, position, object);
        }


        public Object instantiateItem(ViewGroup container, int position) {
            FreshersDetailsFragment fragment = new FreshersDetailsFragment();

            // Attach some data to it that we'll
            // use to populate our fragment layouts
            Bundle args = new Bundle();
            args.putParcelable("data", freshersDatas.get(position));

            // Set the arguments on the fragment
            // that will be fetched in DemoFragment@onCreateView
            fragment.setArguments(args);;
            mPageReferenceMap.put(Integer.valueOf(position), new WeakReference<FreshersDetailsFragment>(fragment));
            return super.instantiateItem(container, position);
        }

        public FreshersDetailsFragment getFragment(int key) {
            WeakReference<FreshersDetailsFragment> weakReference = mPageReferenceMap.get(key);
            if (null != weakReference) {
                return (FreshersDetailsFragment) weakReference.get();
            }
            else {
                return null;
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FreshersDetailsFragment extends Fragment {
        private View rootView;
        private FreshersData freshersData;
        private TextView institutionNAME, city, courceNAME,
                eligibility, instructorName, email, contactNum,
                contactLandNum, startDate, endDate, duration, courceFee, discountType, discountValue, addr, conctPerson, compUrl, startTime, endTime, venue;


        public FreshersDetailsFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                freshersData = getArguments().getParcelable("data");
            }
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_freshers_details, container, false);
            initializeUi();
            bindData();
            return rootView;
        }

        public void initializeUi() {
            institutionNAME = (TextView) rootView.findViewById(R.id.institutionNAME);
            city = (TextView) rootView.findViewById(R.id.city);
            courceNAME = (TextView) rootView.findViewById(R.id.courceNAME);
            eligibility = (TextView) rootView.findViewById(R.id.eligibility);
            instructorName = (TextView) rootView.findViewById(R.id.instructorName);
            email = (TextView) rootView.findViewById(R.id.email);
            contactNum = (TextView) rootView.findViewById(R.id.contactNum);
            contactLandNum = (TextView) rootView.findViewById(R.id.contactLandNum);
            startDate = (TextView) rootView.findViewById(R.id.startDate);
            endDate = (TextView) rootView.findViewById(R.id.endDate);
            duration = (TextView) rootView.findViewById(R.id.duration);
            courceFee = (TextView) rootView.findViewById(R.id.courceFee);
            discountType = (TextView) rootView.findViewById(R.id.discountType);
            discountValue = (TextView) rootView.findViewById(R.id.discountValue);
            addr = (TextView) rootView.findViewById(R.id.addr);
            conctPerson = (TextView) rootView.findViewById(R.id.conctPerson);
            compUrl = (TextView) rootView.findViewById(R.id.compUrl);
            startTime = (TextView) rootView.findViewById(R.id.startTime);
            endTime = (TextView) rootView.findViewById(R.id.endTime);
            venue = (TextView) rootView.findViewById(R.id.venue);
        }

        public void bindData() {
            institutionNAME.setText("" + freshersData.getInstitutionName());
            city.setText("" + freshersData.getCity());
            courceNAME.setText("" + freshersData.getCourceName());
            eligibility.setText("" + freshersData.getEligibility());
            instructorName.setText("" + freshersData.getInstruvtorName());
            email.setText("" + freshersData.getEmail());
            contactNum.setText("" + freshersData.getFreConNum());
            contactLandNum.setText("" + freshersData.getLandNum());
            startDate.setText("" + freshersData.getStartDate());
            endDate.setText("" + freshersData.getEndDate());
            duration.setText("" + freshersData.getDuration());
            courceFee.setText("" + freshersData.getCourceFee());
            discountType.setText("" + freshersData.getDiscountType());
            discountValue.setText("" + freshersData.getDiscountValue());
            addr.setText("" + freshersData.getAddress());
            conctPerson.setText("" + freshersData.getContactPname());
            compUrl.setText("" + freshersData.getCompURL());
            startTime.setText("" + freshersData.getStartTime());
            endTime.setText("" + freshersData.getEndTime());
            venue.setText("" + freshersData.getVenue());
        }
    }
}
