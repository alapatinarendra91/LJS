package com.localjobserver.freshers;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonArrayAdapterDropDown;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.models.FresherRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

public class FreshersProfileActivity extends ActionBarActivity {
    private ActionBar actionBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freshers_profile);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Profile Information");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FreshersProfileFragment())
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class FreshersProfileFragment extends Fragment {

        private EditText fnameEdt,lnameEdt,emailEdt,contactNumEdt,landNumEdt,passwordEdt,confirmpasswordEdt,collegeNameEdt
                , bDate;
        private MultiAutoCompleteTextView keySkillsEdt;
        private RadioGroup radioSex;
        private RadioButton radioMaleButton, radioFeMaleButton;
        private Button submitBtn,cancel;
        private ProgressDialog progressDialog;
        private View flipper;
        private Spinner eduSp,locationNameEdt;
        private  FresherRegistrationModel FresherProfileList;
        private FresherRegistrationModel mDataModel;
        private LinkedHashMap<String, String> educationMap;
        private Calendar myCalendar = Calendar.getInstance();
        private ArrayList<HashMap> morderHashMapList;
        private LinkedHashMap<String, String> locationsMap;
        public ArrayList<String> keySkillsList = null;

        public FreshersProfileFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            flipper = inflater.inflate(R.layout.freshers_profile_fragment, container, false);

            initViews();
            setViews();
            getEducations();
            getLocations();
            getKeyWords();

            return flipper;
        }

        private void initViews() {
            fnameEdt  = (EditText)flipper.findViewById(R.id.fnameEdt);
            lnameEdt = (EditText)flipper.findViewById(R.id.lnameEdt);
            emailEdt = (EditText)flipper.findViewById(R.id.emailEdt);
            contactNumEdt  = (EditText)flipper.findViewById(R.id.contactNumEdt);
            landNumEdt  = (EditText)flipper.findViewById(R.id.landNumEdt);
            passwordEdt = (EditText)flipper.findViewById(R.id.passwordEdt);
            confirmpasswordEdt  = (EditText)flipper.findViewById(R.id.confirmpasswordEdt);
            collegeNameEdt  = (EditText)flipper.findViewById(R.id.collegeNameEdt);
            locationNameEdt = (Spinner)flipper.findViewById(R.id.locationNameEdt);
            keySkillsEdt = (MultiAutoCompleteTextView)flipper.findViewById(R.id.keySkillsEdt);
            keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            bDate  = (EditText)flipper.findViewById(R.id.dateOfBirth);

            radioMaleButton = (RadioButton)flipper.findViewById(R.id.radioMale);
            radioFeMaleButton = (RadioButton)flipper.findViewById(R.id.radioFemale);
            radioSex = (RadioGroup)flipper.findViewById(R.id.radioSex);

            eduSp = (Spinner)flipper.findViewById(R.id.eduSp);

            submitBtn = (Button)flipper.findViewById(R.id.submitBtn);
            cancel = (Button)flipper.findViewById(R.id.cancel);


        }

        private void setViews() {

            FresherProfileList = DatabaseAccessObject.getFresherRegistrationDetails(Queries.getInstance().getDetails("FreshersRegistration"),getActivity());

            fnameEdt.setText(""+FresherProfileList.getFirstName());
            lnameEdt.setText(""+FresherProfileList.getLastName());
            emailEdt.setText(""+FresherProfileList.getEmail());
            contactNumEdt.setText(""+FresherProfileList.getContactNo());
            keySkillsEdt.setText(""+FresherProfileList.getKeySkills());
            bDate.setText(""+CommonUtils.getCleanDate(FresherProfileList.getDateOfBirth()));
            if(FresherProfileList.getGender().equalsIgnoreCase("Male"))
                radioSex.check(R.id.radioMale);
            else
                radioSex.check(R.id.radioFemale);

            submitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setFresherDatatoModel();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().finish();
                }
            });

            bDate.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

        }

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        private void updateLabel() {

            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            bDate.setText(sdf.format(myCalendar.getTime()));
        }

        private void setFresherDatatoModel() {

            mDataModel = new FresherRegistrationModel();
            mDataModel.setFirstName(fnameEdt.getText().toString());
            mDataModel.setLastName(lnameEdt.getText().toString());
            mDataModel.setEducation(eduSp.getSelectedItem().toString());
            mDataModel.setPassword(FresherProfileList.getPassword());
            mDataModel.setConfirmPassword(FresherProfileList.getConfirmPassword());
            mDataModel.setEmail(emailEdt.getText().toString());
            mDataModel.setContactNo(contactNumEdt.getText().toString());
//            mDataModel.setLandLine(landNumEdt.getText().toString());
            if (radioMaleButton.isChecked()){
                mDataModel.setGender("Male");
            }else{
                mDataModel.setGender("FeMale");
            }
            mDataModel.setDateOfBirth(bDate.getText().toString());
            mDataModel.setLocation(locationNameEdt.getSelectedItem().toString());
            mDataModel.setKeySkills(keySkillsEdt.getText().toString());

            toFresherUpdateDerailsUpload();

        }

        private void toFresherUpdateDerailsUpload() {
             morderHashMapList = new ArrayList<HashMap>();
            HashMap RecuiterRegDataMap = new HashMap();

//            LinkedHashMap RecuiterRegDataMap = new LinkedHashMap();
            RecuiterRegDataMap.put("FirstName", mDataModel.getFirstName());
            RecuiterRegDataMap.put("LastName", mDataModel.getLastName());
            RecuiterRegDataMap.put("Email", mDataModel.getEmail());
            RecuiterRegDataMap.put("Education", mDataModel.getEducation());
            RecuiterRegDataMap.put("DateOfBirth", mDataModel.getDateOfBirth());
            RecuiterRegDataMap.put("KeySkills", mDataModel.getKeySkills());
            RecuiterRegDataMap.put("Location", mDataModel.getLocation());
            RecuiterRegDataMap.put("Gender", mDataModel.getGender());
            RecuiterRegDataMap.put("ContactNo", mDataModel.getContactNo());

            morderHashMapList.add(RecuiterRegDataMap);
            /*String whereCondition=" where Email='"+ emailEdt.getText().toString().trim()+"'";
            DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
            dbHelper.updateData("FreshersRegistration", morderHashMapList, whereCondition, getActivity());*/

            updateFresharsProfile(RecuiterRegDataMap);
        }

        private void updateFresharsProfile(HashMap<String,String> input){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            JSONObject json = new JSONObject(input);
            String requestString = json.toString();
            requestString = CommonUtils.encodeURL(requestString);
            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        // if (result != null) {
                        Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                        //  }
                        return;
                    }

                    if (result != null) {
                        if (result.toString().equalsIgnoreCase("true")) {

                            String whereCondition = " where Email='" + emailEdt.getText().toString().trim() + "'";
                            DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
                            dbHelper.updateData("FreshersRegistration", morderHashMapList, whereCondition, getActivity());

                            Toast.makeText(getActivity(), "Profile Details updated successfully", Toast.LENGTH_SHORT).show();
                            Intent uiUpdateIntent = new Intent("updateProfilePick_Fresher");
                            uiUpdateIntent.putExtra("ProfileName", fnameEdt.getText().toString());
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

                        }
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.modifyFreshersDetails, CommonUtils.getUserEmail(getActivity()), "" + requestString));
        }

        public void getKeyWords(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }

                    if (result != null) {
                        if (null != keySkillsList && keySkillsList.size() > 0) {
                            keySkillsEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));

                        }
                        progressDialog.dismiss();
                    }
                }
            });
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {

                            }
                        }
                    });
                }
            });

        }

        public void getLocations() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        locationsMap = (LinkedHashMap<String, String>) result;
                        if (null != locationsMap && null != locationNameEdt) {

                            String[] jobType_array = CommonUtils.fromMap(locationsMap, "Location");
                            locationNameEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), jobType_array));
                            for(int i=0 ; i <jobType_array.length ; i++)
                            {
                                if (jobType_array[i].contains(FresherProfileList.getLocation())) {
                                    locationNameEdt.setSelection(i);
                                }
                            }

                        } else {
                            Toast.makeText(getActivity(), "Not able to get education details", Toast.LENGTH_SHORT).show();
                        }

                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }

        public void getEducations(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        educationMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
//                        messageToParent.what = RESULT_EDUCATION;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
//                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                        if (null != educationMap && null != eduSp) {

                            String[] jobType_array = CommonUtils.fromMap(educationMap, "Education");
                            eduSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), jobType_array));
                            for(int i=0 ; i <jobType_array.length ; i++)
                            {
                                if (jobType_array[i].contains(FresherProfileList.getEducation())) {
                                    eduSp.setSelection(i);
                                }
                            }

                        } else {
                            Toast.makeText(getActivity(), "Not able to get education details", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
        }


    }

}
