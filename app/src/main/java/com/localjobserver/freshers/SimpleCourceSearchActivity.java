package com.localjobserver.freshers;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;

public class SimpleCourceSearchActivity extends ActionBarActivity {
    private ActionBar actionBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_cource_search);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Simple Search");

        Bundle bundle = new Bundle();
        SimpleCourceSearch fragInfo = new SimpleCourceSearch();
        fragInfo.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, fragInfo)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
