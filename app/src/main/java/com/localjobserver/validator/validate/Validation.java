package com.localjobserver.validator.validate;

 
public interface Validation {

    String getErrorMessage();

    boolean isValid(String text);

}