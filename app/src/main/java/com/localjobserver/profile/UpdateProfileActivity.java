package com.localjobserver.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.localjobserver.commonutils.Queries;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.SetupData;

import java.util.List;

/**
 */
public class UpdateProfileActivity extends ActionBarActivity {
    private ActionBar actionBar = null;
    public List<SetupData> jobSeekerProfileList;
    public SetupData mSetupData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_profile);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Update Information");

        mSetupData = new SetupData();
        getProfileDate();

        Fragment newFragment = MainProfileFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, newFragment).addToBackStack(null).commit();

    }

    public  void getProfileDate() {
        getCompleteProfile();
        setDataToObject();
    }


    private void getCompleteProfile(){
        jobSeekerProfileList = DatabaseAccessObject.getJobSeekerProfle(Queries.getInstance().getDetails("JobSeekers"), UpdateProfileActivity.this);
    }

    private void setDataToObject(){
        SetupData mJobListData = jobSeekerProfileList.get(0);
//        CommonUtils.showToast("is "+mJobListData.getDateOfBirth(),getApplicationContext());
        mSetupData.setfName(mJobListData.getfName());
        mSetupData.setlName(mJobListData.getlName());
        mSetupData.setEmail(mJobListData.getEmail());
        mSetupData.setContactNum(mJobListData.getContactNum());
        mSetupData.setLandNum(mJobListData.getLandNum());
        mSetupData.setPassword(mJobListData.getPassword());
        mSetupData.setConfirmPass(mJobListData.getConfirmPass());
        mSetupData.setDateOfBirth(mJobListData.getDateOfBirth());
        mSetupData.setlanguagesKnown(mJobListData.getlanguagesKnown());
        mSetupData.setGender(mJobListData.getGender());
        mSetupData.setCurrentIndestry(mJobListData.getCurrentIndestry());
//        Log.i("", "Poffessional Details is updatee: " + mJobListData.getCurrentIndestry());
        mSetupData.setCurrentCompany(mJobListData.getCurrentCompany());
        mSetupData.setRole(mJobListData.getRole());
        mSetupData.setCurrentDisgnation(mJobListData.getCurrentDisgnation());
        mSetupData.setTotExp(mJobListData.getTotExp());
        mSetupData.setCurrentCtc(mJobListData.getCurrentCtc());
        mSetupData.setExpectedCtc(mJobListData.getExpectedCtc());
        mSetupData.setKeySkills(mJobListData.getKeySkills());
        mSetupData.setCurrentLocation(mJobListData.getCurrentLocation());
        mSetupData.setPreferedLocation(mJobListData.getPreferedLocation());
        mSetupData.setNoticePeriod(mJobListData.getNoticePeriod());
        mSetupData.setEduCourse(mJobListData.getEduCourse());
        mSetupData.setInstitution(mJobListData.getInstitution());
        mSetupData.setYearPassing(mJobListData.getYearPassing());
        mSetupData.setResumeHeadLine(mJobListData.getResumeHeadLine());
        mSetupData.setInId(mJobListData.getInId());
        mSetupData.setJobtype(mJobListData.getJobtype());
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_profile, menu);
//        return super.onCreateOptionsMenu(menu);

//    }


        @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){

            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }else{
                    this.finish();
                }
                break;

        }
        return true;

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

