package com.localjobserver.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.ui.ProfileActivity;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.SetupData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfessionalDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfessionalDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog progressDialog;
    private MultiSpinnerSearch currentIndeSpin,roleSpn,currentLocationSpn;
    private Spinner jobTypeSpn,expSpn,totexpmonths,totCtcSp,totThCtcSp,totExpCtcSp,totThExceptCtcSp,notiSp;
    private EditText resumeEdt;
    private AutoCompleteTextView curDesigEdt,currentCmpEdt;
    private MultiAutoCompleteTextView keySkillsEdt,preferredLocationEdt;
    private Button updateprofessional,cancelprofessional;
    private List<SetupData> professionalDetailsList;
    public ArrayList<String> keySkillsList = null;
    public LinkedHashMap<String,String> industries = null,locationsMap = null,  roleMap,
            cuttentLocMap,noticeMap, educationMap,institutionMap,yearMap,disigntionMap = null,companiesMap;

    private static final int RESULT_KEYSKILLS = 1;
    private static final int RESULT_INDUSTRIES = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_JOBTYPE = 4;
    private static final int RESULT_EXPERIENCE = 5;
    private static final int RESULT_NOTICEPERIED = 6;
    private static final int RESULT_EDUCATION = 7;
    private static final int RESULT_INSTITUTION = 8;
    private static final int RESULT_YEAR = 9;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_ROLES = 10;
    private static final int RESULT_DISIGNATIONS = 11;
    private static final int RESULT_USERDATA = 12;
    private static final int RESULT_COMPANIES = 13;
    private StringTokenizer tokens ;
    private String lacks;
    private String thousands ;
    private String selectedInd;
    private  boolean isClassVisible = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.exce
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfessionalDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfessionalDetailsFragment newInstance(String param1, String param2) {
        ProfessionalDetailsFragment fragment = new ProfessionalDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfessionalDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_professional_profile, container, false);
        professionalDetailsList = ((UpdateProfileActivity)getActivity()).jobSeekerProfileList;

        Log.i("","Poffessional Details is : "+professionalDetailsList.get(0).getCurrentIndestry());

        currentIndeSpin = (MultiSpinnerSearch)rootView.findViewById(R.id.currentIndeSpin);
        roleSpn = (MultiSpinnerSearch)rootView.findViewById(R.id.roleSp);
        jobTypeSpn = (Spinner)rootView.findViewById(R.id.jobTypeSp);
        expSpn = (Spinner)rootView.findViewById(R.id.expSp);
        totexpmonths = (Spinner)rootView.findViewById(R.id.totexpmonths);
        currentLocationSpn = (MultiSpinnerSearch)rootView.findViewById(R.id.currrentLocSp);
        totCtcSp = (Spinner)rootView.findViewById(R.id.totCtcSp);
        totThCtcSp = (Spinner)rootView.findViewById(R.id.totThCtcSp);
        totExpCtcSp = (Spinner)rootView.findViewById(R.id.totExpCtcSp);
        totThExceptCtcSp = (Spinner)rootView.findViewById(R.id.totThExceptCtcSp);
        notiSp = (Spinner)rootView.findViewById(R.id.notiSp);

        curDesigEdt = (AutoCompleteTextView)rootView.findViewById(R.id.currentDisEdt);
        keySkillsEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt);
        keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        resumeEdt = (EditText)rootView.findViewById(R.id.resumeHeadEdt);
        currentCmpEdt = (AutoCompleteTextView)rootView.findViewById(R.id.currentCompEdt);
        preferredLocationEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.preLocEdt);
        preferredLocationEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        cancelprofessional = (Button) rootView.findViewById(R.id.cancelprofessional);
        updateprofessional = (Button) rootView.findViewById(R.id.updateprofessional);
        updateprofessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateProfessionalDetails() && spinnervalidateProfessionalDetails())
                toPersonalDerailsUpload();
            }
        });
        cancelprofessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });;

        resumeEdt.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (expSpn.getSelectedItemId() == 0)
                    resumeEdt.setText(0+"+ years of experience on" +keySkillsEdt.getText().toString());
                else
                    resumeEdt.setText(expSpn.getSelectedItemPosition()-1+"+ years of experience on" +keySkillsEdt.getText().toString());

                return false;
            }
        });

        currentIndeSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (null != industries && currentIndeSpin.getSelectedIds().size() > 0){
//                    selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
//                    getRoles(selectedInd);

                    selectedInd = String.valueOf(currentIndeSpin.getSelectedIds().get(0));
                    getRoles(selectedInd);


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bindData();
        return rootView;
    }

    public boolean validateProfessionalDetails(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using((AutoCompleteTextView) rootView.findViewById(R.id.currentDisEdt)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(preferredLocationEdt).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.resumeHeadEdt)).validate(NotEmpty.build(getActivity())));
        return (mForm.isValid()) ? true : false;
    }

    public  boolean spinnervalidateProfessionalDetails(){

        if (!CommonUtils.spinnerSelect("Current Industry",currentIndeSpin.getSelectedIds().size(),getActivity()) ||
                !CommonUtils.spinnerSelect("Role",roleSpn.getSelectedIds().size(),getActivity())||
                !CommonUtils.spinnerSelect("Job Type",jobTypeSpn.getSelectedItemPosition(),getActivity())||
                !CommonUtils.spinnerSelect("Total Experience years",expSpn.getSelectedItemPosition(),getActivity())||
                !CommonUtils.spinnerSelect("Total Experience months",totexpmonths.getSelectedItemPosition(),getActivity())||
                !CommonUtils.spinnerSelect("Annual salary Lakhs",totCtcSp.getSelectedItemPosition(),getActivity())||
                !CommonUtils.spinnerSelect("Annual salary Thousand",totThCtcSp.getSelectedItemPosition(),getActivity())||
//                !CommonUtils.spinnerSelect("Expected salary Lakhs",totExpCtcSp.getSelectedItemPosition(),getActivity())||
//                !CommonUtils.spinnerSelect("Expected salary Thousand",totThExceptCtcSp.getSelectedItemPosition(),getActivity())||
                !CommonUtils.spinnerSelect("Current Location",currentLocationSpn.getSelectedIds().size(),getActivity())||
                !CommonUtils.spinnerSelect("Notice period",notiSp.getSelectedItemPosition(),getActivity())
                ){
            return false;
        }

        return true;
    }

    public void toPersonalDerailsUpload(){
        List<HashMap> morderHashMapList = new ArrayList<HashMap>();

        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put(CommonKeys.LJS_Industry,""+currentIndeSpin.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_Role,""+roleSpn.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_InId,""+selectedInd);
        seekerDataMap.put(CommonKeys.LJS_Jobtype,""+jobTypeSpn.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_CurrentDesignation,""+curDesigEdt.getText().toString());
        seekerDataMap.put(CommonKeys.LJS_Experience, "" + expSpn.getSelectedItem().toString() + "." + totexpmonths.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_CurrentCTC,""+totCtcSp.getSelectedItem().toString()+"."+totThCtcSp.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_expectedCTC,""+totExpCtcSp.getSelectedItem().toString()+"."+totThExceptCtcSp.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_KeySkills,""+keySkillsEdt.getText().toString());
        seekerDataMap.put(CommonKeys.LJS_ResumeHeadLine,""+resumeEdt.getText().toString());
        seekerDataMap.put(CommonKeys.LJS_CurrentCompany,""+currentCmpEdt.getText().toString());
        seekerDataMap.put(CommonKeys.LJS_Location,""+currentLocationSpn.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_PreferredLocation,""+preferredLocationEdt.getText().toString());
        seekerDataMap.put(CommonKeys.LJS_NoticePeriod,""+CommonUtils.convertToDays(notiSp.getSelectedItem().toString()));
        seekerDataMap.put("UpdateOn", "" + CommonUtils.getDateTime());


        morderHashMapList.add(seekerDataMap);
        String whereCondition=" where Email='"+CommonUtils.getUserEmail(getActivity())+"'";
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        dbHelper.updateData("JobSeekers", morderHashMapList, whereCondition, getActivity());
        updateJobSeekerProfile(seekerDataMap);
    }


    private void updateJobSeekerProfile(LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(getActivity(), "Professional Details updated successfully", Toast.LENGTH_SHORT).show();
                        ProfileActivity.tvkeyskills.setText(keySkillsEdt.getText().toString());
//                        ProfileActivity.tvresumeheadline.setText(resumeEdt.getText().toString());
                        if (expSpn.getSelectedItemId() == 0)
                            ProfileActivity.tvresumeheadline.setText(0+"+ years of experience on" +keySkillsEdt.getText().toString());
                        else
                            ProfileActivity.tvresumeheadline.setText(expSpn.getSelectedItemPosition()-1+"+ years of experience on" +keySkillsEdt.getText().toString());
                        ProfileActivity.tvexperience.setText(expSpn.getSelectedItem().toString()+"."+totexpmonths.getSelectedItem().toString() + " year(s)");
                        ProfileActivity.tvlatupdateddate.setText("Last Update: "+CommonUtils.getDateTime());
                        getActivity().finish();
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyJSDetails, CommonUtils.getUserEmail(getActivity()), "" + requestString));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    private void bindData(){
        curDesigEdt.setText(professionalDetailsList.get(0).getCurrentDisgnation());
        keySkillsEdt.setText(professionalDetailsList.get(0).getKeySkills());
        resumeEdt.setText(professionalDetailsList.get(0).getResumeHeadLine());
        currentCmpEdt.setText(professionalDetailsList.get(0).getCurrentCompany());
        preferredLocationEdt.setText(professionalDetailsList.get(0).getPreferedLocation());

        jobTypeSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),getActivity().getResources().getStringArray(R.array.positiontype)));
        String[] jobType_array = getResources().getStringArray(R.array.positiontype);
        for(int i=0 ; i <getResources().getStringArray(R.array.positiontype).length ; i++)
        {
            if (jobType_array[i].contains(professionalDetailsList.get(0).getJobtype())) {
                jobTypeSpn.setSelection(i);
            }
        }

        expSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        totexpmonths.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.months)));
        String[] expArr = null;

        if ( professionalDetailsList.get(0).getExperience().contains(".")) {
            expArr = professionalDetailsList.get(0).getExperience().split("\\.");
            if (null != expArr) {
                if (expArr.length == 2) {
                    lacks = expArr[0];
                    thousands = expArr[1];
                } else if (expArr.length == 1) {
                    lacks = expArr[0];
                    thousands = "0";
                }
            }
        } else {
            lacks = (null != professionalDetailsList.get(0).getExperience()) ? professionalDetailsList.get(0).getExperience() : "0";
        }

        expSpn.setSelection(Integer.parseInt((null != lacks) ? lacks : "0")+ 1 );
        totexpmonths.setSelection(Integer.parseInt((null != thousands) ? thousands : "0")+ 1 );

        totCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.getGenericArrayValues("", 100)));
        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.getGenericArrayValues("", 100)));
        String[] ctcArr = null;
        if ( professionalDetailsList.get(0).getCurrentCtc().contains(".")) {
            ctcArr = professionalDetailsList.get(0).getCurrentCtc().split("\\.");
            if (null != ctcArr) {
                if (ctcArr.length == 2) {
                    lacks = ctcArr[0];
                    thousands = ctcArr[1];
                } else if (ctcArr.length == 1) {
                    lacks = ctcArr[0];
                    thousands = "0";
                }
            }
        } else {
            lacks = (null != professionalDetailsList.get(0).getCurrentCtc()) ? professionalDetailsList.get(0).getCurrentCtc() : "0";
        }


        totCtcSp.setSelection(Integer.parseInt((null != lacks) ? lacks : "0") + 1);
        totThCtcSp.setSelection(Integer.parseInt((null != thousands) ? thousands : "0") + 1);

        totCtcSp.setOnItemSelectedListener(spinListener);
        expSpn.setOnItemSelectedListener(spinListener);

        totExpCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.getGenericArrayValues("", 100)));
        totThExceptCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.getGenericArrayValues("", 100)));

        String[] exceptedctcArr = null;
        if ( professionalDetailsList.get(0).getExpectedCtc().contains(".")) {
            exceptedctcArr = professionalDetailsList.get(0).getExpectedCtc().split("\\.");
            if (null != exceptedctcArr) {
                if (exceptedctcArr.length == 2) {
                    lacks = exceptedctcArr[0];
                    thousands = exceptedctcArr[1];
                } else if (exceptedctcArr.length == 1) {
                    lacks = exceptedctcArr[0];
                    thousands = "0";
                }
            }
        } else {
            lacks = (null != professionalDetailsList.get(0).getExpectedCtc()) ? professionalDetailsList.get(0).getExpectedCtc() : "0";
        }

        totExpCtcSp.setSelection(Integer.parseInt((null != lacks) ? lacks : "0") + 1);
        totThExceptCtcSp.setSelection(Integer.parseInt((null != thousands) ? thousands : "0") + 1);


        notiSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),getActivity().getResources().getStringArray(R.array.noticeperiod)));
        String[] jobType_notice = getResources().getStringArray(R.array.noticeperiod);
        for(int i=0 ; i <jobType_notice.length ; i++)
        {
            if (jobType_notice[i].contains(CommonUtils.convertToWeeks(professionalDetailsList.get(0).getNoticePeriod()))) {
                notiSp.setSelection(i);
            }
        }

        getIndustries();
        getDisegnation();
        getLocations();
        getCompanies();
    }



    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String,String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INDUSTRIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public void getRoles(final String selectedInd){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    roleMap = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_ROLES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getRoles,selectedInd), CommonKeys.arrRoles);
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Getting keyskills..");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();

                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                }
            }
        });
    }

    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }

    public void getCompanies(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
//                        progressDialog.dismiss();
                    companiesMap = (LinkedHashMap<String,String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_COMPANIES;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_EDUCATION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }

    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_LOCATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }


    public void getDisegnation(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    disigntionMap = (LinkedHashMap<String,String>)result;
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_DISIGNATIONS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getDesignations),CommonKeys.arrDesignations);
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){

                case R.id.totCtcSp:
                    if (totCtcSp.getSelectedItemId() == 100){
                        totExpCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totThExceptCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totExpCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totCtcSp.getSelectedItemId() != 0){
                        totExpCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSp.getSelectedItem().toString())+1, 100)));
                        totThExceptCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSp.getSelectedItem().toString())+2, 100)));
                        totExpCtcSp.setOnItemSelectedListener(spinListener);
                    }

                    break;

                case R.id.totExpCtcSp:
                    if (totExpCtcSp.getSelectedItemId() == 100){
                        totThExceptCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totThExceptCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totExpCtcSp.getSelectedItemId() != 0){
                        totThExceptCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totExpCtcSp.getSelectedItem().toString())+1, 100)));
                        totThExceptCtcSp.setOnItemSelectedListener(spinListener);
                    }

                    break;

                case R.id.expSp:
                    resumeEdt.setText(expSpn.getSelectedItem().toString()+"+ years of experience on" +keySkillsEdt.getText().toString());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    class StatusHandler extends Handler {

        public void handleMessage(android.os.Message msg)
        {
            switch (msg.what){
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occured while getting data from  server.", Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_INDUSTRIES:
                    if (null != industries && isClassVisible == true) {
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndeSpin,industries,0,professionalDetailsList.get(0).getCurrentIndestry());

                    }else{
                        Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_LOCATIONS:
                    if (null != locationsMap && isClassVisible == true) {
//                        String[] jobType_array = CommonUtils.fromMap(locationsMap);
//                        Arrays.sort(jobType_array);
//                        currentLocationSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(locationsMap, "Location")));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentLocationSpn,locationsMap,0,professionalDetailsList.get(0).getCurrentLocation());
                        preferredLocationEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(locationsMap, "Location")));
//                        for(int i=0 ; i <CommonUtils.fromMap(locationsMap, "Location").length ; i++)
//                        {
//                            if (CommonUtils.fromMap(locationsMap, "Location")[i].contains(professionalDetailsList.get(0).getCurrentLocation())) {
//                                currentLocationSpn.setSelection(i);
//                            }
//                        }

                    }else{
                        Toast.makeText(getActivity(),"Not able to get locations",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_JOBTYPE:
//                        cuttentLocSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(jobTypeMap)));
                    break;
                case RESULT_NOTICEPERIED:
                    break;
                case RESULT_YEAR:
                    expSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(yearMap, "Year")));
                    resumeEdt.setText(expSpn.getSelectedItemPosition()-1+"+ years of experience on" +keySkillsEdt.getText().toString());
                    break;
                case RESULT_DISIGNATIONS:
                    if (null != disigntionMap&& isClassVisible == true) {
                        String[] jobType_array = CommonUtils.fromMap(disigntionMap, "Disignation");
//                        Arrays.sort(jobType_array);
                        curDesigEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, jobType_array));
                    }
                    break;

                case RESULT_ROLES:
//                    roleSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                    getKeyWords();
                    if (null != roleMap && isClassVisible == true) {
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpn,roleMap,0,professionalDetailsList.get(0).getRole());
//                        String[] jobType_array = CommonUtils.fromMap(roleMap, "Role");
////                        Arrays.sort(jobType_array);
//                        roleSpn.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, jobType_array));
//
//                        for (int i = 0; i < jobType_array.length; i++) {
//                            if (jobType_array[i].equals(professionalDetailsList.get(0).getRole())) {
//                                roleSpn.setSelection(i);
//                            }
//                        }
                    }
                    break;
                case RESULT_COMPANIES:
                    if (null != companiesMap && null != currentCmpEdt && isClassVisible == true) {
                        currentCmpEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(companiesMap, "Company")));
                    }
                    break;
                case RESULT_KEYSKILLS:
                    if ( isClassVisible == true)
                        keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
//                    getDisegnation();
                    break;

                /*case  RESULT_USERDATA:
                    showYesNoDialog();
                    break;*/
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
