package com.localjobserver.profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.localjobserver.ui.ProfileActivity;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.models.SetupData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.IsPositiveInteger;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PersonalDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    private SetupData personalProfileData;
    private SetupData mSetupData;
    private EditText firstNameEdt, lastNameEdt, emailEdt, contactNoEdt, landlineEdt, pwdEdt, cnfPwdEdt,dateOfBirth;
    private RadioGroup genderRdGrp;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog progressDialog;
    private Calendar myCalendar = Calendar.getInstance();
    private MultiAutoCompleteTextView languages;
    private LinkedHashMap<String, String> languageMap;
    private  boolean isClassVisible = false;

    private Button cancelBtn;
    private Button updateBtn;

    //***************************Click Listener********************************
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.updatepersonal:
                    if (validatePerssonalDetailsScreens())
                    setSetupData();

                    break;
                case R.id.cancelpersonal:
                    getActivity().finish();
//                    getActivity().getSupportFragmentManager().popBackStack();
//                    Intent in = new Intent(getActivity(),ProfileActivity.class);
//                    startActivity(in);
                    break;
                default:

            }
        }
    };

    public PersonalDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PersonalDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalDetailsFragment newInstance(String param1, String param2) {
        PersonalDetailsFragment fragment = new PersonalDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_personal_profile, container, false);
        cancelBtn = (Button) rootView.findViewById(R.id.cancelpersonal);
        updateBtn = (Button) rootView.findViewById(R.id.updatepersonal);
        updateBtn.setOnClickListener(onClickListener);
        cancelBtn.setOnClickListener(onClickListener);
        personalProfileData = ((UpdateProfileActivity) getActivity()).jobSeekerProfileList.get(0);
        firstNameEdt = (EditText) rootView.findViewById(R.id.fnameEdt);
        lastNameEdt = (EditText) rootView.findViewById(R.id.lnameEdt);
        emailEdt = (EditText) rootView.findViewById(R.id.emailEdt);
        contactNoEdt = (EditText) rootView.findViewById(R.id.contactNumEdt);
        landlineEdt = (EditText) rootView.findViewById(R.id.landNumEdt);
        pwdEdt = (EditText) rootView.findViewById(R.id.passwordEdt);
        cnfPwdEdt = (EditText) rootView.findViewById(R.id.confirmpasswordEdt);
        dateOfBirth = (EditText) rootView.findViewById(R.id.dateOfBirth);
        genderRdGrp = (RadioGroup) rootView.findViewById(R.id.radioSex);
        languages = (MultiAutoCompleteTextView)rootView.findViewById(R.id.languages);
        languages.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        dateOfBirth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mSetupData = ((UpdateProfileActivity) getActivity()).mSetupData;
        getLanguages();
        bindData();

        return rootView;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void getLanguages(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    languageMap = (LinkedHashMap<String, String>) result;
                    if (null != languageMap && isClassVisible == true) {
                        languages.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(languageMap, "Language")));
                    } else {
                        Toast.makeText(getActivity(), "Not able to get Languages", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLanguages), CommonKeys.arrLanguages);
    }

    private void bindData() {
        emailEdt.setFocusable(false);
        firstNameEdt.setText(personalProfileData.getfName());
        lastNameEdt.setText(personalProfileData.getlName());
        emailEdt.setText(personalProfileData.getEmail());
        contactNoEdt.setText(personalProfileData.getContactNum());
        landlineEdt.setText(personalProfileData.getLandNum());
        pwdEdt.setText(personalProfileData.getPassword());
        cnfPwdEdt.setText(personalProfileData.getConfirmPass());
        if (personalProfileData.getDateOfBirth().contains("Date"))
        dateOfBirth.setText(CommonUtils.getCleanDate(personalProfileData.getDateOfBirth()));
        else
            dateOfBirth.setText(personalProfileData.getDateOfBirth());
        languages.setText(personalProfileData.getlanguagesKnown());
        if(personalProfileData.getGender().equalsIgnoreCase("Male"))
            genderRdGrp.check(R.id.radioMale);
        else
            genderRdGrp.check(R.id.radioFemale);
    }

    public boolean validatePerssonalDetailsScreens(){
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using(firstNameEdt).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(lastNameEdt).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using(contactNoEdt).validate(NotEmpty.build(getActivity())).validate(IsPositiveInteger.build(getActivity())));
        if (validateMobileNo()){
            return (mForm.isValid()) ? true : false;
        }

        return (mForm.isValid()) ? false : false;
    }

    public boolean validateMobileNo(){
        if (CommonUtils.isValidMobile(contactNoEdt.getText().toString(),contactNoEdt))
            return true;
        else
            return  false;
    }

    private void setSetupData() {
        mSetupData.setfName(firstNameEdt.getText() != null ? firstNameEdt.getText().toString() : personalProfileData.getfName());
        mSetupData.setlName(lastNameEdt.getText() != null ? lastNameEdt.getText().toString() : personalProfileData.getlName());
        mSetupData.setEmail(emailEdt.getText() != null ? emailEdt.getText().toString() : personalProfileData.getEmail());
        mSetupData.setContactNum(contactNoEdt.getText() != null ? contactNoEdt.getText().toString() : personalProfileData.getContactNum());
        mSetupData.setLandNum(landlineEdt.getText() != null ? landlineEdt.getText().toString() : personalProfileData.getLandNum());
        mSetupData.setPassword(pwdEdt.getText() != null ? pwdEdt.getText().toString() : personalProfileData.getPassword());
        mSetupData.setConfirmPass(cnfPwdEdt.getText() != null ? cnfPwdEdt.getText().toString() : personalProfileData.getConfirmPass());
        mSetupData.setDateOfBirth(dateOfBirth.getText() != null ? dateOfBirth.getText().toString() : personalProfileData.getDateOfBirth());
        mSetupData.setlanguagesKnown(languages.getText() != null ? languages.getText().toString() : personalProfileData.getlanguagesKnown());
        mSetupData.setGender(((RadioButton) rootView.findViewById(genderRdGrp.getCheckedRadioButtonId())).getText().toString());

//        ((ProfileActivity)getActivity()).toUpload();
        toPersonalDerailsUpload();
    }


    public void toPersonalDerailsUpload(){
        List<HashMap> morderHashMapList = new ArrayList<HashMap>();
        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put("FirstName",""+firstNameEdt.getText() != null ? firstNameEdt.getText().toString() : personalProfileData.getfName());
        seekerDataMap.put("LastName", "" + lastNameEdt.getText() != null ? lastNameEdt.getText().toString() : personalProfileData.getlName());
        seekerDataMap.put("ContactNo", "" + contactNoEdt.getText() != null ? contactNoEdt.getText().toString() : personalProfileData.getContactNum());
        seekerDataMap.put("ContactNo_Landline", "" + landlineEdt.getText() != null ? landlineEdt.getText().toString() : personalProfileData.getLandNum());
        seekerDataMap.put("DateOfBirth", "" + dateOfBirth.getText() != null ? dateOfBirth.getText().toString() : personalProfileData.getDateOfBirth());
        seekerDataMap.put("languagesKnown", "" + languages.getText() != null ? languages.getText().toString() : personalProfileData.getlanguagesKnown());
        seekerDataMap.put("Gender", "" + ((RadioButton) rootView.findViewById(genderRdGrp.getCheckedRadioButtonId())).getText().toString());
        seekerDataMap.put("UpdateOn", "" + CommonUtils.getDateTime());

        morderHashMapList.add(seekerDataMap);
        String whereCondition=" where Email='"+mSetupData.getEmail()+"'";
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        dbHelper.updateData("JobSeekers", morderHashMapList, whereCondition, getActivity());
        updateJobSeekerProfile(seekerDataMap);
    }


    private void updateJobSeekerProfile(LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(getActivity(), "Personal Details updated successfully", Toast.LENGTH_SHORT).show();
                        Intent uiUpdateIntent = new Intent("updateProfilePick");
                        uiUpdateIntent.putExtra("ProfileName", firstNameEdt.getText().toString()+lastNameEdt.getText().toString());
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                        ProfileActivity.tvname.setText(firstNameEdt.getText().toString() +" "+lastNameEdt.getText().toString());
                        ProfileActivity.tvlatupdateddate.setText("Last Update: "+CommonUtils.getDateTime());

//                        startActivity(new Intent(getActivity(), ProfileActivity.class));
                        getActivity().finish();

                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyJSDetails, emailEdt.getText().toString(), "" + requestString));
    }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }

}
