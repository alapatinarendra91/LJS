package com.localjobserver.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.ui.ProfileActivity;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.models.SetupData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link EducationDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EducationDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog progressDialog;
    private MultiSpinnerSearch inSpn;
    private List<SetupData> educationList;
    private Spinner eduSpn,ypSpn;
    private Button updateeducation,canceleducation;
    public LinkedHashMap<String,String> institutionMap,educationMap;
    private static final int RESULT_INSTITUTION = 1;
    private static final int RESULT_EDUCATION = 2;
    private  boolean isClassVisible = false;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EducationDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EducationDetailsFragment newInstance(String param1, String param2) {
        EducationDetailsFragment fragment = new EducationDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public EducationDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        educationList = ((UpdateProfileActivity)getActivity()).jobSeekerProfileList;

        Log.i("", "education Details is : " + educationList.get(0).getEduCourse());

        rootView = inflater.inflate(R.layout.fragment_education_profile, container, false);
        educationList = ((UpdateProfileActivity)getActivity()).jobSeekerProfileList;
        eduSpn = (Spinner)rootView.findViewById(R.id.eduSp);
        inSpn = (MultiSpinnerSearch)rootView.findViewById(R.id.qualificationSp);
        ypSpn = (Spinner)rootView.findViewById(R.id.yearofSp);
        canceleducation = (Button) rootView.findViewById(R.id.canceleducation);
        updateeducation = (Button) rootView.findViewById(R.id.updateeducation);
        updateeducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinnerEducationDetails())
                toPersonalDerailsUpload();
            }
        });
        canceleducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });;

        BindData();
        getInstitutions();
        getEducations();
        return rootView;
    }

    public  boolean spinnerEducationDetails(){
        if (!CommonUtils.spinnerSelect("Education",eduSpn.getSelectedItemPosition(),getActivity()) ||
                !CommonUtils.spinnerSelect("Institute",inSpn.getSelectedIds().size(),getActivity())
//                ||
//                !CommonUtils.spinnerSelect("Year of passing",ypSpn.getSelectedItemPosition(),getActivity())
                ){
            return false;
        }

        return true;
    }

    public void toPersonalDerailsUpload(){
        List<HashMap> morderHashMapList = new ArrayList<HashMap>();

        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put(CommonKeys.LJS_Education,""+eduSpn.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_Institute,""+inSpn.getSelectedItem().toString());
        seekerDataMap.put(CommonKeys.LJS_YearOfPass, "" + ypSpn.getSelectedItem().toString());
        seekerDataMap.put("UpdateOn", "" + CommonUtils.getDateTime());

        morderHashMapList.add(seekerDataMap);
        String whereCondition=" where Email='"+CommonUtils.getUserEmail(getActivity())+"'";
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        dbHelper.updateData("JobSeekers", morderHashMapList, whereCondition, getActivity());
        updateJobSeekerProfile(seekerDataMap);
    }


    private void updateJobSeekerProfile(LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(getActivity(), "Educational Details updated successfully", Toast.LENGTH_SHORT).show();
                        ProfileActivity.tvlatupdateddate.setText("Last Update: "+CommonUtils.getDateTime());
//                        startActivity(new Intent(getActivity(), ProfileActivity.class));
                        getActivity().finish();

                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyJSDetails, CommonUtils.getUserEmail(getActivity()), "" + requestString));
    }
    private void BindData(){
        ypSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValuesForYear("Select Year", 36,getActivity())));
        String[] jobType_notice = CommonUtils.getGenericArrayValuesForYear("Select Year", 36,getActivity());
        for(int i=0 ; i <jobType_notice.length ; i++)
        {
            if (jobType_notice[i].contains(educationList.get(0).getYearPassing())) {
                ypSpn.setSelection(i);
            }
        }
    }

    public void getInstitutions(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    institutionMap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_INSTITUTION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getInstitutes), CommonKeys.arrInstitutes);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_EDUCATION;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }

    class StatusHandler extends Handler {

        public void handleMessage(android.os.Message msg)
        {
            if (null != getActivity() && getActivity().isFinishing())
                return;
            switch (msg.what){

                case RESULT_INSTITUTION:
                    if (null != institutionMap && null != inSpn && isClassVisible == true) {
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),inSpn,institutionMap,0,educationList.get(0).getInstitution());
//                        inSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(institutionMap, "Institution")));
//                        String[] eduType_array = CommonUtils.fromMap(institutionMap, "Institution");
//                        for(int i=0 ; i <eduType_array.length ; i++)
//                        {
//                            if (eduType_array[i].contains(educationList.get(0).getInstitution())) {
//                                inSpn.setSelection(i);
//                            }
//                        }
                    } else {
                        Toast.makeText(getActivity(),"Not able to get institution details",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case RESULT_EDUCATION:
                    if (null != educationMap && null != eduSpn && isClassVisible == true) {
                        eduSpn.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Education")));
                        String[] eduType_array = CommonUtils.fromMap(educationMap, "Education");
                        for(int i=0 ; i <eduType_array.length ; i++)
                        {
                            if (eduType_array[i].contains(educationList.get(0).getEduCourse())) {
                                eduSpn.setSelection(i);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(),"Not able to get education details",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
