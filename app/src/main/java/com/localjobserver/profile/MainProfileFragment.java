package com.localjobserver.profile;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localjobserver.jobsinfo.MainFragmentStatePagerAdapter;
import com.localjobserver.ui.MainInfoActivity;
import com.localjobserver.viewpagerindicator.TabPageIndicator;
import co.talentzing.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MainProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ViewPager pager;
    private TabPageIndicator indicator;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;

    public static MainProfileFragment newInstance() {
        MainProfileFragment fragment = new MainProfileFragment();

        return fragment;
    }

    public MainProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main_jobs_info2, container, false);


        pager = (ViewPager)rootView.findViewById(R.id.pager);
        indicator = (TabPageIndicator)rootView.findViewById(R.id.indicator);

        final List<Bundle> payloads = new java.util.ArrayList<Bundle>();
        String[] titles = new String[3];
        final Bundle personalbundle = new Bundle();
        personalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, PersonalDetailsFragment.class.getName());
        personalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Personal");
        payloads.add(personalbundle);
        titles[0] = "Personal";
        final Bundle professionalbundle = new Bundle();
        professionalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, ProfessionalDetailsFragment.class.getName());
        professionalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Professional");
        payloads.add(professionalbundle);
        titles[1] = "Professional";
        final Bundle educationalbundle = new Bundle();
        educationalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, EducationDetailsFragment.class.getName());
        educationalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Education");
        payloads.add(educationalbundle);
        titles[2] = "Education";
//        final Bundle finalbundle = new Bundle();
//        finalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, FinalDetailsFragment.class.getName());
//        finalbundle.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Preferences");
//        payloads.add(finalbundle);

        pager.setAdapter(new MainFragmentStatePagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), payloads, titles));

        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(opcl);

            pager.setCurrentItem(0);
            indicator.setCurrentItem(0);

        return rootView;
    }


    private final ViewPager.OnPageChangeListener opcl = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {

        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
//            mListener = (OnFragmentInteractionListener) activity;
            if (activity instanceof  MainInfoActivity){
                ((MainInfoActivity) activity).onSectionAttached("Jobs For You");
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
