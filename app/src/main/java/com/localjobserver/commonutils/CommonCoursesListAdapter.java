package com.localjobserver.commonutils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.models.FresharsCoursesModel;

import java.util.ArrayList;

public class CommonCoursesListAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<FresharsCoursesModel> jobList;
    public CommonCoursesListAdapter(Activity context,  ArrayList<FresharsCoursesModel> jobList) {
        super();
        this.context=context;
        this.jobList=jobList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.common_courses_list_adapter, null);
        }
        TextView jobTitle = (TextView)convertView.findViewById(R.id.positionNAME);
        jobTitle.setText(((FresharsCoursesModel)jobList.get(position)).getCourseName());
        TextView jobLoc = (TextView)convertView.findViewById(R.id.locJob);
        jobLoc.setText("" + (((FresharsCoursesModel)jobList.get(position)).getLocation()));
        TextView jobComp = (TextView)convertView.findViewById(R.id.compJob);
        jobComp.setText(""+(((FresharsCoursesModel)jobList.get(position)).getInstituteName()));
        TextView jobExp = (TextView)convertView.findViewById(R.id.expJob);
        String salStr = "( "+((FresharsCoursesModel)jobList.get(position)).getCourseFee();
        jobExp.setText(""+salStr);
        TextView jobRole = (TextView)convertView.findViewById(R.id.roleJob);
        jobRole.setText(""+(((FresharsCoursesModel)jobList.get(position)).getContactNumber()));
        TextView jobKeySkills = (TextView)convertView.findViewById(R.id.keySkills);
        jobKeySkills.setText(""+(((FresharsCoursesModel)jobList.get(position)).getInstituteName()));
        TextView jobNotice = (TextView)convertView.findViewById(R.id.noticePeriod);
        jobNotice.setText(""+(((FresharsCoursesModel)jobList.get(position)).getCity()));
        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jobList.size();
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return jobList.get(position);
    }

}