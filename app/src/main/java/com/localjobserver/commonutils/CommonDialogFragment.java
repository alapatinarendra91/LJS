package com.localjobserver.commonutils;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import co.talentzing.R;

public class CommonDialogFragment extends DialogFragment {
    private Button btnYes,btnNo;
    static String DialogBoxTitle;
    public String msg;
    private TextView msgView;
    public interface YesNoDialogListener {
        void onFinishYesNoDialog(boolean state);
    }

    //---empty constructor required
    public CommonDialogFragment(){

    }

    //---set the title of the dialog window---
    public void setDialogTitle(String title) {
        DialogBoxTitle= title;
    }

    //---set the title of the dialog window---
    public void setDialogMessaage(String msg) {
        this.msg= msg;
    }

    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState ) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view= inflater.inflate(R.layout.common_dialog, container);
        //---get the Button views---
        btnYes = (Button) view.findViewById(R.id.okBtn);
        btnNo = (Button) view.findViewById(R.id.cancelBtn);
        msgView = (TextView) view.findViewById(R.id.msgTxt);
        // Button listener
        btnYes.setOnClickListener(btnListener);
        btnNo.setOnClickListener(btnListener);
        msgView.setText(""+msg);

        //---set the title for the dialog
//        getDialog().setTitle(DialogBoxTitle);

        return view;
    }

    //---create an anonymous class to act as a button click listener
    private OnClickListener btnListener = new OnClickListener()
    {
        public void onClick(View v)
        {
            //---gets the calling activity---
            YesNoDialogListener activity = (YesNoDialogListener) getActivity();
            boolean state = ((Button) v).getText().toString().equals("Ok") ? true : false;
            activity.onFinishYesNoDialog(state);
            //---dismiss the alert---
            dismiss();
        }
    };
}