package com.localjobserver.commonutils;

/**
 * Created by admin on 20-04-2015.
 */
public class CommonKeys {
    public static final String LJS_id = "_id";
    public static final String LJS_ReturnUrl = "SeekerId";
    public static final String LJS_SeekerId = "SeekerId";
    public static final String LJS_FirstName = "FirstName";
    public static final String LJS_LastName = "LastName";
    public static final String LJS_Password = "Password";
    public static final String LJS_ConfirmPassword = "ConfirmPassword";
    public static final String LJS_DateOfBirth = "DateOfBirth";
    public static final String LJS_languagesKnown = "languagesKnown";
    public static final String LJS_Email = "Email";
    public static final String LJS_Visibility = "Visibility";
    public static final String LJS_ContactNo = "ContactNo";
    public static final String LJS_ContactNo_Landline = "ContactNo_Landline";
    public static final String LJS_Industry = "Industry";
    public static final String LJS_Experience = "Experience";
    public static final String LJS_CurrentCTC = "CurrentCTC";
    public static final String LJS_expectedCTC = "expectedCTC";
    public static final String LJS_Education = "Education";
    public static final String LJS_KeySkills = "KeySkills";
    public static final String LJS_StdKeySkills = "StdKeySkills";
    public static final String LJS_Location = "Location";
    public static final String LJS_UpdateResume = "UpdateResume";
    public static final String LJS_Gender = "Gender";
    public static final String LJS_Institute = "Institute";
    public static final String LJS_YearOfPass = "YearOfPass";
    public static final String LJS_FunctionalArea = "FunctionalArea";
    public static final String LJS_Role = "Role";
    public static final String LJS_ResumeHeadLine = "ResumeHeadLine";
//    public static final String LJS_ProfileUpdate = "UpdateOn";

    public static final String LJS_UpdateOn = "UpdateOn";
    public static final String LJS_Resume = "Resume";
    public static final String LJS_TextResume = "TextResume";
    public static final String LJS_UserName = "UserName";
    public static final String LJS_Status = "Status";
    public static final String LJS_Photo = "Photo";
    public static final String LJS_CurrentDesignation = "CurrentDesignation";
    public static final String LJS_CurrentCompany = "CurrentCompany";
    public static final String LJS_StdCurCompany = "StdCurCompany";
    public static final String LJS_PreviousDesignation = "PreviousDesignation";
    public static final String LJS_PreviousCompany = "PreviousCompany";
    public static final String LJS_StdPreCompany = "StdPreCompany";
    public static final String LJS_PreferredLocation = "PreferredLocation";

    public static final String LJS_NoticePeriod = "NoticePeriod";
    public static final String LJS_EmailVerified = "EmailVerified";
    public static final String LJS_VisibleSettings = "VisibleSettings";
    public static final String LJS_JsId = "JsId";
    public static final String LJS_IsOnline = "IsOnline";
    public static final String LJS_ViewedCount = "ViewedCount";
    public static final String LJS_DownloadedCount = "DownloadedCount";
    public static final String LJS_CandidatesActiveinLast = "CandidatesActiveinLast";
//    public static final String LJS_ResumeperPage = "ResumeperPage";
//    public static final String LJS_SearchQuery = "SearchQuery";
    public static final String LJS_MatchedScore = "MatchedPercent";
    public static final String LJS_FId = "FId";

    public static final String LJS_JTId = "JTId";
    public static final String LJS_Jobtype = "Jobtype";
    public static final String LJS_CreatedBy = "CreatedBy";

    public static final String LJS_DirectRegistration = "DirectRegistration";
    public static final String LJS_ProfileAccessSpecifier = "ProfileAccessSpecifier";
    public static final String LJS_SharedCompaniesList = "SharedCompaniesList";


    public static final String LJS_UserKeySkills = "UserKeySkills";
    public static final String LJS_IndustryType = "IndustryType";
    public static final String LJS_IndustryCount = "IndustryCount";


    public static final  String LJS_Recid="_id";
    public static final  String LJS_RecRecruterId="RecruterId";
    public static final  String LJS_RecFirstName="FirstName";
    public static final  String LJS_RecLastName="LastName";
    public static final  String LJS_RecPassword="Password";
    public static final  String LJS_RecConfirmPassword="ConfirmPassword";
    public static final  String LJS_RecCompanyName="CompanyName";
    public static final  String LJS_RecStdCurCompany="StdCurCompany";
    public static final  String LJS_RecEmail="Email";
    public static final  String LJS_RecCompanyUrl="CompanyUrl";
    public static final  String LJS_RecContactNo="ContactNo";
    public static final  String LJS_RecContactNo_Landline="ContactNo_Landline";
    public static final  String LJS_RecEmployType="EmployType";
    public static final  String LJS_RecIndustryType="IndustryType";
    public static final  String LJS_RecLocation="Location";
    public static final  String LJS_RecAddress="Address";
    public static final  String LJS_RecCompanyProfile="CompanyProfile";
    public static final  String LJS_RecKeySkills="KeySkills";
    public static final  String LJS_RecActivation="Activation";
    public static final  String LJS_RecRegDate="RegDate";
    public static final  String LJS_RecEmailVerified="EmailVerified";
    public static final  String LJS_RecIsOnline="IsOnline";
    public static final  String LJS_RecDesignation="Designation";
    public static final  String LJS_RecCompanyLogo="CompanyLogo";
//    public static final  String LJS_RecLogoString="LogoString";

    public  static String[] arrLoadRepliedSMSJobTitles = {"Value", "Text"};
    public  static String[] arrEducation = {"Eid", "Qualification"};
    public  static String[] arrRoles = {"RId", "RoleName"};
    public  static String[] arrLocations = {"citycode", "city"};
    public  static String[] arrLocalities = {"locationcode", "location"};
    public  static String[] arrDiscountType = {"DisCode", "DiscountType"};
    public  static String[] arrLanguages = {"LangId", "Language"};
    public  static String[] arrDesignations = {"DId", "DesignationName"};
    public  static String[] arrInstitutes = {"InstId", "InstituteName"};
    public  static String[] arrIndustries = {"InId", "IndustryType"};
    public  static String[] arrSharedJobsdata = {"_id", "JobTitle"};
    public  static String[] arrCompanies = {"UserCompanyNames", "StdCompanyNames"};

    //Local Jobs
    public static final  String LJS_RId="RId";
    public static final  String LJS_InId="InId";
    public static final  String LJS_RoleName="RoleName";
    public static final  String LJS_roleCount="roleCount";

    //preference keys
    public static final  String LJS_PREF_EMAILID = "emailid";
    public static final  String LJS_PREF_PASSWORD = "password";
    public static final  String LJS_PREF_USERTYPE = "usertype";
    public static final  String LJS_PREF_ISLOGIN = "isLogin";
    public static final  String LJS_PREF_FRESHER_ISLOGIN = "isFresherLogin";
    public static final  String LJS_PREF_TRAINER_ISLOGIN = "isTrainerLogin";
    public static final  String LJS_PREF_SUBUSER_ISLOGIN = "isSubuserLogin";

    public static final  String LJS_PREF_SEEKER_TABLENAME = "JobSeekers";
    public static final  String LJS_PREF_RECRUITER_TABLENAME = "RecuiterRegistration";

    public static final  String LJS_WeekDay = "WeekDay";
    public static final  String LJS_OnDemandTime = "OnDemandTime";
    public static final  String LJS_EmailAlert = "EmailAlert";
    public static final  String LJS_SmsAlert = "SmsAlert";
    public static final  String LJS_ChatAlert = "ChatAlert";
    public static final  String LJS_LjsCommunication = "LjsCommunication";
    public static final  String LJS_PremiumJobSeekerService = "PremiumJobSeekerService";
    public static  int USER_LOGIN_TYPE = 0;
}
