package com.localjobserver.commonutils;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkConncetion {

	public static boolean checkInternetConnection(Context context)
	{
		boolean isConnected=false;
	    ConnectivityManager connec =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connected = (connec.getActiveNetworkInfo() != null && connec.getActiveNetworkInfo().isAvailable() && connec.getActiveNetworkInfo().isConnected());
		 
		if(!connected)
		{
			isConnected=false;
		}
		else
		{
			isConnected=true;
		}
		return isConnected;
	}
	
}
