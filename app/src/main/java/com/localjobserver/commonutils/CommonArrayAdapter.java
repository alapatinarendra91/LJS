package com.localjobserver.commonutils;

import android.content.Context;
import android.widget.ArrayAdapter;

import co.talentzing.R;

/**
 * Created by admin on 16-03-2015.
 */
public class CommonArrayAdapter {
    private static  CommonArrayAdapter mCommonArrayAdapter = null;
    public  ArrayAdapter spinnerArrayAdapter = null;
    public  CommonArrayAdapter(){
    }
    public static CommonArrayAdapter getInstance(){
        if(mCommonArrayAdapter == null ) {
            return (mCommonArrayAdapter = new CommonArrayAdapter());
        }
        return mCommonArrayAdapter;
    }
    public ArrayAdapter getGenericAdapper(Context mContext,String[] dataToPopulate){
//        spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, dataToPopulate);
//        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

         spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, dataToPopulate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_button);

        return  spinnerArrayAdapter;
    }
}
