package com.localjobserver.commonutils;

/**
 * Created by admin on 21-03-2015.
 */
public class Queries {

    private static Queries instance;

    public static  Queries getInstance()
    {
        if(instance==null) {
            instance = new Queries();
        }
        return instance;
    }

    public String getRecuiterRegDetails()
    {
        return  "select EmployType,FirstName,LastName,CompanyName,Email,CompanyUrl,Password,ConfirmPassword,Designation,ContactNo,ContactNo_Landline,IndustryType,\n" +
                "Address,CompanyProfile,CompanyLogo,ProfileAlerts  from RecuiterRegistration";
    }

    public String getSubuserRegDetails()
    {
        return  "select CompanyEmailId,SubUserEmailId,CreatedOn,UserName,ContactNo,Status,UserType  from SubUserRegistration";
    }
    public String getJobSeekerDetails()
    {
        return "select Jobtype,JTId,Location,Role,Experience,CurrentCTC,ResumeHeadLine,NoticePeriod,KeySkills,ViewedCount,ContactNo,PreferredLocation,FirstName,FunctionalArea,Email,Industry,Photo from  JobSeekers";
    }

    public String getDetails(String tableName){
        return  "select * from "+tableName;
    }

    public String checkUserExistance(String emailId,String tableName){
       return "select * from "+tableName+" where Email = "+"'"+emailId+"'";
    }

    public String checkExistance(String tableName){
        return "select * from "+tableName;
    }


    public String getSingleValue(String columnName,String tableName){
        return "select "+columnName+" from "+tableName;
    }

    public String deleteRecord(String emailId,String tableName){
        return "delete from "+tableName+" where Email = "+"'"+emailId+"'";
    }


    public String chatQuery(String emailId){
        return "SELECT  FromEmailId, ToEmailId,Message,MessageType,Date,Time from ChatData where ToEmailId = "+"'"+emailId+"'";
    }

}
