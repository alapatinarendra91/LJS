package com.localjobserver.commonutils;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.util.Common;
import com.facebook.Session;
import com.localjobserver.LoginScreen;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.JobsData;
import com.localjobserver.models.PotentialModel;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.models.SearchResultsServiceModel;
import com.localjobserver.multiplespinner.KeyPairBoolData;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.multiplespinner.SpinnerListener;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.Log;
import co.talentzing.R;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for using different android utils
 * @author jonnalagaddasiva
 *
 */
public class  CommonUtils {
    public static File Image_destination ;
    public static String userType = "";
    public static int selectedPos_withotLogin = 0;
    public static String id_withotLogin = "";
    public static String highlightingSkill = "highlightingSkill";
    public static boolean fromAppliedJobs = false;
    public static String userEmail = "";
    public static boolean isLogin = false;
    public static int badgeCount = 0;
    public static boolean isUserExisted, isFresherExisted,isTrainerExisted;
    private static SharedPreferences appPrefs = null,gcmPrefs = null;
    public static ArrayList<JobsData> jobList = new ArrayList<JobsData>();
    public static ArrayList<JobsData> appliedjobList = new ArrayList<JobsData>();
    public static ArrayList<SearchResultsServiceModel> ResumeList = new ArrayList<SearchResultsServiceModel>();
    public static ArrayList<RecommendedCourses> CoursesList = new ArrayList<RecommendedCourses>();
    public static ArrayList<RecommendedCourses> CoursesList_applied = new ArrayList<RecommendedCourses>();
    public static ArrayList<PotentialModel> PotentialList = new ArrayList<PotentialModel>();
    public static ProgressDialog progressDialog;

    /**
	 * validating email pattern
	 */
	public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
	          "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
	          "\\@" +
	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
	          "(" +
	          "\\." +
	          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
	          ")+"
	      );

    /**
	 * Checking the given emailId is valid or not
	 * @param email --> user emailId
	 * @return ---> true if valid else false
	 */
	 public static boolean isValidEmail(String email) {
	        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	    }

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-zA-Z]).{6,20})";
    private static final String REFARENCE_PATTERN = "((?=.*\\d)(?=.*[a-zA-Z]).{1,20})";
    static Pattern pattern = null;
    static Matcher matcher;

    /**
     * Validate password with regular expression
     *
     * @param password
     *            password for validation
     * @return true valid password, false invalid password
     */

    public static boolean passwordValidate(final String password, Context context) {
		/*
		 * Pattern pattern = null; Matcher matcher;
		 */
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        if (matcher.matches() == false)
            Toast.makeText(context,"Password Must contain: Minimum 6 characters and atleast one number.",Toast.LENGTH_SHORT).show();
        return matcher.matches();

    }

    public static boolean referanceNoValidate(final String NO, Context context) {
		/*
		 * Pattern pattern = null; Matcher matcher;
		 */
        pattern = Pattern.compile(REFARENCE_PATTERN);
        matcher = pattern.matcher(NO);
        if (matcher.matches() == false)
            Toast.makeText(context," Job Reference must contain:Minimum 4 and Maximum 10 characters and atleast one number.",Toast.LENGTH_SHORT).show();
        return matcher.matches();

    }
	
	/**
	 * Checking the Internet connection is available or not
	 * @param context
	 * @return  true if available else false
	 */
	 public static boolean isNetworkAvailable(final Context context) {
	        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	        if (null != connectivityManager) {
	            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	        }

	        return false;
	    }

    public static void showToast(String message,Context context){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();

    }


    /**
     * Logout From Facebook
     */
    public static void callFacebookLogout(Context context) {
        Session session = Session.getActiveSession();
        if (session != null) {

            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        } else {

            session = new Session(context);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
            //clear your preferences if saved

        }

    }


    public static void logoutDialog(final int userType,final Context context){
        callFacebookLogout(context);
        final SharedPreferences.Editor editor;
        final SharedPreferences.Editor editor_gcmPrefs;
        appPrefs =  context.getSharedPreferences("ljs_prefs", context.MODE_PRIVATE);
        gcmPrefs = context.getSharedPreferences("ljs_prefs", context.MODE_PRIVATE);
        editor = appPrefs.edit();
        editor_gcmPrefs = gcmPrefs.edit();

        final Dialog logoutdialog;
        logoutdialog = new Dialog(context);
        logoutdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutdialog.setContentView(R.layout.logout_layout);
        logoutdialog.setCanceledOnTouchOutside(true);
        logoutdialog.findViewById(R.id.yesBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ApplicationThread.dbPost("Delete", "Delete Record", new Runnable() {

                     String ConfigForgoturl = "";

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        if (CommonUtils.getUserLoggedInType(context) == 0) {
                            ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.seekerLogout, "" + CommonUtils.getUserEmail(context));
                            LogoutService(context,ConfigForgoturl);
                        } else if (CommonUtils.getUserLoggedInType(context) == 1) {
                            String CompanyMail = DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId","SubUserRegistration"),context);
                            ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.subuserLogout, "" + CommonUtils.getUserEmail(context),CompanyMail);
                            LogoutService(context,ConfigForgoturl);
                        }else if (userType == 2) {
                            DatabaseAccessObject.deleteRecord("FreshersRegistration", context);
                        } else if (userType == 3 ) {
                            DatabaseAccessObject.deleteRecord("TrainersRegistration", context);
                        } else if (userType == 4 ) {
                            DatabaseAccessObject.deleteRecord("RecuiterRegistration", context);
                        }else if (userType == 5 ) {
                            DatabaseAccessObject.deleteRecord("TrainersRegistration", context);
                        }
                        editor.clear();
                        editor_gcmPrefs.clear();
                        editor.commit();
                        editor_gcmPrefs.commit();


                    }
                });

                SharedPreferences.Editor editor = appPrefs.edit();
                editor.putBoolean("isLogin", false);
                editor.putBoolean("isFresherLogin", false);
                editor.putBoolean("isTrainerLogin", false);
                editor.putBoolean("isSubuserLogin", false);
                editor.commit();
            }
        });

        logoutdialog.findViewById(R.id.noBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                logoutdialog.dismiss();
            }
        });
        logoutdialog.show();

    }

    public static void LogoutService(final Context context,String ConfigForgoturl) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success != true) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Failed  " + result.toString(), Toast.LENGTH_LONG).show();
                    if (result != null) {
                    }
                    return;
                }
                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        DatabaseAccessObject.deleteRecord("JobSeekers", context);
                        DatabaseAccessObject.deleteRecord("SubUserRegistration", context);
                        Toast.makeText(context, "Log out Successfully", Toast.LENGTH_LONG).show();
                        if (progressDialog.isShowing())
                            progressDialog.cancel();
                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putBoolean("isLogin", false);
                        editor.putBoolean("isFresherLogin", false);
                        editor.putBoolean("isTrainerLogin", false);
                        editor.putBoolean("isSubuserLogin", false);
                        editor.commit();
                        context.startActivity(new Intent(context, LoginScreen.class).setAction("fromLogout").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                    }
                }
            }
        }, ConfigForgoturl);
    }


	
	 /**
	  * Deleting a particular directory from sdcard
	  * @param path ---> file path
	  * @return  ---> true is successfully deleted else false
	  */
	 public static boolean deleteDirectory(File path) {
	        if (path.exists()) {
	            File[] files = path.listFiles();
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) deleteDirectory(files[i]);
	                else files[i].delete();
	            }
	        }
	        return true;
	    }
	 
	 /**
	  * Class for common alert dialog
	  * @author jonnalagaddasiva
	  *
	  */
	 public class AlertDialogMsg extends AlertDialog.Builder{
			
			Context context;
			public AlertDialogMsg(Context context, String title, String msg) { 
				super(context);
				this.setMessage(msg);
				this.setTitle(title);
				this.setCancelable(false);
		   }

		}


    public static void hideKeyPad(Context context,EditText editField) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editField.getWindowToken(), 0);
    }
	 
	 /**
	  * Checking SD card is available or not in mobile
	  * @param context
	  * @return --> true if available else false
	  */
	 public static boolean isSDcardAvailable(Context context){
			boolean isAvailable=false;
		    boolean available =  Environment.getExternalStorageState().toString().equalsIgnoreCase(Environment.MEDIA_MOUNTED)||Environment.getExternalStorageState().toString().equalsIgnoreCase(Environment.MEDIA_CHECKING)||
	 							 Environment.getExternalStorageState().toString().equalsIgnoreCase(Environment.MEDIA_MOUNTED_READ_ONLY);
		    if(!available){
				isAvailable=false;
			}
			else{
				isAvailable=true;
			}
			return isAvailable;
		}


    public static boolean isValidMobile(String phone2,EditText tv)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+","  "))
        {
            if(phone2.length() < 10 || phone2.length() > 10)
            {
                check = false;
                tv.setError(Html.fromHtml("<font color='red'>"+ "Please specify a valid contact number" +"</font>"));
                tv.requestFocus();

            }
            else
            {
                check = true;
            }
        }
        else
        {
            check=false;
        }
        return check;
    }



	 /**
	  * Checking a string contains an integer or not
	  * @param s  --> Input string
	  * @return true is string contains number else false.
	  */
	 public static boolean isInteger(final String s) {
	        return Pattern.matches("^\\d*$", s);
	    }
	 
	 
	 /**
	  * Getting the device id
	  * @param context  --> Current context
	  * @return  --> Device IMEI number as string
	  */
	 public static String getDeviceId(Context context){
		 TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		 String strDeviceIMEI=telephonyManager.getDeviceId();
		 if (strDeviceIMEI != null) {
			return strDeviceIMEI;
		}else{
			return "";
		}
	 }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }

    public static String encodeFileToBase64Binary(File file)
            throws IOException {

        byte[] bytes = loadFile(file);

        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }


    /**
	  * Getting the device information(android version, Device model,Device manufacturer)
	  * @return  ---> Device information in a string formate
	  */
	 public static String getDeviceExtraInfo(){
		 return ""+Build.VERSION.RELEASE+"/n"+Build.MANUFACTURER + " - " + Build.MODEL+"/n";
	 }
	 
	 /**
	  * Getting the current date and time with comma separated.
	  * @return  if both are required returns date and time with comma separated else if only time required returns time else only date else default date and time.
	  */
	 public static String getDateTime() {
         Date date = new Date();
         String modifiedDate= new SimpleDateFormat("dd/MM/yyyy").format(date);
         return modifiedDate;
	}

    /**
     * Replacing empty spaces with %20
     * @param url  ---> Input url
     * @return  ----> encoded url
     */
    public static String encodeURL(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        return url;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void setimageDestination( String imageName) {
        Image_destination =  new File(Environment.getExternalStorageDirectory().getPath() +
                "/LJS");
        if (!Image_destination.exists())
            Image_destination.mkdirs();

        if (!imageName.equalsIgnoreCase(""))
        Image_destination =  new File(Environment.getExternalStorageDirectory().getPath() +
                "/LJS/"+imageName+".jpg");
    }


    public static String getRealPathFromURI(Uri uri,Context context) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    public static void getAddressFromLocation(final Context context,final double latitude, final double longitude, ApplicationThread.OnComplete onComplete) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            String result = null;
            List<Address> addressList = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Address address = addressList.get(0);
                StringBuilder sb = new StringBuilder();
                sb.append(address.getLocality());
                result = sb.toString();
                onComplete.execute(true, result, address.getSubLocality());
            }
        } catch (IOException e) {
            Log.e("", "Unable connect to Geocoder", e);
            onComplete.execute(false, null, null);
        }
    }
	 
	 /**
	  * Converting given string into date format
	  * @param dateStr  ---> Given date
	  * @param _dateFormatteStr   ----> Given date format
	  * @return  ----> converted date   (if any exception occurred returns null)
	  */
	 public Date convertStringToDate(String dateStr, String _dateFormatteStr){
		 Date _convertedDate = null;
		 SimpleDateFormat _dateFormate = new SimpleDateFormat(""+_dateFormatteStr);
		 try {
			_convertedDate = _dateFormate.parse(_dateFormatteStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return _convertedDate;
	 }
	 
	 /**
	  * Converting given date into required format 
	  * @param _date --> Date to convert
	  * @param requiredFormat   ----> Format required
	  * @return  final date string
	  */
	 public String convertDateToString(Date _date, String requiredFormat){
		 String _finalDateStr = "";
		 SimpleDateFormat _dateFormatter = new SimpleDateFormat(requiredFormat);
		 try{
			 _finalDateStr = _dateFormatter.format(_date);
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 return _finalDateStr;
	 }


    public static String getCopyright(final Context context) {
        String version;
        Calendar c = Calendar.getInstance();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            StringBuilder sb = new StringBuilder();
            sb.append("Version ").append(packageInfo.versionName)
                    // .append("\n(Build ").append(Config.BUILD_ID).append(")\n")
                    .append(String.format("\nCopyright " + Html.fromHtml("&copy;") + "2010-" + c.get(Calendar.YEAR) + ",\n Vishist Business Solutions Pvt Ltd.\nAll Rights Reserved."));
            version = sb.toString();
        } catch (PackageManager.NameNotFoundException e) {
            version = Html.fromHtml("&copy;") + "2010" + c.get(Calendar.YEAR) + "\nVishist Business Solutions Pvt Ltd.\nAll Rights Reserved.";
        }
        return version;
    }

    public  static boolean spinnerSelect(String spinner,int position,Context context) {
        if (position == 0 || position <= 0){
            Toast.makeText(context,"Please select "+spinner,Toast.LENGTH_SHORT).show();
            return false;
        }else
            return true;
    }

    public  static boolean spinnerPositinCondition(String spinner,int minimum,int maximum,Context context) {
        if (minimum >= maximum){
            Toast.makeText(context,"Please select "+spinner,Toast.LENGTH_SHORT).show();
            return false;
        }else
            return true;
    }

    public static void spinnerbinditem(Spinner mSpinner,String[] jobType_array,String item){
        for(int i=0 ; i <jobType_array.length ; i++)
        {
            if (jobType_array[i].equalsIgnoreCase(item)) {
                mSpinner.setSelection(i);
            }
        }
    }

    public static void spinnerSetLacks(Spinner mSpinner,String value){
        String[] expArr = null;
        if (!value.equalsIgnoreCase("")){
            if ( value.contains(".")) {
                expArr = value.split("\\.");
                if (null != expArr) {
                    mSpinner.setSelection(Integer.parseInt(expArr[0])+1);
                }
            } else {
                mSpinner.setSelection(Integer.parseInt(value)+1);
            }
        }else
            mSpinner.setSelection(0);
    }

    public static void spinnerSetThousands(Spinner mSpinner,String value){
        String[] expArr = null;
        if (!value.equalsIgnoreCase("")){
            if ( value.contains(".")) {
                expArr = value.split("\\.");
                if (null != expArr) {
                    mSpinner.setSelection(Integer.parseInt(expArr[1])+1);
                }
            } else {
                mSpinner.setSelection(0);
            }
        }else
            mSpinner.setSelection(0);
    }


    public static String[] getGenericArrayValues(String type, int size){
        String[] items = new String[size+1];
        items[0] = "Select";
        for (int i = 1; i <= size; i++){

            if(type.length() == 0){
                if(i == size)
                    items[i]=""+(i-1)+"+";
                else{
                    items[i]=""+(i-1);
                }

            }else{
                items[i]=""+((i-1)+1980);
            }
        }
        return items;
    }

    public static String[] getGenericArrayValuesForYear(String select, int size,Context mContext){
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        String[] years = new String[(thisYear - 1980) + 2];
        years[0] = select;

        int k = 1;

        for (int i = 1980; i <= thisYear; i++) {
            //years.add(Integer.toString(i));
            years[k] = String.valueOf(i);
            k++;
        }
        return years;
    }

    public static String[] getGenericAfterArrayValues(int startSize, int size){
        String[] items = new String[size-startSize];
        Log.i("","Item size: "+items.length);
        for (int i = 0; i <= size-1; i++){

            if(i >= startSize){

                if(i == size-1)
                    items[i-startSize]=""+(i)+"+";
                else{
                    items[i-startSize]=""+(i);
                }

            }else{
//                items[i]="Extra";
            }
        }
        return items;
    }

    public static int getImageOrientation(String imagePath){
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }


    public static String[] fromMap(LinkedHashMap<String,String> inputMap, String type){
        Collection c = inputMap.values();
        Iterator itr = c.iterator();
        int size =  inputMap.size()+1;
        String[] toMap = new String[size];
        toMap[0] = "--Select "+type+"--";
        int iCount = 1;
        while (iCount < size && itr.hasNext()) {
            toMap[iCount] = itr.next().toString();
            iCount++;
        }
        while (iCount < size && itr.hasNext()) {
            toMap[iCount] = itr.next().toString();
            iCount++;
        }

//        Arrays.sort(toMap);
//        toMap[0] = "Select";
        return toMap;
    }

//    public  static  String[] sortStringArray(String[] sort){
//
//        String[] jobType_array = sort;
//        Arrays.sort(jobType_array);
//        jobType_array[0] = "Select";
//
//        return  jobType_array;
//
//    }


    public static String formattedTime(){
        DateFormat stringTime = new SimpleDateFormat("hh:mm a");
        Date currentDate = new Date(System.currentTimeMillis());
        return stringTime.format(currentDate);
    }

    public static int parseIntValue(String inputValue){
        if (!TextUtils.isEmpty(inputValue) && TextUtils.isDigitsOnly(inputValue)) {
            try {
                return Integer.parseInt(inputValue);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        return 0;
    }

    public static String convertToDays(String inputStr){
        int resultInt = 0;
        String resultStr = inputStr.replaceAll("\\D+","");
        if (inputStr.contains("Week") || inputStr.contains("Weeks")){
            resultInt = parseIntValue(resultStr) * 7;
            resultStr = String.valueOf(resultInt);
        } else if (inputStr.contains("Month") || inputStr.contains("Months")){
            resultInt = parseIntValue(resultStr) * 30;
            resultStr = String.valueOf(resultInt);
        }
        return  resultStr;
    }

    public static String convertToWeeks(String inputStr){

        if (inputStr.equals("7")){
            inputStr = "1 Week";
        }else if (inputStr.contains("15")){
            inputStr = "15 Days";
        }else if (inputStr.contains("21")){
            inputStr = "3 Weeks";
        }else if (inputStr.contains("30")){
            inputStr = "1 Month";
        }else if (inputStr.contains("60")){
            inputStr = "2 Months";
        }else if (inputStr.contains("90")){
            inputStr = "3 Months";
        }else {
            inputStr = "0 Day";
        }
        return  inputStr;
    }

    public static int getUserLoggedInType(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE,0);
    }

    public static boolean isLoggedIn(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return appPrefs.getBoolean("isLogin", false);
    }

    public static boolean isFresherLoggedIn(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return appPrefs.getBoolean("isFresherLogin", false);
    }

    public static boolean isSubuserLoggedIn(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return appPrefs.getBoolean("isSubuserLogin", false);
    }

    public static boolean isTrainerLoggedIn(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return appPrefs.getBoolean("isTrainerLogin", false);
    }

    public static String getUserEmail(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return  appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "");
    }


    public static String getDeviceIdEmail(Context context){
        appPrefs =  context.getSharedPreferences("ljs_prefs",context.MODE_PRIVATE);
        return  appPrefs.getString("gcmRegId", "");
    }


    public static String getCleanDate(String dateToFormate){
        if (!dateToFormate.equalsIgnoreCase("")){
            long dateValue = Long.parseLong(dateToFormate.replaceAll("[^0-9]", ""));
            Date date=new Date(dateValue);
            SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
            String dateText = df2.format(date);
            return dateText;
        }
        return dateToFormate;
    }

    public static String getCleanTime(String dateToFormate){
        if (!dateToFormate.equalsIgnoreCase("")){
            long dateValue = Long.parseLong(dateToFormate.replaceAll("[^0-9]", ""));
            Date date=new Date(dateValue);
            SimpleDateFormat df2 = new SimpleDateFormat("HH:MM");
            String dateText = df2.format(date);
            return dateText;
        }
        return dateToFormate;
    }

    /*public static void PdfsaveResumeToSdcard(final ApplicationThread.OnComplete oncomplete,String resumeBase64String, String email, Context context){
        String resumesFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/LJS/Brochures";
        File resumesDirectory=new File(resumesFolderPath);
        if(!resumesDirectory.exists()){
            resumesDirectory.mkdirs();
        }
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Common.class.getName());
        wl.acquire();
        OutputStream output = null;
        try {
            String fileName = resumesFolderPath +"/"+ email+".pdf";
            output = new FileOutputStream(fileName);//change extension
            byte[] decodedBytes =   Base64.decode(resumeBase64String, Base64.DEFAULT);
            output.write(decodedBytes);
            oncomplete.execute(true, email+".pdf", null);
        }
        catch (Exception e){
            e.toString();
            oncomplete.execute(false,  e.toString(), null);
        }
        finally {
            wl.release();
            try {
                if (output != null)
                    output.close();
            }
            catch (IOException ignored) {
                oncomplete.execute(false,  ignored.toString(), null);
            }
        }

    }*/


    public static void saveResumeToSdcard(final ApplicationThread.OnComplete oncomplete,String resumeBase64String, String email,String type, Context context){
        String resumesFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/TZ/Resumes";
        File resumesDirectory=new File(resumesFolderPath);
        if(!resumesDirectory.exists()){
            resumesDirectory.mkdirs();
        }
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Common.class.getName());
        wl.acquire();
        OutputStream output = null;
        try {
//            String fileName = resumesFolderPath +"/"+ email+type;
            String fileName = resumesFolderPath +"/"+ email.substring(0, email.length() - 4)+type;
            output = new FileOutputStream(fileName);//change extension
            byte[] decodedBytes =   Base64.decode(resumeBase64String, Base64.DEFAULT);
            output.write(decodedBytes);
            oncomplete.execute(true, email.substring(0, email.length() - 4)+type, null);
        }
        catch (Exception e){
            e.toString();
            oncomplete.execute(false,  e.toString(), null);
        }
        finally {
            wl.release();
            try {
                if (output != null)
                    output.close();
            }
            catch (IOException ignored) {
                oncomplete.execute(false,  ignored.toString(), null);
            }
        }

    }

    public static void saveBrochureToSdcard(final ApplicationThread.OnComplete oncomplete,String resumeBase64String, String email,String type, Context context){
        String resumesFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/LJS/Brochure";
        File resumesDirectory=new File(resumesFolderPath);
        if(!resumesDirectory.exists()){
            resumesDirectory.mkdirs();
        }
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Common.class.getName());
        wl.acquire();
        OutputStream output = null;
        try {
            String fileName = resumesFolderPath +"/"+ email+type+".pdf";
            output = new FileOutputStream(fileName);//change extension
            byte[] decodedBytes =   Base64.decode(resumeBase64String, Base64.DEFAULT);
            output.write(decodedBytes);
            oncomplete.execute(true, email+type+".pdf", null);
        }
        catch (Exception e){
            e.toString();
            oncomplete.execute(false,  e.toString(), null);
        }
        finally {
            wl.release();
            try {
                if (output != null)
                    output.close();
            }
            catch (IOException ignored) {
                oncomplete.execute(false,  ignored.toString(), null);
            }
        }

    }


    /**
     * Checking whether the file existed in SD card or not
     * @param fileName
     * @return  true if file existed and false if file not existed.
     */
    public static boolean  isFileExisted(String fileName)
    {
        boolean iChecker = false;

        File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TZ/Resumes");
        try
        {
            boolean recursive = true;
            Collection files = FileUtils.listFiles(root, null, recursive);
            for (Iterator iterator = files.iterator(); iterator.hasNext();)
            {
                File file = (File) iterator.next();
                if (file.getName().equalsIgnoreCase(fileName))
                {
                    File isFile = new File(file.getAbsolutePath());
                    if(isFile.exists())
                    {
                        iChecker = true;
                    }else{
                        iChecker = false;
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return iChecker;
    }

    /**
     * Checking whether the file existed in SD card or not
     * @param brochure
     * @return  true if file existed and false if file not existed.
     */
    public static boolean  isBrochureExisted(String fileName)
    {
        boolean iChecker = false;

        File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/LJS/Brochure");
        try
        {
            boolean recursive = true;
            Collection files = FileUtils.listFiles(root, null, recursive);
            for (Iterator iterator = files.iterator(); iterator.hasNext();)
            {
                File file = (File) iterator.next();
                if (file.getName().startsWith(fileName))
                {
                    File isFile = new File(file.getAbsolutePath());
                    if(isFile.exists())
                    {
                        iChecker = true;
                    }else{
                        iChecker = false;
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return iChecker;
    }


    public static int getIndex(Set<? extends String> set, String value) {
        int result = 0;
        for (String entry:set) {
            Log.d("","entry value"+entry+"   "+value);
            if (entry.equalsIgnoreCase(value)) {
                return result;
            }
            result++;
        }
        return -1;
    }

    public static String getKeyFromValue(LinkedHashMap<String,String> map, String value){
        String toReturnvalue = "";
        for(Map.Entry entry: map.entrySet()){
            if(value.equalsIgnoreCase(entry.getValue().toString())){
                toReturnvalue = (String)entry.getKey();
                break; //breaking because its one to one map
            }
        }
        return toReturnvalue;
    }

    public void getActivityName(final ApplicationThread.OnComplete oncomplete, final Context context){
        ApplicationThread.bgndPost("", "Registering Device...", new Runnable() {
            @Override
            public void run() {
                String[] activePackages;
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                    activePackages = getActivePackages(context);
                } else {
                    activePackages = getActivePackagesCompat(context);
                }
                if (activePackages != null) {
                    for (String activePackage : activePackages) {
                        if (activePackage.equals("com.google.android.calendar")) {
                            //Calendar app is launched, do something
                        }
                    }
                }
                }
        });
    }

    String[] getActivePackagesCompat(Context context) {
        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningTaskInfo> taskInfo = mActivityManager.getRunningTasks(1);
        final ComponentName componentName = taskInfo.get(0).topActivity;
        final String[] activePackages = new String[1];
        activePackages[0] = componentName.getPackageName();
        return activePackages;
    }

    String[] getActivePackages(Context context) {
        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = mActivityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(Arrays.asList(processInfo.pkgList));
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }

    public static String removeLastComma(String str) {
        if (str.length() > 0 && str.charAt(str.length()-1)==',') {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    public static String replaceNewLines(String inputStr){

        if (null != inputStr && inputStr.length() > 0) {
            return inputStr.replaceAll("\\\\n","");
        }
        return "";
    }

    public static void setTouch(final Context mContext,final EditText touchEdit,final EditText errrEdit,final String showText){
        touchEdit.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (showText.contains("mobile") && !isValidMobile(errrEdit.getText().toString(),errrEdit)){
                    return true;
                }else if (showText.contains("email") && !isValidEmail(errrEdit.getText().toString())){
                    errrEdit.setError(""+"Invalid email");
                    errrEdit.requestFocus();
                    return true;
                }else if (errrEdit.getText().toString().length() == 0){
                    errrEdit.setError(""+showText);
                    errrEdit.requestFocus();
                    return true;
                }else
                    errrEdit.setError(null);

                return false;
            }
        });
    }

    public static Dialog dialogIntialize(Context mContext, int xml){
        Dialog dialog;
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(xml);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        Double width = metrics.widthPixels * .95;
        Double height = metrics.heightPixels * .74;
        Window win = dialog.getWindow();
        win.setLayout(width.intValue(), height.intValue());

        return dialog;
    }

    public static boolean appInstalledOrNot(String uri,Context mContext) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
            CommonUtils.showToast("App is not currently installed on your phone",mContext);
        }
        return app_installed;
    }

    public static void edittextChangelistner(Context mContext,final EditText mEdittext){

        mEdittext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                mEdittext.setError(null);
            }
        });

    }

    public static boolean checkSpinnerposition(Spinner mSpinner){
        if (mSpinner.getSelectedItemPosition() == 0)
            return false;
        else
            return true;
    }

    public static boolean checkEditTextEampty(EditText mEditText){
        if (mEditText.getText().toString().equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    public static void setMultispuinnerDataFromHashmap(final Context mContext,MultiSpinnerSearch mSpinner,LinkedHashMap<String,String> dataMAp,int limit,String bindData){
        String[] splitParts;
        if (bindData.length()>0 && !bindData.contains(","))
            bindData = bindData.toString()+",";

            splitParts = bindData.split(",");

        final List<KeyPairBoolData> listArray = new ArrayList<>();
        for (Map.Entry<String,String> entry : dataMAp.entrySet()) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId("" + entry.getKey());
            h.setName(entry.getValue());
            h.setSelected(false);
            if (splitParts != null && splitParts.length > 0){
                for (int j = 0; j < splitParts.length; j++){
                    if (entry.getValue().equalsIgnoreCase(splitParts[j].toString()))
                        h.setSelected(true);
                }
            }
            listArray.add(h);
        }

        mSpinner.setItems(listArray, -1, new SpinnerListener() {
            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
//                for (int i = 0; i < items.size(); i++) {
//                    if (items.get(i).isSelected()) {
//                        android.util.Log.e("TAG", i + " : " + items.get(i).getName() + " : " + items.get(i).isSelected());
//                    }
//                }
            }
        });

        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(mContext, R.layout.spinner_button, new String[]{bindData});
        if (!bindData.equalsIgnoreCase(""))
        mSpinner.setAdapter(adapterSpinner);

        mSpinner.setLimit(limit, new MultiSpinnerSearch.LimitExceedListener() {
            @Override
            public void onLimitListener(KeyPairBoolData data) {
                                    Toast.makeText(mContext,
                                            "Limit exceed ", Toast.LENGTH_LONG).show();
            }
        });

    }

    public static void CancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    public static void addContact(Context mContext, String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(Contacts.People.NUMBER, phone.replace(" ",""));
        values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        values.put(Contacts.People.LABEL, name);
        values.put(Contacts.People.NAME, name);
        Uri dataUri = mContext.getContentResolver().insert(Contacts.People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
        values.put(Contacts.People.NUMBER, phone);
        updateUri = mContext.getContentResolver().insert(updateUri, values);
    }

    public static void deleteContact(Context mContext){

        ContentResolver cr = mContext.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        while (cur.moveToNext()) {
            try{
                String lookupName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);

                if (lookupName.equalsIgnoreCase("TZing")){
                    System.out.println("The uri is " +lookupName +"   "+ uri.toString());
                    cr.delete(uri, null, null);
                }

            }
            catch(Exception e)
            {
                System.out.println(e.getStackTrace());
            }
        }
    }
}
