package com.localjobserver.commonutils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import co.talentzing.R;

public class AlertDialogMsg extends Dialog{
	Context context;
	public AlertDialogMsg(Context context, String title, String msg) {
	    super(context);
        this.context=context;
        final TextView versionTxt = (TextView)this.findViewById(R.id.version);
        versionTxt.setText("");
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.common_dialog);
        this.setCanceledOnTouchOutside(true);
	}
}
