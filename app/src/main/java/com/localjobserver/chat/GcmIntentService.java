package com.localjobserver.chat;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.DateFormats;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.networkutils.ApplicationThread;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

public class GcmIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	public static final String TAG = "GcmIntentService";
	SharedPreferences gcmPrefs;
	SharedPreferences.Editor editor;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
		editor = gcmPrefs.edit();

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				if (extras.containsKey("alert")) {
					addRecordToDb(extras);
					if (!gcmPrefs.getBoolean("ChatScreen",false))
						sendPushNotification(extras);
					else{
						Intent uiUpdateIntent = new Intent("update");
						uiUpdateIntent.putExtra("fromuserid", extras.get("fromuserid").toString());
						uiUpdateIntent.putExtra("message", extras.get("alert").toString());
						LocalBroadcastManager.getInstance(this).sendBroadcast(uiUpdateIntent);
					}
				} else {
					sendNotification("Received Message : "
							+ extras.getString("message"));
				}

				Log.i(TAG, "Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);

	}

	private void sendPushNotification(Bundle b) {
		Log.d(TAG, "Preparing to send notification...: " + b.get("alert"));
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent myintent = new Intent(this, LoginScreen.class);
		Bundle chatBundle = new Bundle();
		myintent.putExtra("fromuserid", b.get("fromuserid").toString());
		myintent.putExtra("message", b.get("alert").toString());
		if (b.get("alert").toString().equalsIgnoreCase("JobAlert"))
			myintent.setAction("fromJobAlert");
		else if (b.get("alert").toString().toUpperCase().equalsIgnoreCase("APPLYALERT")){
			myintent.setAction("APPLYALERT");
		}
		else
		myintent.setAction("fromChat");

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_ONE_SHOT);

		Intent uiUpdateIntent = new Intent("update");
		uiUpdateIntent.putExtra("fromuserid", b.get("fromuserid").toString());
		uiUpdateIntent.putExtra("message", b.get("alert").toString());
		LocalBroadcastManager.getInstance(this).sendBroadcast(uiUpdateIntent);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
//				this).setSmallIcon(R.drawable.ic_notif_icon)
				this).setSmallIcon(R.drawable.icon_notification)
				.setContentTitle("TalentZing")
				.setAutoCancel(true)
//				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(b.get("alert").toString());

		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(0, mBuilder.build());
		Log.d(TAG, "Notification sent successfully.");

		try {
			if (b.get("alert").toString().equalsIgnoreCase("JobAlert")){
				int seekerMachedSkillJob_count = gcmPrefs.getInt("badgeCount_seekerMachedSkillJob", 0);
				int total_count = gcmPrefs.getInt("badgeCount_Total", 0);
				seekerMachedSkillJob_count = seekerMachedSkillJob_count+1;
				total_count = total_count+1;
				editor.putInt("badgeCount_seekerMachedSkillJob", seekerMachedSkillJob_count);
				editor.putInt("badgeCount_Total", total_count);
				editor.commit();
                ShortcutBadger.applyCount(GcmIntentService.this,total_count);
			}else if (b.get("alert").toString().toUpperCase().equalsIgnoreCase("APPLYALERT")){
				int recruiterAlert_count = gcmPrefs.getInt("badgeCount_recruiterAlert", 0);
				int total_count = gcmPrefs.getInt("badgeCount_Total", 0);
				recruiterAlert_count = recruiterAlert_count+1;
				total_count = total_count+1;
				editor.putString("recruiterAlertJobId", b.get("jobid").toString());
				editor.putInt("badgeCount_recruiterAlert", recruiterAlert_count);
				editor.putInt("badgeCount_Total", total_count);
				editor.commit();
				ShortcutBadger.applyCount(GcmIntentService.this,total_count);
			}else {
				int badgeCount_Chat = gcmPrefs.getInt("badgeCount_Chat", 0);
				int total_count = gcmPrefs.getInt("badgeCount_Total", 0);
				badgeCount_Chat = badgeCount_Chat+1;
				total_count = total_count+1;
				editor.putInt("badgeCount_Chat", badgeCount_Chat);
				editor.putInt("badgeCount_Total", total_count);
				editor.putString("fromuserid", b.get("fromuserid").toString());
				editor.commit();
				ShortcutBadger.applyCount(GcmIntentService.this,total_count);
			}
		} catch (Exception e) {
			Log.d("", e.getMessage());
		}

	}

//
	public void  addRecordToDb(final Bundle mChatMessage){
		String dates[] = null;
		String timeStr;
		String dateTime = DateFormats.getCurrentDatetimePrefix();
		if (dateTime.length() > 0) {
			dates = dateTime.split(" ");
		}
		timeStr = dates[1]+" "+dates[2];

		LinkedHashMap eachEntryDataMap = new LinkedHashMap();
		final List<LinkedHashMap> recordList = new ArrayList<LinkedHashMap>();
		eachEntryDataMap.put("ToEmailId", mChatMessage.get("fromuserid").toString());
		eachEntryDataMap.put("FromEmailId", CommonUtils.getUserEmail(this));
		eachEntryDataMap.put("Message", "" + mChatMessage.get("alert").toString());
		eachEntryDataMap.put("MessageType","left");
		eachEntryDataMap.put("Time", timeStr);
		eachEntryDataMap.put("Date", "" + dates[0]);
		eachEntryDataMap.put("Created_date", "" + dateTime);
		eachEntryDataMap.put("Updated_Date", "" + dateTime);
		recordList.add(eachEntryDataMap);

		ApplicationThread.dbPost(this.getClass().getName(), "insert..ChatData..", new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				final DatabaseHelper seekerInsertDB = new DatabaseHelper(GcmIntentService.this);

				seekerInsertDB.insertData("ChatData", recordList, GcmIntentService.this);


			}
		});
	}
	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String msg) {

        Log.d(TAG, "Preparing to send notification...: " + msg);
        mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent myintent = new Intent(this, LoginScreen.class);
        myintent.putExtra("message", msg);
        myintent.setAction("fromNotification");
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,myintent, 0);
        SharedPreferences gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
		int seekerMachedSkillJob_count = gcmPrefs.getInt("badgeCount_seekerMachedSkillJob", 0);
		seekerMachedSkillJob_count = seekerMachedSkillJob_count+1;
		SharedPreferences.Editor editor = gcmPrefs.edit();
		editor.putInt("badgeCount", 0);
		editor.putInt("badgeCount_seekerMachedSkillJob", seekerMachedSkillJob_count);
		editor.commit();
		CommonUtils.badgeCount = 0;
		ShortcutBadger.applyCount(GcmIntentService.this,0);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.icon)
                .setContentTitle("LocalJobServer")
                .setAutoCancel(true)
//				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText("You 2 recommanded jobs for you");
        mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/"+ R.raw.droplet));
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        Log.d(TAG, "Notification sent successfully.");

	}

}