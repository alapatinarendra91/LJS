package com.localjobserver.chat;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.talentzing.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class LJSChatActivityFragment extends Fragment {

    public LJSChatActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ljschat, container, false);
    }
}
