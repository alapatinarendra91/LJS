package com.localjobserver.chat;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.DateFormats;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

public class LJSChatActivity extends ActionBarActivity {
    private ActionBar actionBar;

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;

    private Intent intent;
    private boolean side = false;
    private Bundle recevierBundle;
    private String fromEmailId, toEmailId;
    private List<ChatMessage> chatMessagesArr;
    private String dateTime = "", timeStr = "", message = "", timeUniqueStr = "";
    private String dates[] = null;
    private Intent receiverIntent;
    private SharedPreferences gcmPrefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(R.layout.action_title);

         gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
         editor = gcmPrefs.edit();

        int seekerMachedSkillJob_count = gcmPrefs.getInt("badgeCount_seekerMachedSkillJob", 0);
        editor.putInt("badgeCount_Chat", 0);
        editor.putInt("badgeCount_Total", seekerMachedSkillJob_count);
        editor.commit();
        ShortcutBadger.applyCount(LJSChatActivity.this,seekerMachedSkillJob_count);

        chatMessagesArr = new ArrayList<ChatMessage>();

        receiverIntent = getIntent();
        recevierBundle = receiverIntent.getExtras();

        fromEmailId = recevierBundle.getString("fromEmailId");
        toEmailId = recevierBundle.getString("toEmailId");
        actionBar.setTitle(""+toEmailId);


        buttonSend = (Button) findViewById(R.id.send);

        listView = (ListView) findViewById(R.id.msgview);
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
        listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) findViewById(R.id.msg);
        chatText.setFocusable(true);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });

        if(chatText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!chatText.getText().toString().equalsIgnoreCase(""))
                    sendChatMessage();
                else
                    CommonUtils.showToast("Please type Message to send", getApplicationContext());
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);
//        if (null != receiverIntent.getAction() && receiverIntent.getAction().equalsIgnoreCase("fromChatNoti")) {
//
//            message = recevierBundle.getString("message");
//            dateTime = DateFormats.getCurrentDatetimePrefix();
//            if (dateTime.length() > 0) {
//                dates = dateTime.split(" ");
//            }
//            timeStr = dates[1]+" "+dates[2];
//            ChatMessage mChatObj = new ChatMessage();
//            mChatObj.setMessageType("left");
//            mChatObj.setMessage(message);
//            mChatObj.setFromEmailId(fromEmailId);
//            mChatObj.setToEmailId(toEmailId);
//            mChatObj.setTime(timeStr);
////            addRecordToDb(mChatObj);
//            chatArrayAdapter.add(mChatObj);
//        }
        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

        LocalBroadcastManager.getInstance((LJSChatActivity.this)).registerReceiver(mMessageReceiver, new IntentFilter("update"));

        populateChatMessages();
    }

//    @Override
//    public void onResume(){
//        super.onResume();
//
//    }

    private boolean sendChatMessage(){
        dateTime = DateFormats.getCurrentDatetimePrefix();
        if (dateTime.length() > 0) {
            dates = dateTime.split(" ");
        }
        timeStr = dates[1]+" "+dates[2];
        ChatMessage mChatObj = new ChatMessage();
        mChatObj.setMessageType("right");
        mChatObj.setMessage(chatText.getText().toString());
        mChatObj.setFromEmailId(fromEmailId);
        mChatObj.setToEmailId(toEmailId);
        mChatObj.setTime(timeStr);
        addRecordToDb(mChatObj);
        sendMessageToServer(chatText.getText().toString());
        return true;
    }

    public void sendMessageToServer(String message) {
            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        Toast.makeText(LJSChatActivity.this, "Error while sending message", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null) {
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.SendGCMNotification, message, fromEmailId, toEmailId, CommonUtils.getUserLoggedInType(LJSChatActivity.this)));
    }


    public void populateChatMessages(){
        chatMessagesArr = DatabaseAccessObject.getChatMessage(Queries.getInstance().chatQuery(toEmailId), LJSChatActivity.this);
        if (null != chatMessagesArr && chatMessagesArr.size() > 0) {
            for (int cCount = 0; cCount < chatMessagesArr.size(); cCount++) {
                chatArrayAdapter.add(chatMessagesArr.get(cCount));
            }
        }
    }


    public void  addRecordToDb(final ChatMessage mChatMessage){

        LinkedHashMap eachEntryDataMap = new LinkedHashMap();
        final List<LinkedHashMap> recordList = new ArrayList<LinkedHashMap>();
        eachEntryDataMap.put("FromEmailId",mChatMessage.getFromEmailId());
        eachEntryDataMap.put("ToEmailId",mChatMessage.getToEmailId());
        eachEntryDataMap.put("Message",""+mChatMessage.getMessage());
        eachEntryDataMap.put("MessageType",""+mChatMessage.getMessageType());
        eachEntryDataMap.put("Time",timeStr);
        eachEntryDataMap.put("timeUnique",dateTime);
        eachEntryDataMap.put("Date",""+dates[0]);
        eachEntryDataMap.put("Created_date",""+dateTime);
        eachEntryDataMap.put("Updated_Date",""+dateTime);
        recordList.add(eachEntryDataMap);

        ApplicationThread.dbPost(this.getClass().getName(), "insert..ChatData..", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                final DatabaseHelper seekerInsertDB = new DatabaseHelper(LJSChatActivity.this);

                seekerInsertDB.insertData("ChatData", recordList, LJSChatActivity.this);

                ApplicationThread.uiPost(LJSChatActivity.this.getClass().getName(), "show/hide progress", new Runnable() {
                    @Override
                    public void run() {
                        chatArrayAdapter.add(mChatMessage);
                        chatText.setText("");
                    }
                });
            }
        });
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("update")) {
                    Log.v("","Check...");
                    ChatMessage mChatObj = new ChatMessage();
                    mChatObj.setMessageType("left");
                    mChatObj.setMessage(intent.getExtras().getString("message"));
                    mChatObj.setFromEmailId(fromEmailId);
                    mChatObj.setToEmailId(toEmailId);
                    mChatObj.setTime(timeStr);
                    if (null != mChatObj) {
//                        addRecordToDb(mChatObj);
                        chatArrayAdapter.add(mChatObj);
//                        chatText.setText("");
                    }
                }
            }
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
//            startActivity(new Intent(LJSChatActivity.this, LoginScreen.class));
            finish();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        editor.putBoolean("ChatScreen", true);
        editor.commit();
        CancelNotification(getApplicationContext(),0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        editor.putBoolean("ChatScreen", false);
        editor.commit();
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(LJSChatActivity.this, LoginScreen.class));
//        finish();
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void CancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx
                .getSystemService(ns);
        nMgr.cancel(notifyId);
    }
}
