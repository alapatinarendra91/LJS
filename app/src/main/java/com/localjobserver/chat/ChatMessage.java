package com.localjobserver.chat;

import java.io.Serializable;

/**
 * Created by siva on 22/08/15.
 */
public class ChatMessage implements Serializable{
    public boolean left;
    public String FromEmailId;
    public String ToEmailId;
    public String Message;
    public String MessageType;
    public String date;
    public String time;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public String getFromEmailId() {
        return FromEmailId;
    }

    public void setFromEmailId(String fromEmailId) {
        FromEmailId = fromEmailId;
    }

    public String getToEmailId() {
        return ToEmailId;
    }

    public void setToEmailId(String toEmailId) {
        ToEmailId = toEmailId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getMessageType() {
        return MessageType;
    }

    public void setMessageType(String messageType) {
        MessageType = messageType;
    }

    //    public ChatMessage(boolean left, String message) {
//        super();
//        this.left = left;
//        this.message = message;
//    }
}
