package com.localjobserver.TrainingInstitute;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.models.PotentialModel;
import co.talentzing.R;

import java.util.ArrayList;

public class PotentialAdopter extends BaseAdapter {

    private OnCartChangedListener onCartChangedListener;
    public Context context;
    private ArrayList<PotentialModel> potentialList;
    public PotentialAdopter(Context context, ArrayList<PotentialModel> potentialList) {
        super();
        this.context=context;
        this.potentialList=potentialList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.activity_potential_adopter, null);
        }
        TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
        jobTitle.setText(potentialList.get(position).getFirstName());
        TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
        jobLoc.setText(Html.fromHtml("<b>Contact: </b>" + potentialList.get(position).getContactNo()));
        TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
        jobComp.setText(Html.fromHtml("<b>Email Id: </b>"+potentialList.get(position).getEmail()));
        TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
        jobExp.setText(Html.fromHtml("<b>Location: </b>"+potentialList.get(position).getLocation()));
        TextView keySkills = (TextView)convertView.findViewById(R.id.contactPerson);
        keySkills.setText(Html.fromHtml("<b>Key Skills: </b>"+potentialList.get(position).getKeySkills()));
        TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
        courseFee.setText(Html.fromHtml("<b>Education: </b>" +""));
        TextView activeSince = (TextView)convertView.findViewById(R.id.activeSince);
        activeSince.setText(Html.fromHtml("<b>Active Since: </b>" +CommonUtils.getCleanDate(potentialList.get(position).getUpDateOn())));
        ImageView chatImg = (ImageView)convertView.findViewById(R.id.chatImg);
        ImageView emailImg = (ImageView)convertView.findViewById(R.id.emailImg);
        ImageView callImg = (ImageView)convertView.findViewById(R.id.callImg);
        chatImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("chat",position);
            }
        });

        emailImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("email",position);
            }
        });

        callImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("call",position);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return potentialList.size();
    }


    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return potentialList.get(position);
    }
    private class ViewHolder {
    }

    public void setOnCartChangedListener(OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status, int position);
    }


}
