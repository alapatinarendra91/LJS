package com.localjobserver.TrainingInstitute;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonCoursesListAdapter;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.freshers.CourceDescriptionActivity;
import com.localjobserver.models.PotentialModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.List;

public class PotentialCandidatesFragment extends Fragment implements PotentialAdopter.OnCartChangedListener{

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View rootView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView potentialCandidatesListView;
    private TextView noCountTxt;
    private ProgressDialog progressDialog;
    private CommonCoursesListAdapter commonArrayAdapter;
    private ArrayList<PotentialModel> potentialList;
    private PotentialAdopter resumesListAdpter = null;
    Activity mHomeActivity;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppliedJobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PotentialCandidatesFragment newInstance(String param1, String param2) {
        PotentialCandidatesFragment fragment = new PotentialCandidatesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PotentialCandidatesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

         mHomeActivity=this.getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.activity_potential_candidates_fragment, container, false);
        potentialCandidatesListView = (ListView)rootView.findViewById(R.id.potentialsList);
        noCountTxt = (TextView)rootView.findViewById(R.id.noCountTxt);
//        getJobsData();
        getPotentialCandidates();
        potentialCandidatesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                startActivity(new Intent(getActivity(), JobDescriptionActivity.class).putExtra("jobItems", (ArrayList<FresharsCoursesModel>) jobList).putExtra("_id", ((FresharsCoursesModel) jobList.get(position)).getId()).putExtra("selectedPos", position).setAction("fromApplied"));

                Intent jobIntent = new Intent(getActivity(), CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
//                jobIntent.putExtra("_id", ((RecommendedCourses) potentialList.get(position)).get_id());
                jobIntent.setAction("applied");
                startActivity(jobIntent);

            }
        });
        return rootView;
    }

    public void getPotentialCandidates(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        potentialList = new ArrayList<>();
        LiveData.getPotentials(new ApplicationThread.OnComplete<List<PotentialModel>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    potentialList = (ArrayList) result;
                    if (null != potentialList && potentialList.size() > 0) {

                        potentialList = (ArrayList) result;
                        CommonUtils.PotentialList = (ArrayList) result;
                        resumesListAdpter = new PotentialAdopter(getActivity(), potentialList);
                        potentialCandidatesListView.setAdapter(resumesListAdpter);
                        resumesListAdpter.setOnCartChangedListener(PotentialCandidatesFragment.this);

//                        Intent uiUpdateIntent = new Intent("update");
//                        uiUpdateIntent.putExtra("type", 0);
//                        uiUpdateIntent.putExtra("appliedCount", potentialList.size());
//                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                    } else {
                        potentialCandidatesListView.setVisibility(View.GONE);
                        noCountTxt.setText("No Potential Candidates");
                    }
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.potentialCandidates,  CommonUtils.getUserEmail(getActivity())));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void setCartClickListener(String clickItem,final int selectPos) {
         if (clickItem.contains("call")){
            CommonUtils.showToast("Call",getActivity());
        }else if (clickItem.contains("email")){
             CommonUtils.showToast("Email", getActivity());
        }else {
             CommonUtils.showToast("Chat",getActivity());
        }
    }

//    class CoursesListAdpter extends BaseAdapter {
//
//        Context context;
//        public CoursesListAdpter(Context context) {
//            super();
//            this.context=context;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                LayoutInflater mInflater= LayoutInflater.from(context);
//                convertView = mInflater.inflate(R.layout.courses_list_item, null);
//            }
//            TextView jobTitle = (TextView)convertView.findViewById(R.id.courseNAME);
//            jobTitle.setText(courcesList.get(position).getCourseName());
//            TextView jobLoc = (TextView)convertView.findViewById(R.id.institueNAME);
//            jobLoc.setText(""+courcesList.get(position).getInstituteName());
//            TextView jobComp = (TextView)convertView.findViewById(R.id.locNAME);
//            jobComp.setText(""+courcesList.get(position).getCity());
//            TextView jobExp = (TextView)convertView.findViewById(R.id.localityNAME);
//            jobExp.setText(""+courcesList.get(position).getLocation());
//            TextView keySkills = (TextView)convertView.findViewById(R.id.contactPerson);
//            keySkills.setText(""+courcesList.get(position).getContactNumber());
//            TextView courseFee = (TextView)convertView.findViewById(R.id.courseFee);
//            courseFee.setText(""+courcesList.get(position).getCourseFee());
//            return convertView;
//        }
//
//
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return courcesList.size();
//        }
//
//
//        @Override
//        public long getItemId(int arg0) {
//            // TODO Auto-generated method stub
//            return 0;
//        }
//
//        @Override
//        public Object getItem(int position) {
//            // TODO Auto-generated method stub
//            return courcesList.get(position);
//        }
//
//    }

}
