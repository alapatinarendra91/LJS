package com.localjobserver.TrainingInstitute;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.ui.AboutUsActivity;
import com.localjobserver.ui.FeedbackActivity;
import com.localjobserver.ui.SettingsActivity;
import co.talentzing.R;


public class InstitutionNavigationActivity extends ActionBarActivity implements InstitutenavigateFragment.NavigationDrawerCallbacks {

    private InstitutenavigateFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private SharedPreferences appPrefs = null;
    private int userType = 0;
    private Bundle bundle1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_institution_navigation);

        mNavigationDrawerFragment = (InstitutenavigateFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp( R.id.navigation_drawer,(DrawerLayout) findViewById(R.id.drawer_layout));

        appPrefs =  this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE,0);

        mTitle = "ProfileView";
        restoreActionBar();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent in = null;
        switch (position) {
            case 0:
                if (CommonUtils.isTrainerLoggedIn(getApplicationContext())) {
                    mTitle = "ProfileView";
                    restoreActionBar();
                    fragmentManager.beginTransaction().replace(R.id.container, TrainingViewProfileActivity.newInstance(position + 1)).commit();
                }
                else{
                    mTitle = "Login";
                    restoreActionBar();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(position + 1, "freshers")).commit();
                }


            break;
            case 1:
                if (CommonUtils.isTrainerLoggedIn(this)) {
                    bundle1 = new Bundle();
                    bundle1.putString("POSITION", "");
                    startActivity(new Intent(InstitutionNavigationActivity.this, TrainerPostCourseActivity.class).putExtra("POSITION",bundle1).putExtra("POST","POST"));
                } else {
                    mTitle = "Login";
                    restoreActionBar();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginScreen.LoginFragment.newInstance(position + 1, "freshers")).commit();
                }
                break;
            case 2:
                if (CommonUtils.isTrainerLoggedIn(this)) {
                    startActivity(new Intent(InstitutionNavigationActivity.this, TrainerManagePostingActivity.class));
                } else  {
                    bundle1 = new Bundle();
                    bundle1.putString("flag", "0");
                    startActivity(new Intent(this, TrainingInstituteRegistratiojn.class).putExtra("flag", bundle1).putExtra("POST","POST"));
                }
                break;
            case 3:

                if (CommonUtils.isTrainerLoggedIn(this)) {
                    fragmentManager.beginTransaction().replace(R.id.container, TrainerE_learnings.newInstance("", "")).commit();
                } else  {
                    Toast.makeText(this, "Please login or register to access this feature",Toast.LENGTH_SHORT).show();
                }

                break;
            case 4:
                if (CommonUtils.isTrainerLoggedIn(this)) {
                    startActivity(new Intent(InstitutionNavigationActivity.this, SettingsActivity.class));
                } else  {
                    Toast.makeText(this, "Please login or register to access this feature",Toast.LENGTH_SHORT).show();
                }

                break;
            case 5:
                startActivity(new Intent(InstitutionNavigationActivity.this, AboutUsActivity.class));
                break;
            case 6:
                startActivity(new Intent(InstitutionNavigationActivity.this, FeedbackActivity.class));
                break;
            case 7:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://localjobserver.com/PrivacyPolacy.aspx"));
                startActivity(i);
                break;
            case 8:
                Intent ii = new Intent(Intent.ACTION_VIEW);
                ii.setData(Uri.parse("https://localjobserver.com/FrequentlyAskedQuestions.aspx"));
                startActivity(ii);
                break;

            case 9:
                Intent terms = new Intent(Intent.ACTION_VIEW);
                terms.setData(Uri.parse("http://ljsjobs.com/TermsAndConditions.aspx"));
                startActivity(terms);
                break;

            case 10:
                startActivity(new Intent(InstitutionNavigationActivity.this, PotentialInfoActivity.class));
                break;

            case 11:
                CommonUtils.logoutDialog(userType, InstitutionNavigationActivity.this);

                break;
                    }
    }


    Dialog dialog;
    public void designAboutDialog(){
        dialog = new Dialog(InstitutionNavigationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aboutus);
        dialog.setCanceledOnTouchOutside(true);
        final TextView versionTxt = (TextView)dialog.findViewById(R.id.version);
        versionTxt.setText(""+ CommonUtils.getCopyright(InstitutionNavigationActivity.this));
        dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    public void onSectionAttached(String title) {
        mTitle = title;
        restoreActionBar();

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_info, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

}
