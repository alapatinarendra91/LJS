package com.localjobserver.TrainingInstitute;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.freshers.CourceDescriptionActivity;
import com.localjobserver.helper.SocialHelper;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.slider.BaseActivity;
import com.localjobserver.slider.SlidingMenu;
import com.localjobserver.social.LinkedInSocialDialog;
import co.talentzing.R;

import java.util.ArrayList;
import java.util.List;

public class TrainerManagePostingActivity extends BaseActivity {
    private SlidingMenu sampleMenu;
    private ProgressDialog progressDialog;
    public TrainerManagePostingActivity() {
        super(R.string.title_activity_job_list);
    }
    private JobListAdapter jobLllistAd = null;
    private ListView jobsListView;
    public ArrayList<RecommendedCourses> mTrainerPostCourseModel;
    private ActionBar actionBar = null;
    //    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    /* Any number for uniquely distinguish your request */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    private ProgressDialog pDialog;
    private static SharedPreferences mSharedPreferences;
    private ImageView editBtn;
    private String searchStr,ridStr,locationStr,selectedJobType;
    private String preparedUrltoGetJobs = "";
    private SocialHelper mSocialHelper = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_job_list);
        getSupportActionBar().setTitle("Manage Post");

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        jobsListView = (ListView)findViewById(R.id.jobListView);
        editBtn = (ImageView)findViewById(R.id.searchEdit);


        getPostedCorsesData(String.format(Config.LJS_BASE_URL + Config.getCoursesTrainer, "" + CommonUtils.getUserEmail(getApplicationContext())));

        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
        jobsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent jobIntent = new Intent(TrainerManagePostingActivity.this, CourceDescriptionActivity.class);
                jobIntent.putExtra("selectedPos", position);
                jobIntent.putExtra("_id", ((RecommendedCourses) mTrainerPostCourseModel.get(position)).get_id());
                jobIntent.putExtra("module", "trainer");
                jobIntent.setAction("applied");
                startActivity(jobIntent);
            }
        });

        mSocialHelper = new SocialHelper(TrainerManagePostingActivity.this, this,null,mTrainerPostCourseModel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_job_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

//        if (id == R.id.action_filter) {
//           sampleMenu.toggle();
//        }

        return super.onOptionsItemSelected(item);
    }


    public void getPostedCorsesData(final String url){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(TrainerManagePostingActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        mTrainerPostCourseModel = new ArrayList<>();
        LiveData.getCoursePostDetails(new ApplicationThread.OnComplete<List<RecommendedCourses>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(TrainerManagePostingActivity.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (success == true) {
                    CommonUtils.CoursesList_applied = (ArrayList) result;
                    mTrainerPostCourseModel = (ArrayList) result;
                    jobLllistAd = new JobListAdapter(TrainerManagePostingActivity.this);
                    jobsListView.setAdapter(jobLllistAd);
                    progressDialog.dismiss();
                }
            }
        }, url);
    }

    class JobListAdapter extends BaseAdapter {

        Activity context;
        public JobListAdapter(Activity context) {
            super();
            this.context=context;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater mInflater= LayoutInflater.from(context);
                convertView = mInflater.inflate(R.layout.custom_manage_post, null);
            }
            TextView standardDiscount = (TextView)convertView.findViewById(R.id.standardDiscount);
            standardDiscount.setText(""+((RecommendedCourses) mTrainerPostCourseModel.get(position)).getCourseName());
            TextView location = (TextView) convertView.findViewById(R.id.location);
            location.setText(Html.fromHtml("<b>Location: </b>" + (((RecommendedCourses)mTrainerPostCourseModel.get(position)).getLocation())));
            TextView instituteName = (TextView)convertView.findViewById(R.id.instituteName);
            instituteName.setText(Html.fromHtml("<b>Institute Name: </b>"+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).getInstituteName())));
            TextView locality = (TextView)convertView.findViewById(R.id.locality);
            locality.setText(Html.fromHtml("<b>Locality: </b>"+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).getLocation())));
            TextView contactPerson = (TextView)convertView.findViewById(R.id.contactPerson);
            contactPerson.setText(Html.fromHtml("<b>Contact Person: </b>"+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).getContactPerson())));
            TextView coursefee = (TextView)convertView.findViewById(R.id.coursefee);
            coursefee.setText(Html.fromHtml("<b>Course fee: </b>"+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).getCourseFee())));
            TextView discountfee = (TextView)convertView.findViewById(R.id.discountfee);
            discountfee.setText(Html.fromHtml("<b>After Discounted Fee: </b>"+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).getAfterDiscountedfee())));
            TextView startdate = (TextView)convertView.findViewById(R.id.startdate);
            startdate.setText(Html.fromHtml("<b>Start date: </b>"+CommonUtils.getCleanDate(((RecommendedCourses) mTrainerPostCourseModel.get(position)).getStartDate())));
            TextView matchingProfile = (TextView)convertView.findViewById(R.id.matchingProfile);
//            matchingProfile.setText("Matching Profiles: "+(((RecommendedCourses)mTrainerPostCourseModel.get(position)).get));

            ImageView linkedInImg = (ImageView)convertView.findViewById(R.id.linkedInImg);
            ImageView fbImg = (ImageView)convertView.findViewById(R.id.fbImg);

            linkedInImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuffer lnStr = new StringBuffer();
                    lnStr.append("\nCourse Title:");
                    lnStr.append("\n");
                    lnStr.append(((RecommendedCourses) mTrainerPostCourseModel.get(position)).getCourseName());
                    lnStr.append("\n");
                    lnStr.append("More Info At:");
                    lnStr.append("\n");
                    lnStr.append("https://localjobserver.com/home.aspx");
                    startActivity(new Intent(context, LinkedInSocialDialog.class).putExtra("lnData", lnStr.toString()));
                }
            });

            fbImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Session.openActiveSession(context, true, new Session.StatusCallback() {
                        // callback when session changes state
                        public void call(Session session, SessionState state, Exception exception) {
                            if (session.isOpened()) {
                                //Fresher responce and Trainer respone are same
                                mSocialHelper.publishFeedDialog(position, "fresher");
                            }
                        }
                    });
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mTrainerPostCourseModel.size();
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return mTrainerPostCourseModel.get(position);
        }

    }


}
