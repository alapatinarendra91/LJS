package com.localjobserver.TrainingInstitute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.jobsinfo.MainFragmentStatePagerAdapter;
import com.localjobserver.viewpagerindicator.TabPageIndicator;
import co.talentzing.R;

import java.util.List;

public class PotentialInfoActivity extends ActionBarActivity {

    private ViewPager pager;
    private TabPageIndicator indicator;
    private MainFragmentStatePagerAdapter mStateAdapter;
    private ActionBar actionBar = null;
    private SharedPreferences appPrefs = null;
    private int userType = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potential_info);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        pager = (ViewPager) findViewById(R.id.pager);
        indicator = (TabPageIndicator) findViewById(R.id.indicator);
        appPrefs =  this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);

        final List<Bundle> payloads = new java.util.ArrayList<Bundle>();
        String[] titles = new String[2];
        final Bundle topPayload = new Bundle();
        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, PotentialCandidatesFragment.class.getName());
//        topPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Applied");
        payloads.add(topPayload);
        titles[0] = "Potential Candidates";

        if (userType == 3){

        final Bundle tvShowsPayload = new Bundle();
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, PotentialTrainers.class.getName());
        tvShowsPayload.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Recommended");
        payloads.add(tvShowsPayload);
        titles[1] = "Potential Trainers";
        }


//        final Bundle tvShowsPayload1 = new Bundle();
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_CLAZZ_KEY, SavedJobsFragment.class.getName());
//        tvShowsPayload1.putString(MainFragmentStatePagerAdapter.FRAGMENT_TITLE_KEY, "Saved");
//        payloads.add(tvShowsPayload1);

        mStateAdapter = new MainFragmentStatePagerAdapter(this.getSupportFragmentManager(), getApplicationContext(), payloads, titles);
        pager.setAdapter(mStateAdapter);

        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(opcl);
        indicator.setCurrentItem(0);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("update"));
    }

    private final ViewPager.OnPageChangeListener opcl = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(final int position) {

        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }else{
                    this.finish();
                }
                break;
        }
        return true;
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("update")) {
                    if (intent.getIntExtra("type", 0) == 0) {
                        int appliedCount = intent.getIntExtra("appliedCount", 0);
                        mStateAdapter.updateTitle("Applied"+" ("+appliedCount+")" , 0);
                        indicator.notifyDataSetChanged();
                    } else {
                        int appliedCount = intent.getIntExtra("appliedCount", 0);
                        mStateAdapter.updateTitle("Recommended"+" ("+appliedCount+")" , 1);
                        indicator.notifyDataSetChanged();
                    }
                }
            }
        }
    };
}
