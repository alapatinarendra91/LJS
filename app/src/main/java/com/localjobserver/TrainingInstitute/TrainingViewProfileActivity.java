package com.localjobserver.TrainingInstitute;

/**
 * Created by admin on 01-04-2015.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.jobsinfo.MainJobsInfoFragment;
import com.localjobserver.models.TrainerRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.onlineTrainers.RegisterOnlineTrainer;
import co.talentzing.R;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

public class TrainingViewProfileActivity extends Fragment {

    private TextView tvname,tvmailid,tvphonenum,tvlocation,tvlatupdateddate;
    private static ProgressDialog progressDialog;
    public static RelativeLayout profilepic;
    private Button editprofilebtn,downloadresumebtn;
    public List<TrainerRegistrationModel> trainerProfileList;
    public TrainerRegistrationModel mSetupData;
    private Context context;
    private ActionBar actionBar = null;
    private Bitmap bmp = null;
    private static Drawable dr = null;
    private ImageView edit_dis;
    private String profile_pic = "";
    private SharedPreferences appPrefs = null;
    private int userType = 0;
    private byte[] seekerImageData;
    private static String  compressedStr;
    private View rootView;
    private static final String ARG_PARAM1 = "param1";
    private Intent updateProf;


    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.activity_training_view_profile, container, false);


        context=getActivity();
        mSetupData = new TrainerRegistrationModel();
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);

//        actionBar = (ActionBarActivity)getActivity().getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle("Profile Information");

        tvname=(TextView)rootView.findViewById(R.id.profilename_dis);
        tvmailid=(TextView)rootView.findViewById(R.id.mailid);
        tvphonenum=(TextView)rootView.findViewById(R.id.phonenum);
        tvlocation=(TextView)rootView.findViewById(R.id.location);
        tvlatupdateddate=(TextView)rootView.findViewById(R.id.lastupdateddate);
        edit_dis = (ImageView)rootView.findViewById(R.id.edit_dis);
        profilepic=(RelativeLayout)rootView.findViewById(R.id.sec_rel);

        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        userType = appPrefs.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);
        profile_pic = appPrefs.getString(CommonUtils.getUserEmail(getActivity()), null);



        if (profile_pic != null){
            try {
                String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                BitmapDrawable dr = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray
                        (seekerImageData, 0, seekerImageData.length));
                profilepic.setBackground(dr);
                profilepic.invalidate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else
            getProfilePick();

//        profilepic.setBackgroundResource(R.drawable.sample);
        editprofilebtn=(Button)rootView.findViewById(R.id.profile_details_edit_btn);
        editprofilebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment newFragment = MainProfileFragment.newInstance();
//                getSupportFragmentManager().beginTransaction().replace(android.R.id.content, newFragment).addToBackStack(null).commit();

                Bundle searchBundle = new Bundle();
                searchBundle.putString("flag", "1");

                if (userType == 3)
                 updateProf = new Intent(getActivity(),TrainingInstituteRegistratiojn.class);
                else if (userType == 5)
                   updateProf = new Intent(getActivity(),RegisterOnlineTrainer.class);
                
                updateProf.putExtra("flag",searchBundle);
                startActivity(updateProf);

            }
        });
        downloadresumebtn=(Button)rootView.findViewById(R.id.profile_details_download_btn);


        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(android.R.id.content, new PlaceholderFragment()).addToBackStack(null)
                        .commit();
            }
        });
        Button recJobsBtn = (Button)rootView.findViewById(R.id.recommandedjobsList_btn);
        recJobsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = MainJobsInfoFragment.newInstance(2, "rec");
                getActivity().getSupportFragmentManager().beginTransaction().add(android.R.id.content, newFragment).commit();

            }
        });


        getProfileDate();

        return rootView;

    }

    public static TrainingViewProfileActivity newInstance(int param1) {
        TrainingViewProfileActivity fragment = new TrainingViewProfileActivity();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }


    public void getProfilePick(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
//        progressDialog.show();
        LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }


                if (result != null) {
                        try {
                            seekerImageData = Base64.decode(CommonUtils.replaceNewLines(result.toString()), Base64.DEFAULT);
                            BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                                    (seekerImageData, 0, seekerImageData.length));
                            profilepic.setBackground(dr);
                            profilepic.invalidate();
                        } catch (Exception e) {

                        }

                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getTrainerProfilePic, CommonUtils.getUserEmail(getActivity())));
    }


    public  void getProfileDate() {
        getCompleteProfile();
        getData();
        bindData();
    }


    private void updateJobSeekerProfile(LinkedHashMap<String,String> input){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Uploading user data..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(getActivity(), "Error while registering user", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (result != null) {
                    if (result.toString().equalsIgnoreCase("true")) {
                        Toast.makeText(getActivity(),"Profile Details updated successfully", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.modifyJSDetails,mSetupData.getEmail(), "" + requestString));
    }

    private void getCompleteProfile(){
        trainerProfileList = DatabaseAccessObject.getTrainerProfile(Queries.getInstance().getDetails("TrainersRegistration"), context);
    }
    private void getData()
    {

//        seekerdetails= DatabaseAccessObject.getJobSeekerDetails(Queries.getInstance(). getJobSeekerDetails(),context);
    }

    private void bindData()
    {
        for(int i=0;i<trainerProfileList.size();i++)
        {
            tvname.setText(trainerProfileList.get(i).getInstituteName());
            tvmailid.setText(trainerProfileList.get(i).getEmail());
            tvphonenum.setText(trainerProfileList.get(i).getContactNo());
            tvlocation.setText(trainerProfileList.get(i).getLocation());
            tvlatupdateddate.setText("Last Update: "+CommonUtils.getCleanDate(trainerProfileList.get(i).getUpdateOn()));

//            byte[] imhvbytes=trainerProfileList.get(i).getPhoto();
//                bmp= getBitmap(imhvbytes);
            if(bmp!=null){
                dr = new BitmapDrawable(context.getResources(), bmp);
                (profilepic).setBackground(dr);
            }

        }

    }
    public Bitmap getBitmap(byte[] bitmap) {
        return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
    }

    private static   Bitmap ShrinkBitmap(String file, int width, int height){

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
        int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

        if (heightRatio > 1 || widthRatio > 1)
        {
            if (heightRatio > widthRatio)
            {
                bmpFactoryOptions.inSampleSize = heightRatio;
            } else {
                bmpFactoryOptions.inSampleSize = widthRatio;
            }
        }

        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("update"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){

            case android.R.id.home:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }else{
                    getActivity().finish();
                }
                break;

        }
        return true;

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static  int CURRENT_DISPLAY_SCREEN = 0, FILE_CHOOSER = 1, SELECT_PICTURE =2, FROMCAM = 3;
        private ImageView profilePick;
        private byte[] seekerImageData;
        private View rootView = null;
        private Button choosePic = null,  cancelBtn = null;
        private String profile_pic = "";
        private SharedPreferences appPrefs = null;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_profile2, container, false);
            profilePick = (ImageView)rootView.findViewById(R.id.proFic);
            appPrefs =  getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
            profile_pic = appPrefs.getString(CommonUtils.getUserEmail(getActivity()), null);

            if(null != dr){
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
            }

            if (profile_pic != null){
                try {
                    String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                    seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                    BitmapDrawable dr = new BitmapDrawable(getActivity().getResources(), BitmapFactory.decodeByteArray
                            (seekerImageData, 0, seekerImageData.length));
                    profilePick.setImageDrawable(dr);
                    profilePick.invalidate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            choosePic = (Button)rootView.findViewById(R.id.edit);
            choosePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(choosePic.getText().toString().equalsIgnoreCase("edit")){
                        profilePic();
                    }else{

                        uploadUserImage();

                    }
                }
            });



            cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    if (fm.getBackStackEntryCount() > 0) {
//                        fm.popBackStack();
//                    }else{
//                        getActivity().finish();
//                    }

                    DeleteImage();
                }
            });


            return rootView;
        }

        public void uploadUserImage() {

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
//                progressDialog.show();
            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    android.util.Log.i("", "Result is : " + result.toString());
                    if (result != null && result.toString().contains("true")) {
                        progressDialog.dismiss();

                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);
                        editor.commit();
                        Toast.makeText(getActivity(), "Your photo has been updated successfully", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        BitmapDrawable dr = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray
                                (seekerImageData, 0, seekerImageData.length));
                        profilepic.setBackground(dr);

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        } else {
                            getActivity().finish();
                        }

                        Intent uiUpdateIntent = new Intent("updateProfilePick");
                        uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

//                        Intent uiUpdateIntent = new Intent("update");
//                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

                    }
                }
            }, "" + CommonUtils.getUserEmail(getActivity()) + ".png", CommonUtils.getUserEmail(getActivity()), compressedStr, "uploadTrainerImage", getActivity(), 0);

        }

        public void DeleteImage() {

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
//                progressDialog.show();
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    android.util.Log.i("", "Result is : " + result.toString());
                    if (result != null && result.toString().contains("true")) {
                        progressDialog.dismiss();

                        SharedPreferences.Editor editor = appPrefs.edit();
                        editor.putString(CommonUtils.getUserEmail(getActivity()), "");
                        editor.commit();
                        Toast.makeText(getActivity(),"Delete Profile pic",Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        Intent uiUpdateIntent = new Intent("updateProfilePick");
                        uiUpdateIntent.putExtra("imageString", "");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
                        Intent uiUpdateIntentupdate = new Intent("update");
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntentupdate);

                        if (fm.getBackStackEntryCount() > 0) {
                            fm.popBackStack();
                        }else{
                            getActivity().finish();
                        }

                    }
                }
            }, "default" ,CommonUtils.getUserEmail(getActivity()), "", "uploadTrainerImage", getActivity(), 0);

        }


        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
                        //selectImage();
                        // Calls an Intent that opens camera to take a picture
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, FROMCAM);
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        //Call an intent that opens gallery to select photo
                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == FROMCAM) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(filePath,80,80);
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                Matrix matrix = new Matrix();
                matrix.postRotate(CommonUtils.getImageOrientation(f.getAbsolutePath()));
//                Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(),yourSelectedImage.getHeight(), matrix, true);
                dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                seekerImageData = outputStream.toByteArray();

//                Intent uiUpdateIntent = new Intent("updateProfilePick");
//                uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
//                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);

            } else if (requestCode == SELECT_PICTURE){
                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inPurgeable = true;
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath,options);
                dr = new BitmapDrawable(yourSelectedImage);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                yourSelectedImage.compress(Bitmap.CompressFormat.JPEG, 90,outputStream);
                seekerImageData = outputStream.toByteArray();
                profilePick.setImageDrawable(dr);
                profilePick.invalidate();
                choosePic.setText("OK");

//                Intent uiUpdateIntent = new Intent("updateProfilePick");
//                uiUpdateIntent.putExtra("imageString", Base64.encodeToString(seekerImageData, Base64.DEFAULT));
//                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(uiUpdateIntent);
            }// for if
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                if (intent.getAction().equals("updateProfilePick")) {
                    // restore from shared prefs if there's value there
                    if(null != dr && !intent.getStringExtra("imageString").equalsIgnoreCase(""))
                        (profilepic).setBackground(dr);
                    else
                        (profilepic).setBackgroundResource(R.drawable.app_icon);
                }
            }
        }
    };

//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent != null && intent.getAction() != null) {
//                if (intent.getAction().equals("update")) {
//                    // restore from shared prefs if there's value there
//                    if(null != dr)
//                        (profilepic).setBackground(dr);
//                }
//            }
//        }
//    };

}
