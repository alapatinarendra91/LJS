package com.localjobserver.TrainingInstitute;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonArrayAdapterDropDown;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.FileChooser;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.models.TrainerRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

public class TrainerPostCourseActivity extends ActionBarActivity {

    private ActionBar actionBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer_post_course);


        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (getIntent().getStringExtra("POST") == null)
            actionBar.setTitle("Edit Post Details");
        else
            actionBar.setTitle("Post Course Details");
        if (savedInstanceState == null) {

            Fragment itemFrag = new TraininPostCourseFragment();
            itemFrag.setArguments(getIntent().getBundleExtra("POSITION"));
            getSupportFragmentManager().beginTransaction().add(R.id.container, itemFrag).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class TraininPostCourseFragment extends Fragment {

        private View flipper;
        private Button choose_bouchure,submitbtn,cancilbtnbtn;
        private TextView resumeNameTxt;
        private EditText Institute_NameEdt,contactnameEdt,emailEdt,contactnumberEdt,landlineEdt,corsesfeeEdt,eligibilityEdt,instructorNameEdt,coursestartsEdt,durationEdt,classStartTimeEdt,classendTimeEdt,labStartTimeEdt,labendTimeEdt, batchSizeEdt,discountvalueEdt,groupDiscountEdt,groupDiscountValueEdt,websiteEdt,descriptionEdt,venueEdt,addressEdt;
        private MultiAutoCompleteTextView areaEdt,courseNameEdt;
        private CheckBox normalCheck, fastCheck,projectCheck,everyDayCheck,weekendCheck,onlineCheck,sameAddresscheck;
        private RadioButton openRadio, closeRadio;
        private RadioGroup radiostatus;
        private Spinner discountSpin,locationSpin,demoClassSpin,courseTrainerSpin,placementSupportSpin;
        private boolean take_photo = false;
        private static final int SELECT_PICTURE =1,FROMCAM = 2,FILE_CHOOSER = 3;
        private byte[] seekerImageData;
        private ProgressDialog progressDialog;
        private String selectedDay = "", compressedStr = "";
        private SharedPreferences preferences;
        private RecommendedCourses mDataModel;
        private ArrayList<RecommendedCourses> bindData;
        private LinkedHashMap<String, String> locationsMap;
        private Calendar myCalendar = Calendar.getInstance();
        public int mHour;
        public int mMinute;
        private Bundle bundlePosition;
        private String configUrl;
        private String fileSelected = "",resumeFile;
        private File resume;
        private String brochureFromServer;
        private static final int RESULT_ERROR = 0;
        private static final int RESULT_BROCHURE = 1;
        private static final int RESULT_BROCHURE_SAVED = 2;
        private String BrochureName;
        private String sdcard_path;
        private List<TrainerRegistrationModel> trainerProfileList;
        private ArrayList<String> keySkillsList;
        private int userType = 0;

        public TraininPostCourseFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            flipper = inflater.inflate(R.layout.fragment_trainer_post_course, container, false);

            initViews();
            setViews();
            getLocations();
            getKeyWords();
//            updateLabel();
            bundlePosition = getArguments();
            if (!bundlePosition.getString("POSITION").equals(""))
                bindData();
            return flipper;
        }

        private void initViews() {
            progressDialog = new ProgressDialog(getActivity());
            mDataModel = new RecommendedCourses();
            preferences = getActivity().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);
            userType = preferences.getInt(CommonKeys.LJS_PREF_USERTYPE, 0);

            Institute_NameEdt  = (EditText)flipper.findViewById(R.id.Institute_NameEdt);
            contactnameEdt  = (EditText)flipper.findViewById(R.id.contactnameEdt);
            emailEdt  = (EditText)flipper.findViewById(R.id.emailEdt);
            areaEdt  = (MultiAutoCompleteTextView)flipper.findViewById(R.id.areaEdt);
            areaEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            courseNameEdt  = (MultiAutoCompleteTextView)flipper.findViewById(R.id.courseNameEdt);
            courseNameEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            contactnumberEdt  = (EditText)flipper.findViewById(R.id.contactnumberEdt);
            landlineEdt  = (EditText)flipper.findViewById(R.id.landlineEdt);
            addressEdt  = (EditText)flipper.findViewById(R.id.addressEdt);
            corsesfeeEdt  = (EditText)flipper.findViewById(R.id.corsesfeeEdt);
            eligibilityEdt  = (EditText)flipper.findViewById(R.id.eligibilityEdt);
            instructorNameEdt  = (EditText)flipper.findViewById(R.id.instructorNameEdt);
            coursestartsEdt  = (EditText)flipper.findViewById(R.id.coursestartsEdt);
            durationEdt  = (EditText)flipper.findViewById(R.id.durationEdt);
            classStartTimeEdt  = (EditText)flipper.findViewById(R.id.classStartTimeEdt);
            classendTimeEdt  = (EditText)flipper.findViewById(R.id.classendTimeEdt);
            labStartTimeEdt  = (EditText)flipper.findViewById(R.id.labStartTimeEdt);
            labendTimeEdt  = (EditText)flipper.findViewById(R.id.labendTimeEdt);
            batchSizeEdt  = (EditText)flipper.findViewById(R.id.batchSizeEdt);
            discountvalueEdt  = (EditText)flipper.findViewById(R.id.discountvalueEdt);
            groupDiscountEdt  = (EditText)flipper.findViewById(R.id.groupDiscountEdt);
            groupDiscountValueEdt  = (EditText)flipper.findViewById(R.id.groupDiscountValueEdt);
            websiteEdt  = (EditText)flipper.findViewById(R.id.websiteEdt);
            descriptionEdt  = (EditText)flipper.findViewById(R.id.descriptionEdt);
            venueEdt  = (EditText)flipper.findViewById(R.id.venueEdt);

            choose_bouchure  = (Button)flipper.findViewById(R.id.choose_bouchure);
            submitbtn  = (Button)flipper.findViewById(R.id.submitbtn);
            cancilbtnbtn  = (Button)flipper.findViewById(R.id.cancilbtnbtn);

            resumeNameTxt   = (TextView)flipper.findViewById(R.id.resumeName);

            normalCheck = (CheckBox)flipper.findViewById(R.id.normalCheck);
            fastCheck = (CheckBox)flipper.findViewById(R.id.fastCheck);
            projectCheck = (CheckBox)flipper.findViewById(R.id.projectCheck);
            everyDayCheck = (CheckBox)flipper.findViewById(R.id.everyDayCheck);
            weekendCheck = (CheckBox)flipper.findViewById(R.id.weekendCheck);
            onlineCheck = (CheckBox)flipper.findViewById(R.id.onlineCheck);
            sameAddresscheck = (CheckBox)flipper.findViewById(R.id.sameAddresscheck);

            openRadio = (RadioButton) flipper.findViewById(R.id.openRadio);
            closeRadio = (RadioButton) flipper.findViewById(R.id.closeRadio);
            radiostatus  = (RadioGroup)flipper.findViewById(R.id.radiostatus);
            discountSpin = (Spinner) flipper.findViewById(R.id.discountSpin);
            locationSpin = (Spinner) flipper.findViewById(R.id.locationSpin);
            demoClassSpin = (Spinner) flipper.findViewById(R.id.demoClassSpin);
            courseTrainerSpin = (Spinner) flipper.findViewById(R.id.courseTrainerSpin);
            placementSupportSpin = (Spinner) flipper.findViewById(R.id.placementSupportSpin);

        }

        private void setViews() {
            trainerProfileList = DatabaseAccessObject.getTrainerProfile(Queries.getInstance().getDetails("TrainersRegistration"), getActivity());
            Institute_NameEdt.setText("" + trainerProfileList.get(0).getInstituteName());
            emailEdt.setText("" + trainerProfileList.get(0).getEmail());
            contactnameEdt.setText("" + trainerProfileList.get(0).getFirstName());
//            emailEdt.setEnabled(false);

            discountSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.Discount_types)));
            demoClassSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.DemoClass_array)));
            courseTrainerSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.CourseTrainer_array)));
            placementSupportSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.PlacementSupport_array)));

            discountSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    selectedDay = parent.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            resumeNameTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String path = Environment.getExternalStorageDirectory() + "/LJS/Brochure" + "/" + bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id() + ".pdf";
                    File f = new File(path);
                    if (!bundlePosition.getString("POSITION").equals("")) {

                        if (CommonUtils.isBrochureExisted(bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id().toString())) {
                            fileSelected = bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id().toString() + ".pdf";
                            resumeNameTxt.setText("" + fileSelected);
                            openAttachment(f, fileSelected);
                        } else {
                            getBrochure(bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id());
                        }
                    } else
                        openAttachment(f, fileSelected);


                }
            });

            coursestartsEdt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            classStartTimeEdt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            classStartTimeEdt.setText( selectedHour + ":" + selectedMinute);
                            if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                                classStartTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                            else if (String.valueOf(selectedHour).length() == 1)
                                classStartTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                            else if (String.valueOf(selectedMinute).length() == 1)
                                classStartTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                            else
                                classStartTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            classendTimeEdt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            classendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                            if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                                classendTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                            else if (String.valueOf(selectedHour).length() == 1)
                                classendTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                            else if (String.valueOf(selectedMinute).length() == 1)
                                classendTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                            else
                                classendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

                }
            });

            labStartTimeEdt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            labStartTimeEdt.setText( selectedHour + ":" + selectedMinute);
                            if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                                labStartTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                            else if (String.valueOf(selectedHour).length() == 1)
                                labStartTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                            else if (String.valueOf(selectedMinute).length() == 1)
                                labStartTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                            else
                                labStartTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            });
            labendTimeEdt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            labendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                            if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                                labendTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                            else if (String.valueOf(selectedHour).length() == 1)
                                labendTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                            else if (String.valueOf(selectedMinute).length() == 1)
                                labendTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                            else
                                labendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time

                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

                }
            });

            sameAddresscheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        venueEdt.setText(addressEdt.getText().toString());
                    } else {
                        venueEdt.setText("");
                    }
                }
            });

            choose_bouchure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    profilePic();
                    Intent intent = new Intent(getActivity(), FileChooser.class);
                    ArrayList<String> extensions = new ArrayList<String>();
                    extensions.add(".pdf");
                    intent.putStringArrayListExtra("filterFileExtension", extensions);
                    startActivityForResult(intent, FILE_CHOOSER);
                }
            });

            submitbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (validateTrainerRegister() == true && validateMobileNo() == true) {
//                        if (take_photo) {
                            setTrainingData();
                            uploadPostcourseData(toUpload());

//                        } else {
//                            Toast.makeText(getActivity(), "Select Brochure", Toast.LENGTH_SHORT).show();
//                        }

                    } else {
                        Toast.makeText(getActivity(), "Required All fields", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            cancilbtnbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();

                }
            });

        }

        private void openAttachment(File fileIn, String resumeName) {
            // TODO Auto-generated method stub
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);

            Uri uri = Uri.fromFile(fileIn);

            if(resumeName.endsWith(".doc"))
            {
                intent.setDataAndType(uri, "application/msword");
                startActivity(intent);
            }else if(resumeName.endsWith(".pdf"))
            {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }else if(resumeName.endsWith(".rtf"))
            {
                intent.setDataAndType(uri, "application/vnd.ms-excel");
                startActivity(intent);
            }
            else {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }
        }

        private void openAttachmentBrochure() {
            // TODO Auto-generated method stub
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            sdcard_path = String.format(Environment.getExternalStorageDirectory().getAbsolutePath()+"/LJS/Brochures");
            File   fileIn = new File(sdcard_path+ "/"+BrochureName);
            Uri uri = Uri.fromFile(fileIn);

            if(BrochureName.endsWith(".pdf"))
            {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }
            else {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }
        }

        public void  uploadUserBrochure(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            File f = new File(fileSelected);
            System.out.println(f.getName());
            fileSelected = f.getName();
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        progressDialog.dismiss();
                        CommonUtils.showToast("Brochure Posted Successfully",getActivity());
                    }
                }
            }, fileSelected, "563ca46079d0ce12bcbadb5d",resumeFile ,getActivity(),2 );
        }

        private void bindData() {
            submitbtn.setText("Update A Course");
            int position = Integer.parseInt(bundlePosition.getString("POSITION"));
            bindData = CommonUtils.CoursesList_applied;
            resumeNameTxt.setVisibility(View.VISIBLE);

            Institute_NameEdt.setText(bindData.get(position).getInstituteName());
            areaEdt.setText(bindData.get(position).getCity());
            courseNameEdt.setText(bindData.get(position).getCourseName());
            eligibilityEdt.setText(bindData.get(position).getEligibility());
            instructorNameEdt.setText(bindData.get(position).getInstructorName());
            emailEdt.setText(bindData.get(position).getEmail());
            contactnumberEdt.setText(bindData.get(position).getContactNumber());
            landlineEdt.setText(bindData.get(position).getContactNoLandline());
            coursestartsEdt.setText(CommonUtils.getCleanDate(bindData.get(position).getStartDate()));
            durationEdt.setText(bindData.get(position).getDuration());
            classStartTimeEdt.setText(bindData.get(position).getStartTime());
            classStartTimeEdt.setText(bindData.get(position).getStartTime());
            classendTimeEdt.setText(bindData.get(position).getEndTime());
            corsesfeeEdt.setText(bindData.get(position).getCourseFee());
            batchSizeEdt.setText(""+bindData.get(position).getBatchSize());
            discountvalueEdt.setText(bindData.get(position).getDiscountValue());
            groupDiscountEdt.setText(""+bindData.get(position).getGroupCount());
            groupDiscountValueEdt.setText(bindData.get(position).getGroupValue());
            contactnameEdt.setText(bindData.get(position).getContactPerson());
            websiteEdt.setText(bindData.get(position).getCompanyURL());
            descriptionEdt.setText(bindData.get(position).getCourseDescription());
            addressEdt.setText(bindData.get(position).getAddress());
            venueEdt.setText(bindData.get(position).getVenue());
            if (bindData.get(position).getStatus().toString().equalsIgnoreCase("open"))
                openRadio.setChecked(true);
            else
            closeRadio.setChecked(true);

            String[] jobType_array = getResources().getStringArray(R.array.Discount_types);
            for(int i=0 ; i <getResources().getStringArray(R.array.Discount_types).length ; i++)
            {
                if (jobType_array[i].contains(bindData.get(position).getDiscountType())) {
                    discountSpin.setSelection(i);
                }
            }

            if (bindData.get(position).getNormaltrack())
                normalCheck.setChecked(true);
            if (bindData.get(position).getFastTrack())
                fastCheck.setChecked(true);
            if (bindData.get(position).getProjectWork())
                projectCheck.setChecked(true);
            if (bindData.get(position).getEveryDay())
                everyDayCheck.setChecked(true);
            if (bindData.get(position).getWeekend())
                weekendCheck.setChecked(true);
            if (bindData.get(position).getOnline())
                onlineCheck.setChecked(true);

        }

        public void getBrochure(String id){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getResultsValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null && result.toString().length() > 0) {
                        brochureFromServer = (String) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_BROCHURE;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry No Brochure Found Add new brochure", Toast.LENGTH_SHORT).show();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getBrochure, id));
        }

        public void getSaveBrochure(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Saving...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            CommonUtils.saveBrochureToSdcard(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        progressDialog.dismiss();
                        BrochureName = (String) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_BROCHURE_SAVED;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);

                    }
                }
            }, brochureFromServer, bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id(), "", getActivity());
        }

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        private void updateLabel() {

            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            coursestartsEdt.setText(sdf.format(myCalendar.getTime()));
        }

        public void getLocations(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error occured while getting Locations from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        progressDialog.dismiss();
                        locationsMap = (LinkedHashMap<String, String>) result;
                        if (null != locationsMap && null != locationSpin) {
                            locationSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(locationsMap, "Location")));
                            areaEdt.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(locationsMap, "Location")));
                            if (!bundlePosition.getString("POSITION").equals("")) {
                                String[] jobType_array = CommonUtils.fromMap(locationsMap, "Location");
                                for (int i = 0; i < jobType_array.length; i++) {
                                    if (jobType_array[i].contains(bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).getLocation())) {
                                        locationSpin.setSelection(i);
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Not able to get locations", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }

        public void getKeyWords(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }

                    if (result != null) {
                        if (null != keySkillsList && keySkillsList.size() > 0 && isVisible()) {
                            courseNameEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                        }
                        progressDialog.dismiss();
                    }
                }
            });
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {

                            }
                        }
                    });
                }
            });

        }

        public boolean validateTrainerRegister(){
            Form mForm = new Form(getActivity());
            mForm.addField(Field.using(Institute_NameEdt).validate(NotEmpty.build(getActivity())));
            if (CommonUtils.spinnerSelect("Location", locationSpin.getSelectedItemPosition(), getActivity())){

                mForm.addField(Field.using(areaEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
                mForm.addField(Field.using(contactnameEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(courseNameEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(instructorNameEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(corsesfeeEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(coursestartsEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(durationEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(descriptionEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(addressEdt).validate(NotEmpty.build(getActivity())));
                return (mForm.isValid()) ? true : false;
            }
            return (mForm.isValid()) ? false : false;
        }

        public boolean validateMobileNo(){

            if (CommonUtils.isValidMobile(contactnumberEdt.getText().toString(), contactnumberEdt))
                return true;
            else
                return  false;

        }

        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")){
                        //selectImage();
                        // Calls an Intent that opens camera to take a picture
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, FROMCAM);
                    }
                    else if (options[item].equals("Choose from Gallery")){
                        //Call an intent that opens gallery to select photo
                        Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            take_photo = true;
            if (requestCode == FILE_CHOOSER) {
                if (null != data) {
                    fileSelected = data.getStringExtra("fileSelected");
                    resumeNameTxt.setText(fileSelected);
                    resumeNameTxt.setVisibility(View.VISIBLE);
//                    mSetupData.setResumePath(fileSelected);
                    File dir = Environment.getExternalStorageDirectory();
                    resume = new File(fileSelected);
                    try{
                        resumeFile = CommonUtils.encodeFileToBase64Binary(resume);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }

        Bitmap ShrinkBitmap(String file, int width, int height){

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

            int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
            int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

            if (heightRatio > 1 || widthRatio > 1)
            {
                if (heightRatio > widthRatio)
                {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
            return bitmap;
        }

        public void setTrainingData() {
            int selectedId = radiostatus.getCheckedRadioButtonId();
            RadioButton statusbutton = (RadioButton)flipper.findViewById(selectedId);

            mDataModel.setInstituteName(Institute_NameEdt.getText().toString());
            mDataModel.setLocation(locationSpin.getSelectedItem().toString());
            mDataModel.setCity(areaEdt.getText().toString());
            mDataModel.setCourseName(courseNameEdt.getText().toString());
            mDataModel.setEligibility(eligibilityEdt.getText().toString());
            mDataModel.setInstructorName(instructorNameEdt.getText().toString());
            mDataModel.setEmail(emailEdt.getText().toString());
            mDataModel.setContactNumber(contactnumberEdt.getText().toString());
            mDataModel.setContactNoLandline(landlineEdt.getText().toString());
            mDataModel.setStartDate(coursestartsEdt.getText().toString());
            mDataModel.setDuration(durationEdt.getText().toString() + " days");
            mDataModel.setStartTime(classStartTimeEdt.getText().toString());
            mDataModel.setStartTime(classStartTimeEdt.getText().toString());
            mDataModel.setEndTime(classendTimeEdt.getText().toString());
            mDataModel.setCourseFee(corsesfeeEdt.getText().toString());
            mDataModel.setBatchSize(Integer.parseInt(batchSizeEdt.getText().toString()));
            mDataModel.setDiscountType(discountSpin.getSelectedItem().toString());
            mDataModel.setDiscountValue(discountvalueEdt.getText().toString());
            mDataModel.setGroupCount(Integer.parseInt(groupDiscountEdt.getText().toString()));
            mDataModel.setGroupValue(groupDiscountValueEdt.getText().toString());
            mDataModel.setContactPerson(contactnameEdt.getText().toString());
            mDataModel.setCompanyURL(websiteEdt.getText().toString());
            mDataModel.setCourseDescription(descriptionEdt.getText().toString());
            mDataModel.setAddress(addressEdt.getText().toString());
            mDataModel.setVenue(venueEdt.getText().toString());
            mDataModel.setStatus(statusbutton.getText().toString());
            mDataModel.setPostDate(CommonUtils.getDateTime());

            if (userType == 3)
            mDataModel.setPostedBy("Institute");
            else if (userType == 5)
                mDataModel.setPostedBy("Trainer");

            mDataModel.setIsOnline(true);
            //            if (take_photo)
//                mDataModel.setPhotoString(Base64.encodeToString(seekerImageData, Base64.DEFAULT));
//            else
//                mDataModel.setPhotoString("");

            if (normalCheck.isChecked())
                mDataModel.setNormaltrack(true);
            else
                mDataModel.setNormaltrack(false);

            if (fastCheck.isChecked())
                mDataModel.setFastTrack(true);
            else
                mDataModel.setFastTrack(false);

            if (projectCheck.isChecked())
                mDataModel.setProjectWork(true);
            else
                mDataModel.setProjectWork(false);

            if (everyDayCheck.isChecked())
                mDataModel.setEveryDay(true);
            else
                mDataModel.setEveryDay(false);

            if (weekendCheck.isChecked())
                mDataModel.setWeekend(true);
            else
                mDataModel.setWeekend(false);

            if (onlineCheck.isChecked())
                mDataModel.setOnline(true);
            else
                mDataModel.setOnline(false);


        }

        public HashMap toUpload(){
            HashMap recruiterRegDataMap = new HashMap();
            recruiterRegDataMap.put("InstituteName",mDataModel.getInstituteName());
            recruiterRegDataMap.put("Location",mDataModel.getLocation());
            recruiterRegDataMap.put("City",mDataModel.getCity());
            recruiterRegDataMap.put("CourseName",mDataModel.getCourseName());
            recruiterRegDataMap.put("Eligibility",mDataModel.getEligibility());
            recruiterRegDataMap.put("InstructorName",mDataModel.getInstructorName());
            recruiterRegDataMap.put("Email",mDataModel.getEmail());
            recruiterRegDataMap.put("ContactNumber",mDataModel.getContactNumber());
            recruiterRegDataMap.put("ContactNo_Landline",mDataModel.getContactNoLandline());
            recruiterRegDataMap.put("StartDate",mDataModel.getStartDate());
            recruiterRegDataMap.put("Duration",mDataModel.getDuration());
            recruiterRegDataMap.put("StartTime",mDataModel.getStartTime());
            recruiterRegDataMap.put("EndTime",mDataModel.getEndTime());
            recruiterRegDataMap.put("CourseFee",mDataModel.getCourseFee());
            recruiterRegDataMap.put("BatchSize",mDataModel.getBatchSize());
            recruiterRegDataMap.put("Normaltrack",mDataModel.getNormaltrack());
            recruiterRegDataMap.put("FastTrack",mDataModel.getFastTrack());
            recruiterRegDataMap.put("ProjectWork",mDataModel.getProjectWork());
            recruiterRegDataMap.put("DiscountType",mDataModel.getDiscountType());
            recruiterRegDataMap.put("DiscountValue",mDataModel.getDiscountValue()+"%");
            recruiterRegDataMap.put("GroupCount",mDataModel.getGroupCount());
            recruiterRegDataMap.put("GroupValue",mDataModel.getGroupValue());
            recruiterRegDataMap.put("ContactPerson",mDataModel.getContactPerson());
            recruiterRegDataMap.put("CompanyURL",mDataModel.getCompanyURL());
            recruiterRegDataMap.put("EveryDay",mDataModel.getEveryDay());
            recruiterRegDataMap.put("Weekend",mDataModel.getWeekend());
            recruiterRegDataMap.put("Online",mDataModel.getOnline());
            recruiterRegDataMap.put("CourseDescription",mDataModel.getCourseDescription());
            recruiterRegDataMap.put("Address",mDataModel.getAddress());
            recruiterRegDataMap.put("Venue",mDataModel.getVenue());
            recruiterRegDataMap.put("Status",mDataModel.getStatus());
            recruiterRegDataMap.put("PostDate",mDataModel.getPostDate());
            recruiterRegDataMap.put("PostedBy",mDataModel.getPostedBy());

            return recruiterRegDataMap;
        }

        public void uploadPostcourseData(HashMap<String,String> input){
            if (progressDialog == null) {
                progressDialog.setMessage("Post Course data..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            JSONObject json = new JSONObject(input);
            String requestString = json.toString();
            requestString = CommonUtils.encodeURL(requestString);
            if (!bundlePosition.getString("POSITION").equals(""))
                configUrl = String.format(Config.LJS_BASE_URL + Config.modifyCourse, "" + requestString,bindData.get(Integer.parseInt(bundlePosition.getString("POSITION"))).get_id());
            else
            configUrl = String.format(Config.LJS_BASE_URL + Config.postCoursesTrainer, "" + requestString);

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Post course Failed",Toast.LENGTH_LONG).show();
                        if (result != null) {
                        }
                        return;
                    }
                    if (result != null) {
                        if (result.toString().equalsIgnoreCase("true")) {
          //                  uploadUserImage();
                            designSuccessDialog();
                        }
//                    progressDialog.dismiss();
                    }
                }
            }, configUrl);
        }



        public void  uploadUserImage(){

//        if (progressDialog == null) {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Uploading user data..");
//            progressDialog.setCancelable(false);
//        }
//        progressDialog.show();
            compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    Log.i("", "Result is : " + result.toString());
                    if (result != null && result.toString().contains("true")) {
                        progressDialog.dismiss();

                        if (preferences.getString(CommonUtils.getUserEmail(getActivity()), null) == null) {
                            designSuccessDialog();
                        }

                    }
                }
            }, "" + emailEdt.getText().toString() + ".png", emailEdt.getText().toString(), compressedStr, getActivity(), 0);

        }

        Dialog dialog;
        public void designSuccessDialog(){
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.common_dialog);
            dialog.setCanceledOnTouchOutside(true);
            TextView msgView = (TextView) dialog.findViewById(R.id.msgTxt);
            TextView titleHeader = (TextView) dialog.findViewById(R.id.titleHeader);
            titleHeader.setText("Confirmation");
            if (!bundlePosition.getString("POSITION").equals(""))
            msgView.setText("Updated Course Successfully");
            else
                msgView.setText("Thank you for Posting Course to Local Job Server ");
            dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    startActivity(new Intent(getActivity(), LoginScreen.class));
                    getActivity().finish();
                }
            });

            dialog.show();
        }

    class StatusHandler extends Handler {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_BROCHURE:
                    getSaveBrochure();
                    break;
                case RESULT_BROCHURE_SAVED:
                    resumeNameTxt.setVisibility(View.VISIBLE);
                    resumeNameTxt.setText("" + BrochureName);
                    Toast.makeText(getActivity(), "Now open the brochure", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }


    }
}
