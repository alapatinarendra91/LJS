package com.localjobserver.menu;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import co.talentzing.R;

public class ShareResumeActivity extends ActionBarActivity {
    private ActionBar actionBar = null;
    public static boolean back_var = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Share Details");

        Bundle bundle = new Bundle();
        bundle.putString("_id",getIntent().getStringExtra("_id"));
        bundle.putString("email_resume",getIntent().getStringExtra("email_resume"));
        ShareResumeFragment fragInfo = new ShareResumeFragment();
        fragInfo.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, fragInfo)
                .commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        back_var = false;
        this.finish();


    }
}
