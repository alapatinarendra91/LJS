package com.localjobserver.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.JobsListToShareModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SendSMSFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendSMSFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1",ARG_PARAM_id = "id",ARG_PARAM_email = "email";

    // TODO: Rename and change types of parameters
    private String mParam1,mParam_id,mParamemail_resume,url;
    private View rootView;
    private EditText emailIdEdt,subjectEdt,subjectEdtSms,jobDescriptionEdt;
    private Spinner expSp,toSp,ctcSp,toctcSp,savedTemplateSpin;
    private CheckBox marketRateCheck;
    private AutoCompleteTextView locationEdt;
    private Button submitBtn,clearBtn;
    private TextView titleTxt;
    private LinearLayout smsLay,emailLay;
    private ProgressDialog progressDialog;
    public LinkedHashMap<String,String>  locationsMap = null;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_JobshareData = 2;
    private SharedPreferences appPrefs;
    List<JobsListToShareModel> Shared_JoblistaList;
    private LinkedHashMap<String, String> sharedjobmap;
    private  boolean isClassVisible = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment SendSMSFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendSMSFragment newInstance(String param1,String id,String email) {
        SendSMSFragment fragment = new SendSMSFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM_id, id);
        args.putString(ARG_PARAM_email, email);
        fragment.setArguments(args);
        return fragment;
    }

    public SendSMSFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam_id = getArguments().getString(ARG_PARAM_id);
            mParamemail_resume = getArguments().getString(ARG_PARAM_email);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView = inflater.inflate(R.layout.sms_layout, container, false);

         titleTxt = (TextView)rootView.findViewById(R.id.titleTxt);
        titleTxt.setText(""+mParam1);
        smsLay = (LinearLayout)rootView.findViewById(R.id.smsLay);
        emailLay = (LinearLayout)rootView.findViewById(R.id.emailLay);
        emailIdEdt = (EditText)rootView.findViewById(R.id.emailIdEdt);
        subjectEdt = (EditText)rootView.findViewById(R.id.subjectEdt);
        subjectEdtSms = (EditText)rootView.findViewById(R.id.subjectEdtSms);
        jobDescriptionEdt = (EditText)rootView.findViewById(R.id.jobDescriptionEdt);
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        toSp = (Spinner)rootView.findViewById(R.id.toSp);
        ctcSp = (Spinner)rootView.findViewById(R.id.ctcSp);
        toctcSp = (Spinner)rootView.findViewById(R.id.toctcSp);
        savedTemplateSpin = (Spinner)rootView.findViewById(R.id.savedTemplateSpin);
        marketRateCheck = (CheckBox)rootView.findViewById(R.id.marketRateCheck);
        locationEdt = (AutoCompleteTextView)rootView.findViewById(R.id.locationEdt);
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        clearBtn = (Button)rootView.findViewById(R.id.clearBtn);

        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        emailIdEdt.setText(""+appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""));

        submitBtn.setOnClickListener(this);
        clearBtn.setOnClickListener(this);
        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expSp.setSelection(1);
        expSp.setOnItemSelectedListener(spinListener);
        ctcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        ctcSp.setSelection(1);
        ctcSp.setOnItemSelectedListener(spinListener);
        savedTemplateSpin.setOnItemSelectedListener(spinListener);

        getLocations();
        getJobsListToShare();

        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String,String>) result;
                    locationEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.expSp:
                    if (expSp.getSelectedItemId() == 31){
                        toSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        toSp.setOnItemSelectedListener(spinListener);
                    }else if (expSp.getSelectedItemId() == 1){
                        toSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expSp.getSelectedItem().toString()), 31)));
                        toSp.setOnItemSelectedListener(spinListener);
                    }else if (expSp.getSelectedItemId() != 0){
                        toSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expSp.getSelectedItem().toString())+1, 31)));
                        toSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.ctcSp:
                    toctcSp:
                    if (ctcSp.getSelectedItemId() == 100){
                        toctcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        toctcSp.setOnItemSelectedListener(spinListener);
                    }else if (ctcSp.getSelectedItemId() == 1){
                        toctcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(ctcSp.getSelectedItem().toString()), 100)));
                        toctcSp.setOnItemSelectedListener(spinListener);
                    }else if (ctcSp.getSelectedItemId() != 0){
                        toctcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(ctcSp.getSelectedItem().toString())+1, 100)));
                        toctcSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.savedTemplateSpin:
                    if (savedTemplateSpin.getSelectedItemId() != 0){
                        toSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
                        toctcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));

//                        emailIdEdt.setText(""+Shared_JoblistaList.get(position - 1).getEmail());
                        subjectEdt.setText(Shared_JoblistaList.get(position - 1).getJobTitle());
                        expSp.setSelection(Integer.parseInt(Shared_JoblistaList.get(position - 1).getExpMin())+1);
                        toSp.setSelection(Integer.parseInt(Shared_JoblistaList.get(position - 1).getExpMax())+1);
                        ctcSp.setSelection(Integer.parseInt(Shared_JoblistaList.get(position - 1).getMinSal())+1);
                        toctcSp.setSelection(Integer.parseInt(Shared_JoblistaList.get(position - 1).getMaxSal())+1);
                        locationEdt.setText(Shared_JoblistaList.get(position - 1).getLocation());
                        jobDescriptionEdt.setText(Shared_JoblistaList.get(position - 1).getJobDescription());

                    }else {

                    }
                    break;

            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.submitBtn:

             /*   if (mParam1.equalsIgnoreCase("Send Email")){
                    if (validateMailFieldsDetails()  && spinnerValidation()){
                        CommonUtils.showToast("Wait for Service", getActivity());
                        uploadMail(toUpload());
                    }
                }else if (validateMailFieldsDetails()){
                    uploadMail(null);

                }*/
                if (validateMailFieldsDetails()  && spinnerValidation()){
                    uploadMail(toUpload());
                }

                break;

            case R.id.clearBtn :
                getActivity().finish();
                break;

            default :
        }
    }

    public boolean validateMailFieldsDetails(){
        Form mForm = new Form(getActivity());
//        if (mParam1.equalsIgnoreCase("Send Email")){
        mForm.addField(Field.using(emailIdEdt).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using(subjectEdt).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(locationEdt).validate(NotEmpty.build(getActivity())));
        mForm.addField(Field.using(jobDescriptionEdt).validate(NotEmpty.build(getActivity())));
//        }else
//            mForm.addField(Field.using(subjectEdtSms).validate(NotEmpty.build(getActivity())));

        return (mForm.isValid()) ? true : false;
    }
    public  boolean spinnerValidation(){
        if (!CommonUtils.spinnerSelect("Experience Minimum",expSp.getSelectedItemPosition(),getActivity()) ||
                !CommonUtils.spinnerSelect("CTC Minimum",ctcSp.getSelectedItemPosition(),getActivity())
                ){
            return false;
        }

        return true;
    }

    public HashMap toUpload(){
        HashMap RecuiterMailDataMap = new HashMap();
        RecuiterMailDataMap.put("JobTemplateNo","");
        RecuiterMailDataMap.put("JobTitle",subjectEdt.getText().toString());
        RecuiterMailDataMap.put("Email",emailIdEdt.getText().toString());
        RecuiterMailDataMap.put("ExpMin",expSp.getSelectedItem().toString());
        RecuiterMailDataMap.put("ExpMax",toSp.getSelectedItem().toString());
        RecuiterMailDataMap.put("MinSal", ctcSp.getSelectedItem().toString());
        RecuiterMailDataMap.put("MaxSal", toctcSp.getSelectedItem().toString());
        RecuiterMailDataMap.put("Location", locationEdt.getText().toString());
        RecuiterMailDataMap.put("JobDescription", jobDescriptionEdt.getText().toString());
        for (Map.Entry<String, String> entry : locationsMap.entrySet()) {
            if (entry.getValue().equals(locationEdt.getText().toString())) {
                RecuiterMailDataMap.put("LocId", entry.getKey());
            }
        }
        return RecuiterMailDataMap;
    }

    public void uploadMail(HashMap<String,String> input) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        requestString = CommonUtils.encodeURL(requestString);
        if (mParam1.equalsIgnoreCase("Send Email")) {
            url = String.format(Config.LJS_BASE_URL + Config.sendRecruiterBulkEmailOrSMS, requestString, emailIdEdt.getText().toString(), mParamemail_resume,"1");
        }else
            url = String.format(Config.LJS_BASE_URL + Config.sendRecruiterBulkEmailOrSMS, requestString, emailIdEdt.getText().toString(), mParamemail_resume,"2");

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Mail Send failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_USERDATA;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, url);

    }

    public void getJobsListToShare() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        ApplicationThread.bgndPost(getClass().getSimpleName(), "getTrainerDetailsHere...", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getJobsListToShare, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "")), false, new ApplicationThread.OnComplete() {
                    public void run() {
                        if (success == false || result == null || result.toString().contains("Invalid")) {
                            progressDialog.dismiss();
                            return;
                        }
                        JobsListToShareModel mRecData = null;
                        try {
                            if (result != null && ((String) result).toString().length() > 0) {
                                progressDialog.dismiss();
                                try {
                                    Type collectionType = new TypeToken<Collection<JobsListToShareModel>>() {
                                    }.getType();
                                    Gson googleJson = new Gson();
                                    Shared_JoblistaList = googleJson.fromJson((String) result, collectionType);
                                    Log.v(LiveData.class.getSimpleName(), "JobsListToShareModel Register size.." + Shared_JoblistaList.size());

                                    getgenericvalues();
                                } catch (Exception ex) {

                                    ex.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            Message messageToParent = new Message();
                            messageToParent.what = RESULT_ERROR;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }

                    }
                });
            }
        });
    }

    public void getgenericvalues(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    if (progressDialog.isShowing())
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    if (progressDialog.isShowing())
                    progressDialog.dismiss();
                    sharedjobmap = (LinkedHashMap<String, String>) result;
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_JobshareData;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getJobsListToShare, appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, "")), CommonKeys.arrSharedJobsdata);
    }

    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error occurred while updating the password.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:
                    if (mParam1.equalsIgnoreCase("Send Email"))
                    Toast.makeText(getActivity(), "Your email has been sent successfully.", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), "Your SMS has been sent successfully.", Toast.LENGTH_SHORT).show();
//                    getActivity().finish();
                    clearFields();
                    break;
                case RESULT_JobshareData:
                    if (isClassVisible == true)
                    savedTemplateSpin.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(sharedjobmap, "Saved Template")));
                    break;
            }
        }
    }

    private void clearFields(){
        savedTemplateSpin.setSelection(0);
        subjectEdt.setText("");
        locationEdt.setText("");
        jobDescriptionEdt.setText("");
        expSp.setSelection(1);
        ctcSp.setSelection(1);
        toSp.setSelection(1);
        toctcSp.setSelection(1);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
