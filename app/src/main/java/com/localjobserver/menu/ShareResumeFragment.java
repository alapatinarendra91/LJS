package com.localjobserver.menu;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

/**
 * Created by KTSL-NILESH on 23-02-2016.
 */
public class ShareResumeFragment extends Fragment {
    private View rootView;
    private EditText  emailEdit,descriptionEdit;
    private CheckBox TrackerCheck;
    private Button submitBtn,cancelBtn;
    private ProgressDialog progressDialog;
    private SharedPreferences appPrefs;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_share_resume, container, false);
        appPrefs = getActivity().getSharedPreferences("ljs_prefs", getActivity().MODE_PRIVATE);
        emailEdit = (EditText)rootView.findViewById(R.id.emailEdit);
        descriptionEdit = (EditText)rootView.findViewById(R.id.descriptionEdit);
        TrackerCheck = (CheckBox)rootView.findViewById(R.id.TrackerCheck);
        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateShareDetailsDetails()){

                    uploadShareDetails();
                }
            }
        });

        cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return rootView;
    }

    public boolean validateShareDetailsDetails(){
        Form mForm = new Form(getActivity());
        if (!emailEdit.getText().toString().contains(","))
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.emailEdit)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
        mForm.addField(Field.using((EditText) rootView.findViewById(R.id.descriptionEdit)).validate(NotEmpty.build(getActivity())));
        return (mForm.isValid()) ? true : false;
    }

    public void uploadShareDetails() {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();

        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Share Details Send failed", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Share Details Send Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.shareProfilesWithRecruiters, emailEdit.getText().toString(),getArguments().getString("email_resume"),appPrefs.getString(CommonKeys.LJS_PREF_EMAILID, ""), descriptionEdit.getText().toString(),TrackerCheck.isChecked()));

    }

}
