package com.localjobserver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.localjobserver.TrainingInstitute.InstitutionNavigationActivity;
import com.localjobserver.TrainingInstitute.TrainingInstituteRegistratiojn;
import com.localjobserver.chat.LJSChatActivity;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.databaseutils.DatabaseHelper;
import com.localjobserver.employee.EmployeeRegisterActivity;
import com.localjobserver.models.FresherRegistrationModel;
import com.localjobserver.models.JobsData;
import com.localjobserver.models.RecuiterObject;
import com.localjobserver.models.SetupData;
import com.localjobserver.models.SubUserModel;
import com.localjobserver.models.TrainerRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.onlineTrainers.RegisterOnlineTrainer;
import com.localjobserver.setup.FreshersRegistration;
import com.localjobserver.setup.RegisterFragmentActivity;
import com.localjobserver.trainingsNfreshers.FnTNavigationActivity;
import com.localjobserver.ui.JobDescriptionActivity;
import com.localjobserver.ui.MainInfoActivity;
import com.localjobserver.ui.RecruiterRegistrationActivity;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import co.talentzing.R;

import io.fabric.sdk.android.Fabric;
import me.leolin.shortcutbadger.ShortcutBadger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;


public class LoginScreen extends FragmentActivity {

    private Button loginBtn, registerBtn, searchBtn,resumesBtn;
    private ImageView logoImg;
    private LinearLayout loginLL,findLay;
    private Dialog dialog;

    private GoogleCloudMessaging gcm;
    public static String regid = "";
    private String PROJECT_NUMBER = "398933963650";

    private SharedPreferences gcmPrefs = null;
    public String gcmId = "";
    private String msg = "";
    private ProgressDialog progressDialog;
    public ArrayList<JobsData> jobList;
    private Bundle bundle1;
    private int badgeCount_Chat;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

         gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
         editor = gcmPrefs.edit();


//        Intent intent = new Intent();
//        String packageName = getApplicationContext().getPackageName();
//        ComponentName componentName = new ComponentName(packageName,
//                packageName + ".ActivityListItem");
//        intent.setComponent(componentName);

        try {
            DatabaseHelper initialDB = new DatabaseHelper(LoginScreen.this);
            initialDB.createDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        gcmId = gcmPrefs.getString("gcmRegId", "");
        int BCount = gcmPrefs.getInt("badgeCount", 0);

        if (null != getIntent() && (null != getIntent().getAction() && getIntent().getAction().equalsIgnoreCase("fromNotification")) || BCount > 0) {
            CommonUtils.badgeCount = 0;
            SharedPreferences.Editor editor = gcmPrefs.edit();
            editor.putInt("badgeCount", 0);
            editor.commit();
            ShortcutBadger.applyCount(LoginScreen.this, 0);
            getJobsData();
        } else if (null != getIntent() && (null != getIntent().getAction() && getIntent().getAction().equalsIgnoreCase("fromChat"))) {

            badgeCount_Chat = gcmPrefs.getInt("badgeCount_Chat", 0);
            int remainingBadges =  gcmPrefs.getInt("badgeCount_Total", 0) - badgeCount_Chat;
            ShortcutBadger.applyCount(LoginScreen.this, remainingBadges);
            editor.putInt("badgeCount_Chat", 0);
            editor.putInt("badgeCount_Total", remainingBadges);
            editor.commit();

            Intent recevierIntent = getIntent();
            Intent i = new Intent(LoginScreen.this, LJSChatActivity.class);
            Bundle msgBundle = new Bundle();
            msgBundle.putString("fromEmailId", "" + CommonUtils.getUserEmail(this));
            msgBundle.putString("toEmailId", recevierIntent.getExtras().getString("fromuserid"));
            msgBundle.putString("message", recevierIntent.getExtras().getString("message"));
            i.putExtras(msgBundle);
            i.setAction("fromChatNoti");
            startActivity(i);
            finish();


        } else {
            setContentView(R.layout.activity_login_screen);

            loginBtn = (Button) findViewById(R.id.loginBtn);
            registerBtn = (Button) findViewById(R.id.registerBtn);
            logoImg = (ImageView) findViewById(R.id.logo);
            loginLL = (LinearLayout) findViewById(R.id.loginBtnLL);
            findLay = (LinearLayout) findViewById(R.id.findLay);
            searchBtn = (Button) findViewById(R.id.searchBtn);
            resumesBtn = (Button) findViewById(R.id.resumesBtn);

            CommonUtils.isUserExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("JobSeekers"), LoginScreen.this);

            if (!CommonUtils.isUserExisted) {
//                CommonUtils.isUserExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("RecuiterRegistration"), LoginScreen.this);
                CommonUtils.isUserExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("SubUserRegistration"), LoginScreen.this);
            }

            if (!CommonUtils.isUserExisted && !CommonUtils.isTrainerExisted) {
                CommonUtils.isFresherExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("FreshersRegistration"), LoginScreen.this);
            }

            if (!CommonUtils.isUserExisted && !CommonUtils.isFresherExisted) {
                CommonUtils.isTrainerExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("TrainersRegistration"), LoginScreen.this);
            }

            if (CommonUtils.isUserExisted) {
//                SharedPreferences gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
//                SharedPreferences.Editor editor = gcmPrefs.edit();
                badgeCount_Chat = gcmPrefs.getInt("badgeCount_Chat", 0);
                if (null != getIntent() && (null != getIntent().getAction() && getIntent().getAction().equalsIgnoreCase("fromChat")) || badgeCount_Chat > 0) {
                    Intent recevierIntent = getIntent();
                    Intent i = new Intent(LoginScreen.this, LJSChatActivity.class);
                    Bundle msgBundle = new Bundle();
                    msgBundle.putString("fromEmailId", "" + CommonUtils.getUserEmail(this));
                    msgBundle.putString("toEmailId", gcmPrefs.getString("fromuserid", null));
                    if (getIntent().getExtras()  != null)
                    msgBundle.putString("message", recevierIntent.getExtras().getString("message"));
                    else
                        msgBundle.putString("message", "Eampty");
                    i.putExtras(msgBundle);
                    i.setAction("fromChatNoti");
                    int remainingBadges =  gcmPrefs.getInt("badgeCount_Total", 0) - badgeCount_Chat;
                    ShortcutBadger.applyCount(LoginScreen.this, remainingBadges);

                    editor.putInt("badgeCount_Chat", 0);
                    editor.putInt("badgeCount_Total", remainingBadges);
                    editor.commit();

                    startActivity(i);
                    finish();

                }else {
                    startActivity(new Intent(this, MainInfoActivity.class));
                    finish();
                }

            } else if (CommonUtils.isFresherExisted) {
                startActivity(new Intent(this, FnTNavigationActivity.class));
                finish();
            }else if (CommonUtils.isTrainerExisted) {
                startActivity(new Intent(this, InstitutionNavigationActivity.class));
                finish();
            }else{
//                findLay.setVisibility(View.GONE);
//                loginLL.setVisibility(View.GONE);
//                Thread timerThread = new Thread(){
//                    public void run(){
//                        try{
//                            sleep(1000);
////                    sleep(600);
//                        }catch(InterruptedException e){
//                            e.printStackTrace();
//                        }finally{
//                            showService();
////
//
//                        }
//                    }
//                };
//                timerThread.start();
            }


            final Button popUpMenuBtn = (Button) findViewById(R.id.overflowMenuBtn);
            popUpMenuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(LoginScreen.this, popUpMenuBtn);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater().inflate(R.menu.menu_first_navigation, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_freshers:
                                    startActivity(new Intent(LoginScreen.this, FnTNavigationActivity.class));
                                    break;
                                case R.id.action_trainings:
                                    startActivity(new Intent(LoginScreen.this, InstitutionNavigationActivity.class));
                                    break;

                            }
                            return true;
                        }
                    });

                    popup.show();
                }
            });

            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(LoginScreen.this, MainInfoActivity.class));
                    finish();
                }
            });
            resumesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logoImg.setVisibility(View.GONE);
                    loginLL.setVisibility(View.GONE);
                    findLay.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.newInstance(1, "jr")).addToBackStack(null).commit();

                }
            });
            loginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    logoImg.setVisibility(View.GONE);
                    loginLL.setVisibility(View.GONE);
                    findLay.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LoginFragment.newInstance(1, "jr")).addToBackStack(null).commit();
                }
            });
            registerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   designTypeDialog();
                }
            });
        }


        if (gcmId.length() == 0) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(LoginScreen.this);
                progressDialog.setMessage("Registering device...");
                progressDialog.setCancelable(false);
            }

            progressDialog.show();

            getGCMRegId(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (success) {
                        SharedPreferences.Editor editor = gcmPrefs.edit();
                        editor.putString("gcmRegId", regid);
                        editor.commit();
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }
            });
        } else {
            regid = gcmId;
        }

    }

    private void showService(){
        runOnUiThread(new Runnable() {
            public void run() {
                            findLay.setVisibility(View.VISIBLE);
                            loginLL.setVisibility(View.VISIBLE);
                startActivity(new Intent(LoginScreen.this, MainInfoActivity.class));
            }
        });

    }
    public void getJobsData() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LoginScreen.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(LoginScreen.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    jobList = (ArrayList) result;
                    CommonUtils.jobList = (ArrayList) result;
                    progressDialog.dismiss();
                    startActivity(new Intent(LoginScreen.this, JobDescriptionActivity.class).putExtra("selectedPos", 2).putExtra("_id", ((JobsData) jobList.get(2)).get_id()).setAction("fromSearch"));
                    finish();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getJobs, "java", 1));
    }


    public void getGCMRegId(final ApplicationThread.OnComplete oncomplete) {
        ApplicationThread.bgndPost(LoginScreen.this.getClass().getSimpleName(), "Registering Device...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(PROJECT_NUMBER);
                    Log.i("GCM", msg);
                } catch (IOException ex) {
                    oncomplete.execute(false, null, null);
                    return;
                }
                if (regid.length() > 0) {
                    oncomplete.execute(true, regid, null);
                }
            }
        });
    }

    public void designTypeDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.registrationtype);
        dialog.setCanceledOnTouchOutside(true);

        Button seekerBtn = (Button) dialog.findViewById(R.id.radioSeeker);
        Button recruiterBtn = (Button) dialog.findViewById(R.id.radioRecruiter);
        Button fresherBtn = (Button) dialog.findViewById(R.id.fresher);
        Button trainerBtn = (Button) dialog.findViewById(R.id.trainer);
        Button employeeBtn = (Button) dialog.findViewById(R.id.employee);
        Button onlineTrainer = (Button) dialog.findViewById(R.id.onlineTrainer);
        seekerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (CommonUtils.isNetworkAvailable(LoginScreen.this)) {
                    startActivity(new Intent(LoginScreen.this, RegisterFragmentActivity.class));
                } else {
                    Toast.makeText(LoginScreen.this, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        recruiterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (CommonUtils.isNetworkAvailable(LoginScreen.this)) {
                    Intent i = new Intent(getApplicationContext(), RecruiterRegistrationActivity.class);
                    i.putExtra("flag", 0);
                    startActivity(i);
                } else {
                    Toast.makeText(LoginScreen.this, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fresherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginScreen.this, FreshersRegistration.class));
                dialog.dismiss();
            }
        });

        trainerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1 = new Bundle();
                bundle1.putString("flag", "0");
                startActivity(new Intent(LoginScreen.this, TrainingInstituteRegistratiojn.class).putExtra("flag", bundle1).putExtra("POST","POST"));
                dialog.dismiss();
            }
        });
        employeeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (CommonUtils.isNetworkAvailable(LoginScreen.this)) {
                    Intent i = new Intent(LoginScreen.this, EmployeeRegisterActivity.class);
                    i.putExtra("flag", 0);
                    startActivity(i);
                } else {
                    Toast.makeText(LoginScreen.this, "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        onlineTrainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle1 = new Bundle();
                bundle1.putString("flag", "0");
                startActivity(new Intent(LoginScreen.this, RegisterOnlineTrainer.class).putExtra("flag", bundle1).putExtra("POST", "Register"));
                dialog.dismiss();
            }
        });

                dialog.show();
            }

            /**
             * A placeholder fragment containing a simple view.
             */
            public static class LoginFragment extends Fragment {
                private Spinner loginTypeSpin;
                private Button submitBtn;
                private Button popup_subsubmitBtn;
                private ImageView eye_image;
                private ImageView popup_subeye_image;
                private boolean eye_var = false;
                private boolean popup_subeye_var = false;
                private ProgressDialog progressDialog;
                private String selectedUser = "";
                private static final int RESULT_ERROR = 0;
                private static final int RESULT_USERDATA = 1;
                private EditText emailEdt, passwordEdt;
                private Spinner popup_subunameSpin;
                private EditText popup_subpasswordEdt;
                private TextView forgot_password, newUser;
                private SharedPreferences appPrefs;
                private static final String ARG_PARAM1 = "param1";
                private boolean isRecordExisted;
                private ArrayAdapter<String> spinnerArrayAdapter = null;
                private String typeStr = "", ConfigForgoturl;
                private Dialog dialog;
                private Dialog dialo_subUserg;
                private Bundle bundle1;
                private boolean subUser_var = false ;
                public ArrayList<SubUserModel> mSubUserModelListArray;
                private String CompanyName;

                public LoginFragment() {
                }

                public static LoginFragment newInstance(int param1, String type) {
                    LoginFragment fragment = new LoginFragment();
                    Bundle args = new Bundle();
                    args.putInt(ARG_PARAM1, param1);
                    args.putString("type", "" + type);
                    fragment.setArguments(args);
                    return fragment;
                }

                @Override
                public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                    final View rootView = (View) inflater.inflate(R.layout.fragment_login, container, false);

                    CommonUtils.setimageDestination("");

                    try {
                        PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                                "co.talentzing",
                                PackageManager.GET_SIGNATURES);
                        for (Signature signature : info.signatures) {
                            MessageDigest md = MessageDigest.getInstance("SHA");
                            md.update(signature.toByteArray());
                            Log.e("KeyHash:", "KeyHashIs : "+Base64.encodeToString(md.digest(), Base64.DEFAULT));
                        }
                    } catch (PackageManager.NameNotFoundException e) {

                    } catch (NoSuchAlgorithmException e) {

                    }

                    dialo_subUserg = new Dialog(getActivity());
                    dialo_subUserg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialo_subUserg.setContentView(R.layout.popup_subuser);
                    dialo_subUserg.setCanceledOnTouchOutside(false);

                    loginTypeSpin = (Spinner) rootView.findViewById(R.id.jobTypeSp);
                    appPrefs = getActivity().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
                    typeStr = getArguments().getString("type");
                    if (null != typeStr && typeStr.equalsIgnoreCase("jr")) {
                        spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.loginroles)); //selected item will look like a spinner set from XML
                    } else {
                        spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.freshers_loginroles)); //selected item will look like a spinner set from XML
                    }
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    loginTypeSpin.setAdapter(spinnerArrayAdapter);
                    submitBtn = (Button) rootView.findViewById(R.id.submitBtn);
                    popup_subsubmitBtn = (Button) dialo_subUserg.findViewById(R.id.popup_subsubmitBtn);

                    emailEdt = (EditText) rootView.findViewById(R.id.unameEdt);
                    popup_subunameSpin = (Spinner) dialo_subUserg.findViewById(R.id.popup_subunameSpin);
                    passwordEdt = (EditText) rootView.findViewById(R.id.passwordEdt);
                    popup_subpasswordEdt = (EditText) dialo_subUserg.findViewById(R.id.popup_subpasswordEdt);
                    forgot_password = (TextView) rootView.findViewById(R.id.forgot_password);
                    newUser = (TextView) rootView.findViewById(R.id.newUser);
                    String styledText = "New User?  <font color='#008ACE'>Register Now</a></font>";
                    newUser.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
                    newUser.setMovementMethod(LinkMovementMethod.getInstance());
                    eye_image = (ImageView) rootView.findViewById(R.id.eye_image);
                    popup_subeye_image = (ImageView) dialo_subUserg.findViewById(R.id.popup_subeye_image);

                    popup_subpasswordEdt.setText("vishist123");

                    eye_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (eye_var == false) {
                                eye_var = true;
                                eye_image.setImageResource(R.drawable.eye_hidden);
                                passwordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                                passwordEdt.setSelection(passwordEdt.getText().length());
                            } else {
                                eye_var = false;
                                eye_image.setImageResource(R.drawable.eye_show);
                                passwordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                passwordEdt.setSelection(passwordEdt.getText().length());
                            }
                        }
                    });

                    popup_subeye_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (popup_subeye_var == false) {
                                popup_subeye_var = true;
                                popup_subeye_image.setImageResource(R.drawable.eye_hidden);
                                popup_subpasswordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                                popup_subpasswordEdt.setSelection(popup_subpasswordEdt.getText().length());
                            } else {
                                popup_subeye_var = false;
                                popup_subeye_image.setImageResource(R.drawable.eye_show);
                                popup_subpasswordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                popup_subpasswordEdt.setSelection(popup_subpasswordEdt.getText().length());
                            }
                        }
                    });

                    forgot_password.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (CommonUtils.spinnerSelect("Login Type", loginTypeSpin.getSelectedItemPosition(), getActivity())) {
                                if (!emailEdt.getText().toString().equals("")) {
                                    designForgotPassworDialog();
                                } else
                                    Toast.makeText(getActivity(), "Enter Email Address", Toast.LENGTH_SHORT).show();

                            }


                        }
                    });

                    newUser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            designTypeDialog_newUser();
                        }
                    });

                    popup_subsubmitBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            subUser_var = true;
                            getSubuserData();
                        }
                    });

                    passwordEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_GO) {
                                sendLoginData();
                            }
                            return false;
                        }
                    });


                    submitBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            sendLoginData();

//                            Uri mUri = Uri.parse("smsto:9490504952");
//                            Intent mIntent = new Intent(Intent.ACTION_SENDTO, mUri);
//                            mIntent.setPackage("com.whatsapp");
//                            mIntent.putExtra("sms_body", "The text goes here");
//                            mIntent.putExtra(Intent.EXTRA_TEXT, "Hai Good Morning");
//                            mIntent.putExtra("chat",false);
//                            startActivity(mIntent);


//                            Uri uri = Uri.parse("smsto: 9441633918");
//                            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
//                            i.putExtra("sms_body", "Cool!!");
//                            i.setPackage("com.whatsapp");
//                            startActivity(i);



//                            if (!CommonUtils.isNetworkAvailable(getActivity())) {
//                                Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//                            if (!CommonUtils.spinnerSelect("Login Type", loginTypeSpin.getSelectedItemPosition(), getActivity()))
//                                return;
//
//                            Form mForm = new Form(getActivity());
//                            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.unameEdt)).validate(NotEmpty.build(getActivity())));
//                            mForm.addField(Field.using((EditText) rootView.findViewById(R.id.passwordEdt)).validate(NotEmpty.build(getActivity())));
//                            if (mForm.isValid()) {
//                                if (selectedUser.equalsIgnoreCase(getActivity().getResources().getString(R.string.jseeker))) {
//                                    CommonUtils.userType = "1";
//                                    ApplicationThread.dbPost("Get user Record..", "get record", new Runnable() {
//
//                                        @Override
//                                        public void run() {
//                                            // TODO Auto-generated method stub
//                                            isRecordExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkUserExistance(emailEdt.getText().toString(), "JobSeekers"), getActivity());
//                                        }
//                                    });
//                                    if (isRecordExisted) {
//                                        Message messageToParent = new Message();
//                                        messageToParent.what = RESULT_USERDATA;
//                                        Bundle bundleData = new Bundle();
//                                        messageToParent.setData(bundleData);
//                                        saveUserInfo(0, "JobSeekers");
//                                        new StatusHandler().sendMessage(messageToParent);
//                                        Log.i("", "Is record existed");
//                                    } else {
//                                        Log.i("", "Is record not existed");
//                                        getUsersData();
//                                    }
//                                } else if (selectedUser.equalsIgnoreCase(getActivity().getResources().getString(R.string.recruiter))) {
//                                    CommonUtils.userType = "2";
//                                        Log.i("", "Is record not existed");
//                                        getRecritersData();
////                                    }
//                                } else if (selectedUser.equalsIgnoreCase("Freshers Corner")) {
//                                    geFresherssData();
//                                } else if (selectedUser.equalsIgnoreCase("Training Institutes") || selectedUser.equalsIgnoreCase(getResources().getString(R.string.onlineTrainer))) {
//                                    geTrainerData();
//                                } else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.employee))) {
//                                    CommonUtils.userType = "2";
//                                    ApplicationThread.dbPost("Get user Record..", "get record", new Runnable() {
//
//                                        @Override
//                                        public void run() {
//                                            // TODO Auto-generated method stub
//                                            isRecordExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkUserExistance(emailEdt.getText().toString(), "RecuiterRegistration"), getActivity());
//
//                                        }
//                                    });
//                                    if (isRecordExisted) {
//                                        Message messageToParent = new Message();
//                                        messageToParent.what = RESULT_USERDATA;
//                                        Bundle bundleData = new Bundle();
//                                        messageToParent.setData(bundleData);
//                                        saveUserInfo(4, "RecuiterRegistration");
//                                        new StatusHandler().sendMessage(messageToParent);
//                                        Log.i("", "Is record existed");
//                                    } else {
//                                        Log.i("", "Is record not existed");
//                                        getRecritersData();
////                                        geEmployeeReferralData();
//                                    }
//                                }
//
//                            }
                        }
                    });

                    loginTypeSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedUser = parent.getSelectedItem().toString();
                            if (selectedUser.equalsIgnoreCase("Job Seeker")) {


//                                Live
                             emailEdt.setText("surendraljs@gmail.com");
                             passwordEdt.setText("abc1234");

//                                emailEdt.setText("");
//                                passwordEdt.setText("");

                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordJobSeeker, "" + emailEdt.getText().toString().trim());

                            } else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.recruiter))) {
                                emailEdt.setText("development@vishist.com");
                                passwordEdt.setText("tz@123");

//                                emailEdt.setText("");
//                                passwordEdt.setText("");

                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordRecruiter, "" + emailEdt.getText().toString().trim());

                            } else if (selectedUser.equalsIgnoreCase("Freshers Corner")) {
//                                emailEdt.setText("csenaveen22@gmail.com");
//                                passwordEdt.setText("naveen246");
                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordFreshers, "" + emailEdt.getText().toString().trim());
                        emailEdt.setText("");
                        passwordEdt.setText("");
                            } else if (selectedUser.equalsIgnoreCase("Training Institutes")) {
//                                emailEdt.setText("suri143.babu1@gmail.com");
//                                passwordEdt.setText("abc123");
                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordTrainer, "" + emailEdt.getText().toString().trim());
                        emailEdt.setText("");
                        passwordEdt.setText("");

                            }else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.employee))) {
//                                emailEdt.setText("naveenrdy22@gmail.com");
//                                passwordEdt.setText("naveen143");
                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordRecruiter, "" + emailEdt.getText().toString().trim());
                        emailEdt.setText("");
                        passwordEdt.setText("");

                            }else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.onlineTrainer))) {
                                emailEdt.setText("kunchesuresh7@gmail.com");
//                                passwordEdt.setText("suresh6");
//                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordTrainer, "" + emailEdt.getText().toString().trim());
                        emailEdt.setText("");
                        passwordEdt.setText("");

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    return rootView;
                }

                private void sendLoginData(){
                    if (!CommonUtils.isNetworkAvailable(getActivity())) {
                        Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!CommonUtils.spinnerSelect("Login Type", loginTypeSpin.getSelectedItemPosition(), getActivity()))
                        return;

                    Form mForm = new Form(getActivity());
                    mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(getActivity())));
                    mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(getActivity())));
                    if (mForm.isValid()) {
                        if (selectedUser.equalsIgnoreCase(getActivity().getResources().getString(R.string.jseeker))) {
                            CommonUtils.userType = "1";
                            ApplicationThread.dbPost("Get user Record..", "get record", new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    isRecordExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkUserExistance(emailEdt.getText().toString(), "JobSeekers"), getActivity());
                                }
                            });
                            if (isRecordExisted) {
                                Message messageToParent = new Message();
                                messageToParent.what = RESULT_USERDATA;
                                Bundle bundleData = new Bundle();
                                messageToParent.setData(bundleData);
                                saveUserInfo(0, "JobSeekers");
                                new StatusHandler().sendMessage(messageToParent);
                                Log.i("", "Is record existed");
                            } else {
                                Log.i("", "Is record not existed");
                                getUsersData();
                            }
                        } else if (selectedUser.equalsIgnoreCase(getActivity().getResources().getString(R.string.recruiter))) {
                            CommonUtils.userType = "2";
                            Log.i("", "Is record not existed");
                            getRecritersData();
//                                    }
                        } else if (selectedUser.equalsIgnoreCase("Freshers Corner")) {
                            geFresherssData();
                        } else if (selectedUser.equalsIgnoreCase("Training Institutes") || selectedUser.equalsIgnoreCase(getResources().getString(R.string.onlineTrainer))) {
                            geTrainerData();
                        } else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.employee))) {
                            CommonUtils.userType = "2";
                            ApplicationThread.dbPost("Get user Record..", "get record", new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    isRecordExisted = DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkUserExistance(emailEdt.getText().toString(), "RecuiterRegistration"), getActivity());

                                }
                            });
                            if (isRecordExisted) {
                                Message messageToParent = new Message();
                                messageToParent.what = RESULT_USERDATA;
                                Bundle bundleData = new Bundle();
                                messageToParent.setData(bundleData);
                                saveUserInfo(4, "RecuiterRegistration");
                                new StatusHandler().sendMessage(messageToParent);
                                Log.i("", "Is record existed");
                            } else {
                                Log.i("", "Is record not existed");
                                getRecritersData();
//                                        geEmployeeReferralData();
                            }
                        }

                    }
                }

                public void designForgotPassworDialog(){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Are you sure ?");

                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                                    selectedUser = loginTypeSpin.getSelectedItem().toString();
                                    if (selectedUser.equalsIgnoreCase("Job Seeker")) {
                                        ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordJobSeeker, "" + emailEdt.getText().toString().trim());
                                    } else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.recruiter))) {
                                        ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordRecruiter, "" + emailEdt.getText().toString().trim());
                                    } else if (selectedUser.equalsIgnoreCase("Freshers Corner")) {
                                        ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordFreshers, "" + emailEdt.getText().toString().trim());
                                    } else if (selectedUser.equalsIgnoreCase("Training Institutes")) {
                                        ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordTrainer, "" + emailEdt.getText().toString().trim());
                                    } else if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.employee))) {
                                        ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.forgetPasswordRecruiter, "" + emailEdt.getText().toString().trim());

                                    }

                                    GetForgotPassword();
                        }
                    });

                    alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }

                public void designTypeDialog_newUser() {
                    dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.registrationtype);
                    dialog.setCanceledOnTouchOutside(true);

                    Button seekerBtn = (Button) dialog.findViewById(R.id.radioSeeker);
                    Button recruiterBtn = (Button) dialog.findViewById(R.id.radioRecruiter);
                    Button fresherBtn = (Button) dialog.findViewById(R.id.fresher);
                    Button trainerBtn = (Button) dialog.findViewById(R.id.trainer);
                    Button employeeBtn = (Button) dialog.findViewById(R.id.employee);
                    Button onlineTrainer = (Button) dialog.findViewById(R.id.onlineTrainer);
                    seekerBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (CommonUtils.isNetworkAvailable(getActivity())) {
                                startActivity(new Intent(getActivity(), RegisterFragmentActivity.class));
                            } else {
                                Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    recruiterBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (CommonUtils.isNetworkAvailable(getActivity())) {
                                Intent i = new Intent(getActivity(), RecruiterRegistrationActivity.class);
                                i.putExtra("flag", 0);
                                startActivity(i);
                            } else {
                                Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    fresherBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getActivity(), FreshersRegistration.class));
                            dialog.dismiss();
                        }
                    });

                    trainerBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bundle1 = new Bundle();
                            bundle1.putString("flag", "0");
                            startActivity(new Intent(getActivity(), TrainingInstituteRegistratiojn.class).putExtra("flag", bundle1).putExtra("POST", "POST"));
                            dialog.dismiss();
                        }
                    });

                    employeeBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (CommonUtils.isNetworkAvailable(getActivity())) {
                                Intent i = new Intent(getActivity(), EmployeeRegisterActivity.class);
                                i.putExtra("flag", 0);
                                startActivity(i);
                            } else {
                                Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    onlineTrainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bundle1 = new Bundle();
                            bundle1.putString("flag", "0");
                            startActivity(new Intent(getActivity(), RegisterOnlineTrainer.class).putExtra("flag", bundle1).putExtra("POST", "Register"));
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }

                public void geTrainerData() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getTrainerDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }

                            if (success == true && result != null) {
                                if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.onlineTrainer)))
                                    saveUserInfo(5, "TrainersRegistration");
                                else
                                    saveUserInfo(3, "TrainersRegistration");

                                CommonKeys.USER_LOGIN_TYPE = 2;
                                Message messageToParent = new Message();
                                messageToParent.what = RESULT_USERDATA;
                                Bundle bundleData = new Bundle();
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);
                                progressDialog.dismiss();

                            }
                        }
                    });
                }


                public void geFresherssData() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getFreshersDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false || result == null || result.toString().equals("")) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }

                            if (success == true && result != null) {
                                saveUserInfo(2, "FreshersRegistration");
                                CommonKeys.USER_LOGIN_TYPE = 2;
                                Message messageToParent = new Message();
                                messageToParent.what = RESULT_USERDATA;
                                Bundle bundleData = new Bundle();
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);
                                progressDialog.dismiss();

                            }
                        }
                    });
                }

                public void getRecritersData() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getRecruiterDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }

                            if (success == true && result != null) {
                                progressDialog.dismiss();

                                if (subUser_var == true) {

                                    RecuiterObject mRecDataObj = (RecuiterObject) result;
                                    DatabaseAccessObject.insertRecuiterRegData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                                        public void run() {
                                            if (success == false) {
                                                return;
                                            }
                                            if (result != null) {
                                                Message messageToParent = new Message();
                                                messageToParent.what = RESULT_USERDATA;
                                                if (selectedUser.equalsIgnoreCase(getResources().getString(R.string.employee)))
                                                    saveUserInfo(4, "RecuiterRegistration");
                                                else
                                                    saveUserInfo(1, "RecuiterRegistration");

                                                Bundle bundleData = new Bundle();
                                                messageToParent.setData(bundleData);
                                                new StatusHandler().sendMessage(messageToParent);

                                            }
                                        }
                                    }, mRecDataObj, getActivity());

                                } else {
//                                    dialo_subUserg.show();
                                    getSubusersList();

                                }
                            }
                        }
                    });
                }

                public void getSubusersList() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getSubuserListDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_LONG).show();
                                }
                                return;
                            }

                            if (success == true && result != null) {
                                progressDialog.dismiss();
                                mSubUserModelListArray = (ArrayList) result;
                                ArrayList<String> subUserEmailList = new ArrayList<String>();
                                if (mSubUserModelListArray.size() > 0 && mSubUserModelListArray != null) {
                                    for (int i = 0; i < mSubUserModelListArray.size(); i++)
                                        subUserEmailList.add(mSubUserModelListArray.get(i).getSubUserEmailId());

                                    spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, subUserEmailList);
                                    popup_subunameSpin.setAdapter(spinnerArrayAdapter);

                                    dialo_subUserg.show();
                                } else
                                    CommonUtils.showToast("Please create Subuser", getActivity());

                            }
                        }
                    });
                }

                public void getSubuserData() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getSubuserDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    subUser_var = false;
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }

                            if (success == true && result != null) {
                                progressDialog.dismiss();

                                if (subUser_var == true) {

                                    SubUserModel mRecDataObj = (SubUserModel) result;
                                    DatabaseAccessObject.insertSubuserData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                                        public void run() {
                                            if (success == false) {
                                                return;
                                            }
                                            if (result != null) {
                                                Message messageToParent = new Message();
                                                messageToParent.what = RESULT_USERDATA;
                                                    saveUserInfo(6, "SubUserRegistration");

                                                Bundle bundleData = new Bundle();
                                                messageToParent.setData(bundleData);
                                                new StatusHandler().sendMessage(messageToParent);

                                            }
                                        }
                                    }, mRecDataObj, getActivity());

                                } else {
                                    dialo_subUserg.show();

                                }
                            }
                        }
                    });
                }

                public void saveUserInfo(int type, String tableName) {
                    SharedPreferences.Editor editor = appPrefs.edit();
                    if (type == 0) {
                        Cursor dataCur = DatabaseAccessObject.getuserInfo(Queries.getInstance().checkExistance(tableName), getActivity());
                        if (dataCur != null && dataCur.moveToFirst()) {
                            editor.putString(CommonKeys.LJS_PREF_EMAILID, dataCur.getString(dataCur.getColumnIndex("Email")));
                            editor.putString(CommonKeys.LJS_PREF_PASSWORD, dataCur.getString(dataCur.getColumnIndex("Password")));
                            if (tableName.equalsIgnoreCase("JobSeekers")) {
                                CommonKeys.USER_LOGIN_TYPE = 0;
                                editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 0);
                            } else {
                                CommonKeys.USER_LOGIN_TYPE = 1;
                                editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 1);
                            }
                            editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
                            editor.commit();
                        }
                    } else if (type == 1) {
                        editor.putString(CommonKeys.LJS_PREF_EMAILID, emailEdt.getText().toString());
                        editor.putString(CommonKeys.LJS_PREF_PASSWORD, passwordEdt.getText().toString());
                        if (tableName.equalsIgnoreCase("JobSeekers")) {
                            CommonKeys.USER_LOGIN_TYPE = 0;
                            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 0);
                        } else {
                            CommonKeys.USER_LOGIN_TYPE = 1;
                            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 1);
                        }
                        editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
                        editor.commit();
                    } else if (type == 2) {
                        editor.putString(CommonKeys.LJS_PREF_EMAILID, emailEdt.getText().toString());
                        editor.putString(CommonKeys.LJS_PREF_PASSWORD, passwordEdt.getText().toString());
                        if (tableName.equalsIgnoreCase("FreshersRegistration")) {
                            CommonKeys.USER_LOGIN_TYPE = 2;
                            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 2);
                        } else {
                            CommonKeys.USER_LOGIN_TYPE = 2;
                            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 3);
                        }
                        editor.putBoolean(CommonKeys.LJS_PREF_FRESHER_ISLOGIN, true);
                        editor.commit();
                    }else if (type == 4) {
                        editor.putString(CommonKeys.LJS_PREF_EMAILID, emailEdt.getText().toString());
                        editor.putString(CommonKeys.LJS_PREF_PASSWORD, passwordEdt.getText().toString());

                            CommonKeys.USER_LOGIN_TYPE = 4;
                            editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 4);
                        editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
                        editor.commit();
                    } else if (type == 3){
                        editor.putString(CommonKeys.LJS_PREF_EMAILID, emailEdt.getText().toString());
                        editor.putString(CommonKeys.LJS_PREF_PASSWORD, passwordEdt.getText().toString());
                        editor.putBoolean(CommonKeys.LJS_PREF_TRAINER_ISLOGIN, true);
                        CommonKeys.USER_LOGIN_TYPE = 2;
                        editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 3);
                        editor.putBoolean(CommonKeys.LJS_PREF_TRAINER_ISLOGIN, true);
                        editor.commit();
                    }else if (type == 5){
                        editor.putString(CommonKeys.LJS_PREF_EMAILID, emailEdt.getText().toString());
                        editor.putString(CommonKeys.LJS_PREF_PASSWORD, passwordEdt.getText().toString());
                        editor.putBoolean(CommonKeys.LJS_PREF_TRAINER_ISLOGIN, true);
                        CommonKeys.USER_LOGIN_TYPE = 2;
                        editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 5);
                        editor.putBoolean(CommonKeys.LJS_PREF_TRAINER_ISLOGIN, true);
                        editor.commit();
                    }else if (type == 6){
                        //Sub user
                        Cursor dataCur = DatabaseAccessObject.getuserInfo(Queries.getInstance().checkExistance(tableName), getActivity());
                        if (dataCur != null && dataCur.moveToFirst()) {
                            editor.putString(CommonKeys.LJS_PREF_EMAILID, dataCur.getString(dataCur.getColumnIndex("SubUserEmailId")));
                        }
                        editor.putBoolean(CommonKeys.LJS_PREF_SUBUSER_ISLOGIN, true);
                        CommonKeys.USER_LOGIN_TYPE = 2;
                        editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 1);
                        editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
                        editor.commit();
                    }
                }

                public void getUsersData() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait..");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();
                    getUserDetailsHere(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success == false) {
                                progressDialog.dismiss();
                                if (result != null) {
                                    Toast.makeText(getActivity(), "" + result.toString(), Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }
                            if (result != null) {
                                SetupData mDataObj = (SetupData) result;
                                DatabaseAccessObject.insertJobSeekerData(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                                    public void run() {
                                        if (success == false) {
                                            return;
                                        }
                                        if (result != null) {
                                            saveUserInfo(1, "JobSeekers");
                                            Message messageToParent = new Message();
                                            messageToParent.what = RESULT_USERDATA;
                                            Bundle bundleData = new Bundle();
                                            messageToParent.setData(bundleData);
                                            new StatusHandler().sendMessage(messageToParent);
                                            progressDialog.dismiss();
                                        }
                                    }
                                }, mDataObj, getActivity());

                            }
                        }
                    });
                }

                public void getUserDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getUserDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            HttpClient.download(String.format(Config.LJS_BASE_URL + Config.userDetails, CommonUtils.userType, "" + emailEdt.getText().toString().trim(), "" + passwordEdt.getText().toString(), "" + regid), false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid") || result.toString().contains("Your email not yet verified")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    SetupData mSetupData = null;
                                    JSONArray jArray = null;
                                    try {
                                        jArray = new JSONArray(result.toString());
                                        if (jArray.length() == 0) {
                                            oncomplete.execute(false, getActivity().getResources().getString(R.string.not_registered), null);
                                            return;
                                        }
                                        for (int i = 0; i < jArray.length(); i++) {
                                            JSONObject resultObj = jArray.getJSONObject(i);
                                            mSetupData = new SetupData();
                                            mSetupData.setfName(resultObj.getString(CommonKeys.LJS_FirstName));
                                            mSetupData.setlName(resultObj.getString(CommonKeys.LJS_LastName));
                                            mSetupData.setEmail(resultObj.getString(CommonKeys.LJS_Email));
                                            mSetupData.setContactNum(resultObj.getString(CommonKeys.LJS_ContactNo));
                                            mSetupData.setLandNum(resultObj.getString(CommonKeys.LJS_ContactNo_Landline));
                                            mSetupData.setPassword(resultObj.getString(CommonKeys.LJS_Password));
                                            mSetupData.setConfirmPass(resultObj.getString(CommonKeys.LJS_ConfirmPassword));
                                            mSetupData.setDateOfBirth(resultObj.getString(CommonKeys.LJS_DateOfBirth));
                                            mSetupData.setlanguagesKnown(resultObj.getString(CommonKeys.LJS_languagesKnown));

                                            mSetupData.setCurrentDisgnation(resultObj.getString(CommonKeys.LJS_CurrentDesignation));
                                            mSetupData.setKeySkills(resultObj.getString(CommonKeys.LJS_KeySkills));
                                            mSetupData.setResumeHeadLine(resultObj.getString(CommonKeys.LJS_ResumeHeadLine));
                                            mSetupData.setCurrentCompany(resultObj.getString(CommonKeys.LJS_CurrentCompany));
                                            mSetupData.setPreferedLocation(resultObj.getString(CommonKeys.LJS_PreferredLocation));
                                            mSetupData.setCurrentCtc(resultObj.getString(CommonKeys.LJS_CurrentCTC));
                                            mSetupData.setExpectedCtc(resultObj.getString(CommonKeys.LJS_expectedCTC));
                                            mSetupData.setCurrentIndestry(resultObj.getString(CommonKeys.LJS_Industry));
//                                    mSetupData.setFunArea(resultObj.getString(CommonKeys.LJS_FunctionalArea));
                                            mSetupData.setRole(resultObj.getString(CommonKeys.LJS_Role));
                                            mSetupData.setJobType(resultObj.getString(CommonKeys.LJS_Jobtype));
                                            mSetupData.setCurrentLocation(resultObj.getString(CommonKeys.LJS_Location));
                                            mSetupData.setNoticePeriod(resultObj.getString(CommonKeys.LJS_NoticePeriod));
                                            mSetupData.setEduCourse(resultObj.getString(CommonKeys.LJS_Education));
                                            mSetupData.setInstitution(resultObj.getString(CommonKeys.LJS_Institute));
                                            mSetupData.setYearPassing(resultObj.getString(CommonKeys.LJS_YearOfPass));
                                            mSetupData.setSeekerImageData(null);
                                            mSetupData.setResumePath(resultObj.getString(CommonKeys.LJS_Resume));
                                            mSetupData.setExperience(resultObj.getString(CommonKeys.LJS_Experience));

                                            mSetupData.setStdKeySkills(resultObj.getString(CommonKeys.LJS_StdKeySkills));
                                            mSetupData.setUpdateResume(resultObj.getString(CommonKeys.LJS_UpdateResume));
                                            mSetupData.setGender(resultObj.getString(CommonKeys.LJS_Gender));
//                                    mSetupData.setProfileUpdate(resultObj.getString(CommonKeys.LJS_ProfileUpdate));
                                            mSetupData.setUpdateOn(resultObj.getString(CommonKeys.LJS_UpdateOn));
                                            mSetupData.setTextResume(resultObj.getString(CommonKeys.LJS_TextResume));
                                            mSetupData.setUserName(resultObj.getString(CommonKeys.LJS_UserName));
                                            mSetupData.setStatus(resultObj.getString(CommonKeys.LJS_Status));
                                            mSetupData.setStdCurCompany(resultObj.getString(CommonKeys.LJS_StdCurCompany));
                                            mSetupData.setPreviousDesignation(resultObj.getString(CommonKeys.LJS_PreviousDesignation));

                                            mSetupData.setPreviousCompany(resultObj.getString(CommonKeys.LJS_PreviousCompany));
                                            mSetupData.setStdPreCompany(resultObj.getString(CommonKeys.LJS_StdPreCompany));
                                            mSetupData.setEmailVerified(resultObj.getString(CommonKeys.LJS_EmailVerified));
                                            mSetupData.setVisibleSettings(resultObj.getString(CommonKeys.LJS_VisibleSettings));
                                            mSetupData.setJsId(resultObj.getString(CommonKeys.LJS_JsId));
                                            mSetupData.setIsOnline(resultObj.getString(CommonKeys.LJS_IsOnline));
                                            mSetupData.setViewedCount(resultObj.getString(CommonKeys.LJS_ViewedCount));
                                            mSetupData.setDownloadedCount(resultObj.getString(CommonKeys.LJS_DownloadedCount));
                                            mSetupData.setCandidatesActiveinLast(resultObj.getString(CommonKeys.LJS_CandidatesActiveinLast));
//                                    mSetupData.setResumeperPage(resultObj.getString(CommonKeys.LJS_ResumeperPage));
//                                    mSetupData.setSearchQuery(resultObj.getString(CommonKeys.LJS_SearchQuery));
                                            mSetupData.setMatchedScore(resultObj.getString(CommonKeys.LJS_MatchedScore));
                                            mSetupData.setInId(resultObj.getString(CommonKeys.LJS_InId));
                                            mSetupData.setfId(resultObj.getString(CommonKeys.LJS_FId));
                                            mSetupData.setrId(resultObj.getString(CommonKeys.LJS_RId));

                                            mSetupData.setjTId(resultObj.getString(CommonKeys.LJS_JTId));
                                            mSetupData.setCreatedBy(resultObj.getString(CommonKeys.LJS_CreatedBy));

                                            mSetupData.setDirectRegistration(resultObj.getString(CommonKeys.LJS_DirectRegistration));
                                            mSetupData.setProfileAccessSpecifier(resultObj.getString(CommonKeys.LJS_ProfileAccessSpecifier));
                                            mSetupData.setSharedCompaniesList(resultObj.getString(CommonKeys.LJS_SharedCompaniesList));


                                        }
                                        oncomplete.execute(true, mSetupData, null);

                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }
                                }
                            });
                        }
                    });

                }


                public void getRecruiterDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getRecruiterDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                                ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.userDetails, CommonUtils.userType, "" + emailEdt.getText().toString().trim(), "" + passwordEdt.getText().toString().trim(), regid);
                            HttpClient.download(ConfigForgoturl, false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid") || result.toString().contains("\"Your email not yet verified,please check your mail to verify..\"")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    RecuiterObject mRecData = null;
                                    JSONArray jArray = null;
                                    try {
                                        jArray = new JSONArray(result.toString());
                                        if (jArray.length() == 0) {
                                            oncomplete.execute(false, getActivity().getResources().getString(R.string.not_registered), null);
                                            return;
                                        }
                                        for (int i = 0; i < jArray.length(); i++) {
                                            JSONObject resultObj = jArray.getJSONObject(i);
                                            mRecData = new RecuiterObject();
                                            mRecData.setRecruterId(resultObj.getString(CommonKeys.LJS_RecRecruterId));
                                            mRecData.setFirstName(resultObj.getString(CommonKeys.LJS_RecFirstName));
                                            mRecData.setLastName(resultObj.getString(CommonKeys.LJS_RecLastName));
                                            mRecData.setPassword(resultObj.getString(CommonKeys.LJS_RecPassword));
                                            mRecData.setConfirmPassword(resultObj.getString(CommonKeys.LJS_RecConfirmPassword));
                                            mRecData.setCompanyName(resultObj.getString(CommonKeys.LJS_RecCompanyName));

                                             CompanyName = resultObj.getString(CommonKeys.LJS_RecCompanyName);

                                            mRecData.setStdCurCompany(resultObj.getString(CommonKeys.LJS_RecStdCurCompany));
                                            mRecData.setEmail(resultObj.getString(CommonKeys.LJS_RecEmail));
                                            mRecData.setCompanyUrl(resultObj.getString(CommonKeys.LJS_RecCompanyUrl));
                                            mRecData.setContactNo(resultObj.getString(CommonKeys.LJS_RecContactNo));
                                            mRecData.setContactNoLandline(resultObj.getString(CommonKeys.LJS_RecContactNo_Landline));
                                            mRecData.setEmployType(resultObj.getString(CommonKeys.LJS_RecEmployType));
                                            mRecData.setIndustryType(resultObj.getString(CommonKeys.LJS_RecIndustryType));
                                            mRecData.setLocation(resultObj.getString(CommonKeys.LJS_RecLocation));
                                            mRecData.setAddress(resultObj.getString(CommonKeys.LJS_RecAddress));
                                            mRecData.setCompanyProfile(resultObj.getString(CommonKeys.LJS_RecCompanyProfile));
                                            mRecData.setKeySkills(resultObj.getString(CommonKeys.LJS_RecKeySkills));
                                            mRecData.setActivation(resultObj.getString(CommonKeys.LJS_RecActivation));
                                            mRecData.setRegDate(resultObj.getString(CommonKeys.LJS_RecRegDate));
                                            mRecData.setEmailVerified(true);
                                            mRecData.setISOnline(true);
                                            mRecData.setDesignation(resultObj.getString(CommonKeys.LJS_RecDesignation));
//                                    mRecData.setLogoString(resultObj.getString(CommonKeys.LJS_RecLogoString));

                                        }
                                            oncomplete.execute(true, mRecData, null);

                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }
                                }
                            });
                        }
                    });
                }


                public void getSubuserListDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getSubuserListDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.getSubUsers,"" + emailEdt.getText().toString().trim());
                            HttpClient.download(ConfigForgoturl, false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    SubUserModel mSubuserData = null;
                                    ArrayList<SubUserModel> mSubuserDataList = new ArrayList<SubUserModel>();
                                    JSONArray jArray = null;
                                    try {
                                        jArray = new JSONArray(result.toString());
                                        if (jArray.length() == 0) {
                                            oncomplete.execute(false, "Please create atleast one subuser using website", null);
                                            return;
                                        }
                                        for (int i = 0; i < jArray.length(); i++) {
                                            JSONObject resultObj = jArray.getJSONObject(i);
                                            mSubuserData = new SubUserModel();
                                            mSubuserData.setCompanyEmailId(resultObj.getString("CompanyEmailId"));
                                            mSubuserData.setSubUserEmailId(resultObj.getString("SubUserEmailId"));
                                            mSubuserData.setContactNo(resultObj.getString("ContactNo"));
                                            mSubuserData.setLandlineNo(resultObj.getString("LandlineNo"));
                                            mSubuserData.setUserName(resultObj.getString("UserName"));
                                            mSubuserDataList.add(mSubuserData);
                                        }
                                        oncomplete.execute(true, mSubuserDataList, null);
                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }
                                }
                            });
                        }
                    });
                }

                public void getSubuserDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getsUBUSERDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            ConfigForgoturl = String.format(Config.LJS_BASE_URL + Config.getSubUserDetails,"" + popup_subunameSpin.getSelectedItem().toString(), emailEdt.getText().toString(), ""+ popup_subpasswordEdt.getText().toString().trim(),regid);
                            HttpClient.download(ConfigForgoturl, false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    SubUserModel mSubuserData = null;
                                    JSONArray jArray = null;
                                    try {
                                        jArray = new JSONArray(result.toString());
                                        if (jArray.length() == 0) {
                                            oncomplete.execute(false, getActivity().getResources().getString(R.string.not_registered), null);
                                            return;
                                        }
                                        for (int i = 0; i < jArray.length(); i++) {
                                            JSONObject resultObj = jArray.getJSONObject(i);
                                                mSubuserData = new SubUserModel();
                                                mSubuserData.setCompanyEmailId(resultObj.getString("CompanyEmailId"));
                                                mSubuserData.setRecruiterCompanyName(CompanyName);
                                                mSubuserData.setSubUserEmailId(resultObj.getString("SubUserEmailId"));
                                                mSubuserData.setContactNo(resultObj.getString("ContactNo"));
                                                mSubuserData.setLandlineNo(resultObj.getString("LandlineNo"));
                                                mSubuserData.setUserName(resultObj.getString("UserName"));

                                        }
                                        oncomplete.execute(true, mSubuserData, null);
                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }
                                }
                            });
                        }
                    });
                }

                public void getTrainerDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getTrainerDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            HttpClient.download(String.format(Config.LJS_BASE_URL + Config.TrainerDetails, "" + emailEdt.getText().toString().trim(), "" + passwordEdt.getText().toString()), false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains(getResources().getString(R.string.invalid_Responce))) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    TrainerRegistrationModel mRecData = null;
                                    try {
                                        if (result != null && ((String) result).toString().length() > 0) {
                                            try {
                                                Type collectionType = new TypeToken<Collection<TrainerRegistrationModel>>() {
                                                }.getType();
                                                Gson googleJson = new Gson();
                                                List<TrainerRegistrationModel> userDataList = googleJson.fromJson((String) result, collectionType);
                                                Log.v(LiveData.class.getSimpleName(), "Trainer Register size.." + userDataList.size());
                                                JSONArray jArray = null;
                                                jArray = new JSONArray(result.toString());
                                                LinkedHashMap<String, String> map = null;
                                                for (int i = 0; i < jArray.length(); i++) {
                                                    JSONObject resultObj = jArray.getJSONObject(i);
                                                    map = new LinkedHashMap<String, String>();
                                                    Iterator<?> keys = resultObj.keys();

                                                    while (keys.hasNext()) {
                                                        String key = (String) keys.next();
                                                        String value = resultObj.getString(key);
                                                        map.put(key, value);

                                                    }
                                                }

                                                final List<LinkedHashMap> trainersDataList = new ArrayList<LinkedHashMap>();
                                                trainersDataList.add(map);

                                                ApplicationThread.dbPost("Userdetails Saving..", "insert", new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        // TODO Auto-generated method stub
                                                        final DatabaseHelper seekerInsertDB = new DatabaseHelper(getActivity());
                                                        seekerInsertDB.insertData("TrainersRegistration", trainersDataList, getActivity());
                                                        oncomplete.execute(true, "success", null);
                                                    }
                                                });

//                                                oncomplete.execute(true, userDataList, null);
                                            } catch (Exception ex) {

                                                ex.printStackTrace();
                                                oncomplete.execute(false, null, null);
                                            }
                                        }

                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }

                                }
                            });
                        }
                    });
                }

                public void getFreshersDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getFreshersDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            HttpClient.download(String.format(Config.LJS_BASE_URL + Config.FresherDetails, "" + emailEdt.getText().toString().trim(), "" + passwordEdt.getText().toString().trim()), false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    try {
                                        if (result != null && ((String) result).toString().length() > 0) {
                                            try {
                                                Type collectionType = new TypeToken<Collection<FresherRegistrationModel>>() {
                                                }.getType();
                                                Gson googleJson = new Gson();
                                                List<FresherRegistrationModel> userDataList = googleJson.fromJson((String) result, collectionType);
                                                Log.v(LiveData.class.getSimpleName(), "Resumes size.." + userDataList.size());
                                                JSONArray jArray = null;
                                                jArray = new JSONArray(result.toString());
                                                LinkedHashMap<String, String> map = null;
                                                for (int i = 0; i < jArray.length(); i++) {
                                                    JSONObject resultObj = jArray.getJSONObject(i);
                                                    map = new LinkedHashMap<String, String>();
                                                    Iterator<?> keys = resultObj.keys();

                                                    while (keys.hasNext()) {
                                                        String key = (String) keys.next();
                                                        String value = resultObj.getString(key);
                                                        if (!key.equalsIgnoreCase("_id"))
                                                            map.put(key, value);


                                                    }
                                                }

                                                final List<LinkedHashMap> freshersDataList = new ArrayList<LinkedHashMap>();
                                                freshersDataList.add(map);

                                                ApplicationThread.dbPost("Userdetails Saving..", "insert", new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        // TODO Auto-generated method stub
                                                        final DatabaseHelper seekerInsertDB = new DatabaseHelper(getActivity());
                                                        seekerInsertDB.insertData("FreshersRegistration", freshersDataList, getActivity());
                                                        oncomplete.execute(true, "success", null);
                                                    }
                                                });

                                                oncomplete.execute(true, userDataList, null);
                                            } catch (Exception ex) {

                                                ex.printStackTrace();
                                                oncomplete.execute(false, null, null);
                                            }
                                        }

                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }
                                }
                            });
                        }
                    });
                }

                public void getEmployeeRefDetailsHere(final ApplicationThread.OnComplete oncomplete) {
                    ApplicationThread.bgndPost(getClass().getSimpleName(), "getEmployeeReferalDetailsHere...", new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            HttpClient.download(String.format(Config.LJS_BASE_URL + Config.TrainerDetails, "" + emailEdt.getText().toString().trim(), "" + passwordEdt.getText().toString()), false, new ApplicationThread.OnComplete() {
                                public void run() {
                                    if (success == false || result == null || result.toString().contains("Invalid")) {
                                        oncomplete.execute(false, result, null);
                                        return;
                                    }
                                    TrainerRegistrationModel mRecData = null;
                                    try {
                                        if (result != null && ((String) result).toString().length() > 0) {
                                            try {
                                                Type collectionType = new TypeToken<Collection<TrainerRegistrationModel>>() {
                                                }.getType();
                                                Gson googleJson = new Gson();
                                                List<TrainerRegistrationModel> userDataList = googleJson.fromJson((String) result, collectionType);
                                                Log.v(LiveData.class.getSimpleName(), "Trainer Register size.." + userDataList.size());
                                                JSONArray jArray = null;
                                                jArray = new JSONArray(result.toString());
                                                LinkedHashMap<String, String> map = null;
                                                for (int i = 0; i < jArray.length(); i++) {
                                                    JSONObject resultObj = jArray.getJSONObject(i);
                                                    map = new LinkedHashMap<String, String>();
                                                    Iterator<?> keys = resultObj.keys();

                                                    while (keys.hasNext()) {
                                                        String key = (String) keys.next();
                                                        String value = resultObj.getString(key);
                                                        map.put(key, value);

                                                    }
                                                }

                                                final List<LinkedHashMap> trainersDataList = new ArrayList<LinkedHashMap>();
                                                trainersDataList.add(map);

                                                ApplicationThread.dbPost("Userdetails Saving..", "insert", new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        // TODO Auto-generated method stub
                                                        final DatabaseHelper seekerInsertDB = new DatabaseHelper(getActivity());
                                                        seekerInsertDB.insertData("TrainersRegistration", trainersDataList, getActivity());
                                                        oncomplete.execute(true, "success", null);
                                                    }
                                                });

                                                oncomplete.execute(true, userDataList, null);
                                            } catch (Exception ex) {

                                                ex.printStackTrace();
                                                oncomplete.execute(false, null, null);
                                            }
                                        }

                                    } catch (Exception e) {
                                        Message messageToParent = new Message();
                                        messageToParent.what = RESULT_ERROR;
                                        Bundle bundleData = new Bundle();
                                        bundleData.putString("Time Over", "Lost Internet Connection");
                                        messageToParent.setData(bundleData);
                                        new StatusHandler().sendMessage(messageToParent);
                                    }

                                }
                            });
                        }
                    });
                }

                public void GetForgotPassword() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Please wait...");
                        progressDialog.setCancelable(false);
                    }
                    progressDialog.show();


                    LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                        public void run() {
                            if (success != true) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "" + result.toString().replace("\"", ""), Toast.LENGTH_LONG).show();
                                if (result != null) {
                                }
                                return;
                            }
                            if (result != null) {
                                if (result.toString().equalsIgnoreCase("true")) {
                                    Toast.makeText(getActivity(), "Password Sent to your mail Please check", Toast.LENGTH_LONG).show();
                                    progressDialog.cancel();
                                }
                            }
                        }
                    }, ConfigForgoturl);
                }


                @Override
                public void onAttach(Activity activity) {
                    super.onAttach(activity);
                    try {
                        if (null != ARG_PARAM1 && ARG_PARAM1.length() > 0 && !ARG_PARAM1.equalsIgnoreCase("param1")) {
                            ((MainInfoActivity) activity).onSectionAttached("Login");
                        }
                    } catch (ClassCastException e) {
                        throw new ClassCastException(activity.toString()
                                + " must implement OnFragmentInteractionListener");
                    }
                }

                class StatusHandler extends Handler {
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case RESULT_ERROR:
                                Toast.makeText(getActivity(), "Error occurred while getting data from  server.", Toast.LENGTH_SHORT).show();
                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                }
                                break;
                            case RESULT_USERDATA:
//                        Toast.makeText(getActivity(), "You  are logged into Local Job Server successfully..", Toast.LENGTH_SHORT).show();
                                if (CommonKeys.USER_LOGIN_TYPE != 2) {
                                    CommonUtils.userEmail = emailEdt.getText().toString();
                                    CommonUtils.isLogin = true;
                                    CommonUtils.isUserExisted = true;
//                                    if (CommonUtils.fromAppliedJobs){
//                                        Intent jobIntent = new Intent(getActivity(), JobDescriptionActivity.class);
//                                        jobIntent.putExtra("selectedPos", CommonUtils.selectedPos_withotLogin);
//                                        jobIntent.putExtra("_id", CommonUtils.id_withotLogin);
//                                        jobIntent.setAction("fromSearch");
//                                        startActivity(jobIntent);
//                                        CommonUtils.fromAppliedJobs = false;
//
//                                    }else {
                                        startActivity(new Intent(getActivity(), MainInfoActivity.class).putExtra("position", 0));
//                                    }

                                    getActivity().finish();
                                } else {
                                    if (CommonUtils.isFresherLoggedIn(getActivity())) {
                                        startActivity(new Intent(getActivity(), FnTNavigationActivity.class));
                                        getActivity().finish();
                                    }else if (CommonUtils.isSubuserLoggedIn(getActivity())){
                                        CommonUtils.isLogin = true;
                                        CommonUtils.isUserExisted = true;
                                        startActivity(new Intent(getActivity(), MainInfoActivity.class).putExtra("position", 0));
                                        getActivity().finish();
                                    } else {
                                        startActivity(new Intent(getActivity(), InstitutionNavigationActivity.class));
                                        getActivity().finish();
                                    }

                                }
                                break;
                        }
                    }
                }

            }

            @Override
            public void onBackPressed() {
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.v(getClass().getName(), "Check..count.." + fm.getBackStackEntryCount());
                    fm.popBackStack();
                    logoImg.setVisibility(View.VISIBLE);
                    loginLL.setVisibility(View.VISIBLE);
                    findLay.setVisibility(View.VISIBLE);
                } else {
                    this.finish();
                }
            }

        }
