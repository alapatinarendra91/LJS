package com.localjobserver.networkutils;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.Environment;

public class UrlPost
{
	private HttpURLConnection connection;
	private InputStream inStream;
	private static final int TIMEOUT_CONNECT_MILLIS = 200000;
	private static final int TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;
	
	public InputStream soapPost(String xmlString,URL url,String soapUrl,Context context) throws Exception
	{
		try
		{
			connection = (HttpURLConnection)url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
			connection.setReadTimeout(TIMEOUT_READ_MILLIS);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
			connection.setRequestProperty("Content-Length",""+xmlString.length() );
			connection.setRequestProperty("SOAPAction", soapUrl);
			DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(xmlString);
			outputStream.flush();
			
			inStream = (InputStream) connection.getInputStream();
			

		}
		catch(Exception e)
		{
			System.out.println("error:::"+e.getMessage());
			throw e;
		}
		return inStream;
	}

}

