package com.localjobserver.networkutils;

/**
 * Created by admin on 13-03-2015.
 */
public class Config {
    public static final boolean DEVELOPER_MODE = false;
    public static final String url = "http://192.168.3.25:8080/MyLoginForm/rest/WebService/";
    public static final String registration_url = "isRegistered/%s";
    public static final String getusers_url = "GetUsers";
    public static final String getjobcategories_url = "GetJobCategories";

    /*From Satish Anna*/
//    public static String LINKEDIN_CONSUMER_KEY = "751ualur529ui8";
//    public static String LINKEDIN_CONSUMER_SECRET = "tN7aJefIhFIZQNyt";
    /*
    * From Mine
    * */
    public static String LINKEDIN_CONSUMER_KEY = "81ff7culzygg2w";
    public static String LINKEDIN_CONSUMER_SECRET = "62vYPTt6dMXab8em";


    public static String OAUTH_CALLBACK_SCHEME = "x-oauthflow-linkedin";
    public static String OAUTH_CALLBACK_HOST = "callback";
    public static String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;


    //     Production (Live)
    public static String LJS_BASE_URL = "https://www.talentzing.com/LjsAppService.asmx";
//    Qa (Testing)
//    public static String LJS_BASE_URL = "http://ljsjobs.com/LjsAppService.asmx";

    public static String userDetails = "/userDetails?userTypeId=%s&EmailId=%s&password=%s&deviceId=%s";
    public static String getSubUserDetails = "/getSubUserDetails?subEmail=%s&companyEmail=%s&password=%s&deviceId=%s";
    public static String getSubUsers = "/getSubUsers?companyEmail=%s";
    public static String TrainerDetails = "/trainerDetails?emailId=%s&password=%s";
    public static String FresherDetails = "/FresherDetails?emailId=%s&password=%s";
    public static String getKeywords = "/getKeywords";
    public static String getIndustries = "/getIndustries";
    public static String getJobs = "/getjobs?keywords=%s&indid=%s";
    public static String getAppliedJobsBySMS = "/getAppliedJobsBySMS?Email=%s";
    public static String getSimilarJobs = "/getSimilarJobs?id=%s";
    public static String getSimilarProfiles = "/getSimilarProfiles?email=%s";
    public static String getAppliedJobs = "/getAppliedJobs?email=%s";
    public static String getJobsListToShare = "/getJobsTemplatesToShare?recEmail=%s";
    public static String getRecommendedJobs = "/getRecommendedJobs?email=%s";
    public static String getSavedContacts = "/getSavedContacts?recEmail=%s";
    public static String getAppliedJobs_freshars = "/getFresherAppliedCourses?email=%s";
    public static String getRecommendedJobs_freshars = "/getRecommendedCourses?email=%s";
    public static String applyJob = "/applyJob?jobId=%s&email=%s";
    public static String deleteJob = "/deleteJob?id=%s";
    public static String deletePostedCourse = "/deletePostedCourse?id=%s";
    public static String EnrollFresher = "/EnrollFresher?email=%s&id=%s";
    public static String getResumes = "/getResumes?keywords=%s&indid=%s";
    public static String LoadRepliedSMSJobTitles = "/LoadRepliedSMSJobTitles?companyEmail=%s&RecruierEmail=%s";
    public static String LoadAppliedSMSJobTitles = "/LoadAppliedSMSJobTitles?companyEmail=%s&RecruierEmail=%s";
    public static String LoadEmailJobTitles = "/LoadEmailJobTitles?companyEmail=%s&subUserEmail=%s";
    public static String LoadEmailRepliedCandidatesData = "/LoadEmailRepliedCandidatesData?selectedId=%s";
    public static String LoadSMSRepliedGridDetails = "/LoadSMSRepliedGridDetails?Smscode=%s";
    public static String UpdateSMSComments = "/UpdateSMSComments?Smscode=%s&comments=%s&mbl=%s";
    public static String UpdateEmailComments = "/UpdateEmailComments?jobId=%s&companyEmail=%s&SubuserEmail=%s&jsEmail=%s&comments=%s";

    public static String getEducation = "/getEducation";
    public static String getCompanies = "/getCompanies";
    public static String getDesignations = "/getDesignations";
    public static String getLocations = "/getLocations";
    public static String getLocalities = "/getLocalities?locationID=%s";
    public static String getLanguages = "/getLanguages";
    public static String getDiscountTypes = "/getDiscountTypes";
    public static String registerJobSeeker = "/registerJobSeeker?details=%s";
    public static String getJobSearchResults = "/getJobSearchResults?searchKeysDetails=%s";
    public static String registerRecruiter = "/registerRecruiter?details=%s";
    public static String loginDetails = "/api/Users/Emplogin?username=%s&password=%s";
    public static String seekerLogout = "/seekerLogout?email=%s";
    public static String recruiterLogout = "/recruiterLogout?email=%s";
    public static String subuserLogout = "/subuserLogout?subEmail=%s&companyEmail=%s";
    public static String registerTrainer = "/registerTrainingInstitute?details=%s";
    public static String registerOnlineTrainer = "/registerOnlineTrainer?details=%s";
    public static String modifyTrainerDetails = "/modifyTrainerDetails?email=%s&details=%s";
    public static String postCoursesTrainer = "/postCoursesTrainer?details=%s";
    public static String modifyCourse = "/modifyCourse?Details=%s&courseId=%s";
    public static String getCoursesTrainer = "/getCoursesTrainer?Email=%s";
    public static String registerFresher = "/registerFresher?details=%s";
    public static String postElearnings = "/postElearnings?details=%s";
    public static String sendSMS = "/sendSMS?jobId=%s&emailId=%s"; //rec
    public static String getLocalJobsCount = "/getLocalJobsCount?loc=%s&locality=%s";
    public static String getLocalitiesWithLatLng = "/getLocalitiesWithLatLng?location=%s";
    public static String getLocalCourceList = "/getLocalCourses?location=%s";
    public static String getLocalJobsList = "/getLocalJobsList?rid=%s&loc=%s";
    public static String getMyJobPostings = "/getJobPostings?companyEmail=%s&subUserEmail=%s";
    public static String getAppliedResumesByJobId = "/getAppliedResumesByJobId?JobId=%s";
    public static String getMatchedProfiles = "/getMatchedProfiles?jobid=%s";
    public static String sendFeedcack = "/sendFeedback?name=%s&email=%s&msg=%s&rate=%s";
    public static String deleteSavedResume = "/deleteSavedResume?emailidsToDelete=%s&recEmail=%s&folderName=%s";
    public static String shareProfilesWithRecruiters = "/shareProfilesWithRecruiters?emailsToShre=%s&profilesEmails=%s&recruiterEmail=%s&description=%s&isTrackerChecked=%s";
    public static String sendEmail = "/sendEmail?fromEmail=%s&toEmail=%s&jobId=%s&subject=%s&message=%s"; //Seeker
    public static String sendJobAsSms = "/sendJobAsSms?email=%s&mobileNumbers=%s&subject=%s&jobId=%s"; //Seeker
    public static String sendRecruiterBulkEmailOrSMS = "/sendRecruiterBulkEmailOrSMS?details=%s&email=%s&toAddress=%s&mode=%s";
    public static String SendCourseAsEmail = "/SendCourseAsEmail?fromEmail=%s&toEmail=%s&courseId=%s&subject=%s&message=%s";
    public static String EmployeeShareReferral = "/EmployeeShareReferral?jobId=%s&email=%s&sharingEmailsList=%s&subject=%s&message=%s";
    public static String modifyJSDetails = "/modifyJSDetails?email=%s&details=%s";
    public static String modifyRecDetails = "/modifyRecDetails?email=%s&details=%s";
    public static String modifyFreshersDetails = "/modifyFreshersDetails?email=%s&details=%s";
    public static String getFresherEnrolledCourses = "/getFresherEnrolledCourses?emailId=%s";
    public static String potentialCandidates = "/potentialCandidates?Email=%s";

    public static String changePassword = "/changePassword?email=%s&pwd=%s&npwd=%s";
    public static String sendVenue = "/sendVenueDetails?number=%s&jobTitle=%s&address=%s";
    public static String sendSMSCommunicationToCandidates = "/sendSMSCommunicationToCandidates?subUserEmail=%s&companyEmail=%s&recruiterNumber=%s&recieversNumbers=%s&jobTitle=%s&msg=%s";
    public static String sendEmailCommunicationToCandidates = "/sendEmailCommunicationToCandidates?subUserEmail=%s&companyEmail=%s&recieversEmails=%s&msg=%s";
    public static String sendVenueDetailsAsEmail = "/sendVenueDetailsAsEmail?emails=%s&jobTitle=%s&address=%s&recrEmail=%s";
    public static String changeVisibleSettings = "/changeVisibleSettings?email=%s&val=%s";
    public static String getJsVisibleSettings = "/getJsVisibleSettings?email=%s";
    public static String getRoles = "/getRoles?inId=%s";
    //    public static String uploadImage = "/uploadImage?fileName=%s&email=%s";
    public static String alertsJobSeeker = "/alertsJobSeeker?details=%s";
    public static String recruiterPrivacySettings = "/recruiterPrivacySettings?email=%s&day=%s&time=%s&emailAlert=%s&smsAlert=%s&chatAlert=%s";
    public static String fresherPrivacySettings = "/fresherPrivacySettings?email=%s&day=%s&time=%s&emailAlert=%s&chatAlert=%s";
    public static String trainerPrivacySettings = "/trainerPrivacySettings?email=%s&day=%s&time=%s&emailAlert=%s&chatAlert=%s";
    public static String getDetailsFromResume = "/getDetailsFromResume";
    public static String uploadResume = "/uploadResume";
    public static String deleteResume = "/deleteResume?email=%s";
    public static String saveResume = "/saveResume?fileName=%s&email=%s";
    public static String getSavedFolders = "/getSavedFolders?Email=%s";
    public static String RecruiterPostJob = "/RecruiterPostJob?details=%s";
    public static String RecruiterUpdatePostJob = "/RecruiterUpdatePostJob?details=%s&id=%s";
    public static String updateJobDescription = "/updateJobDescription?description=%s&id=%s";
    public static String updateJobCompanyProfile = "/updateJobCompanyProfile?companyDescription=%s&id=%s";
    public static String ResumeResults = "/ResumeResults?details=%s";
    public static String getSeekerPrivacySettings = "/getSeekerPrivacySettings?Email=%s";
    public static String getFresherPrivacySettings = "/getFresherPrivacySettings?email=%s";
    public static String getTrainerPrivacySettings = "/getTrainerPrivacySettings?email=%s";
    public static String getRecruiterPrivacySettings = "/getRecruiterPrivacySettings?Email=%s";
    public static String getProfilePic = "/getProfilePic?emailId=%s";
    public static String getRecruiterProfilePic = "/getRecruiterProfilePic?emailId=%s";
    public static String checkSeekerEmail = "/checkSeekerEmail?email=%s&mobile=%s";
    public static String getSavedResumes = "/getSavedResumes?Email=%s&FolderName=%s";
    public static String getProfileResume = "/getProfileResume?emailId=%s";
    public static String getFreshersProfileResume = "/getFreshersProfileResume?emailId=%s";
    public static String getBrochure = "/getBrochure?courseId=%s";
    public static String getFresherProfilePic = "/getFresherProfilePic?emailId=%s";
    public static String getTrainerProfilePic = "/getTrainerProfilePic?emailId=%s";
    public static String uploadImage = "/uploadImage";
    public static String getInstitutes = "/getInstitutes";
    public static String getElearnings = "/getElearnings";
    public static String getOfferedCourses = "/getOfferedCourses";
    public static String fresherChangePassword = "/fresherChangePassword?email=%s&pwd=%s&npwd=%s";
    public static String changeRecruiterPassword = "/changeRecruiterPassword?email=%s&pwd=%s&npwd=%s";
    public static String changeSubUserPassword = "/changeSubUserPassword?companyEmail=%s&subUserEmail=%s&pwd=%s&npwd=%s";
    public static String trainerChangePassword = "/trainerChangePassword?email=%s&pwd=%s&npwd=%s";
    public static String getTopInstitutes = "/getTopInstitutes";
    public static String SendGCMNotification = "/SendGCMNotification?message=%s&fromUserId=%s&toUserId=%s&type=%s";
    public static String getRecommendedCourses = "/getRecommendedCourses?Email=%s";
    public static String SearchFresherJobsByCourseWithLocation = "/SearchFresherJobsByCourseWithLocation?location=%s&course=%s";
    public static String getFreshersLocalCoursesWithCount = "/getFreshersLocalCoursesWithCount?Location=%s";
    public static String simpleCourseSearch = "/simpleCourseSearch?courseName=%s&city=%s&locality=%s&instName=%s&discount=%s";
    public static String forgetPasswordFreshers = "/forgetPasswordFreshers?email=%s";
    public static String forgetPasswordJobSeeker = "/forgetPasswordJobSeeker?email=%s";
    public static String forgetPasswordRecruiter = "/forgetPasswordRecruiter?email=%s";
    public static String forgetPasswordTrainer = "/forgetPasswordTrainer?email=%s";
}
