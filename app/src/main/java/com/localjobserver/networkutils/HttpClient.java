package com.localjobserver.networkutils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.http.AndroidHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Class for communicating with the sever.
 */

@SuppressLint("NewApi")
public class HttpClient {
    public static boolean offline = false;
    public static final String OFFLINE = "3";
    private static final int CONNECTION_TIMEOUT = 2000000; // 20 seconds
    public boolean servicefailed=false;
    public static final String SOAP_ACTION_URL =  "http://localhost:1966/LocalJobServer/";
    /**
     * Downloading data from server
     * @param url   -----> web service url
     * @param asBytes  ---> if the value is true return data in bytes form else string format
     * @param onComplete  ---> class to post the data to ui
     */
    public static void download(final String url, final boolean asBytes, final ApplicationThread.OnComplete onComplete) {
        // check the connectivity mode
        if (offline) {
            onComplete.execute(false, null, "ljs not connected");
            return;
        }

        final String encoded_url = encodeURL(url);
        com.localjobserver.networkutils.Log.d(HttpClient.class.getName(), "######## encoded_url: " + encoded_url);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParameters, CONNECTION_TIMEOUT);

        // Not running on main thread so can use AndroidHttpClient.newInstance
//        final android.net.http.AndroidHttpClient client = android.net.http.AndroidHttpClient.newInstance("ljs");

        // allow redirects
//        HttpClientParams.setRedirecting(client.getParams(), true);

        org.apache.http.impl.client.DefaultHttpClient client = new DefaultHttpClient(httpParameters);
        HttpGet request = new HttpGet(encoded_url);


        InputStream inputStream = null;
        try {

            HttpResponse response = client.execute(request);
            final int statusCode = response.getStatusLine().getStatusCode();
			Log.e(HttpClient.class.getName(), "inside...downloader.."+statusCode);

            if (statusCode != HttpStatus.SC_OK) {
                com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), "Error " + statusCode + " while retrieving data from " + encoded_url + "\n" + response.getStatusLine().getReasonPhrase());
                onComplete.execute(false, statusCode+"|"+response.getStatusLine().getReasonPhrase()+"|"+encoded_url, "Error " + statusCode + " while retrieving data from " + encoded_url + "\n" + response.getStatusLine().getReasonPhrase());
                return;
            }

            /** If response came successfully **/
            onComplete.execute(true, asBytes?org.apache.http.util.EntityUtils.toByteArray(response.getEntity()):org.apache.http.util.EntityUtils.toString(response.getEntity(), "UTF-8"), null);
        } catch (Exception e) {
            com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), e);

            onComplete.execute(false, null, e.getMessage());
        } finally {
        	if(inputStream != null){
        		try{
        			inputStream.close();
        		}
        		catch(Exception e)
        		{
        			//ignore
        		}
        	}
//            client.close();
        }
    }

	/**
	 * Replacing empty spaces with %20
	 * @param url  ---> Input url
	 * @return  ----> encoded url
	 */
    private static String encodeURL(String url) {
//        try {
//            return URLEncoder.encode(url, "UTF-8");
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        return url.trim().replaceAll(" ", "%20");
    }

    
    /**
     * If only header are required from server
     * @param url  ---> Input url
     * @param onComplete  ---> Interface for executing result
     */
    public static void get_header(final String url, final ApplicationThread.OnComplete onComplete) {
        // check the connectivity mode
        if (offline) {
            onComplete.execute(false, OFFLINE, "LJS not connected");
            return;
        }

        String encoded_url = encodeURL(url);
        encoded_url = encoded_url.trim().replaceAll(" ", "%20");
        com.localjobserver.networkutils.Log.d(HttpClient.class.getName(), "######## url: " + encoded_url);

        // Not running on main thread so can use AndroidHttpClient.newInstance
        final android.net.http.AndroidHttpClient client = android.net.http.AndroidHttpClient.newInstance("LJS");

        // allow redirects
        HttpClientParams.setRedirecting(client.getParams(), true);

        final org.apache.http.client.methods.HttpHead getRequest = new org.apache.http.client.methods.HttpHead(encoded_url);

        try {
            final org.apache.http.HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            onComplete.execute(HttpStatus.SC_OK==statusCode, response.getAllHeaders(), String.valueOf(statusCode));
        } catch (Exception e) {
            com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), "accessing: "+encoded_url, e);
            getRequest.abort();

            onComplete.execute(false, null, e.getMessage());
        } finally {
            client.close();
        }
    }



    /**
     * Posting data to server
     * @param url  ----> webservice url
     * @param values  ----> Hashmap which contains posting data keys and posring data values.
     * @param onComplete
     */
    public static void postDataToServer(final String url, final java.util.Map<String, String> values, final ApplicationThread.OnComplete onComplete) {
        // check the connectivity mode
        if (offline) {
            if (null != onComplete) onComplete.execute(false, null, "LJS not connected");
            return;
        }

        // Not running on main thread so can use AndroidHttpClient.newInstance
        final android.net.http.AndroidHttpClient client = android.net.http.AndroidHttpClient.newInstance("LJS");

        // enable redirects
        HttpClientParams.setRedirecting(client.getParams(), true);

        final org.apache.http.client.methods.HttpPost post = new org.apache.http.client.methods.HttpPost(url);

        try {
            final java.util.List<org.apache.http.NameValuePair> params = new java.util.ArrayList<org.apache.http.NameValuePair>();
            for (java.util.Map.Entry<String, String> entry : values.entrySet()) {
                params.add(new org.apache.http.message.BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            final org.apache.http.client.entity.UrlEncodedFormEntity entity = new org.apache.http.client.entity.UrlEncodedFormEntity(params, "UTF-8");
            post.setEntity(entity);
            final org.apache.http.HttpResponse response = client.execute(post);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT) {
                com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), "Error " + statusCode + " while retrieving data from " + url + "\n" + response.getStatusLine().getReasonPhrase());
                if (null != onComplete) onComplete.execute(false, statusCode+"|"+response.getStatusLine().getReasonPhrase()+"|"+url, "Error " + statusCode + " while posting data to " + url + "\n" + response.getStatusLine().getReasonPhrase());
                return;
            }
            if (statusCode != HttpStatus.SC_NO_CONTENT) {
                final String postResponse = org.apache.http.util.EntityUtils.toString(response.getEntity(), "UTF-8");
                com.localjobserver.networkutils.Log.d(HttpClient.class.getName(), "\n\npost response: \n" + postResponse);
                if (null != onComplete) onComplete.execute(true, postResponse, null);
            } else {
                if (null != onComplete) onComplete.execute(true, "HttpStatus: 204 No Content", null);
            }
        } catch(Exception e) {
            com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), e);
            post.abort();

            if (null != onComplete) onComplete.execute(false, null, e.getMessage());
        } finally {
            client.close();
        }
    }


    public static void postMultiPart(String url, String base64String, ApplicationThread.OnComplete<String> onComplete) {
        // check the connectivity mode
        if (offline) {
            if (null != onComplete) onComplete.execute(false, null, "LJS not connected");
            return;
        }

        final AndroidHttpClient client = AndroidHttpClient.newInstance("LJS");
        String BOUNDRY = "----_Part_40_30154021.1312258728328";

        // enable redirects
        HttpClientParams.setRedirecting(client.getParams(), true);

//        MultipartEntity mpEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//        try {
//            mpEntity.addPart("dummy", new StringBody(base64String));
//
//        } catch (Exception e){
//
//        }

        com.localjobserver.networkutils.Log.d(HttpClient.class.getName(), "######## url: " + url);

        final org.apache.http.client.methods.HttpPost post = new org.apache.http.client.methods.HttpPost(url);
        post.addHeader("encodeSting", "check");
//        post.addHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDRY);

//        post.setEntity(mpEntity);

//        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//        urlParameters.add(new BasicNameValuePair("encodeSting", "ass"));


        HttpResponse response;
        try {
//            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            response = client.execute(post);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (!(statusCode == HttpStatus.SC_OK || statusCode == HttpStatus.SC_CREATED)) {
                if (null != onComplete) onComplete.execute(false, statusCode + "|" + response.getStatusLine().getReasonPhrase() + "|" + post.getURI(), "Error " + statusCode + " while posting data to " + post.getURI() + "\nreason phrase: " + response.getStatusLine().getReasonPhrase());
                return;
            }

            if (null != onComplete)
                onComplete.execute(true, EntityUtils.toString(response.getEntity(), HTTP.UTF_8), null);

        } catch (IOException e) {
            com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), e);
        } finally {
            client.close();
        }
    }


    // HTTP GET request
    public static void sendGet(String url, String headerData, ApplicationThread.OnComplete<String> onComplet) {


        org.apache.http.impl.client.DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", "ljs");
        request.addHeader("details", headerData);

        try {
            HttpResponse response = client.execute(request);

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            onComplet.execute(true, result.toString(), null);
            System.out.println(result.toString());
        }catch (IOException e) {
            com.localjobserver.networkutils.Log.e(HttpClient.class.getName(), e);
            onComplet.execute(false, "", null);
        } finally {
        }


    }


    public static void uploadImage(String fileName,String encodeString,String email, String methodName, Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<"+methodName+" xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<fileName>"+fileName+"</fileName>"+
                            "<email>"+email+"</email>"+
                            "<encodeString>"+encodeString+"</encodeString>"+
                            "</"+methodName+">"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+methodName,context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }


    public static void uploadImage(String fileName,String encodeString,String email, Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<uploadImage xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<fileName>"+fileName+"</fileName>"+
                            "<email>"+email+"</email>"+
                            "<encodeString>"+encodeString+"</encodeString>"+
                            "</uploadImage>"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+"uploadImage",context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse,  null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }

    public static void uploadResume(String fileName,String encodeString,String email, String methodName, Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<"+methodName+" xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<fileName>"+fileName+"</fileName>"+
                            "<email>"+email+"</email>"+
                            "<encodeSting>"+encodeString+"</encodeSting>"+
                            "</"+methodName+">"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+methodName,context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }


    public static void uploadResume(String fileName,String encodeString,String email, Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<uploadResume xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<fileName>"+fileName+"</fileName>"+
                            "<email>"+email+"</email>"+
                            "<encodeSting>"+encodeString+"</encodeSting>"+
                            "</uploadResume>"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+"uploadResume",context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }

    public static void getResumeDetails(String fileName,String encodeString, Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<getDetailsFromResume xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<encodeSting>"+encodeString+"</encodeSting>"+
                            "<fileName>"+fileName+"</fileName>"+
                            "</getDetailsFromResume>"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+"getDetailsFromResume",context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }

    public static void uploadBrochure(String brochureString,String fileName, String courseId,Context context, ApplicationThread.OnComplete<String> onComplete)
    {
        String strResponse="";
        try
        {
            String xmlString=
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
                            "<soap:Body>"+
                            "<uploadBrochure xmlns=\"http://localhost:1966/LocalJobServer/\">"+
                            "<brochureString>"+brochureString+"</brochureString>"+
                            "<fileName>"+fileName+"</fileName>"+
                            "<courseId>"+courseId+"</courseId>"+
                            "</uploadBrochure>"+
                            "</soap:Body>"+
                            "</soap:Envelope>";

            UrlPost urlPost=new UrlPost();
            InputStream inputStream =urlPost.soapPost(xmlString, new URL(Config.LJS_BASE_URL),"http://localhost:1966/LocalJobServer/"+"uploadBrochure",context);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            strResponse = bufferedReader.readLine();

        }catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
        try{
            if(strResponse.contains("true")) {
                onComplete.execute(true, strResponse, null);

            }  else if(strResponse.contains("false"))  {
                onComplete.execute(false, strResponse, null);
            }  else {
            }
        } catch(Exception e){
            onComplete.execute(false, strResponse, null);
        }
    }
}
