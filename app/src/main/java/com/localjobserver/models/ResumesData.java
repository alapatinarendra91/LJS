package com.localjobserver.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 28-03-2015.
 */
public class ResumesData implements Parcelable{

    private String resumeHeadLine;
    private String resumeId;
    private String firstName;
    private String currentDesignation;
    private String education;
    private String jobExp;
    private String jobCTC;
    private String jobRole;
    private String jobNoticePeriod;
    private String jobKeySkills;
    private String jobConNum;
    private String yearofpassing;
    private String jobConPerson;
    private String jobFunArea;
    private String jobEmail;
    private String jobIndType;
    private String gender;
    private String institution;
    private String lastActiveDate;
    private String viewedCount;
    private String downloadedCount;
    private String qualification;
    private String preferedLoc;
    private String currentLoc;
    private String jobCompName;



    public String getResumeHeadLine() {
        return resumeHeadLine;
    }

    public void setResumeHeadLine(String resumeHeadLine) {
        this.resumeHeadLine = resumeHeadLine;
    }

    public String getResumeId() {
        return resumeId;
    }

    public void setResumeId(String resumeId) {
        this.resumeId = resumeId;
    }

    public String getCurrentDesignation() {
        return currentDesignation;
    }

    public void setCurrentDesignation(String currentDesignation) {
        this.currentDesignation = currentDesignation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getYearofpassing() {
        return yearofpassing;
    }

    public void setYearofpassing(String yearofpassing) {
        this.yearofpassing = yearofpassing;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getLastActiveDate() {
        return lastActiveDate;
    }

    public void setLastActiveDate(String lastActiveDate) {
        this.lastActiveDate = lastActiveDate;
    }

    public String getViewedCount() {
        return viewedCount;
    }

    public void setViewedCount(String viewedCount) {
        this.viewedCount = viewedCount;
    }

    public String getDownloadedCount() {
        return downloadedCount;
    }

    public void setDownloadedCount(String downloadedCount) {
        this.downloadedCount = downloadedCount;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getPreferedLoc() {
        return preferedLoc;
    }

    public void setPreferedLoc(String preferedLoc) {
        this.preferedLoc = preferedLoc;
    }

    public String getCurrentLoc() {
        return currentLoc;
    }

    public void setCurrentLoc(String currentLoc) {
        this.currentLoc = currentLoc;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getJobCompName() {
        return jobCompName;
    }


    public void setJobCompName(String jobCompName) {
        this.jobCompName = jobCompName;
    }


    public static Parcelable.Creator getCreator() {
        return CREATOR;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getJobExp() {
        return jobExp;
    }

    public void setJobExp(String jobExp) {
        this.jobExp = jobExp;
    }

    public String getJobCTC() {
        return jobCTC;
    }

    public void setJobCTC(String jobCTC) {
        this.jobCTC = jobCTC;
    }



    public String getJobNoticePeriod() {
        return jobNoticePeriod;
    }

    public void setJobNoticePeriod(String jobNoticePeriod) {
        this.jobNoticePeriod = jobNoticePeriod;
    }

    public String getJobKeySkills() {
        return jobKeySkills;
    }

    public void setJobKeySkills(String jobKeySkills) {
        this.jobKeySkills = jobKeySkills;
    }

    public String getJobConNum() {
        return jobConNum;
    }

    public void setJobConNum(String jobConNum) {
        this.jobConNum = jobConNum;
    }


    public String getJobConPerson() {
        return jobConPerson;
    }

    public void setJobConPerson(String jobConPerson) {
        this.jobConPerson = jobConPerson;
    }

    public String getJobFunArea() {
        return jobFunArea;
    }

    public void setJobFunArea(String jobFunArea) {
        this.jobFunArea = jobFunArea;
    }

    public String getJobEmail() {
        return jobEmail;
    }

    public void setJobEmail(String jobEmail) {
        this.jobEmail = jobEmail;
    }

    public String getJobIndType() {
        return jobIndType;
    }

    public void setJobIndType(String jobIndType) {
        this.jobIndType = jobIndType;
    }


    public  ResumesData(){

    }

    public ResumesData(Parcel in){
        resumeHeadLine  = in.readString();
        resumeId  = in.readString();
        firstName  = in.readString();
        currentDesignation  = in.readString();
        education  = in.readString();
        jobExp = in.readString();
        jobCTC = in.readString();
        jobRole = in.readString();
        jobNoticePeriod = in.readString();
        jobKeySkills = in.readString();
        jobConNum = in.readString();
        yearofpassing = in.readString();
        jobFunArea = in.readString();
        jobEmail = in.readString();
        jobIndType = in.readString();
        gender = in.readString();
        institution  = in.readString();
        lastActiveDate = in.readString();
        viewedCount = in.readString();
        downloadedCount = in.readString();
        qualification = in.readString();
        preferedLoc = in.readString();
        currentLoc  = in.readString();
        jobCompName  = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(resumeHeadLine);
        dest.writeString(resumeId);
        dest.writeString(firstName);
        dest.writeString(currentDesignation);
        dest.writeString(education);
        dest.writeString(jobExp);
        dest.writeString(jobCTC);
        dest.writeString(jobRole);
        dest.writeString(jobNoticePeriod);
        dest.writeString(jobKeySkills);
        dest.writeString(jobConNum);
        dest.writeString(yearofpassing);
        dest.writeString(jobFunArea);
        dest.writeString(jobEmail);
        dest.writeString(jobIndType);
        dest.writeString(gender);
        dest.writeString(institution);
        dest.writeString(lastActiveDate);
        dest.writeString(viewedCount);
        dest.writeString(downloadedCount);
        dest.writeString(qualification);
        dest.writeString(preferedLoc);
        dest.writeString(currentLoc);
        dest.writeString(jobCompName);
    }

    /** * * This field is needed for Android to be able to * create new objects, individually or as arrays. * *
     * This also means that you can use use the default * constructor to create the object and use another * method to hyrdate it as necessary.
     *  * * I just find it easier to use the constructor. * It makes sense for the way my brain thinks ;-)
     *  * */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ResumesData createFromParcel(Parcel in) {
            return new ResumesData(in);
        }
        public ResumesData[] newArray(int size) {
            return new ResumesData[size];
        } };


    @Override
    public int describeContents() {
        return 0;
    }
}
