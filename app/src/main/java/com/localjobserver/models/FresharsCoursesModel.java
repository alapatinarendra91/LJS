package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FresharsCoursesModel {

    @SerializedName("_id")
    @Expose
    private String Id;
    @SerializedName("InstituteName")
    @Expose
    private String InstituteName;
    @SerializedName("City")
    @Expose
    private String City;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("CourseFee")
    @Expose
    private String CourseFee;
    @SerializedName("CourseName")
    @Expose
    private String CourseName;
    @SerializedName("ContactNumber")
    @Expose
    private String ContactNumber;
    @SerializedName("DiscountType")
    @Expose
    private String DiscountType;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("Venue")
    @Expose
    private String Venue;
    @SerializedName("PostDate")
    @Expose
    private String PostDate;
    @SerializedName("StartDate")
    @Expose
    private String StartDate;
    @SerializedName("EndDate")
    @Expose
    private String EndDate;
    @SerializedName("AfterDiscountedfee")
    @Expose
    private Integer AfterDiscountedfee;
    @SerializedName("PostedBy")
    @Expose
    private String PostedBy;
    @SerializedName("Email")
    @Expose
    private String Email;

    /**
     *
     * @return
     * The Id
     */
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     * The _id
     */
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     * The InstituteName
     */
    public String getInstituteName() {
        return InstituteName;
    }

    /**
     *
     * @param InstituteName
     * The InstituteName
     */
    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    /**
     *
     * @return
     * The City
     */
    public String getCity() {
        return City;
    }

    /**
     *
     * @param City
     * The City
     */
    public void setCity(String City) {
        this.City = City;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The CourseFee
     */
    public String getCourseFee() {
        return CourseFee;
    }

    /**
     *
     * @param CourseFee
     * The CourseFee
     */
    public void setCourseFee(String CourseFee) {
        this.CourseFee = CourseFee;
    }

    /**
     *
     * @return
     * The CourseName
     */
    public String getCourseName() {
        return CourseName;
    }

    /**
     *
     * @param CourseName
     * The CourseName
     */
    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }

    /**
     *
     * @return
     * The ContactNumber
     */
    public String getContactNumber() {
        return ContactNumber;
    }

    /**
     *
     * @param ContactNumber
     * The ContactNumber
     */
    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    /**
     *
     * @return
     * The DiscountType
     */
    public String getDiscountType() {
        return DiscountType;
    }

    /**
     *
     * @param DiscountType
     * The DiscountType
     */
    public void setDiscountType(String DiscountType) {
        this.DiscountType = DiscountType;
    }

    /**
     *
     * @return
     * The Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     * The Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     * The Venue
     */
    public String getVenue() {
        return Venue;
    }

    /**
     *
     * @param Venue
     * The Venue
     */
    public void setVenue(String Venue) {
        this.Venue = Venue;
    }

    /**
     *
     * @return
     * The PostDate
     */
    public String getPostDate() {
        return PostDate;
    }

    /**
     *
     * @param PostDate
     * The PostDate
     */
    public void setPostDate(String PostDate) {
        this.PostDate = PostDate;
    }

    /**
     *
     * @return
     * The StartDate
     */
    public String getStartDate() {
        return StartDate;
    }

    /**
     *
     * @param StartDate
     * The StartDate
     */
    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    /**
     *
     * @return
     * The EndDate
     */
    public String getEndDate() {
        return EndDate;
    }

    /**
     *
     * @param EndDate
     * The EndDate
     */
    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    /**
     *
     * @return
     * The AfterDiscountedfee
     */
    public Integer getAfterDiscountedfee() {
        return AfterDiscountedfee;
    }

    /**
     *
     * @param AfterDiscountedfee
     * The AfterDiscountedfee
     */
    public void setAfterDiscountedfee(Integer AfterDiscountedfee) {
        this.AfterDiscountedfee = AfterDiscountedfee;
    }

    /**
     *
     * @return
     * The PostedBy
     */
    public String getPostedBy() {
        return PostedBy;
    }

    /**
     *
     * @param PostedBy
     * The PostedBy
     */
    public void setPostedBy(String PostedBy) {
        this.PostedBy = PostedBy;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

}