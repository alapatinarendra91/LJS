package com.localjobserver.models;

/**
 * Created by dell on 31-08-2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubUserModel {

    @SerializedName("CompanyEmailId")
    @Expose
    private String companyEmailId;
    @SerializedName("RecruiterCompanyName")
    @Expose
    private String RecruiterCompanyName;
    @SerializedName("Locality")
    @Expose
    private String Locality;
    @SerializedName("SubUserEmailId")
    @Expose
    private String subUserEmailId;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("LoginStatus")
    @Expose
    private String loginStatus;
    @SerializedName("EmailsLimit")
    @Expose
    private Integer emailsLimit;
    @SerializedName("EmailsUsed")
    @Expose
    private Integer emailsUsed;
    @SerializedName("SMSLimit")
    @Expose
    private Integer sMSLimit;
    @SerializedName("SMSUsed")
    @Expose
    private Integer sMSUsed;
    @SerializedName("ResumesLimit")
    @Expose
    private Integer resumesLimit;
    @SerializedName("ResumesUsed")
    @Expose
    private Integer resumesUsed;
    @SerializedName("DialUpCallsLimit")
    @Expose
    private Integer dialUpCallsLimit;
    @SerializedName("DialUpCallsUsed")
    @Expose
    private Integer dialUpCallsUsed;
    @SerializedName("JobPostingsLimit")
    @Expose
    private Integer jobPostingsLimit;
    @SerializedName("JobPostingsUsed")
    @Expose
    private Integer jobPostingsUsed;
    @SerializedName("PostWalkinsLimit")
    @Expose
    private Integer postWalkinsLimit;
    @SerializedName("PostWalkinsUsed")
    @Expose
    private Integer postWalkinsUsed;
    @SerializedName("SharedProfilesLimit")
    @Expose
    private Integer sharedProfilesLimit;
    @SerializedName("SharedProfilesUsed")
    @Expose
    private Integer sharedProfilesUsed;
    @SerializedName("TrackersLimit")
    @Expose
    private Integer trackersLimit;
    @SerializedName("TrackersUsed")
    @Expose
    private Integer trackersUsed;
    @SerializedName("SubLoginsLimit")
    @Expose
    private Integer subLoginsLimit;
    @SerializedName("SubLoginsUsed")
    @Expose
    private Integer subLoginsUsed;
    @SerializedName("BrandingCompanyLogo")
    @Expose
    private Integer brandingCompanyLogo;
    @SerializedName("UpdatedOn")
    @Expose
    private String updatedOn;
    @SerializedName("LandlineNo")
    @Expose
    private String landlineNo;
    @SerializedName("IsOnline")
    @Expose
    private Boolean isOnline;

    /**
     *
     * @return
     * The companyEmailId
     */
    public String getCompanyEmailId() {
        return companyEmailId;
    }

    /**
     *
     * @param companyEmailId
     * The CompanyEmailId
     */
    public void setCompanyEmailId(String companyEmailId) {
        this.companyEmailId = companyEmailId;
    }

    /**
     *
     * @return
     * The RecruiterCompanyName
     */
    public String getRecruiterCompanyName() {
        return RecruiterCompanyName;
    }

    /**
     *
     * @param RecruiterCompanyName
     * The RecruiterCompanyName
     */
    public void setRecruiterCompanyName(String RecruiterCompanyName) {
        this.RecruiterCompanyName = RecruiterCompanyName;
    }

    /**
     *
     * @return
     * The Locality
     */
    public String getLocality() {
        return Locality;
    }

    /**
     *
     * @param Locality
     * The Locality
     */
    public void setLocality(String Locality) {
        this.Locality = Locality;
    }

    /**
     *
     * @return
     * The subUserEmailId
     */
    public String getSubUserEmailId() {
        return subUserEmailId;
    }

    /**
     *
     * @param subUserEmailId
     * The SubUserEmailId
     */
    public void setSubUserEmailId(String subUserEmailId) {
        this.subUserEmailId = subUserEmailId;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The CreatedOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     * The UserName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     * The contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     *
     * @param contactNo
     * The ContactNo
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The Status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     *
     * @param userType
     * The UserType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The Password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     * The loginStatus
     */
    public String getLoginStatus() {
        return loginStatus;
    }

    /**
     *
     * @param loginStatus
     * The LoginStatus
     */
    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     *
     * @return
     * The emailsLimit
     */
    public Integer getEmailsLimit() {
        return emailsLimit;
    }

    /**
     *
     * @param emailsLimit
     * The EmailsLimit
     */
    public void setEmailsLimit(Integer emailsLimit) {
        this.emailsLimit = emailsLimit;
    }

    /**
     *
     * @return
     * The emailsUsed
     */
    public Integer getEmailsUsed() {
        return emailsUsed;
    }

    /**
     *
     * @param emailsUsed
     * The EmailsUsed
     */
    public void setEmailsUsed(Integer emailsUsed) {
        this.emailsUsed = emailsUsed;
    }

    /**
     *
     * @return
     * The sMSLimit
     */
    public Integer getSMSLimit() {
        return sMSLimit;
    }

    /**
     *
     * @param sMSLimit
     * The SMSLimit
     */
    public void setSMSLimit(Integer sMSLimit) {
        this.sMSLimit = sMSLimit;
    }

    /**
     *
     * @return
     * The sMSUsed
     */
    public Integer getSMSUsed() {
        return sMSUsed;
    }

    /**
     *
     * @param sMSUsed
     * The SMSUsed
     */
    public void setSMSUsed(Integer sMSUsed) {
        this.sMSUsed = sMSUsed;
    }

    /**
     *
     * @return
     * The resumesLimit
     */
    public Integer getResumesLimit() {
        return resumesLimit;
    }

    /**
     *
     * @param resumesLimit
     * The ResumesLimit
     */
    public void setResumesLimit(Integer resumesLimit) {
        this.resumesLimit = resumesLimit;
    }

    /**
     *
     * @return
     * The resumesUsed
     */
    public Integer getResumesUsed() {
        return resumesUsed;
    }

    /**
     *
     * @param resumesUsed
     * The ResumesUsed
     */
    public void setResumesUsed(Integer resumesUsed) {
        this.resumesUsed = resumesUsed;
    }

    /**
     *
     * @return
     * The dialUpCallsLimit
     */
    public Integer getDialUpCallsLimit() {
        return dialUpCallsLimit;
    }

    /**
     *
     * @param dialUpCallsLimit
     * The DialUpCallsLimit
     */
    public void setDialUpCallsLimit(Integer dialUpCallsLimit) {
        this.dialUpCallsLimit = dialUpCallsLimit;
    }

    /**
     *
     * @return
     * The dialUpCallsUsed
     */
    public Integer getDialUpCallsUsed() {
        return dialUpCallsUsed;
    }

    /**
     *
     * @param dialUpCallsUsed
     * The DialUpCallsUsed
     */
    public void setDialUpCallsUsed(Integer dialUpCallsUsed) {
        this.dialUpCallsUsed = dialUpCallsUsed;
    }

    /**
     *
     * @return
     * The jobPostingsLimit
     */
    public Integer getJobPostingsLimit() {
        return jobPostingsLimit;
    }

    /**
     *
     * @param jobPostingsLimit
     * The JobPostingsLimit
     */
    public void setJobPostingsLimit(Integer jobPostingsLimit) {
        this.jobPostingsLimit = jobPostingsLimit;
    }

    /**
     *
     * @return
     * The jobPostingsUsed
     */
    public Integer getJobPostingsUsed() {
        return jobPostingsUsed;
    }

    /**
     *
     * @param jobPostingsUsed
     * The JobPostingsUsed
     */
    public void setJobPostingsUsed(Integer jobPostingsUsed) {
        this.jobPostingsUsed = jobPostingsUsed;
    }

    /**
     *
     * @return
     * The postWalkinsLimit
     */
    public Integer getPostWalkinsLimit() {
        return postWalkinsLimit;
    }

    /**
     *
     * @param postWalkinsLimit
     * The PostWalkinsLimit
     */
    public void setPostWalkinsLimit(Integer postWalkinsLimit) {
        this.postWalkinsLimit = postWalkinsLimit;
    }

    /**
     *
     * @return
     * The postWalkinsUsed
     */
    public Integer getPostWalkinsUsed() {
        return postWalkinsUsed;
    }

    /**
     *
     * @param postWalkinsUsed
     * The PostWalkinsUsed
     */
    public void setPostWalkinsUsed(Integer postWalkinsUsed) {
        this.postWalkinsUsed = postWalkinsUsed;
    }

    /**
     *
     * @return
     * The sharedProfilesLimit
     */
    public Integer getSharedProfilesLimit() {
        return sharedProfilesLimit;
    }

    /**
     *
     * @param sharedProfilesLimit
     * The SharedProfilesLimit
     */
    public void setSharedProfilesLimit(Integer sharedProfilesLimit) {
        this.sharedProfilesLimit = sharedProfilesLimit;
    }

    /**
     *
     * @return
     * The sharedProfilesUsed
     */
    public Integer getSharedProfilesUsed() {
        return sharedProfilesUsed;
    }

    /**
     *
     * @param sharedProfilesUsed
     * The SharedProfilesUsed
     */
    public void setSharedProfilesUsed(Integer sharedProfilesUsed) {
        this.sharedProfilesUsed = sharedProfilesUsed;
    }

    /**
     *
     * @return
     * The trackersLimit
     */
    public Integer getTrackersLimit() {
        return trackersLimit;
    }

    /**
     *
     * @param trackersLimit
     * The TrackersLimit
     */
    public void setTrackersLimit(Integer trackersLimit) {
        this.trackersLimit = trackersLimit;
    }

    /**
     *
     * @return
     * The trackersUsed
     */
    public Integer getTrackersUsed() {
        return trackersUsed;
    }

    /**
     *
     * @param trackersUsed
     * The TrackersUsed
     */
    public void setTrackersUsed(Integer trackersUsed) {
        this.trackersUsed = trackersUsed;
    }

    /**
     *
     * @return
     * The subLoginsLimit
     */
    public Integer getSubLoginsLimit() {
        return subLoginsLimit;
    }

    /**
     *
     * @param subLoginsLimit
     * The SubLoginsLimit
     */
    public void setSubLoginsLimit(Integer subLoginsLimit) {
        this.subLoginsLimit = subLoginsLimit;
    }

    /**
     *
     * @return
     * The subLoginsUsed
     */
    public Integer getSubLoginsUsed() {
        return subLoginsUsed;
    }

    /**
     *
     * @param subLoginsUsed
     * The SubLoginsUsed
     */
    public void setSubLoginsUsed(Integer subLoginsUsed) {
        this.subLoginsUsed = subLoginsUsed;
    }

    /**
     *
     * @return
     * The brandingCompanyLogo
     */
    public Integer getBrandingCompanyLogo() {
        return brandingCompanyLogo;
    }

    /**
     *
     * @param brandingCompanyLogo
     * The BrandingCompanyLogo
     */
    public void setBrandingCompanyLogo(Integer brandingCompanyLogo) {
        this.brandingCompanyLogo = brandingCompanyLogo;
    }

    /**
     *
     * @return
     * The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     *
     * @param updatedOn
     * The UpdatedOn
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     *
     * @return
     * The landlineNo
     */
    public String getLandlineNo() {
        return landlineNo;
    }

    /**
     *
     * @param landlineNo
     * The LandlineNo
     */
    public void setLandlineNo(String landlineNo) {
        this.landlineNo = landlineNo;
    }

    /**
     *
     * @return
     * The isOnline
     */
    public Boolean getIsOnline() {
        return isOnline;
    }

    /**
     *
     * @param isOnline
     * The IsOnline
     */
    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

}
