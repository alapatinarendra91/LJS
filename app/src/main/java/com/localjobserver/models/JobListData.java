package com.localjobserver.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 19-03-2015.
 */
public class JobListData implements Parcelable{

    private String jobName;
    private String jobid;
    private String jobLocation;
    private String jobRole;
    private String jobExp;
    private String jobCTC;
    private String jobDec;
    private String jobNoticePeriod;
    private String jobKeySkills;
    private String jobEndClient;
    private String jobConNum;
    private String jobAddr;
    private String jobSeekerFName;
    private String jobFunArea;
    private String jobEmail;
    private String jobIndType;
    private String jobSeekerLName;
    private String jobSeekerPwd;
    private String jobSeekerCnfPwd;
    private String jobSeekerConNum1;
    private String jobSeekerExpCTC;
    private String jobSeekerEdu;
    private String jobSeekerStdSkills;
    private String jobSeekerUpdateResume;
    private String jobSeekerGender;
    private String jobSeekerInstitute;
    private String jobSeekerYP;
    private String jobSeekerProfileUpdate;
    private String jobSeekerResume;
    private String jobSeekerTextResume;
    private String jobSeekerUserName;
    private String jobSeekerStatus;

    public String getJobSeekerLName() {
        return jobSeekerLName;
    }

    public void setJobSeekerLName(String jobSeekerLName) {
        this.jobSeekerLName = jobSeekerLName;
    }

    public String getJobSeekerPwd() {
        return jobSeekerPwd;
    }

    public void setJobSeekerPwd(String jobSeekerPwd) {
        this.jobSeekerPwd = jobSeekerPwd;
    }

    public String getJobSeekerCnfPwd() {
        return jobSeekerCnfPwd;
    }

    public void setJobSeekerCnfPwd(String jobSeekerCnfPwd) {
        this.jobSeekerCnfPwd = jobSeekerCnfPwd;
    }

    public String getJobSeekerConNum1() {
        return jobSeekerConNum1;
    }

    public void setJobSeekerConNum1(String jobSeekerConNum1) {
        this.jobSeekerConNum1 = jobSeekerConNum1;
    }

    public String getJobSeekerExpCTC() {
        return jobSeekerExpCTC;
    }

    public void setJobSeekerExpCTC(String jobSeekerExpCTC) {
        this.jobSeekerExpCTC = jobSeekerExpCTC;
    }

    public String getJobSeekerEdu() {
        return jobSeekerEdu;
    }

    public void setJobSeekerEdu(String jobSeekerEdu) {
        this.jobSeekerEdu = jobSeekerEdu;
    }

    public String getJobSeekerStdSkills() {
        return jobSeekerStdSkills;
    }

    public void setJobSeekerStdSkills(String jobSeekerStdSkills) {
        this.jobSeekerStdSkills = jobSeekerStdSkills;
    }

    public String getJobSeekerUpdateResume() {
        return jobSeekerUpdateResume;
    }

    public void setJobSeekerUpdateResume(String jobSeekerUpdateResume) {
        this.jobSeekerUpdateResume = jobSeekerUpdateResume;
    }

    public String getJobSeekerGender() {
        return jobSeekerGender;
    }

    public void setJobSeekerGender(String jobSeekerGender) {
        this.jobSeekerGender = jobSeekerGender;
    }

    public String getJobSeekerInstitute() {
        return jobSeekerInstitute;
    }

    public void setJobSeekerInstitute(String jobSeekerInstitute) {
        this.jobSeekerInstitute = jobSeekerInstitute;
    }

    public String getJobSeekerYP() {
        return jobSeekerYP;
    }

    public void setJobSeekerYP(String jobSeekerYP) {
        this.jobSeekerYP = jobSeekerYP;
    }

    public String getJobSeekerProfileUpdate() {
        return jobSeekerProfileUpdate;
    }

    public void setJobSeekerProfileUpdate(String jobSeekerProfileUpdate) {
        this.jobSeekerProfileUpdate = jobSeekerProfileUpdate;
    }

    public String getJobSeekerResume() {
        return jobSeekerResume;
    }

    public void setJobSeekerResume(String jobSeekerResume) {
        this.jobSeekerResume = jobSeekerResume;
    }

    public String getJobSeekerTextResume() {
        return jobSeekerTextResume;
    }

    public void setJobSeekerTextResume(String jobSeekerTextResume) {
        this.jobSeekerTextResume = jobSeekerTextResume;
    }

    public String getJobSeekerUserName() {
        return jobSeekerUserName;
    }

    public void setJobSeekerUserName(String jobSeekerUserName) {
        this.jobSeekerUserName = jobSeekerUserName;
    }

    public String getJobSeekerStatus() {
        return jobSeekerStatus;
    }

    public void setJobSeekerStatus(String jobSeekerStatus) {
        this.jobSeekerStatus = jobSeekerStatus;
    }

    public String getJobSeekerPhoto() {
        return jobSeekerPhoto;
    }

    public void setJobSeekerPhoto(String jobSeekerPhoto) {
        this.jobSeekerPhoto = jobSeekerPhoto;
    }

    public String getJobSeekerCurDesig() {
        return jobSeekerCurDesig;
    }

    public void setJobSeekerCurDesig(String jobSeekerCurDesig) {
        this.jobSeekerCurDesig = jobSeekerCurDesig;
    }

    public String getJobSeekerCurComp() {
        return jobSeekerCurComp;
    }

    public void setJobSeekerCurComp(String jobSeekerCurComp) {
        this.jobSeekerCurComp = jobSeekerCurComp;
    }

    public String getJobSeekerStdComp() {
        return jobSeekerStdComp;
    }

    public void setJobSeekerStdComp(String jobSeekerStdComp) {
        this.jobSeekerStdComp = jobSeekerStdComp;
    }

    public String getJobSeekerPreviousDesig() {
        return jobSeekerPreviousDesig;
    }

    public void setJobSeekerPreviousDesig(String jobSeekerPreviousDesig) {
        this.jobSeekerPreviousDesig = jobSeekerPreviousDesig;
    }

    public String getJobSeekerPreviousComp() {
        return jobSeekerPreviousComp;
    }

    public void setJobSeekerPreviousComp(String jobSeekerPreviousComp) {
        this.jobSeekerPreviousComp = jobSeekerPreviousComp;
    }

    public String getJobSeekerStdPrefComp() {
        return jobSeekerStdPrefComp;
    }

    public void setJobSeekerStdPrefComp(String jobSeekerStdPrefComp) {
        this.jobSeekerStdPrefComp = jobSeekerStdPrefComp;
    }

    public String getJobSeekerPrefLocation() {
        return jobSeekerPrefLocation;
    }

    public void setJobSeekerPrefLocation(String jobSeekerPrefLocation) {
        this.jobSeekerPrefLocation = jobSeekerPrefLocation;
    }

    private String jobSeekerPhoto;
    private String jobSeekerCurDesig;
    private String jobSeekerCurComp;
    private String jobSeekerStdComp;
    private String jobSeekerPreviousDesig;
    private String jobSeekerPreviousComp;
    private String jobSeekerStdPrefComp;
    private String jobSeekerPrefLocation;






    public byte[] getSeekerphoto() {
        return seekerphoto;
    }

    public void setSeekerphoto(byte[] seekerphoto) {
        this.seekerphoto = seekerphoto;
    }

    private byte[] seekerphoto;

    public String getJobCompName() {
        return jobCompName;
    }

    public void setJobCompName(String jobCompName) {
        this.jobCompName = jobCompName;
    }

    private String jobCompName;


    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public static Creator getCreator() {
        return CREATOR;
    }

    private String postedOn;

    public String getJobEndClient() {
        return jobEndClient;
    }

    public void setJobEndClient(String jobEndClient) {
        this.jobEndClient = jobEndClient;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getJobExp() {
        return jobExp;
    }

    public void setJobExp(String jobExp) {
        this.jobExp = jobExp;
    }

    public String getJobCTC() {
        return jobCTC;
    }

    public void setJobCTC(String jobCTC) {
        this.jobCTC = jobCTC;
    }

    public String getJobDec() {
        return jobDec;
    }

    public void setJobDec(String jobDec) {
        this.jobDec = jobDec;
    }

    public String getJobNoticePeriod() {
        return jobNoticePeriod;
    }

    public void setJobNoticePeriod(String jobNoticePeriod) {
        this.jobNoticePeriod = jobNoticePeriod;
    }

    public String getJobKeySkills() {
        return jobKeySkills;
    }

    public void setJobKeySkills(String jobKeySkills) {
        this.jobKeySkills = jobKeySkills;
    }

    public String getJobConNum() {
        return jobConNum;
    }

    public void setJobConNum(String jobConNum) {
        this.jobConNum = jobConNum;
    }

    public String getJobAddr() {
        return jobAddr;
    }

    public void setJobAddr(String jobAddr) {
        this.jobAddr = jobAddr;
    }

    public String getJobSeekerFName() {
        return jobSeekerFName;
    }

    public void setJobSeekerFName(String jobSeekerFName) {
        this.jobSeekerFName = jobSeekerFName;
    }

    public String getJobFunArea() {
        return jobFunArea;
    }

    public void setJobFunArea(String jobFunArea) {
        this.jobFunArea = jobFunArea;
    }

    public String getJobEmail() {
        return jobEmail;
    }

    public void setJobEmail(String jobEmail) {
        this.jobEmail = jobEmail;
    }

    public String getJobIndType() {
        return jobIndType;
    }

    public void setJobIndType(String jobIndType) {
        this.jobIndType = jobIndType;
    }


    public  JobListData(){

    }

    public JobListData(Parcel in){
        jobName  = in.readString();
        jobid  = in.readString();
        jobLocation = in.readString();
        jobRole = in.readString();
        jobExp = in.readString();
        jobCTC = in.readString();
        jobDec = in.readString();
        jobNoticePeriod = in.readString();
        jobKeySkills = in.readString();
        jobEndClient  = in.readString();
        jobConNum = in.readString();
        jobAddr = in.readString();
        jobSeekerFName = in.readString();
        jobFunArea = in.readString();
        jobEmail = in.readString();
        jobIndType = in.readString();
        postedOn = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(jobName);
        dest.writeString(jobid);
        dest.writeString(jobLocation);
        dest.writeString(jobRole);
        dest.writeString(jobExp);
        dest.writeString(jobCTC);
        dest.writeString(jobDec);
        dest.writeString(jobNoticePeriod);
        dest.writeString(jobKeySkills);
        dest.writeString(jobEndClient);
        dest.writeString(jobConNum);
        dest.writeString(jobAddr);
        dest.writeString(jobSeekerFName);
        dest.writeString(jobFunArea);
        dest.writeString(jobEmail);
        dest.writeString(jobIndType);
        dest.writeString(postedOn);
    }

    /** * * This field is needed for Android to be able to * create new objects, individually or as arrays. * *
     * This also means that you can use use the default * constructor to create the object and use another * method to hyrdate it as necessary.
     *  * * I just find it easier to use the constructor. * It makes sense for the way my brain thinks ;-)
     *  * */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public JobListData createFromParcel(Parcel in) {
            return new JobListData(in);
        }
        public JobListData[] newArray(int size) {
            return new JobListData[size];
        } };


    @Override
    public int describeContents() {
        return 0;
    }

}
