package com.localjobserver.models;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * Created by siva on 11/07/15.
 */
public class SearchResultsServiceModel implements Serializable{

    @JsonProperty("_id")
    private String _id;
    @JsonProperty("FirstName")
    private String FirstName;
    @JsonProperty("LastName")
    private String LastName;
    @JsonProperty("CurrentDesignation")
    private String CurrentDesignation;
    @JsonProperty("CurrentCompany")
    private String CurrentCompany;
    @JsonProperty("NoticePeriod")
    private String NoticePeriod;
    @JsonProperty("PreferredLocation")
    private String PreferredLocation;
    @JsonProperty("ContactNo")
    private String ContactNo;
    @JsonProperty("Email")
    private String Email;
    @JsonProperty("Education")
    private String Education;
    @JsonProperty("Experience")
    private String Experience;
    @JsonProperty("Location")
    private String Location;
    @JsonProperty("KeySkills")
    private String KeySkills;
    @JsonProperty("ResumeHeadLine")
    private String ResumeHeadLine;
    @JsonProperty("CurrentCTC")
    private String CurrentCTC;
    @JsonProperty("ViewedCount")
    private String ViewedCount;
    @JsonProperty("DownloadedCount")
    private String DownloadedCount;
    @JsonProperty("similarCount")
    private String similarCount;
    @JsonProperty("MatchedPercent")
    private String MatchedPercent;
    @JsonProperty("RelevantScore")
    private String RelevantScore;
    @JsonProperty("IsOnline")
    private String IsOnline;
    @JsonProperty("Gender")
    private String Gender;
    @JsonProperty("Institute")
    private String Institute;
    @JsonProperty("Industry")
    private String Industry;
    @JsonProperty("Role")
    private String Role;
    @JsonProperty("YearOfPass")
    private String YearOfPass;
    @JsonProperty("isChecked")
    private boolean isChecked;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCurrentDesignation() {
        return CurrentDesignation;
    }

    public void setCurrentDesignation(String currentDesignation) {
        CurrentDesignation = currentDesignation;
    }

    public String getCurrentCompany() {
        return CurrentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        CurrentCompany = currentCompany;
    }

    public String getNoticePeriod() {
        return NoticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        NoticePeriod = noticePeriod;
    }

    public String getPreferredLocation() {
        return PreferredLocation;
    }

    public void setPreferredLocation(String preferredLocation) {
        PreferredLocation = preferredLocation;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getExperience() {
        return Experience;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getKeySkills() {
        return KeySkills;
    }

    public void setKeySkills(String keySkills) {
        KeySkills = keySkills;
    }

    public String getResumeHeadLine() {
        return ResumeHeadLine;
    }

    public void setResumeHeadLine(String resumeHeadLine) {
        ResumeHeadLine = resumeHeadLine;
    }

    public String getCurrentCTC() {
        return CurrentCTC;
    }

    public void setCurrentCTC(String currentCTC) {
        CurrentCTC = currentCTC;
    }

    public String getViewedCount() {
        return ViewedCount;
    }

    public void setViewedCount(String viewedCount) {
        ViewedCount = viewedCount;
    }

    public String getDownloadedCount() {
        return DownloadedCount;
    }

    public void setDownloadedCount(String downloadedCount) {
        DownloadedCount = downloadedCount;
    }

    public String getSimilarCount() {
        return similarCount;
    }

    public void setSimilarCount(String similarCount) {
        this.similarCount = similarCount;
    }

    public String getMatchedPercent() {
        return MatchedPercent;
    }

    public void setMatchedPercent(String matchedPercent) {
        MatchedPercent = matchedPercent;
    }

    public String getRelevantScore() {
        return RelevantScore;
    }

    public void setRelevantScore(String relevantScore) {
        RelevantScore = relevantScore;
    }
    public String getIsOnline() {
        return IsOnline;
    }

    public void setIsOnline(String isOnline) {
        IsOnline = isOnline;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        Gender = Gender;
    }

    public String getInstitute() {
        return Institute;
    }

    public void setInstitute(String Institute) {
        Institute = Institute;
    }
    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String Industry) {
        Industry = Industry;
    }
    public String getRole() {
        return Role;
    }

    public void setRole(String Role) {
        Role = Role;
    }

    public String getYearOfPass() {
        return YearOfPass;
    }

    public void setYearOfPass(String YearofPass) {
        YearOfPass = YearofPass;
    }

    public boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean ischecked) {
        isChecked = ischecked;
    }
}
