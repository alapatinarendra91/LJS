package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecuiterObject {

    @SerializedName("RecruterId")
    @Expose
    private String RecruterId;
    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("Password")
    @Expose
    private String Password;
    @SerializedName("ConfirmPassword")
    @Expose
    private String ConfirmPassword;
    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;
    @SerializedName("StdCurCompany")
    @Expose
    private String StdCurCompany;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("CompanyUrl")
    @Expose
    private String CompanyUrl;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("ContactNo_Landline")
    @Expose
    private String ContactNoLandline;
    @SerializedName("EmployType")
    @Expose
    private String EmployType;
    @SerializedName("IndustryType")
    @Expose
    private String IndustryType;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("CompanyProfile")
    @Expose
    private String CompanyProfile;
    @SerializedName("KeySkills")
    @Expose
    private String KeySkills;
    @SerializedName("Activation")
    @Expose
    private String Activation;
    @SerializedName("RegDate")
    @Expose
    private String RegDate;
    @SerializedName("EmailVerified")
    @Expose
    private Boolean EmailVerified;
    @SerializedName("IsOnline")
    @Expose
    private Boolean IsOnline;
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("CompanyLogo")
    @Expose
    private byte[] CompanyLogo;
    @SerializedName("Visibility")
    @Expose
    private Boolean Visibility;
    @SerializedName("UpdateDate")
    @Expose
    private String UpdateDate;
    @SerializedName("DeviceID")
    @Expose
    private String DeviceID;
    @SerializedName("LogoString")
    @Expose
    private String LogoString;
    @SerializedName("IsMobileOnline")
    @Expose
    private Boolean IsMobileOnline;
    @SerializedName("LastLogin")
    @Expose
    private String LastLogin;
    @SerializedName("LastActive")
    @Expose
    private String LastActive;

    /**
     *
     * @return
     * The RecruterId
     */
    public String getRecruterId() {
        return RecruterId;
    }

    /**
     *
     * @param RecruterId
     * The RecruterId
     */
    public void setRecruterId(String RecruterId) {
        this.RecruterId = RecruterId;
    }

    /**
     *
     * @return
     * The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     *
     * @param FirstName
     * The FirstName
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     *
     * @return
     * The LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     *
     * @param LastName
     * The LastName
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     *
     * @return
     * The Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     *
     * @param Password
     * The Password
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     *
     * @return
     * The ConfirmPassword
     */
    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    /**
     *
     * @param ConfirmPassword
     * The ConfirmPassword
     */
    public void setConfirmPassword(String ConfirmPassword) {
        this.ConfirmPassword = ConfirmPassword;
    }

    /**
     *
     * @return
     * The CompanyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     *
     * @param CompanyName
     * The CompanyName
     */
    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    /**
     *
     * @return
     * The StdCurCompany
     */
    public String getStdCurCompany() {
        return StdCurCompany;
    }

    /**
     *
     * @param StdCurCompany
     * The StdCurCompany
     */
    public void setStdCurCompany(String StdCurCompany) {
        this.StdCurCompany = StdCurCompany;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The CompanyUrl
     */
    public String getCompanyUrl() {
        return CompanyUrl;
    }

    /**
     *
     * @param CompanyUrl
     * The CompanyUrl
     */
    public void setCompanyUrl(String CompanyUrl) {
        this.CompanyUrl = CompanyUrl;
    }

    /**
     *
     * @return
     * The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     *
     * @param ContactNo
     * The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     *
     * @return
     * The ContactNoLandline
     */
    public String getContactNoLandline() {
        return ContactNoLandline;
    }

    /**
     *
     * @param ContactNoLandline
     * The ContactNo_Landline
     */
    public void setContactNoLandline(String ContactNoLandline) {
        this.ContactNoLandline = ContactNoLandline;
    }

    /**
     *
     * @return
     * The EmployType
     */
    public String getEmployType() {
        return EmployType;
    }

    /**
     *
     * @param EmployType
     * The EmployType
     */
    public void setEmployType(String EmployType) {
        this.EmployType = EmployType;
    }

    /**
     *
     * @return
     * The IndustryType
     */
    public String getIndustryType() {
        return IndustryType;
    }

    /**
     *
     * @param IndustryType
     * The IndustryType
     */
    public void setIndustryType(String IndustryType) {
        this.IndustryType = IndustryType;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     * The Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     * The CompanyProfile
     */
    public String getCompanyProfile() {
        return CompanyProfile;
    }

    /**
     *
     * @param CompanyProfile
     * The CompanyProfile
     */
    public void setCompanyProfile(String CompanyProfile) {
        this.CompanyProfile = CompanyProfile;
    }

    /**
     *
     * @return
     * The KeySkills
     */
    public String getKeySkills() {
        return KeySkills;
    }

    /**
     *
     * @param KeySkills
     * The KeySkills
     */
    public void setKeySkills(String KeySkills) {
        this.KeySkills = KeySkills;
    }

    /**
     *
     * @return
     * The Activation
     */
    public String getActivation() {
        return Activation;
    }

    /**
     *
     * @param Activation
     * The Activation
     */
    public void setActivation(String Activation) {
        this.Activation = Activation;
    }

    /**
     *
     * @return
     * The RegDate
     */
    public String getRegDate() {
        return RegDate;
    }

    /**
     *
     * @param RegDate
     * The RegDate
     */
    public void setRegDate(String RegDate) {
        this.RegDate = RegDate;
    }

    /**
     *
     * @return
     * The EmailVerified
     */
    public Boolean getEmailVerified() {
        return EmailVerified;
    }

    /**
     *
     * @param EmailVerified
     * The EmailVerified
     */
    public void setEmailVerified(Boolean EmailVerified) {
        this.EmailVerified = EmailVerified;
    }

    /**
     *
     * @return
     * The IsOnline
     */
    public Boolean getISOnline() {
        return IsOnline;
    }

    /**
     *
     * @param IsOnline
     * The IsOnline
     */
    public void setISOnline(Boolean IsOnline) {
        this.IsOnline = IsOnline;
    }

    /**
     *
     * @return
     * The Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     *
     * @param Designation
     * The Designation
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     *
     * @return
     * The CompanyLogo
     */
    public byte[] getCompanyLogo() {
        return CompanyLogo;
    }

    /**
     *
     * @param CompanyLogo
     * The CompanyLogo
     */
    public void setCompanyLogo(byte[] CompanyLogo) {
        this.CompanyLogo = CompanyLogo;
    }

    /**
     *
     * @return
     * The Visibility
     */
    public Boolean getVisibility() {
        return Visibility;
    }

    /**
     *
     * @param Visibility
     * The Visibility
     */
    public void setVisibility(Boolean Visibility) {
        this.Visibility = Visibility;
    }

    /**
     *
     * @return
     * The UpdateDate
     */
    public String getUpdateDate() {
        return UpdateDate;
    }

    /**
     *
     * @param UpdateDate
     * The UpdateDate
     */
    public void setUpdateDate(String UpdateDate) {
        this.UpdateDate = UpdateDate;
    }

    /**
     *
     * @return
     * The DeviceID
     */
    public String getDeviceID() {
        return DeviceID;
    }

    /**
     *
     * @param DeviceID
     * The DeviceID
     */
    public void setDeviceID(String DeviceID) {
        this.DeviceID = DeviceID;
    }

    /**
     *
     * @return
     * The LogoString
     */
    public String getLogoString() {
        return LogoString;
    }

    /**
     *
     * @param LogoString
     * The LogoString
     */
    public void setLogoString(String LogoString) {
        this.LogoString = LogoString;
    }

    /**
     *
     * @return
     * The IsMobileOnline
     */
    public Boolean getIsMobileOnline() {
        return IsMobileOnline;
    }

    /**
     *
     * @param IsMobileOnline
     * The IsMobileOnline
     */
    public void setIsMobileOnline(Boolean IsMobileOnline) {
        this.IsMobileOnline = IsMobileOnline;
    }

    /**
     *
     * @return
     * The LastLogin
     */
    public String getLastLogin() {
        return LastLogin;
    }

    /**
     *
     * @param LastLogin
     * The LastLogin
     */
    public void setLastLogin(String LastLogin) {
        this.LastLogin = LastLogin;
    }

    /**
     *
     * @return
     * The LastActive
     */
    public String getLastActive() {
        return LastActive;
    }

    /**
     *
     * @param LastActive
     * The LastActive
     */
    public void setLastActive(String LastActive) {
        this.LastActive = LastActive;
    }

}