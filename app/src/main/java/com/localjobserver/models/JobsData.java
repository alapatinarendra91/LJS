package com.localjobserver.models;

import android.os.Parcel;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

public class JobsData implements Serializable
{
    @JsonProperty("SearchQuery")
    private String SearchQuery;
    @JsonProperty("AppliedCount")
    private int AppliedCount;
    @JsonProperty("Qualification")
    private String Qualification;
    @JsonProperty("Phone")
    private String Phone;
    @JsonProperty("LocId")
    private int LocId;
    @JsonProperty("ViewsCount")
    private int ViewsCount;
    @JsonProperty("jobPositions")
    private String jobPositions;
    @JsonProperty("ExpMax")
    private int ExpMax;
    @JsonProperty("JobNo")
    private int JobNo;
    @JsonProperty("JobDescription")
    private String JobDescription;
    @JsonProperty("StdCurCompany")
    private String StdCurCompany;
    @JsonProperty("EndClient")
    private String EndClient;
    @JsonProperty("PostDate")
    private String PostDate;
    @JsonProperty("JobSummary")
    private String JobSummary;
    @JsonProperty("Resume")
    private String Resume;
    @JsonProperty("MatchedScore")
    private String MatchedScore;
    @JsonProperty("Salary")
    private String Salary;
    @JsonProperty("IsRecruiterOnline")
    private String IsRecruiterOnline;
    @JsonProperty("CompanyProfile")
    private String CompanyProfile;
    @JsonProperty("StdKeySkills")
    private String StdKeySkills;
    @JsonProperty("NoticePeriod")
    private String NoticePeriod;
    @JsonProperty("JTId")
    private int JTId;
    @JsonProperty("ContactPerson")
    private String ContactPerson;
    @JsonProperty("PostedType")
    private String PostedType;
    @JsonProperty("JobReference")
    private String JobReference;
    @JsonProperty("Location")
    private String Location;
    @JsonProperty("Locality")
    private String Locality;
    @JsonProperty("ExpMin")
    private String ExpMin;
    @JsonProperty("MinSal")
    private String MinSal;
    @JsonProperty("CompanyName")
    private String CompanyName;
    @JsonProperty("FId")
    private int FId;
    @JsonProperty("FunctionalArea")
    private String FunctionalArea;
    @JsonProperty("PbId")
    private int PbId;
    @JsonProperty("ContactNo_Landline")
    private String ContactNo_Landline;
    @JsonProperty("KeySkills")
    private String KeySkills;
    @JsonProperty("Email")
    private String Email;
    @JsonProperty("CompanyEmailId")
    private String CompanyEmailId;
    @JsonProperty("RecruiterCompanyName")
    private String RecruiterCompanyName;
    @JsonProperty("RId")
    private int RId;
    @JsonProperty("InId")
    private int InId;
    @JsonProperty("Address")
    private String Address;
    @JsonProperty("Role")
    private String Role;
    @JsonProperty("RolesResponsibilties")
    private String RolesResponsibilties;
    @JsonProperty("MaxSal")
    private String MaxSal;
    @JsonProperty("JobTitle")
    private String JobTitle;
    @JsonProperty("WalkinDate")
    private String WalkinDate;
    @JsonProperty("Designation")
    private String Designation;
    @JsonProperty("Industry")
    private String Industry;
    @JsonProperty("MatchedProfilesCount")
    private String MatchedProfilesCount;
    @JsonProperty("SimilarJobsCount")
    private String SimilarJobsCount;

    @JsonProperty("_id")
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSearchQuery ()
    {
        return SearchQuery;
    }

    public void setSearchQuery (String SearchQuery)
    {
        this.SearchQuery = SearchQuery;
    }

    public int getAppliedCount ()
    {
        return AppliedCount;
    }

    public void setAppliedCount (int AppliedCount)
    {
        this.AppliedCount = AppliedCount;
    }

    public String getQualification ()
    {
        return Qualification;
    }

    public void setQualification (String Qualification)
    {
        this.Qualification = Qualification;
    }

    public String getPhone ()
    {
        return Phone;
    }

    public void setPhone (String Phone)
    {
        this.Phone = Phone;
    }

    public int getLocId ()
    {
        return LocId;
    }

    public void setLocId (int LocId)
    {
        this.LocId = LocId;
    }

    public int getViewsCount ()
    {
        return ViewsCount;
    }

    public void setViewsCount (int ViewsCount)
    {
        this.ViewsCount = ViewsCount;
    }

    public String getJobPositions ()
    {
        return jobPositions;
    }

    public void setJobPositions (String jobPositions)
    {
        this.jobPositions = jobPositions;
    }

    public int getExpMax ()
    {
        return ExpMax;
    }

    public void setExpMax (int ExpMax)
    {
        this.ExpMax = ExpMax;
    }

    public int getJobNo ()
    {
        return JobNo;
    }

    public void setJobNo (int JobNo)
    {
        this.JobNo = JobNo;
    }

    public String getJobDescription ()
    {
        return JobDescription;
    }

    public void setJobDescription (String JobDescription)
    {
        this.JobDescription = JobDescription;
    }

    public String getStdCurCompany ()
    {
        return StdCurCompany;
    }

    public void setStdCurCompany (String StdCurCompany)
    {
        this.StdCurCompany = StdCurCompany;
    }

    public String getEndClient ()
    {
        return EndClient;
    }

    public void setEndClient (String EndClient)
    {
        this.EndClient = EndClient;
    }

    public String getPostDate ()
    {
        return PostDate;
    }

    public void setPostDate (String PostDate)
    {
        this.PostDate = PostDate;
    }

    public String getJobSummary ()
    {
        return JobSummary;
    }

    public void setJobSummary (String JobSummary)
    {
        this.JobSummary = JobSummary;
    }

    public String getResume ()
    {
        return Resume;
    }

    public void setResume (String Resume)
    {
        this.Resume = Resume;
    }

    public String getMatchedScore ()
    {
        return MatchedScore;
    }

    public void setMatchedScore (String MatchedScore)
    {
        this.MatchedScore = MatchedScore;
    }

    public String getSalary ()
    {
        return Salary;
    }

    public void setSalary (String Salary)
    {
        this.Salary = Salary;
    }

    public String getIsRecruiterOnline ()
    {
        return IsRecruiterOnline;
    }

    public void setIsRecruiterOnline (String IsRecruiterOnline)
    {
        this.IsRecruiterOnline = IsRecruiterOnline;
    }

    public String getCompanyProfile ()
    {
        return CompanyProfile;
    }

    public void setCompanyProfile (String CompanyProfile)
    {
        this.CompanyProfile = CompanyProfile;
    }

    public String getStdKeySkills ()
    {
        return StdKeySkills;
    }

    public void setStdKeySkills (String StdKeySkills)
    {
        this.StdKeySkills = StdKeySkills;
    }

    public String getNoticePeriod ()
    {
        return NoticePeriod;
    }

    public void setNoticePeriod (String NoticePeriod)
    {
        this.NoticePeriod = NoticePeriod;
    }

    public int getJTId ()
    {
        return JTId;
    }

    public void setJTId (int JTId)
    {
        this.JTId = JTId;
    }

    public String getContactPerson ()
    {
        return ContactPerson;
    }

    public void setContactPerson (String ContactPerson)
    {
        this.ContactPerson = ContactPerson;
    }

    public String getPostedType ()
    {
        return PostedType;
    }

    public void setPostedType (String PostedType)
    {
        this.PostedType = PostedType;
    }

    public String getJobReference ()
    {
        return JobReference;
    }

    public void setJobReference (String JobReference)
    {
        this.JobReference = JobReference;
    }

    public String getLocation ()
    {
        return Location;
    }

    public void setLocation (String Location)
    {
        this.Location = Location;
    }

    public String getLocality ()
    {
        return Locality;
    }

    public void setLocality (String Locality)
    {
        this.Locality = Locality;
    }

    public String getExpMin ()
    {
        return ExpMin;
    }

    public void setExpMin (String ExpMin)
    {
        this.ExpMin = ExpMin;
    }

    public String getMinSal ()
    {
        return MinSal;
    }

    public void setMinSal (String MinSal)
    {
        this.MinSal = MinSal;
    }

    public String getCompanyName ()
    {
        return CompanyName;
    }

    public void setCompanyName (String CompanyName)
    {
        this.CompanyName = CompanyName;
    }

    public int getFId ()
    {
        return FId;
    }

    public void setFId (int FId)
    {
        this.FId = FId;
    }

    public String getFunctionalArea ()
    {
        return FunctionalArea;
    }

    public void setFunctionalArea (String FunctionalArea)
    {
        this.FunctionalArea = FunctionalArea;
    }

    public int getPbId ()
    {
        return PbId;
    }

    public void setPbId (int PbId)
    {
        this.PbId = PbId;
    }

    public String getContactNo_Landline ()
    {
        return ContactNo_Landline;
    }

    public void setContactNo_Landline (String ContactNo_Landline)
    {
        this.ContactNo_Landline = ContactNo_Landline;
    }

    public String getKeySkills ()
    {
        return KeySkills;
    }

    public void setKeySkills (String KeySkills)
    {
        this.KeySkills = KeySkills;
    }

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getCompanyEmailId ()
    {
        return CompanyEmailId;
    }

    public void setCompanyEmailId (String CompanyEmailId)
    {
        this.CompanyEmailId = CompanyEmailId;
    }

 public String getRecruiterCompanyName ()
    {
        return RecruiterCompanyName;
    }

    public void setRecruiterCompanyName (String RecruiterCompanyName)
    {
        this.RecruiterCompanyName = RecruiterCompanyName;
    }

    public int getRId ()
    {
        return RId;
    }

    public void setRId (int RId)
    {
        this.RId = RId;
    }

    public int getInId ()
    {
        return InId;
    }

    public void setInId (int InId)
    {
        this.InId = InId;
    }

    public String getAddress ()
    {
        return Address;
    }

    public void setAddress (String Address)
    {
        this.Address = Address;
    }

    public String getRole ()
    {
        return Role;
    }

    public void setRole (String Role)
    {
        this.Role = Role;
    }

    public String getRolesResponsibilties ()
    {
        return RolesResponsibilties;
    }

    public void setRolesResponsibilties (String RolesResponsibilties)
    {
        this.RolesResponsibilties = Role;
    }

    public String getMaxSal ()
    {
        return MaxSal;
    }

    public void setMaxSal (String MaxSal)
    {
        this.MaxSal = MaxSal;
    }

    public String getJobTitle ()
    {
        return JobTitle;
    }

    public void setJobTitle (String JobTitle)
    {
        this.JobTitle = JobTitle;
    }

    public String getWalkinDate ()
    {
        return WalkinDate;
    }

    public void setWalkinDate (String WalkinDate)
    {
        this.WalkinDate = WalkinDate;
    }

    public String getDesignation ()
    {
        return Designation;
    }

    public void setDesignation (String Designation)
    {
        this.Designation = Designation;
    }

    public String getIndustry ()
    {
        return Industry;
    }

    public void setIndustry (String Industry)
    {
        this.Industry = Industry;
    }

    public String getMatchedProfilesCount ()
    {
        return MatchedProfilesCount;
    }

    public void setMatchedProfilesCount (String MatchedProfilesCount)
    {
        this.MatchedProfilesCount = MatchedProfilesCount;
    }
    ////////////////////////////
    public String getSimilarJobsCount ()
    {
        return SimilarJobsCount;
    }

    public void setSimilarJobsCount (String SimilarJobsCount)
    {
        this.SimilarJobsCount = SimilarJobsCount;
    }

    
    public  JobsData(){

    }

    public JobsData(Parcel in){
    	JobNo = in.readInt();
    	JobReference = in.readString();
        _id = in.readString();
    	JobTitle = in.readString();
    	JobSummary = in.readString();
    	JobDescription = in.readString();
    	KeySkills = in.readString();
    	StdKeySkills = in.readString();
    	MinSal = in.readString();
    	MaxSal = in.readString();
    	CompanyProfile = in.readString();
    	FunctionalArea = in.readString();
    	Industry = in.readString();
    	ExpMin = in.readString();
    	ExpMax =in.readInt();
    	Location  = in.readString();
    	Qualification = in.readString();
    	Email = in.readString();
    	Phone = in.readString();
    	ContactNo_Landline = in.readString();
    	PostDate = in.readString();
    	Role = in.readString();
        RolesResponsibilties = in.readString();
    	Address = in.readString();
    	CompanyName = in.readString();
    	StdCurCompany = in.readString();
    	ContactPerson = in.readString();
    	Salary = in.readString();
    	LocId = in.readInt();
    	InId = in.readInt();
    	FId = in.readInt();
    	RId = in.readInt();
    	JTId = in.readInt();
    	PbId = in.readInt();
    	PostedType = in.readString();
    	Designation = in.readString();
    	ViewsCount = in.readInt();
    	AppliedCount = in.readInt();
    	EndClient = in.readString();
    	MatchedScore = in.readString();
    	NoticePeriod = in.readString();
    	WalkinDate = in.readString();
    	SearchQuery = in.readString();
    	Resume = in.readString();
    	jobPositions = in.readString();
    }
    
}
