package com.localjobserver.models;

/**
 * Created by admin on 09-03-2015.
 */
public class SetupData {

    private String EmployType;
    private String fName;
    private String lName;
    private String contactNum;
    private String landNum;
    private String password;
    private String DateOfBirth;
    private String languagesKnown;
    private String confirmPass;
    private String currentIndestry;
    private String funArea;
    private String role;
    private String jobType;
    private String currentDisgnation;
    private String totExp;
    private String currentCtc;
    private String expectedCtc;
    private String keySkills;
    private String resumeHeadLine;
    private String currentCompany;
    private String currentLocation;
    private String Locality;
    private String preferedLocation;
    private String noticePeriod;
    private String eduCourse;
    private String institution;
    private String resumePath;
    private byte[] seekerImageData;
    private String yearPassing;
    private String email;
    private String alerts;

    private String seekerId;
    private String stdKeySkills;
    private String updateResume;
    private String gender;
    private String profileUpdate;

    private String updateOn;
    private String textResume;
    private String userName;
    private String status;
    private String photo;

    public  String emailVerified ;
    public  String visibleSettings;
    public  String jsId;
    public  String isOnline;
    public  String viewedCount;
    public  String downloadedCount;
    public  String candidatesActiveinLast;
    public  String resumeperPage;
    public  String searchQuery;
    public  String matchedScore;
    public  String inId;
    public  String fId;

    public  String rId;
    public  String jTId;
    public  String jobtype;
    public  String createdBy;

    private String stdCurCompany;


    public  String previousDesignation;
    public String  previousCompany;
    public String  stdPreCompany;
    public String jType;

    public String  directRegistration;
    public String  profileAccessSpecifier;
    public String sharedCompaniesList;

    public String  JbType;
    public String CurrentLocationInt;
    public String Experience;

    public String getExperience() {
        return Experience;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public String getJbType() {
        return JbType;
    }

    public void setJbType(String jbType) {
        JbType = jbType;
    }

    public String getCurrentLocationInt() {
        return CurrentLocationInt;
    }

    public void setCurrentLocationInt(String currentLocationInt) {
        CurrentLocationInt = currentLocationInt;
    }

    public String getDirectRegistration() {
        return directRegistration;
    }

    public void setDirectRegistration(String directRegistration) {
        this.directRegistration = directRegistration;
    }

    public String getProfileAccessSpecifier() {
        return profileAccessSpecifier;
    }

    public void setProfileAccessSpecifier(String profileAccessSpecifier) {
        this.profileAccessSpecifier = profileAccessSpecifier;
    }

    public String getSharedCompaniesList() {
        return sharedCompaniesList;
    }

    public void setSharedCompaniesList(String sharedCompaniesList) {
        this.sharedCompaniesList = sharedCompaniesList;
    }

    public String getjType() {
        return jType;
    }

    public void setjType(String jType) {
        this.jType = jType;
    }

    public String getSeekerId() {
        return seekerId;
    }

    public void setSeekerId(String seekerId) {
        this.seekerId = seekerId;
    }

    public String getStdKeySkills() {
        return stdKeySkills;
    }

    public void setStdKeySkills(String stdKeySkills) {
        this.stdKeySkills = stdKeySkills;
    }

    public String getUpdateResume() {
        return updateResume;
    }

    public void setUpdateResume(String updateResume) {
        this.updateResume = updateResume;
    }

    public String getProfileUpdate() {
        return profileUpdate;
    }

    public void setProfileUpdate(String profileUpdate) {
        this.profileUpdate = profileUpdate;
    }

    public String getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(String updateOn) {
        this.updateOn = updateOn;
    }

    public String getTextResume() {
        return textResume;
    }

    public void setTextResume(String textResume) {
        this.textResume = textResume;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStdCurCompany() {
        return stdCurCompany;
    }

    public void setStdCurCompany(String stdCurCompany) {
        this.stdCurCompany = stdCurCompany;
    }

    public String getPreviousDesignation() {
        return previousDesignation;
    }

    public void setPreviousDesignation(String previousDesignation) {
        this.previousDesignation = previousDesignation;
    }

    public String getPreviousCompany() {
        return previousCompany;
    }

    public void setPreviousCompany(String previousCompany) {
        this.previousCompany = previousCompany;
    }

    public String getStdPreCompany() {
        return stdPreCompany;
    }

    public void setStdPreCompany(String stdPreCompany) {
        this.stdPreCompany = stdPreCompany;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getVisibleSettings() {
        return visibleSettings;
    }

    public void setVisibleSettings(String visibleSettings) {
        this.visibleSettings = visibleSettings;
    }

    public String getJsId() {
        return jsId;
    }

    public void setJsId(String jsId) {
        this.jsId = jsId;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getViewedCount() {
        return viewedCount;
    }

    public void setViewedCount(String viewedCount) {
        this.viewedCount = viewedCount;
    }

    public String getDownloadedCount() {
        return downloadedCount;
    }

    public void setDownloadedCount(String downloadedCount) {
        this.downloadedCount = downloadedCount;
    }

    public String getCandidatesActiveinLast() {
        return candidatesActiveinLast;
    }

    public void setCandidatesActiveinLast(String candidatesActiveinLast) {
        this.candidatesActiveinLast = candidatesActiveinLast;
    }

    public String getResumeperPage() {
        return resumeperPage;
    }

    public void setResumeperPage(String resumeperPage) {
        this.resumeperPage = resumeperPage;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getMatchedScore() {
        return matchedScore;
    }

    public void setMatchedScore(String matchedScore) {
        this.matchedScore = matchedScore;
    }

    public String getInId() {
        return inId;
    }

    public void setInId(String inId) {
        this.inId = inId;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }

    public String getjTId() {
        return jTId;
    }

    public void setjTId(String jTId) {
        this.jTId = jTId;
    }

    public String getJobtype() {
        return jobtype;
    }

    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getResumePath() {
        return resumePath;
    }

    public void setResumePath(String resumePath) {
        this.resumePath = resumePath;
    }

    public byte[] getSeekerImageData() {
        return seekerImageData;
    }

    public void setSeekerImageData(byte[] seekerImageData) {
        this.seekerImageData = seekerImageData;
    }

    public String getCurrentIndestry() {
        return currentIndestry;
    }

    public void setCurrentIndestry(String currentIndestry) {
        this.currentIndestry = currentIndestry;
    }

    public String getFunArea() {
        return funArea;
    }

    public void setFunArea(String funArea) {
        this.funArea = funArea;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCurrentDisgnation() {
        return currentDisgnation;
    }

    public void setCurrentDisgnation(String currentDisgnation) {
        this.currentDisgnation = currentDisgnation;
    }

    public String getTotExp() {
        return totExp;
    }

    public void setTotExp(String totExp) {
        this.totExp = totExp;
    }

    public String getCurrentCtc() {
        return currentCtc;
    }

    public void setCurrentCtc(String currentCtc) {
        this.currentCtc = currentCtc;
    }

    public String getExpectedCtc() {
        return expectedCtc;
    }

    public void setExpectedCtc(String expectedCtc) {
        this.expectedCtc = expectedCtc;
    }

    public String getKeySkills() {
        return keySkills;
    }

    public void setKeySkills(String keySkills) {
        this.keySkills = keySkills;
    }

    public String getResumeHeadLine() {
        return resumeHeadLine;
    }

    public void setResumeHeadLine(String resumeHeadLine) {
        this.resumeHeadLine = resumeHeadLine;
    }

    public String getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        this.currentCompany = currentCompany;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getLocality() {
        return Locality;
    }

    public void setLocality(String Locality) {
        this.Locality = Locality;
    }

    public String getPreferedLocation() {
        return preferedLocation;
    }

    public void setPreferedLocation(String preferedLocation) {
        this.preferedLocation = preferedLocation;
    }

    public String getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getEduCourse() {
        return eduCourse;
    }

    public void setEduCourse(String eduCourse) {
        this.eduCourse = eduCourse;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getYearPassing() {
        return yearPassing;
    }

    public void setYearPassing(String yearPassing) {
        this.yearPassing = yearPassing;
    }

    public String getAlerts() {
        return alerts;
    }

    public void setAlerts(String alerts) {
        this.alerts = alerts;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getEmployType() {
        return EmployType;
    }

    public void setEmployType(String EmployType) {
        this.EmployType = EmployType;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getContactNum() {
        return contactNum;
    }

    public void setContactNum(String contactNum) {
        this.contactNum = contactNum;
    }

    public String getLandNum() {
        return landNum;
    }

    public void setLandNum(String landNum) {
        this.landNum = landNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public String getlanguagesKnown() {
        return languagesKnown;
    }

    public void setlanguagesKnown(String languagesKnown) {
        this.languagesKnown = languagesKnown;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


//    @Override
//
//    public String toString() {
//        return new StringBuffer(" First Name : ").append(this.firstName)
//                .append(" Last Name : ").append(this.lastName).append(" Age : ").append(this.age).append(" ID : ").append(this.id).toString();
//    }

}
