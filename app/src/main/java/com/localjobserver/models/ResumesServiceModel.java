package com.localjobserver.models;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * Created by siva on 10/07/15.
 */
public class ResumesServiceModel implements Serializable {

    @JsonProperty("_id")
    private String _id;
    @JsonProperty("FirstName")
    private String FirstName;
    @JsonProperty("LastName")
    private String LastName;
    @JsonProperty("CurrentDesignation")
    private String CurrentDesignation;
    @JsonProperty("CurrentCompany")
    private String CurrentCompany;
    @JsonProperty("NoticePeriod")
    private String NoticePeriod;
    @JsonProperty("PreferredLocation")
    private String PreferredLocation;
    @JsonProperty("ContactNo")
    private String ContactNo;
    @JsonProperty("Email")
    private String Email;
    @JsonProperty("Education")
    private String Education;
    @JsonProperty("Experience")
    private String Experience;
    @JsonProperty("Location")
    private String Location;
    @JsonProperty("KeySkills")
    private String KeySkills;
    @JsonProperty("ResumeHeadLine")
    private String ResumeHeadLine;


    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCurrentDesignation() {
        return CurrentDesignation;
    }

    public void setCurrentDesignation(String currentDesignation) {
        CurrentDesignation = currentDesignation;
    }

    public String getCurrentCompany() {
        return CurrentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        CurrentCompany = currentCompany;
    }

    public String getNoticePeriod() {
        return NoticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        NoticePeriod = noticePeriod;
    }

    public String getPreferredLocation() {
        return PreferredLocation;
    }

    public void setPreferredLocation(String preferredLocation) {
        PreferredLocation = preferredLocation;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getExperience() {
        return Experience;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getKeySkills() {
        return KeySkills;
    }

    public void setKeySkills(String keySkills) {
        KeySkills = keySkills;
    }

    public String getResumeHeadLine() {
        return ResumeHeadLine;
    }

    public void setResumeHeadLine(String resumeHeadLine) {
        ResumeHeadLine = resumeHeadLine;
    }
}
