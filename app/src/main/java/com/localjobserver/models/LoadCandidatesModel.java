package com.localjobserver.models;

/**
 * Created by latitude on 04-08-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoadCandidatesModel {

    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("RepliedDate")
    @Expose
    private String RepliedDate;
    @SerializedName("Message")
    @Expose
    private String Message;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("Comments")
    @Expose
    private String Comments;
//    @SerializedName("Industry")
//    @Expose
//    private String industry;
//    @SerializedName("Experience")
//    @Expose
//    private Integer experience;
//    @SerializedName("CurrentCTC")
//    @Expose
//    private Integer currentCTC;
//    @SerializedName("expectedCTC")
//    @Expose
//    private Integer expectedCTC;
//    @SerializedName("Education")
//    @Expose
//    private String education;
//    @SerializedName("KeySkills")
//    @Expose
//    private String keySkills;
//    @SerializedName("StdKeySkills")
//    @Expose
//    private String stdKeySkills;
//    @SerializedName("Location")
//    @Expose
//    private String location;
//    @SerializedName("Locality")
//    @Expose
//    private String locality;
//    @SerializedName("UpdateResume")
//    @Expose
//    private String updateResume;
//    @SerializedName("Gender")
//    @Expose
//    private String gender;
//    @SerializedName("Institute")
//    @Expose
//    private String institute;
//    @SerializedName("YearOfPass")
//    @Expose
//    private String yearOfPass;
//    @SerializedName("Role")
//    @Expose
//    private String role;
//    @SerializedName("ResumeHeadLine")
//    @Expose
//    private String resumeHeadLine;
//    @SerializedName("UpdateOn")
//    @Expose
//    private String updateOn;
//    @SerializedName("Resume")
//    @Expose
//    private String resume;
//    @SerializedName("TextResume")
//    @Expose
//    private String textResume;
//    @SerializedName("UserName")
//    @Expose
//    private String userName;
//    @SerializedName("Status")
//    @Expose
//    private String status;
//    @SerializedName("Photo")
//    @Expose
//    private String photo;
//    @SerializedName("CurrentDesignation")
//    @Expose
//    private String currentDesignation;
//    @SerializedName("CurrentCompany")
//    @Expose
//    private String currentCompany;
//    @SerializedName("StdCurCompany")
//    @Expose
//    private String stdCurCompany;
//    @SerializedName("PreviousDesignation")
//    @Expose
//    private String previousDesignation;
//    @SerializedName("PreviousCompany")
//    @Expose
//    private String previousCompany;
//    @SerializedName("StdPreCompany")
//    @Expose
//    private String stdPreCompany;
//    @SerializedName("PreferredLocation")
//    @Expose
//    private String preferredLocation;
//    @SerializedName("NoticePeriod")
//    @Expose
//    private Integer noticePeriod;
//    @SerializedName("EmailVerified")
//    @Expose
//    private Boolean emailVerified;
//    @SerializedName("VisibleSettings")
//    @Expose
//    private Integer visibleSettings;
//    @SerializedName("JsId")
//    @Expose
//    private String jsId;
//    @SerializedName("IsOnline")
//    @Expose
//    private Boolean isOnline;
//    @SerializedName("ViewedCount")
//    @Expose
//    private Integer viewedCount;
//    @SerializedName("DownloadedCount")
//    @Expose
//    private Integer downloadedCount;
//    @SerializedName("CandidatesActiveinLast")
//    @Expose
//    private String candidatesActiveinLast;
//    @SerializedName("InId")
//    @Expose
//    private Integer inId;
//    @SerializedName("FId")
//    @Expose
//    private Integer fId;
//    @SerializedName("RId")
//    @Expose
//    private Integer rId;
//    @SerializedName("JTId")
//    @Expose
//    private Integer jTId;
//    @SerializedName("Jobtype")
//    @Expose
//    private String jobtype;
//    @SerializedName("CreatedBy")
//    @Expose
//    private String createdBy;
//    @SerializedName("similarCount")
//    @Expose
//    private Integer similarCount;
//    @SerializedName("MatchedPercent")
//    @Expose
//    private String matchedPercent;
//    @SerializedName("RelevantScore")
//    @Expose
//    private Integer relevantScore;
//    @SerializedName("DirectRegistration")
//    @Expose
//    private Boolean directRegistration;
//    @SerializedName("ProfileAccessSpecifier")
//    @Expose
//    private String profileAccessSpecifier;
//    @SerializedName("SharedCompaniesList")
//    @Expose
//    private String sharedCompaniesList;
//    @SerializedName("photoString")
//    @Expose
//    private String photoString;
//    @SerializedName("languagesKnown")
//    @Expose
//    private String languagesKnown;
//    @SerializedName("ResumeFile")
//    @Expose
//    private String resumeFile;
//    @SerializedName("DeviceID")
//    @Expose
//    private String deviceID;
//    @SerializedName("ProfileName")
//    @Expose
//    private String profileName;
//    @SerializedName("IsMobileOnline")
//    @Expose
//    private Boolean isMobileOnline;
//    @SerializedName("DateOfBirth")
//    @Expose
//    private String dateOfBirth;
//    @SerializedName("LastLogin")
//    @Expose
//    private String lastLogin;
//    @SerializedName("LastActive")
//    @Expose
//    private String lastActive;
//    @SerializedName("UploadedBy")
//    @Expose
//    private String uploadedBy;
//    @SerializedName("UploadedDate")
//    @Expose
//    private String uploadedDate;
//    @SerializedName("OnNoticePeriod")
//    @Expose
//    private String onNoticePeriod;
//    @SerializedName("LastUpdatedNoticePeriod")
//    @Expose
//    private String lastUpdatedNoticePeriod;
//    @SerializedName("LastWorkingDay")
//    @Expose
//    private String lastWorkingDay;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRepliedDate() {
        return RepliedDate;
    }

    public void setRepliedDate(String RepliedDate) {
        this.RepliedDate = RepliedDate;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

//    public String getIndustry() {
//        return industry;
//    }
//
//    public void setIndustry(String industry) {
//        this.industry = industry;
//    }
//
//    public Integer getExperience() {
//        return experience;
//    }
//
//    public void setExperience(Integer experience) {
//        this.experience = experience;
//    }
//
//    public Integer getCurrentCTC() {
//        return currentCTC;
//    }
//
//    public void setCurrentCTC(Integer currentCTC) {
//        this.currentCTC = currentCTC;
//    }
//
//    public Integer getExpectedCTC() {
//        return expectedCTC;
//    }
//
//    public void setExpectedCTC(Integer expectedCTC) {
//        this.expectedCTC = expectedCTC;
//    }
//
//    public String getEducation() {
//        return education;
//    }
//
//    public void setEducation(String education) {
//        this.education = education;
//    }
//
//    public String getKeySkills() {
//        return keySkills;
//    }
//
//    public void setKeySkills(String keySkills) {
//        this.keySkills = keySkills;
//    }
//
//    public String getStdKeySkills() {
//        return stdKeySkills;
//    }
//
//    public void setStdKeySkills(String stdKeySkills) {
//        this.stdKeySkills = stdKeySkills;
//    }
//
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    public String getLocality() {
//        return locality;
//    }
//
//    public void setLocality(String locality) {
//        this.locality = locality;
//    }
//
//    public String getUpdateResume() {
//        return updateResume;
//    }
//
//    public void setUpdateResume(String updateResume) {
//        this.updateResume = updateResume;
//    }
//
//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public String getInstitute() {
//        return institute;
//    }
//
//    public void setInstitute(String institute) {
//        this.institute = institute;
//    }
//
//    public String getYearOfPass() {
//        return yearOfPass;
//    }
//
//    public void setYearOfPass(String yearOfPass) {
//        this.yearOfPass = yearOfPass;
//    }
//
//    public String getRole() {
//        return role;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }
//
//    public String getResumeHeadLine() {
//        return resumeHeadLine;
//    }
//
//    public void setResumeHeadLine(String resumeHeadLine) {
//        this.resumeHeadLine = resumeHeadLine;
//    }
//
//    public String getUpdateOn() {
//        return updateOn;
//    }
//
//    public void setUpdateOn(String updateOn) {
//        this.updateOn = updateOn;
//    }
//
//    public String getResume() {
//        return resume;
//    }
//
//    public void setResume(String resume) {
//        this.resume = resume;
//    }
//
//    public String getTextResume() {
//        return textResume;
//    }
//
//    public void setTextResume(String textResume) {
//        this.textResume = textResume;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public String getCurrentDesignation() {
//        return currentDesignation;
//    }
//
//    public void setCurrentDesignation(String currentDesignation) {
//        this.currentDesignation = currentDesignation;
//    }
//
//    public String getCurrentCompany() {
//        return currentCompany;
//    }
//
//    public void setCurrentCompany(String currentCompany) {
//        this.currentCompany = currentCompany;
//    }
//
//    public String getStdCurCompany() {
//        return stdCurCompany;
//    }
//
//    public void setStdCurCompany(String stdCurCompany) {
//        this.stdCurCompany = stdCurCompany;
//    }
//
//    public String getPreviousDesignation() {
//        return previousDesignation;
//    }
//
//    public void setPreviousDesignation(String previousDesignation) {
//        this.previousDesignation = previousDesignation;
//    }
//
//    public String getPreviousCompany() {
//        return previousCompany;
//    }
//
//    public void setPreviousCompany(String previousCompany) {
//        this.previousCompany = previousCompany;
//    }
//
//    public String getStdPreCompany() {
//        return stdPreCompany;
//    }
//
//    public void setStdPreCompany(String stdPreCompany) {
//        this.stdPreCompany = stdPreCompany;
//    }
//
//    public String getPreferredLocation() {
//        return preferredLocation;
//    }
//
//    public void setPreferredLocation(String preferredLocation) {
//        this.preferredLocation = preferredLocation;
//    }
//
//    public Integer getNoticePeriod() {
//        return noticePeriod;
//    }
//
//    public void setNoticePeriod(Integer noticePeriod) {
//        this.noticePeriod = noticePeriod;
//    }
//
//    public Boolean getEmailVerified() {
//        return emailVerified;
//    }
//
//    public void setEmailVerified(Boolean emailVerified) {
//        this.emailVerified = emailVerified;
//    }
//
//    public Integer getVisibleSettings() {
//        return visibleSettings;
//    }
//
//    public void setVisibleSettings(Integer visibleSettings) {
//        this.visibleSettings = visibleSettings;
//    }
//
//    public String getJsId() {
//        return jsId;
//    }
//
//    public void setJsId(String jsId) {
//        this.jsId = jsId;
//    }
//
//    public Boolean getIsOnline() {
//        return isOnline;
//    }
//
//    public void setIsOnline(Boolean isOnline) {
//        this.isOnline = isOnline;
//    }
//
//    public Integer getViewedCount() {
//        return viewedCount;
//    }
//
//    public void setViewedCount(Integer viewedCount) {
//        this.viewedCount = viewedCount;
//    }
//
//    public Integer getDownloadedCount() {
//        return downloadedCount;
//    }
//
//    public void setDownloadedCount(Integer downloadedCount) {
//        this.downloadedCount = downloadedCount;
//    }
//
//    public String getCandidatesActiveinLast() {
//        return candidatesActiveinLast;
//    }
//
//    public void setCandidatesActiveinLast(String candidatesActiveinLast) {
//        this.candidatesActiveinLast = candidatesActiveinLast;
//    }
//
//    public Integer getInId() {
//        return inId;
//    }
//
//    public void setInId(Integer inId) {
//        this.inId = inId;
//    }
//
//    public Integer getFId() {
//        return fId;
//    }
//
//    public void setFId(Integer fId) {
//        this.fId = fId;
//    }
//
//    public Integer getRId() {
//        return rId;
//    }
//
//    public void setRId(Integer rId) {
//        this.rId = rId;
//    }
//
//    public Integer getJTId() {
//        return jTId;
//    }
//
//    public void setJTId(Integer jTId) {
//        this.jTId = jTId;
//    }
//
//    public String getJobtype() {
//        return jobtype;
//    }
//
//    public void setJobtype(String jobtype) {
//        this.jobtype = jobtype;
//    }
//
//    public String getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(String createdBy) {
//        this.createdBy = createdBy;
//    }
//
//    public Integer getSimilarCount() {
//        return similarCount;
//    }
//
//    public void setSimilarCount(Integer similarCount) {
//        this.similarCount = similarCount;
//    }
//
//    public String getMatchedPercent() {
//        return matchedPercent;
//    }
//
//    public void setMatchedPercent(String matchedPercent) {
//        this.matchedPercent = matchedPercent;
//    }
//
//    public Integer getRelevantScore() {
//        return relevantScore;
//    }
//
//    public void setRelevantScore(Integer relevantScore) {
//        this.relevantScore = relevantScore;
//    }
//
//    public Boolean getDirectRegistration() {
//        return directRegistration;
//    }
//
//    public void setDirectRegistration(Boolean directRegistration) {
//        this.directRegistration = directRegistration;
//    }
//
//    public String getProfileAccessSpecifier() {
//        return profileAccessSpecifier;
//    }
//
//    public void setProfileAccessSpecifier(String profileAccessSpecifier) {
//        this.profileAccessSpecifier = profileAccessSpecifier;
//    }
//
//    public String getSharedCompaniesList() {
//        return sharedCompaniesList;
//    }
//
//    public void setSharedCompaniesList(String sharedCompaniesList) {
//        this.sharedCompaniesList = sharedCompaniesList;
//    }
//
//    public String getPhotoString() {
//        return photoString;
//    }
//
//    public void setPhotoString(String photoString) {
//        this.photoString = photoString;
//    }
//
//    public String getLanguagesKnown() {
//        return languagesKnown;
//    }
//
//    public void setLanguagesKnown(String languagesKnown) {
//        this.languagesKnown = languagesKnown;
//    }
//
//    public String getResumeFile() {
//        return resumeFile;
//    }
//
//    public void setResumeFile(String resumeFile) {
//        this.resumeFile = resumeFile;
//    }
//
//    public String getDeviceID() {
//        return deviceID;
//    }
//
//    public void setDeviceID(String deviceID) {
//        this.deviceID = deviceID;
//    }
//
//    public String getProfileName() {
//        return profileName;
//    }
//
//    public void setProfileName(String profileName) {
//        this.profileName = profileName;
//    }
//
//    public Boolean getIsMobileOnline() {
//        return isMobileOnline;
//    }
//
//    public void setIsMobileOnline(Boolean isMobileOnline) {
//        this.isMobileOnline = isMobileOnline;
//    }
//
//    public String getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(String dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
//
//    public String getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(String lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public String getLastActive() {
//        return lastActive;
//    }
//
//    public void setLastActive(String lastActive) {
//        this.lastActive = lastActive;
//    }
//
//    public String getUploadedBy() {
//        return uploadedBy;
//    }
//
//    public void setUploadedBy(String uploadedBy) {
//        this.uploadedBy = uploadedBy;
//    }
//
//    public String getUploadedDate() {
//        return uploadedDate;
//    }
//
//    public void setUploadedDate(String uploadedDate) {
//        this.uploadedDate = uploadedDate;
//    }
//
//    public String getOnNoticePeriod() {
//        return onNoticePeriod;
//    }
//
//    public void setOnNoticePeriod(String onNoticePeriod) {
//        this.onNoticePeriod = onNoticePeriod;
//    }
//
//    public String getLastUpdatedNoticePeriod() {
//        return lastUpdatedNoticePeriod;
//    }
//
//    public void setLastUpdatedNoticePeriod(String lastUpdatedNoticePeriod) {
//        this.lastUpdatedNoticePeriod = lastUpdatedNoticePeriod;
//    }
//
//    public String getLastWorkingDay() {
//        return lastWorkingDay;
//    }
//
//    public void setLastWorkingDay(String lastWorkingDay) {
//        this.lastWorkingDay = lastWorkingDay;
//    }

}
