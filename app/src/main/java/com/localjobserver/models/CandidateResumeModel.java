package com.localjobserver.models;

/**
 * Created by ANDROID on 03-Oct-16.
 */

import com.google.gson.annotations.Expose;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

public class CandidateResumeModel implements Serializable {

    @JsonProperty("_id")
    private String id;
    @JsonProperty("CompanyEmailId")
    private String companyEmailId;
    @JsonProperty("JobNo")
    private Integer jobNo;
    @JsonProperty("CompanyLogo")
    @Expose
    private Object companyLogo;
    @JsonProperty("JobReference")
    @Expose
    private String jobReference;
    @JsonProperty("JobTitle")
    @Expose
    private String jobTitle;
    @JsonProperty("JobSummary")
    @Expose
    private String jobSummary;
    @JsonProperty("JobDescription")
    @Expose
    private String jobDescription;
    @JsonProperty("KeySkills")
    @Expose
    private String keySkills;
    @JsonProperty("StdKeySkills")
    @Expose
    private String stdKeySkills;
    @JsonProperty("MinSal")
    @Expose
    private Integer minSal;
    @JsonProperty("MaxSal")
    @Expose
    private Integer maxSal;
    @JsonProperty("CompanyProfile")
    @Expose
    private String companyProfile;
    @JsonProperty("FunctionalArea")
    @Expose
    private String functionalArea;
    @JsonProperty("Industry")
    @Expose
    private String industry;
    @JsonProperty("ExpMin")
    @Expose
    private Integer expMin;
    @JsonProperty("ExpMax")
    @Expose
    private Integer expMax;
    @JsonProperty("Location")
    @Expose
    private String location;
    @JsonProperty("Qualification")
    @Expose
    private String qualification;
    @JsonProperty("Email")
    @Expose
    private String email;
    @JsonProperty("Phone")
    @Expose
    private String phone;
    @JsonProperty("ContactNo_Landline")
    @Expose
    private String contactNoLandline;
    @JsonProperty("PostDate")
    @Expose
    private String postDate;
    @JsonProperty("Role")
    @Expose
    private String role;
    @JsonProperty("Address")
    @Expose
    private String address;
    @JsonProperty("CompanyName")
    @Expose
    private String companyName;
    @JsonProperty("StdCurCompany")
    @Expose
    private String stdCurCompany;
    @JsonProperty("ContactPerson")
    @Expose
    private String contactPerson;
    @JsonProperty("Salary")
    @Expose
    private String salary;
    @JsonProperty("LocId")
    @Expose
    private Integer locId;
    @JsonProperty("InId")
    @Expose
    private Integer inId;
    @JsonProperty("FId")
    @Expose
    private Integer fId;
    @JsonProperty("RId")
    @Expose
    private Integer rId;
    @JsonProperty("JTId")
    @Expose
    private Integer jTId;
    @JsonProperty("PbId")
    @Expose
    private Integer pbId;
    @JsonProperty("PostedType")
    @Expose
    private String postedType;
    @JsonProperty("Designation")
    @Expose
    private String designation;
    @JsonProperty("ViewsCount")
    @Expose
    private Integer viewsCount;
    @JsonProperty("AppliedCount")
    @Expose
    private Integer appliedCount;
    @JsonProperty("EndClient")
    @Expose
    private String endClient;
    @JsonProperty("MatchedScore")
    @Expose
    private String matchedScore;
    @JsonProperty("NoticePeriod")
    @Expose
    private Integer noticePeriod;
    @JsonProperty("WalkinDate")
    @Expose
    private String walkinDate;
    @JsonProperty("SearchQuery")
    @Expose
    private Object searchQuery;
    @JsonProperty("Resume")
    @Expose
    private Object resume;
    @JsonProperty("jobPositions")
    @Expose
    private Integer jobPositions;
    @JsonProperty("LogoString")
    @Expose
    private Object logoString;
    @JsonProperty("RecruiterCompanyName")
    @Expose
    private String recruiterCompanyName;
    @JsonProperty("MatchedProfilesQuery")
    @Expose
    private String matchedProfilesQuery;
    @JsonProperty("SimilarJobsQuery")
    @Expose
    private String similarJobsQuery;
    @JsonProperty("SimilarJobsCount")
    @Expose
    private Integer similarJobsCount;
    @JsonProperty("IsRecruiterOnline")
    @Expose
    private Boolean isRecruiterOnline;
    @JsonProperty("MatchedProfilesCount")
    @Expose
    private Integer matchedProfilesCount;
    @JsonProperty("Documents")
    @Expose
    private String documents;
    @JsonProperty("RolesResponsibilties")
    @Expose
    private String rolesResponsibilties;
    @JsonProperty("questionier")
    @Expose
    private Object questionier;
    @JsonProperty("Locality")
    @Expose
    private String locality;
    @JsonProperty("RatedSkills")
    @Expose
    private Object ratedSkills;
    @JsonProperty("postedBy")
    @Expose
    private String postedBy;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The _id
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     *
     * @return
     * The companyEmailId
     */
    public String getCompanyEmailId() {
        return companyEmailId;
    }

    /**
     *
     * @param companyEmailId
     * The CompanyEmailId
     */
    public void setCompanyEmailId(String companyEmailId) {
        this.companyEmailId = companyEmailId;
    }

    /**
     *
     * @return
     * The jobNo
     */
    public Integer getJobNo() {
        return jobNo;
    }

    /**
     *
     * @param jobNo
     * The JobNo
     */
    public void setJobNo(Integer jobNo) {
        this.jobNo = jobNo;
    }

    /**
     *
     * @return
     * The companyLogo
     */
    public Object getCompanyLogo() {
        return companyLogo;
    }

    /**
     *
     * @param companyLogo
     * The CompanyLogo
     */
    public void setCompanyLogo(Object companyLogo) {
        this.companyLogo = companyLogo;
    }

    /**
     *
     * @return
     * The jobReference
     */
    public String getJobReference() {
        return jobReference;
    }

    /**
     *
     * @param jobReference
     * The JobReference
     */
    public void setJobReference(String jobReference) {
        this.jobReference = jobReference;
    }

    /**
     *
     * @return
     * The jobTitle
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     *
     * @param jobTitle
     * The JobTitle
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     *
     * @return
     * The jobSummary
     */
    public String getJobSummary() {
        return jobSummary;
    }

    /**
     *
     * @param jobSummary
     * The JobSummary
     */
    public void setJobSummary(String jobSummary) {
        this.jobSummary = jobSummary;
    }

    /**
     *
     * @return
     * The jobDescription
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     *
     * @param jobDescription
     * The JobDescription
     */
    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    /**
     *
     * @return
     * The keySkills
     */
    public String getKeySkills() {
        return keySkills;
    }

    /**
     *
     * @param keySkills
     * The KeySkills
     */
    public void setKeySkills(String keySkills) {
        this.keySkills = keySkills;
    }

    /**
     *
     * @return
     * The stdKeySkills
     */
    public String getStdKeySkills() {
        return stdKeySkills;
    }

    /**
     *
     * @param stdKeySkills
     * The StdKeySkills
     */
    public void setStdKeySkills(String stdKeySkills) {
        this.stdKeySkills = stdKeySkills;
    }

    /**
     *
     * @return
     * The minSal
     */
    public Integer getMinSal() {
        return minSal;
    }

    /**
     *
     * @param minSal
     * The MinSal
     */
    public void setMinSal(Integer minSal) {
        this.minSal = minSal;
    }

    /**
     *
     * @return
     * The maxSal
     */
    public Integer getMaxSal() {
        return maxSal;
    }

    /**
     *
     * @param maxSal
     * The MaxSal
     */
    public void setMaxSal(Integer maxSal) {
        this.maxSal = maxSal;
    }

    /**
     *
     * @return
     * The companyProfile
     */
    public String getCompanyProfile() {
        return companyProfile;
    }

    /**
     *
     * @param companyProfile
     * The CompanyProfile
     */
    public void setCompanyProfile(String companyProfile) {
        this.companyProfile = companyProfile;
    }

    /**
     *
     * @return
     * The functionalArea
     */
    public String getFunctionalArea() {
        return functionalArea;
    }

    /**
     *
     * @param functionalArea
     * The FunctionalArea
     */
    public void setFunctionalArea(String functionalArea) {
        this.functionalArea = functionalArea;
    }

    /**
     *
     * @return
     * The industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     *
     * @param industry
     * The Industry
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     *
     * @return
     * The expMin
     */
    public Integer getExpMin() {
        return expMin;
    }

    /**
     *
     * @param expMin
     * The ExpMin
     */
    public void setExpMin(Integer expMin) {
        this.expMin = expMin;
    }

    /**
     *
     * @return
     * The expMax
     */
    public Integer getExpMax() {
        return expMax;
    }

    /**
     *
     * @param expMax
     * The ExpMax
     */
    public void setExpMax(Integer expMax) {
        this.expMax = expMax;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The Location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The qualification
     */
    public String getQualification() {
        return qualification;
    }

    /**
     *
     * @param qualification
     * The Qualification
     */
    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The Phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The contactNoLandline
     */
    public String getContactNoLandline() {
        return contactNoLandline;
    }

    /**
     *
     * @param contactNoLandline
     * The ContactNo_Landline
     */
    public void setContactNoLandline(String contactNoLandline) {
        this.contactNoLandline = contactNoLandline;
    }

    /**
     *
     * @return
     * The postDate
     */
    public String getPostDate() {
        return postDate;
    }

    /**
     *
     * @param postDate
     * The PostDate
     */
    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The Role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The Address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     *
     * @param companyName
     * The CompanyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     *
     * @return
     * The stdCurCompany
     */
    public String getStdCurCompany() {
        return stdCurCompany;
    }

    /**
     *
     * @param stdCurCompany
     * The StdCurCompany
     */
    public void setStdCurCompany(String stdCurCompany) {
        this.stdCurCompany = stdCurCompany;
    }

    /**
     *
     * @return
     * The contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     *
     * @param contactPerson
     * The ContactPerson
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     *
     * @return
     * The salary
     */
    public String getSalary() {
        return salary;
    }

    /**
     *
     * @param salary
     * The Salary
     */
    public void setSalary(String salary) {
        this.salary = salary;
    }

    /**
     *
     * @return
     * The locId
     */
    public Integer getLocId() {
        return locId;
    }

    /**
     *
     * @param locId
     * The LocId
     */
    public void setLocId(Integer locId) {
        this.locId = locId;
    }

    /**
     *
     * @return
     * The inId
     */
    public Integer getInId() {
        return inId;
    }

    /**
     *
     * @param inId
     * The InId
     */
    public void setInId(Integer inId) {
        this.inId = inId;
    }

    /**
     *
     * @return
     * The fId
     */
    public Integer getFId() {
        return fId;
    }

    /**
     *
     * @param fId
     * The FId
     */
    public void setFId(Integer fId) {
        this.fId = fId;
    }

    /**
     *
     * @return
     * The rId
     */
    public Integer getRId() {
        return rId;
    }

    /**
     *
     * @param rId
     * The RId
     */
    public void setRId(Integer rId) {
        this.rId = rId;
    }

    /**
     *
     * @return
     * The jTId
     */
    public Integer getJTId() {
        return jTId;
    }

    /**
     *
     * @param jTId
     * The JTId
     */
    public void setJTId(Integer jTId) {
        this.jTId = jTId;
    }

    /**
     *
     * @return
     * The pbId
     */
    public Integer getPbId() {
        return pbId;
    }

    /**
     *
     * @param pbId
     * The PbId
     */
    public void setPbId(Integer pbId) {
        this.pbId = pbId;
    }

    /**
     *
     * @return
     * The postedType
     */
    public String getPostedType() {
        return postedType;
    }

    /**
     *
     * @param postedType
     * The PostedType
     */
    public void setPostedType(String postedType) {
        this.postedType = postedType;
    }

    /**
     *
     * @return
     * The designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     *
     * @param designation
     * The Designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     *
     * @return
     * The viewsCount
     */
    public Integer getViewsCount() {
        return viewsCount;
    }

    /**
     *
     * @param viewsCount
     * The ViewsCount
     */
    public void setViewsCount(Integer viewsCount) {
        this.viewsCount = viewsCount;
    }

    /**
     *
     * @return
     * The appliedCount
     */
    public Integer getAppliedCount() {
        return appliedCount;
    }

    /**
     *
     * @param appliedCount
     * The AppliedCount
     */
    public void setAppliedCount(Integer appliedCount) {
        this.appliedCount = appliedCount;
    }

    /**
     *
     * @return
     * The endClient
     */
    public String getEndClient() {
        return endClient;
    }

    /**
     *
     * @param endClient
     * The EndClient
     */
    public void setEndClient(String endClient) {
        this.endClient = endClient;
    }

    /**
     *
     * @return
     * The matchedScore
     */
    public String getMatchedScore() {
        return matchedScore;
    }

    /**
     *
     * @param matchedScore
     * The MatchedScore
     */
    public void setMatchedScore(String matchedScore) {
        this.matchedScore = matchedScore;
    }

    /**
     *
     * @return
     * The noticePeriod
     */
    public Integer getNoticePeriod() {
        return noticePeriod;
    }

    /**
     *
     * @param noticePeriod
     * The NoticePeriod
     */
    public void setNoticePeriod(Integer noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    /**
     *
     * @return
     * The walkinDate
     */
    public String getWalkinDate() {
        return walkinDate;
    }

    /**
     *
     * @param walkinDate
     * The WalkinDate
     */
    public void setWalkinDate(String walkinDate) {
        this.walkinDate = walkinDate;
    }

    /**
     *
     * @return
     * The searchQuery
     */
    public Object getSearchQuery() {
        return searchQuery;
    }

    /**
     *
     * @param searchQuery
     * The SearchQuery
     */
    public void setSearchQuery(Object searchQuery) {
        this.searchQuery = searchQuery;
    }

    /**
     *
     * @return
     * The resume
     */
    public Object getResume() {
        return resume;
    }

    /**
     *
     * @param resume
     * The Resume
     */
    public void setResume(Object resume) {
        this.resume = resume;
    }

    /**
     *
     * @return
     * The jobPositions
     */
    public Integer getJobPositions() {
        return jobPositions;
    }

    /**
     *
     * @param jobPositions
     * The jobPositions
     */
    public void setJobPositions(Integer jobPositions) {
        this.jobPositions = jobPositions;
    }

    /**
     *
     * @return
     * The logoString
     */
    public Object getLogoString() {
        return logoString;
    }

    /**
     *
     * @param logoString
     * The LogoString
     */
    public void setLogoString(Object logoString) {
        this.logoString = logoString;
    }

    /**
     *
     * @return
     * The recruiterCompanyName
     */
    public String getRecruiterCompanyName() {
        return recruiterCompanyName;
    }

    /**
     *
     * @param recruiterCompanyName
     * The RecruiterCompanyName
     */
    public void setRecruiterCompanyName(String recruiterCompanyName) {
        this.recruiterCompanyName = recruiterCompanyName;
    }

    /**
     *
     * @return
     * The matchedProfilesQuery
     */
    public String getMatchedProfilesQuery() {
        return matchedProfilesQuery;
    }

    /**
     *
     * @param matchedProfilesQuery
     * The MatchedProfilesQuery
     */
    public void setMatchedProfilesQuery(String matchedProfilesQuery) {
        this.matchedProfilesQuery = matchedProfilesQuery;
    }

    /**
     *
     * @return
     * The similarJobsQuery
     */
    public String getSimilarJobsQuery() {
        return similarJobsQuery;
    }

    /**
     *
     * @param similarJobsQuery
     * The SimilarJobsQuery
     */
    public void setSimilarJobsQuery(String similarJobsQuery) {
        this.similarJobsQuery = similarJobsQuery;
    }

    /**
     *
     * @return
     * The similarJobsCount
     */
    public Integer getSimilarJobsCount() {
        return similarJobsCount;
    }

    /**
     *
     * @param similarJobsCount
     * The SimilarJobsCount
     */
    public void setSimilarJobsCount(Integer similarJobsCount) {
        this.similarJobsCount = similarJobsCount;
    }

    /**
     *
     * @return
     * The isRecruiterOnline
     */
    public Boolean getIsRecruiterOnline() {
        return isRecruiterOnline;
    }

    /**
     *
     * @param isRecruiterOnline
     * The IsRecruiterOnline
     */
    public void setIsRecruiterOnline(Boolean isRecruiterOnline) {
        this.isRecruiterOnline = isRecruiterOnline;
    }

    /**
     *
     * @return
     * The matchedProfilesCount
     */
    public Integer getMatchedProfilesCount() {
        return matchedProfilesCount;
    }

    /**
     *
     * @param matchedProfilesCount
     * The MatchedProfilesCount
     */
    public void setMatchedProfilesCount(Integer matchedProfilesCount) {
        this.matchedProfilesCount = matchedProfilesCount;
    }

    /**
     *
     * @return
     * The documents
     */
    public String getDocuments() {
        return documents;
    }

    /**
     *
     * @param documents
     * The Documents
     */
    public void setDocuments(String documents) {
        this.documents = documents;
    }

    /**
     *
     * @return
     * The rolesResponsibilties
     */
    public String getRolesResponsibilties() {
        return rolesResponsibilties;
    }

    /**
     *
     * @param rolesResponsibilties
     * The RolesResponsibilties
     */
    public void setRolesResponsibilties(String rolesResponsibilties) {
        this.rolesResponsibilties = rolesResponsibilties;
    }

    /**
     *
     * @return
     * The questionier
     */
    public Object getQuestionier() {
        return questionier;
    }

    /**
     *
     * @param questionier
     * The questionier
     */
    public void setQuestionier(Object questionier) {
        this.questionier = questionier;
    }

    /**
     *
     * @return
     * The locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     *
     * @param locality
     * The Locality
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    /**
     *
     * @return
     * The ratedSkills
     */
    public Object getRatedSkills() {
        return ratedSkills;
    }

    /**
     *
     * @param ratedSkills
     * The RatedSkills
     */
    public void setRatedSkills(Object ratedSkills) {
        this.ratedSkills = ratedSkills;
    }

    /**
     *
     * @return
     * The postedBy
     */
    public String getPostedBy() {
        return postedBy;
    }

    /**
     *
     * @param postedBy
     * The postedBy
     */
    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

}
