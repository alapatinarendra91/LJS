package com.localjobserver.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 29-03-2015.
 */
public class FreshersData implements Parcelable{

    private String courceName;
    private String courceID;
    private String institutionName;
    private String contactPname;
    private String discountType;
    private String discountValue;
    private String courceFee;
    private String eligibility;
    private String instruvtorName;
    private String email;
    private String freConNum;
    private String landNum;
    private String startDate;
    private String endDate;
    private String duration;
    private String address;
    private String compURL;
    private String startTime;
    private String endTime;
    private String venue;
    private String batch;
    private String brotcherLink;
    private String section;
    private String postedDate;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;


    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }


    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCourceName() {
        return courceName;
    }

    public void setCourceName(String courceName) {
        this.courceName = courceName;
    }

    public String getCourceID() {
        return courceID;
    }

    public void setCourceID(String courceID) {
        this.courceID = courceID;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getContactPname() {
        return contactPname;
    }

    public void setContactPname(String contactPname) {
        this.contactPname = contactPname;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getCourceFee() {
        return courceFee;
    }

    public void setCourceFee(String courceFee) {
        this.courceFee = courceFee;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public String getInstruvtorName() {
        return instruvtorName;
    }

    public void setInstruvtorName(String instruvtorName) {
        this.instruvtorName = instruvtorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFreConNum() {
        return freConNum;
    }

    public void setFreConNum(String freConNum) {
        this.freConNum = freConNum;
    }

    public String getLandNum() {
        return landNum;
    }

    public void setLandNum(String landNum) {
        this.landNum = landNum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompURL() {
        return compURL;
    }

    public void setCompURL(String compURL) {
        this.compURL = compURL;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getBrotcherLink() {
        return brotcherLink;
    }

    public void setBrotcherLink(String brotcherLink) {
        this.brotcherLink = brotcherLink;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }


    public FreshersData(Parcel in){
        courceName  = in.readString();
        courceID  = in.readString();
        institutionName = in.readString();
        contactPname = in.readString();
        discountType = in.readString();
        discountValue = in.readString();
        courceFee = in.readString();
        eligibility = in.readString();
        instruvtorName = in.readString();
        email  = in.readString();
        freConNum = in.readString();
        landNum = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        duration = in.readString();
        address = in.readString();
        compURL = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        venue = in.readString();
        batch = in.readString();
        brotcherLink = in.readString();
        section = in.readString();
        postedDate = in.readString();
        city = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(courceName);
        dest.writeString(courceID);
        dest.writeString(institutionName);
        dest.writeString(contactPname);
        dest.writeString(discountType);
        dest.writeString(discountValue);
        dest.writeString(courceFee);
        dest.writeString(eligibility);
        dest.writeString(instruvtorName);
        dest.writeString(email);
        dest.writeString(freConNum);
        dest.writeString(landNum);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(duration);
        dest.writeString(address);
        dest.writeString(compURL);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(venue);
        dest.writeString(batch);
        dest.writeString(brotcherLink);
        dest.writeString(section);
        dest.writeString(postedDate);
        dest.writeString(city);
    }

    public  FreshersData(){

    }

    /** * * This field is needed for Android to be able to * create new objects, individually or as arrays. * *
     * This also means that you can use use the default * constructor to create the object and use another * method to hyrdate it as necessary.
     *  * * I just find it easier to use the constructor. * It makes sense for the way my brain thinks ;-)
     *  * */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FreshersData createFromParcel(Parcel in) {
            return new FreshersData(in);
        }
        public FreshersData[] newArray(int size) {
            return new FreshersData[size];
        } };


    @Override
    public int describeContents() {
        return 0;
    }
}
