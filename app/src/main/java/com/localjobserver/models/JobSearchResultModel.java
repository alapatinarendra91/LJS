package com.localjobserver.models;

/**
 * Created by nl on 03-Jun-16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobSearchResultModel {

    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("SubLocation")
    @Expose
    private String subLocation;
    @SerializedName("MinExp")
    @Expose
    private String minExp;
    @SerializedName("MaxExp")
    @Expose
    private String maxExp;
    @SerializedName("MaxSal")
    @Expose
    private String maxSal;
    @SerializedName("MinSal")
    @Expose
    private String minSal;
    @SerializedName("JobType")
    @Expose
    private String jobType;
    @SerializedName("Industry")
    @Expose
    private String industry;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("Education")
    @Expose
    private String education;
    @SerializedName("PostedBy")
    @Expose
    private String postedBy;
    @SerializedName("JType")
    @Expose
    private String jType;

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The Location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The subLocation
     */
    public String getSubLocation() {
        return subLocation;
    }

    /**
     *
     * @param subLocation
     * The SubLocation
     */
    public void setSubLocation(String subLocation) {
        this.subLocation = subLocation;
    }

    /**
     *
     * @return
     * The minExp
     */
    public String getMinExp() {
        return minExp;
    }

    /**
     *
     * @param minExp
     * The MinExp
     */
    public void setMinExp(String minExp) {
        this.minExp = minExp;
    }

    /**
     *
     * @return
     * The maxExp
     */
    public String getMaxExp() {
        return maxExp;
    }

    /**
     *
     * @param maxExp
     * The MaxExp
     */
    public void setMaxExp(String maxExp) {
        this.maxExp = maxExp;
    }

    /**
     *
     * @return
     * The maxSal
     */
    public String getMaxSal() {
        return maxSal;
    }

    /**
     *
     * @param maxSal
     * The MaxSal
     */
    public void setMaxSal(String maxSal) {
        this.maxSal = maxSal;
    }

    /**
     *
     * @return
     * The minSal
     */
    public String getMinSal() {
        return minSal;
    }

    /**
     *
     * @param minSal
     * The MinSal
     */
    public void setMinSal(String minSal) {
        this.minSal = minSal;
    }

    /**
     *
     * @return
     * The jobType
     */
    public String getJobType() {
        return jobType;
    }

    /**
     *
     * @param jobType
     * The JobType
     */
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    /**
     *
     * @return
     * The industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     *
     * @param industry
     * The Industry
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     *
     * @return
     * The role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The Role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The education
     */
    public String getEducation() {
        return education;
    }

    /**
     *
     * @param education
     * The Education
     */
    public void setEducation(String education) {
        this.education = education;
    }

    /**
     *
     * @return
     * The postedBy
     */
    public String getPostedBy() {
        return postedBy;
    }

    /**
     *
     * @param postedBy
     * The PostedBy
     */
    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    /**
     *
     * @return
     * The jType
     */
    public String getJType() {
        return jType;
    }

    /**
     *
     * @param jType
     * The JType
     */
    public void setJType(String jType) {
        this.jType = jType;
    }

}
