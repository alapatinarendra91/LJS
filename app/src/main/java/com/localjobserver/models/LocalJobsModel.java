package com.localjobserver.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 04-05-2015.
 */
public class LocalJobsModel {

    @SerializedName("roleData")
    @Expose
    private List<RoleDatum> roleData = new ArrayList<RoleDatum>();
    @SerializedName("jobCount")
    @Expose
    private Integer jobCount;
    @SerializedName("searchResultsBy")
    @Expose
    private Integer searchResultsBy;

    /**
     *
     * @return
     * The roleData
     */
    public List<RoleDatum> getRoleData() {
        return roleData;
    }

    /**
     *
     * @param roleData
     * The roleData
     */
    public void setRoleData(List<RoleDatum> roleData) {
        this.roleData = roleData;
    }

    /**
     *
     * @return
     * The jobCount
     */
    public Integer getJobCount() {
        return jobCount;
    }

    /**
     *
     * @param jobCount
     * The jobCount
     */
    public void setJobCount(Integer jobCount) {
        this.jobCount = jobCount;
    }

    /**
     *
     * @return
     * The searchResultsBy
     */
    public Integer getSearchResultsBy() {
        return searchResultsBy;
    }

    /**
     *
     * @param searchResultsBy
     * The searchResultsBy
     */
    public void setSearchResultsBy(Integer searchResultsBy) {
        this.searchResultsBy = searchResultsBy;
    }

}
