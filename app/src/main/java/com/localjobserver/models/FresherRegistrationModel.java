package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by siva on 11/10/15.
 */
public class FresherRegistrationModel {

    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("Password")
    @Expose
    private String Password;
    @SerializedName("ConfirmPassword")
    @Expose
    private String ConfirmPassword;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("LandLine")
    @Expose
    private String LandLine;
    @SerializedName("YOP")
    @Expose
    private String YOP;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Education")
    @Expose
    private String Education;
    @SerializedName("UpdateOn")
    @Expose
    private String UpdateOn;
    @SerializedName("Photo")
    @Expose
    private String Photo;
    @SerializedName("StdKeySkills")
    @Expose
    private String StdKeySkills;
    @SerializedName("Gender")
    @Expose
    private String Gender;
    @SerializedName("DateOfBirth")
    @Expose
    private String DateOfBirth;
    @SerializedName("KeySkills")
    @Expose
    private String KeySkills;
    @SerializedName("CollegeName")
    @Expose
    private String CollegeName;
    @SerializedName("EmailVerified")
    @Expose
    private Boolean EmailVerified;
    @SerializedName("IsOnline")
    @Expose
    private Boolean IsOnline;
    @SerializedName("Resume")
    @Expose
    private String Resume;
    @SerializedName("UpdateResume")
    @Expose
    private String UpdateResume;
    @SerializedName("ResumeHeadLine")
    @Expose
    private String ResumeHeadLine;
    @SerializedName("TextResume")
    @Expose
    private String TextResume;
    @SerializedName("FCType")
    @Expose
    private String FCType;
    @SerializedName("photoString")
    @Expose
    private String photoString;



    /**
     *
     * @return
     * The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     *
     * @param FirstName
     * The FirstName
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     *
     * @return
     * The LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     *
     * @param LastName
     * The LastName
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     *
     * @return
     * The Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     *
     * @param Password
     * The Password
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     *
     * @return
     * The ConfirmPassword
     */
    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    /**
     *
     * @param ConfirmPassword
     * The ConfirmPassword
     */
    public void setConfirmPassword(String ConfirmPassword) {
        this.ConfirmPassword = ConfirmPassword;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     *
     * @param ContactNo
     * The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     *
     * @return
     * The LandLine
     */
    public String getLandLine() {
        return LandLine;
    }

    /**
     *
     * @param LandLine
     * The LandLine
     */
    public void setLandLine(String LandLine) {
        this.LandLine = LandLine;
    }

    /**
     *
     * @return
     * The YOP
     */
    public String getYOP() {
        return YOP;
    }

    /**
     *
     * @param YOP
     * The YOP
     */
    public void setYOP(String YOP) {
        this.YOP = YOP;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The Education
     */
    public String getEducation() {
        return Education;
    }

    /**
     *
     * @param Education
     * The Education
     */
    public void setEducation(String Education) {
        this.Education = Education;
    }

    /**
     *
     * @return
     * The UpdateOn
     */
    public String getUpdateOn() {
        return UpdateOn;
    }

    /**
     *
     * @param UpdateOn
     * The UpdateOn
     */
    public void setUpdateOn(String UpdateOn) {
        this.UpdateOn = UpdateOn;
    }

    /**
     *
     * @return
     * The Photo
     */
    public String getPhoto() {
        return Photo;
    }

    /**
     *
     * @param Photo
     * The Photo
     */
    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    /**
     *
     * @return
     * The StdKeySkills
     */
    public String getStdKeySkills() {
        return StdKeySkills;
    }

    /**
     *
     * @param StdKeySkills
     * The StdKeySkills
     */
    public void setStdKeySkills(String StdKeySkills) {
        this.StdKeySkills = StdKeySkills;
    }

    /**
     *
     * @return
     * The Gender
     */
    public String getGender() {
        return Gender;
    }

    /**
     *
     * @param Gender
     * The Gender
     */
    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    /**
     *
     * @return
     * The DateOfBirth
     */
    public String getDateOfBirth() {
        return DateOfBirth;
    }

    /**
     *
     * @param DateOfBirth
     * The DateOfBirth
     */
    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    /**
     *
     * @return
     * The KeySkills
     */
    public String getKeySkills() {
        return KeySkills;
    }

    /**
     *
     * @param KeySkills
     * The KeySkills
     */
    public void setKeySkills(String KeySkills) {
        this.KeySkills = KeySkills;
    }

    /**
     *
     * @return
     * The CollegeName
     */
    public String getCollegeName() {
        return CollegeName;
    }

    /**
     *
     * @param CollegeName
     * The CollegeName
     */
    public void setCollegeName(String CollegeName) {
        this.CollegeName = CollegeName;
    }

    /**
     *
     * @return
     * The EmailVerified
     */
    public Boolean getEmailVerified() {
        return EmailVerified;
    }

    /**
     *
     * @param EmailVerified
     * The EmailVerified
     */
    public void setEmailVerified(Boolean EmailVerified) {
        this.EmailVerified = EmailVerified;
    }

    /**
     *
     * @return
     * The IsOnline
     */
    public Boolean getIsOnline() {
        return IsOnline;
    }

    /**
     *
     * @param IsOnline
     * The IsOnline
     */
    public void setIsOnline(Boolean IsOnline) {
        this.IsOnline = IsOnline;
    }

    /**
     *
     * @return
     * The Resume
     */
    public String getResume() {
        return Resume;
    }

    /**
     *
     * @param Resume
     * The Resume
     */
    public void setResume(String Resume) {
        this.Resume = Resume;
    }

    /**
     *
     * @return
     * The UpdateResume
     */
    public String getUpdateResume() {
        return UpdateResume;
    }

    /**
     *
     * @param UpdateResume
     * The UpdateResume
     */
    public void setUpdateResume(String UpdateResume) {
        this.UpdateResume = UpdateResume;
    }

    /**
     *
     * @return
     * The ResumeHeadLine
     */
    public String getResumeHeadLine() {
        return ResumeHeadLine;
    }

    /**
     *
     * @param ResumeHeadLine
     * The ResumeHeadLine
     */
    public void setResumeHeadLine(String ResumeHeadLine) {
        this.ResumeHeadLine = ResumeHeadLine;
    }

    /**
     *
     * @return
     * The TextResume
     */
    public String getTextResume() {
        return TextResume;
    }

    /**
     *
     * @param TextResume
     * The TextResume
     */
    public void setTextResume(String TextResume) {
        this.TextResume = TextResume;
    }

    /**
     *
     * @return
     * The FCType
     */
    public String getFCType() {
        return FCType;
    }

    /**
     *
     * @param FCType
     * The FCType
     */
    public void setFCType(String FCType) {
        this.FCType = FCType;
    }

    /**
     *
     * @return
     * The photoString
     */
    public String getPhotoString() {
        return photoString;
    }

    /**
     *
     * @param photoString
     * The photoString
     */
    public void setPhotoString(String photoString) {
        this.photoString = photoString;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
