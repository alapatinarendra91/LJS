package com.localjobserver.models;

/**
 * Created by siva on 11/07/15.
 */
public class PrivacySettingsModel {
    public String Email;
    public String WeekDay;
    public String OnDemandTime;
    public boolean EmailAlert;
    public boolean SmsAlert;
    public boolean ChatAlert;
    public boolean LjsCommunication;
    public boolean PremiumJobSeekerService;


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getWeekDay() {
        return WeekDay;
    }

    public void setWeekDay(String weekDay) {
        WeekDay = weekDay;
    }

    public String getOnDemandTime() {
        return OnDemandTime;
    }

    public void setOnDemandTime(String onDemandTime) {
        OnDemandTime = onDemandTime;
    }

    public boolean getEmailAlert() {
        return EmailAlert;
    }

    public void setEmailAlert(boolean emailAlert) {
        EmailAlert = emailAlert;
    }

    public boolean getSmsAlert() {
        return SmsAlert;
    }

    public void setSmsAlert(boolean smsAlert) {
        SmsAlert = smsAlert;
    }

    public boolean getChatAlert() {
        return ChatAlert;
    }

    public void setChatAlert(boolean chatAlert) {
        ChatAlert = chatAlert;
    }

    public boolean getLjsCommunication() {
        return LjsCommunication;
    }

    public void setLjsCommunication(boolean ljsCommunication) {
        LjsCommunication = ljsCommunication;
    }

    public boolean getPremiumJobSeekerService() {
        return PremiumJobSeekerService;
    }

    public void setPremiumJobSeekerService(boolean premiumJobSeekerService) {
        PremiumJobSeekerService = premiumJobSeekerService;
    }
}
