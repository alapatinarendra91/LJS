package com.localjobserver.models;

/**
 * Created by ANDROID on 3/21/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PotentialModel {

    @SerializedName("UpDateOn")
    @Expose
    private String UpDateOn;
    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("StdKeySkills")
    @Expose
    private String StdKeySkills;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("IsOnline")
    @Expose
    private Boolean IsOnline;
    @SerializedName("KeySkills")
    @Expose
    private String KeySkills;
    @SerializedName("TableName")
    @Expose
    private String TableName;
    @SerializedName("PostId")
    @Expose
    private Integer PostId;

    /**
     *
     * @return
     * The UpDateOn
     */
    public String getUpDateOn() {
        return UpDateOn;
    }

    /**
     *
     * @param UpDateOn
     * The UpDateOn
     */
    public void setUpDateOn(String UpDateOn) {
        this.UpDateOn = UpDateOn;
    }

    /**
     *
     * @return
     * The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     *
     * @param FirstName
     * The FirstName
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     *
     * @return
     * The StdKeySkills
     */
    public String getStdKeySkills() {
        return StdKeySkills;
    }

    /**
     *
     * @param StdKeySkills
     * The StdKeySkills
     */
    public void setStdKeySkills(String StdKeySkills) {
        this.StdKeySkills = StdKeySkills;
    }

    /**
     *
     * @return
     * The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     *
     * @param ContactNo
     * The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The Name
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name
     * The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     *
     * @return
     * The IsOnline
     */
    public Boolean getIsOnline() {
        return IsOnline;
    }

    /**
     *
     * @param IsOnline
     * The IsOnline
     */
    public void setIsOnline(Boolean IsOnline) {
        this.IsOnline = IsOnline;
    }

    /**
     *
     * @return
     * The KeySkills
     */
    public String getKeySkills() {
        return KeySkills;
    }

    /**
     *
     * @param KeySkills
     * The KeySkills
     */
    public void setKeySkills(String KeySkills) {
        this.KeySkills = KeySkills;
    }

    /**
     *
     * @return
     * The TableName
     */
    public String getTableName() {
        return TableName;
    }

    /**
     *
     * @param TableName
     * The TableName
     */
    public void setTableName(String TableName) {
        this.TableName = TableName;
    }

    /**
     *
     * @return
     * The PostId
     */
    public Integer getPostId() {
        return PostId;
    }

    /**
     *
     * @param PostId
     * The PostId
     */
    public void setPostId(Integer PostId) {
        this.PostId = PostId;
    }

}
