package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class TrainerRegistrationModel {

    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("Password")
    @Expose
    private String Password;
    @SerializedName("ConfirmPassword")
    @Expose
    private String ConfirmPassword;
    @SerializedName("InstituteName")
    @Expose
    private String InstituteName;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("ContactNo")
    @Expose
    private String ContactNo;
    @SerializedName("ContactNo_Landline")
    @Expose
    private String ContactNoLandline;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Locality")
    @Expose
    private String Locality;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("Photo")
    @Expose
    private String Photo;
    @SerializedName("photoString")
    @Expose
    private String photoString;
    @SerializedName("UpdateOn")
    @Expose
    private String UpdateOn;
    @SerializedName("CoursesOffer")
    @Expose
    private String CoursesOffer;
    @SerializedName("EmailVerified")
    @Expose
    private Boolean EmailVerified;
    @SerializedName("StdCourseName")
    @Expose
    private String StdCourseName;
    @SerializedName("IsOnline")
    @Expose
    private Boolean IsOnline;
    @SerializedName("DTId")
    @Expose
    private Integer DTId;
    @SerializedName("DTType")
    @Expose
    private String DTType;
    @SerializedName("TrainerStatus")
    @Expose
    private Boolean TrainerStatus;

    /**
     *
     * @return
     * The FirstName
     */
    public String getFirstName() {
        return FirstName;
    }

    /**
     *
     * @param FirstName
     * The FirstName
     */
    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    /**
     *
     * @return
     * The LastName
     */
    public String getLastName() {
        return LastName;
    }

    /**
     *
     * @param LastName
     * The LastName
     */
    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    /**
     *
     * @return
     * The Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     *
     * @param Password
     * The Password
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     *
     * @return
     * The ConfirmPassword
     */
    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    /**
     *
     * @param ConfirmPassword
     * The ConfirmPassword
     */
    public void setConfirmPassword(String ConfirmPassword) {
        this.ConfirmPassword = ConfirmPassword;
    }

    /**
     *
     * @return
     * The InstituteName
     */
    public String getInstituteName() {
        return InstituteName;
    }

    /**
     *
     * @param InstituteName
     * The InstituteName
     */
    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The ContactNo
     */
    public String getContactNo() {
        return ContactNo;
    }

    /**
     *
     * @param ContactNo
     * The ContactNo
     */
    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    /**
     *
     * @return
     * The ContactNoLandline
     */
    public String getContactNoLandline() {
        return ContactNoLandline;
    }

    /**
     *
     * @param ContactNoLandline
     * The ContactNo_Landline
     */
    public void setContactNoLandline(String ContactNoLandline) {
        this.ContactNoLandline = ContactNoLandline;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The Locality
     */
    public String getLocality() {
        return Locality;
    }

    /**
     *
     * @param Locality
     * The Locality
     */
    public void setLocality(String Locality) {
        this.Locality = Locality;
    }

    /**
     *
     * @return
     * The Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     * The Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     * The Photo
     */
    public String getPhoto() {
        return Photo;
    }

    /**
     *
     * @param Photo
     * The Photo
     */
    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    /**
     *
     * @return
     * The photoString
     */
    public String getPhotoString() {
        return photoString;
    }

    /**
     *
     * @param photoString
     * The photoString
     */
    public void setPhotoString(String photoString) {
        this.photoString = photoString;
    }

    /**
     *
     * @return
     * The UpdateOn
     */
    public String getUpdateOn() {
        return UpdateOn;
    }

    /**
     *
     * @param UpdateOn
     * The UpdateOn
     */
    public void setUpdateOn(String UpdateOn) {
        this.UpdateOn = UpdateOn;
    }

    /**
     *
     * @return
     * The CoursesOffer
     */
    public String getCoursesOffer() {
        return CoursesOffer;
    }

    /**
     *
     * @param CoursesOffer
     * The CoursesOffer
     */
    public void setCoursesOffer(String CoursesOffer) {
        this.CoursesOffer = CoursesOffer;
    }

    /**
     *
     * @return
     * The EmailVerified
     */
    public Boolean getEmailVerified() {
        return EmailVerified;
    }

    /**
     *
     * @param EmailVerified
     * The EmailVerified
     */
    public void setEmailVerified(Boolean EmailVerified) {
        this.EmailVerified = EmailVerified;
    }

    /**
     *
     * @return
     * The StdCourseName
     */
    public String getStdCourseName() {
        return StdCourseName;
    }

    /**
     *
     * @param StdCourseName
     * The StdCourseName
     */
    public void setStdCourseName(String StdCourseName) {
        this.StdCourseName = StdCourseName;
    }

    /**
     *
     * @return
     * The IsOnline
     */
    public Boolean getIsOnline() {
        return IsOnline;
    }

    /**
     *
     * @param IsOnline
     * The IsOnline
     */
    public void setIsOnline(Boolean IsOnline) {
        this.IsOnline = IsOnline;
    }

    /**
     *
     * @return
     * The DTId
     */
    public Integer getDTId() {
        return DTId;
    }

    /**
     *
     * @param DTId
     * The DTId
     */
    public void setDTId(Integer DTId) {
        this.DTId = DTId;
    }

    /**
     *
     * @return
     * The DTType
     */
    public String getDTType() {
        return DTType;
    }

    /**
     *
     * @param DTType
     * The DTType
     */
    public void setDTType(String DTType) {
        this.DTType = DTType;
    }

    /**
     *
     * @return
     * The TrainerStatus
     */
    public Boolean getTrainerStatus() {
        return TrainerStatus;
    }

    /**
     *
     * @param TrainerStatus
     * The TrainerStatus
     */
    public void setTrainerStatus(Boolean TrainerStatus) {
        this.TrainerStatus = TrainerStatus;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}