package com.localjobserver.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by siva on 10/07/15.
 */
public class SearchModel {

    @JsonProperty("ReturnUrl")
    public String ReturnUrl = "";
    @JsonProperty("KeySkills")
    public String KeySkills = "";
    @JsonProperty("Scope")
    public String Scope = "";
    @JsonProperty("Location")
    public String Location = "";
    @JsonProperty("MinExp")
    public String MinExp = "";
    @JsonProperty("MaxExp")
    public String MaxExp = "";
    @JsonProperty("CurrentCTC")
    public String CurrentCTC = "";
    @JsonProperty("Industry")
    public String Industry = "";
    @JsonProperty("FunctionalArea")
    public String FunctionalArea = "";
    @JsonProperty("CompanyName")
    public String CompanyName = "";
    @JsonProperty("Role")
    public String Role = "";
    @JsonProperty("HighestDegree")
    public String HighestDegree = "";
    @JsonProperty("CandidatesActiveinLast")
    public String CandidatesActiveinLast = "";
    @JsonProperty("ResumeperPage")
    public String ResumeperPage = "";
    @JsonProperty("ActiveDate")
    public String ActiveDate = "";
    @JsonProperty("PreferedLocation")
    public String PreferedLocation = "";
    @JsonProperty("SearchQuery")
    public String SearchQuery = "";
    @JsonProperty("Email")
    public String Email = "";
    @JsonProperty("SearchType")
    public String SearchType = "";
    @JsonProperty("Date")
    public String Date = "";
    @JsonProperty("SaveId")
    public String SaveId = "";
    @JsonProperty("Rid")
    public String Rid = "";
    @JsonProperty("Fid")
    public String Fid = "";
    @JsonProperty("IndId")
    public String IndId = "";
    @JsonProperty("Education")
    public String Education = "";
    @JsonProperty("NoticePeriod")
    public String NoticePeriod = "";


    public String getReturnUrl() {
        return ReturnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        ReturnUrl = returnUrl;
    }

    public String getKeySkills() {
        return KeySkills;
    }

    public void setKeySkills(String keySkills) {
        KeySkills = keySkills;
    }

    public String getScope() {
        return Scope;
    }

    public void setScope(String scope) {
        Scope = scope;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getMinExp() {
        return MinExp;
    }

    public void setMinExp(String minExp) {
        MinExp = minExp;
    }

    public String getMaxExp() {
        return MaxExp;
    }

    public void setMaxExp(String maxExp) {
        MaxExp = maxExp;
    }

    public String getCurrentCTC() {
        return CurrentCTC;
    }

    public void setCurrentCTC(String CurrentCTC) {
        CurrentCTC = CurrentCTC;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String industry) {
        Industry = industry;
    }

    public String getFunctionalArea() {
        return FunctionalArea;
    }

    public void setFunctionalArea(String functionalArea) {
        FunctionalArea = functionalArea;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getHighestDegree() {
        return HighestDegree;
    }

    public void setHighestDegree(String highestDegree) {
        HighestDegree = highestDegree;
    }

    public String getCandidatesActiveinLast() {
        return CandidatesActiveinLast;
    }

    public void setCandidatesActiveinLast(String candidatesActiveinLast) {
        CandidatesActiveinLast = candidatesActiveinLast;
    }

    public String getResumeperPage() {
        return ResumeperPage;
    }

    public void setResumeperPage(String resumeperPage) {
        ResumeperPage = resumeperPage;
    }

    public String getActiveDate() {
        return ActiveDate;
    }

    public void setActiveDate(String activeDate) {
        ActiveDate = activeDate;
    }

    public String getPreferedLocation() {
        return PreferedLocation;
    }

    public void setPreferedLocation(String preferedLocation) {
        PreferedLocation = preferedLocation;
    }

    public String getSearchQuery() {
        return SearchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        SearchQuery = searchQuery;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSearchType() {
        return SearchType;
    }

    public void setSearchType(String searchType) {
        SearchType = searchType;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getSaveId() {
        return SaveId;
    }

    public void setSaveId(String saveId) {
        SaveId = saveId;
    }

    public String getRid() {
        return Rid;
    }

    public void setRid(String rid) {
        Rid = rid;
    }

    public String getFid() {
        return Fid;
    }

    public void setFid(String fid) {
        Fid = fid;
    }

    public String getIndId() {
        return IndId;
    }

    public void setIndId(String indId) {
        IndId = indId;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getNoticePeriod() {
        return NoticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        NoticePeriod = noticePeriod;
    }
}
