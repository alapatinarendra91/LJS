package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class RecommendedCourses implements Serializable {

    @SerializedName("InstituteName")
    @Expose
    private String InstituteName;
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("PostId")
    @Expose
    private Integer PostId;
    @SerializedName("AppliedCourseCount")
    @Expose
    private Integer AppliedCourseCount;
    @SerializedName("InstId")
    @Expose
    private Integer InstId;
    @SerializedName("City")
    @Expose
    private String City;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("CourseFee")
    @Expose
    private String CourseFee;
    @SerializedName("CourseName")
    @Expose
    private String CourseName;
    @SerializedName("CourseDescription")
    @Expose
    private String CourseDescription;
    @SerializedName("StdCourseName")
    @Expose
    private String StdCourseName;
    @SerializedName("InstructorName")
    @Expose
    private String InstructorName;
    @SerializedName("Eligibility")
    @Expose
    private String Eligibility;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("ContactNumber")
    @Expose
    private String ContactNumber;
    @SerializedName("ContactNo_Landline")
    @Expose
    private String ContactNoLandline;
    @SerializedName("DiscountType")
    @Expose
    private String DiscountType;
    @SerializedName("DiscountValue")
    @Expose
    private String DiscountValue;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("ContactPerson")
    @Expose
    private String ContactPerson;
    @SerializedName("CompanyURL")
    @Expose
    private String CompanyURL;
    @SerializedName("Venue")
    @Expose
    private String Venue;
    @SerializedName("Section")
    @Expose
    private String Section;
    @SerializedName("AttachBrochure")
    @Expose
    private String AttachBrochure;
    @SerializedName("PostDate")
    @Expose
    private String PostDate;
    @SerializedName("StartDate")
    @Expose
    private String StartDate;
    @SerializedName("EndDate")
    @Expose
    private String EndDate;
    @SerializedName("StartTime")
    @Expose
    private String StartTime;
    @SerializedName("EndTime")
    @Expose
    private String EndTime;
    @SerializedName("Duration")
    @Expose
    private String Duration;
    @SerializedName("EveryDay")
    @Expose
    private Boolean EveryDay;
    @SerializedName("CourseStatus")
    @Expose
    private Boolean CourseStatus;
    @SerializedName("Online")
    @Expose
    private Boolean Online;
    @SerializedName("FastTrack")
    @Expose
    private Boolean FastTrack;
    @SerializedName("Weekend")
    @Expose
    private Boolean Weekend;
    @SerializedName("IsOnline")
    @Expose
    private Boolean IsOnline;
    @SerializedName("Normaltrack")
    @Expose
    private Boolean Normaltrack;
    @SerializedName("BasicInformation")
    @Expose
    private Boolean BasicInformation;
    @SerializedName("AfterDiscountedfee")
    @Expose
    private Integer AfterDiscountedfee;
    @SerializedName("ProjectWork")
    @Expose
    private Boolean ProjectWork;
    @SerializedName("EarlyBirdDiscountDate")
    @Expose
    private String EarlyBirdDiscountDate;
    @SerializedName("BatchSize")
    @Expose
    private Integer BatchSize;
    @SerializedName("Status")
    @Expose
    private String Status;
    @SerializedName("Resume")
    @Expose
    private String Resume;
    @SerializedName("GroupCount")
    @Expose
    private Integer GroupCount;
    @SerializedName("GroupValue")
    @Expose
    private String GroupValue;
    @SerializedName("PostedBy")
    @Expose
    private String PostedBy;

    /**
     *
     * @return
     * The InstituteName
     */
    public String getInstituteName() {
        return InstituteName;
    }

    /**
     *
     * @param InstituteName
     * The InstituteName
     */
    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    /**
     *
     * @return
     * The _id
     */
    public String get_id() {
        return _id;
    }

    /**
     *
     * @param _id
     * The PostId
     */
    public void set_id(String _id) {
        this._id = _id;
    }

    /**
     *
     * @return
     * The PostId
     */
    public Integer getPostId() {
        return PostId;
    }

    /**
     *
     * @param PostId
     * The PostId
     */
    public void setPostId(Integer PostId) {
        this.PostId = PostId;
    }

    /**
     *
     * @return
     * The AppliedCourseCount
     */
    public Integer getAppliedCourseCount() {
        return AppliedCourseCount;
    }

    /**
     *
     * @param AppliedCourseCount
     * The AppliedCourseCount
     */
    public void setAppliedCourseCount(Integer AppliedCourseCount) {
        this.AppliedCourseCount = AppliedCourseCount;
    }

    /**
     *
     * @return
     * The InstId
     */
    public Integer getInstId() {
        return InstId;
    }

    /**
     *
     * @param InstId
     * The InstId
     */
    public void setInstId(Integer InstId) {
        this.InstId = InstId;
    }

    /**
     *
     * @return
     * The City
     */
    public String getCity() {
        return City;
    }

    /**
     *
     * @param City
     * The City
     */
    public void setCity(String City) {
        this.City = City;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The CourseFee
     */
    public String getCourseFee() {
        return CourseFee;
    }

    /**
     *
     * @param CourseFee
     * The CourseFee
     */
    public void setCourseFee(String CourseFee) {
        this.CourseFee = CourseFee;
    }

    /**
     *
     * @return
     * The CourseName
     */
    public String getCourseName() {
        return CourseName;
    }

    /**
     *
     * @param CourseName
     * The CourseName
     */
    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }

    /**
     *
     * @return
     * The CourseDescription
     */
    public String getCourseDescription() {
        return CourseDescription;
    }

    /**
     *
     * @param CourseDescription
     * The CourseDescription
     */
    public void setCourseDescription(String CourseDescription) {
        this.CourseDescription = CourseDescription;
    }

    /**
     *
     * @return
     * The StdCourseName
     */
    public String getStdCourseName() {
        return StdCourseName;
    }

    /**
     *
     * @param StdCourseName
     * The StdCourseName
     */
    public void setStdCourseName(String StdCourseName) {
        this.StdCourseName = StdCourseName;
    }

    /**
     *
     * @return
     * The InstructorName
     */
    public String getInstructorName() {
        return InstructorName;
    }

    /**
     *
     * @param InstructorName
     * The InstructorName
     */
    public void setInstructorName(String InstructorName) {
        this.InstructorName = InstructorName;
    }

    /**
     *
     * @return
     * The Eligibility
     */
    public String getEligibility() {
        return Eligibility;
    }

    /**
     *
     * @param Eligibility
     * The Eligibility
     */
    public void setEligibility(String Eligibility) {
        this.Eligibility = Eligibility;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The ContactNumber
     */
    public String getContactNumber() {
        return ContactNumber;
    }

    /**
     *
     * @param ContactNumber
     * The ContactNumber
     */
    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    /**
     *
     * @return
     * The ContactNoLandline
     */
    public String getContactNoLandline() {
        return ContactNoLandline;
    }

    /**
     *
     * @param ContactNoLandline
     * The ContactNo_Landline
     */
    public void setContactNoLandline(String ContactNoLandline) {
        this.ContactNoLandline = ContactNoLandline;
    }

    /**
     *
     * @return
     * The DiscountType
     */
    public String getDiscountType() {
        return DiscountType;
    }

    /**
     *
     * @param DiscountType
     * The DiscountType
     */
    public void setDiscountType(String DiscountType) {
        this.DiscountType = DiscountType;
    }

    /**
     *
     * @return
     * The DiscountValue
     */
    public String getDiscountValue() {
        return DiscountValue;
    }

    /**
     *
     * @param DiscountValue
     * The DiscountValue
     */
    public void setDiscountValue(String DiscountValue) {
        this.DiscountValue = DiscountValue;
    }

    /**
     *
     * @return
     * The Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     * The Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     * The ContactPerson
     */
    public String getContactPerson() {
        return ContactPerson;
    }

    /**
     *
     * @param ContactPerson
     * The ContactPerson
     */
    public void setContactPerson(String ContactPerson) {
        this.ContactPerson = ContactPerson;
    }

    /**
     *
     * @return
     * The CompanyURL
     */
    public String getCompanyURL() {
        return CompanyURL;
    }

    /**
     *
     * @param CompanyURL
     * The CompanyURL
     */
    public void setCompanyURL(String CompanyURL) {
        this.CompanyURL = CompanyURL;
    }

    /**
     *
     * @return
     * The Venue
     */
    public String getVenue() {
        return Venue;
    }

    /**
     *
     * @param Venue
     * The Venue
     */
    public void setVenue(String Venue) {
        this.Venue = Venue;
    }

    /**
     *
     * @return
     * The Section
     */
    public String getSection() {
        return Section;
    }

    /**
     *
     * @param Section
     * The Section
     */
    public void setSection(String Section) {
        this.Section = Section;
    }

    /**
     *
     * @return
     * The AttachBrochure
     */
    public String getAttachBrochure() {
        return AttachBrochure;
    }

    /**
     *
     * @param AttachBrochure
     * The AttachBrochure
     */
    public void setAttachBrochure(String AttachBrochure) {
        this.AttachBrochure = AttachBrochure;
    }

    /**
     *
     * @return
     * The PostDate
     */
    public String getPostDate() {
        return PostDate;
    }

    /**
     *
     * @param PostDate
     * The PostDate
     */
    public void setPostDate(String PostDate) {
        this.PostDate = PostDate;
    }

    /**
     *
     * @return
     * The StartDate
     */
    public String getStartDate() {
        return StartDate;
    }

    /**
     *
     * @param StartDate
     * The StartDate
     */
    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    /**
     *
     * @return
     * The EndDate
     */
    public String getEndDate() {
        return EndDate;
    }

    /**
     *
     * @param EndDate
     * The EndDate
     */
    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    /**
     *
     * @return
     * The StartTime
     */
    public String getStartTime() {
        return StartTime;
    }

    /**
     *
     * @param StartTime
     * The StartTime
     */
    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    /**
     *
     * @return
     * The EndTime
     */
    public String getEndTime() {
        return EndTime;
    }

    /**
     *
     * @param EndTime
     * The EndTime
     */
    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    /**
     *
     * @return
     * The Duration
     */
    public String getDuration() {
        return Duration;
    }

    /**
     *
     * @param Duration
     * The Duration
     */
    public void setDuration(String Duration) {
        this.Duration = Duration;
    }

    /**
     *
     * @return
     * The EveryDay
     */
    public Boolean getEveryDay() {
        return EveryDay;
    }

    /**
     *
     * @param EveryDay
     * The EveryDay
     */
    public void setEveryDay(Boolean EveryDay) {
        this.EveryDay = EveryDay;
    }

    /**
     *
     * @return
     * The CourseStatus
     */
    public Boolean getCourseStatus() {
        return CourseStatus;
    }

    /**
     *
     * @param CourseStatus
     * The CourseStatus
     */
    public void setCourseStatus(Boolean CourseStatus) {
        this.CourseStatus = CourseStatus;
    }

    /**
     *
     * @return
     * The Online
     */
    public Boolean getOnline() {
        return Online;
    }

    /**
     *
     * @param Online
     * The Online
     */
    public void setOnline(Boolean Online) {
        this.Online = Online;
    }

    /**
     *
     * @return
     * The FastTrack
     */
    public Boolean getFastTrack() {
        return FastTrack;
    }

    /**
     *
     * @param FastTrack
     * The FastTrack
     */
    public void setFastTrack(Boolean FastTrack) {
        this.FastTrack = FastTrack;
    }

    /**
     *
     * @return
     * The Weekend
     */
    public Boolean getWeekend() {
        return Weekend;
    }

    /**
     *
     * @param Weekend
     * The Weekend
     */
    public void setWeekend(Boolean Weekend) {
        this.Weekend = Weekend;
    }

    /**
     *
     * @return
     * The IsOnline
     */
    public Boolean getIsOnline() {
        return IsOnline;
    }

    /**
     *
     * @param IsOnline
     * The IsOnline
     */
    public void setIsOnline(Boolean IsOnline) {
        this.IsOnline = IsOnline;
    }

    /**
     *
     * @return
     * The Normaltrack
     */
    public Boolean getNormaltrack() {
        return Normaltrack;
    }

    /**
     *
     * @param Normaltrack
     * The Normaltrack
     */
    public void setNormaltrack(Boolean Normaltrack) {
        this.Normaltrack = Normaltrack;
    }

    /**
     *
     * @return
     * The BasicInformation
     */
    public Boolean getBasicInformation() {
        return BasicInformation;
    }

    /**
     *
     * @param BasicInformation
     * The BasicInformation
     */
    public void setBasicInformation(Boolean BasicInformation) {
        this.BasicInformation = BasicInformation;
    }

    /**
     *
     * @return
     * The AfterDiscountedfee
     */
    public Integer getAfterDiscountedfee() {
        return AfterDiscountedfee;
    }

    /**
     *
     * @param AfterDiscountedfee
     * The AfterDiscountedfee
     */
    public void setAfterDiscountedfee(Integer AfterDiscountedfee) {
        this.AfterDiscountedfee = AfterDiscountedfee;
    }

    /**
     *
     * @return
     * The ProjectWork
     */
    public Boolean getProjectWork() {
        return ProjectWork;
    }

    /**
     *
     * @param ProjectWork
     * The ProjectWork
     */
    public void setProjectWork(Boolean ProjectWork) {
        this.ProjectWork = ProjectWork;
    }

    /**
     *
     * @return
     * The EarlyBirdDiscountDate
     */
    public String getEarlyBirdDiscountDate() {
        return EarlyBirdDiscountDate;
    }

    /**
     *
     * @param EarlyBirdDiscountDate
     * The EarlyBirdDiscountDate
     */
    public void setEarlyBirdDiscountDate(String EarlyBirdDiscountDate) {
        this.EarlyBirdDiscountDate = EarlyBirdDiscountDate;
    }

    /**
     *
     * @return
     * The BatchSize
     */
    public Integer getBatchSize() {
        return BatchSize;
    }

    /**
     *
     * @param BatchSize
     * The BatchSize
     */
    public void setBatchSize(Integer BatchSize) {
        this.BatchSize = BatchSize;
    }

    /**
     *
     * @return
     * The Status
     */
    public String getStatus() {
        return Status;
    }

    /**
     *
     * @param Status
     * The Status
     */
    public void setStatus(String Status) {
        this.Status = Status;
    }

    /**
     *
     * @return
     * The Resume
     */
    public String getResume() {
        return Resume;
    }

    /**
     *
     * @param Resume
     * The Resume
     */
    public void setResume(String Resume) {
        this.Resume = Resume;
    }

    /**
     *
     * @return
     * The GroupCount
     */
    public Integer getGroupCount() {
        return GroupCount;
    }

    /**
     *
     * @param GroupCount
     * The GroupCount
     */
    public void setGroupCount(Integer GroupCount) {
        this.GroupCount = GroupCount;
    }

    /**
     *
     * @return
     * The GroupValue
     */
    public String getGroupValue() {
        return GroupValue;
    }

    /**
     *
     * @param GroupValue
     * The GroupValue
     */
    public void setGroupValue(String GroupValue) {
        this.GroupValue = GroupValue;
    }

    /**
     *
     * @return
     * The PostedBy
     */
    public String getPostedBy() {
        return PostedBy;
    }

    /**
     *
     * @param PostedBy
     * The PostedBy
     */
    public void setPostedBy(String PostedBy) {
        this.PostedBy = PostedBy;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}