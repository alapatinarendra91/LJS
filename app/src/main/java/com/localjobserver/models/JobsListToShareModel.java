 package com.localjobserver.models;

 import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobsListToShareModel {

    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("JobNo")
    @Expose
    private String JobNo;
    @SerializedName("JobReference")
    @Expose
    private String JobReference;
    @SerializedName("JobTitle")
    @Expose
    private String JobTitle;
    @SerializedName("JobSummary")
    @Expose
    private String JobSummary;
    @SerializedName("JobDescription")
    @Expose
    private String JobDescription;
    @SerializedName("KeySkills")
    @Expose
    private String KeySkills;
    @SerializedName("StdKeySkills")
    @Expose
    private String StdKeySkills;
    @SerializedName("MinSal")
    @Expose
    private String MinSal;
    @SerializedName("MaxSal")
    @Expose
    private String MaxSal;
    @SerializedName("CompanyProfile")
    @Expose
    private String CompanyProfile;
    @SerializedName("FunctionalArea")
    @Expose
    private String FunctionalArea;
    @SerializedName("Industry")
    @Expose
    private String Industry;
    @SerializedName("ExpMin")
    @Expose
    private String ExpMin;
    @SerializedName("ExpMax")
    @Expose
    private String ExpMax;
    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Qualification")
    @Expose
    private String Qualification;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("Phone")
    @Expose
    private String Phone;
    @SerializedName("ContactNo_Landline")
    @Expose
    private String ContactNoLandline;
    @SerializedName("PostDate")
    @Expose
    private String PostDate;
    @SerializedName("Role")
    @Expose
    private String Role;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;
    @SerializedName("StdCurCompany")
    @Expose
    private String StdCurCompany;
    @SerializedName("ContactPerson")
    @Expose
    private String ContactPerson;
    @SerializedName("Salary")
    @Expose
    private String Salary;
    @SerializedName("LocId")
    @Expose
    private String LocId;
    @SerializedName("InId")
    @Expose
    private String InId;
    @SerializedName("FId")
    @Expose
    private String FId;
    @SerializedName("RId")
    @Expose
    private String RId;
    @SerializedName("JTId")
    @Expose
    private String JTId;
    @SerializedName("PbId")
    @Expose
    private String PbId;
    @SerializedName("PostedType")
    @Expose
    private String PostedType;
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("ViewsCount")
    @Expose
    private String ViewsCount;
    @SerializedName("AppliedCount")
    @Expose
    private String AppliedCount;
    @SerializedName("EndClient")
    @Expose
    private String EndClient;
    @SerializedName("MatchedScore")
    @Expose
    private String MatchedScore;
    @SerializedName("NoticePeriod")
    @Expose
    private String NoticePeriod;
    @SerializedName("WalkinDate")
    @Expose
    private String WalkinDate;
    @SerializedName("SearchQuery")
    @Expose
    private String SearchQuery;
    @SerializedName("Resume")
    @Expose
    private String Resume;
    @SerializedName("jobPositions")
    @Expose
    private String jobPositions;

    /**
     *
     * @return
     * The _id
     */
    public String getId() {
        return _id;
    }

    /**
     *
     * @param _id
     * The _id
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     *
     * @return
     * The JobNo
     */
    public String getJobNo() {
        return JobNo;
    }

    /**
     *
     * @param JobNo
     * The JobNo
     */
    public void setJobNo(String JobNo) {
        this.JobNo = JobNo;
    }

    /**
     *
     * @return
     * The JobReference
     */
    public String getJobReference() {
        return JobReference;
    }

    /**
     *
     * @param JobReference
     * The JobReference
     */
    public void setJobReference(String JobReference) {
        this.JobReference = JobReference;
    }

    /**
     *
     * @return
     * The JobTitle
     */
    public String getJobTitle() {
        return JobTitle;
    }

    /**
     *
     * @param JobTitle
     * The JobTitle
     */
    public void setJobTitle(String JobTitle) {
        this.JobTitle = JobTitle;
    }

    /**
     *
     * @return
     * The JobSummary
     */
    public String getJobSummary() {
        return JobSummary;
    }

    /**
     *
     * @param JobSummary
     * The JobSummary
     */
    public void setJobSummary(String JobSummary) {
        this.JobSummary = JobSummary;
    }

    /**
     *
     * @return
     * The JobDescription
     */
    public String getJobDescription() {
        return JobDescription;
    }

    /**
     *
     * @param JobDescription
     * The JobDescription
     */
    public void setJobDescription(String JobDescription) {
        this.JobDescription = JobDescription;
    }

    /**
     *
     * @return
     * The KeySkills
     */
    public String getKeySkills() {
        return KeySkills;
    }

    /**
     *
     * @param KeySkills
     * The KeySkills
     */
    public void setKeySkills(String KeySkills) {
        this.KeySkills = KeySkills;
    }

    /**
     *
     * @return
     * The StdKeySkills
     */
    public String getStdKeySkills() {
        return StdKeySkills;
    }

    /**
     *
     * @param StdKeySkills
     * The StdKeySkills
     */
    public void setStdKeySkills(String StdKeySkills) {
        this.StdKeySkills = StdKeySkills;
    }

    /**
     *
     * @return
     * The MinSal
     */
    public String getMinSal() {
        return MinSal;
    }

    /**
     *
     * @param MinSal
     * The MinSal
     */
    public void setMinSal(String MinSal) {
        this.MinSal = MinSal;
    }

    /**
     *
     * @return
     * The MaxSal
     */
    public String getMaxSal() {
        return MaxSal;
    }

    /**
     *
     * @param MaxSal
     * The MaxSal
     */
    public void setMaxSal(String MaxSal) {
        this.MaxSal = MaxSal;
    }

    /**
     *
     * @return
     * The CompanyProfile
     */
    public String getCompanyProfile() {
        return CompanyProfile;
    }

    /**
     *
     * @param CompanyProfile
     * The CompanyProfile
     */
    public void setCompanyProfile(String CompanyProfile) {
        this.CompanyProfile = CompanyProfile;
    }

    /**
     *
     * @return
     * The FunctionalArea
     */
    public String getFunctionalArea() {
        return FunctionalArea;
    }

    /**
     *
     * @param FunctionalArea
     * The FunctionalArea
     */
    public void setFunctionalArea(String FunctionalArea) {
        this.FunctionalArea = FunctionalArea;
    }

    /**
     *
     * @return
     * The Industry
     */
    public String getIndustry() {
        return Industry;
    }

    /**
     *
     * @param Industry
     * The Industry
     */
    public void setIndustry(String Industry) {
        this.Industry = Industry;
    }

    /**
     *
     * @return
     * The ExpMin
     */
    public String getExpMin() {
        return ExpMin;
    }

    /**
     *
     * @param ExpMin
     * The ExpMin
     */
    public void setExpMin(String ExpMin) {
        this.ExpMin = ExpMin;
    }

    /**
     *
     * @return
     * The ExpMax
     */
    public String getExpMax() {
        return ExpMax;
    }

    /**
     *
     * @param ExpMax
     * The ExpMax
     */
    public void setExpMax(String ExpMax) {
        this.ExpMax = ExpMax;
    }

    /**
     *
     * @return
     * The Location
     */
    public String getLocation() {
        return Location;
    }

    /**
     *
     * @param Location
     * The Location
     */
    public void setLocation(String Location) {
        this.Location = Location;
    }

    /**
     *
     * @return
     * The Qualification
     */
    public String getQualification() {
        return Qualification;
    }

    /**
     *
     * @param Qualification
     * The Qualification
     */
    public void setQualification(String Qualification) {
        this.Qualification = Qualification;
    }

    /**
     *
     * @return
     * The Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email
     * The Email
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return
     * The Phone
     */
    public String getPhone() {
        return Phone;
    }

    /**
     *
     * @param Phone
     * The Phone
     */
    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    /**
     *
     * @return
     * The ContactNoLandline
     */
    public String getContactNoLandline() {
        return ContactNoLandline;
    }

    /**
     *
     * @param ContactNoLandline
     * The ContactNo_Landline
     */
    public void setContactNoLandline(String ContactNoLandline) {
        this.ContactNoLandline = ContactNoLandline;
    }

    /**
     *
     * @return
     * The PostDate
     */
    public String getPostDate() {
        return PostDate;
    }

    /**
     *
     * @param PostDate
     * The PostDate
     */
    public void setPostDate(String PostDate) {
        this.PostDate = PostDate;
    }

    /**
     *
     * @return
     * The Role
     */
    public String getRole() {
        return Role;
    }

    /**
     *
     * @param Role
     * The Role
     */
    public void setRole(String Role) {
        this.Role = Role;
    }

    /**
     *
     * @return
     * The Address
     */
    public String getAddress() {
        return Address;
    }

    /**
     *
     * @param Address
     * The Address
     */
    public void setAddress(String Address) {
        this.Address = Address;
    }

    /**
     *
     * @return
     * The CompanyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     *
     * @param CompanyName
     * The CompanyName
     */
    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    /**
     *
     * @return
     * The StdCurCompany
     */
    public String getStdCurCompany() {
        return StdCurCompany;
    }

    /**
     *
     * @param StdCurCompany
     * The StdCurCompany
     */
    public void setStdCurCompany(String StdCurCompany) {
        this.StdCurCompany = StdCurCompany;
    }

    /**
     *
     * @return
     * The ContactPerson
     */
    public String getContactPerson() {
        return ContactPerson;
    }

    /**
     *
     * @param ContactPerson
     * The ContactPerson
     */
    public void setContactPerson(String ContactPerson) {
        this.ContactPerson = ContactPerson;
    }

    /**
     *
     * @return
     * The Salary
     */
    public String getSalary() {
        return Salary;
    }

    /**
     *
     * @param Salary
     * The Salary
     */
    public void setSalary(String Salary) {
        this.Salary = Salary;
    }

    /**
     *
     * @return
     * The LocId
     */
    public String getLocId() {
        return LocId;
    }

    /**
     *
     * @param LocId
     * The LocId
     */
    public void setLocId(String LocId) {
        this.LocId = LocId;
    }

    /**
     *
     * @return
     * The InId
     */
    public String getInId() {
        return InId;
    }

    /**
     *
     * @param InId
     * The InId
     */
    public void setInId(String InId) {
        this.InId = InId;
    }

    /**
     *
     * @return
     * The FId
     */
    public String getFId() {
        return FId;
    }

    /**
     *
     * @param FId
     * The FId
     */
    public void setFId(String FId) {
        this.FId = FId;
    }

    /**
     *
     * @return
     * The RId
     */
    public String getRId() {
        return RId;
    }

    /**
     *
     * @param RId
     * The RId
     */
    public void setRId(String RId) {
        this.RId = RId;
    }

    /**
     *
     * @return
     * The JTId
     */
    public String getJTId() {
        return JTId;
    }

    /**
     *
     * @param JTId
     * The JTId
     */
    public void setJTId(String JTId) {
        this.JTId = JTId;
    }

    /**
     *
     * @return
     * The PbId
     */
    public String getPbId() {
        return PbId;
    }

    /**
     *
     * @param PbId
     * The PbId
     */
    public void setPbId(String PbId) {
        this.PbId = PbId;
    }

    /**
     *
     * @return
     * The PostedType
     */
    public String getPostedType() {
        return PostedType;
    }

    /**
     *
     * @param PostedType
     * The PostedType
     */
    public void setPostedType(String PostedType) {
        this.PostedType = PostedType;
    }

    /**
     *
     * @return
     * The Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     *
     * @param Designation
     * The Designation
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     *
     * @return
     * The ViewsCount
     */
    public String getViewsCount() {
        return ViewsCount;
    }

    /**
     *
     * @param ViewsCount
     * The ViewsCount
     */
    public void setViewsCount(String ViewsCount) {
        this.ViewsCount = ViewsCount;
    }

    /**
     *
     * @return
     * The AppliedCount
     */
    public String getAppliedCount() {
        return AppliedCount;
    }

    /**
     *
     * @param AppliedCount
     * The AppliedCount
     */
    public void setAppliedCount(String AppliedCount) {
        this.AppliedCount = AppliedCount;
    }

    /**
     *
     * @return
     * The EndClient
     */
    public String getEndClient() {
        return EndClient;
    }

    /**
     *
     * @param EndClient
     * The EndClient
     */
    public void setEndClient(String EndClient) {
        this.EndClient = EndClient;
    }

    /**
     *
     * @return
     * The MatchedScore
     */
    public String getMatchedScore() {
        return MatchedScore;
    }

    /**
     *
     * @param MatchedScore
     * The MatchedScore
     */
    public void setMatchedScore(String MatchedScore) {
        this.MatchedScore = MatchedScore;
    }

    /**
     *
     * @return
     * The NoticePeriod
     */
    public String getNoticePeriod() {
        return NoticePeriod;
    }

    /**
     *
     * @param NoticePeriod
     * The NoticePeriod
     */
    public void setNoticePeriod(String NoticePeriod) {
        this.NoticePeriod = NoticePeriod;
    }

    /**
     *
     * @return
     * The WalkinDate
     */
    public String getWalkinDate() {
        return WalkinDate;
    }

    /**
     *
     * @param WalkinDate
     * The WalkinDate
     */
    public void setWalkinDate(String WalkinDate) {
        this.WalkinDate = WalkinDate;
    }

    /**
     *
     * @return
     * The SearchQuery
     */
    public String getSearchQuery() {
        return SearchQuery;
    }

    /**
     *
     * @param SearchQuery
     * The SearchQuery
     */
    public void setSearchQuery(String SearchQuery) {
        this.SearchQuery = SearchQuery;
    }

    /**
     *
     * @return
     * The Resume
     */
    public String getResume() {
        return Resume;
    }

    /**
     *
     * @param Resume
     * The Resume
     */
    public void setResume(String Resume) {
        this.Resume = Resume;
    }

    /**
     *
     * @return
     * The jobPositions
     */
    public String getJobPositions() {
        return jobPositions;
    }

    /**
     *
     * @param jobPositions
     * The jobPositions
     */
    public void setJobPositions(String jobPositions) {
        this.jobPositions = jobPositions;
    }

}