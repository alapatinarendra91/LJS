package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeyskillsModel {

    @SerializedName("UserKeySkills")
    @Expose
    private String UserKeySkills;
    @SerializedName("StdKeySkills")
    @Expose
    private String StdKeySkills;

    /**
     *
     * @return
     * The UserKeySkills
     */
    public String getUserKeySkills() {
        return UserKeySkills;
    }

    /**
     *
     * @param UserKeySkills
     * The UserKeySkills
     */
    public void setUserKeySkills(String UserKeySkills) {
        this.UserKeySkills = UserKeySkills;
    }

    /**
     *
     * @return
     * The StdKeySkills
     */
    public String getStdKeySkills() {
        return StdKeySkills;
    }

    /**
     *
     * @param StdKeySkills
     * The StdKeySkills
     */
    public void setStdKeySkills(String StdKeySkills) {
        this.StdKeySkills = StdKeySkills;
    }

}