package com.localjobserver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FresharLocalCourses {

    @SerializedName("course")
    @Expose
    private String course;
    @SerializedName("count")
    @Expose
    private String count;

    /**
     *
     * @return
     * The course
     */
    public String getCourse() {
        return course;
    }

    /**
     *
     * @param course
     * The course
     */
    public void setCourse(String course) {
        this.course = course;
    }

    /**
     *
     * @return
     * The count
     */
    public String getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(String count) {
        this.count = count;
    }

}
