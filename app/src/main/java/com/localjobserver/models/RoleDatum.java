package com.localjobserver.models;

/**
 * Created by dell on 30-07-2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoleDatum {

    @SerializedName("RId")
    @Expose
    private Integer rId;
    @SerializedName("InId")
    @Expose
    private Integer inId;
    @SerializedName("RoleName")
    @Expose
    private String roleName;
    @SerializedName("roleCount")
    @Expose
    private Integer roleCount;
    @SerializedName("SubRoleName")
    @Expose
    private String subRoleName;

    /**
     *
     * @return
     * The rId
     */
    public Integer getRId() {
        return rId;
    }

    /**
     *
     * @param rId
     * The RId
     */
    public void setRId(Integer rId) {
        this.rId = rId;
    }

    /**
     *
     * @return
     * The inId
     */
    public Integer getInId() {
        return inId;
    }

    /**
     *
     * @param inId
     * The InId
     */
    public void setInId(Integer inId) {
        this.inId = inId;
    }

    /**
     *
     * @return
     * The roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     *
     * @param roleName
     * The RoleName
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     *
     * @return
     * The roleCount
     */
    public Integer getRoleCount() {
        return roleCount;
    }

    /**
     *
     * @param roleCount
     * The roleCount
     */
    public void setRoleCount(Integer roleCount) {
        this.roleCount = roleCount;
    }

    /**
     *
     * @return
     * The subRoleName
     */
    public String getSubRoleName() {
        return subRoleName;
    }

    /**
     *
     * @param subRoleName
     * The SubRoleName
     */
    public void setSubRoleName(String subRoleName) {
        this.subRoleName = subRoleName;
    }

}