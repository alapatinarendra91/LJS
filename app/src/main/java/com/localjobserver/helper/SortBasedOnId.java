package com.localjobserver.helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by siva on 17/10/15.
 */
public class SortBasedOnId implements Comparator<JSONObject> {
    /*
    * (non-Javadoc)
    *
    * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
    * lhs- 1st message in the form of json object. rhs- 2nd message in the form
    * of json object.
    */
    @Override
    public int compare(JSONObject lhs, JSONObject rhs) {
        try {
            return lhs.getInt("InId") > rhs.getInt("InId") ? 1 : (lhs
                    .getInt("InId") < rhs.getInt("InId") ? -1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;

    }
}