package com.localjobserver.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import co.talentzing.R;
import com.localjobserver.models.JobsData;
import com.localjobserver.models.RecommendedCourses;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.WebViewActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by admin on 28-04-2015.
 */
public class SocialHelper {

    public static final String OAUTH_CALLBACK_HOST = "litestcalback";
    final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory.getInstance().createLinkedInOAuthService(Config.LINKEDIN_CONSUMER_KEY, Config.LINKEDIN_CONSUMER_SECRET);
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(Config.LINKEDIN_CONSUMER_KEY, Config.LINKEDIN_CONSUMER_SECRET);
    LinkedInRequestToken liToken;
    LinkedInApiClient client;
    public static LinkedInAccessToken linkedInAccessToken = null;
    private ProgressDialog pDialog;

    public static Twitter twitter;
    public static RequestToken requestToken;

    private static SharedPreferences mSharedPreferences;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    public String oAuthVerifier = null;

    public static final int WEBVIEW_REQUEST_CODE = 100;

    //    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    private FragmentActivity activity;
    private Context context;
    public ArrayList<JobsData> jobList;
    private ArrayList<RecommendedCourses> courcesList;

    public SocialHelper(Context context, FragmentActivity activity, ArrayList<JobsData> jobList,ArrayList<RecommendedCourses> courcesList){
        this.context=context;
        this.jobList=jobList;
        this.courcesList=courcesList;
        this.activity=activity;
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        initTwitterConfigs();
    }

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = activity.getString(R.string.twitter_consumer_key);
        consumerSecret = activity.getString(R.string.twitter_consumer_secret);
        callbackUrl = activity.getString(R.string.twitter_callback);
        oAuthVerifier = activity.getString(R.string.twitter_oauth_verifier);
    }


    public void publishFeedDialog(int position,String moduleType) {
        Bundle params = new Bundle();
        String str = String.format(Html.fromHtml("").toString());
        if (moduleType.equalsIgnoreCase("seeker")){
            params.putString("name", ((JobsData)jobList.get(position)).getJobTitle()+"");
            params.putString("caption", ((JobsData)jobList.get(position)).getCompanyProfile());
            params.putString("caption", ((JobsData)jobList.get(position)).getCompanyProfile());
            params.putString("description", Html.fromHtml(((JobsData)jobList.get(position)).getJobDescription()).toString());
            params.putString("link", "https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+((JobsData)jobList.get(position)).get_id());
//            params.putString("link", "http://www.kelltontech.com");
        }else  if (moduleType.equalsIgnoreCase("fresher")){
            params.putString("name", ((RecommendedCourses)courcesList.get(position)).getCourseName());
            params.putString("caption", ((RecommendedCourses)courcesList.get(position)).getStatus());
            params.putString("description", ((RecommendedCourses)courcesList.get(position)).getCourseDescription());
            params.putString("link", "https://talentzing.com/JobSeeker/searchjobsinfo.aspx?jid="+((RecommendedCourses)courcesList.get(position)).get_id());
        }


        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(activity,Session.getActiveSession(),params))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            Toast.makeText(activity,"Posted Successfully" , Toast.LENGTH_SHORT).show();
                            if (postId != null) {
//                                Toast.makeText(activity,"Posted Successfully" , Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
//                                Toast.makeText(activity, "Publish cancelled", Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(activity,"Publish cancelled",Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(activity,"Error posting Job",Toast.LENGTH_SHORT).show();
                        }
                    }

                })
                .build();
        feedDialog.show();
    }


    public void saveTwitterInfo(AccessToken accessToken , int position) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);
            String username = user.getName();
			/* Storing oAuth tokens to shared preferences */
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();

            StringBuilder builder = new StringBuilder();
            builder.append(((JobsData)jobList.get(position))).append("\n").append(((JobsData)jobList.get(position)).getCompanyProfile()).append("\n").append(((JobsData)jobList.get(position)).getLocation()).append("\n").append(((JobsData) jobList.get(position)).getMaxSal());
            new updateTwitterStatus().execute(builder.toString());

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo("com.localjobserver", PackageManager.GET_SIGNATURES); //Your            package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", "Check....." + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void loginToTwitter(final int position) {
        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (!isLoggedIn) {

            new Thread(new Runnable() {
                @Override
                public void run() {


                    final ConfigurationBuilder builder = new ConfigurationBuilder();
                    builder.setOAuthConsumerKey(consumerKey);
                    builder.setOAuthConsumerSecret(consumerSecret);

                    final Configuration configuration = builder.build();
                    final TwitterFactory factory = new TwitterFactory(configuration);
                    twitter = factory.getInstance();

                    try {
                        requestToken = twitter.getOAuthRequestToken(callbackUrl);

                        /**
                         *  Loading twitter login page on webview for authorization
                         *  Once authorized, results are received at onActivityResult
                         *  */
                        final Intent intent = new Intent(activity, WebViewActivity.class);
                        intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                        intent.putExtra("position", position);
                        activity.startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        else {
            StringBuilder builder = new StringBuilder();
            builder.append(((JobsData)jobList.get(position)).getJobTitle()).append("\n").append(((JobsData)jobList.get(position)).getCompanyProfile()).append("\n").append(((JobsData)jobList.get(position)).getLocation()).append("\n").append(((JobsData)jobList.get(position)).getMaxSal());
            new updateTwitterStatus().execute(builder.toString());
        }
    }


    class updateTwitterStatus extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(consumerKey);
                builder.setOAuthConsumerSecret(consumerSecret);

                // Access Token
                String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
                // Access Token Secret
                String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);
                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
			/* Dismiss the progress dialog after sharing */
            pDialog.dismiss();
            Toast.makeText(activity, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
        }

    }

//    public void linkedInLogin(final JobsData jobListData) {
//        ProgressDialog progressDialog = new ProgressDialog(activity);
//
//        LinkedinDialog d = new LinkedinDialog(activity, progressDialog);
//        d.show();
//
//        // set call back listener to get oauth_verifier value
//        d.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
//            @Override
//            public void onVerify(String verifier) {
//                try {
//                    Log.i("LinkedinSample", "verifier: " + verifier);
//
//                    linkedInAccessToken = LinkedinDialog.oAuthService
//                            .getOAuthAccessToken(LinkedinDialog.liToken,
//                                    verifier);
//                    LinkedinDialog.factory.createLinkedInApiClient(linkedInAccessToken);
//                    client = factory.createLinkedInApiClient(linkedInAccessToken);
//                    Log.i("LinkedinSample",
//                            "ln_access_token: " + linkedInAccessToken.getToken());
//                    Log.i("LinkedinSample",
//                            "ln_access_token: " + linkedInAccessToken.getTokenSecret());
//                    Person p = client.getProfileForCurrentUser();
///*
//                    Fragment newFragment =  LinkedInPostFragment.newInstance(p.getFirstName(), jobListData);
//                    FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    ft.add(android.R.id.content, newFragment).commit();*/
//                } catch (Exception e) {
//                    Log.i("LinkedinSample", "error to get verifier");
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        // set progress dialog
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(true);
//        progressDialog.show();
//    }
}
