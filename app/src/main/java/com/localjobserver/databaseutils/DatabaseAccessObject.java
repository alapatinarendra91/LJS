package com.localjobserver.databaseutils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.localjobserver.chat.ChatMessage;
import com.localjobserver.models.FresherRegistrationModel;
import com.localjobserver.models.RecuiterObject;
import com.localjobserver.models.SetupData;
import com.localjobserver.models.SubUserModel;
import com.localjobserver.models.TrainerRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class DatabaseAccessObject {

    public static List<RecuiterObject> getRecuiterRegistrationDetails(String query,Context ctx)
    {
        List<RecuiterObject> recuiterdetails=new ArrayList<RecuiterObject>();
        Cursor cursor=null;
        DatabaseHelper dbhelper=new DatabaseHelper(ctx);
        try
        {
            RecuiterObject  recregobj=null;
            dbhelper.openDataBase();
            cursor=dbhelper.getdata(query);
            if(cursor!=null&&cursor.moveToFirst())
            {
                do {
                    recregobj=new RecuiterObject();
                    recregobj.setEmployType(cursor.getString(0));
                    recregobj.setFirstName(cursor.getString(1));
                    recregobj.setLastName(cursor.getString(2));
                    recregobj.setCompanyName(cursor.getString(3));
                    recregobj.setEmail(cursor.getString(4));
                    recregobj.setCompanyUrl(cursor.getString(5));
                    recregobj.setPassword(cursor.getString(6));
                    recregobj.setConfirmPassword(cursor.getString(7));
                    recregobj.setDesignation(cursor.getString(8));
                    recregobj.setContactNo(cursor.getString(9));
                    recregobj.setContactNoLandline(cursor.getString(10));
                    recregobj.setIndustryType(cursor.getString(11));
                    recregobj.setAddress(cursor.getString(12));
                    recregobj.setCompanyProfile(cursor.getString(13));
                    recregobj.setCompanyLogo(cursor.getBlob(14));
//                    recregobj.setProfilealerts(cursor.getString(15));
                    recuiterdetails.add(recregobj);
                    recregobj = null;
                    }while(cursor.moveToNext());
                }


        }
        catch(Exception e)
        {
            Log.e("DAO",e.getMessage());
        }
        finally
        {
            if(cursor!=null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return recuiterdetails;
    }

    public static List<SubUserModel> getSubuserrRegistrationDetails(String query,Context ctx)
    {
        List<SubUserModel> recuiterdetails=new ArrayList<SubUserModel>();
        Cursor cursor=null;
        DatabaseHelper dbhelper=new DatabaseHelper(ctx);
        try
        {
            SubUserModel  recregobj=null;
            dbhelper.openDataBase();
            cursor=dbhelper.getdata(query);
            if(cursor!=null&&cursor.moveToFirst())
            {
                do {
                    recregobj=new SubUserModel();
                    recregobj.setUserName(cursor.getString(3));
                    recregobj.setSubUserEmailId(cursor.getString(1));
                    recregobj.setCompanyEmailId(cursor.getString(0));
                    recregobj.setContactNo(cursor.getString(4));
                    recuiterdetails.add(recregobj);
                    recregobj = null;
                }while(cursor.moveToNext());
            }


        }
        catch(Exception e)
        {
            Log.e("DAO",e.getMessage());
        }
        finally
        {
            if(cursor!=null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return recuiterdetails;
    }

    public static FresherRegistrationModel getFresherRegistrationDetails(String query,Context ctx)
    {
        Cursor cursor=null;
        DatabaseHelper dbhelper=new DatabaseHelper(ctx);
        FresherRegistrationModel mDataModel = null;
        try
        {
            RecuiterObject  recregobj=null;
            dbhelper.openDataBase();
            cursor=dbhelper.getdata(query);

            if(cursor!=null&&cursor.moveToFirst())
            {
                do {
                    mDataModel = new FresherRegistrationModel();
                    mDataModel.setFirstName(cursor.getString(1));
                    mDataModel.setLastName(cursor.getString(2));
                    mDataModel.setEducation(cursor.getString(9));
                    mDataModel.setPassword(cursor.getString(3));
                    mDataModel.setConfirmPassword(cursor.getString(4));
                    mDataModel.setEmail(cursor.getString(5));
                    mDataModel.setContactNo(cursor.getString(6));
                    mDataModel.setLandLine(cursor.getString(7));
                    mDataModel.setUpdateOn(cursor.getString(10));
                    mDataModel.setPhoto("");
                    mDataModel.setGender(cursor.getString(15));
                    mDataModel.setDateOfBirth(cursor.getString(10));
                    mDataModel.setLocation(cursor.getString(13));
                    mDataModel.setKeySkills(cursor.getString(11));
                }while(cursor.moveToNext());
            }


        }
        catch(Exception e)
        {
            Log.e("DAO",e.getMessage());
        }
        finally
        {
            if(cursor!=null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return mDataModel;
    }

    public static List<TrainerRegistrationModel> getTrainerProfile(String query,Context ctx)
    {
        Cursor cursor=null;
        DatabaseHelper dbhelper=new DatabaseHelper(ctx);
        List<TrainerRegistrationModel> Trainerprofile = new ArrayList();
        TrainerRegistrationModel mDataModel = null;
        try
        {
            RecuiterObject  recregobj=null;
            dbhelper.openDataBase();
            cursor=dbhelper.getdata(query);

            if(cursor!=null&&cursor.moveToFirst())
            {
                do {
                    mDataModel = new TrainerRegistrationModel();
                    mDataModel.setFirstName(cursor.getString(1));
                    mDataModel.setLastName(cursor.getString(2));
                    mDataModel.setPassword(cursor.getString(3));
                    mDataModel.setConfirmPassword(cursor.getString(4));
                    mDataModel.setInstituteName(cursor.getString(5));
                    mDataModel.setEmail(cursor.getString(6));
                    mDataModel.setContactNo(cursor.getString(7));
                    mDataModel.setContactNoLandline(cursor.getString(8));
                    mDataModel.setLocation(cursor.getString(9));
                    mDataModel.setAddress(cursor.getString(10));
//                    mDataModel.setPhoto(cursor.getString(11));
                    mDataModel.setPhotoString(cursor.getString(12));
                    mDataModel.setUpdateOn(cursor.getString(13));
                    mDataModel.setCoursesOffer(cursor.getString(14));
//                    mDataModel.EmailVerified(cursor.getString(15));
                    mDataModel.setStdCourseName(cursor.getString(16));
//                    mDataModel.setIsOnline(cursor.getString(17));
//                    mDataModel.setDTId(cursor.getString(18));
                    mDataModel.setDTType(cursor.getString(19));
//                    mDataModel.setTrainerStatus(cursor.getString(20));
                    mDataModel.setLocality(cursor.getString(21));
                    Trainerprofile.add(mDataModel);
                }while(cursor.moveToNext());
            }


        }
        catch(Exception e)
        {
            Log.e("DAO",e.getMessage());
        }
        finally
        {
            if(cursor!=null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return Trainerprofile;
    }

//    public static List<SetupData> getJobSeekerDetails(String query, Context ctx) {
//        List<SetupData> seekerdetails = new ArrayList<SetupData>();
//        Cursor cursor = null;
//        DatabaseHelper dbhelper = new DatabaseHelper(ctx);
//        try {
//            SetupData seekerobj = null;
//            dbhelper.openDataBase();
//            cursor = dbhelper.getdata(query);
//            if (cursor != null && cursor.moveToFirst()) {
//                do {
//
//
//                    seekerobj = new SetupData();
//                    seekerobj.setJobName(cursor.getString(0));
//                    seekerobj.setJobid(cursor.getString(1));
//                    seekerobj.setJobLocation(cursor.getString(2));
//                    seekerobj.setJobRole(cursor.getString(3));
//                    seekerobj.setJobExp(cursor.getString(4));
//                    seekerobj.setJobCTC(cursor.getString(5));
//                    seekerobj.setJobDec(cursor.getString(6));
//                    seekerobj.setJobNoticePeriod(cursor.getString(7));
//                    seekerobj.setJobKeySkills(cursor.getString(8));
//                    seekerobj.setJobEndClient(cursor.getString(9));
//                    seekerobj.setJobConNum(cursor.getString(10));
//                    seekerobj.setJobAddr(cursor.getString(11));
//                    seekerobj.setJobSeekerFName(cursor.getString(12));
//                    seekerobj.setJobFunArea(cursor.getString(13));
//                    seekerobj.setJobEmail(cursor.getString(14));
//                    seekerobj.setJobIndType(cursor.getString(15));
//                    seekerobj.setSeekerphoto(cursor.getBlob(16));
//                    seekerdetails.add(seekerobj);
//                } while (cursor.moveToNext());
//            }
//
//
//        } catch (Exception e) {
//            Log.e("Jobseekerdetails", e.getMessage());
//        } finally {
//            if (cursor != null)
//                cursor.close();
//            dbhelper.closeDataBase();
//        }
//
//        return seekerdetails;
//    }

    public static List<SetupData> getJobSeekerProfle(String query, Context context){
        List<SetupData> jobseekerprofile = new ArrayList();
        Cursor cursor = null;
        DatabaseHelper dbhelper = new DatabaseHelper(context);
        try {
            SetupData mJobListData = null;
            dbhelper.openDataBase();
            cursor = dbhelper.getdata(query);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    mJobListData = new SetupData();
                    mJobListData.setfName(cursor.getString(cursor.getColumnIndex("FirstName")));
                    mJobListData.setlName(cursor.getString(cursor.getColumnIndex("LastName")));
                    mJobListData.setPassword(cursor.getString(cursor.getColumnIndex("Password")));
                    mJobListData.setConfirmPass(cursor.getString(cursor.getColumnIndex("ConfirmPassword")));
                    mJobListData.setDateOfBirth(cursor.getString(cursor.getColumnIndex("DateOfBirth")));
                    mJobListData.setlanguagesKnown(cursor.getString(cursor.getColumnIndex("languagesKnown")));
                    mJobListData.setEmail(cursor.getString(cursor.getColumnIndex("Email")));
                    mJobListData.setContactNum(cursor.getString(cursor.getColumnIndex("ContactNo")));
                    mJobListData.setLandNum(cursor.getString(cursor.getColumnIndex("ContactNo_Landline")));
                    mJobListData.setCurrentIndestry(cursor.getString(cursor.getColumnIndex("Industry")));
                    mJobListData.setCurrentCtc(cursor.getString(cursor.getColumnIndex("CurrentCTC")));
                    mJobListData.setExpectedCtc(cursor.getString(cursor.getColumnIndex("expectedCTC")));
                    mJobListData.setExperience(cursor.getString(cursor.getColumnIndex("Experience")));
                    mJobListData.setEduCourse(cursor.getString(cursor.getColumnIndex("Education")));
                    mJobListData.setKeySkills(cursor.getString(cursor.getColumnIndex("KeySkills")));
                    mJobListData.setStdKeySkills(cursor.getString(cursor.getColumnIndex("StdKeySkills")));
                    mJobListData.setCurrentLocation(cursor.getString(cursor.getColumnIndex("Location")));
                    mJobListData.setUpdateResume(cursor.getString(cursor.getColumnIndex("UpdateResume")));
                    mJobListData.setGender(cursor.getString(cursor.getColumnIndex("Gender")));
                    mJobListData.setInstitution(cursor.getString(cursor.getColumnIndex("Institute")));
                    mJobListData.setYearPassing(cursor.getString(cursor.getColumnIndex("YearOfPass")));
                    mJobListData.setFunArea(cursor.getString(cursor.getColumnIndex("FunctionalArea")));
                    mJobListData.setRole(cursor.getString(cursor.getColumnIndex("Role")));
                    mJobListData.setJobtype(cursor.getString(cursor.getColumnIndex("Jobtype")));
                    mJobListData.setResumeHeadLine(cursor.getString(cursor.getColumnIndex("ResumeHeadLine")));
                    mJobListData.setProfileUpdate(cursor.getString(cursor.getColumnIndex("ProfileUpdate")));
                    mJobListData.setResumePath(cursor.getString(cursor.getColumnIndex("Resume")));
                    mJobListData.setTextResume(cursor.getString(cursor.getColumnIndex("TextResume")));
                    mJobListData.setUserName(cursor.getString(cursor.getColumnIndex(("UserName"))));
                    mJobListData.setCurrentDisgnation(cursor.getString(cursor.getColumnIndex("CurrentDesignation")));
                    mJobListData.setCurrentCompany(cursor.getString(cursor.getColumnIndex("CurrentCompany")));
                    mJobListData.setPreviousCompany(cursor.getString(cursor.getColumnIndex("PreviousDesignation")));
                    mJobListData.setPreviousCompany(cursor.getString(cursor.getColumnIndex("PreviousCompany")));
                    mJobListData.setPreferedLocation(cursor.getString(cursor.getColumnIndex("PreferredLocation")));
                    mJobListData.setNoticePeriod(cursor.getString(cursor.getColumnIndex("NoticePeriod")));
                    mJobListData.setInId(cursor.getString(cursor.getColumnIndex("InId")));
                    mJobListData.setJbType(cursor.getString(cursor.getColumnIndex("JbType")));
                    mJobListData.setCurrentLocationInt(cursor.getString(cursor.getColumnIndex("CurrentLocationInt")));
                    mJobListData.setUpdateOn(cursor.getString(cursor.getColumnIndex("UpdateOn")));
                    jobseekerprofile.add(mJobListData);
                } while (cursor.moveToNext());
            }


        } catch (Exception e) {
            Log.e("Jobseekerdetails", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
            dbhelper.closeDataBase();
        }


        return jobseekerprofile;


    }


    public static Cursor  getuserInfo(String query, Context ctx) {
        Cursor cursor = null;
        DatabaseHelper dbhelper = new DatabaseHelper(ctx);
        try {
            dbhelper.openDataBase();
            cursor = dbhelper.getdata(query);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor;
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        } finally {
//            if (cursor != null)
//                cursor.close();
            dbhelper.closeDataBase();
        }

        return null;
    }

    public static boolean  isRecordExisted(String query, Context ctx) {
        Cursor cursor = null;
        DatabaseHelper dbhelper = new DatabaseHelper(ctx);
        try {
            dbhelper.openDataBase();
            cursor = dbhelper.getdata(query);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getCount() == 0 ? false : true;
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return false;
    }


    public static String getSingleString(String query, Context ctx) {
        Cursor cursor = null;
        DatabaseHelper dbhelper = new DatabaseHelper(ctx);
        try {
            dbhelper.openDataBase();
            cursor = dbhelper.getdata(query);
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getString(0);
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return "";
    }

    public static void deleteRecord(final String tableName, Context ctx) {
        DatabaseHelper dbhelper = new DatabaseHelper(ctx);
        try {
            dbhelper.openDataBase();
            dbhelper.deleteRow(tableName,"","", false);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        } finally {

            dbhelper.closeDataBase();
        }


    }

    public static void insertRecuiterRegData(final ApplicationThread.OnComplete oncomplete,RecuiterObject recuiterObject,final FragmentActivity ctx){
        final DatabaseHelper seekerInsertDB = new DatabaseHelper(ctx);
        LinkedHashMap RecuiterRegDataMap = new LinkedHashMap();
        final List<LinkedHashMap> RecuiterRegDataList = new ArrayList<LinkedHashMap>();
        RecuiterRegDataMap.put("FirstName",recuiterObject.getFirstName());
        RecuiterRegDataMap.put("LastName",recuiterObject.getLastName());
        RecuiterRegDataMap.put("Password",recuiterObject.getPassword());
        RecuiterRegDataMap.put("ConfirmPassword",recuiterObject.getConfirmPassword());
        RecuiterRegDataMap.put("CompanyName",recuiterObject.getCompanyName());
        RecuiterRegDataMap.put("Email",recuiterObject.getEmail());
        RecuiterRegDataMap.put("CompanyUrl",recuiterObject.getCompanyUrl());
        RecuiterRegDataMap.put("ContactNo",recuiterObject.getContactNo());
        RecuiterRegDataMap.put("ContactNo_Landline",recuiterObject.getContactNoLandline());
        RecuiterRegDataMap.put("EmployType",recuiterObject.getEmployType());
        RecuiterRegDataMap.put("IndustryType",recuiterObject.getIndustryType());
        RecuiterRegDataMap.put("Location","");
        RecuiterRegDataMap.put("Address",recuiterObject.getAddress());
        RecuiterRegDataMap.put("CompanyProfile",recuiterObject.getCompanyProfile());
        RecuiterRegDataMap.put("KeySkills","");
        RecuiterRegDataMap.put("Activation","");
        RecuiterRegDataMap.put("RegDate","");
        RecuiterRegDataMap.put("EmailVerified","");
        RecuiterRegDataMap.put("IsOnline","");
        RecuiterRegDataMap.put("Designation",""+recuiterObject.getDesignation());
        RecuiterRegDataMap.put("CompanyLogo",recuiterObject.getCompanyLogo());
        RecuiterRegDataMap.put("ProfileAlerts","");

        RecuiterRegDataList.add(RecuiterRegDataMap);
        ApplicationThread.dbPost("RecuiterObj", "insert", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                seekerInsertDB.insertData("RecuiterRegistration", RecuiterRegDataList,ctx);
                oncomplete.execute(true, "success", null);
            }
        });

    }

    public static void insertSubuserData(final ApplicationThread.OnComplete oncomplete, SubUserModel recuiterObject, final FragmentActivity ctx){
        final DatabaseHelper seekerInsertDB = new DatabaseHelper(ctx);
        LinkedHashMap RecuiterRegDataMap = new LinkedHashMap();
        final List<LinkedHashMap> RecuiterRegDataList = new ArrayList<LinkedHashMap>();

        RecuiterRegDataMap.put("CompanyEmailId",recuiterObject.getCompanyEmailId());
        RecuiterRegDataMap.put("SubUserEmailId",recuiterObject.getSubUserEmailId());
        RecuiterRegDataMap.put("RecruiterCompanyName",recuiterObject.getRecruiterCompanyName());
        RecuiterRegDataMap.put("ContactNo",recuiterObject.getContactNo());
        RecuiterRegDataMap.put("LandlineNo",recuiterObject.getLandlineNo());
        RecuiterRegDataMap.put("UserName",recuiterObject.getUserName());

        RecuiterRegDataList.add(RecuiterRegDataMap);
        ApplicationThread.dbPost("SubUserModel", "insert", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                seekerInsertDB.insertData("SubUserRegistration", RecuiterRegDataList,ctx);
                oncomplete.execute(true, "success", null);
            }
        });

    }



    public static List<ChatMessage> getChatMessage(String query,Context ctx)
    {
        List<ChatMessage> chatMessagesArr =new ArrayList<ChatMessage>();
        Cursor cursor=null;
        DatabaseHelper dbhelper=new DatabaseHelper(ctx);
        try
        {
            ChatMessage  chatMessage =null;
            dbhelper.openDataBase();
            cursor=dbhelper.getdata(query);
            if(cursor!=null&&cursor.moveToFirst())
            {
                do {
                    chatMessage=new ChatMessage();
                    chatMessage.setFromEmailId(cursor.getString(0));
                    chatMessage.setToEmailId(cursor.getString(1));
                    chatMessage.setMessage(cursor.getString(2));
                    chatMessage.setMessageType(cursor.getString(3));
                    chatMessage.setDate(cursor.getString(4));
                    chatMessage.setTime(cursor.getString(5));
                    chatMessagesArr.add(chatMessage);
                    chatMessage = null;
                }while(cursor.moveToNext());
            }
        }
        catch(Exception e){
            Log.e("DAO",e.getMessage());
        }
        finally{
            if(cursor!=null)
                cursor.close();
            dbhelper.closeDataBase();
        }

        return chatMessagesArr;
    }

    public static void insertJobSeekerData(final ApplicationThread.OnComplete oncomplete,SetupData mSetupData,final FragmentActivity ctx){
        final DatabaseHelper seekerInsertDB = new DatabaseHelper(ctx);
        LinkedHashMap seekerDataMap = new LinkedHashMap();
        final List<LinkedHashMap> seekerDataList = new ArrayList<LinkedHashMap>();
        seekerDataMap.put("FirstName",mSetupData.getfName());
        seekerDataMap.put("LastName",mSetupData.getlName());
        seekerDataMap.put("Password",mSetupData.getPassword());
        seekerDataMap.put("ConfirmPassword",mSetupData.getConfirmPass());
        seekerDataMap.put("Email",mSetupData.getEmail());
        seekerDataMap.put("ContactNo",mSetupData.getContactNum());
        seekerDataMap.put("ContactNo_Landline",mSetupData.getLandNum());
        seekerDataMap.put("Gender",mSetupData.getGender());
        seekerDataMap.put("Industry",mSetupData.getCurrentIndestry());
        seekerDataMap.put("FunctionalArea",mSetupData.getFunArea());
        seekerDataMap.put("Role",""+mSetupData.getRole());
        seekerDataMap.put("Jobtype",""+mSetupData.getJobType());
        seekerDataMap.put("CurrentDesignation",""+mSetupData.getCurrentDisgnation());
        seekerDataMap.put("CurrentCTC",""+Float.parseFloat(mSetupData.getCurrentCtc()));
        seekerDataMap.put("expectedCTC",""+Float.parseFloat(mSetupData.getExpectedCtc()));
        seekerDataMap.put("KeySkills",""+mSetupData.getKeySkills());
        seekerDataMap.put("ResumeHeadLine",""+mSetupData.getResumeHeadLine());
        seekerDataMap.put("CurrentCompany",""+mSetupData.getCurrentCompany());
        seekerDataMap.put("Location",""+mSetupData.getCurrentLocation());
        seekerDataMap.put("PreferredLocation",""+mSetupData.getPreferedLocation());
        seekerDataMap.put("NoticePeriod",""+mSetupData.getNoticePeriod());
        seekerDataMap.put("Education",""+mSetupData.getEduCourse());
        seekerDataMap.put("Institute",""+mSetupData.getInstitution());
        seekerDataMap.put("YearOfPass",""+mSetupData.getYearPassing());
        seekerDataMap.put("Photo",""+mSetupData.getSeekerImageData());
        seekerDataMap.put("InId",""+mSetupData.getInId());
        seekerDataMap.put("RId",""+mSetupData.getrId());
        seekerDataMap.put("CurrentLocationInt","0");
        seekerDataMap.put("Experience",mSetupData.getExperience());
        seekerDataMap.put("UpdateOn",mSetupData.getUpdateOn());

        seekerDataMap.put("JbType",""+mSetupData.getJbType());
        seekerDataMap.put("DateOfBirth",mSetupData.getDateOfBirth());
        seekerDataMap.put("languagesKnown",mSetupData.getlanguagesKnown());

        seekerDataList.add(seekerDataMap);
        ApplicationThread.dbPost("Userdetails Saving..", "insert", new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                seekerInsertDB.insertData("JobSeekers", seekerDataList, ctx);
                oncomplete.execute(true, "success", null);
            }
        });

    }
}
