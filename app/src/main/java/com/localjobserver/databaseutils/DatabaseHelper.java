package com.localjobserver.databaseutils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.localjobserver.networkutils.ApplicationThread;

public class DatabaseHelper extends SQLiteOpenHelper {

    /* Database name */
    private final static String DATABASE_NAME = "localjobserver.sqlite";

    private final static int DATA_VERSION = 1;

    private String DB_PATH;

    private Context mContext;
    private SQLiteDatabase mSqLiteDatabase = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATA_VERSION);
        // TODO Auto-generated constructor stub
        this.mContext = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        Log.v("The Database Path", DB_PATH);
    }

    /* create an empty database if data base is not existed */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if (dbExist) {
            //do nothing - database already exist
        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            //this.getReadableDatabase();

            try {
                copyDataBase();

            } catch (SQLiteException ex) {
                ex.printStackTrace();
                throw new Error("Error copying database");

            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("Error copying database");
            }

            try {
                openDataBase();
            } catch (SQLiteException ex) {
                ex.printStackTrace();
                throw new Error("Error opening database");

            } catch (Exception e) {
                e.printStackTrace();
                throw new Error("Error opening database");
            }

        }

    }


    /* checking the data base is existed or not */
    /* return true if existed else return false */
    private boolean checkDataBase() {
        try {
            String check_Path = DB_PATH + DATABASE_NAME;
            mSqLiteDatabase = SQLiteDatabase.openDatabase(check_Path, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (Exception ex) {
            // TODO: handle exception
            ex.printStackTrace();
        }
        return mSqLiteDatabase != null ? true : false;
    }

    private void copyDataBase() throws IOException {

        File dbDir = new File(DB_PATH);
        if (!dbDir.exists()) {
            dbDir.mkdir();

        }
        //Open your local db as the input stream
        Log.v("1 Opening the assest folder db ", mContext.getAssets() + "/" + DATABASE_NAME);
        InputStream myInput = mContext.getAssets().open(DATABASE_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DATABASE_NAME;

        Log.i("2 Copy the db to the path ", outFileName);
        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        Log.i("Open the output db ", outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        Log.i("Copied the database file", outFileName);
        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    /* Open the database */
    public void openDataBase() throws SQLException {

        String check_Path = DB_PATH + DATABASE_NAME;
        if (mSqLiteDatabase != null) {
            mSqLiteDatabase.close();
            mSqLiteDatabase = null;
            mSqLiteDatabase = SQLiteDatabase.openDatabase(check_Path, null, SQLiteDatabase.OPEN_READWRITE);
        } else {
            mSqLiteDatabase = SQLiteDatabase.openDatabase(check_Path, null, SQLiteDatabase.OPEN_READWRITE);
        }


    }

    public void closeDataBase() {

        if (mSqLiteDatabase != null) {
            mSqLiteDatabase.close();
            mSqLiteDatabase = null;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }


    /**
     * Inserting data into database
     *
     * @param tableName ---> Table name to insert data
     * @param mapList   ---> map list which contains data
     * @param context   ---> Current class context
     */
    public void insertData(String tableName, List<LinkedHashMap> mapList, Context context) {
        if (!ApplicationThread.dbThreadCheck())
            com.localjobserver.networkutils.Log.e(DatabaseHelper.class.getName(), "called on non-db thread", new RuntimeException());

        mSqLiteDatabase = this.getReadableDatabase();

        try {

            for (int i = 0; i < mapList.size(); i++) {
                List<Entry> entryList = new ArrayList<Entry>((mapList.get(i)).entrySet());
                String query = "insert into " + tableName;
                String namestring, valuestring;
                StringBuffer values = new StringBuffer();
                StringBuffer columns = new StringBuffer();
                for (Entry temp : entryList) {
                    columns.append(temp.getKey());
                    columns.append(",");
                    values.append("'");
                    values.append(temp.getValue());
                    values.append("'");
                    values.append(",");
                }
                namestring = "(" + columns.deleteCharAt(columns.length() - 1).toString() + ")";
                valuestring = "(" + values.deleteCharAt(values.length() - 1).toString() + ")";
                query = query + namestring + "values" + valuestring;
                Log.v(getClass().getSimpleName(), "query.." + query);
                mSqLiteDatabase.execSQL(query);
            }
        } catch (Exception e) {
            // TODO: handle exception
            String msg = e.getMessage();
            e.printStackTrace();
        } finally {
//			if (mSqLiteDatabase != null)
//				mSqLiteDatabase.close();
        }

    }

    /**
     * Updating database records
     *
     * @param tableName      ---> Table name to update
     * @param list           ---> List which contains data values
     * @param isClaues       ---> Checking where condition availability
     * @param clauesValue    ---> Where condition values
     * @param whereCondition ---> condition
     * @param context        ---> Current class
     */
    public void updateData(String tableName, List<HashMap> list,String whereCondition, Context context) {

        if (!ApplicationThread.dbThreadCheck())
            com.localjobserver.networkutils.Log.e(DatabaseHelper.class.getName(), "called on non-db thread", new RuntimeException());

        mSqLiteDatabase = this.getWritableDatabase();
        try {
            for (int i = 0; i < list.size(); i++) {
                List<Entry> entryList = new ArrayList<Entry>((list.get(i)).entrySet());
                String query = "update " + tableName + " set ";
                String namestring = "";
                System.out.println("\n==> Size of Entry list: " + entryList.size());
                StringBuffer values = new StringBuffer();
                StringBuffer columns = new StringBuffer();
                for (Entry temp : entryList) {
                    columns.append(temp.getKey());
                    columns.append("='");
                    columns.append(temp.getValue());
                    columns.append("',");
                }
                namestring = columns.deleteCharAt(columns.length() - 1).toString();
                query = query + namestring + "" + whereCondition;
                mSqLiteDatabase.execSQL(query);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//					 mSqLiteDatabase.close();
        }

    }

    /**
     * Deleting records from database table
     *
     * @param tableName  ---> Table name
     * @param columnName ---> Column name to deleting
     * @param value      ---> Value for where condition
     * @param isWhere    ---> Checking where condition is required or not
     */
    public void deleteRow(String tableName, String columnName, String value, boolean isWhere) {
        if (!ApplicationThread.dbThreadCheck())
            com.localjobserver.networkutils.Log.e(DatabaseHelper.class.getName(), "called on non-db thread", new RuntimeException());

        try {
            mSqLiteDatabase = this.getWritableDatabase();

            String query = "delete from " + tableName;
            if (isWhere) {
                query = query + "where" + columnName + "=" + value;
            }
            mSqLiteDatabase.execSQL(query);
        } catch (Exception e) {
            e.printStackTrace();
            // ACRA.getErrorReporter().handleException(e);
        } finally {
            mSqLiteDatabase.close();
        }

    }

//	  public void open() throws SQLException {
//		    database = DatabaseHelper.getWritableDatabase();
//		  }

    /**
     * Sample method which returns a data cursor.
     *
     * @param query_str ---> Query string
     * @return ---> Cursor
     */
    public Cursor getdata(String query_str) {
        if (!ApplicationThread.dbThreadCheck())
            com.localjobserver.networkutils.Log.e(DatabaseHelper.class.getName(), "called on non-db thread", new RuntimeException());

//		  SQLiteDatabase database = DatabaseHelper.getDBAdapterInstance(_context, _dbName, _tables).getWritableDatabase();
//		  database = this.getReadableDatabase();
        mSqLiteDatabase = this.getReadableDatabase();
        try {
            Cursor mCursor = mSqLiteDatabase.rawQuery(query_str, null);
//			Log.v(DatabaseHelper.class.getName(), "query_str...."+query_str);
            return mCursor;
        } catch (Exception e) {
            // ACRA.getErrorReporter().handleException(e);
//			  System.out.print("error"+e.getMessage());
            e.printStackTrace();

            if (mSqLiteDatabase != null)
                mSqLiteDatabase.close();

            return null;
        } finally {
//				if (mSqLiteDatabase != null)
//					mSqLiteDatabase.close();
        }
    }


    public Cursor queryDataBase(String query) {
        DatabaseHelper mDataBaseHelper = new DatabaseHelper(mContext);
        mSqLiteDatabase = mDataBaseHelper.getReadableDatabase();
        Log.v("query ", query);
        Cursor cursor = mSqLiteDatabase.rawQuery(query, null);
        return cursor;
    }

    public void updatePassword(String password, String seekerid) {
        SQLiteDatabase objSqliteDB = null;
        try {

            mSqLiteDatabase = this.getWritableDatabase();
            ContentValues update_values = new ContentValues();

            update_values.put("Password", password);
            update_values.put("ConfirmPassword ", password);

            String where = "SeekerId='" + seekerid + "'";
            mSqLiteDatabase.update("JobSeekers", update_values, where, null);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           // objSqliteDB.close();
        }

    }
}
