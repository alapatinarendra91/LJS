package com.localjobserver.Restaurent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.databaseutils.DatabaseAccessObject;

public class CheckingHomeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checking_home);

        Button reception= (Button)findViewById(R.id.reception);
        Button cook= (Button)findViewById(R.id.cook);

        reception.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseAccessObject.deleteRecord("JobSeekers", getApplicationContext());
                startActivity(new Intent(getApplicationContext(), LoginScreen.class));

            }
        });

        cook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ReadyActivity.class));
            }
        });
    }
}
