package com.localjobserver.Restaurent;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import co.talentzing.R;
import com.localjobserver.data.LiveData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;

public class ReadyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready);

        Button ready= (Button)findViewById(R.id.ready);
        Button welcome= (Button)findViewById(R.id.welcome);

        if (getIntent().getStringExtra("TYPE")!= null){
            welcome.setVisibility(View.VISIBLE);
            ready.setVisibility(View.GONE);
        }else {
            welcome.setVisibility(View.GONE);
            ready.setVisibility(View.VISIBLE);
        }

        ready.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageToServer("Item Ready");

            }
        });

    }

    public void sendMessageToServer(String message) {
        LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(ReadyActivity.this, "Error while sending message", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (result != null) {
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.SendGCMNotification, message, "hari_b@vishist.com", "suri143.babu1@gmail.com", "1"));
    }
}
