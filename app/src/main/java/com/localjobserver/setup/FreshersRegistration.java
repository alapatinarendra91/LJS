package com.localjobserver.setup;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.FileChooser;
import com.localjobserver.data.LiveData;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.FresherRegistrationModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

public class FreshersRegistration extends ActionBarActivity {
    private ActionBar actionBar = null;
    protected static boolean isClassVisible = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freashers_registration);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FreshersRegistrationFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FreshersRegistrationFragment extends Fragment {

        private static final int FILE_CHOOSER = 0, SELECT_PICTURE = 1, FROMCAM = 2;
        private static final int RESULT_LOCATIONS = 3;
        private static final int RESULT_KEYSKILLS = 1;
        private EditText fnameEdt, lnameEdt, emailEdt, contactNumEdt, landNumEdt, passwordEdt, confirmpasswordEdt, collegeNameEdt, bDate;
        private MultiAutoCompleteTextView keySkillsEdt,languages;
        private TextView resumeName,agree_txt;
        private Spinner eduSp, yearofSp, daysSpin,locationNameEdt;
        private RadioGroup radioSex, radioTimeStatus,radioStatus;
        private Button chooseResumeBtn, choosePhotoBtn, submitBtn;
        private CheckBox ac_email, ac_sms_Cource, ac_chat, ac_none, ac_email_1, ac_sms, ac_chat_1, ac_ref, ac_none1, chk_agree;
        private LinearLayout dailyStatus, selectTime, timeStatus;
        private RadioButton radioDaily, radioWeekly, radioInstant;
        private ImageView fresher_image, eye_image;
        private View flipper;
        private boolean take_photo = false;
        private byte[] seekerImageData;
        private RadioButton radioMaleButton, radioFeMaleButton;
        private LinearLayout walkindateLL;
        private ProgressDialog progressDialog;
        public LinkedHashMap<String, String> educationMap, locationsMap = null;
        private String selectedDay = "", compressedStr = "", resumeFile = "";
        private SharedPreferences preferences;
        private String fileSelected = "";
        private File resume;
        private FresherRegistrationModel mDataModel;
        private Calendar myCalendar = Calendar.getInstance();
        private boolean eye_var = false;
        public ArrayList<String> keySkillsList = null;
        private String selectedStr = "";


        public FreshersRegistrationFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            flipper = inflater.inflate(R.layout.fragment_freashers_registration, container, false);

            initViews();
            setViews();

            return flipper;
        }

        private void initViews() {

            mDataModel = new FresherRegistrationModel();
            dailyStatus = (LinearLayout) flipper.findViewById(R.id.dailyStatus);
            selectTime = (LinearLayout) flipper.findViewById(R.id.selectTime);
            timeStatus = (LinearLayout) flipper.findViewById(R.id.timeStatus);
            radioDaily = (RadioButton) flipper.findViewById(R.id.radioDaily);
            radioWeekly = (RadioButton) flipper.findViewById(R.id.radioWeekly);
            radioInstant = (RadioButton) flipper.findViewById(R.id.radioInstant);
            radioTimeStatus = (RadioGroup) flipper.findViewById(R.id.radioTimeStatus);
            radioStatus  = (RadioGroup)flipper.findViewById(R.id.radioStatus);
            radioMaleButton = (RadioButton) flipper.findViewById(R.id.radioMale);
            radioFeMaleButton = (RadioButton) flipper.findViewById(R.id.radioFemale);
            walkindateLL = (LinearLayout) flipper.findViewById(R.id.bDateLL);
            bDate = (EditText) flipper.findViewById(R.id.dateOfBirth);
            radioDaily.setOnClickListener(operationListener);
            radioWeekly.setOnClickListener(operationListener);
            radioInstant.setOnClickListener(operationListener);

            ac_email = (CheckBox) flipper.findViewById(R.id.ac_email);
            ac_sms_Cource = (CheckBox) flipper.findViewById(R.id.ac_sms_Cource);
            ac_chat = (CheckBox) flipper.findViewById(R.id.ac_chat);
            ac_none = (CheckBox) flipper.findViewById(R.id.ac_none);
            ac_email_1 = (CheckBox) flipper.findViewById(R.id.ac_email_1);
            ac_sms = (CheckBox) flipper.findViewById(R.id.ac_sms);
            ac_chat_1 = (CheckBox) flipper.findViewById(R.id.ac_chat_1);
            ac_ref = (CheckBox) flipper.findViewById(R.id.ac_ref);
            ac_none1 = (CheckBox) flipper.findViewById(R.id.ac_none1);
            chk_agree = (CheckBox) flipper.findViewById(R.id.chk_agree);
            agree_txt = (TextView) flipper.findViewById(R.id.agree_txt);
            String styledText = "I have read, understood and agree to the<font color='#008ACE'><a href='http://localjobserver.com/TermsAndConditions.aspx'>Terms and Conditions</a></font>";

            agree_txt.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
            agree_txt.setMovementMethod(LinkMovementMethod.getInstance());

            fnameEdt = (EditText) flipper.findViewById(R.id.fnameEdt);
            lnameEdt = (EditText) flipper.findViewById(R.id.lnameEdt);
            emailEdt = (EditText) flipper.findViewById(R.id.emailEdt);
            contactNumEdt = (EditText) flipper.findViewById(R.id.contactNumEdt);
            landNumEdt = (EditText) flipper.findViewById(R.id.landNumEdt);
            passwordEdt = (EditText) flipper.findViewById(R.id.passwordEdt);
            confirmpasswordEdt = (EditText) flipper.findViewById(R.id.confirmpasswordEdt);
            collegeNameEdt = (EditText) flipper.findViewById(R.id.collegeNameEdt);
            locationNameEdt = (Spinner) flipper.findViewById(R.id.locationNameEdt);
            resumeName = (TextView) flipper.findViewById(R.id.resumeName);
            keySkillsEdt = (MultiAutoCompleteTextView) flipper.findViewById(R.id.keySkillsEdt);
            languages = (MultiAutoCompleteTextView) flipper.findViewById(R.id.languages);
            keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            languages.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

            eduSp = (Spinner) flipper.findViewById(R.id.eduSp);
            yearofSp = (Spinner) flipper.findViewById(R.id.yearofSp);
            daysSpin = (Spinner) flipper.findViewById(R.id.daytimeSpin);

            fresher_image = (ImageView) flipper.findViewById(R.id.fresher_image);
            eye_image = (ImageView) flipper.findViewById(R.id.eye_image);

            chooseResumeBtn = (Button) flipper.findViewById(R.id.chooseResumeBtn);
            choosePhotoBtn = (Button) flipper.findViewById(R.id.choosePhotoBtn);
            submitBtn = (Button) flipper.findViewById(R.id.submitBtn);

            preferences = getActivity().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);

            daysSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));

            daysSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDay = parent.getSelectedItem().toString();
                    if (parent.getSelectedItemId() != 0)
                        timeStatus.setVisibility(View.VISIBLE);
                    else
                        timeStatus.setVisibility(View.GONE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            eye_image.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (eye_var == false) {
                        eye_var = true;
                        eye_image.setImageResource(R.drawable.eye_hidden);
                        passwordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        confirmpasswordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        passwordEdt.setSelection(passwordEdt.getText().length());
                        confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                    } else {
                        eye_var = false;
                        eye_image.setImageResource(R.drawable.eye_show);
                        passwordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        confirmpasswordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        passwordEdt.setSelection(passwordEdt.getText().length());
                        confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                    }
                }
            });


            bDate.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            getEducations();
            getLocations();
            getKeyWords();
        }

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        private void updateLabel() {

            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            bDate.setText(sdf.format(myCalendar.getTime()));
        }

        private void setViews() {

            ac_email_1.setOnClickListener(operationListener);
            ac_sms.setOnClickListener(operationListener);
            ac_chat_1.setOnClickListener(operationListener);
            ac_ref.setOnClickListener(operationListener);
            ac_none1.setOnClickListener(operationListener);
            chooseResumeBtn.setOnClickListener(operationListener);
            choosePhotoBtn.setOnClickListener(operationListener);
            submitBtn.setOnClickListener(operationListener);

            yearofSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("year", 36)));

            yearofSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    String selectedYear =
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            ac_email.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                    if (isChecked) {
                        dailyStatus.setVisibility(View.VISIBLE);
                        timeStatus.setVisibility(View.GONE);
                    } else {
                        dailyStatus.setVisibility(View.GONE);
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                    }
                }
            });

            ac_none.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ac_email.setChecked(false);
                    ac_chat.setChecked(false);
                    ac_sms_Cource.setChecked(false);
                    timeStatus.setVisibility(View.GONE);
                    dailyStatus.setVisibility(View.GONE);
                    selectTime.setVisibility(View.GONE);
                    if (ac_none.isChecked())
                        ac_none.setChecked(false);
                    else
                        ac_none.setChecked(true);
                }
            });

            ac_chat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                }
            });
            ac_sms_Cource.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                }
            });

        }

        public void getLocations() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        locationsMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_LOCATIONS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }

        public void getKeyWords(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }

                    if (result != null) {
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_KEYSKILLS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            });
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {
                                Message messageToParent = new Message();
                                messageToParent.what = 0;
                                Bundle bundleData = new Bundle();
                                bundleData.putString("Time Over", "Lost Internet Connection");
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);
                            }
                        }
                    });
                }
            });

        }


        class StatusHandler extends Handler {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RESULT_LOCATIONS:
                        if (null != locationsMap && null != locationNameEdt && isClassVisible == true) {
                            locationNameEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(locationsMap, "Location")));
                        } else {
//                            Toast.makeText(getActivity(), "Not able to get locations", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case RESULT_KEYSKILLS:
                        if (null != keySkillsList && keySkillsList.size() > 0 && isClassVisible == true) {
//                            keySkillsEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                            keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
                        }
                        break;
                }
            }
        }




        View.OnClickListener operationListener = new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                switch (v.getId()) {

                    case R.id.ac_email_1:
                        ac_none1.setChecked(false);
                        break;
                    case R.id.ac_sms:
                        ac_none1.setChecked(false);
                        break;
                    case R.id.ac_chat_1:
                        ac_none1.setChecked(false);
                        break;
                    case R.id.ac_ref:
                        ac_none1.setChecked(false);
                        break;
                    case R.id.ac_none1:
                        ac_email_1.setChecked(false);
                        ac_sms.setChecked(false);
                        ac_chat_1.setChecked(false);
                        ac_ref.setChecked(false);
                        break;
                    case R.id.radioDaily:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioWeekly:
                        selectTime.setVisibility(View.VISIBLE);
                        timeStatus.setVisibility(View.VISIBLE);

                        break;
                    case R.id.radioInstant:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                        break;
                    case R.id.chooseResumeBtn:
                        Intent intent = new Intent(getActivity(), FileChooser.class);
                        ArrayList<String> extensions = new ArrayList<String>();
                        extensions.add(".pdf");
                        extensions.add(".doc");
                        extensions.add(".docx");
                        intent.putStringArrayListExtra("filterFileExtension", extensions);
                        startActivityForResult(intent, FILE_CHOOSER);
                        break;
                    case R.id.choosePhotoBtn:
                        profilePic();
                        break;
                    case R.id.submitBtn:
                        if (CommonUtils.isNetworkAvailable(getActivity())){
                            if (validateFresherRegister() == true && spinnerValidations() && CommonUtils.passwordValidate(passwordEdt.getText().toString(),getActivity())){
                                if (validatePassword().equals("")){
                                    if (!resumeName.getText().toString().equalsIgnoreCase("No File Selected")){

                                        if (chk_agree.isChecked()) {
                                            setFreshersData();
                                            uploadUsersData(toUpload());
                                        } else {
                                            Toast.makeText(getActivity(), "Please agree terms & Conditions", Toast.LENGTH_SHORT).show();
                                        }
                                    }else {
                                        Toast.makeText(getActivity(), "Select Resume", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                        } else {
                            Toast.makeText(getActivity(), "Please Check network Connection", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
            }
        };



        public void getEducations() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
//            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
//                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error occured while getting data from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        educationMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
//                        messageToParent.what = RESULT_EDUCATION;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
//                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                        if (null != educationMap && null != eduSp) {
                            eduSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(educationMap, "Education")));
                        } else {
                            Toast.makeText(getActivity(), "Not able to get education details", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
        }


        public boolean validateFresherRegister() {
            Form mForm = new Form(getActivity());
            mForm.addField(Field.using(fnameEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(lnameEdt).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using(emailEdt).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using(contactNumEdt).validate(NotEmpty.build(getActivity())));
            if (CommonUtils.isValidMobile(contactNumEdt.getText().toString(), contactNumEdt)){
                //            mForm.addField(Field.using(landNumEdt).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using(passwordEdt).validate(NotEmpty.build(getActivity())));
                if (CommonUtils.passwordValidate(passwordEdt.getText().toString(),getActivity())){
                    mForm.addField(Field.using(confirmpasswordEdt).validate(NotEmpty.build(getActivity())));
                    if (validatePassword().equalsIgnoreCase("")){
                        if (CommonUtils.spinnerSelect("Education", eduSp.getSelectedItemPosition(), getActivity())&&
                                CommonUtils.spinnerSelect("Current Locatin", locationNameEdt.getSelectedItemPosition(), getActivity())){

                            // mForm.addField(Field.using(collegeNameEdt).validate(NotEmpty.build(getActivity())));
                            mForm.addField(Field.using(keySkillsEdt).validate(NotEmpty.build(getActivity())));
//            mForm.addField(Field.using(bDate).validate(NotEmpty.build(getActivity())));
                            return (mForm.isValid()) ? true : false;
                        }

                    }

                }
            }else
            CommonUtils.showToast("Please specify a valid contact number",getActivity());

            return (mForm.isValid()) ? false : false;
        }

        public boolean spinnerValidations() {
            if (!CommonUtils.spinnerSelect("Education", eduSp.getSelectedItemPosition(), getActivity())
                    ||
                    !CommonUtils.spinnerSelect("Current Locatin", locationNameEdt.getSelectedItemPosition(), getActivity())
                    ) {
                return false;
            }

            return true;
        }

        public String validatePassword() {

            if (!(passwordEdt.getText().toString().toLowerCase().equalsIgnoreCase(confirmpasswordEdt.getText().toString().toLowerCase()))) {
                Toast.makeText(getActivity(), "Your password entries do not match", Toast.LENGTH_SHORT).show();
                return "Your password entries do not match";
            } else {
                return "";
            }

        }

        public void profilePic() {
            final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        //selectImage();
                        // Calls an Intent that opens camera to take a picture
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, FROMCAM);
                    } else if (options[item].equals("Choose from Gallery")) {
                        //Call an intent that opens gallery to select photo
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (requestCode == FILE_CHOOSER) {
                if (null != data) {
                    fileSelected = data.getStringExtra("fileSelected");
                    File f = new File(fileSelected);
                    System.out.println(f.getName());
                    resumeName.setText(f.getName());
                    resumeName.setVisibility(View.VISIBLE);
                    mDataModel.setResume(fileSelected);
                    File dir = Environment.getExternalStorageDirectory();
                    resume = new File(fileSelected);
                    try {
                        resumeFile = CommonUtils.encodeFileToBase64Binary(resume);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == FROMCAM) {
                if (null == data)
                    return;

                take_photo = true;

                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
//                imagePath = f.getName();
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap yourSelectedImage = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                Matrix matrix = new Matrix();
                matrix.postRotate(CommonUtils.getImageOrientation(f.getAbsolutePath()));
                Bitmap rotatedBitmap = Bitmap.createBitmap(yourSelectedImage, 0, 0, yourSelectedImage.getWidth(), yourSelectedImage.getHeight(), matrix, true);
                Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                fresher_image.setImageDrawable(dr);
                fresher_image.invalidate();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                seekerImageData = outputStream.toByteArray();
//                mSetupData.setSeekerImageData(seekerImageData);
//                imageString = Base64.encodeToString(seekerImageData, Base64.NO_WRAP);
            } else if (requestCode == SELECT_PICTURE) {
                if (null == data)
                    return;
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};
                take_photo = true;
                Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                options.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(filePath, 80, 80);
                Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                seekerImageData = outputStream.toByteArray();
                fresher_image.setImageDrawable(dr);
                fresher_image.invalidate();
//                recuiterObject.setCompanylogo(seekerImageData);
            }// for if
        }

        Bitmap ShrinkBitmap(String file, int width, int height) {

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

            int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight / (float) height);
            int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth / (float) width);

            if (heightRatio > 1 || widthRatio > 1) {
                if (heightRatio > widthRatio) {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
            return bitmap;
        }






            public void setFreshersData() {
                mDataModel.setFirstName(fnameEdt.getText().toString());
                mDataModel.setLastName(lnameEdt.getText().toString());
                mDataModel.setEducation(eduSp.getSelectedItem().toString());
                mDataModel.setPassword(passwordEdt.getText().toString());
                mDataModel.setConfirmPassword(passwordEdt.getText().toString());
                mDataModel.setEmail(emailEdt.getText().toString());
                mDataModel.setContactNo(contactNumEdt.getText().toString());
                mDataModel.setLandLine(landNumEdt.getText().toString());
                mDataModel.setUpdateOn(CommonUtils.getDateTime());
                if (take_photo)
                    mDataModel.setPhoto(Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                if (radioMaleButton.isChecked()) {
                    mDataModel.setGender("Male");
                } else {
                    mDataModel.setGender("FeMale");
                }
                mDataModel.setDateOfBirth(bDate.getText().toString());
                mDataModel.setCollegeName(collegeNameEdt.getText().toString());
                mDataModel.setEmailVerified(false);
                mDataModel.setIsOnline(false);
                mDataModel.setResume(emailEdt.getText().toString());
                mDataModel.setResumeHeadLine("");
                mDataModel.setUpdateResume("");
                mDataModel.setTextResume("");
                mDataModel.setFCType("Freshers");
                mDataModel.setPhotoString("");
                mDataModel.setYOP(yearofSp.getSelectedItem().toString());
                mDataModel.setLocation(locationNameEdt.getSelectedItem().toString());
                mDataModel.setKeySkills(keySkillsEdt.getText().toString());
                mDataModel.setStdKeySkills(keySkillsEdt.getText().toString());
            }

            public void uploadUsersData(HashMap<String, String> input) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Uploading user data..");
                    progressDialog.setCancelable(false);
                }
                progressDialog.show();
                JSONObject json = new JSONObject(input);
                String requestString = json.toString();
                requestString = CommonUtils.encodeURL(requestString);
                LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                    public void run() {
                        if (success == false) {
                            progressDialog.dismiss();

                            if (result != null) {
                            }
                            return;
                        }
                        if (result != null) {
                            if (result.toString().equalsIgnoreCase("true")) {
                                uploadPrivacySettings();
                            }
                            progressDialog.dismiss();
                        }
                    }
                }, String.format(Config.LJS_BASE_URL + Config.registerFresher, "" + requestString));
            }

        public void uploadPrivacySettings() {

            getFresharconditions();

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null && result.toString().equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        uploadUserResume();

                    }else {
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.fresherPrivacySettings, emailEdt.getText().toString().trim(), selectedDay, selectedStr, ac_email.isChecked(), ac_chat.isChecked()));

        }

        private void getFresharconditions(){
            if (ac_email.isChecked()){
                int selectedId = radioTimeStatus.getCheckedRadioButtonId();
                int selectedId_status = radioStatus.getCheckedRadioButtonId();
                selectedStr = "";
                RadioButton timebutton = (RadioButton)flipper.findViewById(selectedId);
                RadioButton timebutton_status = (RadioButton)flipper.findViewById(selectedId_status);

                if (null != timebutton){
                    selectedStr = timebutton.getText().toString();
                    com.localjobserver.networkutils.Log.i("", "time is : " + selectedStr);
                }else {
                    selectedStr = "";
                }


                if (selectTime.getVisibility() == View.VISIBLE){

                }else{
                    selectedDay = timebutton_status.getText().toString();
                }


            }else {
                selectedDay = "Sunday";
                selectedStr = "8 am";
            }



        }


            public void uploadUserImage() {

                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Uploading user data..");
                    progressDialog.setCancelable(false);
                }
//                progressDialog.show();
                compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);
                LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                    public void run() {
                        if (success == false) {
                            progressDialog.dismiss();
                            return;
                        }
                        Log.i("", "Result is : " + result.toString());
                        if (result != null && result.toString().contains("true")) {
                            progressDialog.dismiss();

                            if (preferences.getString(CommonUtils.getUserEmail(getActivity()), null) == null) {
                                designSuccessDialog();
                            }
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(CommonUtils.getUserEmail(getActivity()), compressedStr);


                        }
                    }
                }, "" + emailEdt.getText().toString() + ".png", emailEdt.getText().toString(), compressedStr, "uploadFresherImage", getActivity(), 0);

            }


            Dialog dialog;

            public void designSuccessDialog() {
                dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.common_dialog);
                dialog.setCanceledOnTouchOutside(true);
                TextView msgView = (TextView) dialog.findViewById(R.id.msgTxt);
                TextView titleHeader = (TextView) dialog.findViewById(R.id.titleHeader);
                titleHeader.setText("Confirmation");
                msgView.setText("Thank you for registering to Local Job Server ");
                dialog.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), LoginScreen.class));
                        getActivity().finish();
                    }
                });

                dialog.show();
            }


            public LinkedHashMap privacyData() {

                int selectedId = radioTimeStatus.getCheckedRadioButtonId();
                String selectedStr = "";
                RadioButton timebutton = (RadioButton) flipper.findViewById(selectedId);
                if (null != timebutton) {
                    selectedStr = timebutton.getText().toString();
                } else {
                    selectedStr = "";
                }
                LinkedHashMap settingsDataMap = new LinkedHashMap();
                settingsDataMap.put("Email", "" + emailEdt.getText().toString());
                if (null != selectedDay && selectedDay.length() > 0) {
                    settingsDataMap.put("WeekDay", selectedDay);
                } else {
                    settingsDataMap.put("WeekDay", "Daily");
                }

                settingsDataMap.put("OnDemandTime", selectedStr);
                settingsDataMap.put("EmailAlert", "" + ac_email.isChecked());
                settingsDataMap.put("SmsAlert", "" + ac_sms.isChecked());
                settingsDataMap.put("ChatAlert", "" + ac_chat.isChecked());
                settingsDataMap.put("PremiumJobSeekerService", "false");
                settingsDataMap.put("LjsCommunication", "false");
                return settingsDataMap;
            }

            public void uploadUserResume() {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
                LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                    public void run() {
                        if (success == false) {
                            progressDialog.dismiss();
                            return;
                        }
                        if (result != null) {
                            progressDialog.cancel();
                            if (take_photo)
                                uploadUserImage();
                            else
                                designSuccessDialog();
                        }
                    }
                }, fileSelected, "csenaveen22@gmail.com", resumeFile, "uploadFresherResume", getActivity(), 1);

            }

            public HashMap toUpload() {
                HashMap freshersDataMap = new HashMap();
                freshersDataMap.put("FirstName", mDataModel.getFirstName());
                freshersDataMap.put("LastName", mDataModel.getLastName());
                freshersDataMap.put("Password", mDataModel.getPassword());
                freshersDataMap.put("ConfirmPassword", mDataModel.getPassword());
                freshersDataMap.put("Email", mDataModel.getEmail());
                freshersDataMap.put("ContactNo", mDataModel.getContactNo());
                freshersDataMap.put("LandLine", mDataModel.getLandLine());
                freshersDataMap.put("YOP", mDataModel.getYOP());
                freshersDataMap.put("Location", mDataModel.getLocation());
                freshersDataMap.put("Education", mDataModel.getEducation());
                freshersDataMap.put("UpdateOn", mDataModel.getUpdateOn());
                freshersDataMap.put("Photo", "");
                freshersDataMap.put("StdKeySkills", mDataModel.getKeySkills());
                freshersDataMap.put("Gender", mDataModel.getGender());
                freshersDataMap.put("DateOfBirth", mDataModel.getDateOfBirth());
                freshersDataMap.put("KeySkills", mDataModel.getKeySkills());
                freshersDataMap.put("CollegeName", mDataModel.getCollegeName());
                freshersDataMap.put("EmailVerified", "false");
                freshersDataMap.put("IsOnline", "false");
                freshersDataMap.put("Resume", mDataModel.getResume());
                freshersDataMap.put("UpdateResume", mDataModel.getUpdateResume());
                freshersDataMap.put("ResumeHeadLine", mDataModel.getResumeHeadLine());
                freshersDataMap.put("TextResume", mDataModel.getTextResume());
                freshersDataMap.put("FCType", mDataModel.getFCType());
//            freshersDataMap.put("photoString", "" + mDataModel.getPhotoString());
                return freshersDataMap;
            }

        }

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
