package com.localjobserver.setup;

import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.SubUserModel;
import com.localjobserver.ui.ChangePasswordFragment;
import com.localjobserver.ui.PrivacySettingsFragment;
import com.localjobserver.ui.VisibilitySettingsFragment;

import java.util.List;

public class SubuserProfileActivity extends ActionBarActivity {

    private ActionBar actionBar = null;
    private TextView tvname,tvgender,tvcompanyName,tvmailid,tvphonenum,tvCompUrl,tvLandNum,tvIndustry,tvAddress,tvCompProfile;
    ImageView company_logo;
    List<SubUserModel> arrRecLisr = null;
    private String profile_pic = "";
    private SharedPreferences appPrefs = null;
    private byte[] seekerImageData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subuser_profile);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Profile Information");

        tvname = (TextView)findViewById(R.id.fname);
        tvmailid = (TextView)findViewById(R.id.email);
        tvcompanyName = (TextView)findViewById(R.id.companynameTxt);
        tvphonenum = (TextView)findViewById(R.id.contactNo);
//        tvgender = (TextView)findViewById(R.id.gender);
//        tvCompUrl = (TextView)findViewById(R.id.companyUrl);
//        tvLandNum = (TextView)findViewById(R.id.contactlandNo);
//        tvIndustry = (TextView)findViewById(R.id.industryType);
//        tvAddress = (TextView)findViewById(R.id.address);
//        tvCompProfile = (TextView)findViewById(R.id.companyProfile);
//        company_logo=(ImageView)findViewById(R.id.company_logo);

        getRecruiterData();

        appPrefs = getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        profile_pic = appPrefs.getString(CommonUtils.getUserEmail(this), null);

        if (profile_pic != null){
            try {
                String newprofile_pic = profile_pic.replaceAll("\\\\n","");
                seekerImageData = Base64.decode(newprofile_pic, Base64.DEFAULT);
                BitmapDrawable dr = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray
                        (seekerImageData, 0, seekerImageData.length));
                company_logo.setBackground(dr);
                company_logo.invalidate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, RecrutierRegistrationFragment.newInstance(1))
//                    .commit();
//        }
    }

    private void getRecruiterData(){
        arrRecLisr= DatabaseAccessObject.getSubuserrRegistrationDetails(Queries.getInstance().getSubuserRegDetails(), this);
        tvname.setText(""+arrRecLisr.get(0).getUserName());
        tvcompanyName.setText(""+arrRecLisr.get(0).getCompanyEmailId());
        tvmailid.setText(""+arrRecLisr.get(0).getSubUserEmailId());
        tvphonenum.setText(""+arrRecLisr.get(0).getContactNo());
//        tvCompUrl.setText(""+arrRecLisr.get(0).getCompanyUrl());
//        tvLandNum.setText(""+arrRecLisr.get(0).getContactNoLandline());
//        tvIndustry.setText("" + arrRecLisr.get(0).getIndustryType());
//        tvAddress .setText("" + arrRecLisr.get(0).getAddress());
//        tvCompProfile.setText("" + arrRecLisr.get(0).getCompanyProfile());

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recruiter_profile, menu);
            menu.getItem(0).setVisible(false);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()){

            case R.id.editprofile:
//                Intent i=new Intent(getApplicationContext(),RecruiterRegistrationActivity.class);
//                i.putExtra("flag",1);
//                startActivity(i);
                CommonUtils.showToast("Wait for clariication",getApplicationContext());

                break;

            case R.id.changepassword:
                getSupportFragmentManager().beginTransaction().add(android.R.id.content,  new ChangePasswordFragment()).addToBackStack(null).commit();
                actionBar.setTitle("Change Password");
                break;

            case R.id.visibilitysettings:
                getSupportFragmentManager().beginTransaction().add(android.R.id.content, new VisibilitySettingsFragment()).addToBackStack(null).commit();
                actionBar.setTitle("Visibility Settings");
                break;


            case R.id.privacysettings:
                getSupportFragmentManager().beginTransaction().add(android.R.id.content,  new PrivacySettingsFragment()).addToBackStack(null).commit();
                actionBar.setTitle("Privacy Settings");
                break;

            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }else{
                    this.finish();
                }
                break;

        }
        return true;

    }
}
