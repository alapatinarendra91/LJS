package com.localjobserver.setup;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import co.talentzing.R;
import com.localjobserver.models.JobListData;
import com.localjobserver.models.RecuiterObject;
import com.localjobserver.models.SetupData;
import com.localjobserver.profile.MainProfileFragment;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class RecruiterProfileActivityFragment extends Fragment {
    private TextView tvname,tvresumeheadline,tvkeyskills,tvexperience,tvmailid,tvphonenum,tvlocation,tvlatupdateddate;
    private ProgressDialog progressDialog;
    private RelativeLayout profilepic;
    private Button editprofilebtn,downloadresumebtn;
    public List<RecuiterObject> recruiterdetails;
    public RecuiterObject mSetupData;
    private Context context;
    private ActionBar actionBar = null;
    private Bitmap bmp = null;
    private static Drawable dr = null;
    private View rootView;



    public RecruiterProfileActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.activity_recruiter_registration, container, false);

        mSetupData = new RecuiterObject();


        tvname=(TextView)rootView.findViewById(R.id.profilename_dis);
        tvresumeheadline=(TextView)rootView.findViewById(R.id.resumeheadline_dis);
        tvkeyskills=(TextView)rootView.findViewById(R.id.keyskills_dis);
        tvexperience=(TextView)rootView.findViewById(R.id.experience_dis);
        tvmailid=(TextView)rootView.findViewById(R.id.mailid);
        tvphonenum=(TextView)rootView.findViewById(R.id.phonenum);
        tvlocation=(TextView)rootView.findViewById(R.id.location);
        tvlatupdateddate=(TextView)rootView.findViewById(R.id.lastupdateddate);

        profilepic=(RelativeLayout)rootView.findViewById(R.id.sec_rel);
        profilepic.setBackgroundResource(R.drawable.sample);
        editprofilebtn=(Button)rootView.findViewById(R.id.profile_details_edit_btn);
        editprofilebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        downloadresumebtn=(Button)rootView.findViewById(R.id.profile_details_download_btn);

        downloadresumebtn.setVisibility(View.GONE);
        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getSupportFragmentManager().beginTransaction()
//                        .replace(android.R.id.content, new PlaceholderFragment()).addToBackStack(null)
//                        .commit();
            }
        });
        Button recJobsBtn = (Button)rootView.findViewById(R.id.recommandedjobsList_btn);
        recJobsBtn.setVisibility(View.GONE);



        return rootView;
    }
}
