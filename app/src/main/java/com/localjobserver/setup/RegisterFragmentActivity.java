package com.localjobserver.setup;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.androidquery.AQuery;
import com.localjobserver.LoginScreen;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonDialogFragment;
import com.localjobserver.commonutils.CommonDialogFragment.YesNoDialogListener;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.FileChooser;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.example.MultipartUtility;
import com.localjobserver.imagecrop.Constant;
import com.localjobserver.imagecrop.CropImage.CropImage;
import com.localjobserver.imagecrop.ImageCropActivity;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.models.SetupData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.validator.validate.IsEmail;
import com.localjobserver.validator.validate.IsPositiveInteger;
import com.localjobserver.validator.validate.NotEmpty;
import com.localjobserver.validator.validator.Field;
import com.localjobserver.validator.validator.Form;
import com.localjobserver.validator.validator.FormUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class
        RegisterFragmentActivity extends ActionBarActivity  implements  YesNoDialogListener{

    private static ActionBar actionBar = null;
    private RegisterFragment registerFragment;
    private static  int CURRENT_DISPLAY_SCREEN = 0, FILE_CHOOSER = 1, SELECT_PICTURE =2, FROMCAM = 3, REQUEST_CODE_CROP_IMAGE = 4;;
    private List<String> setupTitles;
    private static boolean back_var = true;
    private Menu menustatic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_fragment);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setTitle(""+getResources().getString(R.string.pInformation));
        actionBar.setTitle("Job Seeker Registration Form");

        CURRENT_DISPLAY_SCREEN = 0;
        if (savedInstanceState == null) {
            registerFragment = new RegisterFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, registerFragment).commit();
        }
        setupTitles = Arrays.asList(getResources().getStringArray(R.array.setuptitles));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back_var = false;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        CURRENT_DISPLAY_SCREEN = 0;
    }

    public static void  updateTitle(int pos){
//        actionBar.setTitle(""+setupTitles.get(pos));
        actionBar.setTitle("Job Seeker Registration Form");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        menustatic = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_next) {
                if(CURRENT_DISPLAY_SCREEN == 0 && registerFragment.validatePerssonalDetailsScreens() && registerFragment.validateMobileNo() == true && registerFragment.password_check()){
                    if (registerFragment.validatePassword().length() == 0){
                        CURRENT_DISPLAY_SCREEN++;
                        registerFragment.renderSpecificScreen(CURRENT_DISPLAY_SCREEN - 1, CURRENT_DISPLAY_SCREEN);
                        updateTitle(CURRENT_DISPLAY_SCREEN);
                    }else {
                        Toast.makeText(RegisterFragmentActivity.this,""+registerFragment.validatePassword(),Toast.LENGTH_SHORT).show();
                    }

                } else if (CURRENT_DISPLAY_SCREEN == 1 && registerFragment.validateProfessionalDetails() && registerFragment.spinnervalidateProfessionalDetails()) {
                    CURRENT_DISPLAY_SCREEN++;
                    registerFragment.renderSpecificScreen(CURRENT_DISPLAY_SCREEN -1,CURRENT_DISPLAY_SCREEN);
                    updateTitle(CURRENT_DISPLAY_SCREEN);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                } else if (CURRENT_DISPLAY_SCREEN == 2 && registerFragment.spinnerEducationDetails()) {
                    CURRENT_DISPLAY_SCREEN++;
                    registerFragment.renderSpecificScreen(CURRENT_DISPLAY_SCREEN -1,CURRENT_DISPLAY_SCREEN);
                    updateTitle(CURRENT_DISPLAY_SCREEN);
                }

            if (CURRENT_DISPLAY_SCREEN == 3)
                menustatic.getItem(0).setVisible(false);
            else
                menustatic.getItem(0).setVisible(true);
            return true;
        }
        if (id == android.R.id.home) {
            menustatic.getItem(0).setVisible(true);
            if(CURRENT_DISPLAY_SCREEN > 0){
                registerFragment.renderSpecificScreen(CURRENT_DISPLAY_SCREEN,CURRENT_DISPLAY_SCREEN - 1);
                updateTitle(CURRENT_DISPLAY_SCREEN - 1);
                CURRENT_DISPLAY_SCREEN--;

            }else{
                CURRENT_DISPLAY_SCREEN = 0;
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class RegisterFragment extends Fragment{
        public ViewFlipper flipper;
        private static final String LOG_TAG = RegisterFragment.class.getName();
        public HashMap<Integer,View> screensInfo = null;
        private SharedPreferences preferences;
        private static final int SCREEN_PERSONAL = 0;
        private static final int SCREEN_PROFESSIONAL = 1;
        private static final int SCREEN_EDUCATIONAL = 2;
        private static final int SCREEN_FINAL = 3;

        private static final int RESULT_KEYSKILLS = 1;
        private static final int RESULT_INDUSTRIES = 2;
        private static final int RESULT_LOCATIONS = 3;
        private static final int RESULT_JOBTYPE = 4;
        private static final int RESULT_EDUCATION = 7;
        private static final int RESULT_INSTITUTION = 8;
        private static final int RESULT_ERROR = 0;
        private static final int RESULT_ROLES = 10;
        private static final int RESULT_DISIGNATIONS = 11;
        private static final int RESULT_USERDATA = 12;
        private static final int RESULT_COMPANIES = 13;
        private static final int RESULT_LANGUAGE = 14;
        public ArrayList<String> keySkillsList = null;
        private AQuery aQuery = null;
        private SetupData mSetupData = null;
        private Button chooseResBtn,uploadResumeBtn, choosePic,submitBtn,dialog_saveBtn;
        private MultiSpinnerSearch currentIndSpin,roleSpin,cuttentLocSpin,localitySp,educationSpin,institutionSpin;
        private Spinner  funAreaSpin, jobTypeSpin,totExpSpin,totexpmonths,cuttentCTCSpinL,cuttentCTCSpinT,
                        expectedCTCSpinL,expectedCTCSpinT,noticeSpin,yearSpin;
        private RadioButton radioMaleButton, radioFeMaleButton;
        private RadioGroup radioSex;
        private ImageView seekerImag,eye_image;
         String otherType = "";

        private EditText resumeHeadEdt,passwordEdt,confirmpasswordEdt,dateOfBirth;
        private EditText fnameEdt,lnameEdt,emailEdt,contactNumEdt,landNumEdt,dialog_otherEdt;
        private AutoCompleteTextView currentDisEdt, edtcompanyname, currentCompEdt;
        private MultiAutoCompleteTextView keySkillsEdt,languages,preLocEdt;
        private Dialog dialog_other;

        public LinkedHashMap<String,String> industries = null, locationsMap = null,localityMap = null, roleMap = new LinkedHashMap<>(),languageMap,
                                             cuttentLocMap = new LinkedHashMap<>(),noticeMap, educationMap,industriesMap,institutionMap,yearMap,disigntionMap = null, companiesMap = null;

        private byte[] seekerImageData;
        private TextView resumeNameTxt,agree_txt;
        private ProgressDialog progressDialog,progressDialog_localities;
        private boolean returnValue = false;
        private String selectedInd = "";
        private String currentCTCStr = "", expectedCtcStr = "", imageString,imagePath, resumeFile, compressedStr;
        private CheckBox emailChk, SMSview,Chatview,ac_ref,ac_none,chk_agree,tr_email,tr_sms,tr_chat,tr_ref,tr_none;
        private LinearLayout dailyStatus, selectTime, timeStatus,dailyStatus_extra, selectTime_extra, timeStatus_extra;
        private RadioButton radioDaily, radioWeekly, radioInstant,radioDaily_extra, radioWeekly_extra, radioInstant_extra;
        private Spinner daysSpin,daysSpin_extra;
        private String selectedDay = "",selectedDay_extra = "";
        private RadioGroup radioTimeStatus,radioTimeStatus_extra,radioStatus_extra;
        private SharedPreferences gcmPrefs = null;
        private String gcmId = "";
        private File resume;
        private String fileSelected = "";
        private boolean eye_var = false,imageAdd_var = false;
        private Calendar myCalendar = Calendar.getInstance();
        private String selectedStr_extra;
        private String responseString;
        private List<SetupData>  detailsFromResumeModel;

        public RegisterFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            flipper = (ViewFlipper) inflater.inflate(R.layout.fragment_register, container, false);
            screensInfo = new HashMap<>();
            aQuery = new AQuery(getActivity());
            progressDialog = new ProgressDialog(getActivity());
            preferences = getActivity().getSharedPreferences("ljs_prefs", Context.MODE_PRIVATE);
            screensInfo.put(SCREEN_PERSONAL,flipper.findViewById(R.id.personaldetailsLL));
            screensInfo.put(SCREEN_PROFESSIONAL,flipper.findViewById(R.id.proffesionaldetailsLL));
            screensInfo.put(SCREEN_EDUCATIONAL, flipper.findViewById(R.id.educationdetailsLL));
            screensInfo.put(SCREEN_FINAL,flipper.findViewById(R.id.finaldetailsLL));
            chooseResBtn = (Button)flipper.findViewById(R.id.chooseResumeBtn);
            uploadResumeBtn = (Button)flipper.findViewById(R.id.uploadResumeBtn);
            seekerImag  = (ImageView)flipper.findViewById(R.id.seekerImge);
            eye_image  = (ImageView)flipper.findViewById(R.id.eye_image);
            resumeNameTxt = (TextView)flipper.findViewById(R.id.resumeName);
            agree_txt = (TextView)flipper.findViewById(R.id.agree_txt);
            String styledText = "I have read, understood and agree to the<font color='#008ACE'><a href='https://talentzing.com/TermsAndConditions.aspx'>Terms and Conditions</a></font>";

            agree_txt.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
            agree_txt.setMovementMethod(LinkMovementMethod.getInstance());

            resumeNameTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAttachment(resume, fileSelected);
                }
            });

            dailyStatus = (LinearLayout) flipper.findViewById(R.id.dailyStatus);
            selectTime = (LinearLayout) flipper.findViewById(R.id.selectTime);
            timeStatus = (LinearLayout) flipper.findViewById(R.id.timeStatus);
            dailyStatus_extra = (LinearLayout) flipper.findViewById(R.id.dailyStatus_extra);
            selectTime_extra = (LinearLayout) flipper.findViewById(R.id.selectTime_extra);
            timeStatus_extra = (LinearLayout) flipper.findViewById(R.id.timeStatus_extra);
            radioDaily = (RadioButton) flipper.findViewById(R.id.radioDaily);
            radioWeekly = (RadioButton) flipper.findViewById(R.id.radioWeekly);
            radioInstant = (RadioButton) flipper.findViewById(R.id.radioInstant);
            radioDaily_extra = (RadioButton) flipper.findViewById(R.id.radioDaily_extra);
            radioWeekly_extra = (RadioButton) flipper.findViewById(R.id.radioWeekly_extra);
            radioInstant_extra = (RadioButton) flipper.findViewById(R.id.radioInstant_extra);
            radioTimeStatus  = (RadioGroup)flipper.findViewById(R.id.radioTimeStatus);
            radioTimeStatus_extra  = (RadioGroup)flipper.findViewById(R.id.radioTimeStatus_extra);
            radioStatus_extra  = (RadioGroup)flipper.findViewById(R.id.radioStatus_extra);
            radioDaily.setOnClickListener(operationListener);
            radioWeekly.setOnClickListener(operationListener);
            radioInstant.setOnClickListener(operationListener);
            radioDaily_extra.setOnClickListener(operationListener);
            radioWeekly_extra.setOnClickListener(operationListener);
            radioInstant_extra.setOnClickListener(operationListener);
            daysSpin = (Spinner) flipper.findViewById(R.id.daytimeSpin);
            daysSpin_extra = (Spinner) flipper.findViewById(R.id.daytimeSpin_extra);
            emailChk = (CheckBox)flipper.findViewById(R.id.ac_email);
            SMSview = (CheckBox)flipper.findViewById(R.id.ac_sms);
            Chatview = (CheckBox)flipper.findViewById(R.id.ac_chat);
            ac_ref = (CheckBox)flipper.findViewById(R.id.ac_ref);
            ac_none = (CheckBox)flipper.findViewById(R.id.ac_none);
            tr_email = (CheckBox)flipper.findViewById(R.id.tr_email);
            tr_sms = (CheckBox)flipper.findViewById(R.id.tr_sms);
            tr_chat = (CheckBox)flipper.findViewById(R.id.tr_chat);
            tr_ref = (CheckBox)flipper.findViewById(R.id.tr_ref);
            tr_none = (CheckBox)flipper.findViewById(R.id.tr_none);
            chk_agree = (CheckBox)flipper.findViewById(R.id.chk_agree);

            tr_email.setOnClickListener(operationListener);
            tr_sms.setOnClickListener(operationListener);
            tr_chat.setOnClickListener(operationListener);
            tr_ref.setOnClickListener(operationListener);
            tr_none.setOnClickListener(operationListener);
            eye_image.setOnClickListener(operationListener);

            gcmPrefs = getActivity().getSharedPreferences("ljs_prefs", MODE_PRIVATE);
            gcmId = gcmPrefs.getString("gcmRegId", "");

            daysSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));
            daysSpin_extra.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.days)));

            daysSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDay = parent.getSelectedItem().toString();
                    if (parent.getSelectedItemId() != 0)
                        timeStatus.setVisibility(View.VISIBLE);
                    else
                        timeStatus.setVisibility(View.GONE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            daysSpin_extra.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDay_extra = parent.getSelectedItem().toString();
                    if (parent.getSelectedItemId() != 0)
                        timeStatus_extra.setVisibility(View.VISIBLE);
                    else
                        timeStatus_extra.setVisibility(View.GONE);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });



            chooseResBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), FileChooser.class);
                    ArrayList<String> extensions = new ArrayList<String>();
                    extensions.add(".pdf");
                    extensions.add(".doc");
                    extensions.add(".docx");
                    intent.putStringArrayListExtra("filterFileExtension", extensions);
                    startActivityForResult(intent, FILE_CHOOSER);
                }
            });
            uploadResumeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (resume != null){
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Getting Resume details..");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        final boolean _active = true;
                        final int _splashTime = 1000;
                        Thread splashTread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    int waited = 0;
                                    while (_active && (waited < _splashTime)) {
                                        sleep(100);
                                        if (_active) {
                                            waited += 100;
                                        }
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } finally {

                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            uploadDocumentstoServer(Config.getDetailsFromResume);
                                        }
                                    });

                                }
                            }
                        };
                        splashTread.start();
                    }else
                        CommonUtils.showToast("Please select resume",getActivity());


                }
            });
            choosePic = (Button)flipper.findViewById(R.id.choosePhotoBtn);
            choosePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profilePic();
                }
            });
            mSetupData = new SetupData();

            emailChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                    if (isChecked) {
                        dailyStatus.setVisibility(View.VISIBLE);
                        timeStatus.setVisibility(View.GONE);
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                    } else {
                        dailyStatus.setVisibility(View.GONE);
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                    }
                }
            });

            ac_none.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    emailChk.setChecked(false);
                    SMSview.setChecked(false);
                    Chatview.setChecked(false);
                    ac_ref.setChecked(false);
                    timeStatus.setVisibility(View.GONE);
                    dailyStatus.setVisibility(View.GONE);
                    selectTime.setVisibility(View.GONE);
                    if (ac_none.isChecked())
                        ac_none.setChecked(false);
                    else
                        ac_none.setChecked(true);
                }
            });

            SMSview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                }
            });
            Chatview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                }
            });
            ac_ref.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ac_none.setChecked(false);
                }
            });




            initializeUI();

            return flipper;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            getIndustries();
//            getLocations();
//            getKeyWords();
//            getLanguages();
//            getInstitutions();
//            getEducations();

        }

        public void renderSpecificScreen(int prevIndx,int nextIndx){
            Log.e(LOG_TAG, "prevIndx.." + prevIndx + "..nextIndx.." + nextIndx);
            screensInfo.get(prevIndx).setVisibility(View.GONE);
            screensInfo.get(nextIndx).setVisibility(View.VISIBLE);

            if(nextIndx >= 1){
                FormUtils.hideKeyboard(getActivity(),(EditText)flipper.findViewById(R.id.fnameEdt));
            }

            if(nextIndx == SCREEN_PROFESSIONAL){
//                (new Handler()).postDelayed(new Runnable() {
//
//                    public void run() {
                        currentDisEdt.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN , 0, 0, 0));
                        currentDisEdt.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));

//                    }
//                }, 200);

            }else if(nextIndx == SCREEN_EDUCATIONAL){
            }else if(nextIndx == SCREEN_FINAL){
                renderNotiDetails();
            }
        }

        private void openAttachment(File fileIn, String resumeName) {
            // TODO Auto-generated method stub
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);


            Uri uri = Uri.fromFile(fileIn);

            if(resumeName.endsWith(".doc"))
            {
                intent.setDataAndType(uri, "application/msword");
                startActivity(intent);
            }else if(resumeName.endsWith(".pdf"))
            {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }else if(resumeName.endsWith(".rtf"))
            {
                intent.setDataAndType(uri, "application/vnd.ms-excel");
                startActivity(intent);
            }
            else {
                intent.setDataAndType(uri, "*/*");
                startActivity(intent);
            }
        }

        View.OnClickListener operationListener = new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                switch (v.getId()) {

                    case R.id.eye_image:

                        if (eye_var == false) {
                            eye_var = true;
                            eye_image.setImageResource(R.drawable.eye_hidden);
                            passwordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            confirmpasswordEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            passwordEdt.setSelection(passwordEdt.getText().length());
                            confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                        }else {
                            eye_var = false;
                            eye_image.setImageResource(R.drawable.eye_show);
                            passwordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            confirmpasswordEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            passwordEdt.setSelection(passwordEdt.getText().length());
                            confirmpasswordEdt.setSelection(confirmpasswordEdt.getText().length());
                        }

                        break;
                    case R.id.SMSview:

                        break;
                    case R.id.Chatview:

                        break;
                    case R.id.preview:

                        break;
                    case R.id.ljscommunication:

                        break;
                    case R.id.tr_email:
                        tr_none.setChecked(false);
                        if (tr_email.isChecked()) {
                            dailyStatus_extra.setVisibility(View.VISIBLE);
                            timeStatus_extra.setVisibility(View.GONE);
                            selectTime_extra.setVisibility(View.GONE);
                            timeStatus_extra.setVisibility(View.GONE);
                        } else {
                            dailyStatus_extra.setVisibility(View.GONE);
                            selectTime_extra.setVisibility(View.GONE);
                            timeStatus_extra.setVisibility(View.GONE);
                        }

                        break;
                    case R.id.tr_sms:
                        tr_none.setChecked(false);
                        break;
                    case R.id.tr_chat:
                        tr_none.setChecked(false);
                        break;
                    case R.id.tr_ref:
                        tr_none.setChecked(false);
                        break;
                    case R.id.tr_none:

                        tr_email.setChecked(false);
                        tr_sms.setChecked(false);
                        tr_chat.setChecked(false);
                        tr_ref.setChecked(false);

                        timeStatus_extra.setVisibility(View.GONE);
                        dailyStatus_extra.setVisibility(View.GONE);
                        selectTime_extra.setVisibility(View.GONE);
//                        if (tr_none.isChecked())
//                            tr_none.setChecked(false);
//                        else
                            tr_none.setChecked(true);
                        break;

                    case R.id.TrSMSview:
                        tr_none.setChecked(false);
                        break;
                    case R.id.radioDaily:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.VISIBLE);
                        break;

                    case R.id.radioDaily_extra:
                        selectTime_extra.setVisibility(View.GONE);
                        timeStatus_extra.setVisibility(View.VISIBLE);
                        break;
                    case R.id.TrChatview:
                        tr_none.setChecked(false);
                        break;
                    case R.id.radioWeekly:
                        selectTime.setVisibility(View.VISIBLE);
//                        timeStatus.setVisibility(View.VISIBLE);

                        break;
                    case R.id.radioWeekly_extra:
                        selectTime_extra.setVisibility(View.VISIBLE);
//                        timeStatus_extra.setVisibility(View.VISIBLE);

                        break;
                    case R.id.radioInstant:
                        selectTime.setVisibility(View.GONE);
                        timeStatus.setVisibility(View.GONE);
                        break;
                    case R.id.radioInstant_extra:
                        selectTime_extra.setVisibility(View.GONE);
                        timeStatus_extra.setVisibility(View.GONE);
                        break;

                }
            }
        };

        public void initializeUI() {

            currentDisEdt = (AutoCompleteTextView)flipper.findViewById(R.id.currentDisEdt);
            languages = (MultiAutoCompleteTextView)flipper.findViewById(R.id.languages);
            languages.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            keySkillsEdt = (MultiAutoCompleteTextView)flipper.findViewById(R.id.keySkillsEdt);
            keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            preLocEdt = (MultiAutoCompleteTextView)flipper.findViewById(R.id.preLocEdt);
            preLocEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            resumeHeadEdt = (EditText)flipper.findViewById(R.id.resumeHeadEdt);
            passwordEdt = (EditText)flipper.findViewById(R.id.passwordEdt);
            confirmpasswordEdt = (EditText)flipper.findViewById(R.id.confirmpasswordEdt);
            dateOfBirth = (EditText)flipper.findViewById(R.id.dateOfBirth);
            fnameEdt = (EditText)flipper.findViewById(R.id.fnameEdt);
            lnameEdt = (EditText)flipper.findViewById(R.id.lnameEdt);
            emailEdt = (EditText)flipper.findViewById(R.id.emailEdt);
            contactNumEdt = (EditText)flipper.findViewById(R.id.contactNumEdt);
            landNumEdt = (EditText)flipper.findViewById(R.id.landNumEdt);
            currentCompEdt = (AutoCompleteTextView)flipper.findViewById(R.id.currentCompEdt);


            CommonUtils.setTouch(getActivity(),lnameEdt,fnameEdt,"Specify your first name");
            CommonUtils.setTouch(getActivity(),emailEdt,lnameEdt,"Specify your last name");
            CommonUtils.setTouch(getActivity(),contactNumEdt,emailEdt,"email");
            CommonUtils.setTouch(getActivity(),landNumEdt,contactNumEdt,"mobile");
            CommonUtils.setTouch(getActivity(),passwordEdt,contactNumEdt,"mobile");
            CommonUtils.setTouch(getActivity(),confirmpasswordEdt,passwordEdt,"Specify your password");
            CommonUtils.setTouch(getActivity(),dateOfBirth,confirmpasswordEdt,"Specify your conform password");
            CommonUtils.setTouch(getActivity(),resumeHeadEdt,keySkillsEdt,"Specify your keyskills");
            CommonUtils.setTouch(getActivity(),currentCompEdt,resumeHeadEdt,"Specify your resume headline");

            CommonUtils.edittextChangelistner(getActivity(),resumeHeadEdt);
            CommonUtils.edittextChangelistner(getActivity(),keySkillsEdt);


            resumeHeadEdt.setOnTouchListener(new View.OnTouchListener(){
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (resumeHeadEdt.getText().toString().length() == 0){
                        if (totExpSpin.getSelectedItemId() == 0)
                            resumeHeadEdt.setText(0+"+ years of experience on" +keySkillsEdt.getText().toString());
                        else
                            resumeHeadEdt.setText(totExpSpin.getSelectedItemPosition()-1+"+ years of experience on" +keySkillsEdt.getText().toString());
                    }

                    return false;
                }
            });



            dateOfBirth.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
            radioMaleButton = (RadioButton)flipper.findViewById(R.id.radioMale);
            radioFeMaleButton = (RadioButton)flipper.findViewById(R.id.radioFemale);
            radioSex = (RadioGroup)flipper.findViewById(R.id.radioSex);


            currentIndSpin = (MultiSpinnerSearch)flipper.findViewById(R.id.currentIndeSpin);
            currentIndSpin.setOnItemSelectedListener(spinListener);

            funAreaSpin = (Spinner)flipper.findViewById(R.id.funcareaSp);
            funAreaSpin.setOnItemSelectedListener(spinListener);

            roleSpin = (MultiSpinnerSearch)flipper.findViewById(R.id.roleSp);
//            roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));
            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,"Select Role");
            roleSpin.setOnItemSelectedListener(spinListener);

            jobTypeSpin = (Spinner)flipper.findViewById(R.id.jobTypeSp);
            jobTypeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.positiontype)));
            jobTypeSpin.setOnItemSelectedListener(spinListener);

            totExpSpin = (Spinner)flipper.findViewById(R.id.expSp);
            totExpSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
            totExpSpin.setOnItemSelectedListener(spinListener);

            totexpmonths = (Spinner)flipper.findViewById(R.id.totexpmonths);
            totexpmonths.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.months)));
            totexpmonths.setOnItemSelectedListener(spinListener);

            cuttentCTCSpinL = (Spinner)flipper.findViewById(R.id.totCtcSp);
            cuttentCTCSpinL.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
            cuttentCTCSpinL.setOnItemSelectedListener(spinListener);

            cuttentCTCSpinT = (Spinner)flipper.findViewById(R.id.totThCtcSp);
            cuttentCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
            cuttentCTCSpinT.setOnItemSelectedListener(spinListener);

            expectedCTCSpinL = (Spinner)flipper.findViewById(R.id.totExpCtcSp);
            expectedCTCSpinL.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
            expectedCTCSpinL.setOnItemSelectedListener(spinListener);

            expectedCTCSpinT = (Spinner)flipper.findViewById(R.id.totThExceptCtcSp);
            expectedCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
            expectedCTCSpinT.setOnItemSelectedListener(spinListener);

            cuttentLocSpin = (MultiSpinnerSearch)flipper.findViewById(R.id.currrentLocSp);
//            cuttentLocSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.location)));
            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),cuttentLocSpin,cuttentLocMap,1,"Select Current Location");
            cuttentLocSpin.setOnItemSelectedListener(spinListener);

            localitySp = (MultiSpinnerSearch)flipper.findViewById(R.id.localitySp);

            noticeSpin = (Spinner)flipper.findViewById(R.id.notiSp);
            noticeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.noticeperiod)));
            noticeSpin.setOnItemSelectedListener(spinListener);


            educationSpin = (MultiSpinnerSearch)flipper.findViewById(R.id.eduSp);
            educationSpin.setOnItemSelectedListener(spinListener);

            institutionSpin = (MultiSpinnerSearch)flipper.findViewById(R.id.qualificationSp);
            institutionSpin.setOnItemSelectedListener(spinListener);

            yearSpin = (Spinner) flipper.findViewById(R.id.yearofSp);
            yearSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValuesForYear("Select Year", 36,getActivity())));
            yearSpin.setOnItemSelectedListener(spinListener);


            dialog_other = CommonUtils.dialogIntialize(getActivity(),R.layout.dialog_other);
            dialog_otherEdt = (EditText) dialog_other.findViewById(R.id.dialog_otherEdt);
            dialog_saveBtn = (Button) dialog_other.findViewById(R.id.dialog_saveBtn);

            dialog_saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!dialog_otherEdt.getText().toString().equalsIgnoreCase("")){
                        if (otherType.equalsIgnoreCase("Education")){
                            mSetupData.setEduCourse(dialog_otherEdt.getText().toString());
                            educationMap.put(CommonUtils.getKeyFromValue(educationMap, "Others"), ""+dialog_otherEdt.getText().toString());
//                            educationSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Education")));
//                            educationSpin.setSelection(educationMap.size());
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),educationSpin,educationMap,1,dialog_otherEdt.getText().toString());
                        }else {
                            mSetupData.setInstitution(dialog_otherEdt.getText().toString());
                            institutionMap.put(CommonUtils.getKeyFromValue(institutionMap, "Others"), ""+dialog_otherEdt.getText().toString());
//                            institutionSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(institutionMap, "Institution")));
//                            institutionSpin.setSelection(institutionMap.size());
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),institutionSpin,institutionMap,1,dialog_otherEdt.getText().toString());
                        }
                        dialog_otherEdt.setText("");
                        dialog_other.dismiss();
                    }else
                        CommonUtils.showToast("Please specify other Name",getActivity());

                }
            });

        }

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        private void updateLabel() {

            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            dateOfBirth.setText(sdf.format(myCalendar.getTime()));
        }


        public void getLocations(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Locations...");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        getKeyWords();
                        Toast.makeText(getActivity(), "Error occured while getting Location data from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        getKeyWords();
                        locationsMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_LOCATIONS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
        }

        public void getLocalities(String locationCode) {

                progressDialog_localities = new ProgressDialog(getActivity());
            progressDialog_localities.setMessage("Getting Localities...");
            progressDialog_localities.setCancelable(false);
            progressDialog_localities.show();

            LiveData.getLocalityGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog_localities.dismiss();
                        return;
                    }
                    if (result != null) {
                        localityMap = (LinkedHashMap<String, String>) result;
//                        localitySp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(localityMap, "Locality")));
                        CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),localitySp,localityMap,1,"Select Locality");
                        progressDialog_localities.dismiss();
                    }
                    progressDialog_localities.dismiss();
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLocalities, locationCode), CommonKeys.arrLocalities);
        }

        public void getLanguages(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Languages...");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        getInstitutions();
                        Toast.makeText(getActivity(), "Error occured while getting Languages data from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        getInstitutions();
                        languageMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_LANGUAGE;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getLanguages), CommonKeys.arrLanguages);
        }

        public void getKeyWords(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting keyskills..");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }
            getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        getLanguages();
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error occured while getting Keywords data from server.", Toast.LENGTH_SHORT).show();

                        return;
                    }

                    if (result != null) {
                        getLanguages();
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_KEYSKILLS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            });
        }



        public void uploadUsersData(LinkedHashMap<String,String> input){
            if (progressDialog == null) {

                progressDialog.setMessage("Uploading user data..");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            JSONObject json = new JSONObject(input);
            String requestString = json.toString();


            LiveData.uploadRegistrationDetailsWithHeader(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        if (result != null) {
                            Toast.makeText(getActivity(), "" + result, Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    if (result != null) {
                        if (result.toString().equalsIgnoreCase("true")) {
//                            progressDialog.dismiss();
                            uploadPrivacySettings();
                        }
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.registerJobSeeker, "register"), requestString);
        }


        public  void renderNotiDetails(){
            submitBtn = (Button)flipper.findViewById(R.id.submitBtn);
            submitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chk_agree.isChecked()) {
                        setScreenDatatoModel();
                        uploadUsersData(toUpload());
                    } else {
                        Toast.makeText(getActivity(), "Please agree terms & Conditions", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        public void uploadPrivacySettings() {

//            if (progressDialog == null) {
//                progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setMessage("Please wait...");
//                progressDialog.setCancelable(false);
//            }
//            progressDialog.show();

            JSONObject json = new JSONObject(privacyData());
            String requestString = json.toString();
            requestString = CommonUtils.encodeURL(requestString);

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
//                        progressDialog.dismiss();

                        uploadRecruterPrivacySettings();

                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.alertsJobSeeker, "" + requestString));

        }

        public void uploadRecruterPrivacySettings() {

                privacyData_extra();

//            if (progressDialog == null) {
//                progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setMessage("Please wait...");
//                progressDialog.setCancelable(false);
//            }
//            progressDialog.show();

            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null && result.toString().equalsIgnoreCase("true")) {
//                        progressDialog.dismiss();
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_USERDATA;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);

                    }else {
                        Toast.makeText(getActivity(),"Updated failed",Toast.LENGTH_SHORT).show();
                    }
                }
            }, String.format(Config.LJS_BASE_URL +  Config.trainerPrivacySettings, mSetupData.getEmail(),selectedDay , selectedStr_extra,tr_email.isChecked(),tr_chat.isChecked()));

        }


        public LinkedHashMap privacyData() {

            int selectedId = radioTimeStatus.getCheckedRadioButtonId();
            String selectedStr = "";
            RadioButton timebutton = (RadioButton)flipper.findViewById(selectedId);
            if (null != timebutton){
                selectedStr = timebutton.getText().toString();
            }else {
                selectedStr = "";
            }
            LinkedHashMap settingsDataMap = new LinkedHashMap();
            settingsDataMap.put("Email", "" + mSetupData.getEmail());
            if (null != selectedDay && selectedDay.length() > 0){
                settingsDataMap.put("WeekDay", selectedDay);
            }else{
                settingsDataMap.put("WeekDay", "Daily");
            }

            settingsDataMap.put("OnDemandTime", selectedStr);
            settingsDataMap.put("EmailAlert", "" + emailChk.isChecked());
            settingsDataMap.put("SmsAlert", "" + SMSview.isChecked());
            settingsDataMap.put("ChatAlert", "" + Chatview.isChecked());
            settingsDataMap.put("PremiumJobSeekerService", "false");
            settingsDataMap.put("LjsCommunication", "false");
            return settingsDataMap;
        }

        public void privacyData_extra() {

            int selectedId_extra = radioTimeStatus_extra.getCheckedRadioButtonId();
            int selectedId_status_extra = radioStatus_extra.getCheckedRadioButtonId();
            selectedStr_extra = "";
            RadioButton timebutton_extra = (RadioButton)flipper.findViewById(selectedId_extra);
            RadioButton timebutton_status_extra = (RadioButton)flipper.findViewById(selectedId_status_extra);

            if (null != timebutton_extra){
                selectedStr_extra = timebutton_extra.getText().toString();
                com.localjobserver.networkutils.Log.i("", "time is : " + selectedStr_extra);
            }else {
                selectedStr_extra = "";
            }


            if (selectTime_extra.getVisibility() == View.VISIBLE){

            }else{
                selectedDay_extra = timebutton_status_extra.getText().toString();
            }

            selectedDay = selectedDay_extra;
//            selectedStr = selectedStr_extra;
            emailChk.setChecked(tr_email.isChecked());
            Chatview.setChecked(tr_chat.isChecked());
        }

      public void  uploadUserImage(){

//          if (progressDialog == null) {
//              progressDialog = new ProgressDialog(getActivity());
//              progressDialog.setMessage("Uploading user data..");
//              progressDialog.setCancelable(false);
//          }
          progressDialog.show();
          if (seekerImageData != null)
          compressedStr = Base64.encodeToString(seekerImageData, Base64.DEFAULT);

          LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
              public void run() {
                  if (success == false) {
                      progressDialog.dismiss();
                      return;
                  }
                  if (result != null) {
//                      progressDialog.dismiss();
                      uploadDocumentstoServer(Config.uploadResume);
//                      uploadUserResume();

                  }
              }
          }, "" + mSetupData.getEmail() + ".png", mSetupData.getEmail(), compressedStr, "seekerUploadImage", getActivity(), 0);

      }

        private void uploadDocumentstoServer(String resumeType) {

            String charset = "UTF-8";
            String requestURL = "";
            if (resumeType.equalsIgnoreCase(Config.getDetailsFromResume))
             requestURL = Config.LJS_BASE_URL +Config.getDetailsFromResume;
            else
                requestURL = Config.LJS_BASE_URL +Config.uploadResume;

            try {
                MultipartUtility multipart = new MultipartUtility(requestURL, charset);
                multipart.addFilePart("", resume);

                List<String> response = multipart.finish();
                System.out.println("SERVER REPLIED:");
                for (String line : response) {
                    Log.e("Resp", "Respo is : " + line.toString());
                    responseString = line.toString();
                }
//                CommonUtils.showToast("" + response, getActivity());
                progressDialog.cancel();

                if (resumeType.equalsIgnoreCase(Config.getDetailsFromResume))
                getResumeData(responseString);
                else
                    saveResume();

            } catch (IOException ex) {
                Log.e("Exce", "Exce is : " + ex.getMessage());
                CommonUtils.showToast("" + ex.getMessage(), getActivity());
                progressDialog.cancel();
            }
        }

        public void saveResume() {

            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Save Resume...");
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
            LiveData.uploadRegistrationDetails(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        showYesNoDialog();
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {

                        showYesNoDialog();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.saveResume, resume.getName(),emailEdt.getText().toString().trim()));

        }


        public void  uploadUserResume(){
//            if (progressDialog == null) {
//                progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setMessage("Uploading user data..");
//                progressDialog.setCancelable(false);
//            }
//            progressDialog.show();
            LiveData.uploadSoapDataInXml(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        progressDialog.dismiss();
                        showYesNoDialog();
                    }
                }
//            }, fileSelected, mSetupData.getEmail(), "Resume", getActivity(), 1);
            }, "", mSetupData.getEmail(), "Resume", getActivity(), 1);

        }

        public void checkEmail_phNoExis() {

                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Checking Email...");
                progressDialog.setCancelable(false);
                progressDialog.show();

            LiveData.checkEmilExist(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false ) {
                        progressDialog.dismiss();
                        CommonUtils.showToast("" + result.toString().replace("\"",""), getActivity());
                        returnValue = false;
                        returnvalue(false);
                        return ;
                    }
                    if (result != null) {
                        returnValue = true;
                        returnvalue(true);

                        CURRENT_DISPLAY_SCREEN++;
                        renderSpecificScreen(CURRENT_DISPLAY_SCREEN - 1, CURRENT_DISPLAY_SCREEN);
//                        updateTitle(CURRENT_DISPLAY_SCREEN);
                    }
                    progressDialog.dismiss();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            returnvalue(true);

                        }
                    });
                }
            }, String.format(Config.LJS_BASE_URL + Config.checkSeekerEmail, emailEdt.getText().toString(), contactNumEdt.getText().toString()));

        }

        private boolean returnvalue (boolean value){

            return value;
        }
        public boolean validateProfessionalDetails(){
            Form mForm = new Form(getActivity());
            if (
                    CommonUtils.spinnerSelect("Current Industry",currentIndSpin.getSelectedIds().size(),getActivity()) &&
                    CommonUtils.spinnerSelect("Role",roleSpin.getSelectedIds().size(),getActivity())&&
                    CommonUtils.spinnerSelect("Job Type",jobTypeSpin.getSelectedItemPosition(),getActivity())&&
                    CommonUtils.spinnerSelect("Total Experience",totExpSpin.getSelectedItemPosition(),getActivity())&&
                    CommonUtils.spinnerSelect("Total Experience Months",totexpmonths.getSelectedItemPosition(),getActivity())&&
                    CommonUtils.spinnerSelect("Current CTC Lakhs",cuttentCTCSpinL.getSelectedItemPosition(),getActivity())&&
                    CommonUtils.spinnerSelect("Current CTC Thousands",cuttentCTCSpinT.getSelectedItemPosition(),getActivity())){
                //            mForm.addField(Field.using((AutoCompleteTextView)flipper.findViewById(R.id.currentDisEdt)).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using((MultiAutoCompleteTextView)flipper.findViewById(R.id.keySkillsEdt)).validate(NotEmpty.build(getActivity())));
                mForm.addField(Field.using((EditText) flipper.findViewById(R.id.resumeHeadEdt)).validate(NotEmpty.build(getActivity())));
//            mForm.addField(Field.using((EditText) flipper.findViewById(R.id.currentCompEdt)).validate(NotEmpty.build(getActivity())));
                if (mForm.isValid())
                if (CommonUtils.spinnerSelect("Current Location",cuttentLocSpin.getSelectedIds().size(),getActivity())){
                    mForm.addField(Field.using((MultiAutoCompleteTextView)flipper.findViewById(R.id.preLocEdt)).validate(NotEmpty.build(getActivity())));
                    return (mForm.isValid()) ? true : false;
                }
            }
            return (mForm.isValid()) ? false : false;
        }

        public  boolean spinnervalidateProfessionalDetails(){

                    if (
                            !CommonUtils.spinnerSelect("Current Industry",currentIndSpin.getSelectedIds().size(),getActivity()) ||
                            !CommonUtils.spinnerSelect("Role",roleSpin.getSelectedIds().size(),getActivity())||
                            !CommonUtils.spinnerSelect("Job Type",jobTypeSpin.getSelectedItemPosition(),getActivity())||
                            !CommonUtils.spinnerSelect("Total Experience",totExpSpin.getSelectedItemPosition(),getActivity())||
                            !CommonUtils.spinnerSelect("Total Experience Months",totexpmonths.getSelectedItemPosition(),getActivity())||
                            !CommonUtils.spinnerSelect("Annual salary Lakhs",cuttentCTCSpinL.getSelectedItemPosition(),getActivity())||
//                            !CommonUtils.spinnerSelect("Annual salary Thousand",cuttentCTCSpinT.getSelectedItemPosition(),getActivity())||
//                            !CommonUtils.spinnerSelect("Expected salary Lakhs",expectedCTCSpinL.getSelectedItemPosition(),getActivity())||
//                            !CommonUtils.spinnerSelect("Expected salary Thousand",expectedCTCSpinT.getSelectedItemPosition(),getActivity())||
                            !CommonUtils.spinnerSelect("Current Location",cuttentLocSpin.getSelectedIds().size(),getActivity())
//                            ||
//                            !CommonUtils.spinnerSelect("Notice period",noticeSpin.getSelectedItemPosition(),getActivity())
                            ){
                        return false;
                    }

            return true;
        }

        public  boolean spinnerEducationDetails(){
            if (!CommonUtils.spinnerSelect("Education",educationSpin.getSelectedIds().size(),getActivity()) ||
                    !CommonUtils.spinnerSelect("Institute",institutionSpin.getSelectedIds().size(),getActivity())
//                    ||
//                    !CommonUtils.spinnerSelect("Year of passing",yearSpin.getSelectedItemPosition(),getActivity())
                    ){
                return false;
            }

            return true;
        }


        public boolean validatePerssonalDetailsScreens(){
            Form mForm = new Form(getActivity());
            mForm.addField(Field.using((EditText) flipper.findViewById(R.id.fnameEdt)).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using((EditText) flipper.findViewById(R.id.lnameEdt)).validate(NotEmpty.build(getActivity())));
            mForm.addField(Field.using((EditText)flipper.findViewById(R.id.emailEdt)).validate(NotEmpty.build(getActivity())).validate(IsEmail.build(getActivity())));
            mForm.addField(Field.using((EditText)flipper.findViewById(R.id.contactNumEdt)).validate(NotEmpty.build(getActivity())).validate(IsPositiveInteger.build(getActivity())));
            if (validateMobileNo()){
//                if (checkEmail_phNoExis()){
                mForm.addField(Field.using((EditText) flipper.findViewById(R.id.passwordEdt)).validate(NotEmpty.build(getActivity())));
                if (mForm.isValid()){
                if (password_check()) {
                    mForm.addField(Field.using((EditText) flipper.findViewById(R.id.confirmpasswordEdt)).validate(NotEmpty.build(getActivity())));
                    if (validatePassword().length() == 0){
                        if (radioSex.getCheckedRadioButtonId() == -1){
                            CommonUtils.showToast("Please Select Gender " , getActivity());

                        }else
                            checkEmail_phNoExis();
//                            return (mForm.isValid()) ? true : false;
                    }
                }
                }
//            }
            }

            return (mForm.isValid()) ? false : false;
        }

        public boolean validateMobileNo(){
            EditText tv = (EditText)flipper.findViewById(R.id.contactNumEdt);

            if (CommonUtils.isValidMobile(tv.getText().toString(),tv))
                return true;
            else
                return  false;
        }

        public  boolean password_check(){

            return CommonUtils.passwordValidate(passwordEdt.getText().toString(),getActivity());
        }


        public String validatePassword(){

            if (!(aQuery.id(R.id.passwordEdt).getText().toString().toLowerCase().equalsIgnoreCase(aQuery.id(R.id.confirmpasswordEdt).getText().toString().toLowerCase()))){
                Toast.makeText(getActivity(),"Password and confirm password is need to be same",Toast.LENGTH_SHORT).show();
                return "Password and confirm password is need to be same";
            }else {
                return "";
            }

        }

        public void setScreenDatatoModel(){
            mSetupData.setEmployType("Recruiter");
            mSetupData.setfName(aQuery.id(R.id.fnameEdt).getText().toString());
            mSetupData.setlName(aQuery.id(R.id.lnameEdt).getText().toString());
            mSetupData.setEmail(aQuery.id(R.id.emailEdt).getText().toString());
            mSetupData.setContactNum(aQuery.id(R.id.contactNumEdt).getText().toString());
            mSetupData.setLandNum(aQuery.id(R.id.landNumEdt).getText().toString());
            mSetupData.setPassword(aQuery.id(R.id.passwordEdt).getText().toString());
            mSetupData.setConfirmPass(aQuery.id(R.id.confirmpasswordEdt).getText().toString());
            if (!dateOfBirth.getText().toString().equalsIgnoreCase(""))
            mSetupData.setDateOfBirth(aQuery.id(R.id.dateOfBirth).getText().toString());
            else
                mSetupData.setDateOfBirth("01/01/1901");
            mSetupData.setlanguagesKnown(aQuery.id(R.id.languages).getText().toString());
            mSetupData.setCurrentDisgnation(aQuery.id(R.id.currentDisEdt).getText().toString());
            if (localitySp != null && localityMap != null && localitySp.getSelectedIds().size() > 0)
                mSetupData.setLocality(localitySp.getSelectedItem().toString());
            else
                mSetupData.setLocality("");

            mSetupData.setKeySkills(aQuery.id(R.id.keySkillsEdt).getText().toString());
            mSetupData.setPreferedLocation(aQuery.id(R.id.preLocEdt).getText().toString());
            mSetupData.setCurrentCompany(aQuery.id(R.id.currentCompEdt).getText().toString());
            mSetupData.setCurrentCtc(cuttentCTCSpinL.getSelectedItem().toString() + "." + cuttentCTCSpinT.getSelectedItem().toString());
            mSetupData.setExpectedCtc(expectedCTCSpinL.getSelectedItem().toString() + "." + expectedCTCSpinT.getSelectedItem().toString());
            mSetupData.setTotExp(totExpSpin.getSelectedItem().toString() + "."+totexpmonths.getSelectedItem().toString());
            mSetupData.setResumeHeadLine(resumeHeadEdt.getText().toString());
            if (radioMaleButton.isChecked()){
                mSetupData.setGender("Male");
            }else{
                mSetupData.setGender("FeMale");
            }
        }


        private  LinkedHashMap  toUpload(){
            LinkedHashMap seekerDataMap = new LinkedHashMap();
            seekerDataMap.put("ReturnUrl","");
            seekerDataMap.put("SeekerId","0");
            seekerDataMap.put("FirstName",""+mSetupData.getfName());
            seekerDataMap.put("LastName",""+mSetupData.getlName());
            seekerDataMap.put("Password",""+mSetupData.getPassword());
            seekerDataMap.put("ConfirmPassword",""+mSetupData.getConfirmPass());
            seekerDataMap.put("DateOfBirth",""+mSetupData.getDateOfBirth());
            seekerDataMap.put("languagesKnown",""+mSetupData.getlanguagesKnown());
            seekerDataMap.put("Email",""+mSetupData.getEmail());
            seekerDataMap.put("ContactNo",""+mSetupData.getContactNum());
            seekerDataMap.put("ContactNo_Landline",""+mSetupData.getLandNum());
            seekerDataMap.put("Industry",""+mSetupData.getCurrentIndestry());
            seekerDataMap.put("Experience",""+mSetupData.getTotExp());
            seekerDataMap.put("CurrentCTC",""+mSetupData.getCurrentCtc());
            seekerDataMap.put("expectedCTC",""+mSetupData.getExpectedCtc());
            seekerDataMap.put("Education",""+mSetupData.getEduCourse());
            seekerDataMap.put("KeySkills",""+mSetupData.getKeySkills());
            seekerDataMap.put("StdKeySkills",""+mSetupData.getKeySkills());
            seekerDataMap.put("Location",""+mSetupData.getCurrentLocation());
            seekerDataMap.put("Locality",""+mSetupData.getLocality());
            seekerDataMap.put("UpdateResume","");
            seekerDataMap.put("Gender",""+mSetupData.getGender());
            seekerDataMap.put("Institute",""+mSetupData.getInstitution());
            seekerDataMap.put("YearOfPass",""+mSetupData.getYearPassing());
            seekerDataMap.put("FunctionalArea",""+mSetupData.getFunArea());
            seekerDataMap.put("Role",""+roleSpin.getSelectedItem().toString());
            seekerDataMap.put("ResumeHeadLine",""+mSetupData.getResumeHeadLine());
            seekerDataMap.put("ProfileUpdate","0");
            seekerDataMap.put("UpdateOn", CommonUtils.getDateTime());
            seekerDataMap.put("Resume","");
            seekerDataMap.put("TextResume","");
            seekerDataMap.put("UserName","");
            seekerDataMap.put("Status","Active");
            seekerDataMap.put("Photo","");
            seekerDataMap.put("CurrentDesignation",""+mSetupData.getCurrentDisgnation());
            seekerDataMap.put("CurrentCompany",""+mSetupData.getCurrentCompany());
            seekerDataMap.put("StdCurCompany",""+mSetupData.getCurrentCompany());
            seekerDataMap.put("PreviousDesignation",""+mSetupData.getPreviousDesignation());
            seekerDataMap.put("PreviousCompany",""+mSetupData.getPreviousCompany());
            seekerDataMap.put("StdPreCompany",""+mSetupData.getPreviousCompany());
            seekerDataMap.put("PreferredLocation",""+mSetupData.getPreferedLocation());
            seekerDataMap.put("NoticePeriod",""+mSetupData.getNoticePeriod());
            seekerDataMap.put("EmailVerified","false");
            seekerDataMap.put("VisibleSettings","0");
            seekerDataMap.put("JsId","0");
            seekerDataMap.put("IsOnline","false");
            seekerDataMap.put("ViewedCount","0");
            seekerDataMap.put("DownloadedCount","0");
            seekerDataMap.put("CandidatesActiveinLast","");
            seekerDataMap.put("ResumeperPage","");
            seekerDataMap.put("SearchQuery","");
            seekerDataMap.put("MatchedScore","");
            seekerDataMap.put("InId",""+mSetupData.getInId());
            seekerDataMap.put("FId","0");
            seekerDataMap.put("RId","0");
            seekerDataMap.put("JTId","0");
            seekerDataMap.put("Jobtype",""+mSetupData.getjType());
            seekerDataMap.put("CreatedBy","");
            seekerDataMap.put("similarCount","0");
            seekerDataMap.put("MatchedPercent","");
            seekerDataMap.put("RelevantScore","0");
            seekerDataMap.put("DirectRegistration","true");
            seekerDataMap.put("ProfileAccessSpecifier","public");
            seekerDataMap.put("SharedCompaniesList","");
            seekerDataMap.put("photoString","");
            seekerDataMap.put("languagesKnown",languages.getText().toString());
            seekerDataMap.put("ResumeFile","");
            seekerDataMap.put("DeviceID",""+gcmId);
            seekerDataMap.put("ProfileName","");
            return seekerDataMap;
        }

        public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
            ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                        public void run() {

                            if (success == false || result == null) {
                                oncomplete.execute(false, null, null);
                                return;
                            }
                            JSONArray jArray = null;
                            keySkillsList = new ArrayList<>();

                            try {
                                jArray = new JSONArray(result.toString());
                                for (int i = 0; i < jArray.length(); i++) {
                                    JSONObject c = jArray.getJSONObject(i);
                                    keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                                }
                                oncomplete.execute(true, keySkillsList, null);

                            } catch (JSONException e) {
                                Message messageToParent = new Message();
                                messageToParent.what = 0;
                                Bundle bundleData = new Bundle();
                                bundleData.putString("Time Over", "Lost Internet Connection");
                                messageToParent.setData(bundleData);
                                new StatusHandler().sendMessage(messageToParent);
                            }
                        }
                    });
                }
            });

        }
        public void getIndustries(){
//            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Industries...");
                progressDialog.setCancelable(false);
//            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        getDisegnation();
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        getDisegnation();
                        Log.i("","Institutes is : "+result);
                        industries = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_INDUSTRIES;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
        }

        public void getRoles(final String selectedInd){
//            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Roles...");
                progressDialog.setCancelable(false);
//            }
            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        Toast.makeText(getActivity(),"Error occured while getting Role data from server.",Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        return;
                    }
                    if (result != null) {
                        roleMap = (LinkedHashMap<String,String>)result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_ROLES;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                        progressDialog.dismiss();
                    }
                    progressDialog.dismiss();
                }
            },String.format(Config.LJS_BASE_URL + Config.getRoles,selectedInd), CommonKeys.arrRoles);
        }

        public void getInstitutions(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Institutes...");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }

            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        getEducations();
                        return;
                    }
                    if (result != null) {
                        getEducations();
                        institutionMap = (LinkedHashMap<String, String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_INSTITUTION;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getInstitutes), CommonKeys.arrInstitutes);
        }

        public void getEducations(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Educations...");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Error occured while getting Education data from server.",Toast.LENGTH_SHORT).show();

                        return;
                    }
                    if (result != null) {
                        educationMap = (LinkedHashMap<String,String>)result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_EDUCATION;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
//                        progressDialog.dismiss();
                    }
                }
            },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
        }


        public void getDisegnation(){
            if (!progressDialog.isShowing()) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Designation...");
                progressDialog.setCancelable(false);
//                progressDialog.show();
            }
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
                        progressDialog.dismiss();
                        getLocations();
                        Toast.makeText(getActivity(),"Error occured while getting Designation data from server.",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null) {
                        getLocations();
                        disigntionMap = (LinkedHashMap<String,String>)result;
//                        progressDialog.dismiss();
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_DISIGNATIONS;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                    }
                }
            },String.format(Config.LJS_BASE_URL + Config.getDesignations),CommonKeys.arrDesignations);
        }


        public void getCompanies(){
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Getting Companies...");
                progressDialog.setCancelable(false);
            }
//            progressDialog.show();
            LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
                public void run() {
                    if (success == false) {
//                        progressDialog.dismiss();
                        Toast.makeText(getActivity(),"Error occured while getting Companies data from server.",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (result != null) {
//                        progressDialog.dismiss();
                        companiesMap = (LinkedHashMap<String,String>) result;
                        Message messageToParent = new Message();
                        messageToParent.what = RESULT_COMPANIES;
                        Bundle bundleData = new Bundle();
                        messageToParent.setData(bundleData);
                        new StatusHandler().sendMessage(messageToParent);
                    }
                }
            }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
        }

        AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()){
                    case R.id.currentIndeSpin:
//                        if (currentIndSpin.getSelectedIds().size() == 0)
//                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndSpin,industries,1,"Select Industry");

                        if (null != industries && currentIndSpin.getSelectedIds().size() > 0){
//                            selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
                            selectedInd = String.valueOf(currentIndSpin.getSelectedIds().get(0));
                            mSetupData.setCurrentIndestry(currentIndSpin.getSelectedItem().toString());
                            mSetupData.setInId(selectedInd);
                            getRoles(selectedInd);
                        }
//                        else
//                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndSpin,industries,1,"Select Industry");
                        break;
                    case R.id.funcareaSp:
                        mSetupData.setFunArea(funAreaSpin.getSelectedItem().toString());
                        break;
                    case R.id.roleSp:
//                        mSetupData.setRole(roleSpin.getSelectedItem().toString());
//                        if (null == roleMap || roleSpin.getSelectedIds().size() == 0)
//                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,"Select Role");
                        break;
                    case R.id.jobTypeSp:
                        mSetupData.setjType(jobTypeSpin.getSelectedItem().toString());
                        break;
                    case R.id.expSp:
                        mSetupData.setTotExp(totExpSpin.getSelectedItem().toString() + "." + totexpmonths.getSelectedItem().toString());
                        if (totExpSpin.getSelectedItemPosition() != 0)
                        resumeHeadEdt.setText(totExpSpin.getSelectedItemPosition()-1+"+ years of experience on" +keySkillsEdt.getText().toString());
                        break;
                    case R.id.totexpmonths:
                        mSetupData.setTotExp(totExpSpin.getSelectedItem().toString()+"."+totexpmonths.getSelectedItem().toString());
                        break;
                    case R.id.totCtcSp:
                        currentCTCStr = cuttentCTCSpinL.getSelectedItem().toString();
                        mSetupData.setCurrentCtc(currentCTCStr);
                         if (cuttentCTCSpinL.getSelectedItemId() == 100){
                            expectedCTCSpinL.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                             expectedCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                            expectedCTCSpinL.setOnItemSelectedListener(spinListener);
                             expectedCTCSpinT.setOnItemSelectedListener(spinListener);
                        }else if (cuttentCTCSpinL.getSelectedItemId() != 0){
                            expectedCTCSpinL.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(currentCTCStr)+1, 100)));
                             expectedCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(currentCTCStr)+2, 100)));
                            expectedCTCSpinL.setOnItemSelectedListener(spinListener);
                             expectedCTCSpinT.setOnItemSelectedListener(spinListener);
                        }

                        break;
                    case R.id.totThCtcSp:
                        if (currentCTCStr != null && currentCTCStr.length() > 0){
                            currentCTCStr = currentCTCStr + "." +cuttentCTCSpinT.getSelectedItem().toString();
                        }else{
                            currentCTCStr = "0" + "." +cuttentCTCSpinT.getSelectedItem().toString();
                        }
                        mSetupData.setCurrentCtc(currentCTCStr);
                        break;
                    case R.id.totExpCtcSp:
                        expectedCtcStr  = expectedCTCSpinL.getSelectedItem().toString();
                        mSetupData.setExpectedCtc(expectedCtcStr);
                        if (expectedCTCSpinL.getSelectedItemId() == 100){
                            expectedCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                            expectedCTCSpinT.setOnItemSelectedListener(spinListener);
                        }else if (expectedCTCSpinL.getSelectedItemId() != 0){
                            expectedCTCSpinT.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expectedCtcStr)+1, 100)));
                            expectedCTCSpinT.setOnItemSelectedListener(spinListener);
                        }

                        break;
                    case R.id.totThExceptCtcSp:
                        if (expectedCtcStr != null && expectedCtcStr.length() > 0){
                            expectedCtcStr = expectedCtcStr + "." +expectedCTCSpinT.getSelectedItem().toString();
                        }else{
                            expectedCtcStr = "0" + "." +expectedCTCSpinT.getSelectedItem().toString();
                        }
                        mSetupData.setExpectedCtc(expectedCtcStr);
                        break;
                    case R.id.currrentLocSp:
                        mSetupData.setCurrentLocation(cuttentLocSpin.getSelectedItem().toString());
                        if (cuttentLocSpin.getSelectedIds().size() > 0){
                            getLocalities("" + cuttentLocSpin.getSelectedIds().get(0));
//                            for (Map.Entry<String, String> e : locationsMap.entrySet()) {
//                                if (e.getValue().equalsIgnoreCase(cuttentLocSpin.getSelectedItem().toString())){
//                                    getLocalities("" + e.getKey());
//                                    break;
//                                }
//
//                            }
                        }

                        break;
                    case R.id.notiSp:
                        if (noticeSpin.getSelectedItemId() == 0 )
                            mSetupData.setNoticePeriod("0");
                        else
                        mSetupData.setNoticePeriod(CommonUtils.convertToDays(noticeSpin.getSelectedItem().toString()));
                        break;
                    case R.id.eduSp:
                        if(educationSpin.getSelectedItem().toString().equalsIgnoreCase("others")){
                            dialog_other.show();
                            otherType  = "Education";
                        }else
                        mSetupData.setEduCourse(educationSpin.getSelectedItem().toString());
                        break;
                    case R.id.qualificationSp:
                        if(institutionSpin.getSelectedItem().toString().equalsIgnoreCase("others")){
                            dialog_other.show();
                            otherType  = "Instutution";
                        }else
                        mSetupData.setInstitution(institutionSpin.getSelectedItem().toString());
                        break;
                    case R.id.yearofSp:
                        mSetupData.setYearPassing(yearSpin.getSelectedItemPosition() == 0 ? "NA" : yearSpin.getSelectedItem().toString());
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        class StatusHandler extends Handler {

            public void handleMessage(Message msg)
            {
                if (null != getActivity() && getActivity().isFinishing())
                    return;
                switch (msg.what){
                    case RESULT_ERROR:
                        Toast.makeText(getActivity(),"Error occured while getting data from  server....",Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_INDUSTRIES:
                        if (null != industries && null != currentIndSpin) {
//                            currentIndSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(industries, "Industry")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndSpin,industries,1,"Select Industry");
                        }else{
                            Toast.makeText(getActivity(),"Not able to get industries",Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case RESULT_LOCATIONS:
                        if (null != locationsMap && null != cuttentLocSpin) {
//                            cuttentLocSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(locationsMap, "Location")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),cuttentLocSpin,locationsMap,1,"Select Location");
                            preLocEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(locationsMap, "Location")));
                        }else{
                            Toast.makeText(getActivity(),"Not able to get locations",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case RESULT_LANGUAGE:
                        if (null != languageMap) {
                            languages.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(languageMap, "Language")));
                        }else{
                            Toast.makeText(getActivity(),"Not able to get Languages",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case RESULT_JOBTYPE:
//                        cuttentLocSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(jobTypeMap)));
                        break;
                    case RESULT_EDUCATION:
                        if (null != educationMap && null != educationSpin) {
//                            educationSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(educationMap, "Education")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),educationSpin,educationMap,1,"Select Education");
                        } else {
                            Toast.makeText(getActivity(),"Not able to get education details",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case RESULT_INSTITUTION:
                        if (null != institutionMap && null != institutionSpin) {
//                            institutionSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(),CommonUtils.fromMap(institutionMap, "Institution")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),institutionSpin,institutionMap,1,"Select Institution");
                        } else {
                            Toast.makeText(getActivity(),"Not able to get institution details",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case RESULT_DISIGNATIONS:
                        if (null != disigntionMap && null != currentDisEdt) {
                            List<String> stringList = new ArrayList<String>(Arrays.asList(CommonUtils.fromMap(disigntionMap, "Designation")));
                            currentDisEdt.setAdapter(new SearchableAdapter(getActivity(),stringList));
//                            currentDisEdt.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,CommonUtils.fromMap(disigntionMap, "Designation")));
                        }
                        getCompanies();
                        break;
                    case RESULT_KEYSKILLS:
                        if (null != keySkillsList && keySkillsList.size() > 0) {
//                            keySkillsEdt.setAdapter(new CommonArrayAdapterDropDown(getActivity(), keySkillsList.toArray(new String[keySkillsList.size()])));
                            keySkillsEdt.setAdapter(new SearchableAdapter(getActivity(),keySkillsList));
//                            getDisegnation();
                        }
                        break;
                    case RESULT_ROLES:
                        if (null != roleMap && null != roleSpin) {
//                            roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                            CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,"Select Role");
                            if (mSetupData.getRole()!= null && !mSetupData.getRole().equalsIgnoreCase("")&& roleMap != null)
//                                CommonUtils.spinnerbinditem(roleSpin, CommonUtils.fromMap(roleMap, "Role"), mSetupData.getRole());
                                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),roleSpin,roleMap,1,mSetupData.getRole());
                            if (progressDialog.isShowing())
                                progressDialog.cancel();
                        }
                        break;
                    case RESULT_COMPANIES:
                        if (null != companiesMap && null != currentCompEdt) {
                            if (back_var == true)
                            currentCompEdt.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, CommonUtils.fromMap(companiesMap, "Company")));
                        }
                        break;
                    case  RESULT_USERDATA:
                        uploadUserImage();
//                        showYesNoDialog();
                        break;
                }
            }
        }
        public void profilePic(){
            final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {

                        CommonUtils.setimageDestination(Constant.imageNames.SeekerImage);
                        Intent intent = new Intent(getActivity(), ImageCropActivity.class);
                        intent.putExtra("ACTION", "action-camera");
                        startActivityForResult(intent, FROMCAM);

                    } else if (options[item].equals("Choose from Gallery")) {
                        CommonUtils.setimageDestination(Constant.imageNames.SeekerImage);
                        Intent intent = new Intent(getActivity(), ImageCropActivity.class);
                        intent.putExtra("ACTION", "action-gallery");
                        startActivityForResult(intent, FROMCAM);

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        Bitmap ShrinkBitmap(String file, int width, int height){

            BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
            bmpFactoryOptions.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

            int heightRatio = (int)Math.ceil(bmpFactoryOptions.outHeight/(float)height);
            int widthRatio = (int)Math.ceil(bmpFactoryOptions.outWidth/(float)width);

            if (heightRatio > 1 || widthRatio > 1)
            {
                if (heightRatio > widthRatio)
                {
                    bmpFactoryOptions.inSampleSize = heightRatio;
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio;
                }
            }

            bmpFactoryOptions.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
            return bitmap;
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == FILE_CHOOSER) {
                if (null != data) {
                    imageAdd_var = true;
                    fileSelected = data.getStringExtra("fileSelected");
                    resumeNameTxt.setText(fileSelected);
                    resumeNameTxt.setVisibility(View.VISIBLE);
                    mSetupData.setResumePath(fileSelected);
                    File dir = Environment.getExternalStorageDirectory();
                    resume = new File(fileSelected);
                    try{
                        resumeFile = CommonUtils.encodeFileToBase64Binary(resume);

                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Getting Resume data..");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        final boolean _active = true;
                        final int _splashTime = 1000;
                        Thread splashTread = new Thread() {
                            @Override
                            public void run() {
                                try {
                                    int waited = 0;
                                    while (_active && (waited < _splashTime)) {
                                        sleep(100);
                                        if (_active) {
                                            waited += 100;
                                        }
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } finally {

                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            uploadDocumentstoServer(Config.getDetailsFromResume);
                                        }
                                    });

                                }
                            }
                        };
                        splashTread.start();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }else if (requestCode == FROMCAM) {
                if (null == data)
                    return;

                imageAdd_var = true;
                startCropImage();
            } else if (requestCode == SELECT_PICTURE){
                if (data == null)
                    return;


                Uri uri = data.getData();
                String[] projection = { MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(uri, projection,null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = 2;
                bitmapOptions.inPurgeable = true;
                Bitmap rotatedBitmap = ShrinkBitmap(filePath,80,80);
                Drawable dr = new BitmapDrawable(this.getResources(), rotatedBitmap);
                seekerImag.setImageDrawable(dr);
                seekerImag.invalidate();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 80,outputStream);
                seekerImageData = outputStream.toByteArray();
                mSetupData.setSeekerImageData(seekerImageData);

//                imageString = Base64.encodeToString(seekerImageData, Base64.NO_WRAP);
            }else if (requestCode == REQUEST_CODE_CROP_IMAGE) {
                if (resultCode == RESULT_CANCELED)
                    return;

                Bitmap photo = BitmapFactory.decodeFile(CommonUtils.Image_destination.getPath());
                try {
                    FileOutputStream out = new FileOutputStream(CommonUtils.Image_destination);
                    photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    Bitmap myBitmap = BitmapFactory.decodeFile(CommonUtils.Image_destination.getAbsolutePath());

                    Drawable dr = new BitmapDrawable(this.getResources(), myBitmap);
                    seekerImag.setImageDrawable(dr);
                    seekerImag.invalidate();

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
                    seekerImageData = outputStream.toByteArray();
                    imageString = Base64.encodeToString(seekerImageData, Base64.NO_WRAP);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        private void startCropImage() {
            Intent intent = new Intent(getActivity(), CropImage.class);
            intent.putExtra(CropImage.IMAGE_PATH, CommonUtils.Image_destination.getPath());
            intent.putExtra(CropImage.SCALE, true);

            intent.putExtra(CropImage.ASPECT_X, 1);
            intent.putExtra(CropImage.ASPECT_Y, 1);

            //indicate output X and Y
            intent.putExtra("outputX", 256);
            intent.putExtra("outputY", 256);

            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        }

        private void showYesNoDialog() {
            saveUserInfo();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            CommonDialogFragment yesnoDialog = new CommonDialogFragment();
            yesnoDialog.setCancelable(false);
            yesnoDialog.setDialogTitle("Confirmation");
            yesnoDialog.setDialogMessaage(getResources().getString(R.string.registrationSuccessDialog_Seeker));
            yesnoDialog.show(fragmentManager, "OK");
        }

        public  void  saveUserInfo(){

            SharedPreferences.Editor editor = preferences.edit();
            CommonUtils.isLogin = true;
            CommonUtils.isUserExisted = true;
            Cursor dataCur = DatabaseAccessObject.getuserInfo(Queries.getInstance().checkExistance("JobSeekers"),getActivity());
            if (dataCur != null && dataCur.moveToFirst()) {
                String email = dataCur.getString(dataCur.getColumnIndex("Email"));
                CommonUtils.userEmail =email;
                editor.putString(CommonKeys.LJS_PREF_EMAILID, email);
                editor.putString(CommonKeys.LJS_PREF_PASSWORD, dataCur.getString(dataCur.getColumnIndex("Password")));
                editor.putInt(CommonKeys.LJS_PREF_USERTYPE, 0);
                editor.putBoolean(CommonKeys.LJS_PREF_ISLOGIN, true);
                if(seekerImageData != null) {
                    editor.putString(email, Base64.encodeToString(seekerImageData, Base64.DEFAULT));
                }
                editor.commit();
            }
        }

        private void getResumeData(String responce){
             mSetupData = null;
            try {
                if (responce.length() == 0) {
                    Toast.makeText(getActivity(),"Sorry unable to get the details from resume",Toast.LENGTH_LONG).show();
                    return;
                }
                    JSONObject resultObj = new JSONObject(responce.toString());
                    mSetupData = new SetupData();
                fnameEdt.setText(resultObj.getString(CommonKeys.LJS_FirstName));
                lnameEdt.setText(resultObj.getString(CommonKeys.LJS_LastName));
                emailEdt.setText(resultObj.getString(CommonKeys.LJS_Email));
                contactNumEdt.setText(resultObj.getString(CommonKeys.LJS_ContactNo));
                landNumEdt.setText(resultObj.getString(CommonKeys.LJS_ContactNo_Landline));
                if (!CommonUtils.getDateTime().equalsIgnoreCase(CommonUtils.getCleanDate(resultObj.getString(CommonKeys.LJS_DateOfBirth))))
                dateOfBirth.setText(CommonUtils.getCleanDate(resultObj.getString(CommonKeys.LJS_DateOfBirth)));
                if (resultObj.getString(CommonKeys.LJS_Gender).equalsIgnoreCase("male"))
                    radioMaleButton.setChecked(true);
                else if (resultObj.getString(CommonKeys.LJS_Gender).equalsIgnoreCase("female"))
                    radioFeMaleButton.setChecked(true);
                languages.setText(resultObj.getString(CommonKeys.LJS_languagesKnown));
                currentDisEdt.setText(resultObj.getString(CommonKeys.LJS_CurrentDesignation));
                keySkillsEdt.setText(resultObj.getString(CommonKeys.LJS_KeySkills));
                currentCompEdt.setText(resultObj.getString(CommonKeys.LJS_CurrentCompany));
                preLocEdt.setText(resultObj.getString(CommonKeys.LJS_PreferredLocation));
//                CommonUtils.spinnerbinditem(currentIndSpin, CommonUtils.fromMap(industries, "Industry"), resultObj.getString(CommonKeys.LJS_Industry));
                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),currentIndSpin,industries,0,resultObj.getString(CommonKeys.LJS_Industry));
                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),cuttentLocSpin,locationsMap,0,resultObj.getString(CommonKeys.LJS_Location));
                //Needed Dont Delete
                mSetupData.setRole(resultObj.getString(CommonKeys.LJS_Role));
                CommonUtils.spinnerbinditem(jobTypeSpin, getResources().getStringArray(R.array.positiontype), resultObj.getString(CommonKeys.LJS_Jobtype));
                CommonUtils.spinnerSetLacks(totExpSpin, resultObj.getString(CommonKeys.LJS_Experience));
                CommonUtils.spinnerSetLacks(totexpmonths, resultObj.getString(CommonKeys.LJS_Experience));
                CommonUtils.spinnerSetLacks(cuttentCTCSpinL, resultObj.getString(CommonKeys.LJS_CurrentCTC));
                CommonUtils.spinnerSetThousands(cuttentCTCSpinT, resultObj.getString(CommonKeys.LJS_CurrentCTC));
                CommonUtils.spinnerSetLacks(expectedCTCSpinL, resultObj.getString(CommonKeys.LJS_expectedCTC));
                CommonUtils.spinnerSetThousands(expectedCTCSpinT, resultObj.getString(CommonKeys.LJS_expectedCTC));
//                CommonUtils.spinnerbinditem(educationSpin, CommonUtils.fromMap(educationMap, "Education"), resultObj.getString(CommonKeys.LJS_Education));
                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),educationSpin,educationMap,0,resultObj.getString(CommonKeys.LJS_Education));
//                CommonUtils.spinnerbinditem(institutionSpin, CommonUtils.fromMap(institutionMap, "Institution"), resultObj.getString(CommonKeys.LJS_Institute));
                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),institutionSpin,institutionMap,0,resultObj.getString(CommonKeys.LJS_Institute));
                CommonUtils.spinnerbinditem(yearSpin, CommonUtils.getGenericArrayValuesForYear("Select Year", 36, getActivity()), resultObj.getString(CommonKeys.LJS_YearOfPass));
                resumeHeadEdt.setText(resultObj.getString(CommonKeys.LJS_Role)+resultObj.getString(CommonKeys.LJS_ResumeHeadLine));

                String[] jobType_notice = getResources().getStringArray(R.array.noticeperiod);
                for(int i=0 ; i <jobType_notice.length ; i++)
                {
                    if (jobType_notice[i].contains(CommonUtils.convertToWeeks(resultObj.getString(CommonKeys.LJS_NoticePeriod)))) {
                        noticeSpin.setSelection(i+1);
                    }
                }

            }catch (Exception e) {
//                Toast.makeText(getActivity(),"Error occured while getting data from  server.",Toast.LENGTH_SHORT).show();
            }


        }
    }





    @Override
    public void onFinishYesNoDialog(boolean state) {
        // -- Finish dialog box show msg
//        Toast.makeText(this, "Which Option Selected: " + state,Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, LoginScreen.class));
        finish();
    }

}
