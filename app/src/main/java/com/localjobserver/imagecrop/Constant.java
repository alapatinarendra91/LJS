package com.localjobserver.imagecrop;

/**
 * Created by ANDROID on 22-Sep-16.
 */
public class Constant {
    /**
     * Error constant
     */


    public interface IntentExtras {
        String ACTION_CAMERA = "action-camera";
        String ACTION_GALLERY = "action-gallery";
        String IMAGE_PATH = "image-path";
    }

    public interface PicModes {
        String CAMERA = "Camera";
        String GALLERY = "Gallery";
    }

    public interface imageNames {
        String SeekerImage = "seeker";
        String recruiter = "recruiter";
    }

}


