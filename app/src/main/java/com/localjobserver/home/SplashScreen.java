package com.localjobserver.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.localjobserver.LoginScreen;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.commonutils.Queries;
import com.localjobserver.data.LiveData;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.models.JobsData;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.ui.JobDescriptionActivity;
import com.localjobserver.ui.MainInfoActivity;

import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends FragmentActivity {

    private ViewPager pager;
    private ImageView splashCircle1, splashCircle2, splashCircle3;
    private Button startBtn;
    private ProgressDialog progressDialog;
    public ArrayList<JobsData> jobList;
    private SharedPreferences gcmPrefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        gcmPrefs = this.getSharedPreferences("ljs_prefs", MODE_PRIVATE);
        int BCount = gcmPrefs.getInt("badgeCount",0);
        if(null != getIntent() && (getIntent().getAction().equalsIgnoreCase("fromNotification")) ||  BCount > 0){
            CommonUtils.badgeCount = 0;
            SharedPreferences.Editor editor = gcmPrefs.edit();
            editor.putInt("badgeCount", 0);
            editor.commit();
            getJobsData();
        }else {


            if (DatabaseAccessObject.isRecordExisted(Queries.getInstance().checkExistance("JobSeekers"), SplashScreen.this)&&gcmPrefs.getBoolean("isLogin", false)) {
                startActivity(new Intent(SplashScreen.this, MainInfoActivity.class));
                finish();
            }

            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SplashScreen.this, LoginScreen.class));
                }
            });

            setContentView(R.layout.activity_splash_screen);
            startBtn = (Button)findViewById(R.id.setup_btn);
            pager = (ViewPager) findViewById(R.id.pager);
            splashCircle1 = (ImageView) findViewById(R.id.splash_circle_1);
            splashCircle2 = (ImageView) findViewById(R.id.splash_circle_2);
            splashCircle3 = (ImageView) findViewById(R.id.splash_circle_3);


            pager.setAdapter(new SplashPagerAdapter(this));

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            splashCircle1.setImageResource(R.drawable.splash_circle_select);
                            splashCircle2.setImageResource(R.drawable.splash_circle_unselect);
                            splashCircle3.setImageResource(R.drawable.splash_circle_unselect);
                            break;
                        case 1:
                            splashCircle1.setImageResource(R.drawable.splash_circle_unselect);
                            splashCircle2.setImageResource(R.drawable.splash_circle_select);
                            splashCircle3.setImageResource(R.drawable.splash_circle_unselect);
                            break;
                        case 2:
                            splashCircle1.setImageResource(R.drawable.splash_circle_unselect);
                            splashCircle2.setImageResource(R.drawable.splash_circle_unselect);
                            splashCircle3.setImageResource(R.drawable.splash_circle_select);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }

    public void getJobsData(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(SplashScreen.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        jobList = new ArrayList<>();
        LiveData.getJobDetails(new ApplicationThread.OnComplete<List<JobsData>>(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    if (result != null) {
                        Toast.makeText(SplashScreen.this, "" + result.toString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (result != null) {
                    jobList = (ArrayList) result;
                    progressDialog.dismiss();
                    startActivity(new Intent(SplashScreen.this, JobDescriptionActivity.class).putExtra("jobItems", (ArrayList<JobsData>) jobList).putExtra("selectedPos", 2).putExtra("_id", ((JobsData) jobList.get(2)).get_id()).setAction("fromSearch"));
                    finish();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getJobs, "java", 1));
    }

    private class SplashPagerAdapter extends PagerAdapter {

        private LayoutInflater inflater;

        public SplashPagerAdapter(final Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return 3;
        }

        /**
         * Create the page for the given position.  The adapter is responsible
         * for adding the view to the container given here, although it only
         * must ensure this is done by the time it returns from
         * {@link #finishUpdate(android.view.ViewGroup)}.
         *
         * @param collection The containing View in which the page will be shown.
         * @param position   The page position to be instantiated.
         * @return Returns an Object representing the new page.  This does not
         * need to be a View, but can be some other container of the page.
         */
        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            View view;
            switch (position) {
                case 0:
                    view = inflater.inflate(R.layout.splash_pager1, null);
                    break;
                case 1:
                    view = inflater.inflate(R.layout.splash_pager2, null);
                    break;
                case 2:
                    view = inflater.inflate(R.layout.splash_pager3, null);
                    break;
                default:
                    view = null;
                    break;
            }

            collection.addView(view, 0);
            return view;
        }

        /**
         * Remove a page for the given position.  The adapter is responsible
         * for removing the view from its container, although it only must ensure
         * this is done by the time it returns from {@link #finishUpdate(android.view.ViewGroup)}.
         *
         * @param collection The containing View from which the page will be removed.
         * @param position   The page position to be removed.
         * @param view       The same object that was returned by
         *                   {@link #instantiateItem(android.view.View, int)}.
         */
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((RelativeLayout) view);
        }


        /**
         * Determines whether a page View is associated with a specific key object
         * as returned by instantiateItem(ViewGroup, int). This method is required
         * for a PagerAdapter to function properly.
         *
         * @param view   Page View to check for association with object
         * @param object Object to check for association with view
         * @return
         */
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }


        /**
         * Called when the a change in the shown pages has been completed.  At this
         * point you must ensure that all of the pages have actually been added or
         * removed from the container as appropriate.
         *
         * @param arg0 The containing View which is displaying this adapter's
         *             page views.
         */
        @Override
        public void finishUpdate(ViewGroup arg0) {
        }


        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup arg0) {
        }

    }

}
