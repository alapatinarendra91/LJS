package com.localjobserver.filter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import com.localjobserver.networkutils.Log;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.keyskillsAdapter.SearchableAdapter;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.ui.JobListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by ANDROID on 04/01/2017.
 */
public class FilterSeekerFragment extends Fragment implements View.OnClickListener {
    private View rootView;
    private MultiAutoCompleteTextView autoKeys;
    private Spinner roleSp, expMinSp,expSp;
    private TextView jobTypeTxt,PostedbyTxt,walkinTxt;
    private MultiSpinnerSearch LocTxt,EduTxt,Industrytxt;
    private String jobType_str  = "",postedByStr = "";
    public StringBuffer searchStr;
    private Button searchBtn,cancilBtn;
    private ProgressDialog progressDialog;private static  int SEARCH_RESULT = 0;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_KEYSKILLS = 1;
    private static final int RESULT_INDUSTRIES = 2;
    private static final int RESULT_LOCATIONS = 3;
    private static final int RESULT_LOCALRESULT = 4;
    private static final int RESULT_DISIGNATIONS = 5;
    private static final int RESULT_EDUCATION = 6;
    private static final int RESULT_ROLES = 7;
    private String selectedInd = "";
    public LinkedHashMap<String,String> industries = null, locationsMap = null,disigntionMap = null,educationMap,roleMap;
    public ArrayList<String> keySkillsList = null;
    private String activityComingFrom = "",industriesArrayString = "",keySkill = "";
    private Dialog dialog;
    private ListView dialog_list;
    private TextView staticTextView;
    private Button dialog_doneBtn;
    private FilterDialogAdaptor mFilterDialogAdaptor;
    private ArrayList<String> check_list = new ArrayList<>();
    private TextView listName;
    private String[] splitParts,dataActual;

    public FilterSeekerFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filter_seeker, container, false);
        Bundle bundle = this.getArguments();
        activityComingFrom = bundle.getString("Filter");
        keySkill = bundle.getString("keySkills");

        autoKeys = (MultiAutoCompleteTextView)rootView.findViewById(R.id.autoskillskeyword);
        autoKeys.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        autoKeys.setThreshold(1);
        LocTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.LocTxt);
        EduTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.EduTxt);
        expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        roleSp = (Spinner)rootView.findViewById(R.id.roleSp);
        Industrytxt = (MultiSpinnerSearch)rootView.findViewById(R.id.Industrytxt);
        walkinTxt = (TextView)rootView.findViewById(R.id.walkinTxt);
        PostedbyTxt = (TextView)rootView.findViewById(R.id.PostedbyTxt);
        jobTypeTxt = (TextView)rootView.findViewById(R.id.jobTypeTxt);
        searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
        cancilBtn = (Button)rootView.findViewById(R.id.cancelBtn);
        staticTextView = new TextView(getActivity());

        walkinTxt.setText("All");

        dialog = CommonUtils.dialogIntialize(getActivity(),R.layout.filter_dialoglist);
        dialog_list = (ListView) dialog.findViewById(R.id.dialog_list);
        dialog_doneBtn = (Button) dialog.findViewById(R.id.dialog_doneBtn);

        dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
//                if (null != industries && position !=0){
//                    selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
//                    CommonUtils.showToast(""+selectedInd,getActivity());
//                }

                String test = ",";
                listName = (TextView) view.findViewById(R.id.listName);
                staticTextView.setTextColor(getResources().getColor(R.color.blue2));
                if (position != 0)
                if (check_list.get(position).equalsIgnoreCase("true")){
                    check_list.set(position, "false");
                    staticTextView.setText(staticTextView.getText().toString().replace(listName.getText().toString(), "").replace(",,", ","));
//                    if (staticTextView == Industrytxt){
//                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
//                        industriesArrayString = industriesArrayString.replace(""+selectedInd,"").replace(",,", ",");
//                    }else
                        if (staticTextView == jobTypeTxt){
                        jobType_str = jobType_str.replace(""+position,"").replace(",,", ",");
                    }

                    if (staticTextView.getText().toString().equalsIgnoreCase(",")){
                        staticTextView.setText("");
                            if (staticTextView == jobTypeTxt){
                            jobType_str = "";
                        }
                    }

                    if (staticTextView.getText().toString().length() > 0 && (staticTextView.getText().toString().charAt(0) == test.toString().charAt(0)))
                        staticTextView.setText(staticTextView.getText().toString().substring(1));

                }else {
//                    if (staticTextView == Industrytxt){
//                        selectedInd = industries.keySet().toArray(new String[industries.size()])[position-1];
//                        if (staticTextView.getText().toString().length()>0)
//                            industriesArrayString = industriesArrayString+","+selectedInd;
//                        else
//                            industriesArrayString = selectedInd;
//                    }else
                        if (staticTextView == jobTypeTxt){
                        if (staticTextView.getText().toString().length()>0)
                            jobType_str = jobType_str+","+position;
                        else
                            jobType_str = ""+position;
                    }

                    check_list.set(position, "true");
                    if (staticTextView.getText().toString().length()>0){
                        staticTextView.append(","+listName.getText().toString());
                        if (staticTextView.getText().toString().charAt(0) == test.toString().charAt(0))
                            staticTextView.setText(staticTextView.getText().toString().substring(1));
                    }
                    else
                        staticTextView.append(""+listName.getText().toString());

                }
                mFilterDialogAdaptor.notifyDataSetChanged();
            }
        });

        searchStr  = new StringBuffer();
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        roleSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));

        expMinSp.setSelection(1);
        expSp.setSelection(1);

//        Industrytxt.setOnClickListener(this);
        jobTypeTxt.setOnClickListener(this);
        PostedbyTxt.setOnClickListener(this);
        walkinTxt.setOnClickListener(this);
//        LocTxt.setOnClickListener(this);
//        EduTxt.setOnClickListener(this);
        dialog_doneBtn.setOnClickListener(this);

        expSp.setOnItemSelectedListener(spinListener);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchStr.append(autoKeys.getText().toString());
                if (!CommonUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), "Please check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                    return;
                }
//                if (!CommonUtils.checkEditTextEampty(autoKeys) &&
//                        !CommonUtils.checkSpinnerposition(roleSp) &&
//                        expSp.getSelectedItem().toString().equalsIgnoreCase("0")
//                        ){
//                    CommonUtils.showToast("Please select any search criteria.",getActivity());
//                    return;
//                }


                LinkedHashMap<String,String> input = toUpload();
                JSONObject json = new JSONObject(input);
                String requestString = json.toString();
                Log.e("TAG", "Request Str is :" + requestString);
                requestString = CommonUtils.encodeURL(requestString);
                Bundle searchBundle = new Bundle();
                searchBundle.putString("searchTxt", requestString);
                searchBundle.putString("selectedJobType", selectedInd);
                Intent jobListIntent = new Intent(getActivity(), JobListActivity.class);
                jobListIntent.putExtras(searchBundle);
                startActivity(jobListIntent);
            }
        });

        cancilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

//        getKeyWords();
        getIndustries();
        getLocations();
        getEducations();

        if (activityComingFrom != null && activityComingFrom.equalsIgnoreCase("Filter"))
            autoKeys.setVisibility(View.GONE);

        return rootView;
    }


    private  LinkedHashMap  toUpload(){
        industriesArrayString = "";
        if (Industrytxt.getSelectedIds().size() > 0)
            for (int i = 0;i <Industrytxt.getSelectedIds().size();i++ )
                industriesArrayString = i == 0 ? Industrytxt.getSelectedIds().get(i) : industriesArrayString+","+Industrytxt.getSelectedIds().get(i);

        LinkedHashMap seekerDataMap = new LinkedHashMap();
        seekerDataMap.put("KeyWords",""+keySkill);
        seekerDataMap.put("Location",LocTxt.getSelectedItem().toString().contains("Select Location") ? "" : LocTxt.getSelectedItem().toString());
        seekerDataMap.put("SubLocation","");
        seekerDataMap.put("MinExp",""+expMinSp.getSelectedItem().toString());
//            seekerDataMap.put("MinExp",""+expMinSp.getItems().toString());
        seekerDataMap.put("MaxExp",""+expSp.getSelectedItem().toString());
        seekerDataMap.put("MaxSal","0");
        seekerDataMap.put("MinSal","0");
        seekerDataMap.put("JobType",jobType_str.toString().equals(",") ? "" : jobType_str);
//        seekerDataMap.put("Industry",""+Industrytxt.getText().toString());
        seekerDataMap.put("Industry",""+industriesArrayString);
        seekerDataMap.put("Role", "");
        seekerDataMap.put("Education", EduTxt.getSelectedItem().toString().contains("Select Education") ? "" : EduTxt.getSelectedItem().toString());
//        seekerDataMap.put("PostedBy", PostedbyTxt.getText().toString());
        seekerDataMap.put("PostedBy", getPostedby(PostedbyTxt.getText().toString()));
        seekerDataMap.put("JType", walkinTxt.getText().toString());
        return seekerDataMap;
    }

    private String getPostedby(String postedby){
        postedByStr = " ";
        if (postedby.contains("Recruiter"))
            postedByStr = "1,";
        if (postedby.contains("Employee"))
            postedByStr = postedByStr + "2,";
        if (postedby.contains("Employer"))
            postedByStr = postedByStr + "3,";

        return postedByStr.substring(0, postedByStr.length() - 1);
    }


    public void getKeyWords(){
//            if (progressDialog == null) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
//            }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    getIndustries();
                    getLocations();
                    getEducations();
                    return;
                }

                if (result != null) {
                    getIndustries();
                    getLocations();
                    getEducations();
                }
            }
        });
    }


    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getIndusPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),Industrytxt,industries,9999,"Select Industry");
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
//            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
//                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),EduTxt,educationMap,9999,"Select Education");
//                        progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }


    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String, String>) result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocTxt,locationsMap,9999,"Select Location");
                    progressDialog.dismiss();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }

    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {

                        }
                    }
                });
            }
        });
    }


    @Override
    public void onClick(View v) {
        check_list = new ArrayList<>();
        switch (v.getId()){
//            case R.id.Industrytxt:
//                staticTextView = Industrytxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(industries, "Industry");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                    for (int j = 0; j < splitParts.length; j++){
//                        if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                            check_list.set(i,"true");
//                    }
//                }
//
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(industries, "Industry"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;

            case R.id.jobTypeTxt:
                staticTextView = jobTypeTxt;

                splitParts = staticTextView.getText().toString().split(",");
                dataActual = getActivity().getResources().getStringArray(R.array.positiontype);
                for (int i = 0; i < dataActual.length; i++){
                    check_list.add("false");
                    if (splitParts != null && splitParts.length > 0)
                        for (int j = 0; j < splitParts.length; j++){
                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
                                check_list.set(i,"true");
                        }
                }
                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),getActivity().getResources().getStringArray(R.array.positiontype),check_list);
                dialog_list.setAdapter(mFilterDialogAdaptor);
                dialog.show();
                break;

            case R.id.PostedbyTxt:
                staticTextView = PostedbyTxt;
                splitParts = staticTextView.getText().toString().split(",");
                dataActual = getActivity().getResources().getStringArray(R.array.postedBy);
                for (int i = 0; i < dataActual.length; i++){
                    check_list.add("false");
                    if (splitParts != null && splitParts.length > 0)
                        for (int j = 0; j < splitParts.length; j++){
                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
                                check_list.set(i,"true");
                        }
                }
                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),getActivity().getResources().getStringArray(R.array.postedBy),check_list);
                dialog_list.setAdapter(mFilterDialogAdaptor);
                dialog.show();
                break;

            case R.id.walkinTxt:
                staticTextView = walkinTxt;
                splitParts = staticTextView.getText().toString().split(",");
                dataActual = getActivity().getResources().getStringArray(R.array.referralJobs);
                for (int i = 0; i < dataActual.length; i++){
                    check_list.add("false");
                    if (splitParts != null && splitParts.length > 0)
                        for (int j = 0; j < splitParts.length; j++){
                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
                                check_list.set(i,"true");
                        }
                }
                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),getActivity().getResources().getStringArray(R.array.referralJobs),check_list);
                dialog_list.setAdapter(mFilterDialogAdaptor);
                dialog.show();
                break;

//            case R.id.LocTxt:
//                staticTextView = LocTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(locationsMap, "Location");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(locationsMap, "Location"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;
//
//            case R.id.EduTxt:
//                staticTextView = EduTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(educationMap, "Education");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(educationMap, "Education"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;

            case R.id.dialog_doneBtn:
                dialog.cancel();
                break;
        }

    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){
                case R.id.expMinSp:
                    if (expMinSp.getSelectedItemId() == 31){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() == 1){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() != 0){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.expSp:
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
