package com.localjobserver.filter;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localjobserver.commonutils.Queries;
import com.localjobserver.databaseutils.DatabaseAccessObject;
import com.localjobserver.multiplespinner.MultiSpinnerSearch;
import co.talentzing.R;
import com.localjobserver.commonutils.CommonArrayAdapter;
import com.localjobserver.commonutils.CommonKeys;
import com.localjobserver.commonutils.CommonUtils;
import com.localjobserver.data.LiveData;
import com.localjobserver.models.SearchModel;
import com.localjobserver.networkutils.ApplicationThread;
import com.localjobserver.networkutils.Config;
import com.localjobserver.networkutils.HttpClient;
import com.localjobserver.networkutils.Log;
import com.localjobserver.ui.ResumesListActivity;
import com.localjobserver.validator.validator.Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterRecruiterFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    private View rootView;
    private LinearLayout keySkillsLay;
    private  MultiAutoCompleteTextView keySkillsEdt;
    private Spinner expMinSp,totCtcSpmin,totThCtcSp,expSp,roleSpin,scopeSpin;
    private TextView activeLastTxt;
    private MultiSpinnerSearch LocTxt,preLocTxt,Industrytxt,compNameTxt,degreeHighestTxt;
    private Button submitBtn,cancelBtn;
    private String selectedInd;
    public LinkedHashMap<String,String> industries = null, locationsMap = null,roleMap;
    private ProgressDialog progressDialog;
    private static final int RESULT_ERROR = 0;
    private static final int RESULT_USERDATA = 1;
    private static final int RESULT_KEYSKILLS = 2;
    private ArrayList<String> keySkillsList;
    private LinkedHashMap<String, String> companiesMap;
    private LinkedHashMap<String, String> educationMap;
    private SearchModel searchModel;
    private  boolean isClassVisible = false;
    private String activityComingFrom = "";
    private Dialog dialog;
    private ListView dialog_list;
    private TextView staticTextView;
    private Button dialog_doneBtn;
    private FilterDialogAdaptor mFilterDialogAdaptor;
    private ArrayList<String> check_list = new ArrayList<>();
    private TextView listName;
    private String[] splitParts,dataActual;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_filter_recruiter, container, false);
        searchModel = new SearchModel();
        keySkillsLay = (LinearLayout) rootView.findViewById(R.id.keySkillsLay);
        keySkillsEdt = (MultiAutoCompleteTextView)rootView.findViewById(R.id.keySkillsEdt);
        LocTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.LocTxt);
        preLocTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.preLocTxt);
        compNameTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.compNameTxt);
        keySkillsEdt.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        scopeSpin = (Spinner) rootView.findViewById(R.id.scopeSpin);
        expMinSp = (Spinner)rootView.findViewById(R.id.expMinSp);
        expSp = (Spinner)rootView.findViewById(R.id.expSp);
        Industrytxt = (MultiSpinnerSearch)rootView.findViewById(R.id.Industrytxt);
        totCtcSpmin = (Spinner)rootView.findViewById(R.id.totCtcSp);
        totThCtcSp = (Spinner)rootView.findViewById(R.id.totThCtcSp);
        roleSpin = (Spinner)rootView.findViewById(R.id.roleSpin);
        degreeHighestTxt = (MultiSpinnerSearch)rootView.findViewById(R.id.degreeHighestTxt);
        activeLastTxt = (TextView)rootView.findViewById(R.id.activeLastTxt);

        activeLastTxt.setText("90Days");

//        scopeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.scope)));
        expMinSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 31)));
        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totThCtcSp.setOnItemSelectedListener(spinListener);
        totCtcSpmin = (Spinner)rootView.findViewById(R.id.totCtcSpmin);
        totCtcSpmin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericArrayValues("", 100)));
        totCtcSpmin.setOnItemSelectedListener(spinListener);
        roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.roletype)));
        scopeSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), getActivity().getResources().getStringArray(R.array.scope)));

        submitBtn = (Button)rootView.findViewById(R.id.submitBtn);
        cancelBtn = (Button)rootView.findViewById(R.id.cancelBtn);

            keySkillsLay.setVisibility(View.GONE);
        scopeSpin.setSelection(1);
//            getKeyWords();

        getIndustries();

//        LocTxt.setOnClickListener(this);
//        preLocTxt.setOnClickListener(this);
//        Industrytxt.setOnClickListener(this);
//        compNameTxt.setOnClickListener(this);
//        degreeHighestTxt.setOnClickListener(this);
        activeLastTxt.setOnClickListener(this);


        expMinSp.setOnItemSelectedListener(spinListener);
        expSp.setOnItemSelectedListener(spinListener);
        roleSpin.setOnItemSelectedListener(spinListener);
        scopeSpin.setOnItemSelectedListener(spinListener);
        expMinSp.setSelection(1);
        totCtcSpmin.setSelection(1);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    postSearchData(toPostSearch(searchModel));
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        dialog = CommonUtils.dialogIntialize(getActivity(),R.layout.filter_dialoglist);
        dialog_list = (ListView) dialog.findViewById(R.id.dialog_list);
        dialog_doneBtn = (Button) dialog.findViewById(R.id.dialog_doneBtn);
        dialog_doneBtn.setOnClickListener(this);

        dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                listName = (TextView) view.findViewById(R.id.listName);
                staticTextView.setTextColor(getResources().getColor(R.color.blue2));
                if (position != 0)
                if (check_list.get(position).equalsIgnoreCase("true")) {
                    check_list.set(position, "false");
                    staticTextView.setText(staticTextView.getText().toString().replace(listName.getText().toString(), "").replace(",,", ","));
                    if (staticTextView.getText().toString().equalsIgnoreCase(","))
                        staticTextView.setText("");
                } else {
                    check_list.set(position, "true");
                    if (staticTextView.getText().toString().length() > 0)
                        staticTextView.append("," + listName.getText().toString());
                    else
                        staticTextView.append("" + listName.getText().toString());
                }

                mFilterDialogAdaptor.notifyDataSetChanged();


            }
        });

        return rootView;
    }


    public void postSearchData(LinkedHashMap<String,String> input){
        JSONObject json = new JSONObject(input);
        String requestString = json.toString();
        Log.e("TAG", "ReqSt Str  is  :" + requestString);
        requestString = CommonUtils.encodeURL(requestString);
        String preparedUrltoGetResumes = String.format(Config.LJS_BASE_URL + Config.ResumeResults, "" + requestString);
        Log.i("", "preparedUrltoGetResumes is  :" + preparedUrltoGetResumes);
        startActivity(new Intent(getActivity(), ResumesListActivity.class).putExtra("searchTxt", "").putExtra("resumesLink", preparedUrltoGetResumes));
    }

    public LinkedHashMap toPostSearch(SearchModel searchModel){
        LinkedHashMap postSearch = new LinkedHashMap();
        postSearch.put("ReturnUrl","");
        postSearch.put("KeySkills",keySkillsEdt.getText().toString());
        postSearch.put("Scope", scopeSpin.getSelectedItem().toString());
        postSearch.put("Location", LocTxt.getSelectedItem().toString().contains("Select Location") ? "" : LocTxt.getSelectedItem().toString());
//        if (!searchModel.getMinExp().equalsIgnoreCase("Select"))
//            postSearch.put("MinExp",""+searchModel.getMinExp());
//        else
//            postSearch.put("MinExp","0");
//
//        if (!searchModel.getMaxExp().equalsIgnoreCase("Select"))
//            postSearch.put("MaxExp",""+searchModel.getMaxExp());
//        else
//            postSearch.put("MaxExp","0");

        postSearch.put("MinExp","0");
        postSearch.put("MaxExp","0");

        if (!searchModel.getCurrentCTC().equalsIgnoreCase("Select"))
            postSearch.put("CurrentCTC",""+searchModel.getCurrentCTC());
        else
            postSearch.put("CurrentCTC","0.0");

        postSearch.put("Industry",Industrytxt.getSelectedItem().toString().contains("Select Industry")? "" : Industrytxt.getSelectedItem().toString());
        postSearch.put("FunctionalArea","");
        postSearch.put("CompanyName",compNameTxt.getSelectedItem().toString().contains("Select Companies")? "" : compNameTxt.getSelectedItem().toString());
        postSearch.put("Role",searchModel.getRole());
        postSearch.put("HighestDegree", degreeHighestTxt.getSelectedItem().toString().contains("Select Education")? "" : degreeHighestTxt.getSelectedItem().toString());
        postSearch.put("CandidatesActiveinLast", activeLastTxt.getText().toString().replace("Days",""));
        postSearch.put("ResumeperPage", "");
        postSearch.put("ActiveDate", "");
        postSearch.put("PreferedLocation", preLocTxt.getSelectedItem().toString().contains("Select Location")? "" : preLocTxt.getSelectedItem().toString());
        postSearch.put("SearchQuery", "");
        postSearch.put("Email",""+ DatabaseAccessObject.getSingleString(Queries.getInstance().getSingleValue("CompanyEmailId", "SubUserRegistration"), getActivity()));
        postSearch.put("SearchType","");
//        postSearch.put("Date", "" + CommonUtils.getDateTime());
        postSearch.put("SaveId", "0");
        postSearch.put("Rid", "0");
        postSearch.put("Fid", "0");
        postSearch.put("IndId", "" + searchModel.getIndId());
        postSearch.put("Education", degreeHighestTxt.getSelectedItem().toString().contains("Select Education")? "" : degreeHighestTxt.getSelectedItem().toString());
        postSearch.put("NoticePeriod", "0");
        return postSearch;
    }

    public void getLocations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    getCompanies();
                    return;
                }
                if (result != null) {
                    locationsMap = (LinkedHashMap<String,String>) result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocTxt,locationsMap,9999,"Select Location");
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),preLocTxt,locationsMap,9999,"Select Location");
                    getCompanies();

                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getLocations), CommonKeys.arrLocations);
    }
    public void getCompanies(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    getEducations();
                    return;
                }
                if (result != null) {
                    companiesMap = (LinkedHashMap<String,String>) result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),compNameTxt,companiesMap,9999,"Select Companies");
                    getEducations();
                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getCompanies), CommonKeys.arrCompanies);
    }

    public void getEducations(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
            progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();

                    return;
                }
                if (result != null) {
                    educationMap = (LinkedHashMap<String,String>)result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),degreeHighestTxt,educationMap,9999,"Select Education");
                        progressDialog.dismiss();
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getEducation), CommonKeys.arrEducation);
    }

    public void getKeyWords(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        getKeySkills(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    progressDialog.dismiss();
                    return;
                }

                if (result != null) {
                    Message messageToParent = new Message();
                    messageToParent.what = RESULT_KEYSKILLS;
                    Bundle bundleData = new Bundle();
                    messageToParent.setData(bundleData);
                    new StatusHandler().sendMessage(messageToParent);
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void getIndustries(){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    getLocations();
                    return;
                }
                if (result != null) {
                    industries = (LinkedHashMap<String, String>) result;
                    CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),Industrytxt,industries,9999,"Select Industry");
                    getLocations();

                }
            }
        }, String.format(Config.LJS_BASE_URL + Config.getIndustries), CommonKeys.arrIndustries);
    }


    public void  getKeySkills(final ApplicationThread.OnComplete oncomplete){
        ApplicationThread.bgndPost(getClass().getSimpleName(), "Getting KeySkills...", new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub

                HttpClient.download(String.format(Config.LJS_BASE_URL + Config.getKeywords), false, new ApplicationThread.OnComplete() {
                    public void run() {

                        if (success == false || result == null) {
                            oncomplete.execute(false, null, null);
                            return;
                        }
                        JSONArray jArray = null;
                        keySkillsList = new ArrayList<>();

                        try {
                            jArray = new JSONArray(result.toString());
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject c = jArray.getJSONObject(i);
                                keySkillsList.add(c.getString(CommonKeys.LJS_UserKeySkills));
                            }
                            oncomplete.execute(true, keySkillsList, null);

                        } catch (JSONException e) {
                            Message messageToParent = new Message();
                            messageToParent.what = 0;
                            Bundle bundleData = new Bundle();
                            bundleData.putString("Time Over", "Lost Internet Connection");
                            messageToParent.setData(bundleData);
                            new StatusHandler().sendMessage(messageToParent);
                        }
                    }
                });
            }
        });

    }
    class StatusHandler extends Handler {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case RESULT_ERROR:
                    Toast.makeText(getActivity(), "Error while getting data from server.", Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                case RESULT_USERDATA:

                    break;
            }
        }
    }

    public void getRoles(final String selectedInd){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
        LiveData.getGenericPairValues(new ApplicationThread.OnComplete(ApplicationThread.OnComplete.UI) {
            public void run() {
                if (success == false) {
                    Toast.makeText(getActivity(),"Error occured while getting data from server.",Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    return;
                }
                if (result != null) {
                    progressDialog.dismiss();
                    roleMap = (LinkedHashMap<String,String>)result;
                    if (null != roleMap && null != roleSpin) {
                        roleSpin.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.fromMap(roleMap, "Role")));
                    }
                }
            }
        },String.format(Config.LJS_BASE_URL + Config.getRoles,selectedInd), CommonKeys.arrRoles);
    }

    public boolean validateFields(){
        Form mForm = new Form(getActivity());

        if (keySkillsEdt.getText().toString().equals("") &&
                LocTxt.getSelectedIds().size() == 0&&
                preLocTxt.getSelectedIds().size() == 0&&
                compNameTxt.getSelectedIds().size() == 0&&
                Industrytxt.getSelectedIds().size() == 0&&
                expMinSp.getSelectedItem().toString().equalsIgnoreCase("0")&&
                degreeHighestTxt.getSelectedIds().size() == 0){
            Toast.makeText(getActivity(),"Please Select any critaria",Toast.LENGTH_SHORT).show();
            return (mForm.isValid()) ? false : false;

        }else
            return (mForm.isValid()) ? true : false;

    }

    @Override
    public void onClick(View v) {
        check_list = new ArrayList<>();
        switch (v.getId()){

            case R.id.LocTxt:
//                staticTextView = LocTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(locationsMap, "Location");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(locationsMap, "Location"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),LocTxt,locationsMap,1,"Select Location");
                break;

            case R.id.preLocTxt:
//                staticTextView = preLocTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(locationsMap, "Prefered Location");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(locationsMap, "Prefered Location"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                CommonUtils.setMultispuinnerDataFromHashmap(getActivity(),preLocTxt,locationsMap,1,"Select Location");
                break;

//            case R.id.Industrytxt:
//                staticTextView = Industrytxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(industries, "Industry");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(industries, "Industry"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;
//
//            case R.id.compNameTxt:
//                staticTextView = compNameTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(companiesMap, "Company");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(companiesMap, "Company"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;
//
//            case R.id.degreeHighestTxt:
//                staticTextView = degreeHighestTxt;
//                splitParts = staticTextView.getText().toString().split(",");
//                dataActual = CommonUtils.fromMap(educationMap, "Qualification");
//                for (int i = 0; i < dataActual.length; i++){
//                    check_list.add("false");
//                    if (splitParts != null && splitParts.length > 0)
//                        for (int j = 0; j < splitParts.length; j++){
//                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
//                                check_list.set(i,"true");
//                        }
//                }
//
//                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),CommonUtils.fromMap(educationMap, "Qualification"),check_list);
//                dialog_list.setAdapter(mFilterDialogAdaptor);
//                dialog.show();
//                break;

            case R.id.activeLastTxt:
                staticTextView = activeLastTxt;
                splitParts = staticTextView.getText().toString().split(",");
                dataActual = getActivity().getResources().getStringArray(R.array.activeInLast);
                for (int i = 0; i < dataActual.length; i++){
                    check_list.add("false");
                    if (splitParts != null && splitParts.length > 0)
                        for (int j = 0; j < splitParts.length; j++){
                            if (dataActual[i].toString().equalsIgnoreCase(splitParts[j].toString()))
                                check_list.set(i,"true");
                        }
                }
                mFilterDialogAdaptor = new FilterDialogAdaptor(getActivity(),getActivity().getResources().getStringArray(R.array.activeInLast),check_list);
                dialog_list.setAdapter(mFilterDialogAdaptor);
                dialog.show();
                break;

            case R.id.dialog_doneBtn:
                dialog.cancel();
                break;
    }
    }

    AdapterView.OnItemSelectedListener spinListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()){

                case R.id.expMinSp:
                    searchModel.setMinExp(expMinSp.getSelectedItem().toString());
                    if (expMinSp.getSelectedItemId() == 31){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(30, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() == 1){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString()), 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }else if (expMinSp.getSelectedItemId() != 0){
                        expSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(expMinSp.getSelectedItem().toString())+1, 31)));
                        expSp.setOnItemSelectedListener(spinListener);
                    }
                    break;

                case R.id.totCtcSpmin:
                    searchModel.setCurrentCTC(totCtcSpmin.getSelectedItem().toString());

                    if (totCtcSpmin.getSelectedItemId() == 100){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(99, 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totCtcSpmin.getSelectedItemId() == 1){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSpmin.getSelectedItem().toString()), 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }else if (totCtcSpmin.getSelectedItemId() != 0){
                        totThCtcSp.setAdapter(CommonArrayAdapter.getInstance().getGenericAdapper(getActivity(), CommonUtils.getGenericAfterArrayValues(Integer.parseInt(totCtcSpmin.getSelectedItem().toString())+1, 100)));
                        totThCtcSp.setOnItemSelectedListener(spinListener);
                    }
                    break;
                case R.id.totThCtcSp:
                    searchModel.setCurrentCTC(totCtcSpmin.getSelectedItem().toString()+"."+totThCtcSp.getSelectedItem().toString());
                    break;

                case R.id.expSp:
                    searchModel.setMaxExp(expSp.getSelectedItem().toString());
                    break;
                case R.id.roleSpin:
                    if (null != roleMap &&  roleSpin.getSelectedItemId() != 0)
                        searchModel.setRole(roleSpin.getSelectedItem().toString());
                    else
                        searchModel.setRole("");
                    break;

            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        isClassVisible = true;
    }


    @Override
    public void onPause()
    {
        super.onPause();
        isClassVisible = false;
    }
}
