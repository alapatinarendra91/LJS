package com.localjobserver.filter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import co.talentzing.R;

import java.util.ArrayList;

/**
 * Created by ANDROID on 04/01/2017.
 */
public class FilterDialogAdaptor extends BaseAdapter {

    private Activity context;
    private String[] itemList;
    private ArrayList<String> check_list = new ArrayList<>();
    public FilterDialogAdaptor(Activity context,  String[] itemList,ArrayList<String> check_list) {
        super();
        check_list.set(0, "hjkj");
        this.context=context;
        this.itemList=itemList;
        this.check_list=check_list;

//
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater= LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.filterdialog, null);
        }
        ImageView listImage = (ImageView)convertView.findViewById(R.id.listImage);
        TextView listName = (TextView)convertView.findViewById(R.id.listName);
        listName.setText(itemList[position].toString());

            if (check_list.get(position).toString().equals("false"))
                listImage.setBackgroundResource(R.drawable.cancil);
            else if (check_list.get(position).toString().equals("true"))
                listImage.setBackgroundResource(R.drawable.done);
            else
                listImage.setBackgroundResource(R.drawable.dot);

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.length;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList[position];
    }

}